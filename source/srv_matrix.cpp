// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifdef __cplusplus
    extern "C"
    {
        #include <cblas.h>
        #include <clapack.h>
    }
#else
    #include <cblas.h>
    #include <clapack.h>
#endif
#include <omp.h>
#include "srv_matrix.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | BLAS/LAPACK/ATLAS DECLARATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // DOUBLE PRECISION FUNCTION DEFINITIONS.
    #define SRV_DGEQRF dgeqrf_
    #define SRV_DORGQR dorgqr_
    #define SRV_DGESVD dgesvd_
    #define SRV_DGESDD dgesdd_
    #define SRV_DSYEV  dsyev_
    #define SRV_DGESV  dgesv_
    #define SRV_DGEEV  dgeev_
    #define SRV_DGEHRD dgehrd_
    #define SRV_DHSEQR dhseqr_
    #define SRV_DORGHR dorghr_
    #define SRV_DTREVC dtrevc_
    // SINGLE PRECISION FUNCTION DEFINITIONS.
    #define SRV_SGEQRF sgeqrf_
    #define SRV_SORGQR sorgqr_
    #define SRV_SGESVD sgesvd_
    #define SRV_SGESDD sgesdd_
    #define SRV_SSYEV  ssyev_
    #define SRV_SGESV  sgesv_
    #define SRV_SGEEV  sgeev_
    #define SRV_SGEHRD sgehrd_
    #define SRV_SHSEQR shseqr_
    #define SRV_SORGHR sorghr_
    #define SRV_STREVC strevc_
    
    extern "C"
    {
        // ############################################################################################################
        // ## DOUBLE PRECISION FUNCTIONS
        // ############################################################################################################
        void SRV_DGEQRF(int * M, int * N, double * A, int * lda, double * tau, double * work, int * lwork, int * info);
        void SRV_DORGQR(int * M, int * N, int * K, double * A, int * lda, double * tau, double * work, int * lwork, int * info);
        void SRV_DGESVD(const char *jobu, const char *jobvt, int *m, int *n, double *A, int *lda, double *s, double *u, int *ldu, double *v, int *ldvt, double *work, int *lwork, int *info);
        void SRV_DGESDD(const char *jobz, int *m, int *n, double *A, int *lda, double *s, double *u, int *ldu, double *v, int *ldvt, double *work, int *lwork, int *iwork, int *info);
        void SRV_DSYEV(const char *jobz, const char *uplo, int *n, double *A, int *lda, double *w, double *work, int *lwork, int *info);
        void SRV_DGESV(int *N, int *NRHS, double *A, int *lda, int *ipiv, double *b, int *ldb, int *info);
        void SRV_DGEEV(char *jobvl, char *jobvr, int *n, double *A, int *lda, double *wr, double *wi, double *vl, int *ldvl, double *vr, int *ldvr, double *work, int *lwork, int *info);
        void SRV_DGEHRD(int *n, int *ilo, int *ihi, double *A, int *lda, double *tau, double *work, int *lwork, int *info);
        void SRV_DHSEQR(char *job, char *compz, int *n, int *ilo, int *ihi, double *H, int *ldh, double *wr, double *wi, double *z, int *ldz, double *work, int *lwork, int *info);
        void SRV_DORGHR(int *n, int *ilo, int *ihi, double *A, int *lda, double *tau, double *work, int *lwork, int *info);
        void SRV_DTREVC(char *side, char *howmny, bool *select, int *n, double *T, int *ldt, double *vl, int *ldvl, double *vr, int *ldvr, int *mm, int *m, double *work, int *info);
        
        // ############################################################################################################
        // ## SINGLE PRECISION FUNCTIONS
        // ############################################################################################################
        void SRV_SGEQRF(int * M, int * N, float * A, int * lda, float * tau, float * work, int * lwork, int * info);
        void SRV_SORGQR(int * M, int * N, int * K, float * A, int * lda, float * tau, float * work, int * lwork, int * info);
        void SRV_SGESVD(const char *jobu, const char *jobvt, int *m, int *n, float *A, int *lda, float *s, float *u, int *ldu, float *v, int *ldvt, float *work, int *lwork, int *info);
        void SRV_SGESDD(const char *jobz, int *m, int *n, float *A, int *lda, float *s, float *u, int *ldu, float *v, int *ldvt, float *work, int *lwork, int *iwork, int *info);
        void SRV_SSYEV(const char *jobz, const char *uplo, int *n, float *A, int *lda, float *w, float *work, int *lwork, int *info);
        void SRV_SGESV(int *N, int *NRHS, float *A, int *lda, int *ipiv, float *b, int *ldb, int *info);
        void SRV_SGEEV(char *jobvl, char *jobvr, int *n, float *A, int *lda, float *wr, float *wi, float *vl, int *ldvl, float *vr, int *ldvr, float *work, int *lwork, int *info);
        void SRV_SGEHRD(int *n, int *ilo, int *ihi, float *A, int *lda, float *tau, float *work, int *lwork, int *info);
        void SRV_SHSEQR(char *job, char *compz, int *n, int *ilo, int *ihi, float *H, int *ldh, float *wr, float *wi, float *z, int *ldz, float *work, int *lwork, int *info);
        void SRV_SORGHR(int *n, int *ilo, int *ihi, float *A, int *lda, float *tau, float *work, int *lwork, int *info);
        void SRV_STREVC(char *side, char *howmny, bool *select, int *n, float *T, int *ldt, float *vl, int *ldvl, float *vr, int *ldvr, int *mm, int *m, float *work, int *info);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GENERAL MATRIX CLASS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <>
    void Matrix<double>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<double> &destination) const
    {
        int M;
        
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        
        destination.set(final_row - initial_row + 1, final_column - initial_column + 1);
        M = final_row - initial_row + 1;
        for (int column = initial_column, destination_column = 0; column <= final_column; ++column, ++destination_column)
            cblas_dcopy(M, m_matrix_values + column * m_number_of_rows + initial_row, 1, destination.m_matrix_values + destination_column * destination.m_number_of_rows, 1);
    }
    
    template <>
    void Matrix<double>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<double> &destination, int destination_row, int destination_column) const
    {
        int M;
        
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        if ((destination_row < 0) || (destination_column < 0))
            throw Exception("Error, invalid submatrix destination row/column.");
        if ((final_row - initial_row + 1 > destination.m_number_of_rows - destination_row) || (final_column - initial_column + 1 > destination.m_number_of_columns - destination_column))
            throw Exception("Error, the destination submatrix is smaller than the original submatrix.");
        
        M = final_row - initial_row + 1;
        for (int column = initial_column, result_column = destination_column; column <= final_column; ++column, ++result_column)
            cblas_dcopy(M, m_matrix_values + column * m_number_of_rows + initial_row, 1, destination.m_matrix_values + result_column * destination.m_number_of_rows + destination_row, 1);
    }
    
    template <>
    void Matrix<double>::copyColumn(int destination_column, const double *column)
    {
        double *destination_ptr = m_matrix_values + destination_column * m_number_of_rows;
        cblas_dcopy(m_number_of_rows, column, 1, destination_ptr, 1);
    }
    
    template <>
    void Matrix<float>::copyColumn(int destination_column, const float *column)
    {
        float *destination_ptr = m_matrix_values + destination_column * m_number_of_rows;
        cblas_scopy(m_number_of_rows, column, 1, destination_ptr, 1);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALLS TO ATLAS FUNCTIONS FOR DOUBLE  |
    //                   | PRECISION FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    
    void MatrixGEMM(double alpha, const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, double beta, Matrix<double> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_dgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, alpha, A(), lda, B(), ldb, beta, R(), ldc);
    }
    
    void MatrixMultiplication(double alpha, const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, Matrix<double> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_dgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, alpha, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixMultiplication(const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, Matrix<double> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_dgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixMultiplication(const Matrix<double> &A, const Matrix<double> &B, Matrix<double> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if (B.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = A.getNumberOfRows();
        n = B.getNumberOfColumns();
        k = B.getNumberOfRows();
        lda = srvMax(1, m);
        ldb = srvMax(1, k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixPartialMultiplication(const Matrix<double> &A, const Matrix<double> &B, int final_column, Matrix<double> &R)
    {
        int lda, ldb, ldc, m, n, k;
        double *r_ptr;
        // Check the matrix geometry.
        if (B.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = A.getNumberOfRows();
        n = srvMin(final_column + 1, B.getNumberOfColumns());
        k = B.getNumberOfRows();
        lda = srvMax(1, m);
        ldb = srvMax(1, k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
        r_ptr = R() + R.getNumberOfRows() * (final_column + 1);
        for (int column = final_column + 1; column < R.getNumberOfColumns(); ++column)
            for (int row = 0; row < R.getNumberOfRows(); ++row, ++r_ptr)
                *r_ptr = 0;
    }
    
    void MatrixQRDecomposition(Matrix<double> &Q, Matrix<double> &R, unsigned int number_of_threads)
    {
        srv::Matrix<double> A(Q);
        int m, n, k, lda, lwork, info;
        double * work;
        VectorDense<double> tau;
        
        // 1) Use the lapack function 'dgeqrf' to compute the QR factorization of the real matrix m-by-n Q.
        //    The function modifies the matrix Q so that, the elements on and above the diagonal of the array
        //    contain the min(m, n)-by-n upper trapezoidal matrix R (R is upper triangular if m >= n); the
        //    elements below the diagonal, with the array TAU, represent the orthogonal matrix Q as a product
        //    of min(m, n) elementary reflectors.
        // ------------------------------------------------------------------------------------------------------------------------------------------
        m = A.getNumberOfRows();
        n = A.getNumberOfColumns();
        k = srvMin(m, n);
        tau.set(k);
        lda = A.getNumberOfRows();
        work = new double[1];
        lwork = -1;
        SRV_DGEQRF(&m, &n, A(), &lda, tau.getData(), work, &lwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new double[lwork];
        SRV_DGEQRF(&m, &n, A(), &lda, tau.getData(), work, &lwork, &info);
        delete [] work;
        // 2) Get the R matrix from the resulting matrix A and extract the values of the Q matrix of m-by-k elements ................................
        R.set(k, n);
        Q.set(m, k);
        #pragma omp parallel num_threads(number_of_threads)
        {
            const int thread_identifier = omp_get_thread_num();
            for (int column = thread_identifier; column < n; column += number_of_threads)
            {
                const double * __restrict__ a_ptr = A(column);
                double * __restrict__ r_ptr = R(column);
                for (int row = 0; row < k; ++row, ++r_ptr, ++a_ptr)
                    *r_ptr = (row <= column)?*a_ptr:0.0;
            }
            for (int column = thread_identifier; column < k; column += number_of_threads)
            {
                const double * __restrict__ a_ptr = A(column);
                double * __restrict__ q_ptr = Q(column);
                // Elements below the diagonal with the array TAU represents the orthogonal matrix Q.
                for (int row = 0; row < m; ++row, ++q_ptr, ++a_ptr)
                    *q_ptr = (row <= column)?0.0:*a_ptr;
            }
        }
        // 3) Use the lapack function 'dorgqr' to generate the M-by-N real matrix Q with orthonormal columns ........................................
        lda = Q.getNumberOfRows();
        work = new double[1];
        lwork = -1;
        SRV_DORGQR(&m, &k, &k, Q(), &lda, tau.getData(), work, &lwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new double[lwork];
        SRV_DORGQR(&m, &k, &k, Q(), &lda, tau.getData(), work, &lwork, &info);
        delete [] work;
        
        /////int M, N;
        /////M = Q.getNumberOfRows();
        /////N = Q.getNumberOfColumns();
        /////
        /////R.set(N, N, 0);
        /////
        /////for (int k = 0; k < N; ++k)
        /////{
        /////    double column_norm;
        /////    
        /////    column_norm = cblas_dnrm2(M, Q(k), 1);
        /////    if ((column_norm < 1e-100) || (isnan(column_norm))) column_norm = 0;
        /////    R(k, k) = column_norm;
        /////    
        /////    // Normalize the 'k'-th column by its norm.
        /////    if (column_norm > 0)
        /////    {
        /////       cblas_dscal(M, 1.0 / column_norm, Q(k), 1);
        /////        
        /////        #pragma omp parallel num_threads(number_of_threads)
        /////        {
        /////            const int thread_identifier = omp_get_thread_num();
        /////            for (int j = k + 1 + thread_identifier; j < N; j += number_of_threads)
        /////            {
        /////                double value;
        /////                
        /////                value = cblas_ddot(M, Q(k), 1, Q(j), 1);
        /////                R(k, j) = value;
        /////                
        /////                if (value != 0)
        /////                    cblas_daxpy(M, -value, Q(k), 1, Q(j), 1);
        /////            }
        /////        }
        /////    }
        /////    else
        /////    {
        /////        for (int j = 0; j < M; ++j)
        /////            Q(j, k) = 0.0;
        /////        for (int j = k + 1; j < N; ++j)
        /////            R(k, j) = 0.0;
        /////    }
        /////}
    }
    
    void MatrixSVD(Matrix<double> &A, Matrix<double> &U, VectorDense<double> &s, Matrix<double> &V, bool economic)
    {
        /////// int m, n, lda, ldu, ldvt, lwork, info;
        /////// char jobu = 'A', jobvt = 'A';
        /////// double *work;
        /////// 
        /////// U.set(A.getNumberOfRows(), A.getNumberOfRows());
        /////// V.set(A.getNumberOfColumns(), A.getNumberOfColumns());
        /////// m = A.getNumberOfRows();
        /////// n = A.getNumberOfColumns();
        /////// lda = A.getNumberOfRows();
        /////// ldu = U.getNumberOfRows();
        /////// ldvt = V.getNumberOfRows();
        /////// s.set(srvMin(m, n));
        /////// work = new double[1];
        /////// lwork = -1;
        /////// SRV_DGESVD(&jobu, &jobvt, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, &info);
        /////// lwork = (int)work[0];
        /////// delete [] work;
        /////// work = new double[(int)lwork];
        /////// SRV_DGESVD(&jobu, &jobvt, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, &info);
        /////// delete [] work;
        /////// for (int column = 0; column < n - 1; ++column) // In-place matrix transpose
        ///////     for (int row = column + 1; row < n; ++row)
        ///////         srvSwap(V(row, column), V(column, row));
        
        int m, n, lda, ldu, ldvt, lwork, info, min;
        double *work;
        int *iwork;
        // jobu values are: A = All values of U and Vt are returned.
        //                  S = the first min(m, n) columns of U and the first min(m, n) rows of Vt are returned in the arrays U and Vt
        //                  N = no columns of U or rows of Vt are returned.
        //                  O = ...
        char jobu;
        
        m = A.getNumberOfRows();
        n = A.getNumberOfColumns();
        min = srvMin(m, n);
        if (economic)
        {
            jobu = 'S';
            U.set(A.getNumberOfRows(), min);
            V.set(min, A.getNumberOfColumns());
        }
        else
        {
            jobu = 'A';
            U.set(A.getNumberOfRows(), A.getNumberOfRows());
            V.set(A.getNumberOfColumns(), A.getNumberOfColumns());
        }
        lda = A.getNumberOfRows();
        ldu = U.getNumberOfRows();
        ldvt = V.getNumberOfRows();
        s.set(min);
        iwork = new int[8 * min];
        work = new double[1];
        lwork = -1;
        SRV_DGESDD(&jobu, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, iwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new double[(int)lwork];
        SRV_DGESDD(&jobu, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, iwork, &info);
        delete [] work;
        delete [] iwork;
    }
    
    /////// void MatrixRSVD(Matrix<double> &A, Matrix<double> &U, VectorDense<double> &s, Matrix<double> &V, unsigned int number_of_threads)
    /////// {
    ///////     Matrix<double> R, aux;
    ///////     
    ///////     if (A.getNumberOfColumns() > A.getNumberOfRows())
    ///////     {
    ///////         A.transpose();
    ///////         MatrixQRDecomposition(A, R, number_of_threads);
    ///////         MatrixSVD(R, U, s, V);
    ///////         MatrixMultiplication(A, U, aux);
    ///////         U = V;
    ///////         V = aux;
    ///////         U.transpose();
    ///////         V.transpose();
    ///////     }
    ///////     else
    ///////     {
    ///////         MatrixQRDecomposition(A, R, number_of_threads);
    ///////         MatrixSVD(R, U, s, V);
    ///////         MatrixMultiplication(A, U, aux);
    ///////         U = aux;
    ///////     }
    /////// }
    
    void MatrixInverse(const Matrix<double> &original, Matrix<double> &inverse)
    {
        int *ipiv, N, info;
        
        if (original.getNumberOfRows() != original.getNumberOfColumns()) throw Exception("Cannot invert a non-square matrix.");
        else N = original.getNumberOfRows();
        
        inverse = original;
        ipiv = new int[N];
        // Turn Y into its LU form, store pivot matrix);
        info = clapack_dgetrf(CblasColMajor, N, N, inverse(), N, ipiv);
        if (info != 0) throw Exception("Error while calculating the inverse of the matrix (Singularity or illegal parameters with BLAS error code %d", info);
        // Use the lapack to calculate the inverse using the previously calculated LU decomposition.
        clapack_dgetri(CblasColMajor, N, inverse(), N, ipiv);
        
        delete [] ipiv;
    }
    
    void MatrixInverse(Matrix<double> &inverse)
    {
        int *ipiv, N, info;
        
        if (inverse.getNumberOfRows() != inverse.getNumberOfColumns()) throw Exception("Cannot invert a non-square matrix.");
        else N = inverse.getNumberOfRows();
        
        ipiv = new int[N];
        // Turn Y into its LU form, store pivot matrix);
        info = clapack_dgetrf(CblasColMajor, N, N, inverse(), N, ipiv);
        if (info != 0) throw Exception("Error while calculating the inverse of the matrix (Singularity or illegal parameters with BLAS error code %d", info);
        // Use the lapack to calculate the inverse using the previously calculated LU decomposition.
        clapack_dgetri(CblasColMajor, N, inverse(), N, ipiv);
        
        delete [] ipiv;
    }
    
    void MatrixSolve(const Matrix<double> &A, const Matrix<double> &B, Matrix<double> &solution)
    {
        int n, nrhs, lda, ldb, *ipiv, info;
        Matrix<double> matrix_a(A), matrix_b(B);
        
        n = matrix_a.getNumberOfRows();
        nrhs = matrix_b.getNumberOfColumns();
        lda = srvMax<int>(1, matrix_a.getNumberOfRows());
        ldb = srvMax<int>(1, matrix_b.getNumberOfRows());
        ipiv = new int[n];
        
        SRV_DGESV(&n, &nrhs, matrix_a(), &lda, ipiv, matrix_b(), &ldb, &info);
        if (info > 0) throw ExceptionSingularity("U(%d, %d) is exactly zero. The factorization has been completed, but the factor U is exactly singular, so the solution could not be computed.", info, info);
        else if (info < 0) throw ExceptionArgument("The %d-th argument has an illegal value.", info);
        
        solution = matrix_b;
        delete [] ipiv;
    }
    
    void MatrixSolve(Matrix<double> &A, Matrix<double> &B, VectorDense<int> &IPIV)
    {
        int n, nrhs, lda, ldb, info;
        
        n = A.getNumberOfRows();
        nrhs = B.getNumberOfColumns();
        lda = srvMax<int>(1, A.getNumberOfRows());
        ldb = srvMax<int>(1, B.getNumberOfRows());
        IPIV.set(n);
        
        SRV_DGESV(&n, &nrhs, A(), &lda, IPIV.getData(), B(), &ldb, &info);
        
        if (info > 0) throw ExceptionSingularity("U(%d, %d) is exactly zero. The factorization has been completed, but the factor U is exactly singular, so the solution could not be computed.", info, info);
        else if (info < 0) throw ExceptionArgument("The %d-th argument has an illegal value.", info);
    }
    void MatrixEigenSymmetric(const Matrix<double> &A, Matrix<double> &V, VectorDense<double> &D)
    {
        char jobz, uplo;
        int lda, lwork, info, n;
        VectorDense<double> work(1);
        
        V = A;
        jobz = 'V';
        uplo = 'U'; // Dunno U -> the leading N-by-N upper triangular part of A contains the upper triangular part of the matrix A. If UPLO = 'L', the leading N-by-N lower triangular part of A contains the lower triangular part of the matrix A.
        n = V.getNumberOfRows();
        lda = srvMax<int>(1, V.getNumberOfRows());
        lwork = -1;
        D.set(A.getNumberOfRows());
        SRV_DSYEV(&jobz, &uplo, &n, V(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
        lwork = (int)work[0];
        work.set(srvMax<int>(1, lwork));
        SRV_DSYEV(&jobz, &uplo, &n, V(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
    }
    
    void MatrixEigenSymmetric(const Matrix<double> &A, VectorDense<double> &D)
    {
        char jobz, uplo;
        int lda, lwork, info, n;
        VectorDense<double> work(1);
        Matrix<double> matrix_a(A);
        
        jobz = 'N';
        uplo = 'U'; // Dunno U -> the leading N-by-N upper triangular part of A contains the upper triangular part of the matrix A. If UPLO = 'L', the leading N-by-N lower triangular part of A contains the lower triangular part of the matrix A.
        n = matrix_a.getNumberOfRows();
        lda = srvMax<int>(1, matrix_a.getNumberOfRows());
        lwork = -1;
        D.set(A.getNumberOfRows());
        SRV_DSYEV(&jobz, &uplo, &n, matrix_a(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
        lwork = (int)work[0];
        work.set(srvMax<int>(1, lwork));
        SRV_DSYEV(&jobz, &uplo, &n, matrix_a(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
    }
    
    void MatrixEigenNonSymmetric(const Matrix<double> &A, bool no_balance, Matrix<double> &V, VectorDense<double> &D)
    {
        if (no_balance)
        {
            int n, ilo, ihi, lwork, info, ldz, lda, ldvl, ldvr;
            char job, compz, side, howmny;
            Matrix<double> matrix_a(A);
            VectorDense<double> tau(A.getNumberOfRows() - 1), work(1), wi(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            ldz = ldvr = lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            ldvl = 1;
            ilo = 1;
            ihi = n;
            
            // DGEHRD reduces a real matrix A to upper Hessenberg form H by an orthogonal similarity transform: Q**T * A * Q = H
            lwork = -1;
            SRV_DGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_DGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            
            V = matrix_a; // Save a copy of H in V.
            // DORGHR generates a real orthogonal matrix Q which is defined as the product of IHI-ILO elementary reflectors
            // of order N, as returned by DGEHRD.
            lwork = -1;
            work.set(1);
            SRV_DORGHR(&n, &ilo, &ihi, V(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DORGHR': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_DORGHR(&n, &ilo, &ihi, V(), &lda, tau.getData(), work.getData(), &lwork, &info);
            
            
            // DHSEQR computes the eigenvalues of a Hessenberg matrix H and, optionally, the matrices T and Z from the
            // Schur decomposition H = Z T Z**T, where T is an upper quasi-triangular matrix (the Schur form), and
            // Z is the orthogonal matrix of Schur vectors.
            //
            // Optionally Z may be postmultiplied into an input orthogonal matrix Q so that this routine can give
            // the Schur factorization of a matrix A which has been reduced to the Hessenberg form H by the
            // orthogonal matrix Q: A = Q * H * Q ** T = (QZ) * T * (QZ) ** T
            job = 'S'; // Compute eigenvalues and the Schur form T.
            compz = 'V'; // Z must contain an orthogonal matrix Q on entry, and the product Q * Z is returned.
            D.set(matrix_a.getNumberOfColumns());
            work.set(1);
            lwork = -1;
            SRV_DHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), V(), &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_DHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), V(), &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            
            // DTREVC computes some or all of the right and/or left eigenvectors of a real upper quasi-triangular matrix T.
            // Matrices of this type are produced by the Schur factorization of a real general matrix: A = Q*T*Q**T,
            // as computed by DHSEQR.
            //
            // The right eigenvector and the left eigenvector y of T corresponding to an eigenvalue w are defined by:
            //
            // T * w = w * x, (y ** T) * T = w * (y ** T)
            // 
            // where y ** T denotes the transpose of y.
            // The eigenvalues are not input to this routine, but are read directly from the diagonal blocks of T.
            //
            // This routine returns the matrices X and/or Y of right and left eigenvectors of T, or the product
            // Q * X and/or Q * Y, where Q is an input matrix. If Q is the orthonormal factor that reduces a matrix
            // A to Schur form T, then Q * X and Q * Y are the matrices of right and left eigenvectors of A.
            side = 'R'; // Compute right eigenvectors only.
            howmny = 'B'; // Compute all right and/or left eigenvectors, backtransformed by the matrices in VR and/or VL.
            work.set(3 * n);
            SRV_DTREVC(&side, &howmny, 0, &n, matrix_a(), &lda, 0, &ldvl, V(), &ldvr, &n, &n, work.getData(), &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DTREVC': Incorrect %d-th argument.", -info);
            // TODO: Workaround: The eigenvectors of the negative conjugate eigenvalues are replaced by their
            // positive counterpart. See if there is any function in LAPACK which does the same thing or if
            // there is a more elegant solution.
            for (unsigned int i = 0; i < wi.size(); ++i)
            {
                if (wi[i] < 0)
                    V.copyColumn(i, V(i - 1));
            }
        }
        else
        {
            char jobvl, jobvr;
            int n, lda, ldvl, ldvr, info, lwork;
            Matrix<double> matrix_a(A);
            VectorDense<double> wi(A.getNumberOfRows()), work(1), scale(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            jobvl = 'N'; // Left eigenvectors of A are not computed.
            jobvr = 'V'; // Right eigenvectors of A are not computed.
            D.set(matrix_a.getNumberOfRows());
            V.set(matrix_a.getNumberOfRows(), matrix_a.getNumberOfRows());
            ldvl = 1; // Leading dimension of the left eigenvectors matrix.
            ldvr = srvMax<int>(1, matrix_a.getNumberOfRows()); // Leading dimension of the right eigenvectors matrix.
            lwork = -1;
            SRV_DGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, V(), &ldvr, work.getData(), &lwork, &info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            SRV_DGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, V(), &ldvr, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            // TODO: Workaround: The eigenvectors of the negative conjugate eigenvalues are replaced by their
            // positive counterpart. See if there is any function in LAPACK which does the same thing or if
            // there is a more elegant solution.
            for (unsigned int i = 0; i < wi.size(); ++i)
                if (wi[i] < 0)
                    V.copyColumn(i, V(i - 1));
        }
    }
    
    void MatrixEigenNonSymmetric(const Matrix<double> &A, bool no_balance, VectorDense<double> &D)
    {
        if (no_balance)
        {
            int n, ilo, ihi, lwork, info, ldz, lda;
            char job, compz;
            Matrix<double> matrix_a(A);
            VectorDense<double> tau(A.getNumberOfRows() - 1), work(1), wi(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            ilo = 1;
            ihi = n;
            lwork = -1;
            
            // DGEHRD reduces a real matrix A to upper Hessenberg form H by an orthogonal similarity transform: Q**T * A * Q = H
            SRV_DGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_DGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            
            // DHSEQR computes the eigenvalues of a Hessenberg matrix H and, optionally, the matrices T and Z from the
            // Schur decomposition H = Z T Z**T, where T is an upper quasi-triangular matrix (the Schur form), and
            // Z is the orthogonal matrix of Schur vectors.
            //
            // Optionally Z may be postmultiplied into an input orthogonal matrix Q so that this routine can give
            // the Schur factorization of a matrix A which has been reduced to the Hessenberg form H by the
            // orthogonal matrix Q: A = Q * H * Q ** T = (QZ) * T * (QZ) ** T
            job = 'E'; // Compute eigenvalues only.
            compz = 'N'; // no Schur vectors are computed.
            D.set(matrix_a.getNumberOfColumns());
            ldz = 1;
            work.set(1);
            lwork = -1;
            SRV_DHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_DHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
        }
        else
        {
            char jobvl, jobvr;
            int n, lda, ldvl, ldvr, info, lwork;
            Matrix<double> matrix_a(A);
            VectorDense<double> wi(A.getNumberOfRows()), work(1);
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            jobvl = 'N'; // Left eigenvectors of A are not computed.
            jobvr = 'N'; // Right eigenvectors of A are not computed.
            D.set(matrix_a.getNumberOfRows());
            ldvl = 1; // Leading dimension of the left eigenvectors matrix.
            ldvr = 1; // Leading dimension of the right eigenvectors matrix.
            lwork = -1;
            SRV_DGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, 0, &ldvr, work.getData(), &lwork, &info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            SRV_DGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, 0, &ldvr, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
        }
    }
    
    void MatrixLogarithm(const Matrix<double> &A, Matrix<double> &B, unsigned int number_of_threads)
    {
        // Check matrix dimensionality ..............................................................................................................
        if (A.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("Matrix logarithm can only be calculated from square images.");
        // Variables ................................................................................................................................
        const unsigned int size = A.getNumberOfRows();
        VectorDense<double> D;
        Matrix<double> V, V2(size, size);
        int ld = srvMax(1, (int)size);
        
        MatrixEigen(A, true, V, D);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
            {
                if (D[i] < 0.0)
                {
                    cblas_dscal(size,             -1.0,  V(i), 1);
                    cblas_dcopy(size, V(i), 1, V2(i), 1);
                    cblas_dscal(size, -std::log(-D[i]), V2(i), 1);
                }
                else if (D[i] == 0.0)
                {
                    cblas_dscal(size, 0.0,  V(i), 1);
                    cblas_dscal(size, 0.0, V2(i), 1);
                }
                else
                {
                    cblas_dcopy(size, V(i), 1, V2(i), 1);
                    cblas_dscal(size,  std::log( D[i]), V2(i), 1);
                }
            }
        }
        B.set(size, size);
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, size, size, size, 1.0, V2(), ld, V(), ld, 0.0, B(), ld);
    }
    
    void MatrixExponential(const Matrix<double> &A, Matrix<double> &B, unsigned int number_of_threads)
    {
        // Check matrix dimensionality ..............................................................................................................
        if (A.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("Matrix exponential can only be calculated from square images.");
        // Variables ................................................................................................................................
        const unsigned int size = A.getNumberOfRows();
        VectorDense<double> D;
        Matrix<double> V, V2(size, size);
        int ld = srvMax(1, (int)size);
        
        MatrixEigen(A, true, V, D);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
            {
                cblas_dcopy(size, V(i), 1, V2(i), 1);
                cblas_dscal(size, std::exp(D[i]), V2(i), 1);
            }
        }
        B.set(size, size);
        cblas_dgemm(CblasColMajor, CblasNoTrans, CblasTrans, size, size, size, 1.0, V2(), ld, V(), ld, 0.0, B(), ld);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALLS TO ATLAS FUNCTIONS FOR SINGLE  |
    //                   | PRECISION FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void MatrixGEMM(float alpha, const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, float beta, Matrix<float> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_sgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, alpha, A(), lda, B(), ldb, beta, R(), ldc);
    }
    
    void MatrixMultiplication(float alpha, const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, Matrix<float> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_sgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, alpha, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixMultiplication(const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, Matrix<float> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if ((!transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (!transposeB) && (B.getNumberOfRows() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((!transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfColumns())) throw Exception("GEMM: Incorrect matrix geometry.");
        else if ((transposeA) && (transposeB) && (B.getNumberOfColumns() != A.getNumberOfRows())) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = (transposeA)?A.getNumberOfColumns():A.getNumberOfRows();
        n = (transposeB)?B.getNumberOfRows():B.getNumberOfColumns();
        k = (transposeB)?B.getNumberOfColumns():B.getNumberOfRows();
        lda = srvMax(1, (transposeA)?k:m);
        ldb = srvMax(1, (transposeB)?n:k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_sgemm(CblasColMajor, (transposeA)?CblasTrans:CblasNoTrans, (transposeB)?CblasTrans:CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixMultiplication(const Matrix<float> &A, const Matrix<float> &B, Matrix<float> &R)
    {
        int lda, ldb, ldc, m, n, k;
        // Check the matrix geometry.
        if (B.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = A.getNumberOfRows();
        n = B.getNumberOfColumns();
        k = B.getNumberOfRows();
        lda = srvMax(1, m);
        ldb = srvMax(1, k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        R.set(m, n);
        cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
    }
    
    void MatrixPartialMultiplication(const Matrix<float> &A, const Matrix<float> &B, int final_column, Matrix<float> &R)
    {
        int lda, ldb, ldc, m, n, k;
        float *r_ptr;
        // Check the matrix geometry.
        if (B.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("GEMM: Incorrect matrix geometry.");
        // Calculate the dominant direction values.
        m = A.getNumberOfRows();
        n = srvMin(final_column + 1, B.getNumberOfColumns());
        k = B.getNumberOfRows();
        lda = srvMax(1, m);
        ldb = srvMax(1, k);
        ldc = srvMax(1, m);
        // Call the cblas routine.
        cblas_sgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, m, n, k, 1.0, A(), lda, B(), ldb, 0.0, R(), ldc);
        r_ptr = R() + R.getNumberOfRows() * (final_column + 1);
        for (int column = final_column + 1; column < R.getNumberOfColumns(); ++column)
            for (int row = 0; row < R.getNumberOfRows(); ++row, ++r_ptr)
                *r_ptr = 0;
    }
    
    void MatrixQRDecomposition(Matrix<float> &Q, Matrix<float> &R, unsigned int number_of_threads)
    {
        srv::Matrix<float> A(Q);
        int m, n, k, lda, lwork, info;
        float * work;
        VectorDense<float> tau;
        
        // 1) Use the lapack function 'dgeqrf' to compute the QR factorization of the real matrix m-by-n Q.
        //    The function modifies the matrix Q so that, the elements on and above the diagonal of the array
        //    contain the min(m, n)-by-n upper trapezoidal matrix R (R is upper triangular if m >= n); the
        //    elements below the diagonal, with the array TAU, represent the orthogonal matrix Q as a product
        //    of min(m, n) elementary reflectors.
        // ------------------------------------------------------------------------------------------------------------------------------------------
        m = A.getNumberOfRows();
        n = A.getNumberOfColumns();
        k = srvMin(m, n);
        tau.set(k);
        lda = A.getNumberOfRows();
        work = new float[1];
        lwork = -1;
        SRV_SGEQRF(&m, &n, A(), &lda, tau.getData(), work, &lwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new float[lwork];
        SRV_SGEQRF(&m, &n, A(), &lda, tau.getData(), work, &lwork, &info);
        delete [] work;
        // 2) Get the R matrix from the resulting matrix A and extract the values of the Q matrix of m-by-k elements ................................
        R.set(k, n);
        Q.set(m, k);
        #pragma omp parallel num_threads(number_of_threads)
        {
            const int thread_identifier = omp_get_thread_num();
            for (int column = thread_identifier; column < n; column += number_of_threads)
            {
                const float * __restrict__ a_ptr = A(column);
                float * __restrict__ r_ptr = R(column);
                for (int row = 0; row < k; ++row, ++r_ptr, ++a_ptr)
                    *r_ptr = (row <= column)?*a_ptr:0.0f;
            }
            for (int column = thread_identifier; column < k; column += number_of_threads)
            {
                const float * __restrict__ a_ptr = A(column);
                float * __restrict__ q_ptr = Q(column);
                // Elements below the diagonal with the array TAU represents the orthogonal matrix Q.
                for (int row = 0; row < m; ++row, ++q_ptr, ++a_ptr)
                    *q_ptr = (row <= column)?0.0f:*a_ptr;
            }
        }
        /////#pragma omp parallel num_threads(number_of_threads)
        /////{
        /////    const int thread_identifier = omp_get_thread_num();
        /////    float * __restrict__ r_ptr = R();
        /////    float * __restrict__ q_ptr = Q();
        /////    for (int column = 0; column < n; ++column)
        /////    {
        /////        const float * __restrict__ a_ptr = A(column);
        /////        for (int row = thread_identifier; row < k; row += number_of_threads)
        /////        {
        /////            *r_ptr = (row <= column)?*a_ptr:0.0f;
        /////            r_ptr += number_of_threads;
        /////            a_ptr += number_of_threads;
        /////        }
        /////    }
        /////    for (int column = 0; column < k; ++column)
        /////    {
        /////        const float * __restrict__ a_ptr = A(column);
        /////        for (int row = thread_identifier; row < m; row += number_of_threads)
        /////        {
        /////            // Elements below the diagonal with the array TAU represents the orthogonal matrix Q.
        /////            *q_ptr = (row <= column)?0.0f:*a_ptr;
        /////            q_ptr += number_of_threads;
        /////            a_ptr += number_of_threads;
        /////        }
        /////    }
        /////}
        // 3) Use the lapack function 'dorgqr' to generate the M-by-N real matrix Q with orthonormal columns ........................................
        lda = Q.getNumberOfRows();
        work = new float[1];
        lwork = -1;
        SRV_SORGQR(&m, &k, &k, Q(), &lda, tau.getData(), work, &lwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new float[lwork];
        SRV_SORGQR(&m, &k, &k, Q(), &lda, tau.getData(), work, &lwork, &info);
        delete [] work;
    }
    
    void MatrixSVD(Matrix<float> &A, Matrix<float> &U, VectorDense<float> &s, Matrix<float> &V, bool economic)
    {
        int m, n, lda, ldu, ldvt, lwork, info, min;
        float *work;
        int *iwork;
        // jobu values are: A = All values of U and Vt are returned.
        //                  S = the first min(m, n) columns of U and the first min(m, n) rows of Vt are returned in the arrays U and Vt
        //                  N = no columns of U or rows of Vt are returned.
        //                  O = ...
        char jobu;
        
        m = A.getNumberOfRows();
        n = A.getNumberOfColumns();
        min = srvMin(m, n);
        if (economic)
        {
            jobu = 'S';
            U.set(A.getNumberOfRows(), min);
            V.set(min, A.getNumberOfColumns());
        }
        else
        {
            jobu = 'A';
            U.set(A.getNumberOfRows(), A.getNumberOfRows());
            V.set(A.getNumberOfColumns(), A.getNumberOfColumns());
        }
        lda = A.getNumberOfRows();
        ldu = U.getNumberOfRows();
        ldvt = V.getNumberOfRows();
        s.set(min);
        iwork = new int[8 * min];
        work = new float[1];
        lwork = -1;
        SRV_SGESDD(&jobu, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, iwork, &info);
        lwork = (int)work[0];
        delete [] work;
        work = new float[(int)lwork];
        SRV_SGESDD(&jobu, &m, &n, A(), &lda, s.getData(), U(), &ldu, V(), &ldvt, work, &lwork, iwork, &info);
        delete [] work;
        delete [] iwork;
    }
    
    /////// void MatrixRSVD(Matrix<float> &A, Matrix<float> &U, VectorDense<float> &s, Matrix<float> &V, unsigned int number_of_threads)
    /////// {
    ///////     Matrix<float> R, aux;
    ///////     
    ///////     MatrixQRDecomposition(A, R, number_of_threads);
    ///////     MatrixSVD(R, U, s, V);
    ///////     MatrixMultiplication(A, U, aux);
    ///////     U = aux;
    /////// }
    
    
    void MatrixInverse(const Matrix<float> &original, Matrix<float> &inverse)
    {
        int *ipiv, N, info;
        
        if (original.getNumberOfRows() != original.getNumberOfColumns()) throw Exception("Cannot invert a non-square matrix.");
        else N = original.getNumberOfRows();
        
        inverse = original;
        ipiv = new int[N];
        // Turn Y into its LU form, store pivot matrix);
        info = clapack_sgetrf(CblasColMajor, N, N, inverse(), N, ipiv);
        if (info != 0) throw Exception("Error while calculating the inverse of the matrix (Singularity or illegal parameters with BLAS error code %d", info);
        // Use the lapack to calculate the inverse using the previously calculated LU decomposition.
        clapack_sgetri(CblasColMajor, N, inverse(), N, ipiv);
        
        delete [] ipiv;
    }
    
    void MatrixInverse(Matrix<float> &inverse)
    {
        int *ipiv, N, info;
        
        if (inverse.getNumberOfRows() != inverse.getNumberOfColumns()) throw Exception("Cannot invert a non-square matrix.");
        else N = inverse.getNumberOfRows();
        
        ipiv = new int[N];
        // Turn Y into its LU form, store pivot matrix);
        info = clapack_sgetrf(CblasColMajor, N, N, inverse(), N, ipiv);
        if (info != 0) throw Exception("Error while calculating the inverse of the matrix (Singularity or illegal parameters with BLAS error code %d", info);
        // Use the lapack to calculate the inverse using the previously calculated LU decomposition.
        clapack_sgetri(CblasColMajor, N, inverse(), N, ipiv);
        
        delete [] ipiv;
    }
    
    void MatrixSolve(const Matrix<float> &A, const Matrix<float> &B, Matrix<float> &solution)
    {
        int n, nrhs, lda, ldb, *ipiv, info;
        Matrix<float> matrix_a(A), matrix_b(B);
        
        n = matrix_a.getNumberOfRows();
        nrhs = matrix_b.getNumberOfColumns();
        lda = srvMax<int>(1, matrix_a.getNumberOfRows());
        ldb = srvMax<int>(1, matrix_b.getNumberOfRows());
        ipiv = new int[n];
        
        SRV_SGESV(&n, &nrhs, matrix_a(), &lda, ipiv, matrix_b(), &ldb, &info);
        if (info > 0) throw ExceptionSingularity("U(%d, %d) is exactly zero. The factorization has been completed, but the factor U is exactly singular, so the solution could not be computed.", info, info);
        else if (info < 0) throw ExceptionArgument("The %d-th argument has an illegal value.", info);
        
        solution = matrix_b;
        delete [] ipiv;
    }
    
    void MatrixSolve(Matrix<float> &A, Matrix<float> &B, VectorDense<int> &IPIV)
    {
        int n, nrhs, lda, ldb, info;
        
        n = A.getNumberOfRows();
        nrhs = B.getNumberOfColumns();
        lda = srvMax<int>(1, A.getNumberOfRows());
        ldb = srvMax<int>(1, B.getNumberOfRows());
        IPIV.set(n);
        
        SRV_SGESV(&n, &nrhs, A(), &lda, IPIV.getData(), B(), &ldb, &info);
        if (info > 0) throw ExceptionSingularity("U(%d, %d) is exactly zero. The factorization has been completed, but the factor U is exactly singular, so the solution could not be computed.", info, info);
        else if (info < 0) throw ExceptionArgument("The %d-th argument has an illegal value.", info);
    }
    
    void MatrixEigenSymmetric(const Matrix<float> &A, Matrix<float> &V, VectorDense<float> &D)
    {
        char jobz, uplo;
        int lda, lwork, info, n;
        VectorDense<float> work(1);
        
        V = A;
        jobz = 'V';
        uplo = 'U'; // Dunno U -> the leading N-by-N upper triangular part of A contains the upper triangular part of the matrix A. If UPLO = 'L', the leading N-by-N lower triangular part of A contains the lower triangular part of the matrix A.
        n = V.getNumberOfRows();
        lda = srvMax<int>(1, V.getNumberOfRows());
        lwork = -1;
        D.set(A.getNumberOfRows());
        SRV_SSYEV(&jobz, &uplo, &n, V(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
        lwork = (int)work[0];
        work.set(srvMax<int>(1, lwork));
        SRV_SSYEV(&jobz, &uplo, &n, V(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
    }
    
    void MatrixEigenSymmetric(const Matrix<float> &A, VectorDense<float> &D)
    {
        char jobz, uplo;
        int lda, lwork, info, n;
        VectorDense<float> work(1);
        Matrix<float> matrix_a(A);
        
        jobz = 'N';
        uplo = 'U'; // Dunno U -> the leading N-by-N upper triangular part of A contains the upper triangular part of the matrix A. If UPLO = 'L', the leading N-by-N lower triangular part of A contains the lower triangular part of the matrix A.
        n = matrix_a.getNumberOfRows();
        lda = srvMax<int>(1, matrix_a.getNumberOfRows());
        lwork = -1;
        D.set(A.getNumberOfRows());
        SRV_SSYEV(&jobz, &uplo, &n, matrix_a(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
        lwork = (int)work[0];
        work.set(srvMax<int>(1, lwork));
        SRV_SSYEV(&jobz, &uplo, &n, matrix_a(), &lda, D.getData(), work.getData(), &lwork, &info);
        if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DSYEV': Incorrect %d-th argument.", -info);
        if (info > 0) throw ExceptionSingularity("In MatrixEigen, the algorithm failed to converge; %d off-diagonal elements of an intermediate tridiagonal form did not converge to zero.", info);
    }
    
    void MatrixEigenNonSymmetric(const Matrix<float> &A, bool no_balance, Matrix<float> &V, VectorDense<float> &D)
    {
        if (no_balance)
        {
            int n, ilo, ihi, lwork, info, ldz, lda, ldvl, ldvr;
            char job, compz, side, howmny;
            Matrix<float> matrix_a(A);
            VectorDense<float> tau(A.getNumberOfRows() - 1), work(1), wi(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            ldz = ldvr = lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            ldvl = 1;
            ilo = 1;
            ihi = n;
            
            // DGEHRD reduces a real matrix A to upper Hessenberg form H by an orthogonal similarity transform: Q**T * A * Q = H
            lwork = -1;
            SRV_SGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_SGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            
            V = matrix_a; // Save a copy of H in V.
            // DORGHR generates a real orthogonal matrix Q which is defined as the product of IHI-ILO elementary reflectors
            // of order N, as returned by DGEHRD.
            lwork = -1;
            work.set(1);
            SRV_SORGHR(&n, &ilo, &ihi, V(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DORGHR': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_SORGHR(&n, &ilo, &ihi, V(), &lda, tau.getData(), work.getData(), &lwork, &info);
            
            
            // DHSEQR computes the eigenvalues of a Hessenberg matrix H and, optionally, the matrices T and Z from the
            // Schur decomposition H = Z T Z**T, where T is an upper quasi-triangular matrix (the Schur form), and
            // Z is the orthogonal matrix of Schur vectors.
            //
            // Optionally Z may be postmultiplied into an input orthogonal matrix Q so that this routine can give
            // the Schur factorization of a matrix A which has been reduced to the Hessenberg form H by the
            // orthogonal matrix Q: A = Q * H * Q ** T = (QZ) * T * (QZ) ** T
            job = 'S'; // Compute eigenvalues and the Schur form T.
            compz = 'V'; // Z must contain an orthogonal matrix Q on entry, and the product Q * Z is returned.
            D.set(matrix_a.getNumberOfColumns());
            work.set(1);
            lwork = -1;
            SRV_SHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), V(), &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_SHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), V(), &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            
            // DTREVC computes some or all of the right and/or left eigenvectors of a real upper quasi-triangular matrix T.
            // Matrices of this type are produced by the Schur factorization of a real general matrix: A = Q*T*Q**T,
            // as computed by DHSEQR.
            //
            // The right eigenvector and the left eigenvector y of T corresponding to an eigenvalue w are defined by:
            //
            // T * w = w * x, (y ** T) * T = w * (y ** T)
            // 
            // where y ** T denotes the transpose of y.
            // The eigenvalues are not input to this routine, but are read directly from the diagonal blocks of T.
            //
            // This routine returns the matrices X and/or Y of right and left eigenvectors of T, or the product
            // Q * X and/or Q * Y, where Q is an input matrix. If Q is the orthonormal factor that reduces a matrix
            // A to Schur form T, then Q * X and Q * Y are the matrices of right and left eigenvectors of A.
            side = 'R'; // Compute right eigenvectors only.
            howmny = 'B'; // Compute all right and/or left eigenvectors, backtransformed by the matrices in VR and/or VL.
            work.set(3 * n);
            SRV_STREVC(&side, &howmny, 0, &n, matrix_a(), &lda, 0, &ldvl, V(), &ldvr, &n, &n, work.getData(), &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DTREVC': Incorrect %d-th argument.", -info);
            // TODO: Workaround: The eigenvectors of the negative conjugate eigenvalues are replaced by their
            // positive counterpart. See if there is any function in LAPACK which does the same thing or if
            // there is a more elegant solution.
            for (unsigned int i = 0; i < wi.size(); ++i)
            {
                if (wi[i] < 0)
                    V.copyColumn(i, V(i - 1));
            }
        }
        else
        {
            char jobvl, jobvr;
            int n, lda, ldvl, ldvr, info, lwork;
            Matrix<float> matrix_a(A);
            VectorDense<float> wi(A.getNumberOfRows()), work(1), scale(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            jobvl = 'N'; // Left eigenvectors of A are not computed.
            jobvr = 'V'; // Right eigenvectors of A are not computed.
            D.set(matrix_a.getNumberOfRows());
            V.set(matrix_a.getNumberOfRows(), matrix_a.getNumberOfRows());
            ldvl = 1; // Leading dimension of the left eigenvectors matrix.
            ldvr = srvMax<int>(1, matrix_a.getNumberOfRows()); // Leading dimension of the right eigenvectors matrix.
            lwork = -1;
            SRV_SGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, V(), &ldvr, work.getData(), &lwork, &info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            SRV_SGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, V(), &ldvr, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            // TODO: Workaround: The eigenvectors of the negative conjugate eigenvalues are replaced by their
            // positive counterpart. See if there is any function in LAPACK which does the same thing or if
            // there is a more elegant solution.
            for (unsigned int i = 0; i < wi.size(); ++i)
                if (wi[i] < 0)
                    V.copyColumn(i, V(i - 1));
        }
    }
    
    void MatrixEigenNonSymmetric(const Matrix<float> &A, bool no_balance, VectorDense<float> &D)
    {
        if (no_balance)
        {
            int n, ilo, ihi, lwork, info, ldz, lda;
            char job, compz;
            Matrix<float> matrix_a(A);
            VectorDense<float> tau(A.getNumberOfRows() - 1), work(1), wi(A.getNumberOfRows());
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            ilo = 1;
            ihi = n;
            lwork = -1;
            
            // DGEHRD reduces a real matrix A to upper Hessenberg form H by an orthogonal similarity transform: Q**T * A * Q = H
            SRV_SGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_SGEHRD(&n, &ilo, &ihi, matrix_a(), &lda, tau.getData(), work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEHRD': Incorrect %d-th argument.", -info);
            
            // DHSEQR computes the eigenvalues of a Hessenberg matrix H and, optionally, the matrices T and Z from the
            // Schur decomposition H = Z T Z**T, where T is an upper quasi-triangular matrix (the Schur form), and
            // Z is the orthogonal matrix of Schur vectors.
            //
            // Optionally Z may be postmultiplied into an input orthogonal matrix Q so that this routine can give
            // the Schur factorization of a matrix A which has been reduced to the Hessenberg form H by the
            // orthogonal matrix Q: A = Q * H * Q ** T = (QZ) * T * (QZ) ** T
            job = 'E'; // Compute eigenvalues only.
            compz = 'N'; // no Schur vectors are computed.
            D.set(matrix_a.getNumberOfColumns());
            ldz = 1;
            work.set(1);
            lwork = -1;
            SRV_SHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            SRV_SHSEQR(&job, &compz, &n, &ilo, &ihi, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldz, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DHSEQR': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, DHSEQR failed to compute all of the eigenvalues. Elements 1:%d-1 and %d+1:n of D contain those eigenvalues which have been successfully computed (failures are rare).", ilo, info);
        }
        else
        {
            char jobvl, jobvr;
            int n, lda, ldvl, ldvr, info, lwork;
            Matrix<float> matrix_a(A);
            VectorDense<float> wi(A.getNumberOfRows()), work(1);
            
            n = matrix_a.getNumberOfRows();
            lda = srvMax<int>(1, matrix_a.getNumberOfRows());
            jobvl = 'N'; // Left eigenvectors of A are not computed.
            jobvr = 'N'; // Right eigenvectors of A are not computed.
            D.set(matrix_a.getNumberOfRows());
            ldvl = 1; // Leading dimension of the left eigenvectors matrix.
            ldvr = 1; // Leading dimension of the right eigenvectors matrix.
            lwork = -1;
            SRV_SGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, 0, &ldvr, work.getData(), &lwork, &info);
            lwork = (int)work[0];
            work.set(srvMax<int>(1, lwork));
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
            SRV_SGEEV(&jobvl, &jobvr, &n, matrix_a(), &lda, D.getData(), wi.getData(), 0, &ldvl, 0, &ldvr, work.getData(), &lwork, &info);
            if (info < 0) throw ExceptionArgument("In MatrixEigen, incorrect arguments passed to function 'SRV_DGEEV': Incorrect %d-th argument.", -info);
            if (info > 0) throw ExceptionSingularity("In MatrixEigen, the QR algorithm failed to compute all the eigenvalues, and no eigenvectors have been computed; the elements %d+1:N of WR and WI contain eigenvalues which have converged.", info);
        }
    }
    
    void MatrixLogarithm(const Matrix<float> &A, Matrix<float> &B, unsigned int number_of_threads)
    {
        // Check matrix dimensionality ..............................................................................................................
        if (A.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("Matrix logarithm can only be calculated from square images.");
        // Variables ................................................................................................................................
        const unsigned int size = A.getNumberOfRows();
        VectorDense<float> D;
        Matrix<float> V, V2(size, size);
        int ld = srvMax(1, (int)size);
        
        MatrixEigen(A, true, V, D);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
            {
                if (D[i] < 0.0f)
                {
                    cblas_sscal(size,            -1.0f,  V(i), 1);
                    cblas_scopy(size, V(i), 1, V2(i), 1);
                    cblas_sscal(size, -std::log(-D[i]), V2(i), 1);
                }
                else
                {
                    cblas_scopy(size, V(i), 1, V2(i), 1);
                    cblas_sscal(size,  std::log( D[i]), V2(i), 1);
                }
            }
        }
        B.set(size, size);
        cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, size, size, size, 1.0f, V2(), ld, V(), ld, 0.0f, B(), ld);
    }
    
    void MatrixExponential(const Matrix<float> &A, Matrix<float> &B, unsigned int number_of_threads)
    {
        // Check matrix dimensionality ..............................................................................................................
        if (A.getNumberOfRows() != A.getNumberOfColumns()) throw Exception("Matrix exponential can only be calculated from square images.");
        // Variables ................................................................................................................................
        const unsigned int size = A.getNumberOfRows();
        VectorDense<float> D;
        Matrix<float> V, V2(size, size);
        int ld = srvMax(1, (int)size);
        
        MatrixEigen(A, true, V, D);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
            {
                if (D[i] < 0.0f)
                {
                    cblas_sscal(size,            -1.0f,  V(i), 1);
                    cblas_scopy(size, V(i), 1, V2(i), 1);
                    cblas_sscal(size, -std::exp(-D[i]), V2(i), 1);
                }
                else
                {
                    cblas_scopy(size, V(i), 1, V2(i), 1);
                    cblas_sscal(size,  std::exp( D[i]), V2(i), 1);
                }
            }
        }
        B.set(size, size);
        cblas_sgemm(CblasColMajor, CblasNoTrans, CblasTrans, size, size, size, 1.0f, V2(), ld, V(), ld, 0.0f, B(), ld);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

