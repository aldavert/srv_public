// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_math.hpp"
#include <algorithm>
#include <iostream>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | MATH FUNCTIONS                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    double polynomyEval(double x, const double * coefficients, unsigned int size)
    {
        double y;
        
        y = coefficients[0];
        for (unsigned int i = 1; i < size; ++i)
            y = y * x + coefficients[i];
        
        return y;
    }
    
    double psiFunction(double x)
    {
        static double A[] = { 8.33333333333333333333e-2, -2.10927960927960927961e-2, 7.57575757575757575758e-3, -4.16666666666666666667e-3, 3.96825396825396825397e-3, -8.33333333333333333333e-3, 8.33333333333333333333e-2 };
        double p, q, nz, s, w, y, z;
        bool negative;
        
        negative = false;
        nz = 0.0;
        if (x <= 0.0)                               // Negative values.
        {
            negative = true;
            q = x;
            p = floor(q);
            if (p == q) return c_infinite;
            // Remove the zeros of tan(c_pi x) by subtracting the nearest integer from x.
            nz = q - p;
            if (nz != 0.5)
            {
                if (nz > 0.5)
                {
                    p += 1.0;
                    nz = q - p;
                }
                nz = c_pi / tan(c_pi * nz);
            }
            else nz = 0.0;
            x = 1.0 - x;
        }
        
        if ((x <= 10.0) && (x == floor(x)))         // Check for positive integer up to 10.
        {
            const unsigned int n = (int)x;
            y = 0.0;
            for (unsigned int i = 1; i < n; ++i)
            {
                w = i;
                y += 1.0 / w;
            }
            y -= c_euler;
        }
        else
        {
            s = x;
            w = 0.0;
            while (s < 10.0)
            {
                w += 1.0 / s;
                s += 1.0;
            }
            if (s < 1.0e17)
            {
                z = 1.0 / (s * s);
                y = z * polynomyEval(z, A, 6);
            }
            else y = 0.0;
            y = log(s) - (0.5 / s) - y - w;
        }
        
        return (negative)?(y - nz):y;
    }
    
    double erfinv(double x)
    {
        const double erfinv_a3 = -0.140543331;
        const double erfinv_a2 = 0.914624893;
        const double erfinv_a1 = -1.645349621;
        const double erfinv_a0 = 0.886226899;
        const double erfinv_b4 = 0.012229801;
        const double erfinv_b3 = -0.329097515;
        const double erfinv_b2 = 1.442710462;
        const double erfinv_b1 = -2.118377725;
        const double erfinv_b0 = 1;
        const double erfinv_c3 = 1.641345311;
        const double erfinv_c2 = 3.429567803;
        const double erfinv_c1 = -1.62490649;
        const double erfinv_c0 = -1.970840454;
        const double erfinv_d2 = 1.637067800;
        const double erfinv_d1 = 3.543889200;
        const double erfinv_d0 = 1;
        double x2, r, y;
        int  sign_x;
        
        if ((x < -1) || (x > 1)) return NAN;
        else if (x == 0) return 0;
        else if (x > 0) sign_x = 1;
        else { sign_x = -1; x = -x; }
        
        if (x <= 0.7)
        {
            x2 = x * x;
            r = x * (((erfinv_a3 * x2 + erfinv_a2) * x2 + erfinv_a1) * x2 + erfinv_a0);
            r /= (((erfinv_b4 * x2 + erfinv_b3) * x2 + erfinv_b2) * x2 + erfinv_b1) * x2 + erfinv_b0;
        }
        else
        {
            y = std::sqrt(-std::log((1 - x) / 2));
            r = (((erfinv_c3 * y + erfinv_c2) * y + erfinv_c1) * y + erfinv_c0);
            r /= ((erfinv_d2 * y + erfinv_d1) * y + erfinv_d0);
        }
        r = r * sign_x;
        x = x * sign_x;
        r -= (std::erf(r) - x) / (2.0 / std::sqrt(srv::c_pi) * std::exp(-r * r));
        r -= (std::erf(r) - x) / (2.0 / std::sqrt(srv::c_pi) * std::exp(-r * r));
        
        return r;
    }
    
    double norminv(double p, double mu, double sigma)
    {
        return mu + sigma * -srv::c_sqrt2 * erfinv(1 - 2 * p);
    }
    
    double stirlerr(double n)
    {
        const double sfe[16] = {
            0                            , 0.081061466795327258219670264 ,
            0.041340695955409294093822081, 0.0276779256849983391487892927,
            0.020790672103765093111522771, 0.0166446911898211921631948653,
            0.013876128823070747998745727, 0.0118967099458917700950557241,
            0.010411265261972096497478567, 0.0092554621827127329177286366,
            0.008330563433362871256469318, 0.0075736754879518407949720242,
            0.006942840107209529865664152, 0.0064089941880042070684396310,
            0.005951370112758847735624416, 0.0055547335519628013710386899
        };
        const double S0 = 0.083333333333333333333       ; // 1 / 12
        const double S1 = 0.00277777777777777777778     ; // 1 / 360
        const double S2 = 0.00079365079365079365079365  ; // 1 / 1260
        const double S3 = 0.000595238095238095238095238 ; // 1 / 1680
        const double S4 = 0.0008417508417508417508417508; // 1 / 1188
        double nn;
        
        if (n < 15) return(sfe[(int)n]);
        nn = n * n;
        if      (n > 500) return ((S0 -  S1 / nn) / n);
        else if (n > 80 ) return ((S0 - (S1 -  S2 / nn) / nn) / n);
        else if (n > 35 ) return ((S0 - (S1 - (S2 -  S3 / nn) / nn) / nn) / n);
        else              return ((S0 - (S1 - (S2 - (S3 - S4/nn)/nn)/nn)/nn)/n);
    }
    
    double bd0(double x, double np)
    {
        if (std::fabs(x - np) < 0.1 * (x + np))
        {
            double ej, s, s1, v;
            
            s  = (x - np) * (x - np) / (x + np);
            v  = (x - np) / (x + np);
            ej = 2 * x * v;
            for (int j = 1;; ++j)
            {
                ej *= v * v;
                s1 = s + ej / (2 * j + 1);
                if (s1 == s) return s1;
                s = s1;
            }
        }
        else return x * std::log(x / np) + np - x;
    }
    
    double binomialDistribution(unsigned int k, unsigned int N, double p)
    {
        const double PI2 = 6.283185307179586476925286;
        if ((k <= N) && (p >= 0.0) && (p <= 1.0))
        {
            double rk, rN;
            
            rk = (double)k;
            rN = (double)N;
            
            if      (p == 0.0) return 0.0;                              // Borderline cases.
            else if (p == 1.0) return (double)(k == N);
            else if (k ==   0) return std::exp(rN * std::log(1 - p));
            else if (k ==   N) return std::exp(rN * std::log(    p));
            else if (N < 10)                                            // Faster method that is not accurate for large N
            {
                double nk, lny;
                nk = std::lgamma(rN + 1.0) - std::lgamma(rk + 1.0) - std::lgamma(rN - rk + 1.0);
                lny = nk + rk * std::log(p) + (rN - rk) * std::log1p(-p);
                return std::exp(lny);
            }
            else                                                        // Slower method implemented in "C. Loader, 'Fast and Accurate Calculations of Binomial Probabilities', July 9, 2000."
            {
                double lc;
                
                lc = stirlerr(rN) - stirlerr(rk) - stirlerr(rN - rk) - bd0(rk, rN * p) - bd0(rN - rk, rN * (1.0 - p));
                return std::exp(lc) * std::sqrt(rN / (PI2 * rk * (rN - rk)));
            }
        }
        else return 0;
    }
    
    double poissonDistribution(unsigned int x, double lb)
    {
        const double PI2 = 6.283185307179586476925286;
        double rx = (double)x;
        
        if (lb == 0) return ((x == 0)?1.0:0.0);
        if ( x == 0) return std::exp(-lb);
        return std::exp(-stirlerr(rx) - bd0(rx, lb)) / std::sqrt(PI2 * x);
    }
    
    double binomialCDF(unsigned int k, unsigned int N, double p)
    {
        if      (k >=   N) return 1.0;
        else if (p == 0.0) return 1.0;
        else if (p == 1.0) return (double)(k >= N);
        else if ((p < 0.0) || (p > 1.0)) return std::nan("0");
        else
        {
            const double rN = (double)N;
            const double rk = (double)k;
            double np, y;
            
            np = rN * p;
            if ((rN < 1e5) && ((rk <= np) || (p < 1e-4)))
            {
                y = 0.0;
                for (unsigned int i = 0; i <= k; ++i)
                    y += binomialDistribution(i, N, p);
            }
            else if (rN < 1e5)
            {
                y = 1.0;
                for (unsigned int i = 0; i <= N - k - 1; ++i)
                    y -= binomialDistribution(i, N, 1.0 - p);
            }
            else
            {
                const double eps0 = std::nextafter(0.0, 0.0 + std::numeric_limits<double>::epsilon()) - 0.0;
                const double eps1 = std::nextafter(1.0, 1.0 + std::numeric_limits<double>::epsilon()) - 1.0;
                double s, t1, t2, a, b;
                
                s = std::sqrt(np * (1.0 - p));
                t1 = 40.0;
                t2 = 10.0;
                while (binomialDistribution((unsigned int)std::floor(np - t1 * s), N, p) > eps0)
                    t1 *= 1.5;
                while (binomialDistribution((unsigned int)std::ceil(np + t2 * s), N, p) > eps1)
                    t2 *= 1.5;
                
                a = std::max(0.0, std::floor(np - t1 * s));
                b = std::ceil(np + t2 * s);
                if      (rk < a) y = 0.0;
                else if (rk > b) y = 1.0;
                else
                {
                    y = 0.0;
                    for (unsigned int i = (unsigned int)a; i <= k; ++i)
                        y += binomialDistribution(i, N, p);
                }
            }
            return std::min(1.0, y);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

