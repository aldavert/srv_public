// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLASSIFIER_MAIN_HPP_HEADER_FILE__
#define __SRV_CLASSIFIER_MAIN_HPP_HEADER_FILE__

// -[ Generic header files ]-------------------------------------------------------------------------------------------------------------------------
#include <iostream>
#include "srv_vector.hpp"
#include "srv_file_compression.hpp"
// -[ Headers which include all the classifier classes ]---------------------------------------------------------------------------------------------
#include "classifier/srv_classifier_definitions.hpp"
#include "classifier/srv_classifier_validation.hpp"
#include "classifier/srv_base_classifier.hpp"
#include "classifier/srv_base_svm_classifier.hpp"
#include "classifier/srv_online_classifier.hpp"
#include "classifier/srv_perceptron.hpp"
#include "classifier/srv_multiple_perceptron.hpp"
#include "classifier/srv_pegasos.hpp"
#include "classifier/srv_sgd_first_order.hpp"
#include "classifier/srv_sgd_quasi_newton.hpp"
#include "classifier/srv_l1_regularized_l2_loss_svm.hpp"
#include "classifier/srv_l1_regularized_logistic_regression_svm.hpp"
#include "classifier/srv_l2_regularized_l1_loss_svm_dual.hpp"
#include "classifier/srv_l2_regularized_l2_loss_svm_dual.hpp"
#include "classifier/srv_l2_regularized_l2_loss_svm_primal.hpp"
#include "classifier/srv_l2_regularized_logistic_regression_svm_dual.hpp"
#include "classifier/srv_l2_regularized_logistic_regression_svm_primal.hpp"
#include "classifier/srv_multiclass_svm.hpp"
#include "classifier/srv_probabilistic_score.hpp"
#include "classifier/srv_probabilistic_rescale.hpp"
#include "classifier/srv_probabilistic_sigmoid.hpp"
#include "classifier/srv_probabilistic_asymmetric_laplace.hpp"
#include "classifier/srv_probabilistic_asymmetric_gaussian.hpp"
#include "classifier/srv_probabilistic_binning.hpp"
#include "classifier/srv_probabilistic_isotonic_regression.hpp"
#include "classifier/srv_probabilistic_lut.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | MACROS TO INITIALIZE THE FACTORY     |
    //                   | CLASSES                              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##

#define ADD_LINEAR_LEARNER_FACTORY(CLASS_NAME, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_FACTORY(TEMPLATE_5P(srv::LinearLearnerFactory, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL), TEMPLATE_5P(CLASS_NAME, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL))

#define INITIALIZE_ONLINE_LEARNER_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::MultiplePerceptron, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::Pegasos, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::Perceptron, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::SgdFirstOrder, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::SgdQuasiNewton, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)

#define INITIALIZE_LINEAR_SVM_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L1RegularizedL2LossSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L1RegularizedLogisticRegressionSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L2RegularizedL1LossDualSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L2RegularizedL2LossDualSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L2RegularizedL2LossPrimalSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L2RegularizedLogisticRegressionDualSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::L2RegularizedLogisticRegressionPrimalSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    ADD_LINEAR_LEARNER_FACTORY(srv::MulticlassSVM, TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)

#define INITIALIZE_LINEAR_LEARNER_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    INITIALIZE_ONLINE_LEARNER_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL) \
    INITIALIZE_LINEAR_SVM_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)

#define ADD_PROBABILISTIC_SCORE_FACTORY(CLASS_NAME, TSCORE, TPROBABILITY) \
    ADD_FACTORY(TEMPLATE_2P(srv::ProbabilisticScoreFactory, TSCORE, TPROBABILITY), TEMPLATE_2P(CLASS_NAME, TSCORE, TPROBABILITY))

#define INITIALIZE_PROBABILISTIC_SCORE_FACTORIES(TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticRescale, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticSigmoid, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticAsymmetricLaplace, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticAsymmetricGaussian, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticBinning, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticIsotonicRegression, TSCORE, TPROBABILITY) \
    ADD_PROBABILISTIC_SCORE_FACTORY(srv::ProbabilisticLUT, TSCORE, TPROBABILITY)
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALCULATE THE PARTITIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER>
    void calculatePartitions(TCLASSIFIER minimum_value, TCLASSIFIER maximum_value, unsigned int number_of_partitions, PARTITION_SCALE scale, VectorDense<TCLASSIFIER> &partitions)
    {
        partitions.set(number_of_partitions);
        partitions[0] = minimum_value;
        if      (scale == PARTITION_UNIFORM)
        {
            const double step = ((double)maximum_value - (double)minimum_value);
            for (unsigned int i = 1; i < number_of_partitions; ++i)
                partitions[i] = (TCLASSIFIER)((double)minimum_value + step * (double)i / (double)(number_of_partitions - 1));
        }
        else if (scale == PARTITION_LOG2)
        {
            const double minimum_log2 = std::log(minimum_value) / std::log(2);
            const double maximum_log2 = std::log(maximum_value) / std::log(2);
            const double step = maximum_log2 - minimum_log2;
            for (unsigned int i = 1; i < number_of_partitions; ++i)
                partitions[i] = (TCLASSIFIER)(std::pow(2.0, (double)minimum_log2 + step * (double)i / (double)(number_of_partitions - 1)));
        }
        else if (scale == PARTITION_LOG10)
        {
            const double minimum_log10 = std::log10(minimum_value);
            const double maximum_log10 = std::log10(maximum_value);
            const double step = maximum_log10 - minimum_log10;
            for (unsigned int i = 1; i < number_of_partitions; ++i)
                partitions[i] = (TCLASSIFIER)(std::pow(10.0, (double)minimum_log10 + step * (double)i / (double)(number_of_partitions - 1)));
        }
        else throw Exception("The selection scale with index '%d' is not implemented yet.", (int)scale);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SCORES INFORMATION CLASS DECLARATION |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class ScoresInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]----------------------------------------------------------------------------------------
        /// Default constructor.
        ScoresInformation(void);
        /// Constructor which load the scores information from a file.
        ScoresInformation(const char * filename);
        /** Constructor which initializes all score information structures.
         *  \param[in] scores array of vectors with the scores of each sample.
         *  \param[in] labels array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples in the scores array.
         *  \param[in] number_of_categories number of categories of the classifier.
         *  \param[in] number_of_dimensions number of dimensions of the original classifier.
         */
        ScoresInformation(const VectorDense<double> * scores, const VectorDense<unsigned int> * labels, unsigned int number_of_samples, unsigned int number_of_categories, unsigned int number_of_dimensions);
        /// Copy constructor.
        ScoresInformation(const ScoresInformation &other);
        /// Destructor.
        ~ScoresInformation(void);
        /// Assignation operator.
        ScoresInformation& operator=(const ScoresInformation &other);
        
        // -[ Access functions ]-------------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array of vectors with the scores returned by the classifiers for each sample.
        inline const VectorDense<double> * getScores(void) const { return m_scores; }
        /// Returns a pointer to the array of vectors with the scores returned by the classifiers for each sample.
        inline VectorDense<double> * getScores(void) { return m_scores; }
        /// Returns a constant pointer to the array of the labels associated to each sample.
        inline const VectorDense<unsigned int> * getLabels(void) const { return m_labels; }
        /// Returns a pointer to the array of the labels associated to each sample.
        inline VectorDense<unsigned int> * getLabels(void) { return m_labels; }
        /// Returns the number of samples of the scores information structure.
        inline unsigned int getNumberOfSamples(void) const { return m_number_of_samples; }
        /// Returns the number of categories.
        inline unsigned int getNumberOfCategories(void) const { return m_number_of_categories; }
        /// Returns the number of dimensions of the categorized vectors.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /** Sets the information stored in the score information structure.
         *  \param[in] scores array of vectors with the scores of each sample.
         *  \param[in] labels array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples in the scores array.
         *  \param[in] number_of_categories number of categories of the classifier.
         *  \param[in] number_of_dimensions number of dimensions of the original classifier.
         */
        void set(const VectorDense<double> * scores, const VectorDense<unsigned int> * labels, unsigned int number_of_samples, unsigned int number_of_categories, unsigned int number_of_dimensions);
        /// Loads the scores information from a file.
        void load(const char * filename);
        
    private:
        /// Array of vectors with the scores returned by the classifiers for each sample.
        VectorDense<double> * m_scores;
        /// Array of the labels associated to each sample.
        VectorDense<unsigned int> * m_labels;
        /// Number of samples, i.e. number of elements in the scores and labels pointer arrays.
        unsigned int m_number_of_samples;
        /// Number of categories.
        unsigned int m_number_of_categories;
        /// Number of dimensions of the categorized vectors.
        unsigned int m_number_of_dimensions;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY LINEAR LEARNER FUNCTIONS   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Loads the linear classifier object from an XML file.
     *  \param[in] filename file where the linear classifier object is loaded.
     *  \param[out] object linear classifier object.
     *  \note this function does not deallocates the memory pointed by the object pointer.
     */
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void loadObject(const char * filename, LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object = LinearLearnerFactory<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Type::getInstantiator((LINEAR_LEARNER_IDENTIFIER)((int)parser.getAttribute("Identifier")));
        object->convertFromXML(parser);
        delete file;
    }
    
    /** Saves the linear classifier object into an XML file.
     *  \param[in] filename file where the linear classifier object is stored.
     *  \param[in] object linear classifier object.
     */
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void saveObject(const char * filename, const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    /** Shows the information of a loss function object.
     *  \param[out] out standard library output stream.
     *  \param[in] object loss function object.
     *  \returns the modified output stream.
     */
    template <class T>
    std::ostream& operator<<(std::ostream &out, const LossBase<T> * loss_object)
    {
        switch (loss_object->getIdentifier())
        {
        case LOGISTIC_LOSS:
            out << "Logistic loss";
            break;
        case MARGIN_LOGISTIC_LOSS:
            out << "Margin logistic loss";
            break;
        case SMOOTH_HINGE_LOSS:
            out << "Smooth hinge loss";
            break;
        case SQUARE_HINGE_LOSS:
            out << "Square hinge loss";
            break;
        case HINGE_LOSS:
            out << "Hinge loss";
            break;
        default:
            out << "UNKNOWN";
            break;
        }
        return out;
    }
    
    /** Shows the information of a linear learner object.
     *  \param[out] out standard library output stream.
     *  \param[in] object linear learner object.
     *  \returns the input standard library output stream.
     */
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    std::ostream& operator<<(std::ostream &out, const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object)
    {
        const LINEAR_LEARNER_IDENTIFIER identifier = object->getIdentifier();
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Linear learner ]" << std::endl;
        tab.increase(out);
        out << tab << " · Optimization algorithm: ";
        switch (identifier)
        {
        case SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_PRIMAL:
            out << "L2-Regularized Linear SVM with a Logistic Regression loss (primal)";
            break;
        case SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_DUAL:
            out << "L2-Regularized Linear SVM with a Logistic Regression loss (dual)";
            break;
        case SVM_LOGISTIC_REGRESSION_L1_REGULARIZED:
            out << "L1-Regularized Linear SVM";
            break;
        case SVM_MULTICLASS:
            out << "Multiclass Linear SVM (Crammer and Singer)";
            break;
        case SVM_L2_REGULARIZED_L2_LOSS_DUAL:
            out << "L2-Regularized Linear SVM with a L2-loss (dual)";
            break;
        case SVM_L2_REGULARIZED_L2_LOSS_PRIMAL:
            out << "L2-Regularized Linear SVM with a L2-loss (primal)";
            break;
        case SVM_L2_REGULARIZED_L1_LOSS_SVM_DUAL:
            out << "L2-Regularized Linear SVM with a L1-loss (dual)";
            break;
        case SVM_L1_REGULARIZED_L2_LOSS:
            out << "L1-Regularized Linear SVM with a L2-loss";
            break;
        case ONLINE_PEGASOS_ID:
            out << "Online Pegasos algorithm";
            break;
        case ONLINE_FIRST_ORDER_SGD_ID:
            out << "Online first-order stochastic gradient descent algorithm";
            break;
        case ONLINE_QN_SGD_ID:
            out << "Online quasi-newton stochastic gradient descent algorithm";
            break;
        case ONLINE_PERCEPTRON_ID:
            out << "Online Perceptron algorithm";
            break;
        case ONLINE_MULTIPLE_PERCEPTRON_ID:
            out << "Online multiple Perceptron algorithm";
            break;
        default:
            out << "UNKNOWN";
        }
        out << std::endl;
        out << tab << " · Number of dimensions: " << object->getNumberOfDimensions() << std::endl;
        out << tab << " · Number of categories: " << object->getNumberOfCategories() << std::endl;
        out << tab << "[ Categories classifier information ]" << std::endl;
        tab.increase(out);
        for (unsigned int i = 0; i < object->getNumberOfCategories(); ++i)
        {
            out << tab << " · Category label: " << (int)object->getCategoryIdentifier(i) << std::endl;
            tab.increase(out);
            out << tab << " · Regularization factor: " << object->getParameter(i).getRegularizationFactor() << std::endl;
            out << tab << " · Balancing factor: " << object->getParameter(i).getBalancingFactor() << std::endl;
            out << tab << " · Positive weight: " << object->getParameter(i).getPositiveWeight() << std::endl;
            out << tab << " · Negative weight: " << object->getParameter(i).getNegativeWeight() << std::endl;
            tab.decrease(out);
        }
        tab.decrease(out);
        out << tab << " · Positive label probability: " << (double)object->getPositiveProbability() << std::endl;
        out << tab << " · Difficult label probability: " << (double)object->getDifficultProbability() << std::endl;
        out << tab << " · Ignore difficult samples: " << (object->getIgnoreDifficult()?("[ENABLED]"):("[DISABLED]")) << std::endl;
        
        switch (identifier)
        {
            case SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_PRIMAL:
            case SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_DUAL:
            case SVM_MULTICLASS:
            case SVM_L2_REGULARIZED_L2_LOSS_DUAL:
            case SVM_L2_REGULARIZED_L2_LOSS_PRIMAL:
            case SVM_L2_REGULARIZED_L1_LOSS_SVM_DUAL:
            {
                LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                out << tab << " · Epsilon: " << object_ptr->getEpsilon() << std::endl;
                out << tab << " · Maximum number of iterations: " << object_ptr->getMaximumNumberOfIterations() << std::endl;
                break;
            }
            case SVM_LOGISTIC_REGRESSION_L1_REGULARIZED:
            {
                L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                out << tab << " · Epsilon: " << object_ptr->getEpsilon() << std::endl;
                out << tab << " · Transpose: " << (object_ptr->getTransposeData()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                out << tab << " · Maximum number of iterations: " << object_ptr->getMaximumNumberOfIterations() << std::endl;
                break;
            }
            case SVM_L1_REGULARIZED_L2_LOSS:
            {
                L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                out << tab << " · Epsilon: " << object_ptr->getEpsilon() << std::endl;
                out << tab << " · Transpose: " << (object_ptr->getTransposeData()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                out << tab << " · Maximum number of iterations: " << object_ptr->getMaximumNumberOfIterations() << std::endl;
                break;
            }
            case ONLINE_PEGASOS_ID:
            case ONLINE_FIRST_ORDER_SGD_ID:
            case ONLINE_QN_SGD_ID:
            case ONLINE_PERCEPTRON_ID:
            case ONLINE_MULTIPLE_PERCEPTRON_ID:
            {
                LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * online_learner_ptr = (LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                
                out << tab << " · Average classifier: " << (online_learner_ptr->isAverageClassifier()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                out << tab << " · Average skip: " << online_learner_ptr->getAverageSkip() << std::endl;
                out << tab << " · Chunk size: " << online_learner_ptr->getChunkSize() << std::endl;
                
                if (identifier == ONLINE_PEGASOS_ID)
                {
                    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                    
                    out << tab << " · Loss function: " << object_ptr->getLossFunction() << std::endl;
                    out << tab << " · Spherical projection: " << (object_ptr->getProjection()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                    out << std::endl;
                }
                else if (identifier == ONLINE_FIRST_ORDER_SGD_ID)
                {
                    SgdFirstOrder<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (SgdFirstOrder<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                    
                    out << tab << " · Loss function: " << object_ptr->getLossFunction() << std::endl;
                    out << tab << " · Spherical projection: " << (object_ptr->getProjection()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                    out << tab << " · Initial step: " << object_ptr->getInitialStep() << std::endl;
                    out << tab << " · Skip: " << object_ptr->getSkip() << std::endl;
                }
                else if (identifier == ONLINE_QN_SGD_ID)
                {
                    SgdQuasiNewton<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (SgdQuasiNewton<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                    
                    out << tab << " · Loss function: " << object_ptr->getLossFunction() << std::endl;
                    out << tab << " · Spherical projection: " << (object_ptr->getProjection()?("[ENABLED]"):("[DISABLED]")) << std::endl;
                    out << tab << " · Initial step: " << object_ptr->getInitialStep() << std::endl;
                    out << tab << " · Skip: " << object_ptr->getSkip() << std::endl;
                }
                else if (identifier == ONLINE_PERCEPTRON_ID)
                {
                    Perceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (Perceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                    
                    out << tab << " · Loss function: " << object_ptr->getLossFunction() << std::endl;
                }
                else if (identifier == ONLINE_MULTIPLE_PERCEPTRON_ID)
                {
                    MultiplePerceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * object_ptr = (MultiplePerceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)object;
                    
                    out << tab << " · Number of Perceptrons: " << object_ptr->getNumberOfPerceptrons() << std::endl;
                    out << tab << " · Loss function: " << object_ptr->getLossFunction() << std::endl;
                }
                break;
            }
        default:
            break;
        }
        tab.decrease(out);
        
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PROBABILITY SCORES AUXILIARY         |
    //                   | FUNCTIONS                            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Loads the probabilistic classifier object from an XML file.
     *  \param[in] filename file where the probabilistic classifier object is loaded.
     *  \param[out] object probabilistic classifier object.
     *  \note this function does not deallocates the memory pointed by the object pointer.
     */
    template <class TSCORE, class TPROBABILITY>
    void loadObject(const char * filename, ProbabilisticScoreBase<TSCORE, TPROBABILITY> * &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object = ProbabilisticScoreFactory<TSCORE, TPROBABILITY>::Type::getInstantiator((PROBABILITY_METHOD_IDENTIFIER)((int)parser.getAttribute("Identifier")));
        object->convertFromXML(parser);
        delete file;
    }
    
    /** Saves the probabilistic classifier object into an XML file.
     *  \param[in] filename file where the probabilistic classifier object is stored.
     *  \param[in] object probabilistic classifier object.
     */
    template <class TSCORE, class TPROBABILITY>
    void saveObject(const char * filename, const ProbabilisticScoreBase<TSCORE, TPROBABILITY> * object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    /** Shows in an standard output stream the information of the given probabilistic classifier object.
     *  \param[out] out standard library output stream.
     *  \param[in] object probabilistic classifier object.
     *  \returns resulting output stream with the probabilistic object information.
     */
    template <class TSCORE, class TPROBABILITY>
    std::ostream& operator<<(std::ostream &out, const ProbabilisticScoreBase<TSCORE, TPROBABILITY> * object)
    {
        PROBABILITY_METHOD_IDENTIFIER identifier = object->getIdentifier();
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Probabilistic scores ]" << std::endl;
        tab.increase(out);
        out << tab << " · Probabilities approximation algorithm: ";
        switch (identifier)
        {
        case RESCALING_ALGORITHM:
            out << "Rescaling (or linear fitting) algorithm.";
            break;
        case SIGMOID_ALGORITHM:
            out << "Sigmoid fitting algorithm.";
            break;
        case ASYMMETRIC_LAPLACE_ALGORITHM:
            out << "Asymmetric Laplace fitting algorithm.";
            break;
        case ASYMMETRIC_GAUSSIAN_ALGORITHM:
            out << "Asymmetric Gaussian fitting algorithm.";
            break;
        case BINNING_ALGORITHM:
            out << "Binning algorithm.";
            break;
        case ISOTONIC_REGRESSION_ALGORITHM:
            out << "Isotonic Regression algorithm.";
            break;
        case PROBABILITIES_LUT_ALGORITHM:
            out << "Look-up-table";
            break;
        default:
            out << "UNKNOWN";
            break;
        }
        out << std::endl;
        out << tab << " · Number of categories: " << object->getNumberOfCategories() << std::endl;
        out << tab << " · Joint multi-class probability: " << (object->getMulticlass()?("[ENABLED]"):("[DISABLED]")) << std::endl;
        
        switch (identifier)
        {
        case RESCALING_ALGORITHM:
            {
                ProbabilisticRescale<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticRescale<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Window size: " << object_ptr->getWindowSize() << std::endl;
            }
            break;
        case SIGMOID_ALGORITHM:
            {
                ProbabilisticSigmoid<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticSigmoid<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Balance positive-negative samples: " << (object_ptr->getBalance()?("[ENABLED]"):("[DISABLED]")) << std::endl;
            }
            break;
        case ASYMMETRIC_LAPLACE_ALGORITHM:
            {
                ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Balance positive-negative samples: " << (object_ptr->getBalance()?("[ENABLED]"):("[DISABLED]")) << std::endl;
            }
            break;
        case ASYMMETRIC_GAUSSIAN_ALGORITHM:
            {
                ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Balance positive-negative samples: " << (object_ptr->getBalance()?("[ENABLED]"):("[DISABLED]")) << std::endl;
            }
            break;
        case BINNING_ALGORITHM:
            {
                ProbabilisticBinning<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticBinning<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Number of bins: " << object_ptr->getNumberOfPartitions() << std::endl;
                out << tab << " · Gaussian weight functions: " << (object_ptr->getGaussianBins()?("[ENABLED]"):("[DISABLED]")) << std::endl;
            }
            break;
        case ISOTONIC_REGRESSION_ALGORITHM:
            {
                ProbabilisticIsotonicRegression<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticIsotonicRegression<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Number of bins: " << object_ptr->getNumberOfBins() << std::endl;
            }
            break;
        case PROBABILITIES_LUT_ALGORITHM:
            {
                ProbabilisticLUT<TSCORE, TPROBABILITY> * object_ptr = (ProbabilisticLUT<TSCORE, TPROBABILITY> *)object;
                out << tab << " · Number of bins of the look-up-table: " << object_ptr->getNumberOfBins() << std::endl;
            }
            break;
        default:
            break;
        }
        tab.decrease(out);
        
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

