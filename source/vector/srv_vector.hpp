// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_VECTOR_DEFINITION_HPP_HEADER_FILE__
#define __SRV_VECTOR_DEFINITION_HPP_HEADER_FILE__

#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include <iostream>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DECLARATIONS                         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CLASSES FORWARD DECLARATIONS ]=============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T, class N> class VectorDense;
    template <class T, class N> class VectorSparse;
    template <class T, class N> class SubVectorDense;
    template <class T, class N> class ConstantSubVectorDense;
    template <class T, class N> class SubVectorSparse;
    template <class T, class N> class ConstantSubVectorSparse;
    
    template <template <class, class> class VECTOR, class T, class N>
    struct ConstantVectorType { typedef ConstantSubVectorDense<T, N> Type; };
    template <class T, class N> struct ConstantVectorType<VectorSparse, T, N> { typedef ConstantSubVectorSparse<T, N> Type; };
    template <class T, class N> struct ConstantVectorType<SubVectorSparse, T, N> { typedef ConstantSubVectorSparse<T, N> Type; };
    template <class T, class N> struct ConstantVectorType<ConstantSubVectorSparse, T, N> { typedef ConstantSubVectorSparse<T, N> Type; };
    
    template <template <class, class> class VECTOR>
    struct GeneralVectorType { template <class T, class N> using Type = typename srv::VectorDense<T, N>; };
    template <> struct GeneralVectorType<srv::VectorSparse>            { template <class T, class N> using Type = typename srv::VectorSparse<T, N>; };
    template <> struct GeneralVectorType<srv::SubVectorSparse>         { template <class T, class N> using Type = typename srv::VectorSparse<T, N>; };
    template <> struct GeneralVectorType<srv::ConstantSubVectorSparse> { template <class T, class N> using Type = typename srv::VectorSparse<T, N>; };
    /****
    template <template <class, class> class VECTOR, class T, class N>
    struct VectorType { typedef VectorDense<T, N> Type; };
    template <class T, class N> struct VectorType<VectorSparse, T, N> { typedef VectorSparse<T, N> Type; };
    template <class T, class N> struct VectorType<SubVectorSparse, T, N> { typedef VectorSparse<T, N> Type; };
    template <class T, class N> struct VectorType<ConstantSubVectorSparse, T, N> { typedef VectorSparse<T, N> Type; };
    */
    template <template <class, class> class VECTOR, class T, class N>
    struct VectorType { typedef typename GeneralVectorType<VECTOR>::template Type<T, N> Type; };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ DEFAULT NUMBER OF THREADS FOR THE IMAGE FUNCTIONS ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class VectorDefaultNumberOfThreads
    {
    public:
        /// Reference to the default number of threads for the vector arithmetic operations (i.e.\ to the vector operators).
        inline static unsigned int& VectorArithmetic(void) { static unsigned int m_default_number_of_threads_arithmetic = 1; return m_default_number_of_threads_arithmetic; }
        /// Reference to the default number of threads for the vector distance objects.
        inline static unsigned int& VectorDistances(void) { static unsigned int m_default_number_of_threads_processing = 1; return m_default_number_of_threads_processing; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // ==============================================================================================================================================
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE VECTOR CLASS                   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Dense vector class.
    template <class T, class N = unsigned int>
    class VectorDense
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        VectorDense(void) :
            m_data(0),
            m_number_of_elements(0) {}
        /** Constructor which builds a dense vector of the given size.
         *  \param[in] number_of_elements number of elements of the dense vector.
         */
        VectorDense(const N &number_of_elements) :
            m_data((number_of_elements != 0)?new T[number_of_elements]:0),
            m_number_of_elements(number_of_elements) {}
        /** Constructor which builds a dense vector of the given size and initializes its elements.
         *  \param[in] number_of_elements number of elements of the dense vector.
         *  \param[in] value initialization value used to set all the elements of the dense vector.
         */
        VectorDense(const N &number_of_elements, const T &value) :
            m_data((number_of_elements != 0)?new T[number_of_elements]:0),
            m_number_of_elements(number_of_elements)
        {
            for (N i = 0; i < number_of_elements; ++i)
                m_data[i] = value;
        }
        /** Constructor which builds the dense vector from an array of data.
         *  \param[in] data array of values of the dense vector.
         *  \param[in] number_of_elements number of elements of the dense vector.
         */
        template <class V, class M>
        VectorDense(const V *data, const M &number_of_elements) :
            m_data((number_of_elements != 0)?new T[number_of_elements]:0),
            m_number_of_elements((N)number_of_elements)
        {
            for (N i = 0; i < (N)number_of_elements; ++i)
                m_data[i] = (T)data[i];
        }
        /// Copy constructor.
        VectorDense(const VectorDense<T, N> &other) :
            m_data((other.m_data != 0)?new T[other.m_number_of_elements]:0),
            m_number_of_elements(other.m_number_of_elements)
        {
            for (N i = 0; i < other.m_number_of_elements; ++i)
                m_data[i] = other.m_data[i];
        }
        /// Copy constructor for a dense vector of a different data-type.
        template <class V, class M>
        VectorDense(const VectorDense<V, M> &other) :
            m_data((other.getData() != 0)?new T[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (N i = 0; i < (N)other.size(); ++i)
                m_data[i] = (T)other.getData((M)i);
        }
        /// Destructor
        ~VectorDense(void)
        {
            if (m_data != 0) delete [] m_data;
        }
        /// Assignation operator.
        VectorDense<T, N>& operator=(const VectorDense<T, N> &other)
        {
            if (this != &other)
            {
                if (m_data != 0) delete [] m_data;
                
                m_data = (other.m_data != 0)?new T[other.m_number_of_elements]:0;
                m_number_of_elements = other.m_number_of_elements;
                for (N i = 0; i < other.m_number_of_elements; ++i)
                    m_data[i] = other.m_data[i];
            }
            return *this;
        }
        /// Assignation operator for a dense vector of a different data-type.
        template <class V, class M>
        VectorDense<T, N>& operator=(const VectorDense<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_data = (other.size() > 0)?new T[other.size()]:0;
            m_number_of_elements = (N)other.size();
            for (N i = 0; i < (N)other.size(); ++i)
                m_data[i] = (T)other.getData((M)i);
            
            return *this;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS AND ASSIGNATION OPERATORS FROM VECTORS OF OTHER CLASSES ]=====================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
     
        /// Constructor which builds the dense vector from a sparse vector.
        template <class V, class M>
        VectorDense(const VectorSparse<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[(N)other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
        }
        /// Constructor which builds the dense vector from a dense sub-vector.
        template <class V, class M>
        VectorDense(const SubVectorDense<V, M> &other) :
            m_data((other.size() > 0)?new T[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (N i = 0; i < other.size(); ++i)
                m_data[i] = (T)other[(M)i];
        }
        /// Constructor which builds the dense vector from a sparse sub-vector.
        template <class V, class M>
        VectorDense(const SubVectorSparse<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
        }
        /// Constructor which builds the dense vector from a constant dense sub-vector.
        template <class V, class M>
        VectorDense(const ConstantSubVectorDense<V, M> &other) :
            m_data((other.size() > 0)?new T[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (N i = 0; i < other.size(); ++i)
                m_data[i] = (T)other[(M)i];
        }
        /// Constructor which builds the dense vector from a constant sparse sub-vector.
        template <class V, class M>
        VectorDense(const ConstantSubVectorSparse<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
        }
        
        /// Assignation operator which creates a dense vector from a sparse vector.
        template <class V, class M>
        VectorDense<T, N>& operator=(const VectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
            else m_data = 0;
            
            return *this;
        }
        /// Assignation operator which creates a dense vector from a dense sub-vector.
        template <class V, class M>
        VectorDense<T, N>& operator=(const SubVectorDense<V, M> &other)
        {
            T *old_data = m_data;
            
            if (other.size() > 0)
            {
                m_number_of_elements = (N)other.size();
                m_data = new T[other.size()];
                for (N i = 0; i < (N)other.size(); ++i)
                    m_data[i] = (T)other[(M)i];
            }
            else
            {
                m_data = 0;
                m_number_of_elements = 0;
            }
            
            if (old_data != 0) delete [] old_data;
            
            return *this;
        }
        /// Assignation operator which creates a dense vector from a sparse sub-vector.
        template <class V, class M>
        VectorDense<T, N>& operator=(const SubVectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
            else m_data = 0;
            
            return *this;
        }
        /// Assignation operator which creates a dense vector from a constant dense sub-vector.
        template <class V, class M>
        VectorDense<T, N>& operator=(const ConstantSubVectorDense<V, M> &other)
        {
            T *old_data = m_data;
            
            if (other.size() > 0)
            {
                m_number_of_elements = (N)other.size();
                m_data = new T[other.size()];
                for (N i = 0; i < (N)other.size(); ++i)
                    m_data[i] = (T)other[(M)i];
            }
            else
            {
                m_data = 0;
                m_number_of_elements = 0;
            }
            
            if (old_data != 0) delete [] old_data;
            
            return *this;
        }
        /// Assignation operator which creates a dense vector from a constant sparse sub-vector.
        template <class V, class M>
        VectorDense<T, N>& operator=(const ConstantSubVectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[(M)i].getFirst();
            }
            else m_data = 0;
            
            return *this;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Clear the memory allocated by the vector.
        inline void clear(void)
        {
            if (m_data != 0) delete [] m_data;
            m_data = 0;
            m_number_of_elements = 0;
        }
        /** Clears the current vector information and creates a new vector of the given size.
         *  \param[in] number_of_elements number of elements of the vector.
         */
        inline void set(const N &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements > 0)?new T[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
        }
        /** Clears the current vector information, creates a new vector of the given size and initialized its elements.
         *  \param[in] number_of_elements number of elements of the vector.
         *  \param[in] value initialization value assigned to all the vector elements.
         */
        inline void set(const N &number_of_elements, const T &value)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements > 0)?new T[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
            for (N i = 0; i < number_of_elements; ++i) m_data[i] = value;
        }
        /** Clears the current vector information and creates a copy of the given array.
         *  \param[in] data array with the new values of the vector.
         *  \param[in] number_of_elements number of elements of the dense vector.
         */
        template <class V, class M>
        inline void set(const V *data, const M &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements > 0)?new T[number_of_elements]:0;
            m_number_of_elements = (N)number_of_elements;
            for (M i = 0; i < number_of_elements; ++i) m_data[i] = (T)data[i];
        }
        /** Clears the information of the vector and creates a new vector from the given dense vector.
         *  \param[in] other dense vector of the same type.
         */
        inline void set(const VectorDense<T, N> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.size() > 0)?new T[other.size()]:0;
            m_number_of_elements = other.size();
            for (N i = 0; i < other.size(); ++i) m_data[i] = other.m_data[i];
        }
        /** Clears the information of the vector and creates a new vector from a dense vector of a different data type.
         *  \param[in] other dense vector of a different type.
         */
        template <class V, class M>
        inline void set(const VectorDense<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.size() > 0)?new T[other.size()]:0;
            m_number_of_elements = (N)other.size();
            for (M i = 0; i < other.size(); ++i) m_data[i] = (T)other[i];
        }
        /// Clears the information of the vector and creates a new dense vector from a sparse vector.
        template <class V, class M>
        void set(const VectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[i].getFirst();
            }
            else m_data = 0;
        }
        /// Clears the information of the vector and creates a new dense vector from a dense sub-vector.
        template <class V, class M>
        void set(const SubVectorDense<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.size() > 0)?new T[other.size()]:0;
            m_number_of_elements = (N)other.size();
            for (M i = 0; i < other.size(); ++i) m_data[i] = (T)other[i];
        }
        /// Clears the information of the vector and creates a new dense vector from a sparse sub-vector.
        template <class V, class M>
        void set(const SubVectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if ((N)other[i].getSecond() > m_number_of_elements) m_number_of_elements = (N)other[i].getSecond();
            
            if (m_number_of_elements > 0)
            {
                ++m_number_of_elements;
                m_data = new T[m_number_of_elements];
                for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
                for (M i = 0; i < other.size(); ++i)
                    m_data[other[i].getSecond()] = (T)other[i].getFirst();
            }
            else m_data = 0;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the dense vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the elements of the dense vector.
        inline const T* getData(void) const { return m_data; }
        /// Returns a pointer to the elements of the dense vector.
        inline T* getData(void) { return m_data; }
        /// Returns a constant reference to the index-th element of the dense vector.
        inline const T& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Returns a reference to the index-th element of the dense vector.
        inline T& getData(const N &index) { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a constant reference to the index-th element of the dense vector.
        inline const T& operator[](const N &index) const { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a reference to the index-th element of the dense vector.
        inline T& operator[](const N &index) { return m_data[(unsigned)index]; }
        /** Resizes the dense vector. When the vector decreases its size, the values which fall outside the new vector are dropped.
         * On the other hand, when the vector increases its size, the new positions are initialized to the given value.
         *  \param[in] size new number of elements of the dense vector.
         *  \param[in] value initialization value for the new locations.
         */
        void resize(const N &new_size, const T &value = 0)
        {
            if (new_size == 0)
            {
                if (m_data != 0) delete [] m_data;
                m_data = 0;
                m_number_of_elements = 0;
            }
            else if (new_size != m_number_of_elements)
            {
                N i;
                T *data;
                
                data = new T[new_size];
                for (i = 0; i < srvMin(new_size, m_number_of_elements); ++i) data[i] = m_data[i];
                for (; i < new_size; ++i) data[i] = value;
                srvSwap(m_data, data);
                m_number_of_elements = new_size;
                delete [] data;
            }
        }
        /** Sets the elements of the dense vector to the given value.
         *  \param[in] value value assigned to each position of the vector.
         */
        inline void setValue(const T &value) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = value; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONTENT COPY FUNCTIONS ]===================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Copies the content of another dense vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        inline void copy(const VectorDense<T, N> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other.m_data[i];
        }
        /** Copies the content of a dense vector of a different data type to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a sparse vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        /** Copies the content of a dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a sparse sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        /** Copies the content of a constant dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a constant sparse sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ XML FUNCTIONS ]============================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Stores the dense vector information in an XML object.
        void convertToXML(XmlParser &parser) const
        {
            parser.openTag("Vector");
            parser.setAttribute("Size", m_number_of_elements);
            parser.setAttribute("Sparse", false);
            parser.addChildren();
            writeToBase64Array(parser, m_data, m_number_of_elements);
            parser.closeTag();
        }
        /// Retrieves the dense vector information from an XML object.
        void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Vector"))
            {
                if (m_data != 0) delete [] m_data;
                
                if (parser.getAttribute("Sparse"))
                {
                    unsigned int number_of_elements;
                    Tuple<T, N> *data;
                    
                    number_of_elements = parser.getAttribute("Size");
                    if (number_of_elements > 0) data = new Tuple<T, N>[number_of_elements];
                    else data = 0;
                    
                    while (!(parser.isTagIdentifier("Vector") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Vector", data, number_of_elements);
                        else parser.getNext();
                    }
                    parser.getNext();
                    
                    m_number_of_elements = 0;
                    for (unsigned int i = 0; i < number_of_elements; ++i)
                        if (data[i].getSecond() > m_number_of_elements) m_number_of_elements = data[i].getSecond();
                    if (m_number_of_elements > 0)
                    {
                        ++m_number_of_elements;
                        m_data = new T[m_number_of_elements];
                        for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)0;
                        for (unsigned int i = 0; i < number_of_elements; ++i) m_data[data[i].getSecond()] = data[i].getFirst();
                    }
                    else m_data = 0;
                    
                    if (data != 0) delete [] data;
                }
                else
                {
                    m_number_of_elements = parser.getAttribute("Size");
                    if (m_number_of_elements != 0) m_data = new T[m_number_of_elements];
                    else m_data = 0;
                    
                    while (!(parser.isTagIdentifier("Vector") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Vector", m_data, m_number_of_elements);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
            }
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current dense vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline ConstantSubVectorDense<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorDense<T, N>(&m_data[initial_element], number_of_elements); }
        
        /** Returns a sub-vector of the current dense vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline SubVectorDense<T, N> subvector(const N &initial_element, const N &number_of_elements) { return SubVectorDense<T, N>(&m_data[initial_element], number_of_elements); }
        
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER DECLARATION ]=======================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the dense vector.
        T *m_data;
        /// Number of elements in the dense vector array.
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SPARSE VECTOR CLASS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Sparse vector class.
    template <class T, class N = unsigned int>
    class VectorSparse
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        VectorSparse(void) :
            m_data(0),
            m_number_of_elements(0) {}
        /** Constructor which builds an sparse vector of the given size
         *  \param[in] number_of_elements number of non-zero elements of the sparse vector.
         */
        VectorSparse(const N &number_of_elements) :
            m_data((number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0),
            m_number_of_elements(number_of_elements) {}
        /** Constructor which builds an sparse vector of the specified size and initializes its elements.
         *  \param[in] number_of_elements number of non-zero elements of the sparse vector.
         *  \param[in] value value assigned at each data field of the sparse vector.
         *  \param[in] index index assigned at each index field of the sparse vector.
         */
        VectorSparse(const N &number_of_elements, const T &value, const N &index) :
            m_data((number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0),
            m_number_of_elements(number_of_elements)
        {
            for (N i = 0; i < number_of_elements; ++i)
                m_data[i].setData(value, index);
        }
        /** Constructor which builds an sparse vector from an array. The constructor first counts the number of non-zero elements in the array
         *  and afterwards build the sparse vector from the array.
         *  \param[in] values array of values.
         *  \param[in] number_of_elements elements in the array.
         */
        template <class V, class M>
        VectorSparse(const V *values, const M &number_of_elements) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < number_of_elements; ++i) if ((T)values[i] != 0) ++m_number_of_elements;
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < number_of_elements; ++i)
                {
                    if ((T)values[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)values[i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
        }
        /** Constructor which builds the sparse vector from a data and an indexes arrays.
         *  \param[in] data array with the sparse data values.
         *  \param[in] indexes array with the indexes of each value.
         *  \param[in] number_of_elements number of elements in both arrays.
         */
        template <class V, class M, class P>
        VectorSparse(const V *data, const M *indexes, const P &number_of_elements) :
            m_data((number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0),
            m_number_of_elements(number_of_elements)
        {
            for (N i = 0; i < (N)number_of_elements; ++i)
                m_data[i].setData((T)data[i], (N)indexes[i]);
        }
        /** Constructor which builds the sparse vector from an array of tuples.
         *  \param[in] data array of tuples with the sparse data values.
         *  \param[in] number_of_elements number of elements in the array.
         */
        template <class V, class M, class P>
        VectorSparse(const Tuple<V, M> *data, const P &number_of_elements) :
            m_data((number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0),
            m_number_of_elements(number_of_elements)
        {
            for (N i = 0; i < (N)number_of_elements; ++i)
                m_data[i] = data[(M)i];
        }
        /// Copy constructor.
        VectorSparse(const VectorSparse<T, N> &other) :
            m_data((other.m_data != 0)?new Tuple<T, N>[other.m_number_of_elements]:0),
            m_number_of_elements(other.m_number_of_elements)
        {
            for (N i = 0; i < m_number_of_elements; ++i)
                m_data[(unsigned)i] = other.m_data[(unsigned)i];
        }
        /// Copy constructor for sparse vectors of a different type.
        template <class V, class M>
        VectorSparse(const VectorSparse<V, M> &other) :
            m_data((other.size() > 0)?new Tuple<T, N>[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (M i = 0; i < other.size(); ++i)
                m_data[(unsigned long)i] = other[(unsigned long)i];
        }
        /// Destructor.
        ~VectorSparse(void) { if (m_data != 0) delete [] m_data; }
        /// Assignation operator
        VectorSparse<T, N>& operator=(const VectorSparse<T, N> &other)
        {
            if (this != &other)
            {
                if (m_data != 0) delete [] m_data;
                m_data = (other.m_data != 0)?new Tuple<T, N>[other.m_number_of_elements]:0;
                m_number_of_elements = other.m_number_of_elements;
                for (N i = 0; i < other.m_number_of_elements; ++i) m_data[(unsigned)i] = other.m_data[(unsigned)i];
            }
            return *this;
        }
        /// Assignation operator for an sparse vector of a different data type.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const VectorSparse<V, M> &other) { this->set(other.getData(), other.size()); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS AND ASSIGNATION OPERATORS FROM VECTORS OF OTHER CLASSES ]=====================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Constructor which builds the sparse vector from a dense vector.
        template <class V, class M>
        VectorSparse(const VectorDense<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if (other[i] != 0) ++m_number_of_elements;
            
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < other.size(); ++i)
                {
                    if (other[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)other[(M)i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
        }
        /// Constructor which builds the sparse vector from a dense sub-vector.
        template <class V, class M>
        VectorSparse(const SubVectorDense<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if (other[i] != 0) ++m_number_of_elements;
            
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < other.size(); ++i)
                {
                    if (other[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)other[(M)i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
        }
        /// Constructor which builds the sparse vector from a sparse sub-vector.
        template <class V, class M>
        VectorSparse(const SubVectorSparse<V, M> &other) :
            m_data((other.size() > 0)?new Tuple<T, N>[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (M i = 0; i < other.size(); ++i) m_data[i] = other[(M)i];
        }
        /// Constructor which builds the sparse vector from a constant dense sub-vector.
        template <class V, class M>
        VectorSparse(const ConstantSubVectorDense<V, M> &other) :
            m_data(0),
            m_number_of_elements(0)
        {
            for (M i = 0; i < other.size(); ++i)
                if (other[i] != 0) ++m_number_of_elements;
            
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < other.size(); ++i)
                {
                    if (other[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)other[(M)i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
        }
        /// Constructor which builds the sparse vector from a constant sparse sub-vector.
        template <class V, class M>
        VectorSparse(const ConstantSubVectorSparse<V, M> &other) :
            m_data((other.size() > 0)?new Tuple<T, N>[other.size()]:0),
            m_number_of_elements((N)other.size())
        {
            for (M i = 0; i < other.size(); ++i) m_data[i] = other[(M)i];
        }
        
        /// Assignation operator which creates a sparse vector from a dense vector.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const VectorDense<V, M> &other) { this->set(other.getData(), other.size()); return *this;}
        /// Assignation operator which creates a sparse vector from a dense sub-vector.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const SubVectorDense<V, M> &other) { this->set(other.getData(), other.size()); return *this; }
        /// Assignation operator which creates a sparse vector from a sparse sub-vector.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const SubVectorSparse<V, M> &other) { this->set(other.getData(), other.size()); return *this; }
        /// Assignation operator which creates a sparse vector from a dense sub-vector.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const ConstantSubVectorDense<V, M> &other) { this->set(other.getData(), other.size()); return *this; }
        /// Assignation operator which creates a sparse vector from a sparse sub-vector.
        template <class V, class M>
        VectorSparse<T, N>& operator=(const ConstantSubVectorSparse<V, M> &other) { this->set(other.getData(), other.size()); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Clears the memory allocated by the sparse vector.
        void clear(void)
        {
            if (m_data != 0) delete [] m_data;
            m_data = 0;
            m_number_of_elements = 0;
        }
        
        /** Clears the information of the vector and creates a new sparse vector of the given size.
         *  \param[in] number_of_elements number of non-zero elements of the sparse vector.
         */
        void set(const N &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
        }
        /** Clears the information of the vector, creates an sparse vector of the specified size and initializes its elements.
         *  \param[in] number_of_elements number of non-zero elements of the sparse vector.
         *  \param[in] value value assigned at each data field of the sparse vector.
         *  \param[in] index index assigned at each index field of the sparse vector.
         */
        void set(const N &number_of_elements, const T &value, const N &index)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
            for (N i = 0; i < number_of_elements; ++i) m_data[i].setData(value, index);
        }
        /** Clears the information of the vector and creates an sparse vector from an array. The constructor first counts the number of non-zero elements in the array
         *  and afterwards build the sparse vector from the array.
         *  \param[in] values array of values.
         *  \param[in] number_of_elements elements in the array.
         */
        template <class V, class M>
        void set(const V *values, const M &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < number_of_elements; ++i) if (values[i] != 0) ++m_number_of_elements;
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < number_of_elements; ++i)
                {
                    if (values[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)values[i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
            else m_data = 0;
        }
        /** Clear the information of the vector and creates an sparse vector from a data and an indexes arrays.
         *  \param[in] data array with the sparse data values.
         *  \param[in] indexes array with the indexes of each value.
         *  \param[in] number_of_elements number of elements in both arrays.
         */
        template <class V, class M, class P>
        void set(const V *data, const M *indexes, const P &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
            for (N i = 0; i < (N)number_of_elements; ++i) m_data[i].setData((T)data[i], (N)indexes[i]);
        }
        /** Clear the information of the vector and creates an sparse vector from an array of tuples.
         *  \param[in] data array of tuples with the sparse data values.
         *  \param[in] number_of_elements number of elements in the array.
         */
        template <class V, class M, class P>
        void set(const Tuple<V, M> *data, const P &number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (number_of_elements != 0)?new Tuple<T, N>[number_of_elements]:0;
            m_number_of_elements = number_of_elements;
            for (N i = 0; i < (N)number_of_elements; ++i) m_data[i] = data[(M)i];
        }
        /// Clears the information of the vector and creates a copy of the given sparse vector.
        void set(const VectorSparse<T, N> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.m_data != 0)?new Tuple<T, N>[other.m_number_of_elements]:0;
            m_number_of_elements = other.m_number_of_elements;
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other.m_data[i];
        }
        /// Clears the information of the vector and creates a copy from an sparse vector of a different data type.
        template <class V, class M>
        void set(const VectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.size() > 0)?new Tuple<T, N>[other.size()]:0;
            m_number_of_elements = (N)other.size();
            for (M i = 0; i < other.size(); ++i) m_data[i] = other[i];
        }
        
        /// Clears the information of the vector and creates an sparse vector from the given dense vector.
        template <class V, class M>
        void set(const VectorDense<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if (other[i] != 0) ++m_number_of_elements;
            
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < other.size(); ++i)
                {
                    if (other[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)other[i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
            else m_data = 0;
        }
        /// Clear the information of the vector and creates an sparse vector from the given dense sub-vector.
        template <class V, class M>
        void set(const SubVectorDense<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            
            m_number_of_elements = 0;
            for (M i = 0; i < other.size(); ++i)
                if (other[i] != 0) ++m_number_of_elements;
            
            if (m_number_of_elements > 0)
            {
                m_data = new Tuple<T, N>[m_number_of_elements];
                m_number_of_elements = 0;
                for (M i = 0; i < other.size(); ++i)
                {
                    if (other[i] != 0)
                    {
                        m_data[m_number_of_elements].setData((T)other[i], (N)i);
                        ++m_number_of_elements;
                    }
                }
            }
            else m_data = 0;
        }
        /// Clear the information of the vector and creates an sparse vector from the given sparse sub-vector.
        template <class V, class M>
        void set(const SubVectorSparse<V, M> &other)
        {
            if (m_data != 0) delete [] m_data;
            m_data = (other.size() > 0)?new Tuple<T, N>[other.size()]:0;
            m_number_of_elements = (N)other.size();
            for (M i = 0; i < other.size(); ++i) m_data[i] = other[i];
        }
        
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the sparse vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the data of the sparse vector.
        inline const Tuple<T, N>* getData(void) const { return m_data; }
        /// Returns a pointer to the data of the sparse vector.
        inline Tuple<T, N>* getData(void) { return m_data; }
        /// Returns a constant reference to the index-th element of the sparse vector.
        inline const Tuple<T, N>& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Returns a reference to the index-th element of the sparse vector.
        inline Tuple<T, N>& getData(const N &index) { return m_data[(unsigned)index]; }
        /// Returns a constant reference to the index-th value of the sparse vector.
        inline const T& getValue(const N &index) const { return m_data[(unsigned)index].getFirst(); }
        /// Returns a reference to the index-th value of the sparse vector.
        inline T& getValue(const N &index) { return m_data[(unsigned)index].getFirst(); }
        /// Returns a constant reference to the index-th index of the sparse vector.
        inline const N& getIndex(const N &index) const { return m_data[(unsigned)index].getSecond(); }
        /// Returns a reference to the index-th of the sparse vector.
        inline N& getIndex(const N &index) { return m_data[(unsigned)index].getSecond(); }
        /// Array subscript operator which returns a constant reference to the index-the element of the sparse vector.
        inline const Tuple<T, N>& operator[](const N &index) const { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a reference to the index-the element of the sparse vector.
        inline Tuple<T, N>& operator[](const N &index) { return m_data[(unsigned)index]; }
        /** Resizes the sparse vector. When the vector decreases its size, the values which fall outside the new vector are dropped.
         * On the other hand, when the vector increases its size, the new positions are initialized to the given value.
         *  \param[in] size new number of elements of the dense vector.
         *  \param[in] value initialization value for the new locations.
         *  \param[in] index initialization index for the new locations.
         */
        void resize(const N &new_size, const T &value = 0, const N &index = (N)-1)
        {
            if (new_size == 0)
            {
                if (m_data != 0) delete [] m_data;
                m_data = 0;
                m_number_of_elements = 0;
            }
            else if (new_size != m_number_of_elements)
            {
                N i;
                Tuple<T, N> * data;
                
                data = new Tuple<T, N>[new_size];
                for (i = 0; i < srvMin(new_size, m_number_of_elements); ++i) data[i] = m_data[i];
                for (; i < new_size; ++i) data[i].setData(value, index);
                srvSwap(m_data, data);
                m_number_of_elements = new_size;
                delete [] data;
            }
        }
        /** Sets the values of the sparse vector elements to the given value.
         *  \param[in] value value assigned to each position of the sparse vector.
         */
        inline void setValue(const T &value) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setFirst(value); }
        /** Sets the index of the sparse vector elements to the given value.
         *  \param[in] index index value assigned to each position of the sparse vector.
         */
        inline void setIndex(const N &index) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setSecond(index); }
        /** Sets the elements of the sparse vector elements to the given value and index.
         *  \param[in] value value assigned to each position of the sparse vector.
         *  \param[in] index index assigned to each position of the sparse vector.
         */
        inline void setValue(const T &value, const N &index) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setData(value, index); }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONTENT COPY FUNCTIONS ]===================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Copies the content of a dense vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of another sparse vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        inline void copy(const VectorSparse<T, N> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other.m_data[i];
        }
        /** Copies the content of a sparse vector of a different data type to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        /** Copies the content of a dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of a sparse sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        /** Copies the content of a constant dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of a constant sparse sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ XML FUNCTIONS ]============================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Stores the dense vector information in an XML object.
        void convertToXML(XmlParser &parser) const
        {
            parser.openTag("Vector");
            parser.setAttribute("Size", m_number_of_elements);
            parser.setAttribute("Sparse", true);
            parser.addChildren();
            writeToBase64Array(parser, m_data, m_number_of_elements);
            parser.closeTag();
        }
        /// Retrieves the dense vector information from an XML object.
        void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Vector"))
            {
                if (m_data != 0) delete [] m_data;
                
                if (parser.getAttribute("Sparse"))
                {
                    m_number_of_elements = parser.getAttribute("Size");
                    if (m_number_of_elements != 0) m_data = new Tuple<T, N>[m_number_of_elements];
                    else m_data = 0;
                    
                    while (!(parser.isTagIdentifier("Vector") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Vector", m_data, m_number_of_elements);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else
                {
                    unsigned int number_of_elements;
                    T *data;
                    
                    number_of_elements = parser.getAttribute("Size");
                    if (number_of_elements > 0) data = new T[number_of_elements];
                    else data = 0;
                    
                    while (!(parser.isTagIdentifier("Vector") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Vector", data, number_of_elements);
                        else parser.getNext();
                    }
                    parser.getNext();
                    
                    m_number_of_elements = 0;
                    for (unsigned int i = 0; i < number_of_elements; ++i)
                        if (data[i] != 0) ++m_number_of_elements;
                    if (m_number_of_elements > 0)
                    {
                        m_data = new Tuple<T, N>[m_number_of_elements];
                        m_number_of_elements = 0;
                        for (unsigned int i = 0; i < number_of_elements; ++i)
                        {
                            if (data[i] != 0)
                            {
                                m_data[m_number_of_elements].setData(data[i], (N)i);
                                ++m_number_of_elements;
                            }
                        }
                    }
                    else m_data = 0;
                    
                    if (data != 0) delete [] data;
                }
            }
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current sparse vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline ConstantSubVectorSparse<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorSparse<T, N>(&m_data[initial_element], number_of_elements); }
        
        /** Returns a sub-vector of the current sparse vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline SubVectorSparse<T, N> subvector(const N &initial_element, const N &number_of_elements) { return SubVectorSparse<T, N>(&m_data[initial_element], number_of_elements); }
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER VARIABLES ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the sparse vector.
        Tuple<T, N> * m_data;
        /// Number of non-zero elements of the sparse vector (i.e.\ number of elements in the <i>m_data</i> array).
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE SUB-VECTOR CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Dense sub-vector class.
    template <class T, class N = unsigned int>
    class SubVectorDense
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        SubVectorDense(void) : m_data(0), m_number_of_elements(0) {}
        /** Constructor where the array of data and the size of the sub-vector are given as parameters.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        SubVectorDense(T *data, const N &number_of_elements) : m_data(data), m_number_of_elements(number_of_elements) {}
        /** Constructor which initializes the sub-vector from a dense vector.
         *  \param[in] data dense vector.
         *  \note The sub-vector only stores a pointer to the dense vector, so that, the memory is managed by the dense vector object.
         */
        SubVectorDense(VectorDense<T, N> &data) : m_data(data.getData()), m_number_of_elements(data.size()) {}
        /** Assignation operator where the constant dense sub-vector is set from a non-constant dense vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        inline SubVectorDense<T, N>& operator=(VectorDense<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Resets the dense sub-vector.
        void clear(void)
        {
            m_data = 0;
            m_number_of_elements = 0;
        }
        /** Sets the dense sub-vector to the given array.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        void set(T *data, const N &number_of_elements)
        {
            m_data = data;
            m_number_of_elements = number_of_elements;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the dense sub-vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the elements of the dense sub-vector.
        inline const T* getData(void) const { return m_data; }
        /// Returns a pointer to the elements of the dense sub-vector.
        inline T* getData(void) { return m_data; }
        /// Returns a constant reference to the index-th element of the dense sub-vector.
        inline const T& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Returns a reference to the index-th element of the dense sub-vector.
        inline T& getData(const N &index) { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a constant reference to the index-th element of the dense sub-vector.
        inline const T& operator[](const N &index) const { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a reference to the index-th element of the dense sub-vector.
        inline T& operator[](const N &index) { return m_data[(unsigned)index]; }
        /** Sets the elements of the dense sub-vector to the given value.
         *  \param[in] value value assigned to each position of the sub-vector.
         */
        inline void setValue(const T &value) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = value; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONTENT COPY FUNCTIONS ]===================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Copies the content of a dense sub-vector to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a sparse vector to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        /** Copies the content of another dense vector to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        inline void copy(const SubVectorDense<T, N> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other.m_data[i];
        }
        /** Copies the content of a dense vector of a different data type to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a sparse sub-vector to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        /** Copies the content of a constant dense vector of a different data type to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorDense<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = (T)other[(M)i];
        }
        /** Copies the content of a constant sparse sub-vector to the current sub-vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = 0;
            for (M i = 0; i < other.size(); ++i) m_data[other[i].getSecond()] = (T)other[i].getFirst();
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current dense vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline ConstantSubVectorDense<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorDense<T, N>(&m_data[initial_element], number_of_elements); }
        
        /** Returns a sub-vector of the current dense vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline SubVectorDense<T, N> subvector(const N &initial_element, const N &number_of_elements) { return SubVectorDense<T, N>(&m_data[initial_element], number_of_elements); }
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER VARIABLES ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the dense sub-vector.
        T *m_data;
        /// Number of elements of the dense sub-vector.
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONSTANT DENSE SUB-VECTOR CLASS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Dense sub-vector class.
    template <class T, class N = unsigned int>
    class ConstantSubVectorDense
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        ConstantSubVectorDense(void) : m_data(0), m_number_of_elements(0) {}
        /** Constructor where the array of data and the size of the constant sub-vector are given as parameters.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        ConstantSubVectorDense(const T *data, const N &number_of_elements) : m_data(data), m_number_of_elements(number_of_elements) {}
        /** Constructor where the constant dense sub-vector is initialized from a dense sub-vector.
         *  \param[in] other sub-vector used to initialize the constant sub-vector.
         */
        ConstantSubVectorDense(const SubVectorDense<T, N> &other) : m_data(other.getData()), m_number_of_elements(other.size()) {}
        /** Constructor where the constant dense sub-vector is initialized from a dense vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        ConstantSubVectorDense(const VectorDense<T, N> &other) : m_data(other.getData()), m_number_of_elements(other.size()) {}
        /** Assignation operator where the constant dense sub-vector is set from a non-constant dense sub-vector.
         *  \param[in] other sub-vector used to initialize the constant sub-vector.
         */
        inline ConstantSubVectorDense<T, N>& operator=(const SubVectorDense<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        /** Assignation operator where the constant dense sub-vector is set from a non-constant dense vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        inline ConstantSubVectorDense<T, N>& operator=(const VectorDense<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Resets the dense sub-vector.
        void clear(void)
        {
            m_data = 0;
            m_number_of_elements = 0;
        }
        /** Sets the dense sub-vector to the given array.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        void set(const T *data, const N &number_of_elements)
        {
            m_data = data;
            m_number_of_elements = number_of_elements;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the dense sub-vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the elements of the dense sub-vector.
        inline const T* getData(void) const { return m_data; }
        /// Returns a constant reference to the index-th element of the dense sub-vector.
        inline const T& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a constant reference to the index-th element of the dense sub-vector.
        inline const T& operator[](const N &index) const { return m_data[(unsigned)index]; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current constant dense sub-vector.
         *  \param[in] initial_element index of the first element of the constant sub-vector.
         *  \param[in] number_of_elements number of elements in the constant sub-vector.
         */
        inline ConstantSubVectorDense<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorDense<T, N>(&m_data[initial_element], number_of_elements); }
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER VARIABLES ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the dense sub-vector.
        const T *m_data;
        /// Number of elements of the dense sub-vector.
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SPARSE SUB-VECTOR CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Sparse sub-vector class.
    template <class T, class N = unsigned int>
    class SubVectorSparse
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        SubVectorSparse(void) : m_data(0), m_number_of_elements(0) {}
        /** Constructor where the array of data and the size of the sub-vector are given as parameters.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        SubVectorSparse(Tuple<T, N> *data, const N &number_of_elements) : m_data(data), m_number_of_elements(number_of_elements) {}
        /** Assignation operator where the constant dense sub-vector is set from a non-constant sparse vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        inline SubVectorSparse<T, N>& operator=(VectorSparse<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Resets the sparse sub-vector.
        void clear(void)
        {
            m_data = 0;
            m_number_of_elements = 0;
        }
        /** Sets the sparse sub-vector to the given array.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        void set(Tuple<T, N> *data, const N &number_of_elements)
        {
            m_data = data;
            m_number_of_elements = number_of_elements;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the sparse sub-vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the data of the sparse sub-vector.
        inline const Tuple<T, N>* getData(void) const { return m_data; }
        /// Returns a pointer to the data of the sparse sub-vector.
        inline Tuple<T, N>* getData(void) { return m_data; }
        /// Returns a constant reference to the index-th element of the sparse sub-vector.
        inline const Tuple<T, N>& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Returns a reference to the index-th element of the sparse sub-vector.
        inline Tuple<T, N>& getData(const N &index) { return m_data[(unsigned)index]; }
        /// Returns a constant reference to the index-th value of the sparse sub-vector.
        inline const T& getValue(const N &index) const { return m_data[(unsigned)index].getFirst(); }
        /// Returns a reference to the index-th value of the sparse sub-vector.
        inline T& getValue(const N &index) { return m_data[(unsigned)index].getFirst(); }
        /// Returns a constant reference to the index-th index of the sparse sub-vector.
        inline const N& getIndex(const N &index) const { return m_data[(unsigned)index].getSecond(); }
        /// Returns a reference to the index-th of the sparse sub-vector.
        inline N& getIndex(const N &index) { return m_data[(unsigned)index].getSecond(); }
        /// Array subscript operator which returns a constant reference to the index-the element of the sparse sub-vector.
        inline const Tuple<T, N>& operator[](const N &index) const { return m_data[(unsigned)index]; }
        /// Array subscript operator which returns a reference to the index-the element of the sparse sub-vector.
        inline Tuple<T, N>& operator[](const N &index) { return m_data[(unsigned)index]; }
        /** Sets the values of the sparse sub-vector elements to the given value.
         *  \param[in] value value assigned to each position of the sparse sub-vector.
         */
        inline void setValue(const T &value) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setFirst(value); }
        /** Sets the index of the sparse sub-vector elements to the given value.
         *  \param[in] index index value assigned to each position of the sparse sub-vector.
         */
        inline void setIndex(const N &index) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setSecond(index); }
        /** Sets the elements of the sparse sub-vector elements to the given value and index.
         *  \param[in] value value assigned to each position of the sparse sub-vector.
         *  \param[in] index index assigned to each position of the sparse sub-vector.
         */
        inline void setValue(const T &value, const N &index) { for (N i = 0; i < m_number_of_elements; ++i) m_data[i].setData(value, index); }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONTENT COPY FUNCTIONS ]===================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Copies the content of a dense vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of a sparse sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const VectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        /** Copies the content of a dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of another sparse vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        inline void copy(const SubVectorSparse<T, N> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other.m_data[i];
        }
        /** Copies the content of a sparse vector of a different data type to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const SubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        /** Copies the content of a constant dense sub-vector to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorDense<V, M> &other)
        {
            for (M i = 0, j = 0; i < other.size(); ++i)
            {
                if (other[i] != 0)
                {
                    m_data[j].setData((T)other[i], (N)i);
                    ++j;
                }
            }
        }
        /** Copies the content of a constant sparse vector of a different data type to the current vector.
         *  \note The function does not check the dimensionality of the vectors, so that, a segmentation fault can be generated.
         */
        template <class V, class M>
        inline void copy(const ConstantSubVectorSparse<V, M> &other)
        {
            for (N i = 0; i < m_number_of_elements; ++i) m_data[i] = other[(M)i];
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current sparse vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline ConstantSubVectorSparse<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorSparse<T, N>(&m_data[initial_element], number_of_elements); }
        
        /** Returns a sub-vector of the current sparse vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline SubVectorSparse<T, N> subvector(const N &initial_element, const N &number_of_elements) { return SubVectorSparse<T, N>(&m_data[initial_element], number_of_elements); }
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER VARIABLES ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the sparse sub-vector.
        Tuple<T, N> *m_data;
        /// Number of non-zero elements of the sparse sub-vector (i.e.\ number of elements in the <i>data</i> and <i>indexes</i> arrays).
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONSTANT SPARSE SUB-VECTOR CLASS     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Sparse sub-vector class.
    template <class T, class N = unsigned int>
    class ConstantSubVectorSparse
    {
    public:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Default constructor.
        ConstantSubVectorSparse(void) : m_data(0), m_number_of_elements(0) {}
        /** Constructor where the array of data and the size of the constant sub-vector are given as parameters.
         *  \param[in] data constant array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        ConstantSubVectorSparse(const Tuple<T, N> *data, const N &number_of_elements) : m_data(data), m_number_of_elements(number_of_elements) {}
        /** Constructor where the constant sparse sub-vector is initialized from a non-constant sparse sub-vector.
         *  \param[in] other sub-vector used to initialize the constant sub-vector.
         */
        ConstantSubVectorSparse(const SubVectorSparse<T, N> &other) : m_data(other.getData()), m_number_of_elements(other.size()) {}
        /** Constructor where the constant sparse sub-vector is initialized from a non-constant sparse vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        ConstantSubVectorSparse(const VectorSparse<T, N> &other) : m_data(other.getData()), m_number_of_elements(other.size()) {}
        /** Assignation operator where the constant dense sub-vector is set from a non-constant sparse sub-vector.
         *  \param[in] other sub-vector used to initialize the constant sub-vector.
         */
        inline ConstantSubVectorSparse<T, N>& operator=(const SubVectorSparse<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        /** Assignation operator where the constant dense sub-vector is set from a non-constant sparse vector.
         *  \param[in] other vector used to initialize the constant sub-vector.
         */
        inline ConstantSubVectorSparse<T, N>& operator=(const VectorSparse<T, N> &other) { m_data = other.getData(); m_number_of_elements = other.size(); return *this; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ 'SET' FUNCTIONS WHICH RE-INITIALIZES THE CONTENT OF THE VECTOR ]===========================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Resets the sparse sub-vector.
        void clear(void)
        {
            m_data = 0;
            m_number_of_elements = 0;
        }
        /** Sets the constant sparse sub-vector to the given array.
         *  \param[in] data array with the data of the sub-vector.
         *  \param[in] number_of_elements number of elements of the sub-array.
         *  \note The sub-vector only stores a copy of the pointer, so that, the memory is not managed by the sub-vector object.
         */
        void set(const Tuple<T, N> *data, const N &number_of_elements)
        {
            m_data = data;
            m_number_of_elements = number_of_elements;
        }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ACCESS FUNCTIONS ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Returns the number of elements of the sparse sub-vector.
        inline N size(void) const { return m_number_of_elements; }
        /// Returns a constant pointer to the data of the sparse sub-vector.
        inline const Tuple<T, N>* getData(void) const { return m_data; }
        /// Returns a constant reference to the index-th element of the sparse sub-vector.
        inline const Tuple<T, N>& getData(const N &index) const { return m_data[(unsigned)index]; }
        /// Returns a constant reference to the index-th value of the sparse sub-vector.
        inline const T& getValue(const N &index) const { return m_data[(unsigned)index].getFirst(); }
        /// Returns a constant reference to the index-th index of the sparse sub-vector.
        inline const N& getIndex(const N &index) const { return m_data[(unsigned)index].getSecond(); }
        /// Array subscript operator which returns a constant reference to the index-the element of the sparse sub-vector.
        inline const Tuple<T, N>& operator[](const N &index) const { return m_data[(unsigned)index]; }
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUB-VECTOR FUNCTIONS ]=====================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Returns a constant sub-vector of the current constant sparse sub-vector.
         *  \param[in] initial_element index of the first element of the sub-vector.
         *  \param[in] number_of_elements number of elements in the sub-vector.
         */
        inline ConstantSubVectorSparse<T, N> subvector(const N &initial_element, const N &number_of_elements) const { return ConstantSubVectorSparse<T, N>(&m_data[initial_element], number_of_elements); }
        
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ MEMBER VARIABLES ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /// Array with the elements of the sparse sub-vector.
        const Tuple<T, N> *m_data;
        /// Number of non-zero elements of the sparse sub-vector (i.e.\ number of elements in the <i>data</i> and <i>indexes</i> arrays).
        N m_number_of_elements;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VECTOR OBJECT TO XML FUNCTIONS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function stores an vector encapsulated in the specified XML object.
     *  \param[in,out] parser XML object parser where the vector is going to be stored.
     *  \param[in] identifier identifier of the XML object which stores the vector.
     *  \param[in] data vector which is going to be stored in to the XML object.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline void saveVector(XmlParser &parser, const char *identifier, const VECTOR<T, N> &data)
    {
        parser.openTag(identifier);
        parser.setAttribute("Size", data.size());
        parser.addChildren();
        writeToBase64Array(parser, data.getData(), data.size());
        parser.closeTag();
    }
    
    /** This function loads a vector from the specified XML object.
     *  \param[in,out] parser XML object where the vector is stored.
     *  \param[in] identifier identifier of the XML object which encapsulates the vector.
     *  \param[out] data vector where the resulting vector is stored.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline void loadVector(XmlParser &parser, const char *identifier, VECTOR<T, N> &data)
    {
        if (parser.isTagIdentifier(identifier))
        {
            N size;
            size = parser.getAttribute("Size");
            data.set(size);
            
            while (!(parser.isTagIdentifier(identifier) && parser.isCloseTag()))
            {
                if (parser.isValue()) readFromBase64Array(parser, identifier, data.getData(), size);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IO-STREAM FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Overloads the <b><<</b> operator to put the values of a dense vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const VectorDense<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b><<</b> operator to put the values of a sparse vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const VectorSparse<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b><<</b> operator to put the values of a dense sub-vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const SubVectorDense<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b><<</b> operator to put the values of a sparse sub-vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const SubVectorSparse<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b><<</b> operator to put the values of a constant dense sub-vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const ConstantSubVectorDense<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b><<</b> operator to put the values of a constant sparse sub-vector into an output stream.
    template <class T, class N>
    inline std::ostream& operator<<(std::ostream &out, const ConstantSubVectorSparse<T, N> &vector)
    {
        if (vector.size() > 0)
        {
            out << vector[0];
            for (N i = 1; i < vector.size(); ++i) out << " " << vector[i];
        }
        
        return out;
    }
    
    /// Overloads the <b>>></b> operator to get the values of a dense vector from an input stream.
    template <class T, class N>
    inline std::istream& operator>>(std::istream &in, VectorDense<T, N> &vector)
    {
        for (N i = 0; i < vector.size(); ++i) in >> vector[i];
        return in;
    }
    
    /// Overloads the <b>>></b> operator to get the values of a sparse vector from an input stream.
    template <class T, class N>
    inline std::istream& operator>>(std::istream &in, VectorSparse<T, N> &vector)
    {
        for (N i = 0; i < vector.size(); ++i) in >> vector[i];
        return in;
    }
    
    /// Overloads the <b>>></b> operator to get the values of a dense sub-vector from an input stream.
    template <class T, class N>
    inline std::istream& operator>>(std::istream &in, SubVectorDense<T, N> &vector)
    {
        for (N i = 0; i < vector.size(); ++i) in >> vector[i];
        return in;
    }
    
    /// Overloads the <b>>></b> operator to get the values of a sparse sub-vector from an input stream.
    template <class T, class N>
    inline std::istream& operator>>(std::istream &in, SubVectorSparse<T, N> &vector)
    {
        for (N i = 0; i < vector.size(); ++i) in >> vector[i];
        return in;
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

