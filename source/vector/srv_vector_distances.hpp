// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA


#ifndef __SRV_VECTOR_DISTANCES_HPP_HEADER_FILE__
#define __SRV_VECTOR_DISTANCES_HPP_HEADER_FILE__

#include "srv_vector.hpp"
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_matrix.hpp"
#include "srv_vector_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | NORM CLASSES DEFINITIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ L1 NORM CLASS DECLARATION ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class L1Norm
    {
    public:
        /** Calculates the norm of a dense vector.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class TDISTANCE>
        inline static void normPartial(const TA * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
        }
        /** Calculates the norm of a sparse vector.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class NA, class TDISTANCE>
        inline static void normPartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
        }
        /** Calculates the partial norm of a single dimension of the vector.
         *  \param[in] value value of a dimension of the vector.
         *  \param[out] dimension_norm contribution to the norm of the dimension value.
         */
        template <class TA, class TDISTANCE>
        inline static void normDimension(const TA &value, TDISTANCE &dimension_norm) { dimension_norm = srvAbs<TDISTANCE>((TDISTANCE)value); }
        /** Returns the norm of a vector by merging a set of partial results.
         *  \param[in] partial_norms array with the partial norms.
         *  \param[in] number_of_partial_norms number of elements at the array.
         *  \returns the distance joined norm of the vector.
         */
        template <class TDISTANCE>
        inline static TDISTANCE normMerge(const TDISTANCE * partial_norms, unsigned long number_of_partial_norms)
        {
            TDISTANCE current_norm = 0;
            for (unsigned long i = 0; i < number_of_partial_norms; ++i)
                current_norm += partial_norms[i];
            return current_norm;
        }
        /// Returns the actual norm of the vector.
        template <class TDISTANCE>
        inline static TDISTANCE normFinal(const TDISTANCE &current_norm) { return current_norm; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ L2 NORM CLASS DECLARATION ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class L2Norm
    {
    public:
        /** Calculates the norm of a dense vector.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class TDISTANCE>
        inline static void normPartial(const TA * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeA; ++i)
                result += (TDISTANCE)vectorA[i] * (TDISTANCE)vectorA[i];
        }
        /** Calculates the norm of a sparse vector.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class NA, class TDISTANCE>
        inline static void normPartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeA; ++i)
                result += (TDISTANCE)vectorA[i].getFirst() * (TDISTANCE)vectorA[i].getFirst();
        }
        /** Calculates the partial norm of a single dimension of the vector.
         *  \param[in] value value of a dimension of the vector.
         *  \param[out] dimension_norm contribution to the norm of the dimension value.
         */
        template <class TA, class TDISTANCE>
        inline static void normDimension(const TA &value, TDISTANCE &dimension_norm) { dimension_norm = (TDISTANCE)value * (TDISTANCE)value; }
        /** Returns the norm of a vector by merging a set of partial results.
         *  \param[in] partial_norms array with the partial norms.
         *  \param[in] number_of_partial_norms number of elements at the array.
         *  \returns the distance joined norm of the vector.
         */
        template <class TDISTANCE>
        inline static TDISTANCE normMerge(const TDISTANCE * partial_norms, unsigned long number_of_partial_norms)
        {
            TDISTANCE current_norm = 0;
            for (unsigned long i = 0; i < number_of_partial_norms; ++i)
                current_norm += partial_norms[i];
            return current_norm;
        }
        /// Returns the actual norm of the vector.
        template <class TDISTANCE>
        inline static TDISTANCE normFinal(const TDISTANCE &current_norm) { return (TDISTANCE)sqrt((double)current_norm); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ L-INFINITE CLASS DECLARATION ]=============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class LInfNorm
    {
    public:
        /** Calculates the norm of a dense vector.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class TDISTANCE>
        inline static void normPartial(const TA * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = (TDISTANCE)vectorA[0];
            for (unsigned long i = 1; i < sizeA; ++i)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]));
        }
        /** Calculates the norm of a sparse vector.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[out] result partial norm of the vector.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class NA, class TDISTANCE>
        inline static void normPartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, TDISTANCE &result)
        {
            result = (TDISTANCE)vectorA[0].getFirst();
            for (unsigned long i = 1; i < sizeA; ++i)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst()));
        }
        /** Calculates the partial norm of a single dimension of the vector.
         *  \param[in] value value of a dimension of the vector.
         *  \param[out] dimension_norm contribution to the norm of the dimension value.
         */
        template <class TA, class TDISTANCE>
        inline static void normDimension(const TA &value, TDISTANCE &dimension_norm) { dimension_norm = srvAbs<TDISTANCE>((TDISTANCE)value); }
        /** Returns the norm of a vector by merging a set of partial results.
         *  \param[in] partial_norms array with the partial norms.
         *  \param[in] number_of_partial_norms number of elements at the array.
         *  \returns the distance joined norm of the vector.
         */
        template <class TDISTANCE>
        inline static TDISTANCE normMerge(const TDISTANCE * partial_norms, unsigned long number_of_partial_norms)
        {
            TDISTANCE current_norm = partial_norms[0];
            for (unsigned long i = 1; i < number_of_partial_norms; ++i)
                current_norm = srvMax<TDISTANCE>(current_norm, partial_norms[i]);
            return current_norm;
        }
        /// Returns the actual norm of the vector.
        template <class TDISTANCE>
        inline static TDISTANCE normFinal(const TDISTANCE &current_norm) { return current_norm; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Lp NORM CLASS DECLARATION ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class LpNorm
    {
    public:
        /** Calculates the norm of a dense vector.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[out] result partial norm of the vector.
         *  \param[in] order order of the p-norm.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class TDISTANCE>
        inline static void normPartial(const TA * vectorA, unsigned long sizeA, TDISTANCE &result, double order)
        {
            double current_distance = 0.0;
            for (unsigned long i = 0; i < sizeA; ++i)
                current_distance += pow(srvAbs<double>((double)vectorA[i]), order);
            result = (TDISTANCE)current_distance;
        }
        /** Calculates the norm of a sparse vector.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[out] result partial norm of the vector.
         *  \param[in] order order of the p-norm.
         *  \note this function does not check the dimensionality of the vector.
         */
        template <class TA, class NA, class TDISTANCE>
        inline static void normPartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, TDISTANCE &result, double order)
        {
            double current_distance = 0.0;
            for (unsigned long i = 0; i < sizeA; ++i)
                current_distance += pow(srvAbs<double>((double)vectorA[i].getFirst()), order);
            result = (TDISTANCE)current_distance;
        }
        /** Calculates the partial norm of a single dimension of the vector.
         *  \param[in] value value of a dimension of the vector.
         *  \param[out] dimension_norm contribution to the norm of the dimension value.
         *  \param[in] order order of the p-norm.
         */
        template <class TA, class TDISTANCE>
        inline static void normDimension(const TA &value, TDISTANCE &dimension_norm, double order) { dimension_norm = (TDISTANCE)pow(srvAbs<double>((double)value), order); }
        /** Returns the norm of a vector by merging a set of partial results.
         *  \param[in] partial_norms array with the partial norms.
         *  \param[in] number_of_partial_norms number of elements at the array.
         *  \returns the distance joined norm of the vector.
         */
        template <class TDISTANCE>
        inline static TDISTANCE normMerge(const TDISTANCE * partial_norms, unsigned long number_of_partial_norms)
        {
            TDISTANCE current_norm = 0;
            for (unsigned long i = 0; i < number_of_partial_norms; ++i)
                current_norm += partial_norms[i];
            return current_norm;
        }
        /// Returns the actual norm of the vector.
        template <class TDISTANCE>
        inline static TDISTANCE normFinal(const TDISTANCE &current_norm, double order) { return (TDISTANCE)pow((double)current_norm, 1.0 / order); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | NORMAL CLASS DECLARATION             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class VectorNorm
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        VectorNorm(void) : m_normal_identifier(EUCLIDEAN_NORM), m_order(2.0) {}
        /// Constructor where the identifier of the norm is given as a parameter.
        VectorNorm(NORMAL_IDENTIFIER normal_identifier) : m_normal_identifier(normal_identifier), m_order(2.0) {}
        /// Constructor where the identifier and the order of the norm are given as parameters.
        VectorNorm(NORMAL_IDENTIFIER normal_identifier, double order) : m_normal_identifier(normal_identifier), m_order(order) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the identifier of the norm.
        inline NORMAL_IDENTIFIER getIdentifier(void) const { return m_normal_identifier; }
        /// Returns the order of the norm.
        inline double getOrder(void) const { return m_order; }
        /// Sets the order of the norm.
        inline void setOrder(double order) { m_order = order; }
        /// Sets the identifier of the norm.
        inline void set(NORMAL_IDENTIFIER identifier) { m_normal_identifier = identifier; }
        /// Sets the identifier and the order of the norm.
        inline void set(NORMAL_IDENTIFIER identifier, double order) { m_normal_identifier = identifier; m_order = order; }
        
        // -[ Single vector normalization functions ]------------------------------------------------------------------------------------------------
        /** Calculates the norm of the given vector.
         *  \param[in] vector input vector.
         *  \param[out] vector_normal the norm of the input vector.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void norm(const VECTOR<TA, NA> &vector, TDISTANCE &vector_normal) const;
        
        /** Calculates the norm of the given vector.
         *  \param[in] vector input vector.
         *  \returns the norm of the input vector.
         */
        template <template <class, class> class VECTOR, class TA, class NA>
        inline double norm(const VECTOR<TA, NA> &vector) const { double vector_normal; norm(vector, vector_normal); return vector_normal; }
        
        /** Calculates the partial norm of the given vector. For instance, this function only returns
         *  the sum of squared vector values for the Euclidean norm.
         *  \param[in] vector input vector.
         *  \param[out] partial_norm the partial norm of the input vector.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void normPartial(const VECTOR<TA, NA> &vector, TDISTANCE &partial_norm) const;
        
        /** Calculates the partial norm of a single dimension of the vector.
         *  \param[in] value value of a dimension of the vector.
         *  \param[out] dimension_norm contribution to the norm of the dimension value.
         */
        template <class T, class TDISTANCE>
        void normDimension(const T &value, TDISTANCE &dimension_norm) const;
        
        /** Calculates the partial norm of the given vector. For instance, this function only returns
         *  the sum of squared vector values for the Euclidean norm.
         *  \param[in] vector input vector.
         *  \returns the partial norm of the input vector.
         */
        template <template <class, class> class VECTOR, class TA, class NA>
        inline double normPartial(const VECTOR<TA, NA> &vector) const { double partial_norm; normPartial(vector, partial_norm); return partial_norm; }
        
        /** Merges the norm calculated from different vector parts.
         *  \param[in] partial_norms array with the partial norms.
         *  \param[in] number_of_partial_norms number of elements of the array.
         *  \returns the merged norm.
         */
        template <class TDISTANCE>
        TDISTANCE normMerge(const TDISTANCE * partial_norms, unsigned int number_of_partial_norms) const;
        
        /** Returns the final norm of the vectors. For instance, it calculates the square root of the given norm for the Euclidean norm.
         *  \param[in] merged_norm input norm.
         *  \returns the final norm of the vector.
         */
        template <class TDISTANCE>
        TDISTANCE normFinal(const TDISTANCE &merged_norm) const;
        
        // -[ Multiple vector normalization functions ]----------------------------------------------------------------------------------------------
        /** Calculates the norm of the given set of vectors.
         *  \param[in] vector array with the input set of vectors.
         *  \param[out] vector_normal array with the norm of each input vector.
         *  \param[in] number_of_vectors number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the norms.
         *  \note this function does not allocates memory for the destination norm array.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void norm(const VECTOR<TA, NA> * vector, TDISTANCE * vector_normal, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        /** Calculates the norm of the given set of vectors.
         *  \param[in] vector array of pointers to the input set of vectors.
         *  \param[out] vector_normal array with the norm of each input vector.
         *  \param[in] number_of_vectors number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the norms.
         *  \note this function does not allocates memory for the destination norm array.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void norm(VECTOR<TA, NA> const * const * vector, TDISTANCE * vector_normal, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        /** Calculates the partial norm of the given set of vectors.
         *  \param[in] vector array with the input set of vectors.
         *  \param[out] partial_normal array with the partial norm of each input vector.
         *  \param[in] number_of_vectors number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the partial norms.
         *  \note this function does not allocates memory for the destination norm array.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void normPartial(const VECTOR<TA, NA> * vector, TDISTANCE * partial_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        /** Calculates the partial norm of the given set of vectors.
         *  \param[in] vector array of pointers to the input set of vectors.
         *  \param[out] partial_normal array with the partial norm of each input vector.
         *  \param[in] number_of_vectors number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the partial norms.
         *  \note this function does not allocates memory for the destination norm array.
         */
        template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
        void normPartial(VECTOR<TA, NA> const * const * vector, TDISTANCE * partial_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        /** Merges the norm calculated from different parts of the vectors into a single norm for each vector.
         *  \param[in] partial_norms an array of pointers to the arrays with vector partial norms.
         *  \param[out] merged_norm an array with the resulting merged norms for each vector.
         *  \param[in] number_of_partial_norms number of partial norms of each partial norm array.
         *  \param[in] number_of_vectors number of vectors in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently merge the norm estimation.
         *  \note the <b>partial_norms</b> array has <b>number_of_partial_norms</b> pointers to the arrays with <b>number_of_vectors</b> norms.
         */
        template <class TDISTANCE>
        void normMerge(TDISTANCE const * const * partial_norms, TDISTANCE * merged_norm, unsigned int number_of_partial_norms, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        /** Calculates the final norm (or actual norm) of each vector norm.
         *  \param[in] merged_norm array with partial and merged norms of the vectors.
         *  \param[out] final_norm array with the final norm of each vector.
         *  \param[in] number_of_vectors number of elements of each array.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the final norm of the vectors.
         */
        template <class TDISTANCE>
        void normFinal(const TDISTANCE * merged_norm, TDISTANCE * final_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of the norm into an XML object.
        void convertToXML(XmlParser &parser) const
        {
            parser.openTag("Vector_Norm");
            parser.setAttribute("Identifier", (int)m_normal_identifier);
            parser.setAttribute("Order", m_order);
            parser.closeTag();
        }
        /// Retrieves the information of the norm from an XML object.
        void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Vector_Norm"))
            {
                m_normal_identifier = (NORMAL_IDENTIFIER)((int)parser.getAttribute("Identifier"));
                m_order = parser.getAttribute("Order");
                
                while (!(parser.isTagIdentifier("Vector_Norm") && parser.isCloseTag())) parser.getNext();
                parser.getNext();
            }
        }
        
    protected:
        /// Identifier of the normal.
        NORMAL_IDENTIFIER m_normal_identifier;
        /// Order of the norm.
        double m_order;
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ NORM CLASS IMPLEMENTATION ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::norm(const VECTOR<TA, NA> &vector, TDISTANCE &vector_normal) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            L1Norm::normPartial(vector.getData(), (unsigned long)vector.size(), vector_normal);
            vector_normal = L1Norm::normFinal(vector_normal);
            break;
        case EUCLIDEAN_NORM:
            L2Norm::normPartial(vector.getData(), (unsigned long)vector.size(), vector_normal);
            vector_normal = L2Norm::normFinal(vector_normal);
            break;
        case MAXIMUM_NORM:
            LInfNorm::normPartial(vector.getData(), (unsigned long)vector.size(), vector_normal);
            vector_normal = LInfNorm::normFinal(vector_normal);
            break;
        case P_NORM:
            LpNorm::normPartial(vector.getData(), (unsigned long)vector.size(), vector_normal, m_order);
            vector_normal = LpNorm::normFinal(vector_normal, m_order);
            break;
        default:
            throw Exception("Norm not yet implemented.");
        }
    }
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::normPartial(const VECTOR<TA, NA> &vector, TDISTANCE &partial_norm) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            return L1Norm::normPartial(vector.getData(), (unsigned long)vector.size(), partial_norm);
        case EUCLIDEAN_NORM:
            return L2Norm::normPartial(vector.getData(), (unsigned long)vector.size(), partial_norm);
        case MAXIMUM_NORM:
            return LInfNorm::normPartial(vector.getData(), (unsigned long)vector.size(), partial_norm);
        case P_NORM:
            return LpNorm::normPartial(vector.getData(), (unsigned long)vector.size(), partial_norm, m_order);
        default:
            throw Exception("Norm not yet implemented.");
        }
    }
    
    template <class T, class TDISTANCE>
    void VectorNorm::normDimension(const T &value, TDISTANCE &dimension_norm) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            L1Norm::normDimension(value, dimension_norm);
            break;
        case EUCLIDEAN_NORM:
            L2Norm::normDimension(value, dimension_norm);
            break;
        case MAXIMUM_NORM:
            LInfNorm::normDimension(value, dimension_norm);
            break;
        case P_NORM:
            LpNorm::normDimension(value, dimension_norm, m_order);
            break;
        default:
            throw Exception("Norm not yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorNorm::normMerge(const TDISTANCE * partial_norms, unsigned int number_of_partial_norms) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            return L1Norm::normMerge(partial_norms, number_of_partial_norms);
        case EUCLIDEAN_NORM:
            return L2Norm::normMerge(partial_norms, number_of_partial_norms);
        case MAXIMUM_NORM:
            return LInfNorm::normMerge(partial_norms, number_of_partial_norms);
        case P_NORM:
            return LpNorm::normMerge(partial_norms, number_of_partial_norms, m_order);
        default:
            throw Exception("Norm not yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorNorm::normFinal(const TDISTANCE &merged_norm) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            return L1Norm::normFinal(merged_norm);
        case EUCLIDEAN_NORM:
            return L2Norm::normFinal(merged_norm);
        case MAXIMUM_NORM:
            return LInfNorm::normMerge(merged_norm);
        case P_NORM:
            return LpNorm::normMerge(merged_norm);
        default:
            throw Exception("Norm not yet implemented.");
        }
    }
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::norm(const VECTOR<TA, NA> * vector, TDISTANCE * vector_normal, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    L1Norm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), vector_normal[i]);
                    vector_normal[i] = L1Norm::normFinal(vector_normal[i]);
                }
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    L2Norm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), vector_normal[i]);
                    vector_normal[i] = L2Norm::normFinal(vector_normal[i]);
                }
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    LInfNorm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), vector_normal[i]);
                    vector_normal[i] = LInfNorm::normFinal(vector_normal[i]);
                }
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    LpNorm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), vector_normal[i], m_order);
                    vector_normal[i] = LpNorm::normFinal(vector_normal[i], m_order);
                }
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::norm(VECTOR<TA, NA> const * const * vector, TDISTANCE * vector_normal, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    L1Norm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), vector_normal[i]);
                    vector_normal[i] = L1Norm::normFinal(vector_normal[i]);
                }
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    L2Norm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), vector_normal[i]);
                    vector_normal[i] = L2Norm::normFinal(vector_normal[i]);
                }
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    LInfNorm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), vector_normal[i]);
                    vector_normal[i] = LInfNorm::normFinal(vector_normal[i]);
                }
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                {
                    LpNorm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), vector_normal[i], m_order);
                    vector_normal[i] = LpNorm::normFinal(vector_normal[i], m_order);
                }
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::normPartial(const VECTOR<TA, NA> * vector, TDISTANCE * partial_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    L1Norm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), partial_norm[i]);
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    L2Norm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), partial_norm[i]);
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    LInfNorm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), partial_norm[i]);
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    LpNorm::normPartial(vector[i].getData(), (unsigned long)vector[i].size(), partial_norm[i], m_order);
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTOR, class TA, class NA, class TDISTANCE>
    void VectorNorm::normPartial(VECTOR<TA, NA> const * const * vector, TDISTANCE * partial_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    L1Norm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), partial_norm[i]);
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    L2Norm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), partial_norm[i]);
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    LInfNorm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), partial_norm[i]);
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    LpNorm::normPartial(vector[i]->getData(), (unsigned long)vector[i]->size(), partial_norm[i], m_order);
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    void VectorNorm::normMerge(TDISTANCE const * const * partial_norms, TDISTANCE * merged_norm, unsigned int number_of_partial_norms, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                    merged_norm[i] = partial_norms[0][i];
                for (unsigned int j = 1; j < number_of_partial_norms; ++j)
                    for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                        merged_norm[i] = L1Norm::normMerge(merged_norm[i], partial_norms[j][i]);
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                    merged_norm[i] = partial_norms[0][i];
                for (unsigned int j = 1; j < number_of_partial_norms; ++j)
                    for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                        merged_norm[i] = L2Norm::normMerge(merged_norm[i], partial_norms[j][i]);
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                    merged_norm[i] = partial_norms[0][i];
                for (unsigned int j = 1; j < number_of_partial_norms; ++j)
                    for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                        merged_norm[i] = LInfNorm::normMerge(merged_norm[i], partial_norms[j][i]);
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                    merged_norm[i] = partial_norms[0][i];
                for (unsigned int j = 1; j < number_of_partial_norms; ++j)
                    for (unsigned int i = thread_identifier; i < number_of_vectors; i += number_of_threads)
                        merged_norm[i] = LpNorm::normMerge(merged_norm[i], partial_norms[j][i], m_order);
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    void VectorNorm::normFinal(const TDISTANCE * merged_norm, TDISTANCE * final_norm, unsigned int number_of_vectors, unsigned int number_of_threads) const
    {
        switch (m_normal_identifier)
        {
        case MANHATTAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    final_norm[i] = L1Norm::normFinal(merged_norm[i]);
            }
            break;
        case EUCLIDEAN_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    final_norm[i] = L2Norm::normFinal(merged_norm[i]);
            }
            break;
        case MAXIMUM_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    final_norm[i] = LInfNorm::normFinal(merged_norm[i]);
            }
            break;
        case P_NORM:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_vectors; i += number_of_threads)
                    final_norm[i] = LpNorm::normFinal(merged_norm[i], m_order);
            }
            break;
        default:
            throw Exception("The selected norm has not been yet implemented.");
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DISTANCE CLASSES DEFINITIONS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ MINKOWSKI CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Minkowski distance between two vector arrays.
    class DistanceMinkowski
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[in] order order of the Minkowski distance.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, double order, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            double current_distance = 0.0;
            for (unsigned long i = 0; i < size; ++i)
                current_distance += pow(srvAbs<double>((double)vectorA[i] - (double)vectorB[i]), order);
            result = (TDISTANCE)current_distance;
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[in] order order of the Minkowski distance.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, double order, TDISTANCE &result)
        {
            unsigned long i, j;
            double current_distance = 0.0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    current_distance += pow(srvAbs<double>((double)vectorA[i] - (double)vectorB[j].getFirst()), order);
                    ++i;
                    ++j;
                }
                else
                {
                    current_distance += pow(srvAbs<double>((double)vectorA[i]), order);
                    ++i;
                }
            }
            for (; i < sizeA; ++i)
                current_distance += pow(srvAbs<double>((double)vectorA[i]), order);
            for (; j < sizeB; ++j)
                current_distance += pow(srvAbs<double>((double)vectorB[j].getFirst()), order);
            result = (TDISTANCE)current_distance;
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[in] order order of the Minkowski distance.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, double order, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, order, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[in] order order of the Minkowski distance.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, double order, TDISTANCE &result)
        {
            double current_distance = 0.0;
            unsigned long i, j;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    current_distance += pow(srvAbs<double>((double)vectorA[i].getFirst() - (double)vectorB[j].getFirst()), order);
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    current_distance += pow(srvAbs<double>((double)vectorA[i].getFirst()), order);
                    ++i;
                }
                else
                {
                    current_distance += pow(srvAbs<double>((double)vectorB[j].getFirst()), order);
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                current_distance += pow(srvAbs<double>((double)vectorA[i].getFirst()), order);
            for (; j < sizeB; ++j)
                current_distance += pow(srvAbs<double>((double)vectorB[j].getFirst()), order);
            
            result = (TDISTANCE)current_distance;
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         *  \param[in] order order of the Minkowski distance.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance, double order)
        {
            dimension_distance = (TDISTANCE)pow((double)srvAbs<TDISTANCE>((TDISTANCE)first_value - (TDISTANCE)second_value), order);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance, double order) { return (TDISTANCE)pow((double)current_distance, 1.0 / order); }
        /// Returns the inverse of the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance, double order) { return (TDISTANCE)pow((double)current_distance, order); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MANHATTAN CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceManhattan
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[i]);
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[j].getFirst());
                    ++i;
                    ++j;
                }
                else
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
                    ++i;
                }
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst() - (TDISTANCE)vectorB[j].getFirst());
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
                    ++i;
                }
                else
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = srvAbs<TDISTANCE>((TDISTANCE)first_value - (TDISTANCE)second_value);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return current_distance; }
        /// Returns the inverse of the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ EUCLIDEAN CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceEuclidean
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
            {
                const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[i];
                result += difference * difference;
            }
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB); ++i)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[j].getFirst();
                    result += difference * difference;
                    ++j;
                }
                else result += (TDISTANCE)vectorA[i] * (TDISTANCE)vectorA[i];
            }
            for (; i < sizeA; ++i)
                result += (TDISTANCE)vectorA[i] * (TDISTANCE)vectorA[i];
            for (; j < sizeB; ++j)
                result += (TDISTANCE)vectorB[j].getFirst() * (TDISTANCE)vectorB[j].getFirst();
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i].getFirst() - (TDISTANCE)vectorB[j].getFirst();
                    result += difference * difference;
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    result += (TDISTANCE)vectorA[i].getFirst() * (TDISTANCE)vectorA[i].getFirst();
                    ++i;
                }
                else
                {
                    result += (TDISTANCE)vectorB[j].getFirst() * (TDISTANCE)vectorB[j].getFirst();
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                result += (TDISTANCE)vectorA[i].getFirst() * (TDISTANCE)vectorA[i].getFirst();
            for (; j < sizeB; ++j)
                result += (TDISTANCE)vectorB[j].getFirst() * (TDISTANCE)vectorB[j].getFirst();
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            const TDISTANCE difference = (TDISTANCE)first_value - (TDISTANCE)second_value;
            dimension_distance = difference * difference;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)sqrt((double)current_distance); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance * current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CHEBYSHEV CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Chebyshev distance between two vector arrays.
    class DistanceChebyshev
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = srvAbs<TDISTANCE>((TDISTANCE)vectorA[0] - (TDISTANCE)vectorB[0]);
            for (unsigned long i = 1; i < size; ++i)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[i]));
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            if ((unsigned long)vectorB[0].getSecond() == 0)
            {
                result = srvAbs<TDISTANCE>((TDISTANCE)vectorA[0] - (TDISTANCE)vectorB[0].getFirst());
                j = 1;
            }
            else
            {
                result = srvAbs<TDISTANCE>((TDISTANCE)vectorA[0]);
                j = 0;
            }
            for (i = 1; (i < sizeA) && (j < sizeB); ++i)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[j].getFirst()));
                    ++j;
                }
                else result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]));
            }
            for (; i < sizeA; ++i)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]));
            for (; j < sizeB; ++j)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst()));
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            if ((unsigned long)vectorA[0].getSecond() == (unsigned long)vectorB[0].getSecond())
            {
                result = srvAbs<TDISTANCE>((TDISTANCE)vectorA[0].getFirst() - (TDISTANCE)vectorB[0].getFirst());
                i = j = 1;
            }
            else if ((unsigned long)vectorA[0].getSecond() < (unsigned long)vectorB[0].getSecond())
            {
                result = srvAbs<TDISTANCE>((TDISTANCE)vectorA[0].getFirst());
                i = 1;
                j = 0;
            }
            else
            {
                result = srvAbs<TDISTANCE>((TDISTANCE)vectorB[0].getFirst());
                i = 0;
                j = 1;
            }
            for (; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst() - (TDISTANCE)vectorB[j].getFirst()));
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst()));
                    ++i;
                }
                else
                {
                    result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst()));
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst()));
            for (; j < sizeB; ++j)
                result = srvMax<TDISTANCE>(result, srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst()));
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = srvAbs<TDISTANCE>((TDISTANCE)first_value - (TDISTANCE)second_value);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = partial_distances[0];
            for (unsigned long i = 1; i < number_of_partial_distances; ++i)
                current_distance = srvMax<TDISTANCE>(current_distance, partial_distances[i]);
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return srvMax<TDISTANCE>(first, second); }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return current_distance; }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ COSINE CLASS DECLARATION ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceCosine
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
                result += (TDISTANCE)vectorA[i] * (TDISTANCE)vectorB[i];
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long /* sizeA */, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeB; ++i)
                result += (TDISTANCE)vectorA[vectorB[i].getSecond()] * (TDISTANCE)vectorB[i].getFirst();
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result += (TDISTANCE)vectorA[i].getFirst() * (TDISTANCE)vectorB[j].getFirst();
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond()) ++i;
                else ++j;
            }
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = (TDISTANCE)first_value * (TDISTANCE)second_value;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return 2 - 2 * current_distance; }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return 1 - current_distance / 2; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GEODESIC CLASS DECLARATION ]===============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceGeodesic
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
                result += (TDISTANCE)vectorA[i] * (TDISTANCE)vectorB[i];
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long /* sizeA */, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeB; ++i)
                result += (TDISTANCE)vectorA[vectorB[i].getSecond()] * (TDISTANCE)vectorB[i].getFirst();
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result += (TDISTANCE)vectorA[i].getFirst() * (TDISTANCE)vectorB[j].getFirst();
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond()) ++i;
                else ++j;
            }
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = (TDISTANCE)first_value * (TDISTANCE)second_value;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)acos((double)current_distance); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return (TDISTANCE)cos((double)current_distance); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CANBERRA CLASS DECLARATION ]===============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceCanberra
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
            {
                const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[i];
                const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[i]);
                if (mean > 0)
                    result += srvAbs<TDISTANCE>(difference) / mean;
            }
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB); ++i)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[j].getFirst();
                    const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    if (mean > 0)
                        result += srvAbs<TDISTANCE>(difference) / mean;
                    ++j;
                }
                else if (vectorA[i] != 0) ++result;
            }
            for (; i < sizeA; ++i)
                if (vectorA[i] != 0) ++result;
            for (; j < sizeB; ++j)
                if (vectorB[j].getFirst() != 0) ++result;
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i].getFirst() - (TDISTANCE)vectorB[j].getFirst();
                    const TDISTANCE mean = (srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst()) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst()));
                    if (mean > 0)
                        result += srvAbs<TDISTANCE>(difference) / mean;
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    ++result;
                    ++i;
                }
                else
                {
                    ++result;
                    ++j;
                }
            }
            result += (TDISTANCE)(sizeA - i);
            result += (TDISTANCE)(sizeB - j);
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            const TDISTANCE difference = (TDISTANCE)first_value - (TDISTANCE)second_value;
            const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)first_value) - srvAbs<TDISTANCE>((TDISTANCE)second_value);
            if (mean > 0)
                dimension_distance = srvAbs<TDISTANCE>(difference) / mean;
            else dimension_distance = 0;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return current_distance; }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CHISQUARE CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceChisquare
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
            {
                const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[i];
                const TDISTANCE mean = (srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[i]));
                if (mean > 0)
                    result += (difference * difference) / mean;
            }
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB); ++i)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i] - (TDISTANCE)vectorB[j].getFirst();
                    const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    if (mean > 0)
                        result += (difference * difference) / mean;
                    ++j;
                }
                else result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    const TDISTANCE difference = (TDISTANCE)vectorA[i].getFirst() - (TDISTANCE)vectorB[j].getFirst();
                    const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst()) + srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    if (mean > 0)
                        result += (difference * difference) / mean;
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
                    ++i;
                }
                else
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            const TDISTANCE difference = (TDISTANCE)first_value - (TDISTANCE)second_value;
            const TDISTANCE mean = srvAbs<TDISTANCE>((TDISTANCE)first_value) - srvAbs<TDISTANCE>((TDISTANCE)second_value);
            if (mean > 0)
                dimension_distance = (difference * difference) / mean;
            else dimension_distance = 0;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return current_distance; }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HISTOGRAM INTERSECTION CLASS DECLARATION ]=================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceIntersection
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
                result += srvMin<TDISTANCE>((TDISTANCE)vectorA[i], (TDISTANCE)vectorB[i]);
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long /* sizeA */, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeB; ++i)
                result += srvMin<TDISTANCE>((TDISTANCE)vectorA[vectorB[i].getSecond()], (TDISTANCE)vectorB[i].getFirst());
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result += srvMin<TDISTANCE>((TDISTANCE)vectorA[i].getFirst(), (TDISTANCE)vectorB[j].getFirst());
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond()) ++i;
                else ++j;
            }
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = srvMin<TDISTANCE>((TDISTANCE)first_value, (TDISTANCE)second_value);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)(1 - (double)current_distance); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return (TDISTANCE)(1 - (double)current_distance); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ BHATTACHARYYA CLASS DECLARATION ]==========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceBhattacharyya
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
                result += (TDISTANCE)sqrt((double)vectorA[i] * (double)vectorB[i]);
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long /* sizeA */, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0; i < sizeB; ++i)
                result += (TDISTANCE)sqrt((double)vectorA[vectorB[i].getSecond()] * (double)vectorB[i].getFirst());
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            result = 0;
            for (unsigned long i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    result += (TDISTANCE)sqrt((double)vectorA[i].getFirst() * (double)vectorB[j].getFirst());
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond()) ++i;
                else ++j;
            }
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            dimension_distance = (TDISTANCE)sqrt((double)first_value * (double)second_value);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)sqrt(1.0 - (double)current_distance); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return 1 - current_distance * current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HELLINGER CLASS DECLARATION ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceHellinger
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            result = 0;
            for (unsigned long i = 0; i < size; ++i)
            {
                const double difference = sqrt((double)vectorA[i]) - sqrt((double)vectorB[i]);
                result += (TDISTANCE)(difference * difference);
            }
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB); ++i)
            {
                if ((unsigned long)vectorB[j].getSecond() == i)
                {
                    const double difference = sqrt((double)vectorA[i]) - sqrt((double)vectorB[j].getFirst());
                    result += (TDISTANCE)(difference * difference);
                    ++j;
                }
                else result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i]);
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result) { distancePartial(vectorB, sizeB, vectorA, sizeA, result); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result)
        {
            unsigned long i, j;
            
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    const double difference = sqrt((double)vectorA[i].getFirst()) - sqrt((double)vectorB[j].getFirst());
                    result += (TDISTANCE)(difference * difference);
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
                    ++i;
                }
                else
                {
                    result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorA[i].getFirst());
            for (; j < sizeB; ++j)
                result += srvAbs<TDISTANCE>((TDISTANCE)vectorB[j].getFirst());
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance)
        {
            const double difference = sqrt((double)first_value) - sqrt((double)second_value);
            dimension_distance = (TDISTANCE)(difference * difference);
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)(sqrt((double)current_distance) / 1.414213562); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return 2 * current_distance * current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MAHALANOBIS CLASS DECLARATION ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Calculates the Manhattan distance between two vector arrays.
    class DistanceMahalanobis
    {
    public:
        /** Calculates the distance between two dense vector arrays.
         *  \param[in] vectorA pointer to the first dense vector array.
         *  \param[in] sizeA number of elements in first dense array.
         *  \param[in] vectorB pointer to the second dense vector array.
         *  \param[in] sizeB number of elements in second dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \param[in] covariance_matrix covariance matrix of the Mahalanobis distance.
         *  \param[in] difference_vector auxiliary vector to calculate the difference between the input vectors.
         *  \param[in] multiplication_vector auxiliary vector which stores the product between the difference vector and the covariance matrix.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result, const Matrix<double> &covariance_matrix, VectorDense<double> &difference_vector, VectorDense<double> &multiplication_vector)
        {
            const unsigned long size = srvMax<unsigned long>(sizeA, sizeB);
            difference_vector.setValue(0.0);
            for (unsigned long i = 0; i < size; ++i)
                difference_vector[(unsigned int)i] = (double)vectorA[i] - (double)vectorB[i];
            MatrixVectorMultiplication(covariance_matrix, difference_vector, multiplication_vector);
            result = 0;
            for (unsigned int i = 0; i < multiplication_vector.size(); ++i)
                result += (TDISTANCE)(difference_vector[i] * multiplication_vector[i]);
        }
        /** Calculates the distance between a dense vector array and a sparse vector array.
         *  \param[in] vectorA pointer to the dense vector array.
         *  \param[in] sizeA number of elements of the dense array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \param[in] covariance_matrix covariance matrix of the Mahalanobis distance.
         *  \param[in] difference_vector auxiliary vector to calculate the difference between the input vectors.
         *  \param[in] multiplication_vector auxiliary vector which stores the product between the difference vector and the covariance matrix.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const TA * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result, const Matrix<double> &covariance_matrix, VectorDense<double> &difference_vector, VectorDense<double> &multiplication_vector)
        {
            for (unsigned int i = 0; i < (unsigned int)sizeA; ++i)
                difference_vector[i] = (double)vectorA[i];
            for (unsigned int i = (unsigned int)sizeA; i < difference_vector.size(); ++i)
                difference_vector[i] = 0.0;
            for (unsigned int i = 0; i < sizeB; ++i)
                difference_vector[(unsigned int)vectorB[i].getSecond()] -= (double)vectorB[i].getFirst();
            MatrixVectorMultiplication(covariance_matrix, difference_vector, multiplication_vector);
            result = 0;
            for (unsigned int i = 0; i < multiplication_vector.size(); ++i)
                result += (TDISTANCE)(difference_vector[i] * multiplication_vector[i]);
        }
        /** Calculates the distance between a sparse vector array and a dense vector array.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the dense vector array.
         *  \param[in] sizeB number of elements of the dense array.
         *  \param[out] distance the partial distance between the vectors.
         *  \param[in] covariance_matrix covariance matrix of the Mahalanobis distance.
         *  \param[in] difference_vector auxiliary vector to calculate the difference between the input vectors.
         *  \param[in] multiplication_vector auxiliary vector which stores the product between the difference vector and the covariance matrix.
         *  \note this function does not check the dimensionality of the vectors.
         */
        template <class TA, class NA, class TB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const TB * vectorB, unsigned long sizeB, TDISTANCE &result, const Matrix<double> &covariance_matrix, VectorDense<double> &difference_vector, VectorDense<double> &multiplication_vector) { distancePartial(vectorB, sizeB, vectorA, sizeA, result, covariance_matrix, difference_vector, multiplication_vector); }
        /** Calculates the distance between two sparse vector arrays.
         *  \param[in] vectorA pointer to the sparse vector array.
         *  \param[in] sizeA number of elements of the sparse array.
         *  \param[in] vectorB pointer to the sparse vector array.
         *  \param[in] sizeB number of elements of the sparse array.
         *  \param[out] distance the partial distance between the vectors.
         *  \param[in] covariance_matrix covariance matrix of the Mahalanobis distance.
         *  \param[in] difference_vector auxiliary vector to calculate the difference between the input vectors.
         *  \param[in] multiplication_vector auxiliary vector which stores the product between the difference vector and the covariance matrix.
         */
        template <class TA, class NA, class TB, class NB, class TDISTANCE>
        inline static void distancePartial(const Tuple<TA, NA> * vectorA, unsigned long sizeA, const Tuple<TB, NB> * vectorB, unsigned long sizeB, TDISTANCE &result, const Matrix<double> &covariance_matrix, VectorDense<double> &difference_vector, VectorDense<double> &multiplication_vector)
        {
            unsigned long i, j;
            difference_vector.setValue(0.0);
            result = 0;
            for (i = 0, j = 0; (i < sizeA) && (j < sizeB);)
            {
                if ((unsigned long)vectorA[i].getSecond() == (unsigned long)vectorB[j].getSecond())
                {
                    difference_vector[(unsigned int)vectorA[i].getSecond()] = (double)vectorA[i].getFirst() - (double)vectorB[j].getFirst();
                    ++i;
                    ++j;
                }
                else if ((unsigned long)vectorA[i].getSecond() < (unsigned long)vectorB[j].getSecond())
                {
                    difference_vector[(unsigned int)vectorA[i].getSecond()] = (double)vectorA[i].getFirst();
                    ++i;
                }
                else
                {
                    difference_vector[(unsigned int)vectorB[j].getSecond()] = -(double)vectorB[j].getFirst();
                    ++j;
                }
            }
            for (; i < sizeA; ++i)
                difference_vector[(unsigned int)vectorA[i].getSecond()] = (double)vectorA[i].getFirst();
            for (; j < sizeB; ++j)
                difference_vector[(unsigned int)vectorB[j].getSecond()] = -(double)vectorB[j].getFirst();
            MatrixVectorMultiplication(covariance_matrix, difference_vector, multiplication_vector);
            result = 0;
            for (unsigned int k = 0; k < multiplication_vector.size(); ++k)
                result += (TDISTANCE)(difference_vector[k] * multiplication_vector[k]);
        }
        /** Calculates the partial distance between a single dimension of two vectors.
         *  \param[in] first_value value of single dimension of the first vector.
         *  \param[in] second_value value of single dimension of the second vector.
         *  \param[out] dimension_distance partial distance between the two vectors at the given dimension values.
         */
        template <class TA, class TB, class TDISTANCE>
        inline static void distanceDimension(const TA &/* first_value */, const TB &/* second_value */, TDISTANCE &dimension_distance)
        {
            // The Mahalanobis distance cannot be calculated dimension by dimension.
            dimension_distance = 0;
        }
        /** Returns the actual distance between vectors by merging a set of partial results.
         *  \param[in] partial_distances array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements at the array.
         *  \returns the distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned long number_of_partial_distances)
        {
            TDISTANCE current_distance = 0;
            for (unsigned long i = 0; i < number_of_partial_distances; ++i)
                current_distance += partial_distances[i];
            return current_distance;
        }
        /** Returns the actual distance between vector by merging two partial results.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \return merged distance between vectors.
         */
        template <class TDISTANCE>
        inline static TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) { return first + second; }
        /// Returns the normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceFinal(const TDISTANCE &current_distance) { return (TDISTANCE)sqrt((double)current_distance); }
        /// Returns the inverse normalized distance
        template <class TDISTANCE>
        inline static TDISTANCE distanceInverseFinal(const TDISTANCE &current_distance) { return current_distance * current_distance; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DISTANCE CLASS DECLARATION           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class VectorDistance
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        VectorDistance(void) :
            m_distance_identifier(EUCLIDEAN_DISTANCE),
            m_order(2.0) {}
        /// Constructor which specifies the distance method of the object.
        VectorDistance(DISTANCE_IDENTIFIER identifier) :
            m_distance_identifier(identifier),
            m_order(2.0) {}
        /// Constructor which sets the identifier and the order of the distance.
        VectorDistance(DISTANCE_IDENTIFIER identifier, double order) :
            m_distance_identifier(identifier),
            m_order(order) {}
        /// Constructor which sets the identifier and the distortion matrix of the distance.
        VectorDistance(DISTANCE_IDENTIFIER identifier, const Matrix<double> &distortion_matrix) :
            m_distance_identifier(identifier),
            m_order(2.0),
            m_distortion_matrix(distortion_matrix) {}
        /// Constructor which sets the identifier, the order and the distortion matrix of the distance.
        VectorDistance(DISTANCE_IDENTIFIER identifier, double order, const Matrix<double> &distortion_matrix) :
            m_distance_identifier(identifier),
            m_order(order),
            m_distortion_matrix(distortion_matrix) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the identifier of the distance method.
        inline DISTANCE_IDENTIFIER getIdentifier(void) const { return m_distance_identifier; }
        /// Returns the order of the distance.
        inline double getOrder(void) const { return m_order; }
        /// Sets the order of the distance.
        inline void setOrder(double order) { m_order = order; }
        /// Returns a constant reference to the distortion matrix.
        inline const Matrix<double>& getDistortionMatrix(void) const { return m_distortion_matrix; }
        /// Sets the distortion matrix of the distance.
        inline void setDistortionMatrix(const Matrix<double> &distortion_matrix) { m_distortion_matrix = distortion_matrix; }
        
        /// This function sets the distance metric.
        inline void set(DISTANCE_IDENTIFIER identifier) { m_distance_identifier = identifier; }
        /// This function sets the distance metric and the order of the distance.
        inline void set(DISTANCE_IDENTIFIER identifier, double order) { m_distance_identifier = identifier; m_order = order; }
        /// This function sets the distance metric and the distortion matrix of the distance.
        inline void set(DISTANCE_IDENTIFIER identifier, const Matrix<double> &distortion_matrix) { m_distance_identifier = identifier; m_distortion_matrix = distortion_matrix; }
        /// This function sets the distance metric, the order and the distortion matrix of the distance.
        void set(DISTANCE_IDENTIFIER identifier, double order, const Matrix<double> &distortion_matrix) { m_distance_identifier = identifier; m_order = order; m_distortion_matrix = distortion_matrix; }
        
        // -[ Single distance functions ]------------------------------------------------------------------------------------------------------------
        /** Calculates the distance between to vectors.
         *  \param[in] vectorA first vector.
         *  \param[in] vectorB second vector.
         *  \param[out] result the distance between the vectors.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distance(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, TDISTANCE &result) const;
        
        /** Calculates the distance between to vectors.
         *  \param[in] vectorA first vector.
         *  \param[in] vectorB second vector.
         *  \returns the distance between the vectors.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
        inline double distance(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) const { double result; distance(vectorA, vectorB, result); return result; }
        
        /** Calculates the partial distance between two vectors, i.e. it calculates only the distance of a part of the vectors.
         * For instance, for the Euclidean distance it returns the sum of squared differences without the square root.
         *  \param[in] vectorA first vector.
         *  \param[in] vectorB second vector.
         *  \param[out] result the distance between the vectors.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distancePartial(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, TDISTANCE &result) const;
        
        /** Calculates the partial distance between two vectors, i.e. it calculates only the distance of a part of the vectors.
         * For instance, for the Euclidean distance it returns the sum of squared differences without the square root.
         *  \param[in] vectorA first vector.
         *  \param[in] vectorB second vector.
         *  \returns the partial distance between the vectors.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
        inline double distancePartial(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) const { double result; distancePartial(vectorA, vectorB, result); return result; }
        /** Calculates the partial distance between a dimension value of two different vectors.
         *  \param[in] first_value value of the selected dimension of the first vector.
         *  \param[in] second_value value of the selected dimension of the second vector.
         *  \param[out] dimension_distance distance between the two vectors at the selected dimension.
         */
        template <class TA, class TB, class TDISTANCE>
        void distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance) const;
        
        /** Calculates the distance between two vectors from their partial distance results.
         *  \param[in] partial_distances an array with the partial distances.
         *  \param[in] number_of_partial_distances number of elements in the array.
         *  \returns the final distance between the vectors.
         */
        template <class TDISTANCE>
        TDISTANCE distanceMerge(const TDISTANCE * partial_distances, unsigned int number_of_partial_distances) const;
        
        /** Merges the distance calculate from two different partial distance function call.
         *  \param[in] first first partial distance.
         *  \param[in] second second partial distance.
         *  \returns the merged distance.
         */
        template <class TDISTANCE>
        TDISTANCE distanceMerge(const TDISTANCE &first, const TDISTANCE &second) const;
        
        /** Calculates the final distance of a part calculated distance. For instance, it calculates the square root of the distance for the
         *  Euclidean distance.
         *  \param[in] merged_distance merged partial distances.
         *  \returns the actual distance between two vectors.
         */
        template <class TDISTANCE>
        TDISTANCE distanceFinal(const TDISTANCE &merged_distance) const;
        /** Calculates the inverse of the final distance to obtain the distance originally obtained from the parts.
         *  For instance, it squares the given distance for the Euclidean distance.
         *  \param[in] final_distance actual distance between the two vectors.
         *  \returns the distance prior to calculating the final distance between two vectors.
         */
        template <class TDISTANCE>
        TDISTANCE distanceInverseFinal(const TDISTANCE &final_distance) const;
        
        // -[ Group distance functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the distance between a set of vectors.
         *  \param[in] vectorsA first array of vectors.
         *  \param[in] number_of_vectors_a number of elements in the first array of vectors.
         *  \param[in] vectorsB second array of vectors.
         *  \param[in] number_of_vectors_b number of elements in the second array of vectors.
         *  \param[out] distances array where the resulting number_of_vectors_a * number_of_vectors_b distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distance(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Calculates the distance between a set of vectors.
         *  \param[in] vectorsA first array of pointers to the vectors.
         *  \param[in] number_of_vectors_a number of elements in the first array of vectors.
         *  \param[in] vectorsB second array of pointers to the vectors.
         *  \param[in] number_of_vectors_b number of elements in the second array of vectors.
         *  \param[out] distances array where the resulting number_of_vectors_a * number_of_vectors_b distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distance(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Calculates the partial distance between a set of vectors.
         *  \param[in] vectorsA first array of vectors.
         *  \param[in] number_of_vectors_a number of elements in the first array of vectors.
         *  \param[in] vectorsB second array of vectors.
         *  \param[in] number_of_vectors_b number of elements in the second array of vectors.
         *  \param[out] distances array where the resulting number_of_vectors_a * number_of_vectors_b distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distancePartial(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Calculates the partial distance between a set of vectors.
         *  \param[in] vectorsA first array of pointers to the vectors.
         *  \param[in] number_of_vectors_a number of elements in the first array of vectors.
         *  \param[in] vectorsB second array of pointers to the vectors.
         *  \param[in] number_of_vectors_b number of elements in the second array of vectors.
         *  \param[out] distances array where the resulting number_of_vectors_a * number_of_vectors_b distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distancePartial(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Calculates the partial distance between a vector and a group of vectors.
         *  \param[in] vectorsA single vector.
         *  \param[in] vectorsB array of vectors.
         *  \param[in] number_of_vectors_b number of elements in the array of vectors.
         *  \param[out] distances array where the resulting distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distancePartial(const VECTORA<TA, NA> &vectorsA, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Calculates the partial distance between a vector and a group of vectors.
         *  \param[in] vectorsA single vector.
         *  \param[in] vectorsB array of pointers to the vectors.
         *  \param[in] number_of_vectors_b number of elements in the array of vectors.
         *  \param[out] distances array where the resulting distances are stored. This vector is not allocated by the function.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
        void distancePartial(const VECTORA<TA, NA> &vectorsA, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const;
        
        /** Merges the partial distances calculated between two sets of vectors.
         *  \param[in] partial_distances an array of pointers to the array with the partial distances.
         *  \param[out] merged_distances array with the resulting merged distances.
         *  \param[in] number_of_vectors number of distance measurements.
         *  \param[in] number_of_partial_distances number of parts for each distance.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the merged distances.
         */
        template <class TDISTANCE>
        void distanceMerge(TDISTANCE const * const * partial_distances, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_partial_distances, unsigned int number_of_threads) const;
        
        /** Calculates the final distance of a part calculated distance. For instance, it calculates the square root of the distance for the
         *  Euclidean distance.
         *  \param[in] merged_distance an array with the merged partial distances.
         *  \param[out] actual_distances an array with the resulting actual distances between the two sets of vectors.
         *  \param[in] number_of_distances number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <class TDISTANCE>
        void distanceFinal(const TDISTANCE * merged_distance, TDISTANCE * actual_distances, unsigned int number_of_distances, unsigned int number_of_threads) const;
        /** Calculates the inverse of the final distance to obtain the distance originally obtained from the parts.
         *  For instance, it squares the given distance for the Euclidean distance.
         *  \param[in] actual_distance an array with the actual distances.
         *  \param[out] merged_distances an array with the resulting distances prior calculating the final result.
         *  \param[in] number_of_distances number of elements in both arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         */
        template <class TDISTANCE>
        void distanceInverseFinal(const TDISTANCE * actual_distance, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of the distance object into an XML object.
        void convertToXML(XmlParser &parser) const
        {
            parser.openTag("Vector_Distance");
            parser.setAttribute("Identifier", (int)m_distance_identifier);
            parser.setAttribute("Order", m_order);
            if ((m_distortion_matrix.getNumberOfColumns() > 0) && (m_distortion_matrix.getNumberOfRows() > 0))
            {
                parser.addChildren();
                m_distortion_matrix.convertToXML(parser);
            }
            parser.closeTag();
        }
        
        /// Retrieves the distance object information from an XML object.
        void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Vector_Distance"))
            {
                m_distance_identifier = (DISTANCE_IDENTIFIER)((int)parser.getAttribute("Identifier"));
                m_order = parser.getAttribute("Order");
                m_distortion_matrix.set(0, 0);
                
                while (!(parser.isTagIdentifier("Vector_Distance") && parser.isCloseTag()))
                {
                    if (parser.isTagIdentifier("Matrix")) m_distortion_matrix.convertFromXML(parser);
                    else parser.getNext();
                }
                parser.getNext();
            }
        }
        
    private:
        // -[ In-line private functions for block distance calculation ]-----------------------------------------------------------------------------
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE, class DISTANCE_OBJECT>
        inline void distanceTemplate(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DISTANCE_OBJECT::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
                            distances[index] = DISTANCE_OBJECT::distanceFinal(distances[index]);
                        }
                    }
                }
            }
        }
        
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE, class DISTANCE_OBJECT>
        inline void distanceTemplate(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DISTANCE_OBJECT::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index]);
                            distances[index] = DISTANCE_OBJECT::distanceFinal(distances[index]);
                        }
                    }
                }
            }
        }
        
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE, class DISTANCE_OBJECT>
        inline void distancePartialTemplate(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
        {
            if (number_of_threads > 0)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                            if (index % number_of_threads == thread_identifier)
                                DISTANCE_OBJECT::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
                }
            }
            else
            {
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        DISTANCE_OBJECT::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
            }
        }
        
        template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE, class DISTANCE_OBJECT>
        inline void distancePartialTemplate(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        if (index % number_of_threads == thread_identifier)
                            DISTANCE_OBJECT::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index]);
            }
        }
        
        template <class TDISTANCE, class DISTANCE_OBJECT>
        inline void distanceMergeTemplate(TDISTANCE const * const * partial_distances, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_partial_distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_distances; i += number_of_threads)
                    merged_distances[i] = DISTANCE_OBJECT::distanceMerge(partial_distances[i], number_of_partial_distances);
            }
        }
        
        template <class TDISTANCE, class DISTANCE_OBJECT>
        inline void distanceFinalTemplate(const TDISTANCE * merged_distances, TDISTANCE * actual_distances, unsigned int number_of_distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_distances; i += number_of_threads)
                    actual_distances[i] = DISTANCE_OBJECT::distanceFinal(merged_distances[i]);
            }
        }
        
        template <class TDISTANCE, class DISTANCE_OBJECT>
        inline void distanceInverseFinalTemplate(const TDISTANCE * actual_distances, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_distances; i += number_of_threads)
                    actual_distances[i] = DISTANCE_OBJECT::distanceInverseFinal(merged_distances[i]);
            }
        }
        
        /// Identifier of the distance.
        DISTANCE_IDENTIFIER m_distance_identifier;
        /// Order of the distance object (for instance, the order used by the Minkowski distance).
        double m_order;
        /// Distortion matrix (for instance, the covariance matrix used by the Mahalanobis distance).
        Matrix<double> m_distortion_matrix;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DISTANCE CLASS IMPLEMENTATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ SINGLE DISTANCE FUNCTIONS ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distance(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, TDISTANCE &result) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            DistanceMinkowski::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), m_order, result);
            result = DistanceMinkowski::distanceFinal(result, m_order);
            break;
        case MANHATTAN_DISTANCE:
            DistanceManhattan::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceManhattan::distanceFinal(result);
            break;
        case EUCLIDEAN_DISTANCE:
            DistanceEuclidean::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceEuclidean::distanceFinal(result);
            break;
        case CHEBYSHEV_DISTANCE:
            DistanceChebyshev::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceChebyshev::distanceFinal(result);
            break;
        case COSINE_DISTANCE:
        {
            TDISTANCE norm_a, norm_b;
            
            DistanceCosine::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            L2Norm::normPartial(vectorA.getData(), (unsigned long)vectorA.size(), norm_a);
            L2Norm::normPartial(vectorB.getData(), (unsigned long)vectorB.size(), norm_b);
            norm_a = L2Norm::normFinal(norm_a);
            norm_b = L2Norm::normFinal(norm_b);
            result = DistanceCosine::distanceFinal(result / (norm_a * norm_b));
            break;
        }
        case GEODESIC_DISTANCE:
        {
            TDISTANCE norm_a, norm_b;
            
            DistanceGeodesic::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            L2Norm::normPartial(vectorA.getData(), (unsigned long)vectorA.size(), norm_a);
            L2Norm::normPartial(vectorB.getData(), (unsigned long)vectorB.size(), norm_b);
            norm_a = L2Norm::normFinal(norm_a);
            norm_b = L2Norm::normFinal(norm_b);
            result = DistanceGeodesic::distanceFinal(result / (norm_a * norm_b));
            break;
        }
        case CANBERRA_DISTANCE:
            DistanceCanberra::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceCanberra::distanceFinal(result);
            break;
        case INTERSECTION_DISTANCE:
        {
            TDISTANCE norm_a, norm_b;
            
            DistanceIntersection::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            L1Norm::normPartial(vectorA.getData(), (unsigned long)vectorA.size(), norm_a);
            L1Norm::normPartial(vectorB.getData(), (unsigned long)vectorB.size(), norm_b);
            norm_a = L1Norm::normFinal(norm_a);
            norm_b = L1Norm::normFinal(norm_b);
            result = result / srvMin<TDISTANCE>(norm_a, norm_b);
            result = DistanceIntersection::distanceFinal(result);
            break;
        }
        case CHISQUARE_DISTANCE:
        {
            DistanceChisquare::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceChisquare::distanceFinal(result);
            break;
        }
        case HELLINGER_DISTANCE:
            DistanceHellinger::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceHellinger::distanceFinal(result);
            break;
        case BHATTACHARYYA_DISTANCE:
            DistanceBhattacharyya::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            result = DistanceBhattacharyya::distanceFinal(result);
            break;
        case MAHALANOBIS_DISTANCE:
        {
            VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
            
            DistanceMahalanobis::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result, m_distortion_matrix, difference_vector, multiplication_vector);
            result = DistanceMahalanobis::distanceFinal(result);
            break;
        }
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distancePartial(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, TDISTANCE &result) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            DistanceMinkowski::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), m_order, result);
            break;
        case MANHATTAN_DISTANCE:
            DistanceManhattan::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case EUCLIDEAN_DISTANCE:
            DistanceEuclidean::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case CHEBYSHEV_DISTANCE:
            DistanceChebyshev::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case COSINE_DISTANCE:
            DistanceCosine::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case GEODESIC_DISTANCE:
            DistanceGeodesic::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case CANBERRA_DISTANCE:
            DistanceCanberra::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case INTERSECTION_DISTANCE:
            DistanceIntersection::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case CHISQUARE_DISTANCE:
            DistanceChisquare::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case HELLINGER_DISTANCE:
            DistanceHellinger::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case BHATTACHARYYA_DISTANCE:
            DistanceBhattacharyya::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result);
            break;
        case MAHALANOBIS_DISTANCE:
        {
            VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
            
            DistanceMahalanobis::distancePartial(vectorA.getData(), (unsigned long)vectorA.size(), vectorB.getData(), (unsigned long)vectorB.size(), result, m_distortion_matrix, difference_vector, multiplication_vector);
            break;
        }
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TA, class TB, class TDISTANCE>
    void VectorDistance::distanceDimension(const TA &first_value, const TB &second_value, TDISTANCE &dimension_distance) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            DistanceMinkowski::distanceDimension(first_value, second_value, dimension_distance, m_order);
            break;
        case MANHATTAN_DISTANCE:
            DistanceManhattan::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case EUCLIDEAN_DISTANCE:
            DistanceEuclidean::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case CHEBYSHEV_DISTANCE:
            DistanceChebyshev::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case COSINE_DISTANCE:
            DistanceCosine::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case GEODESIC_DISTANCE:
            DistanceGeodesic::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case CANBERRA_DISTANCE:
            DistanceCanberra::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case INTERSECTION_DISTANCE:
            DistanceIntersection::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case CHISQUARE_DISTANCE:
            DistanceChisquare::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case HELLINGER_DISTANCE:
            DistanceHellinger::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case BHATTACHARYYA_DISTANCE:
            DistanceBhattacharyya::distanceDimension(first_value, second_value, dimension_distance);
            break;
        case MAHALANOBIS_DISTANCE:
            DistanceMahalanobis::distanceDimension(first_value, second_value, dimension_distance);
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorDistance::distanceMerge(const TDISTANCE * partial_distances, unsigned int number_of_partial_distances) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            return DistanceMinkowski::distanceMerge(partial_distances, number_of_partial_distances);
        case MANHATTAN_DISTANCE:
            return DistanceManhattan::distanceMerge(partial_distances, number_of_partial_distances);
        case EUCLIDEAN_DISTANCE:
            return DistanceEuclidean::distanceMerge(partial_distances, number_of_partial_distances);
        case CHEBYSHEV_DISTANCE:
            return DistanceChebyshev::distanceMerge(partial_distances, number_of_partial_distances);
        case COSINE_DISTANCE:
            return DistanceCosine::distanceMerge(partial_distances, number_of_partial_distances);
        case GEODESIC_DISTANCE:
            return DistanceGeodesic::distanceMerge(partial_distances, number_of_partial_distances);
        case CANBERRA_DISTANCE:
            return DistanceCanberra::distanceMerge(partial_distances, number_of_partial_distances);
        case INTERSECTION_DISTANCE:
            return DistanceIntersection::distanceMerge(partial_distances, number_of_partial_distances);
        case CHISQUARE_DISTANCE:
            return DistanceChisquare::distanceMerge(partial_distances, number_of_partial_distances);
        case HELLINGER_DISTANCE:
            return DistanceHellinger::distanceMerge(partial_distances, number_of_partial_distances);
        case BHATTACHARYYA_DISTANCE:
            return DistanceBhattacharyya::distanceMerge(partial_distances, number_of_partial_distances);
        case MAHALANOBIS_DISTANCE:
            return DistanceMahalanobis::distanceMerge(partial_distances, number_of_partial_distances);
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorDistance::distanceMerge(const TDISTANCE &first, const TDISTANCE &second) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            return DistanceMinkowski::distanceMerge(first, second);
        case MANHATTAN_DISTANCE:
            return DistanceManhattan::distanceMerge(first, second);
        case EUCLIDEAN_DISTANCE:
            return DistanceEuclidean::distanceMerge(first, second);
        case CHEBYSHEV_DISTANCE:
            return DistanceChebyshev::distanceMerge(first, second);
        case COSINE_DISTANCE:
            return DistanceCosine::distanceMerge(first, second);
        case GEODESIC_DISTANCE:
            return DistanceGeodesic::distanceMerge(first, second);
        case CANBERRA_DISTANCE:
            return DistanceCanberra::distanceMerge(first, second);
        case INTERSECTION_DISTANCE:
            return DistanceIntersection::distanceMerge(first, second);
        case CHISQUARE_DISTANCE:
            return DistanceChisquare::distanceMerge(first, second);
        case HELLINGER_DISTANCE:
            return DistanceHellinger::distanceMerge(first, second);
        case BHATTACHARYYA_DISTANCE:
            return DistanceBhattacharyya::distanceMerge(first, second);
        case MAHALANOBIS_DISTANCE:
            return DistanceMahalanobis::distanceMerge(first, second);
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorDistance::distanceFinal(const TDISTANCE &merged_distance) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            return DistanceMinkowski::distanceFinal(merged_distance, m_order);
        case MANHATTAN_DISTANCE:
            return DistanceManhattan::distanceFinal(merged_distance);
        case EUCLIDEAN_DISTANCE:
            return DistanceEuclidean::distanceFinal(merged_distance);
        case CHEBYSHEV_DISTANCE:
            return DistanceChebyshev::distanceFinal(merged_distance);
        case COSINE_DISTANCE:
            return DistanceCosine::distanceFinal(merged_distance);
        case GEODESIC_DISTANCE:
            return DistanceGeodesic::distanceFinal(merged_distance);
        case CANBERRA_DISTANCE:
            return DistanceCanberra::distanceFinal(merged_distance);
        case INTERSECTION_DISTANCE:
            return DistanceIntersection::distanceFinal(merged_distance);
        case CHISQUARE_DISTANCE:
            return DistanceChisquare::distanceFinal(merged_distance);
        case HELLINGER_DISTANCE:
            return DistanceHellinger::distanceFinal(merged_distance);
        case BHATTACHARYYA_DISTANCE:
            return DistanceBhattacharyya::distanceFinal(merged_distance);
        case MAHALANOBIS_DISTANCE:
            return DistanceMahalanobis::distanceFinal(merged_distance);
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    TDISTANCE VectorDistance::distanceInverseFinal(const TDISTANCE &final_distance) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            return DistanceMinkowski::distanceInverseFinal(final_distance, m_order);
        case MANHATTAN_DISTANCE:
            return DistanceManhattan::distanceInverseFinal(final_distance);
        case EUCLIDEAN_DISTANCE:
            return DistanceEuclidean::distanceInverseFinal(final_distance);
        case CHEBYSHEV_DISTANCE:
            return DistanceChebyshev::distanceInverseFinal(final_distance);
        case COSINE_DISTANCE:
            return DistanceCosine::distanceInverseFinal(final_distance);
        case GEODESIC_DISTANCE:
            return DistanceGeodesic::distanceInverseFinal(final_distance);
        case CANBERRA_DISTANCE:
            return DistanceCanberra::distanceInverseFinal(final_distance);
        case INTERSECTION_DISTANCE:
            return DistanceIntersection::distanceInverseFinal(final_distance);
        case CHISQUARE_DISTANCE:
            return DistanceChisquare::distanceInverseFinal(final_distance);
        case HELLINGER_DISTANCE:
            return DistanceHellinger::distanceInverseFinal(final_distance);
        case BHATTACHARYYA_DISTANCE:
            return DistanceBhattacharyya::distanceInverseFinal(final_distance);
        case MAHALANOBIS_DISTANCE:
            return DistanceMahalanobis::distanceInverseFinal(final_distance);
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GROUP DISTANCE FUNCTIONS ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distance(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DistanceMinkowski::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), m_order, distances[index]);
                            distances[index] = DistanceMinkowski::distanceFinal(distances[index], m_order);
                        }
                    }
                }
            }
            break;
        case MANHATTAN_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceManhattan>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceEuclidean>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChebyshev>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
        case GEODESIC_DISTANCE:
        case INTERSECTION_DISTANCE:
        {
            VectorDense<TDISTANCE> normals_a(number_of_vectors_a), normals_b(number_of_vectors_b);
            
            if (m_distance_identifier != INTERSECTION_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = thread_identifier; i < number_of_vectors_a; i += number_of_threads)
                    {
                        L2Norm::normPartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), normals_a[i]);
                        normals_a[i] = L2Norm::normFinal(normals_a[i]);
                    }
                    for (unsigned int i = thread_identifier; i < number_of_vectors_b; i += number_of_threads)
                    {
                        L2Norm::normPartial(vectorsB[i].getData(), (unsigned long)vectorsB[i].size(), normals_b[i]);
                        normals_b[i] = L2Norm::normFinal(normals_b[i]);
                    }
                }
            }
            else
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = thread_identifier; i < number_of_vectors_a; i += number_of_threads)
                    {
                        L1Norm::normPartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), normals_a[i]);
                        normals_a[i] = L1Norm::normFinal(normals_a[i]);
                    }
                    for (unsigned int i = thread_identifier; i < number_of_vectors_b; i += number_of_threads)
                    {
                        L1Norm::normPartial(vectorsB[i].getData(), (unsigned long)vectorsB[i].size(), normals_b[i]);
                        normals_b[i] = L1Norm::normFinal(normals_b[i]);
                    }
                }
            }
            
            if (m_distance_identifier == COSINE_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceCosine::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
                                distances[index] = DistanceCosine::distanceFinal(distances[index] / (normals_a[i] * normals_b[j]));
                            }
                        }
                    }
                }
            }
            else if (m_distance_identifier == GEODESIC_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceGeodesic::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
                                distances[index] = DistanceGeodesic::distanceFinal(distances[index] / (normals_a[i] * normals_b[j]));
                            }
                        }
                    }
                }
            }
            else if (m_distance_identifier == INTERSECTION_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceIntersection::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index]);
                                distances[index] = distances[index] / srvMin<TDISTANCE>(normals_a[i], normals_b[j]);
                                distances[index] = DistanceIntersection::distanceFinal(distances[index]);
                            }
                        }
                    }
                }
            }
            break;
        }
        case CANBERRA_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCanberra>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChisquare>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceHellinger>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceBhattacharyya>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DistanceMahalanobis::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index], m_distortion_matrix, difference_vector, multiplication_vector);
                            distances[index] = DistanceMahalanobis::distanceFinal(distances[index]);
                        }
                    }
                }
            }
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distance(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DistanceMinkowski::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), m_order, distances[index]);
                            distances[index] = DistanceMinkowski::distanceFinal(distances[index], m_order);
                        }
                    }
                }
            }
            break;
        case MANHATTAN_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceManhattan>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceEuclidean>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChebyshev>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
        case GEODESIC_DISTANCE:
        case INTERSECTION_DISTANCE:
        {
            VectorDense<TDISTANCE> normals_a(number_of_vectors_a), normals_b(number_of_vectors_b);
            
            if (m_distance_identifier != INTERSECTION_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = thread_identifier; i < number_of_vectors_a; i += number_of_threads)
                    {
                        L2Norm::normPartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), normals_a[i]);
                        normals_a[i] = L2Norm::normFinal(normals_a[i]);
                    }
                    for (unsigned int i = thread_identifier; i < number_of_vectors_b; i += number_of_threads)
                    {
                        L2Norm::normPartial(vectorsB[i]->getData(), (unsigned long)vectorsB[i]->size(), normals_b[i]);
                        normals_b[i] = L2Norm::normFinal(normals_b[i]);
                    }
                }
            }
            else
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = thread_identifier; i < number_of_vectors_a; i += number_of_threads)
                    {
                        L1Norm::normPartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), normals_a[i]);
                        normals_a[i] = L1Norm::normFinal(normals_a[i]);
                    }
                    for (unsigned int i = thread_identifier; i < number_of_vectors_b; i += number_of_threads)
                    {
                        L1Norm::normPartial(vectorsB[i]->getData(), (unsigned long)vectorsB[i]->size(), normals_b[i]);
                        normals_b[i] = L1Norm::normFinal(normals_b[i]);
                    }
                }
            }
            
            if (m_distance_identifier == COSINE_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceCosine::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index]);
                                distances[index] = DistanceCosine::distanceFinal(distances[index] / (normals_a[i] * normals_b[j]));
                            }
                        }
                    }
                }
            }
            else if (m_distance_identifier == GEODESIC_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceGeodesic::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index]);
                                distances[index] = DistanceGeodesic::distanceFinal(distances[index] / (normals_a[i] * normals_b[j]));
                            }
                        }
                    }
                }
            }
            else if (m_distance_identifier == INTERSECTION_DISTANCE)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    {
                        for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        {
                            if (index % number_of_threads == thread_identifier)
                            {
                                DistanceIntersection::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index]);
                                distances[index] = distances[index] / srvMin<TDISTANCE>(normals_a[i], normals_b[j]);
                                distances[index] = DistanceIntersection::distanceFinal(distances[index]);
                            }
                        }
                    }
                }
            }
            break;
        }
        case CANBERRA_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCanberra>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChisquare>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceHellinger>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distanceTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceBhattacharyya>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                {
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                    {
                        if (index % number_of_threads == thread_identifier)
                        {
                            DistanceMahalanobis::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index], m_distortion_matrix, difference_vector, multiplication_vector);
                            distances[index] = DistanceMahalanobis::distanceFinal(distances[index]);
                        }
                    }
                }
            }
            break;
        }
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distancePartial(const VECTORA<TA, NA> * vectorsA, unsigned int number_of_vectors_a, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        if (index % number_of_threads == thread_identifier)
                            DistanceMinkowski::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), m_order, distances[index]);
            }
            break;
        case MANHATTAN_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceManhattan>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceEuclidean>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChebyshev>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCosine>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case GEODESIC_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceGeodesic>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case INTERSECTION_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceIntersection>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CANBERRA_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCanberra>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChisquare>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceHellinger>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceBhattacharyya>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        if (index % number_of_threads == thread_identifier)
                            DistanceMahalanobis::distancePartial(vectorsA[i].getData(), (unsigned long)vectorsA[i].size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[index], m_distortion_matrix, difference_vector, multiplication_vector);
            }
            break;
        }
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distancePartial(VECTORA<TA, NA> const * const * vectorsA, unsigned int number_of_vectors_a, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        if (index % number_of_threads == thread_identifier)
                            DistanceMinkowski::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), m_order, distances[index]);
            }
            break;
        case MANHATTAN_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceManhattan>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceEuclidean>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChebyshev>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCosine>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case GEODESIC_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceGeodesic>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case INTERSECTION_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceIntersection>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CANBERRA_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceCanberra>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceChisquare>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceHellinger>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distancePartialTemplate<VECTORA, TA, NA, VECTORB, TB, NB, TDISTANCE, DistanceBhattacharyya>(vectorsA, number_of_vectors_a, vectorsB, number_of_vectors_b, distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                
                for (unsigned int i = 0, index = 0; i < number_of_vectors_a; ++i)
                    for (unsigned int j = 0; j < number_of_vectors_b; ++j, ++index)
                        if (index % number_of_threads == thread_identifier)
                            DistanceMahalanobis::distancePartial(vectorsA[i]->getData(), (unsigned long)vectorsA[i]->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[index], m_distortion_matrix, difference_vector, multiplication_vector);
            }
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distancePartial(const VECTORA<TA, NA> &vectorsA, const VECTORB<TB, NB> * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceMinkowski::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), m_order, distances[j]);
            }
            break;
        case MANHATTAN_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceManhattan::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case EUCLIDEAN_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceEuclidean::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case CHEBYSHEV_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceChebyshev::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case COSINE_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceCosine::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case GEODESIC_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceGeodesic::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case INTERSECTION_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceIntersection::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case CANBERRA_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceCanberra::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case CHISQUARE_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceChisquare::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case HELLINGER_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceHellinger::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case BHATTACHARYYA_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceBhattacharyya::distancePartial(vectorsA.getData(), vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j]);
            }
            break;
        case MAHALANOBIS_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceMahalanobis::distancePartial(vectorsA.getData(), (unsigned long)vectorsA.size(), vectorsB[j].getData(), (unsigned long)vectorsB[j].size(), distances[j], m_distortion_matrix, difference_vector, multiplication_vector);
            }
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TDISTANCE>
    void VectorDistance::distancePartial(const VECTORA<TA, NA> &vectorsA, VECTORB<TB, NB> const * const * vectorsB, unsigned int number_of_vectors_b, TDISTANCE * distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceMinkowski::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), m_order, distances[j]);
            }
            break;
        case MANHATTAN_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceManhattan::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case EUCLIDEAN_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceEuclidean::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case CHEBYSHEV_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceChebyshev::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case COSINE_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceCosine::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case GEODESIC_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceGeodesic::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case INTERSECTION_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceIntersection::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case CANBERRA_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceCanberra::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case CHISQUARE_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceChisquare::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case HELLINGER_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceHellinger::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case BHATTACHARYYA_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceBhattacharyya::distancePartial(vectorsA->getData(), vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j]);
            }
            break;
        case MAHALANOBIS_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                VectorDense<double> difference_vector(m_distortion_matrix.getNumberOfRows()), multiplication_vector(m_distortion_matrix.getNumberOfRows());
                for (unsigned int j = omp_get_thread_num(); j < number_of_vectors_b; j += number_of_threads)
                    DistanceMahalanobis::distancePartial(vectorsA->getData(), (unsigned long)vectorsA->size(), vectorsB[j]->getData(), (unsigned long)vectorsB[j]->size(), distances[j], m_distortion_matrix, difference_vector, multiplication_vector);
            }
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    void VectorDistance::distanceMerge(TDISTANCE const * const * partial_distances, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_partial_distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceMinkowski>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case MANHATTAN_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceManhattan>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceEuclidean>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceChebyshev>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceCosine>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case GEODESIC_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceGeodesic>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case INTERSECTION_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceIntersection>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case CANBERRA_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceCanberra>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceChisquare>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceHellinger>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceBhattacharyya>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
            distanceMergeTemplate<TDISTANCE, DistanceMahalanobis>(partial_distances, merged_distances, number_of_distances, number_of_partial_distances, number_of_threads);
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    void VectorDistance::distanceFinal(const TDISTANCE * merged_distances, TDISTANCE * actual_distances, unsigned int number_of_distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_distances; i += number_of_threads)
                    actual_distances[i] = DistanceMinkowski::distanceFinal(merged_distances[i], m_order);
            }
            break;
        case MANHATTAN_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceManhattan>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceEuclidean>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceChebyshev>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceCosine>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case GEODESIC_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceGeodesic>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case INTERSECTION_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceIntersection>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case CANBERRA_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceCanberra>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceChisquare>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceHellinger>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceBhattacharyya>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
            distanceFinalTemplate<TDISTANCE, DistanceMahalanobis>(merged_distances, actual_distances, number_of_distances, number_of_threads);
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    template <class TDISTANCE>
    void VectorDistance::distanceInverseFinal(const TDISTANCE * actual_distance, TDISTANCE * merged_distances, unsigned int number_of_distances, unsigned int number_of_threads) const
    {
        switch (m_distance_identifier)
        {
        case MINKOWSKI_DISTANCE:
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_distances; i += number_of_threads)
                    merged_distances[i] = DistanceMinkowski::distanceInverseFinal(actual_distance[i], m_order);
            }
            break;
        case MANHATTAN_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceManhattan>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case EUCLIDEAN_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceEuclidean>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case CHEBYSHEV_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceChebyshev>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case COSINE_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceCosine>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case GEODESIC_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceGeodesic>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case INTERSECTION_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceIntersection>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case CANBERRA_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceCanberra>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case CHISQUARE_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceChisquare>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case HELLINGER_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceHellinger>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case BHATTACHARYYA_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceBhattacharyya>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        case MAHALANOBIS_DISTANCE:
            distanceInverseFinalTemplate<TDISTANCE, DistanceMahalanobis>(actual_distance, merged_distances, number_of_distances, number_of_threads);
            break;
        default:
            throw Exception("The selected distance has not been yet implemented.");
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

