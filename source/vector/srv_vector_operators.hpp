// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_VECTOR_OPERATORS_HPP_HEADER_FILE__
#define __SRV_VECTOR_OPERATORS_HPP_HEADER_FILE__

#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "srv_vector.hpp"
#include <limits>
#include <omp.h>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GENERAL OPERATOR                     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ META-PROGRAMING TYPE DETECTOR ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    struct OUTPUT_VECTOR { typedef VectorDense< typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE > TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<VectorSparse, TA, NA, VectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<VectorSparse, TA, NA, SubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<VectorSparse, TA, NA, ConstantSubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<SubVectorSparse, TA, NA, VectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<SubVectorSparse, TA, NA, SubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<SubVectorSparse, TA, NA, ConstantSubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, VectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, SubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    template <class TA, class NA, class TB, class NB>
    struct OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, ConstantSubVectorSparse, TB, NB> { typedef VectorSparse<typename META_MGT<TA, TB>::TYPE, typename META_MGT<NA, NB>::TYPE> TYPE; };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ DENSE VECTOR - DENSE VECTOR OPERATORS ]====================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] sizeA size of the left-hand vector.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[in] sizeB size of the right-hand vector.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const TA * __restrict__ vectorA, const NA &sizeA, const TB * __restrict__ vectorB, const NB &sizeB, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        if (((unsigned long)sizeA != (unsigned long)sizeB) || ((unsigned long)sizeA != (unsigned long)sizeC)) throw Exception("Incorrect vector size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        if (((void*)vectorA == (void*)vectorB) && ((void*)vectorA == (void*)vectorC)) // A = A OPERATOR A
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::self(vectorC[i]);
            }
        }
        else if ((void*)vectorA == (void*)vectorC) // A = A OPERATOR B
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)vectorC[i], (OPERATOR_TYPE)vectorB[i], vectorC[i]);
            }
        }
        else if ((void*)vectorB == (void*)vectorC) // A = B OPERATOR A
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)vectorA[i], (OPERATOR_TYPE)vectorC[i], vectorC[i]);
            }
        }
        else // A = B OPERATOR C
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)vectorA[i], (OPERATOR_TYPE)vectorB[i], vectorC[i]);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPARSE VECTOR - DENSE VECTOR OPERATORS ]===================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] vectorA left-hand sparse vector operand.
     *  \param[in] sizeA size of the left-hand sparse vector.
     *  \param[in] vectorB right-hand dense vector operand.
     *  \param[in] sizeB size of the right-hand dense vector.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const Tuple<TA, NA> * __restrict__ vectorA, const NA &sizeA, const TB * __restrict__ vectorB, const NB &sizeB, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        if ((unsigned long)sizeB != (unsigned long)sizeC) throw Exception("Incorrect vector size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        if ((void*)vectorB == (void*)vectorC) // B = A OPERATOR B
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
                {
                    while ((j < (unsigned long)sizeA) && ((unsigned long)vectorA[j].getSecond() < i))
                    {
                        if ((unsigned long)vectorA[j].getSecond() >= (unsigned long)sizeB)
                            throw Exception("Incorrect sparse vector size.");
                        ++j;
                    }
                    if (j < (unsigned long)sizeA)
                    {
                        if (vectorA[j].getSecond() == (NA)i) OPERATOR::calculate((OPERATOR_TYPE)vectorA[j].getFirst(), (OPERATOR_TYPE)vectorC[i], vectorC[i]);
                        else OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)vectorC[i], vectorC[i]);
                    }
                    else OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)vectorC[i], vectorC[i]);
                }
            }
        }
        else // A = B OPERATOR C
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
                {
                    while ((j < (unsigned long)sizeA) && ((unsigned long)vectorA[j].getSecond() < i))
                    {
                        if ((unsigned long)vectorA[j].getSecond() >= (unsigned long)sizeB)
                            throw Exception("Incorrect sparse vector size.");
                        ++j;
                    }
                    if (j < (unsigned long)sizeA)
                    {
                        if (vectorA[j].getSecond() == (NA)i) OPERATOR::calculate((OPERATOR_TYPE)vectorA[j].getFirst(), (OPERATOR_TYPE)vectorB[i], vectorC[i]);
                        else OPERATOR::calculate((TB)0, vectorB[i], vectorC[i]);
                    }
                    else OPERATOR::calculate((TB)0, vectorB[i], vectorC[i]);
                }
            }
        }
    }
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] sizeA size of the left-hand dense vector.
     *  \param[in] vectorB right-hand sparse vector operand.
     *  \param[in] sizeB size of the right-hand sparse vector.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const TA * __restrict__ vectorA, const NA &sizeA, const Tuple<TB, NB> * __restrict__ vectorB, const NB &sizeB, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        if ((unsigned long)sizeA != (unsigned long)sizeC) throw Exception("Incorrect vector size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        if ((void*)vectorA == (void*)vectorC) // A = A OPERATOR B
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
                {
                    while ((j < (unsigned long)sizeB) && ((unsigned long)vectorB[j].getSecond() < i))
                    {
                        if ((unsigned long)vectorB[j].getSecond() >= (unsigned long)sizeA)
                            throw Exception("Incorrect sparse vector size.");
                        ++j;
                    }
                    if (j < (unsigned long)sizeB)
                    {
                        if (vectorB[j].getSecond() == (NB)i) OPERATOR::calculate((OPERATOR_TYPE)vectorC[i], (OPERATOR_TYPE)vectorB[j].getFirst(), vectorC[i]);
                        else OPERATOR::calculate((OPERATOR_TYPE)vectorC[i], (OPERATOR_TYPE)0, vectorC[i]);
                    }
                    else OPERATOR::calculate((OPERATOR_TYPE)vectorC[i], (OPERATOR_TYPE)0, vectorC[i]);
                }
            }
        }
        else // A = B OPERATOR C
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
                {
                    while ((j < (unsigned long)sizeB) && ((unsigned long)vectorB[j].getSecond() < i))
                    {
                        if ((unsigned long)vectorB[j].getSecond() >= (unsigned long)sizeA)
                            throw Exception("Incorrect sparse vector size.");
                        ++j;
                    }
                    if (j < (unsigned long)sizeB)
                    {
                        if (vectorB[j].getSecond() == (NB)i) OPERATOR::calculate((OPERATOR_TYPE)vectorA[i], (OPERATOR_TYPE)vectorB[j].getFirst(), vectorC[i]);
                        else OPERATOR::calculate(vectorA[i], (TA)0, vectorC[i]);
                    }
                    else OPERATOR::calculate(vectorA[i], (TA)0, vectorC[i]);
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPARSE VECTOR - SPARSE VECTOR OPERATORS ]==================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] sizeA size of the left-hand dense vector.
     *  \param[in] vectorB right-hand sparse vector operand.
     *  \param[in] sizeB size of the right-hand sparse vector.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const Tuple<TA, NA> * __restrict__ vectorA, const NA &sizeA, const Tuple<TB, NB> * __restrict__ vectorB, const NB &sizeB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads)
    {
        if ((sizeA > 0) && (sizeB > 0))
        {
            typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
            ChainedList<Tuple<TC, NC> > **result_list, *current_ptr;
            NC *thread_sizes, sizeC;
            long maximum_size;
            
            thread_sizes = new NC[number_of_threads];
            result_list = new ChainedList<Tuple<TC, NC> >*[number_of_threads];
            for (unsigned long i = 0; i < number_of_threads; ++i)
            {
                thread_sizes[i] = 0;
                result_list[i] = 0;
            }
            maximum_size = srvMax<long>(vectorA[sizeA - 1].getSecond() + 1, vectorB[sizeB - 1].getSecond() + 1);
            #pragma omp parallel num_threads(number_of_threads)
            {
                TC current_value;
                long thread_id = (long)omp_get_thread_num();
                for (long i = thread_id, j = 0, k = 0; i < maximum_size; i += (long)number_of_threads)
                {
                    while ((j < (long)sizeA) && ((long)vectorA[j].getSecond() < i)) ++j;
                    while ((k < (long)sizeB) && ((long)vectorB[k].getSecond() < i)) ++k;
                    
                    if ((j < (long)sizeA) && (k < (long)sizeB) && ((long)vectorA[j].getSecond() == i) && ((long)vectorB[k].getSecond() == i))
                        OPERATOR::calculate((OPERATOR_TYPE)vectorA[j].getFirst(), (OPERATOR_TYPE)vectorB[k].getFirst(), current_value);
                    else if ((j < (long)sizeA) && ((long)vectorA[j].getSecond() == i))
                        OPERATOR::calculate((OPERATOR_TYPE)vectorA[j].getFirst(), (OPERATOR_TYPE)0, current_value);
                    else if ((k < (long)sizeB) && ((long)vectorB[k].getSecond() == i))
                        OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)vectorB[k].getFirst(), current_value);
                    else
                        OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)0, current_value);
                    
                    if (current_value != 0)
                    {
                        result_list[thread_id] = new ChainedList<Tuple<TC, NC> >(Tuple<TC, NC>(current_value, (NC)i), result_list[thread_id]);
                        ++thread_sizes[thread_id];
                    }
                }
            }
            
            sizeC = thread_sizes[0];
            for (unsigned long i = 1; i < number_of_threads; ++i) sizeC += thread_sizes[i];
            vectorC.resize(sizeC);
            for (NC i = 0; i < sizeC; ++i)
            {
                unsigned long selected = number_of_threads;
                long maximum_index = std::numeric_limits<long>::min();
                for (unsigned long j = 0; j < number_of_threads; ++j)
                {
                    if ((result_list[j] != 0) && ((long)result_list[j]->getData().getSecond() > maximum_index))
                    {
                        maximum_index = (NC)result_list[j]->getData().getSecond();
                        selected = j;
                    }
                }
                if (selected == number_of_threads) throw Exception("Something have gone terribly wrong while building the resulting sparse vector.");
                else
                {
                    vectorC[sizeC - i - 1] = result_list[selected]->getData();
                    current_ptr = result_list[selected];
                    result_list[selected] = result_list[selected]->getNext();
                    current_ptr->setNext(0);
                    delete current_ptr;
                }
            }
            
            delete [] thread_sizes;
            delete [] result_list;
        }
        else if (sizeA == 0) vectorC.set(vectorB, sizeB);
        else if (sizeB == 0) vectorC.set(vectorA, sizeA);
        else vectorC.resize(0);
    }
    
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ VECTOR-VALUE OPERATORS ]===================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] sizeA size of the left-hand dense vector.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const TA * __restrict__ vectorA, const NA &sizeA, const TB &value, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        if ((unsigned long)sizeA != (unsigned long)sizeC) throw Exception("Incorrect vector size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        if ((void*)vectorA == (void*)vectorC) // A = A OPERATOR VALUE
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)vectorC[i], (OPERATOR_TYPE)value, vectorC[i]);
            }
        }
        else // A = B OPERATOR VALUE
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)vectorA[i], (OPERATOR_TYPE)value, vectorC[i]);
            }
        }
    }
    
    /** General vector operator function which operates between dense vectors.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand dense vector operand.
     *  \param[in] sizeB size of the right-hand dense vector.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const TA &value, const TB * __restrict__ vectorB, const NB &sizeB, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        if ((unsigned long)sizeB != (unsigned long)sizeC) throw Exception("Incorrect vector size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        if ((void*)vectorB == (void*)vectorC) // A = VALUE OPERATOR A
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)value, (OPERATOR_TYPE)vectorC[i], vectorC[i]);
            }
        }
        else // A = VALUE OPERATOR B
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
                    OPERATOR::calculate((OPERATOR_TYPE)value, (OPERATOR_TYPE)vectorB[i], vectorC[i]);
            }
        }
    }
    
    /** General vector operator function which operates between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector operand.
     *  \param[in] sizeA size of the left-hand sparse vector.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class NA, class TB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const Tuple<TA, NA> * __restrict__ vectorA, const NA &sizeA, const TB &value, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
            {
                while ((j < (unsigned long)sizeA) && ((unsigned long)vectorA[j].getSecond() < i))
                {
                    if ((unsigned long)vectorA[j].getSecond() >= (unsigned long)sizeC)
                        throw Exception("Incorrect sparse vector size.");
                    ++j;
                }
                if (j < (unsigned long)sizeA)
                {
                    if (vectorA[j].getSecond() == (NA)i) OPERATOR::calculate((OPERATOR_TYPE)vectorA[j].getFirst(), (OPERATOR_TYPE)value, vectorC[i]);
                    else OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)value, vectorC[i]);
                }
                else OPERATOR::calculate((OPERATOR_TYPE)0, (OPERATOR_TYPE)value, vectorC[i]);
            }
        }
    }
    
    /** General vector operator function which operates between sparse vectors.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand sparse vector operand.
     *  \param[in] sizeB size of the right-hand sparse vector.
     *  \param[in] op operator used between the input vectors.
     *  \param[out] vectorC result vector.
     *  \param[in] sizeC size of the result vector.
     */
    template <class TA, class TB, class NB, class TC, class NC, class OPERATOR>
    void VectorGeneralOperator(const TA &value, const Tuple<TB, NB> * __restrict__ vectorB, const NB &sizeB, TC * __restrict__ vectorC, const NC &sizeC, unsigned long number_of_threads)
    {
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(), j = 0; i < (unsigned long)sizeC; i += number_of_threads)
            {
                while ((j < (unsigned long)sizeB) && ((unsigned long)vectorB[j].getSecond() < i))
                {
                    if ((unsigned long)vectorB[j].getSecond() >= (unsigned long)sizeC)
                        throw Exception("Incorrect sparse vector size.");
                    ++j;
                }
                if (j < (unsigned long)sizeB)
                {
                    if (vectorB[j].getSecond() == (NB)i) OPERATOR::calculate((OPERATOR_TYPE)value, (OPERATOR_TYPE)vectorB[j].getFirst(), vectorC[i]);
                    else OPERATOR::calculate((OPERATOR_TYPE)value, (OPERATOR_TYPE)0, vectorC[i]);
                }
                else OPERATOR::calculate((OPERATOR_TYPE)value, (OPERATOR_TYPE)0, vectorC[i]);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ UNARY OPERATOR ]===========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** General unary operator function for sparse vectors.
     *  \param[in] vectorA input sparse vector.
     *  \param[in] sizeA size of the input sparse vector.
     *  \param[out] vectorB output sparse vector.
     *  \param[out] sizeB size of the output sparse vector.
     *  \note The input and output vectors must have the same size and both must by previously allocated.
     */
    template <class TA, class NA, class TB, class NB, class OPERATOR>
    void VectorGeneralOperator(const Tuple<TA, NA> *vectorA, const NA &sizeA, Tuple<TB, NB> *vectorB, const NB &sizeB, unsigned long number_of_threads)
    {
        if ((void*)vectorA == (void*)vectorB)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeB; i += number_of_threads) OPERATOR::self(vectorB[i].getFirst());
            }
        }
        else
        {
            if ((unsigned long)sizeA != (unsigned long)sizeB) throw Exception("Incorrect vector size.");
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeB; i += number_of_threads)
                {
                    OPERATOR::calculate(vectorA[i].getFirst(), vectorB[i].getFirst());
                    vectorB[i].getSecond() = (NB)vectorA[i].getSecond();
                }
            }
        }
    }
    
    /** General unary operator function for dense vectors.
     *  \param[in] vectorA input dense vector.
     *  \param[in] sizeA size of the input dense vector.
     *  \param[out] vectorB output dense vector.
     *  \param[out] sizeB size of the output dense vector.
     *  \note The input and output vectors must have the same size and both must by previously allocated.
     */
    
    template <class TA, class NA, class TB, class NB, class OPERATOR>
    void VectorGeneralOperator(const TA *vectorA, const NA &sizeA, TB *vectorB, const NB &sizeB, unsigned long number_of_threads)
    {
        if ((void*)vectorA == (void*)vectorB)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeB; i += number_of_threads) OPERATOR::self(vectorB[i]);
            }
        }
        else
        {
            if ((unsigned long)sizeA != (unsigned long)sizeB) throw Exception("Incorrect vector size.");
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeB; i += number_of_threads) OPERATOR::calculate(vectorA[i], vectorB[i]);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GENERAL VECTOR MULTIPLICATION FUNCTION ]===================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Calculates the general vector multiplication (similar to the GEMM routines of the BLAS package) between two dense vectors: vectorC = weightA * vectorA * vectorB + weightB * vectorC;
     *  \param[in] vectorA array with the elements of the left-hand operand of the multiplication.
     *  \param[in] sizeA number of elements of the left-hand operand array.
     *  \param[in] vectorB array with the elements of the right-hand operand of the multiplication.
     *  \param[in] sizeB number of elements of the right-hand operand array.
     *  \param[in] weightA weight of the multiplication.
     *  \param[out] vectorC array where the result of the multiplication will be stored.
     *  \param[in] sizeC size of the result array.
     *  \param[in] weightB weight of the previous result array values added to the multiplication result.
     *  \param[in] number_of_threads number of threads used to concurrently process the multiplication.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class WA, class WB>
    void VectorGeneralOperator(const TA *vectorA, const NA &sizeA, const TB *vectorB, const NB &sizeB, const WA &weightA, TC *vectorC, const NC &sizeC, const WB &weightB, unsigned int number_of_threads)
    {
        if ((sizeA != sizeC) || (sizeB != sizeC)) throw Exception("Incorrect vector size. All three vectors must have the same size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        typedef typename META_MGT<WA, WB>::TYPE WEIGHT_TYPE;
        typedef typename META_MGT<OPERATOR_TYPE, WEIGHT_TYPE>::TYPE VGO_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(); i < (unsigned long)sizeC; i += number_of_threads)
            {
                const VGO_TYPE multiplication_result = (VGO_TYPE)vectorA[i] * (VGO_TYPE)vectorB[i] * (VGO_TYPE)weightA;
                const VGO_TYPE weighted_result = (VGO_TYPE)vectorC[i] * (VGO_TYPE)weightB;
                vectorC[i] = (TC)(multiplication_result + weighted_result);
            }
        }
    }
    
    /** Calculates the general vector multiplication (similar to the GEMM routines of the BLAS package) between a sparse and a dense vectors: vectorC = weightA * vectorA * vectorB + weightB * vectorC;
     *  \param[in] vectorA tuples array from an sparse vector with the elements of the left-hand operand of the multiplication.
     *  \param[in] sizeA number of elements in tuples array of the left-hand operand.
     *  \param[in] vectorB array with the elements of the right-hand operand of the multiplication.
     *  \param[in] sizeB number of elements of the right-hand operand array.
     *  \param[in] weightA weight of the multiplication.
     *  \param[out] vectorC array where the result of the multiplication will be stored.
     *  \param[in] sizeC size of the result array.
     *  \param[in] weightB weight of the previous result array values added to the multiplication result.
     *  \param[in] number_of_threads number of threads used to concurrently process the multiplication.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class WA, class WB>
    void VectorGeneralOperator(const Tuple<TA, NA> *vectorA, const NA &sizeA, const TB *vectorB, const NB &sizeB, const WA &weightA, TC *vectorC, const NC &sizeC, const WB &weightB, unsigned int number_of_threads)
    {
        if (sizeB != sizeC) throw Exception("Incorrect vector size. Both dense vectors must have the same size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        typedef typename META_MGT<WA, WB>::TYPE WEIGHT_TYPE;
        typedef typename META_MGT<OPERATOR_TYPE, WEIGHT_TYPE>::TYPE VGO_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (long i = omp_get_thread_num(), j = 0; i < (long)sizeC; i += number_of_threads)
            {
                const VGO_TYPE weighted_result = (VGO_TYPE)vectorC[i] * (VGO_TYPE)weightB;
                VGO_TYPE multiplication_result;
                
                while ((j < sizeA) && ((long)vectorA[j].getSecond() < i)) ++j;
                
                if (j < sizeA)
                {
                    if ((long)vectorA[j].getSecond() == i) multiplication_result = (VGO_TYPE)weightA * (VGO_TYPE)vectorA[j].getFirst() * (VGO_TYPE)vectorB[i];
                    else if ((long)vectorA[j].getSecond() >= (long)sizeC) throw Exception("Incorrect vector size. The dimensionality of the sparse vector is greater than the dimensionality of the result vector.");
                    else multiplication_result = 0;
                }
                else multiplication_result = 0;
                
                vectorC[i] = (TC)(multiplication_result + weighted_result);
            }
        }
    }
    
    /** Calculates the general vector multiplication (similar to the GEMM routines of the BLAS package) between a dense and a sparse vectors: vectorC = weightA * vectorA * vectorB + weightB * vectorC;
     *  \param[in] vectorA array with the elements of the left-hand operand of the multiplication.
     *  \param[in] sizeA number of elements of the left-hand operand array.
     *  \param[in] vectorB tuples array from an sparse vector with the elements of the right-hand operand of the multiplication.
     *  \param[in] sizeB number of elements in tuples array of the right-hand operand.
     *  \param[in] weightA weight of the multiplication.
     *  \param[out] vectorC array where the result of the multiplication will be stored.
     *  \param[in] sizeC size of the result array.
     *  \param[in] weightB weight of the previous result array values added to the multiplication result.
     *  \param[in] number_of_threads number of threads used to concurrently process the multiplication.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class WA, class WB>
    void VectorGeneralOperator(const TA *vectorA, const NA &sizeA, const Tuple<TB, NB> *vectorB, const NB &sizeB, const WA &weightA, TC *vectorC, const NC &sizeC, const WB &weightB, unsigned int number_of_threads)
    {
        if (sizeA != sizeC) throw Exception("Incorrect vector size. Both dense vector must have the same size.");
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        typedef typename META_MGT<WA, WB>::TYPE WEIGHT_TYPE;
        typedef typename META_MGT<OPERATOR_TYPE, WEIGHT_TYPE>::TYPE VGO_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (long i = omp_get_thread_num(), j = 0; i < (long)sizeC; i += number_of_threads)
            {
                const VGO_TYPE weight_result = (VGO_TYPE)vectorC[i] * (VGO_TYPE)weightB;
                VGO_TYPE multiplication_result;
                
                while ((j < sizeB) && ((long)vectorB[j].getSecond() < i)) ++j;
                
                if (j < sizeB)
                {
                    if ((long)vectorB[j].getSecond() == i) multiplication_result = (VGO_TYPE)weightA * (VGO_TYPE)vectorA[i] * (VGO_TYPE)vectorB[j].getFirst();
                    else if ((long)vectorB[j].getSecond() >= (long)sizeC) throw Exception("Incorrect vector size. The dimensionality of the sparse vector is greater than the dimensionality of the result vector.");
                    else multiplication_result = 0;
                }
                else multiplication_result = 0;
                
                vectorC[i] = (TC)(multiplication_result + weight_result);
            }
        }
    }
    
    /** Calculates the general vector multiplication (similar to the GEMM routines of the BLAS package) between sparse vectors: vectorC = weightA * vectorA * vectorB + weightB * vectorC;
     *  \param[in] vectorA tuples array from an sparse vector with the elements of the left-hand operand of the multiplication.
     *  \param[in] sizeA number of elements in tuples array of the left-hand operand.
     *  \param[in] vectorB tuples array from an sparse vector with the elements of the right-hand operand of the multiplication.
     *  \param[in] sizeB number of elements in tuples array of the right-hand operand.
     *  \param[in] weightA weight of the multiplication.
     *  \param[out] vectorC array where the result of the multiplication will be stored.
     *  \param[in] sizeC size of the result array.
     *  \param[in] weightB weight of the previous result array values added to the multiplication result.
     *  \param[in] number_of_threads number of threads used to concurrently process the multiplication.
     */
    template <class TA, class NA, class TB, class NB, class TC, class NC, class WA, class WB>
    void VectorGeneralOperator(const Tuple<TA, NA> *vectorA, const NA &sizeA, const Tuple<TB, NB> *vectorB, const NB &sizeB, const WA &weightA, TC *vectorC, const NC &sizeC, const WB &weightB, unsigned int number_of_threads)
    {
        typedef typename META_MGT<TA, TB>::TYPE OPERATOR_TYPE;
        typedef typename META_MGT<WA, WB>::TYPE WEIGHT_TYPE;
        typedef typename META_MGT<OPERATOR_TYPE, WEIGHT_TYPE>::TYPE VGO_TYPE;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (long i = omp_get_thread_num(), j = 0, k = 0; i < (long)sizeC; i += number_of_threads)
            {
                const VGO_TYPE weight_result = (VGO_TYPE)vectorC[i] * (VGO_TYPE)weightB;
                VGO_TYPE multiplication_result;
                
                while ((j < sizeA) && ((long)vectorA[j].getSecond() < i)) ++j;
                while ((k < sizeB) && ((long)vectorB[k].getSecond() < i)) ++k;
                
                if ((j < sizeA) && (k < sizeB))
                {
                    if (((long)vectorA[j].getSecond() == i) && ((long)vectorB[k].getSecond() == i)) multiplication_result = (VGO_TYPE)weightA * (VGO_TYPE)vectorA[j].getFirst() * (VGO_TYPE)vectorB[k].getFirst();
                    else if ((long)vectorA[j].getSecond() >= (long)sizeC) throw Exception("Incorrect vector size. The dimensionality of the left-hand operand sparse vector is greater than the dimensionality of the result vector.");
                    else if ((long)vectorB[k].getSecond() >= (long)sizeC) throw Exception("Incorrect vector size. The dimensionality of the right-hand operand sparse vector is greater than the dimensionality of the result vector.");
                    else multiplication_result = 0;
                }
                else multiplication_result = 0;
                
                vectorC[i] = (TC)(multiplication_result + weight_result);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MAXIMUM ELEMENT SEARCH FUNCTIONS ]=========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Finds the maximum of a dense vector.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \returns a constant reference to the maximum element of the dense vector (reference to the first maximum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMaximum(const T *elements, const N &size)
    {
        N selected_location = 0;
        for (N i = 1; i < size; ++i) if (elements[i] > elements[selected_location]) selected_location = i;
        return elements[selected_location];
    }
    
    /** Finds the maximum of a dense vector and return a list of locations.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \param[out] locations dense vector with the locations of the maximum elements.
     *  \returns a constant reference to the maximum element of the dense vector (reference to the first maximum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMaximum(const T *elements, const N &size, VectorDense<N> &locations)
    {
        N selected_location = 0, count = 1;
        for (N i = 1; i < size; ++i)
        {
            if (elements[i] > elements[selected_location])
            {
                selected_location = i;
                count = 1;
            }
            else if (elements[i] == elements[selected_location]) ++count;
        }
        locations.resize((unsigned int)count);
        for (N i = selected_location, j = 0; i < size; ++i)
        {
            if (elements[i] == elements[selected_location])
            {
                locations[(unsigned int)j] = i;
                ++j;
            }
        }
        return elements[selected_location];
    }
    
    /** Finds the maximum of an sparse vector.
     *  \param[in] elements a constant tuples array with the elements of an sparse array.
     *  \param[in] size number of elements in the tuples array.
     *  \returns a constant reference to the maximum element of the sparse vector (reference to the first maximum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMaximum(const Tuple<T, N> *elements, const N &size)
    {
        N selected_location = 0;
        for (N i = 1; i < size; ++i) if (elements[i].getFirst() > elements[selected_location].getFirst()) selected_location = i;
        return elements[selected_location].getFirst();
    }
    
    /** Finds the maximum of an sparse vector and return a list of locations.
     *  \param[in] elements a constant tuples array with the elements of an sparse array.
     *  \param[in] size number of elements in the tuples array.
     *  \param[out] locations dense vector with the locations of the maximum elements.
     *  \returns a constant reference to the maximum element of the sparse vector (reference to the first maximum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMaximum(const Tuple<T, N> *elements, const N &size, VectorDense<N> &locations)
    {
        N selected_location = 0, count = 1;
        for (N i = 1; i < size; ++i)
        {
            if (elements[i].getFirst() > elements[selected_location].getFirst())
            {
                selected_location = i;
                count = 1;
            }
            else if (elements[i].getFirst() == elements[selected_location].getFirst()) ++count;
        }
        locations.resize((unsigned int)count);
        for (N i = selected_location, j = 0; i < size; ++i)
        {
            if (elements[i].getFirst() == elements[selected_location].getFirst())
            {
                locations[(unsigned int)j] = i;
                ++j;
            }
        }
        return elements[selected_location].getFirst();
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MINIMUM ELEMENT SEARCH FUNCTIONS ]=========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Finds the minimum of a dense vector.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \returns a constant reference to the minimum element of the dense vector (reference to the first minimum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMinimum(const T *elements, const N &size)
    {
        N selected_location = 0;
        for (N i = 1; i < size; ++i) if (elements[i] < elements[selected_location]) selected_location = i;
        return elements[selected_location];
    }
    
    /** Finds the minimum of a dense vector and return a list of locations.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \param[out] locations dense vector with the locations of the minimum elements.
     *  \returns a constant reference to the minimum element of the dense vector (reference to the first minimum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMinimum(const T *elements, const N &size, VectorDense<N> &locations)
    {
        N selected_location = 0, count = 1;
        for (N i = 1; i < size; ++i)
        {
            if (elements[i] < elements[selected_location])
            {
                selected_location = i;
                count = 1;
            }
            else if (elements[i] == elements[selected_location]) ++count;
        }
        locations.resize((unsigned int)count);
        for (N i = selected_location, j = 0; i < size; ++i)
        {
            if (elements[i] == elements[selected_location])
            {
                locations[(unsigned int)j] = i;
                ++j;
            }
        }
        return elements[selected_location];
    }
    
    /** Finds the minimum of an sparse vector.
     *  \param[in] elements a constant tuples array with the elements of an sparse array.
     *  \param[in] size number of elements in the tuples array.
     *  \returns a constant reference to the minimum element of the sparse vector (reference to the first minimum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMinimum(const Tuple<T, N> *elements, const N &size)
    {
        N selected_location = 0;
        for (N i = 1; i < size; ++i) if (elements[i].getFirst() < elements[selected_location].getFirst()) selected_location = i;
        return elements[selected_location].getFirst();
    }
    
    /** Finds the minimum of an sparse vector and return a list of locations.
     *  \param[in] elements a constant tuples array with the elements of an sparse array.
     *  \param[in] size number of elements in the tuples array.
     *  \param[out] locations dense vector with the locations of the minimum elements.
     *  \returns a constant reference to the minimum element of the sparse vector (reference to the first minimum element found).
     */
    template <class T, class N>
    const T& VectorGeneralFindMinimum(const Tuple<T, N> *elements, const N &size, VectorDense<N> &locations)
    {
        N selected_location = 0, count = 1;
        for (N i = 1; i < size; ++i)
        {
            if (elements[i].getFirst() < elements[selected_location].getFirst())
            {
                selected_location = i;
                count = 1;
            }
            else if (elements[i].getFirst() == elements[selected_location].getFirst()) ++count;
        }
        locations.resize((unsigned int)count);
        for (N i = selected_location, j = 0; i < size; ++i)
        {
            if (elements[i].getFirst() == elements[selected_location].getFirst())
            {
                locations[(unsigned int)j] = i;
                ++j;
            }
        }
        return elements[selected_location].getFirst();
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ ELEMENT SEARCH FUNCTIONS ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Finds the location of an element in a dense vector.
     *  \param[in] searched element searched in the dense vector.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \param[out] locations indexes in the dense vector where the searched element can be found.
     */
    template <class T, class N>
    void VectorGeneralFindElement(const T &searched, const T *elements, const N &size, VectorDense<N> &locations)
    {
        N count = 0;
        for (N i = 0; i < size; ++i) if (elements[i] == searched) ++count;
        locations.resize((unsigned int)count);
        if (count > 0)
        {
            for (N i = 0, j = 0; i < size; ++i)
            {
                if (elements[i] == searched)
                {
                    locations[(unsigned int)j] = i;
                    ++j;
                }
            }
        }
    }
    
    /** Finds the location of an element in an sparse vector.
     *  \param[in] searched element searched in the dense vector.
     *  \param[in] elements a constant tuples array with the elements of an sparse array.
     *  \param[in] size number of elements in the tuples array.
     *  \param[out] locations indexes in the dense vector where the searched element can be found.
     */
    template <class T, class N>
    void VectorGeneralFindElement(const T &searched, const Tuple<T, N> *elements, const N &size, VectorDense<N> &locations)
    {
        N count = 0;
        for (N i = 0; i < size; ++i) if (elements[i].getFirst() == searched) ++count;
        locations.resize((unsigned int)count);
        if (count > 0)
        {
            for (N i = 0, j = 0; i < size; ++i)
            {
                if (elements[i].getFirst() == searched)
                {
                    locations[(unsigned int)j] = i;
                    ++j;
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOCAL MINIMA FUNCTIONS ]===================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Finds the location of local minima elements in a dense vector.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \param[in] window_size number of elements around the local minima that must have a higher value than the local minima.
     *  \param[out] locations indexes in the dense vector where the searched element can be found.
     *  \param[in] number_of_threads number of threads used to process the vector concurrently.
     */
    template <class T, class N>
    void VectorGeneralFindLocalMinima(const T *elements, const N &size, const N &window_size, VectorDense<N> &locations, unsigned int number_of_threads)
    {
        ChainedList<N> **chained_locations;
        N *chained_size, final_size;
        chained_locations = new ChainedList<N>*[number_of_threads];
        chained_size = new N[number_of_threads];
        for (unsigned int i = 0; i < number_of_threads; ++i)
        {
            chained_locations[i] = 0;
            chained_size[i] = 0;
        }
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = window_size + (N)thread_id; i < size - window_size; i += (N)number_of_threads)
            {
                bool extrema = true;
                for (N j = i - window_size; extrema && (j < i); ++j)
                    if (elements[j] <= elements[i]) extrema = false;
                for (N j = i + 1; extrema && (j < i + window_size + 1); ++j)
                    if (elements[j] <= elements[i]) extrema = false;
                if (extrema)
                {
                    chained_locations[thread_id] = new ChainedList<N>(i, chained_locations[thread_id]);
                    ++chained_size[thread_id];
                }
            }
        }
        
        final_size = chained_size[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) final_size += chained_size[i];
        locations.resize((unsigned int)final_size);
        
        for (N i = 0; i < final_size; ++i)
        {
            unsigned int selected = number_of_threads;
            N maximum_index = std::numeric_limits<N>::min();
            for (unsigned int j = 0; j < number_of_threads; ++j)
            {
                if ((chained_locations[j] != 0) && (chained_locations[j]->getData() > maximum_index))
                {
                    maximum_index = chained_locations[j]->getData();
                    selected = j;
                }
            }
            if (selected == number_of_threads) throw Exception("Something have gone terribly wrong while building the resulting sparse vector.");
            else
            {
                ChainedList<N> *current_ptr = chained_locations[selected];
                locations[(unsigned int)(final_size - i  - 1)] = chained_locations[selected]->getData();
                chained_locations[selected] = chained_locations[selected]->getNext();
                current_ptr->setNext(0);
                delete current_ptr;
            }
        }
        
        delete [] chained_locations;
        delete [] chained_size;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOCAL MAXIMA FUNCTIONS ]===================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Finds the location of local maxima elements in a dense vector.
     *  \param[in] elements a constant array with the elements of a dense array.
     *  \param[in] size number of elements in the array.
     *  \param[in] window_size number of elements around the local maxima that must have a lower value than the local maxima.
     *  \param[out] locations indexes in the dense vector where the searched element can be found.
     *  \param[in] number_of_threads number of threads used to process the vector concurrently.
     */
    template <class T, class N>
    void VectorGeneralFindLocalMaxima(const T *elements, const N &size, const N &window_size, VectorDense<N> &locations, unsigned int number_of_threads)
    {
        ChainedList<N> **chained_locations;
        N *chained_size, final_size;
        chained_locations = new ChainedList<N>*[number_of_threads];
        chained_size = new N[number_of_threads];
        for (unsigned int i = 0; i < number_of_threads; ++i)
        {
            chained_locations[i] = 0;
            chained_size[i] = 0;
        }
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = window_size + (N)thread_id; i < size - window_size; i += (N)number_of_threads)
            {
                bool extrema = true;
                for (N j = i - window_size; extrema && (j < i); ++j)
                    if (elements[j] >= elements[i]) extrema = false;
                for (N j = i + 1; extrema && (j < i + window_size); ++j)
                    if (elements[j] >= elements[i]) extrema = false;
                if (extrema)
                {
                    chained_locations[thread_id] = new ChainedList<N>(i, chained_locations[thread_id]);
                    ++chained_size[thread_id];
                }
            }
        }
        
        final_size = chained_size[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) final_size += chained_size[i];
        locations.resize(final_size);
        
        for (N i = 0; i < final_size; ++i)
        {
            unsigned int selected = number_of_threads;
            N maximum_index = std::numeric_limits<N>::min();
            for (unsigned int j = 0; j < number_of_threads; ++j)
            {
                if ((chained_locations[j] != 0) && (chained_locations[j]->getData() > maximum_index))
                {
                    maximum_index = chained_locations[j]->getData();
                    selected = j;
                }
            }
            if (selected == number_of_threads) throw Exception("Something have gone terribly wrong while building the resulting sparse vector.");
            else
            {
                ChainedList<N> *current_ptr = chained_locations[selected];
                locations[final_size - i  - 1] = chained_locations[selected]->getData();
                chained_locations[selected] = chained_locations[selected]->getNext();
                current_ptr->setNext(0);
                delete current_ptr;
            }
        }
        
        delete [] chained_locations;
        delete [] chained_size;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SUMMATION FUNCTIONS ]======================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T, class N>
    T VectorGeneralSum(const T* elements, const N &size, unsigned int number_of_threads)
    {
        VectorDense<T> partial_result(number_of_threads, 0);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (unsigned int i = thread_id; i < size; i += number_of_threads)
                partial_result[thread_id] = (T)(partial_result[thread_id] + elements[i]);
        }
        for (unsigned int i = 1; i < number_of_threads; ++i) partial_result[0] = (T)(partial_result[0] + partial_result[i]);
        return partial_result[0];
    }
    
    template <class T, class N, class R>
    void VectorGeneralSum(const T* elements, const N &size, R &result, unsigned int number_of_threads)
    {
        VectorDense<R> partial_result(number_of_threads, 0);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (unsigned int i = thread_id; i < (N)size; i += number_of_threads)
                partial_result[thread_id] = (R)(partial_result[thread_id] + (R)elements[i]);
        }
        result = partial_result[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) result = (R)(result + partial_result[i]);
    }
    
    template <class T, class N>
    T VectorGeneralSum(const Tuple<T, N>* elements, const N &size, unsigned int number_of_threads)
    {
        VectorDense<T> partial_result(number_of_threads, 0);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (unsigned int i = thread_id; i < (unsigned int)size; i += number_of_threads)
                partial_result[thread_id] = (T)(partial_result[thread_id] + elements[i].getFirst());
        }
        for (unsigned int i = 1; i < number_of_threads; ++i) partial_result[0] = (T)(partial_result[0] + partial_result[i]);
        return partial_result[0];
    }
    
    template <class T, class N, class R>
    void VectorGeneralSum(const Tuple<T, N>* elements, const N &size, R &result, unsigned int number_of_threads)
    {
        VectorDense<R> partial_result(number_of_threads, 0);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (unsigned int i = thread_id; i < (N)size; i += number_of_threads)
                partial_result[thread_id] = (R)(partial_result[thread_id] + (R)elements[i].getFirst());
        }
        result = partial_result[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) result = (R)(result + partial_result[i]);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ PRODUCT FUNCTIONS ]========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T, class N>
    T VectorGeneralProduct(const T* elements, const N &size, unsigned int number_of_threads)
    {
        VectorDense<T> partial_result(number_of_threads, 1);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = (N)thread_id; i < size; i += (N)number_of_threads) partial_result[thread_id] = (T)(partial_result[thread_id] * elements[i]);
        }
        for (unsigned int i = 1; i < number_of_threads; ++i) partial_result[0] = (T)(partial_result[0] * partial_result[i]);
        return partial_result[0];
    }
    
    template <class T, class N, class R>
    void VectorGeneralProduct(const T* elements, const N &size, R &result, unsigned int number_of_threads)
    {
        VectorDense<R> partial_result(number_of_threads, 1);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = (N)thread_id; i < size; i += (N)number_of_threads) partial_result[thread_id] = (R)(partial_result[thread_id] * (R)elements[i]);
        }
        result = partial_result[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) result = (R)(result * partial_result[i]);
    }
    
    template <class T, class N>
    T VectorGeneralProduct(const Tuple<T, N>* elements, const N &size, unsigned int number_of_threads)
    {
        VectorDense<T> partial_result(number_of_threads, 1);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = (N)thread_id; i < size; i += (N)number_of_threads) partial_result[thread_id] = (T)(partial_result[thread_id] * elements[i].getFirst());
        }
        for (unsigned int i = 1; i < number_of_threads; ++i) partial_result[0] = (T)(partial_result[0] * partial_result[i]);
        return partial_result[0];
    }
    
    template <class T, class N, class R>
    void VectorGeneralProduct(const Tuple<T, N>* elements, const N &size, R &result, unsigned int number_of_threads)
    {
        VectorDense<R> partial_result(number_of_threads, 1);
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int thread_id = omp_get_thread_num();
            for (N i = (N)thread_id; i < size; i += (N)number_of_threads) partial_result[thread_id] = (R)(partial_result[thread_id] * (R)elements[i].getFirst());
        }
        result = partial_result[0];
        for (unsigned int i = 1; i < number_of_threads; ++i) result = (R)(result * partial_result[i]);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ PRODUCT FUNCTIONS ]========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TA, class NA, class TB, class NB>
    void VectorGeneralAbs(const Tuple<TA, NA> *elementsA, const NA &sizeA, Tuple<TB, NB> *elementsB, const NB &sizeB, unsigned int number_of_threads)
    {
        if ((long)sizeA != (long)sizeB) throw Exception("Both vectors must have the same size");
        #pragma omp parallel num_threads(number_of_threads)
        {
            long thread_id = omp_get_thread_num();
            for (long i = thread_id; i < (long)sizeA; i += (long)number_of_threads)
            {
                elementsB[i].setFirst(srvAbs<TB>(elementsA[i].getFirst()));
                elementsB[i].setSecond(elementsA[i].getSecond());
            }
        }
    }
    
    template <class TA, class NA, class TB, class NB>
    void VectorGeneralAbs(const TA *elementsA, const NB &sizeA, TB *elementsB, const NB &sizeB, unsigned int number_of_threads)
    {
        if ((long)sizeA != (long)sizeB) throw Exception("Both vectors must have the same size");
        #pragma omp parallel num_threads(number_of_threads)
        {
            long thread_id = omp_get_thread_num();
            for (long i = thread_id; i < (long)sizeA; i += (long)number_of_threads)
                elementsB[i] = srvAbs<TB>(elementsA[i]);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VECTOR VALUE OPERATORS FOR THE       |
    //                   | DIFFERENT VECTOR DEFINED OPERATIONS  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ ARITHMETIC ]===============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which is used to calculate the result of adding the values of two vectors.
    class VectorValueAddition
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(value + value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first + second); }
    };
    
    /// Class which is used to calculate the result of subtracting the values of two vectors.
    class VectorValueSubtraction
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first - second); }
    };
    
    /// Class which is used to calculate the result of multiplying the values of two vectors.
    class VectorValueMultiplication
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(value * value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first * second); }
    };
    
    /// Class which is used to calculate the result of dividing the values of two vectors.
    class VectorValueDivision
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)1; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first / second); }
    };
   
    /// Class which is used to calculate the result of the module of the values of two vectors.
    class VectorValueModulo
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((long)first % (long)second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ COMPARISON ]===============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which compares that the values of two vectors are equal.
    class VectorValueEqual
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)1; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first == second); }
    };
    
    /// Class which compares that the values of two vectors are different.
    class VectorValueDifferent
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first != second); }
    };
    
    /// Class which compares that the values of two vectors are greater.
    class VectorValueGreater
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first > second); }
    };
    
    /// Class which compares that the values of two vectors are lesser.
    class VectorValueLesser
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first < second); }
    };
    
    /// Class which compares that the values of two vectors are greater or equal.
    class VectorValueGreaterEqual
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)1; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first >= second); }
    };
    
    /// Class which compares that the values of two vectors are lesser or equal.
    class VectorValueLesserEqual
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)1; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first <= second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOGIC OPERATORS ]==========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which calculates the logic AND between the elements of two vectors.
    class VectorValueLogicAnd
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(value && value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first && second); }
    };
    
    /// Class which calculates the logic OR between the elements of two vectors.
    class VectorValueLogicOr
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(value || value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)(first || second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ BITWISE OPERATORS ]========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which calculates the bitwise AND between the elements of two vectors.
    class VectorValueBitwiseAnd
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)((unsigned long)value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((unsigned long)first & (unsigned long)second); }
    };
    
    /// Class which calculates the bitwise OR between the elements of two vectors.
    class VectorValueBitwiseOr
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)((unsigned long)value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((unsigned long)first | (unsigned long)second); }
    };
    
    /// Class which calculates the bitwise XOR between the elements of two vectors.
    class VectorValueBitwiseXOR
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = 0; }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((unsigned long)first ^ (unsigned long)second); }
    };
    
    /// Class which calculates the left shift between the elements of two vectors.
    class VectorValueLeftShift
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)((unsigned long)value << (unsigned long)value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((unsigned long)first << (unsigned long)second); }
    };
    
    /// Class which calculates the right shift between the elements of two vectors.
    class VectorValueRightShift
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)((unsigned long)value >> (unsigned long)value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)((unsigned long)first >> (unsigned long)second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ UNARY OPERATORS ]==========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which calculates the bitwise not operator of the elements of a vector.
    class VectorValueBitwiseNot
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(~(unsigned long)value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, T &result) { result = (T)(~(unsigned long)first); }
    };
    
    /// Class which calculates the logic not operator of the elements of a vector.
    class VectorValueLogicNot
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(!value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, T &result) { result = (T)(!first); }
    };
    
    /// Class which calculates the negation operator of the elements of a vector.
    class VectorValueNegate
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &value) { value = (T)(-value); }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, T &result) { result = (T)(-first); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ OTHER OPERATORS ]==========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class which selects the minimum value between the elements of two vectors.
    class VectorValueMinimum
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &/*value*/) { }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)srvMin(first, second); }
    };
    
    /// Class which selects the maximum value between the elements of two vectors.
    class VectorValueMaximum
    {
    public:
        /** Static function which calculates the result operating the vector value by itself.
         *  \param[in,out] value vector value.
         */
        template <class T> static inline void self(T &/*value*/) { }
        /** Static function which calculates the result of operating two different vector values.
         *  \param[in] first left-hand operand value.
         *  \param[in] second right-hand operand value.
         *  \param[out] result where the result of the operation is stored.
         */
        template <class T, class U> static inline void calculate(const U &first, const U &second, T &result) { result = (T)srvMax(first, second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | ARITHMETIC OPERATORS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ ADDITION ]=================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Addition function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorAddition(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueAddition>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Addition function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorAddition(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueAddition>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Addition function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorAddition(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueAddition>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Addition function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorAddition(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueAddition>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Addition operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator+(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorAddition(vectorA, vectorB, result);
        return result;
    }
    
    /** Addition operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator+(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorAddition(vectorA, value, result); return result; }
    
    /** Addition operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator+(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorAddition(value, vectorB, result); return result; }
    
    /** Compound assignation-addition operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator+=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorAddition(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-addition operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator+=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorAddition(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SUBTRACTION ]==============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Subtraction function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorSubtraction(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueSubtraction>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Subtraction function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorSubtraction(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueSubtraction>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Subtraction function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorSubtraction(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueSubtraction>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Subtraction function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorSubtraction(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueSubtraction>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Subtraction operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator-(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorSubtraction(vectorA, vectorB, result);
        return result;
    }
    
    /** Subtraction operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator-(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorSubtraction(vectorA, value, result); return result; }
    
    /** Subtraction operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator-(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorSubtraction(value, vectorB, result); return result; }
    
    /** Compound assignation-subtraction operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator-=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorSubtraction(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-subtraction operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator-=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorSubtraction(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MULTIPLICATION ]===========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Multiplication function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorMultiplication(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMultiplication>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Multiplication function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorMultiplication(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMultiplication>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Multiplication function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMultiplication(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueMultiplication>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Multiplication function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMultiplication(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueMultiplication>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Multiplication operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator*(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorMultiplication(vectorA, vectorB, result);
        return result;
    }
    
    /** Multiplication operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator*(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorMultiplication(vectorA, value, result); return result; }
    
    /** Multiplication operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator*(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorMultiplication(value, vectorB, result); return result; }
    
    /** Compound assignation-multiplication operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator*=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorMultiplication(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-multiplication operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator*=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorMultiplication(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ DIVISION ]=================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Division function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorDivision(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueDivision>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Division function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorDivision(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueDivision>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Division function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorDivision(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueDivision>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Division function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorDivision(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueDivision>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Division operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator/(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorDivision(vectorA, vectorB, result);
        return result;
    }
    
    /** Division operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator/(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorDivision(vectorA, value, result); return result; }
    
    /** Division operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator/(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorDivision(value, vectorB, result); return result; }
    
    /** Compound assignation-division operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator/=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorDivision(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-division operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator/=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorDivision(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MODULO ]===================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Modulo function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorModulo(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueModulo>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Modulo function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorModulo(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueModulo>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Modulo function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorModulo(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueModulo>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Modulo function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorModulo(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueModulo>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Modulo operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator%(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorModulo(vectorA, vectorB, result);
        return result;
    }
    
    /** Modulo operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator%(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorModulo(vectorA, value, result); return result; }
    
    /** Modulo operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator%(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorModulo(value, vectorB, result); return result; }
    
    /** Compound assignation-modulo operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator%=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorModulo(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-modulo operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator%=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorModulo(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | COMPARISON OPERATORS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ EQUAL ]====================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Equal comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Equal comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Equal comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorEqual(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueEqual>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Equal comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorEqual(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueEqual>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Equal comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator==(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorEqual(vectorA, vectorB, result);
        return result;
    }
    
    /** Equal comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator==(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorEqual(vectorA, value, result); return result; }
    
    /** Equal comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator==(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorEqual(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ DIFFERENT ]================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Different comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorDifferent(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueDifferent>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Different comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorDifferent(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueDifferent>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Different comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorDifferent(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueDifferent>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Different comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorDifferent(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueDifferent>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Different comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator!=(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorDifferent(vectorA, vectorB, result);
        return result;
    }
    
    /** Different comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator!=(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorDifferent(vectorA, value, result); return result; }
    
    /** Different comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator!=(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorDifferent(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GREATER THAN ]=============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Greater-than comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorGreater(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueGreater>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorGreater(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueGreater>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Greater-than comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorGreater(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueGreater>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorGreater(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueGreater>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator>(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorGreater(vectorA, vectorB, result);
        return result;
    }
    
    /** Greater-than comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator>(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorGreater(vectorA, value, result); return result; }
    
    /** Greater-than comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator>(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorGreater(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LESSER THAN ]==============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Lesser-than comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLesser(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLesser>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorLesser(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLesser>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Lesser-than comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLesser(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueLesser>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLesser(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueLesser>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator<(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLesser(vectorA, vectorB, result);
        return result;
    }
    
    /** Lesser-than comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLesser(vectorA, value, result); return result; }
    
    /** Lesser-than comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator<(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorLesser(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GREATER THAN OR EQUAL TO ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Greater-than or equal-to comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorGreaterEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueGreaterEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than or equal-to comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorGreaterEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueGreaterEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Greater-than or equal-to comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorGreaterEqual(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueGreaterEqual>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than or equal-to comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorGreaterEqual(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueGreaterEqual>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Greater-than or equal-to comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator>=(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorGreaterEqual(vectorA, vectorB, result);
        return result;
    }
    
    /** Greater-than or equal-to comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator>=(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorGreaterEqual(vectorA, value, result); return result; }
    
    /** Greater-than or equal-to comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator>=(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorGreaterEqual(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LESSER THAN ]==============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Lesser-than or equal-to comparison function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLesserEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLesserEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than or equal-to comparison function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorLesserEqual(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLesserEqual>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Lesser-than or equal-to comparison function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLesserEqual(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueLesserEqual>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than or equal-to comparison function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLesserEqual(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueLesserEqual>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Lesser-than or equal-to comparison operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator<=(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLesserEqual(vectorA, vectorB, result);
        return result;
    }
    
    /** Lesser-than or equal-to comparison operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<=(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLesserEqual(vectorA, value, result); return result; }
    
    /** Lesser-than or equal-to comparison operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator<=(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorLesserEqual(value, vectorB, result); return result; }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | LOGIC OPERATORS                      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ LOGIC AND ]================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Logic AND function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicAnd(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLogicAnd>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic AND function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorLogicAnd(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLogicAnd>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Logic AND function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicAnd(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueLogicAnd>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic AND function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicAnd(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueLogicAnd>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic AND operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator&&(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLogicAnd(vectorA, vectorB, result);
        return result;
    }
    
    /** Logic AND operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator&&(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLogicAnd(vectorA, value, result); return result; }
    
    /** Logic AND operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator&&(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorLogicAnd(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOGIC OR ]=================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Logic OR function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicOr(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLogicOr>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic OR function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorLogicOr(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLogicOr>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Logic OR function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicOr(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueLogicOr>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic OR function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicOr(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueLogicOr>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic OR operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator||(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLogicOr(vectorA, vectorB, result);
        return result;
    }
    
    /** Logic OR operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator||(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLogicOr(vectorA, value, result); return result; }
    
    /** Logic OR operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator||(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorLogicOr(value, vectorB, result); return result; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BITWISE OPERATORS                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ Bitwise AND ]==============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise AND function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseAnd(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseAnd>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise AND function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorBitwiseAnd(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseAnd>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Bitwise AND function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseAnd(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueBitwiseAnd>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise AND function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseAnd(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueBitwiseAnd>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise AND operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator&(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorBitwiseAnd(vectorA, vectorB, result);
        return result;
    }
    
    /** Bitwise AND operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator&(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorBitwiseAnd(vectorA, value, result); return result; }
    
    /** Bitwise AND operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator&(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorBitwiseAnd(value, vectorB, result); return result; }
    
    /** Compound assignation-bitwise AND operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator&=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorBitwiseAnd(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-bitwise AND operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator&=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorBitwiseAnd(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Bitwise OR ]===============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise OR function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseOr(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseOr>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise OR function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorBitwiseOr(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseOr>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Bitwise OR function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseOr(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueBitwiseOr>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise OR function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseOr(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueBitwiseOr>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise OR operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator|(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorBitwiseOr(vectorA, vectorB, result);
        return result;
    }
    
    /** Bitwise OR operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator|(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorBitwiseOr(vectorA, value, result); return result; }
    
    /** Bitwise OR operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator|(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorBitwiseOr(value, vectorB, result); return result; }
    
    /** Compound assignation-bitwise OR operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator|=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorBitwiseOr(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-bitwise OR operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator|=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorBitwiseOr(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Bitwise XOR ]==============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise XOR function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseXOR(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseXOR>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise XOR function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorBitwiseXOR(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueBitwiseXOR>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Bitwise XOR function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseXOR(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueBitwiseXOR>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise XOR function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseXOR(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueBitwiseXOR>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise XOR operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE operator^(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VECTORA, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorBitwiseXOR(vectorA, vectorB, result);
        return result;
    }
    
    /** Bitwise XOR operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator^(const VECTORA<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorBitwiseXOR(vectorA, value, result); return result; }
    
    /** Bitwise XOR operator between a value and a vector.
     *  \param[in] vectorA left-hand value operand.
     *  \param[in] value right-hand vector operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NB> operator^(const TA &value, const VECTORB<TB, NB> &vectorB) { VectorDense<typename META_MGT<TA, TB>::TYPE, NB> result(vectorB.size(), 0); VectorBitwiseXOR(value, vectorB, result); return result; }
    
    /** Compound assignation-bitwise XOR operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator^=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorBitwiseXOR(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-bitwise XOR operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator^=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorBitwiseXOR(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Bitwise left shift ]=======================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise left shift function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLeftShift(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLeftShift>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise left shift function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorLeftShift(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueLeftShift>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Bitwise left shift function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLeftShift(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueLeftShift>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise left shift function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorLeftShift(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueLeftShift>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /* *** TRICKY PART: Avoid problems with 'stream' operator<< defining the shift operator for each vector type in the left-hand operand. *** */
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const VectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand sparse vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const VectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand dense sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<SubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const SubVectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<SubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand sparse sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<SubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const SubVectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<SubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand constant dense sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<ConstantSubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const ConstantSubVectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<ConstantSubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand constant sparse sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator<<(const ConstantSubVectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorLeftShift(vectorA, vectorB, result);
        return result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------
    
    /* *** TRICKY PART: Avoid problems with 'stream' operator<< defining the shift operator for each vector type in the left-hand operand. *** */
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const VectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand sparse vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const VectorSparse<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand dense sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const SubVectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand sparse sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const SubVectorSparse<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand constant dense sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const ConstantSubVectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    /** Bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand constant sparse sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, class TB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator<<(const ConstantSubVectorSparse<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorLeftShift(vectorA, value, result); return result; }
    // ------------------------------------------------------------------------------------------------------------------------------------------
    
    /** Compound assignation-bitwise left shift operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator<<=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorLeftShift(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-bitwise left shift operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator<<=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorLeftShift(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Bitwise right shift ]======================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise right shift function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorRightShift(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueRightShift>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise right shift function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorRightShift(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueRightShift>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Bitwise right shift function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorRightShift(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueRightShift>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Bitwise right shift function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorRightShift(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueRightShift>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /* *** TRICKY PART: Avoid problems with 'stream' operator>> defining the shift operator for each vector type in the left-hand operand. *** */
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const VectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand sparse vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<VectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const VectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<VectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand dense sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<SubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const SubVectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<SubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand sparse sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<SubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const SubVectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<SubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand constant dense sub-vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<ConstantSubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const ConstantSubVectorDense<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<ConstantSubVectorDense, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    /** Bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a dense or an sparse vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE operator>>(const ConstantSubVectorSparse<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB)
    {
        typedef typename META_MGT<NA, NB>::TYPE OUTPUT_N;
        typename OUTPUT_VECTOR<ConstantSubVectorSparse, TA, NA, VECTORB, TB, NB>::TYPE result(srvMax<OUTPUT_N>(vectorA.size(), vectorB.size()));
        result.setValue(0);
        VectorRightShift(vectorA, vectorB, result);
        return result;
    }
    // ------------------------------------------------------------------------------------------------------------------------------------------
    
    /* *** TRICKY PART: Avoid problems with 'stream' operator>> defining the shift operator for each vector type in the left-hand operand. *** */
    /* --------------------------------------------------------------------------------------------------------------------------------------- */
    /** Bitwise right shift operator between a vector and a value.
     *  \param[in] vectorA left-hand dense vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator>>(const VectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorRightShift(vectorA, value, result); return result; }
    /** Bitwise right shift operator between a vector and a value.
     *  \param[in] vectorA left-hand dense sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator>>(const SubVectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorRightShift(vectorA, value, result); return result; }
    /** Bitwise right shift operator between a vector and a value.
     *  \param[in] vectorA left-hand constant dense sub-vector operand.
     *  \param[in] value right-hand value operand.
     *  \returns a dense vector with the operation result.
     */
    template <class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VectorDense<typename META_MGT<TA, TB>::TYPE, NA> operator>>(const ConstantSubVectorDense<TA, NA> &vectorA, const TB &value) { VectorDense<typename META_MGT<TA, TB>::TYPE, NA> result(vectorA.size(), 0); VectorRightShift(vectorA, value, result); return result; }
    // ------------------------------------------------------------------------------------------------------------------------------------------
    
    /** Compound assignation-bitwise right shift operator between vectors.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline VECTORA<TA, NA>& operator>>=(VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB) { VectorRightShift(vectorA, vectorB, vectorA); return vectorA; }
    
    /** Compound assignation-bitwise right shift operator between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] vectorB right-hand value operand.
     *  \returns a reference to the left-hand vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB>
    inline VECTORA<TA, NA>& operator>>=(VECTORA<TA, NA> &vectorA, const TB &value) { VectorRightShift(vectorA, value, vectorA); return vectorA; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | UNARY OPERATORS                      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ BITWISE NOT OPERATOR ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Bitwise NOT function between vectors which result in an dense vector.
     *  \param[in] vectorA origin vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorBitwiseNot(const VECTORA<TA, NA> &vectorA, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic())
    {
        VectorGeneralOperator<TA, NA, TC, NC, VectorValueBitwiseNot>(vectorA.getData(), vectorA.size(), vectorC.getData(), vectorC.size(), number_of_threads);
    }
    
    /** Bitwise NOT operator.
     *  \param[in] input_vector input vector or sub-vector.
     *  \return resulting vector of the same type as the input vector.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE operator~(const VECTOR<T, N> &input_vector)
    {
        typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE result(input_vector.size());
        VectorBitwiseNot(input_vector, result);
        return result;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ NEGATION OPERATOR ]========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Negate function between vectors which result in an dense vector.
     *  \param[in] vectorA origin vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorNegate(const VECTORA<TA, NA> &vectorA, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic())
    {
        VectorGeneralOperator<TA, NA, TC, NC, VectorValueNegate>(vectorA.getData(), vectorA.size(), vectorC.getData(), vectorC.size(), number_of_threads);
    }
    
    /** Negate operator.
     *  \param[in] input_vector input vector or sub-vector.
     *  \return resulting vector of the same type as the input vector.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE operator-(const VECTOR<T, N> &input_vector)
    {
        typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE result(input_vector.size());
        VectorNegate(input_vector, result);
        return result;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOGIC NOT ]================================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Logic NOT function between vectors which result in an dense vector.
     *  \param[in] vectorA origin vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorLogicNot(const VECTORA<TA, NA> &vectorA, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TC, NC, VectorValueLogicNot>(vectorA.getData(), vectorA.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Logic NOT operator.
     *  \param[in] input_vector input vector or sub-vector.
     *  \return resulting vector of the same type as the input vector.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE operator!(const VECTOR<T, N> &input_vector)
    {
        typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE result(input_vector.size());
        VectorLogicNot(input_vector, result);
        return result;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | OTHER OPERATORS                      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ MINIMUM FUNCTION ]=========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Minimum function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorMinimum(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMinimum>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Minimum function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorMinimum(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMinimum>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Minimum function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMinimum(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueMinimum>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Minimum function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMinimum(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueMinimum>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MAXIMUM FUNCTION ]=========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Maximum function between vectors which result in an dense vector.
     *  \param[in] vectorA left-hand vector or sub-vector operand.
     *  \param[in] vectorB right-hand vector or sub-vector operand.
     *  \param[out] vectorC vector where the result is stored. This vector cannot be a sparse vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This functions does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorMaximum(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMaximum>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Maximum function between sparse vectors.
     *  \param[in] vectorA left-hand sparse vector or sub-vector operand.
     *  \param[in] vectorB right-hand sparse vector or sub-vector operand.
     *  \param[out] vectorC sparse vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class TC, class NC>
    inline void VectorMaximum(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, VectorSparse<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, NB, TC, NC, VectorValueMaximum>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), vectorC, number_of_threads); }
    
    /** Maximum function between a vector and a value.
     *  \param[in] vectorA left-hand vector operand.
     *  \param[in] value right-hand value operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <template <class, class> class VECTORA, class TA, class NA, class TB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMaximum(const VECTORA<TA, NA> &vectorA, const TB &value, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, NA, TB, TC, NC, VectorValueMaximum>(vectorA.getData(), vectorA.size(), value, vectorC.getData(), vectorC.size(), number_of_threads); }
    
    /** Maximum function between a vector and a value.
     *  \param[in] value left-hand value operand.
     *  \param[in] vectorB right-hand vector operand.
     *  \param[out] vectorC vector where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \note This function does not allocate memory for the resulting vector. Therefore a <i>segmentation fault</i> can be generated for vectors with incorrect sizes.
     *  \note Sparse type vectors (i.e. sparse vector or sub-vector) cannot be combined with dense type vectors (i.e. dense vector or sub-vector).
     */
    template <class TA, template <class, class> class VECTORB, class TB, class NB, template<class, class> class VECTORC, class TC, class NC>
    inline void VectorMaximum(const TA &value, const VECTORB<TB, NB> &vectorB, VECTORC<TC, NC> &vectorC, unsigned long number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator<TA, TB, NB, TC, NC, VectorValueMaximum>(value, vectorB.getData(), vectorB.size(), vectorC.getData(), vectorC.size(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GENERAL VECTOR MULTIPLICATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Function which calculates the "general" product between two vectors of any type and stores the result in a dense vector. The function weights
     *  the contribution of the multiplication and the incremental addition to the result vector similar to the BLAS GEMM routines for matrices.
     *  \param[in] wm weight of the vector multiplication result.
     *  \param[in] vectorA left-hand vector or sub-vector operand of the multiplication.
     *  \param[in] vectorB right-hand vector or sub-vector operand of the multiplication.
     *  \param[in] ws weight assigned to the previous output values which will be added to the result.
     *  \param[in,out] vectorC result dense vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <class WA, template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class WB, template <class, class> class VECTORC, class TC, class NC>
    inline void VectorGeneralMultiplication(const WA &wm, const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, const WB &ws, VECTORC<TC, NC> &vectorC, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralOperator(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), wm, vectorC.getData(), vectorC.size(), ws, number_of_threads); }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VECTOR AUXILIARY OPERATION FUNCTIONS |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Returns the maximum value of a vector.
     *  \param[in] vector input vector or sub-vector.
     *  \returns a constant reference to the first occurrence of the vector maximum value.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline const T& VectorFindMaximum(const VECTOR<T, N> &vector) { return VectorGeneralFindMaximum(vector.getData(), vector.size()); }
    
    /** Returns the maximum value of a vector and its location in the vector.
     *  \param[in] vector input vector or sub-vector.
     *  \param[out] locations a dense vector with the location of the maximum value. This vector contains as many location as times that the maximum value is present in the vector.
     *  \returns a constant reference to the first occurrence of the vector maximum value.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline const T& VectorFindMaximum(const VECTOR<T, N> &vector, VectorDense<N> &locations) { return VectorGeneralFindMaximum(vector.getData(), vector.size(), locations); }
    
    /** Returns the minimum value of a vector.
     *  \param[in] vector input vector or sub-vector.
     *  \returns a constant reference to the first occurrence of the vector minimum value.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline const T& VectorFindMinimum(const VECTOR<T, N> &vector) { return VectorGeneralFindMinimum(vector.getData(), vector.size()); }
    
    /** Returns the minimum value of a vector and its location in the vector.
     *  \param[in] vector input vector or sub-vector.
     *  \param[out] locations a dense vector with the location of the minimum value. This vector contains as many location as times that the minimum value is present in the vector.
     *  \returns a constant reference to the first occurrence of the vector minimum value.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline const T& VectorFindMinimum(const VECTOR<T, N> &vector, VectorDense<N> &locations) { return VectorGeneralFindMinimum(vector.getData(), vector.size(), locations); }
    
    /** Search an element in a vector and returns the locations where it can be found.
     *  \param[in] element searched element.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] locations locations where the element can be found.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline void VectorFindElement(const T &element, const VECTOR<T, N> &vector, VectorDense<N> &locations) { VectorGeneralFindElement(element, vector.getData(), vector.size(), locations); }
    
    /** Locates the local maxima values in a vector.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] window_size window around the local maxima where no other higher or equal value can be found.
     *  \param[out] locations dense vector with the location of the local maxima values.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline void VectorFindLocalMaxima(const VECTOR<T, N> &vector, const N &window_size, VectorDense<N> &locations, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralFindLocalMaxima(vector.getData(), vector.size(), window_size, locations, number_of_threads); }
    
    /** Locates the local minima values in a vector.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] window_size window around the local minima where no other lower or equal value can be found.
     *  \param[out] locations dense vector with the location of the local minima values.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline void VectorFindLocalMinima(const VECTOR<T, N> &vector, const N &window_size, VectorDense<N> &locations, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralFindLocalMinima(vector.getData(), vector.size(), window_size, locations, number_of_threads); }
    
    /** Returns the summation of all vector values.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline T VectorSum(const VECTOR<T, N> &vector, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { return VectorGeneralSum(vector.getData(), vector.size(), number_of_threads); }
    
    /** Returns the summation of all vector values.
     *  \param[in] vector input vector or sub-vector.
     *  \param[out] result summation result.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N, class R>
    inline void VectorSum(const VECTOR<T, N> &vector, R &result, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralSum(vector.getData(), vector.size(), result, number_of_threads); }
    
    /** Returns the product of all vector values.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline T VectorProduct(const VECTOR<T, N> &vector, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { return VectorGeneralProduct(vector.getData(), vector.size(), number_of_threads); }
    
    /** Returns the product of all vector values.
     *  \param[out] result product result.
     *  \param[in] vector input vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to concurrently process the vectors. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTOR, class T, class N, class R>
    inline void VectorProduct(R &result, const VECTOR<T, N> &vector, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralProduct(vector.getData(), vector.size(), result, number_of_threads); }
    
    /** Calculates the absolute value of each element of the given vector or sub-vector.
     *  \param[in] vectorA input vector or sub-vector.
     *  \param[out] vectorB output vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to process the vectors concurrently. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline void VectorAbs(const VECTORA<TA, NA> &vectorA, VECTORB<TB, NB> &vectorB, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic()) { VectorGeneralAbs<TA, NA, TB, NB>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), number_of_threads); }
    
    /** Calculates the absolute value of each element of the given vector or sub-vector.
     *  \param[in] input_vector input vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to process the vector concurrently. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \returns a new vector containing the absolute value of the input vector data.
     */
    template <template <class, class> class VECTOR, class T, class N>
    inline typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE VectorAbs(const VECTOR<T, N> &input_vector, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic())
    {
        typename OUTPUT_VECTOR<VECTOR, T, N, VECTOR, T, N>::TYPE result_vector(input_vector.size());
        VectorGeneralAbs<T, N, T, N>(input_vector.getData(), input_vector.size(), result_vector.getData(), result_vector.size(), number_of_threads);
        return result_vector;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOT PRODUCT OPERATOR FUNCTIONS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** General operator function which calculates the dot product between two dense vectors.
     *  \param[in] vectorA array with the data of the first vector.
     *  \param[in] sizeA number of elements in the <b>vectorA</b> array.
     *  \param[in] vectorB array with the data of the second vector.
     *  \param[in] sizeB number of elements in the <b>vectorB</b> array.
     *  \param[out] dot_product resulting dot product.
     *  \param[in] number_of_threads number of threads used to calculate the dot product.
     */
    template <class TA, class NA, class TB, class NB, class R>
    void VectorGeneralDot(const TA * vectorA, const NA &sizeA, const TB * vectorB, const NB &sizeB, R &dot_product, unsigned long number_of_threads)
    {
        if ((unsigned long)sizeA != (unsigned long)sizeB) throw Exception("Incorrect vector sizes.");
        VectorDense<R> result((unsigned int)number_of_threads, 0);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned long thread_id = omp_get_thread_num();
            for (unsigned long i = thread_id; i < (unsigned long)sizeA; i += number_of_threads)
                result[(unsigned int)thread_id] = (R)(result[(unsigned int)thread_id] + (R)vectorA[i] * (R)vectorB[i]);
        }
        dot_product = result[0];
        for (unsigned long i = 1; i < number_of_threads; ++i) dot_product += result[(unsigned int)i];
    }
    
    /** General operator function which calculates the dot product between a dense vector and a sparse vector.
     *  \param[in] vectorA array with the data of the first vector.
     *  \param[in] sizeA number of elements in the <b>vectorA</b> array.
     *  \param[in] vectorB array with the data of the second vector.
     *  \param[in] sizeB number of elements in the <b>vectorB</b> array.
     *  \param[out] dot_product resulting dot product.
     *  \param[in] number_of_threads number of threads used to calculate the dot product.
     */
    template <class TA, class NA, class TB, class NB, class R>
    void VectorGeneralDot(const TA * vectorA, const NA &sizeA, const Tuple<TB, NB> * vectorB, const NB &sizeB, R &dot_product, unsigned long number_of_threads)
    {
        VectorDense<R> result((unsigned int)number_of_threads, 0);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned long thread_id = omp_get_thread_num();
            for (unsigned long i = thread_id; i < (unsigned long)sizeB; i += number_of_threads)
                if ((unsigned long)vectorB[i].getSecond() < (unsigned long)sizeA)
                    result[(unsigned int)thread_id] = (R)(result[(unsigned int)thread_id] + (R)vectorA[vectorB[i].getSecond()] * (R)vectorB[i].getFirst());
        }
        dot_product = result[0];
        for (unsigned long i = 1; i < number_of_threads; ++i) dot_product += result[(unsigned int)i];
    }
    
    /** General operator function which calculates the dot product between a sparse vector and a dense vector.
     *  \param[in] vectorA array with the data of the first vector.
     *  \param[in] sizeA number of elements in the <b>vectorA</b> array.
     *  \param[in] vectorB array with the data of the second vector.
     *  \param[in] sizeB number of elements in the <b>vectorB</b> array.
     *  \param[out] dot_product resulting dot product.
     *  \param[in] number_of_threads number of threads used to calculate the dot product.
     */
    template <class TA, class NA, class TB, class NB, class R>
    void VectorGeneralDot(const Tuple<TA, NA> * vectorA, const NA &sizeA, const TB * vectorB, const NB &sizeB, R &dot_product, unsigned long number_of_threads)
    {
        VectorDense<R> result((unsigned int)number_of_threads, 0);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned long thread_id = omp_get_thread_num();
            for (unsigned long i = thread_id; i < (unsigned long)sizeA; i += number_of_threads)
                if ((unsigned long)vectorA[i].getSecond() < (unsigned long)sizeB)
                    result[(unsigned int)thread_id] = (R)(result[(unsigned int)thread_id] + (R)vectorB[vectorA[i].getSecond()] * (R)vectorA[i].getFirst());
        }
        dot_product = result[0];
        for (unsigned long i = 1; i < number_of_threads; ++i) dot_product += result[(unsigned int)i];
    }
   
    /** General operator function which calculates the dot product between two sparse vectors.
     *  \param[in] vectorA array with the data of the first vector.
     *  \param[in] sizeA number of elements in the <b>vectorA</b> array.
     *  \param[in] vectorB array with the data of the second vector.
     *  \param[in] sizeB number of elements in the <b>vectorB</b> array.
     *  \param[out] dot_product resulting dot product.
     *  \param[in] number_of_threads number of threads used to calculate the dot product.
     */
    template <class TA, class NA, class TB, class NB, class R>
    void VectorGeneralDot(const Tuple<TA, NA> * vectorA, const NA &sizeA, const Tuple<TB, NB> * vectorB, const NB &sizeB, R &dot_product, unsigned long number_of_threads)
    {
        VectorDense<R> result((unsigned int)number_of_threads, 0);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned long thread_id = omp_get_thread_num();
            
            if ((unsigned long)sizeA > (unsigned long)sizeB)
            {
                for (unsigned long indexA = thread_id, indexB = 0; (indexA < (unsigned long)sizeA) && (indexB < (unsigned long)sizeB); indexA += number_of_threads)
                {
                    while ((indexB < (unsigned long)sizeB) && (vectorB[indexB].getSecond() < vectorA[indexA].getSecond())) ++indexB;
                    if (vectorB[indexB].getSecond() == vectorA[indexA].getSecond())
                        result[(unsigned int)thread_id] = (R)(result[(unsigned int)thread_id] + (R)vectorA[indexA].getFirst() * (R)vectorB[indexB].getFirst());
                }
            }
            else
            {
                for (unsigned long indexA = 0, indexB = thread_id; (indexA < (unsigned long)sizeA) && (indexB < (unsigned long)sizeB); indexB += number_of_threads)
                {
                    while ((indexA < (unsigned long)sizeA) && (vectorA[indexA].getSecond() < vectorB[indexB].getSecond())) ++indexA;
                    if (vectorB[indexB].getSecond() == vectorA[indexA].getSecond())
                        result[(unsigned int)thread_id] = (R)(result[(unsigned int)thread_id] + (R)vectorA[indexA].getFirst() * (R)vectorB[indexB].getFirst());
                }
            }
        }
        dot_product = result[0];
        for (unsigned long i = 1; i < number_of_threads; ++i) dot_product += result[(unsigned int)i];
    }
    
    /** Calculates the dot product between two vectors or sub-vectors.
     *  \param[in] vectorA first vector or sub-vector.
     *  \param[in] vectorB second vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to process the vectors concurrently. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \returns the dot product between both vectors.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename META_MGT<TA, TB>::TYPE VectorDot(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic())
    {
        typename META_MGT<TA, TB>::TYPE dot_product;
        VectorGeneralDot<TA, NA, TB, NB>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), dot_product, number_of_threads);
        return dot_product;
    }
    
    /** Calculates the dot product between two vectors or sub-vectors.
     *  \param[in] vectorA first vector or sub-vector.
     *  \param[in] vectorB second vector or sub-vector.
     *  \param[in] number_of_threads number of threads used to process the vectors concurrently. By default set to the number of threads specified by \b VectorDefaultNumberOfThreads::VectorArithmetic().
     *  \returns the dot product between both vectors.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, class R>
    inline void VectorDot(const VECTORA<TA, NA> &vectorA, const VECTORB<TB, NB> &vectorB, R &dot_product, unsigned int number_of_threads = VectorDefaultNumberOfThreads::VectorArithmetic())
    {
        VectorGeneralDot<TA, NA, TB, NB>(vectorA.getData(), vectorA.size(), vectorB.getData(), vectorB.size(), dot_product, number_of_threads);
    }
    
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif


