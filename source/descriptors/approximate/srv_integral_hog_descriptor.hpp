// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_INTEGRAL_HOG_DESCRIPTOR_HEADER_FILE__
#define __SRV_INTEGRAL_HOG_DESCRIPTOR_HEADER_FILE__

#include "../srv_dense_descriptors.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | INTEGRAL HOG DESCRIPTOR CLASS        |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Integral Histogram of Gradients descriptors presented in:
     *  Q. Zhu, M.C. Yeh, K-T. Cheng, S. Avidan, <b>"Fast Human Detection using a Cascade of Histograms of Oriented Gradients"</b>, <i>CVPR 2006</i>
     *  This algorithm uses integral images both calculate the image gradient and to accumulate the image features into the descriptor, so that, it greatly
     *  reduces the computational cost of the descriptor extraction algorithm.
     *  \tparam TFILTER type of the Gaussian filter images.
     *  \tparam TFEATURE type of the feature images.
     */
    template <class TFILTER, class TFEATURE>
    class IntegralHOG : public DenseDescriptor<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        IntegralHOG(void);
        /** Constructor which initializes the parameters of the descriptor.
         *  \param[in] number_of_orientation_bins number of bins used to quantize the orientation of the gradient.
         *  \param[in] partitions_x number of divisions in the X-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] partitions_y number of divisions in the Y-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] color_method identifier of the color method applied to the images to extract the features from different color spaces.
         */
        IntegralHOG(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method);
        /** Constructor which initializes the parameters of the descriptor and the region sampling parameters.
         *  \param[in] number_of_orientation_bins number of bins used to quantize the orientation of the gradient.
         *  \param[in] partitions_x number of divisions in the X-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] partitions_y number of divisions in the Y-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] color_method identifier of the color method applied to the images to extract the features from different color spaces.
         *  \param[in] scales_information vector storing the information of the region size and step between pixels (with tuples [size, step]).
         *  \param[in] sigma_ratio ratio between the region size and the size of the Haar-like features used to calculate the image gradient.
         */
        template <template <class, class> class VECTOR>
        IntegralHOG(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method, VECTOR<unsigned int, unsigned int> &scales_information, double sigma_ratio);
        /// Copy constructor.
        IntegralHOG(const IntegralHOG<TFILTER, TFEATURE> &other);
        /// Destructor.
        ~IntegralHOG(void);
        /// Assignation operator.
        IntegralHOG<TFILTER, TFEATURE>& operator=(const IntegralHOG<TFILTER, TFEATURE> &other);
        
        // -[ Set descriptor parameters ]------------------------------------------------------------------------------------------------------------
        /** Constructor which initializes the parameters of the descriptor and the region sampling parameters.
         *  \param[in] number_of_orientation_bins number of bins used to quantize the orientation of the gradient.
         *  \param[in] partitions_x number of divisions in the X-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] partitions_y number of divisions in the Y-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] color_method identifier of the color method applied to the images to extract the features from different color spaces.
         *  \param[in] scales_information vector storing the information of the region size and step between pixels (with tuples [size, step]).
         *  \param[in] sigma_ratio ratio between the region size and the size of the Haar-like features used to calculate the image gradient.
         */
        template <template <class, class> class VECTOR>
        void set(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method, VECTOR<unsigned int, unsigned int> &scales_information, double sigma_ratio);
        /// This function sets the number of bins used to quantize the orientation of the gradient.
        void setNumberOfOrientationBins(unsigned int number_of_orientation_bins);
        /** This function sets the number of partitions in the X- and Y-directions used to create the spatial bins (number of spatial bins = partitions_x * partitions_y).
         *  \param[in] partitions_x number of divisions in the X-direction used to generate the spatial partitions of the descriptor.
         *  \param[in] partitions_y number of divisions in the Y-direction used to generate the spatial partitions of the descriptor.
         */
        void setDescriptorPartitions(unsigned int partitions_x, unsigned int partitions_y);
        
        // -[ Descriptor object information functions ]----------------------------------------------------------------------------------------------
        /// Returns the number of orientation bins of the descriptors.
        inline unsigned int getNumberOfOrientationBins(void) const { return m_number_of_orientation_bins; }
        /// Returns a constant array with the angles associated to each orientation bin.
        inline const double * getAngles(void) const { return m_angle; }
        /// Returns the value of the angle associated with the index-th orientation bin.
        inline double getAngle(unsigned int index) const { return m_angle[index]; }
        /// Returns a constant pointer to the array with the cosine values corresponding to each orientation bin.
        inline const double * getCosines(void) const { return m_cosine; }
        /// Returns the cosine associates with the index-th orientation bin.
        inline double getCosine(unsigned int index) const { return m_cosine[index]; }
        /// Returns a constant pointer to the array with the sine values corresponding to each orientation bin.
        inline double * getSines(void) const { return m_sine; }
        /// Returns the sine associated with the index-th orientation bin.
        inline double getSine(unsigned int index) const { return m_sine[index]; }
        
        /// Returns the number of partitions in the X-direction used to generate the spatial bins of the descriptor.
        inline unsigned int getPartitionsX(void) const { return m_partitions_x; }
        /// Returns the number of partitions in the Y-direction used to generate the spatial bins of the descriptor.
        inline unsigned int getPartitionsY(void) const { return m_partitions_y; }
        /// Returns a constant pointer to the array with the width values of the spatial bins of each scale.
        inline const unsigned int * getPartitionWidths(void) const { return m_partition_width; }
        /// Returns the width of the spatial bin of the index-th scale.
        inline unsigned int getPartitionWidth(unsigned int index) const { return m_partition_width[index]; }
        /// Returns a constant pointer to the array with the height values of the spatial bins of each scale.
        inline const unsigned int * getPartitionHeights(void) const { return m_partition_height; }
        /// Returns the height of the spatial bin of the index-th scale.
        inline unsigned int getPartitionHeight(unsigned int index) const { return m_partition_height[index]; }
        /// Returns the number of bins of the descriptors.
        inline unsigned int getNumberOfBins(unsigned int number_of_channels = 1) const
        {
            if      (this->m_color_method <= DD_MAXIMUM_CHANNEL     ) number_of_channels = 1;
            else if (this->m_color_method <  DD_INDEPENDENT_CHANNELS) number_of_channels = 3;
            return number_of_channels * this->m_number_of_feature_bins * this->m_number_of_spatial_bins;
        }
        
        // -[ Access functions to region sampling parameters ]---------------------------------------------------------------------------------------
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information array of tuples where the first element is the size of the region and the second is the step both in pixels.
         *  \param[in] number_of_scales number of elements in the array.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        void setScalesParameters(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        inline DenseDescriptor<TFILTER, TFEATURE> * duplicate(void) const { return (DenseDescriptor<TFILTER, TFEATURE> *)new IntegralHOG<TFILTER, TFEATURE>(*this); }
        /// Returns the class identifier for the current dense descriptor method.
        inline static DDESCRIPTOR_IDENTIFIER getClassIdentifier(void) { return DD_INTEGRAL_HOG; }
        /// Generates a new empty instance of the dense descriptor.
        inline static DenseDescriptor<TFILTER, TFEATURE> * generateObject(void) { return (DenseDescriptor<TFILTER, TFEATURE> *)new IntegralHOG<TFILTER, TFEATURE>(); }
        /// Returns a flag which states if the dense descriptor class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the dense descriptor type identifier of the current object.
        inline DDESCRIPTOR_IDENTIFIER getIdentifier(void) const { return DD_INTEGRAL_HOG; }
        
    protected:
        typedef typename DenseDescriptor<TFILTER, TFEATURE>::ScaleInformation ScaleInformation;
        // -[ Auxiliary functions ]------------------------------------------------------------------------------------------------------------------
        /// This function initializes the values of the orientation bins.
        void initializeOrientationBins(void);
        // -[ Virtual descriptor extraction functions ]----------------------------------------------------------------------------------------------
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        void protectedExtract(const ConstantSubImage<unsigned char> &image, const VectorDense<DescriptorLocation> &locations, VectorDense<double> * descriptors, unsigned int number_of_threads) const;
        /** This function normalizes the descriptors and rejects descriptors which are below the descriptor threshold norm.
         *  \param[in] number_of_channels number of channels of the work image.
         *  \param[in,out] original_descriptors array with descriptors which are normalized.
         *  \param[in] number_of_descriptors total number of descriptors in both arrays.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[out] descriptor_norm optional vector which stores the norm of each descriptor.
         *  \param[in] number_of_threads number of threads used to concurrently normalized and quantize the descriptors.
         */
        void normalizeDescriptors(unsigned int number_of_channels, VectorDense<double> * original_descriptors, unsigned int number_of_descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// This function which stores the attributes of the derived dense descriptor into the XML object.
        void convertAttributesToXML(XmlParser &parser) const;
        /// This function which stores the data information of the derived dense descriptor into the XML object.
        void convertDataToXML(XmlParser &parser) const;
        /// This function frees the structures before loading the new information from the XML object.
        void freeXML(void);
        /// This function retrieves the attributes of the derived dense descriptor from the XML object.
        void convertAttributesFromXML(XmlParser &parser);
        /// This function initializes the dense descriptor structures once all attributes have been retrieved from the XML object.
        void initializeXML(void);
        /// This function retrieves the data information of the derived dense descriptor from the XML object.
        bool convertDataFromXML(XmlParser &parser);
        /// This function is called once the data and the attributes have been retrieved from the XML object.
        void processXML(void);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Angle associated to each orientation bin.
        double * m_angle;
        /// Cosine corresponding to each orientation bin.
        double * m_cosine;
        /// Sine corresponding to each orientation bin.
        double * m_sine;
        /// Number of orientation bins of the descriptors.
        unsigned int m_number_of_orientation_bins;
        /// Number of partitions in the X-direction used to generate the spatial bins of the descriptor.
        unsigned int m_partitions_x;
        /// Number of partitions in the Y-direction used to generate the spatial bins of the descriptor.
        unsigned int m_partitions_y;
        /// Width of the partitions of each scale.
        unsigned int * m_partition_width;
        /// Height of the partitions at each scale.
        unsigned int * m_partition_height;
        
        // -[ Factory variables ]--------------------------------------------------------------------------------------------------------------------
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | INTEGRAL HISTOGRAM OF GRADIENT CLASS |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    IntegralHOG<TFILTER, TFEATURE>::IntegralHOG(void) :
        DenseDescriptor<TFILTER, TFEATURE>(),
        m_angle(new double[8]),
        m_cosine(new double[8]),
        m_sine(new double[8]),
        m_number_of_orientation_bins(8),
        m_partitions_x(4),
        m_partitions_y(4),
        m_partition_width(0),
        m_partition_height(0)
    {
        this->m_descriptor_has_negative_values = false;
        this->m_number_of_spatial_bins = 16;
        this->m_number_of_feature_bins = 8;
        this->setColorMethod(DD_GRAYSCALE);
        initializeOrientationBins();
    }
    
    template <class TFILTER, class TFEATURE>
    IntegralHOG<TFILTER, TFEATURE>::IntegralHOG(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method) :
        DenseDescriptor<TFILTER, TFEATURE>(),
        m_angle((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_cosine((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_sine((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_number_of_orientation_bins(number_of_orientation_bins),
        m_partitions_x(partitions_x),
        m_partitions_y(partitions_y),
        m_partition_width(0),
        m_partition_height(0)
    {
        this->m_descriptor_has_negative_values = false;
        this->m_number_of_spatial_bins = partitions_x * partitions_y;
        this->m_number_of_feature_bins = number_of_orientation_bins;
        this->setColorMethod(color_method);
        initializeOrientationBins();
    }
    
    template <class TFILTER, class TFEATURE>
    template <template <class, class> class VECTOR>
    IntegralHOG<TFILTER, TFEATURE>::IntegralHOG(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method, VECTOR<unsigned int, unsigned int> &scales_information, double sigma_ratio) :
        DenseDescriptor<TFILTER, TFEATURE>(),
        m_angle((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_cosine((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_sine((number_of_orientation_bins > 0)?new double[number_of_orientation_bins]:0),
        m_number_of_orientation_bins(number_of_orientation_bins),
        m_partitions_x(partitions_x),
        m_partitions_y(partitions_y),
        m_partition_width((scales_information.size() > 0)?new unsigned int[scales_information.size()]:0),
        m_partition_height((scales_information.size() > 0)?new unsigned int[scales_information.size()]:0)
    {
        this->m_descriptor_has_negative_values = false;
        this->m_number_of_spatial_bins = partitions_x * partitions_y;
        this->m_number_of_feature_bins = number_of_orientation_bins;
        this->setColorMethod(color_method);
        this->DenseDescriptor<TFILTER, TFEATURE>::setScalesParameters(scales_information, sigma_ratio);
        initializeOrientationBins();
        for (unsigned int i = 0; i < scales_information.size(); ++i)
        {
            m_partition_width[i]  = srvMax(1U, (unsigned int)scales_information[i].getFirst() / partitions_x);
            m_partition_height[i] = srvMax(1U, (unsigned int)scales_information[i].getFirst() / partitions_y);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    IntegralHOG<TFILTER, TFEATURE>::IntegralHOG(const IntegralHOG<TFILTER, TFEATURE> &other) :
        DenseDescriptor<TFILTER, TFEATURE>(other),
        m_angle((other.m_angle != 0)?new double[other.m_number_of_orientation_bins]:0),
        m_cosine((other.m_cosine != 0)?new double[other.m_number_of_orientation_bins]:0),
        m_sine((other.m_sine != 0)?new double[other.m_number_of_orientation_bins]:0),
        m_number_of_orientation_bins(other.m_number_of_orientation_bins),
        m_partitions_x(other.m_partitions_x),
        m_partitions_y(other.m_partitions_y),
        m_partition_width((other.m_number_of_scales > 0)?new unsigned int[other.m_number_of_scales]:0),
        m_partition_height((other.m_number_of_scales > 0)?new unsigned int[other.m_number_of_scales]:0)
    {
        for (unsigned int i = 0; i < other.m_number_of_orientation_bins; ++i)
        {
            m_angle[i] = other.m_angle[i];
            m_cosine[i] = other.m_cosine[i];
            m_sine[i] = other.m_sine[i];
        }
        for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
        {
            m_partition_width[i] = other.m_partition_width[i];
            m_partition_height[i] = other.m_partition_height[i];
        }
    }
    
    template <class TFILTER, class TFEATURE>
    IntegralHOG<TFILTER, TFEATURE>::~IntegralHOG(void)
    {
        if (m_angle != 0) delete [] m_angle;
        if (m_cosine != 0) delete [] m_cosine;
        if (m_sine != 0) delete [] m_sine;
        if (m_partition_width != 0) delete [] m_partition_width;
        if (m_partition_height != 0) delete [] m_partition_height;
    }
    
    template <class TFILTER, class TFEATURE>
    IntegralHOG<TFILTER, TFEATURE>& IntegralHOG<TFILTER, TFEATURE>::operator=(const IntegralHOG<TFILTER, TFEATURE> &other)
    {
        if (this != &other)
        {
            DenseDescriptor<TFILTER, TFEATURE>::operator=(other);
            // Free .................................................................................................................................
            if (m_angle != 0) delete [] m_angle;
            if (m_cosine != 0) delete [] m_cosine;
            if (m_sine != 0) delete [] m_sine;
            if (m_partition_width != 0) delete [] m_partition_width;
            if (m_partition_height != 0) delete [] m_partition_height;
            
            // Copy .................................................................................................................................
            if (other.m_number_of_orientation_bins > 0)
            {
                m_angle = new double[other.m_number_of_orientation_bins];
                m_cosine = new double[other.m_number_of_orientation_bins];
                m_sine = new double[other.m_number_of_orientation_bins];
                for (unsigned int i = 0; i < other.m_number_of_orientation_bins; ++i)
                {
                    m_angle[i] = other.m_angle[i];
                    m_cosine[i] = other.m_cosine[i];
                    m_sine[i] = other.m_sine[i];
                }
            }
            else m_angle = m_cosine = m_sine = 0;
            m_number_of_orientation_bins = other.m_number_of_orientation_bins;
            m_partitions_x = other.m_partitions_x;
            m_partitions_y = other.m_partitions_y;
            if (other.m_number_of_scales > 0)
            {
                m_partition_width = new unsigned int[other.m_number_of_scales];
                m_partition_height = new unsigned int[other.m_number_of_scales];
                for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
                {
                    m_partition_width[i] = other.m_partition_width[i];
                    m_partition_height[i] = other.m_partition_height[i];
                }
            }
            else m_partition_width = m_partition_height = 0;
        }
        
        return *this;
    }
    
    template <class TFILTER, class TFEATURE>
    template <template <class, class> class VECTOR>
    void IntegralHOG<TFILTER, TFEATURE>::set(unsigned int number_of_orientation_bins, unsigned int partitions_x, unsigned int partitions_y, DDESCRIPTOR_COLOR color_method, VECTOR<unsigned int, unsigned int> &scales_information, double sigma_ratio)
    {
        // Free .....................................................................................................................................
        if (m_angle != 0) delete [] m_angle;
        if (m_cosine != 0) delete [] m_cosine;
        if (m_sine != 0) delete [] m_sine;
        if (m_partition_width != 0) delete [] m_partition_width;
        if (m_partition_height != 0) delete [] m_partition_height;
        
        // Initialize ...............................................................................................................................
        m_number_of_orientation_bins = number_of_orientation_bins;
        if (number_of_orientation_bins > 0)
        {
            m_angle = new double[number_of_orientation_bins];
            m_cosine = new double[number_of_orientation_bins];
            m_sine = new double[number_of_orientation_bins];
            initializeOrientationBins();
        }
        else m_angle = m_cosine = m_sine = 0;
        m_partitions_x = partitions_x;
        m_partitions_y = partitions_y;
        this->m_descriptor_has_negative_values = false;
        this->m_number_of_spatial_bins = partitions_x * partitions_y;
        this->m_number_of_feature_bins = number_of_orientation_bins;
        this->setColorMethod(color_method);
        this->DenseDescriptor<TFILTER, TFEATURE>::setScalesParameters(scales_information, sigma_ratio);
        if (this->m_number_of_scales > 0)
        {
            m_partition_width = new unsigned int[this->m_number_of_scales];
            m_partition_height = new unsigned int[this->m_number_of_scales];
            for (unsigned int i = 0; i < this->m_number_of_scales; ++i)
            {
                m_partition_width[i]  = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_x);
                m_partition_height[i] = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_y);
            }
        }
        else m_partition_width = m_partition_height = 0;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::setNumberOfOrientationBins(unsigned int number_of_orientation_bins)
    {
        if (m_angle != 0) delete [] m_angle;
        if (m_cosine != 0) delete [] m_cosine;
        if (m_sine != 0) delete [] m_sine;
        
        m_number_of_orientation_bins = number_of_orientation_bins;
        if (number_of_orientation_bins > 0)
        {
            m_angle = new double[number_of_orientation_bins];
            m_cosine = new double[number_of_orientation_bins];
            m_sine = new double[number_of_orientation_bins];
            initializeOrientationBins();
        }
        else m_angle = m_cosine = m_sine = 0;
        
        this->m_number_of_feature_bins = number_of_orientation_bins;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::setDescriptorPartitions(unsigned int partitions_x, unsigned int partitions_y)
    {
        m_partitions_x = partitions_x;
        m_partitions_y = partitions_y;
        for (unsigned int i = 0; i < this->m_number_of_scales; ++i)
        {
            m_partition_width[i]  = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_x);
            m_partition_height[i] = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_y);
        }
        this->m_number_of_spatial_bins = partitions_x * partitions_y;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::setScalesParameters(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio)
    {
        // Free ...................................................................................
        if (m_partition_width != 0) delete [] m_partition_width;
        if (m_partition_height != 0) delete [] m_partition_height;
        
        // Calculate the size of the regions at each scale ........................................
        this->setScalesParametersProtected(scales_information, number_of_scales, sigma_ratio);
        if (this->m_number_of_scales > 0)
        {
            m_partition_width = new unsigned int[this->m_number_of_scales];
            m_partition_height = new unsigned int[this->m_number_of_scales];
            for (unsigned int i = 0; i < this->m_number_of_scales; ++i)
            {
                m_partition_width[i]  = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_x);
                m_partition_height[i] = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_y);
            }
        }
        else m_partition_width = m_partition_height = 0;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::normalizeDescriptors(unsigned int /*number_of_channels*/, VectorDense<double> * original_descriptors, unsigned int number_of_descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const
    {
        VectorNorm norm(EUCLIDEAN_NORM);
        if (descriptor_norms != 0) descriptor_norms->set(number_of_descriptors);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int idx = omp_get_thread_num(); idx < number_of_descriptors; idx += number_of_threads)
            {
                VectorDense<double> &current_descriptor = original_descriptors[idx];
                double accumulated_value;
                
                // Normalize the descriptor spatially.
                accumulated_value = 0.0;
                for (unsigned int i = 0; i < current_descriptor.size(); ++i)
                    accumulated_value += current_descriptor[i];
                
                if (descriptor_norms != 0) (*descriptor_norms)[idx] = accumulated_value; // Store the accumulated gradient when the descriptor norms vector is available.
                if (accumulated_value >= descriptor_norm_threshold) // If it is a valid descriptor, the normalize the vector using an Euclidean norm.
                    current_descriptor /= norm.norm(current_descriptor);
                else original_descriptors[idx].set(0); // The descriptor has been rejected!
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::initializeOrientationBins(void)
    {
        const double step = 6.283185307179586 / ((double)m_number_of_orientation_bins);
        for (unsigned int i = 0; i < m_number_of_orientation_bins; ++i)
        {
            m_angle[i] = step * (double)i - 6.283185307179586 / 2.0;
            m_cosine[i] = cos(m_angle[i]);
            m_sine[i] = sin(m_angle[i]);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::protectedExtract(const ConstantSubImage<unsigned char> &image, const VectorDense<DescriptorLocation> &locations, VectorDense<double> * descriptors, unsigned int number_of_threads) const
    {
        // Constant definitions ...................................................................
        typedef typename META_GENERAL_TYPE<TFILTER>::TYPE FILTER_GT;
        const unsigned int width = image.getWidth();
        const unsigned int height = image.getHeight();
        const unsigned int number_of_channels = image.getNumberOfChannels();
        const unsigned int number_of_feature_channels = (this->m_color_method > DD_MAXIMUM_CHANNEL)?number_of_channels:1;
        const unsigned int offset = number_of_feature_channels * this->m_number_of_feature_bins;
        const unsigned int number_of_bins = this->getNumberOfBins(number_of_channels);
        // Local variables declaration ............................................................
        IntegralImage<int> features_integral, image_integral;
        Image<unsigned char> work_image;
        Image<TFEATURE> features;
        Image<TFILTER> dx, dy;
        
        // Initialize local data structures .......................................................
        dx.setGeometry(image);
        dy.setGeometry(image);
        // In features change the number of channels to 1 when DD_MAXIMUM_CHANNEL is used.
        features.setGeometry(width, height, this->m_number_of_scales * this->m_number_of_feature_bins * number_of_feature_channels);
        
        // Convert the image to the descriptors feature space .....................................
        image_integral.set(image, number_of_threads);
        
        // Calculate the features of each scale ...................................................
        if (this->m_color_method == DD_MAXIMUM_CHANNEL)
        {
            for (unsigned int scale = 0, feature_channel = 0; scale < this->m_number_of_scales; ++scale, feature_channel += this->m_number_of_feature_bins)
            {
                // Calculate the image gradient .......................................................
                ImageGradient(image_integral, (unsigned int)std::max(1.0, round(this->m_scales_information[scale].getScaleSigma())), dx, dy, number_of_threads);
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    VectorDense<TFEATURE *> feature_ptr(this->m_number_of_feature_bins);
                    VectorDense<const TFILTER *> dx_ptr(number_of_channels), dy_ptr(number_of_channels);
                    
                    // Calculate the oriented gradient features at each pixel of the image ................
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        // Set the pointers of the current image row.
                        for (unsigned int k = 0; k < this->m_number_of_feature_bins; ++k)
                            feature_ptr[k] = features.get(y, feature_channel + k);
                        for (unsigned int c = 0; c < number_of_channels; ++c) { dx_ptr[c] = dx.get(y, c); dy_ptr[c] = dy.get(y, c); }
                        
                        // Calculate the oriented gradient of each row pixel.
                        for (unsigned int x = 0; x < width; ++x)
                        {
                            TFILTER maximum_dx, maximum_dy;
                            FILTER_GT maximum_module;
                            
                            maximum_module = (FILTER_GT)*dx_ptr[0] * (FILTER_GT)*dx_ptr[0] + (FILTER_GT)*dy_ptr[0] * (FILTER_GT)*dy_ptr[0];
                            maximum_dx = *dx_ptr[0];
                            maximum_dy = *dy_ptr[0];
                            for (unsigned int c = 1; c < number_of_channels; ++c)
                            {
                                FILTER_GT current_module;
                                
                                current_module = (FILTER_GT)*dx_ptr[c] * (FILTER_GT)*dx_ptr[c] + (FILTER_GT)*dy_ptr[c] * (FILTER_GT)*dy_ptr[c];
                                if (current_module > maximum_module)
                                {
                                    maximum_module = current_module;
                                    maximum_dx = *dx_ptr[c];
                                    maximum_dy = *dy_ptr[c];
                                }
                            }
                            for (unsigned int k = 0; k < this->m_number_of_feature_bins; ++k)
                            {
                                *(feature_ptr[k]) = srvMax<TFEATURE>((TFEATURE)0, (TFEATURE)((double)maximum_dx * m_cosine[k] + (double)maximum_dy * m_sine[k]));
                                ++feature_ptr[k];
                            }
                            
                            for (unsigned int c = 0; c < number_of_channels; ++c) { ++dx_ptr[c]; ++dy_ptr[c]; }
                        }
                    }
                }
            }
        }
        else // Other color methods ...............................................................
        {
            for (unsigned int scale = 0, feature_channel = 0; scale < this->m_number_of_scales; ++scale)
            {
                // Calculate the image gradient .......................................................
                ImageGradient(image_integral, (unsigned int)std::max(1.0, round(this->m_scales_information[scale].getScaleSigma())), dx, dy, number_of_threads);
                
                // Calculate the oriented gradient features at each pixel of the image ................
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c, feature_channel += this->m_number_of_feature_bins)
                {
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        VectorDense<TFEATURE *> feature_ptr(this->m_number_of_feature_bins);
                        
                        for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                        {
                            // Set the pointers of the current image row.
                            for (unsigned int k = 0; k < this->m_number_of_feature_bins; ++k)
                                feature_ptr[k] = features.get(y, feature_channel + k);
                            const TFILTER * __restrict__ dx_ptr = dx.get(y, c);
                            const TFILTER * __restrict__ dy_ptr = dy.get(y, c);
                            // Calculate the oriented gradient of each row pixel.
                            for (unsigned int x = 0; x < width; ++x)
                            {
                                for (unsigned int k = 0; k < this->m_number_of_feature_bins; ++k)
                                {
                                    *(feature_ptr[k]) = srvMax<TFEATURE>((TFEATURE)0, (TFEATURE)((double)*dx_ptr * m_cosine[k] + (double)*dy_ptr * m_sine[k]));
                                    ++(feature_ptr[k]);
                                }
                                ++dx_ptr;
                                ++dy_ptr;
                            }
                        }
                    }
                }
            }
        }
        // Create the integral image of the features image.
        features_integral.set(features, number_of_threads);
        
        // Call the generic extraction function ..................................................
        // ---------------------[ spatial ]--------------------- ---------------------[ spatial ]--------------------- ---------------------[ spatial ]---------------------
        // -------[ channel ]-------- -------[ channel ]-------- -------[ channel ]-------- -------[ channel ]-------- -------[ channel ]-------- -------[ channel ]--------
        // features features features features features features features features features features features features features features features features features features 
        // ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ ++++++++ 
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int idx = omp_get_thread_num(); idx < locations.size(); idx += number_of_threads)
            {
                VectorDense<double> &current_descriptor = descriptors[idx];
                const unsigned int current_x = locations[idx].getX();
                const unsigned int current_y = locations[idx].getY();
                const unsigned int current_width = m_partition_width[locations[idx].getScale()];
                const unsigned int current_height = m_partition_height[locations[idx].getScale()];
                
                current_descriptor.set(number_of_bins);
                // ----------------------------------------------------------------------------------------------------------------------------------
                for (unsigned int py = 0, cy = current_y, bin_idx = 0; py < m_partitions_y; ++py, cy += current_height)
                {
                    for (unsigned int px = 0, cx = current_x; px < m_partitions_x; ++px, cx += current_width)
                    {
                        for (unsigned int channel_idx = 0; channel_idx < number_of_feature_channels; ++channel_idx)
                        {
                            const unsigned int ci_idx = offset * locations[idx].getScale() + channel_idx * this->m_number_of_feature_bins;
                            for (unsigned int feature_bin = 0; feature_bin < this->m_number_of_feature_bins; ++feature_bin)
                            {
                                current_descriptor[bin_idx] = features_integral.getSum(cx, cy, current_width, current_height, ci_idx + feature_bin);
                                ++bin_idx;
                            }
                        }
                    }
                }
                // ----------------------------------------------------------------------------------------------------------------------------------
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::convertAttributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Number_Of_Orientation_Bins", m_number_of_orientation_bins);
        parser.setAttribute("Partitions_X", m_partitions_x);
        parser.setAttribute("Partitions_Y", m_partitions_y);
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::convertDataToXML(XmlParser &parser) const
    {
        saveVector(parser, "Angle", ConstantSubVectorDense<double, unsigned int>(m_angle, m_number_of_orientation_bins));
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::freeXML(void)
    {
        if (m_angle != 0) delete [] m_angle;
        if (m_cosine != 0) delete [] m_cosine;
        if (m_sine != 0) delete [] m_sine;
        if (m_partition_width != 0) delete [] m_partition_width;
        if (m_partition_height != 0) delete [] m_partition_height;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::convertAttributesFromXML(XmlParser &parser)
    {
        m_number_of_orientation_bins = parser.getAttribute("Number_Of_Orientation_Bins");
        m_partitions_x = parser.getAttribute("Partitions_X");
        m_partitions_y = parser.getAttribute("Partitions_Y");
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::initializeXML(void)
    {
        if (m_number_of_orientation_bins > 0)
        {
            m_angle = new double[m_number_of_orientation_bins];
            m_cosine = new double[m_number_of_orientation_bins];
            m_sine = new double[m_number_of_orientation_bins];
        }
        else m_angle = m_cosine = m_sine = 0;
        if (this->m_number_of_scales > 0)
        {
            m_partition_width = new unsigned int[this->m_number_of_scales];
            m_partition_height = new unsigned int[this->m_number_of_scales];
        }
        else m_partition_width = m_partition_height = 0;
    }
    
    template <class TFILTER, class TFEATURE>
    bool IntegralHOG<TFILTER, TFEATURE>::convertDataFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Angle"))
        {
            VectorDense<double> data;
            
            loadVector(parser, "Angle", data);
            if (data.size() != m_number_of_orientation_bins) throw Exception("The angles vector in the XML file has an unexpected number of values: it has %d values but the object is expecting %d values.", data.size(), m_number_of_orientation_bins);
            for (unsigned int i = 0; i < m_number_of_orientation_bins; ++i)
            {
                m_angle[i] = data[i];
                m_cosine[i] = cos(data[i]);
                m_sine[i] = sin(data[i]);
            }
            return true;
        }
        else return false;
    }
    
    template <class TFILTER, class TFEATURE>
    void IntegralHOG<TFILTER, TFEATURE>::processXML(void)
    {
        this->m_number_of_feature_bins = m_number_of_orientation_bins;
        for (unsigned int i = 0; i < this->m_number_of_scales; ++i)
        {
            m_partition_width[i]  = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_x);
            m_partition_height[i] = srvMax(1U, (unsigned int)this->m_scales_information[i].getRegionSize() / m_partitions_y);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

