// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_DENSE_DESCRIPTORS_HEADER_FILE__
#define __SRV_DENSE_DESCRIPTORS_HEADER_FILE__

#include "srv_dense_descriptors_definitions.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_image.hpp"
#include <map>
#include <vector>
#include <limits>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DESCRIPTOR LOCATION CLASS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which stores the location of the densely sampled regions where the descriptors are generated from.
    class DescriptorLocation
    {
    public:
        /// Default constructor.
        DescriptorLocation(void) : m_x(0), m_y(0), m_scale(0) {}
        /** Constructor which sets the location of the densely sampled region.
         *  \param[in] x X-coordinate of the top-left corner of the region.
         *  \param[in] y Y-coordinate of the top-left corner of the region.
         *  \param[in] scale identifier of the scale of the region.
         */
        DescriptorLocation(unsigned int x, unsigned int y, unsigned short scale) : m_x(x), m_y(y), m_scale(scale) {}
        /** Function which sets the location of the densely sampled region.
         *  \param[in] x X-coordinate of the top-left corner of the region.
         *  \param[in] y Y-coordinate of the top-left corner of the region.
         *  \param[in] scale identifier of the scale of the region.
         */
        inline void set(unsigned int x, unsigned int y, unsigned short scale) { m_x = x; m_y = y; m_scale = scale; }
        
        /// Returns the X-coordinate of the top-left corner of the region.
        inline unsigned int getX(void) const { return m_x; }
        /// Sets the X-coordinate of the top-left corner of the region.
        inline void setX(unsigned int x) { m_x = x; }
        /// Returns the Y-coordinate of the top-left corner of the region.
        inline unsigned int getY(void) const { return m_y; }
        /// Sets the Y-coordinate of the top-left corner of the region.
        inline void setY(unsigned int y) { m_y = y; }
        /// Returns the identifier of the scale of the region.
        inline unsigned short getScale(void) const { return m_scale; }
        /// Sets the identifier of the scale of the region.
        inline void setScale(unsigned short scale) { m_scale = scale; }
        
        /// Stores the descriptor location information into a XML object.
        inline void convertToXML(XmlParser &parser)
        {
            parser.openTag("Descriptor_Location");
            parser.setAttribute("X", m_x);
            parser.setAttribute("Y", m_y);
            parser.setAttribute("Scale", m_scale);
            parser.closeTag();
        }
        /// Retrieves the descriptor location information from a XML object.
        inline void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Descriptor_Location"))
            {
                m_x = parser.getAttribute("X");
                m_y = parser.getAttribute("Y");
                m_scale = parser.getAttribute("Scale");
                while (!(parser.isTagIdentifier("Descriptor_Location") && parser.isCloseTag())) parser.getNext();
                parser.getNext();
            }
        }
        
    protected:
        /// X-coordinate of the top-left corner of the region.
        unsigned int m_x;
        /// Y-coordinate of the top-left corner of the region.
        unsigned int m_y;
        /// Identifier of the scale of the region.
        unsigned short m_scale;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE DESCRIPTOR CLASS               |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Virtual pure class which defines the basic methods of the densely sampled descriptor objects.
     *  Pure virtual class which defines the common properties and the basic methods of the densely sampled descriptors
     *  objects. Dense descriptors extract local descriptors from isometric and rotation variant image regions, so that,
     *  they can take advantage of the redundancy between neighboring descriptors to speed up the descriptor extraction
     *  process.
     *  \tparam TFILTER Type of the Gaussian filter images.
     *  \tparam TFEATURE Type of the feature images.
     */
    template <class TFILTER, class TFEATURE>
    class DenseDescriptor
    {
    public:
        // -[ Internal classes information ]---------------------------------------------------------------------------------------------------------
        /// Information of the local regions at a given scale.
        class ScaleInformation
        {
        public:
            // -[ Constructors, destructor and assignation operator ]------------------------------
            /// Default constructor.
            ScaleInformation(void) :
                m_region_size(0),
                m_region_step(0),
                m_scale_sigma(0) {}
            /** Constructor which sets the geometry of the sampled regions.
             *  \param[in] region_size size of the regions.
             *  \param[in] region_step separation between adjacent regions.
             *  \param[in] scale_sigma value of the sigma of the Gaussian kernel used to extract information from the image.
             */
            ScaleInformation(unsigned int region_size, unsigned int region_step, double scale_sigma) :
                m_region_size(region_size),
                m_region_step(region_step),
                m_scale_sigma(scale_sigma) {}
            // -[ Set functions ]------------------------------------------------------------------
            inline void set(unsigned int region_size, unsigned int region_step, double scale_sigma) { m_region_size = region_size; m_region_step = region_step; m_scale_sigma = scale_sigma; }
            /** Calculates the parameters of the region grid configuration that can be extracted from the given image geometry.
             *  \param[in] width width of the image.
             *  \param[in] height height of the image.
             *  \param[out] number_of_regions_x number of partitions in the X-direction of the grid.
             *  \param[out] number_of_regions_y number of partitions in the Y-direction of the grid.
             *  \param[out] regions_offset_x initial X-coordinate of the densely sampled regions.
             *  \param[out] regions_offset_y initial Y-coordinate of the densely sampled regions.
             *  \param[out] number_of_regions number of regions of the grid.
             */
            inline void calculateNumberOfRegions(unsigned int width, unsigned int height, unsigned int &number_of_regions_x, unsigned int &number_of_regions_y, unsigned int &regions_offset_x, unsigned int &regions_offset_y, unsigned int &number_of_regions) const
            {
                number_of_regions_x = ((m_region_step > 0) && (width  > m_region_size))?((width  - m_region_size) / m_region_step + 1):0;
                number_of_regions_y = ((m_region_step > 0) && (height > m_region_size))?((height - m_region_size) / m_region_step + 1):0;
                if ((number_of_regions_x > 0) && (number_of_regions_y > 0))
                {
                    regions_offset_x = (width  - ((number_of_regions_x - 1) * m_region_step + m_region_size)) / 2;
                    regions_offset_y = (height - ((number_of_regions_y - 1) * m_region_step + m_region_size)) / 2;
                    number_of_regions = number_of_regions_x * number_of_regions_y;
                }
                else regions_offset_x = regions_offset_y = number_of_regions = 0;
            }
            /** Returns the number of regions following a grid configuration that can be extracted from an image of the given geometry.
             *  \param[in] width width of the image.
             *  \param[in] height height of the image.
             *  \returns number of regions that can be extracted from the image.
             */
            inline unsigned int calculateNumberOfRegions(unsigned int width, unsigned int height) const
            {
                unsigned int number_of_regions_x, number_of_regions_y;
                
                number_of_regions_x = ((m_region_step > 0) && (width  > m_region_size))?((width  - m_region_size) / m_region_step + 1):0;
                number_of_regions_y = ((m_region_step > 0) && (height > m_region_size))?((height - m_region_size) / m_region_step + 1):0;
                if ((number_of_regions_x > 0) && (number_of_regions_y > 0))
                    return number_of_regions_x * number_of_regions_y;
                else return 0;
            }
            // -[ Get functions ]------------------------------------------------------------------
            /// Returns the size of the regions.
            inline unsigned int getRegionSize(void) const { return m_region_size; }
            /// Returns the displacement between regions.
            inline unsigned int getRegionStep(void) const { return m_region_step; }
            /// Returns the sigma of the Gaussian kernel used to create the filter images.
            inline double getScaleSigma(void) const { return m_scale_sigma; }
            
            // -[ XML functions ]------------------------------------------------------------------
            /// Stores the basic region information into an XML object.
            inline void convertToXML(XmlParser &parser) const
            {
                parser.openTag("Scale_Information");
                parser.setAttribute("Region_Size", m_region_size);
                parser.setAttribute("Region_Step", m_region_step);
                parser.setAttribute("Scale_Sigma", m_scale_sigma);
                parser.closeTag();
            }
            /// Retrieves the basic region information from an XML object.
            inline void convertFromXML(XmlParser &parser)
            {
                if (parser.isTagIdentifier("Scale_Information"))
                {
                    m_region_size = parser.getAttribute("Region_Size");
                    m_region_step = parser.getAttribute("Region_Step");
                    m_scale_sigma = parser.getAttribute("Scale_Sigma");
                    while (!(parser.isTagIdentifier("Scale_Information") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
            }
        private:
            // Information of the sampled regions .................................................
            /// Size of the regions.
            unsigned int m_region_size;
            /// Displacement between regions.
            unsigned int m_region_step;
            /// Sigma of the Gaussian kernel used to create the filter images.
            double m_scale_sigma;
        };
        
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Destructor.
        virtual ~DenseDescriptor(void);
        
        // -[ Access functions to region sampling parameters ]---------------------------------------------------------------------------------------
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information array of tuples where the first element is the size of the region and the second is the step both in pixels.
         *  \param[in] number_of_scales number of elements in the array.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        virtual void setScalesParameters(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio) = 0;
        /** Sets the region sampling parameters.
         *  \param[in] minimum_region_size minimum size of the local regions.
         *  \param[in] maximum_region_size maximum size of the local regions.
         *  \param[in] growth_factor growth factor of the regions at consecutive scales of the scale-space.
         *  \param[in] relative_step boolean flag which enables the use of relative step (i.e.\ to calculate the step as a ratio of the region size).
         *  \param[in] step sets the steps of the regions. When the relative step flag is set to true, this parameters sets the ratio between the region size and the step; otherwise its value corresponds to displacement step of the features.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        void setScalesParameters(unsigned int minimum_region_size, unsigned int maximum_region_size, double growth_factor, bool relative_step, double step, double sigma_ratio);
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information dense vector of tuples where the first element is the size of the region and the second is the step both in pixels.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        template <template <class, class> class VECTOR, class NVECTOR>
        inline void setScalesParameters(const VECTOR<Tuple<unsigned int, unsigned int>, NVECTOR> &scales_information, double sigma_ratio) { this->setScalesParameters(scales_information.getData(), (unsigned int)scales_information.size(), sigma_ratio); }
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information sparse vector of where the values corresponds to the size of the region while the index corresponds to the step between pixels.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        template <template <class, class> class VECTOR_SPARSE>
        inline void setScalesParameters(const VECTOR_SPARSE<unsigned int, unsigned int> &scales_information, double sigma_ratio) { this->setScalesParameters(scales_information.getData(), scales_information.size(), sigma_ratio); }
        /// Returns the minimum size of the regions extracted from the image.
        inline unsigned int getMinimumRegionSize(void) const { return m_minimum_region_size; }
        /// Returns the maximum size of the regions extracted from the image.
        inline unsigned int getMaximumRegionSize(void) const { return m_maximum_region_size; }
        /// Returns the growth factor of regions at consecutive levels of the scale space.
        inline double getGrowthFactor(void) const { return m_growth_factor; }
        /// Returns the boolean flag which when true indicates that regions step is relative to regions size while when false indicates the fix region step.
        inline bool getRelativeStep(void) const { return m_enable_relative_step; }
        /// Returns the step between the regions. 
        inline double getStep(void) const { return m_step; }
        /// Ratio between the region size and the size of image features which are used to calculate the descriptor (e.g. this ratio is used to calculate the sigma of the image derivatives).
        inline double getSigmaRatio(void) const { return m_sigma_ratio; }
        /// Returns a constant reference to the information of the densely sampled regions of index-th scale.
        inline const ScaleInformation& getScalesInformation(unsigned int index) const { return m_scales_information[index]; }
        /// Returns the number of scales of the descriptors extracted from the image.
        inline unsigned int getNumberOfScales(void) const { return m_number_of_scales; }
        
        // -[ Access functions to the image derivatives ]--------------------------------------------------------------------------------------------
        /// Sets the color method applied to the images to extract the features from different color spaces.
        virtual void setColorMethod(DDESCRIPTOR_COLOR color_method) { m_color_method = color_method; }
        /// Returns the method applied to extract the image features from different color spaces.
        inline DDESCRIPTOR_COLOR getColorMethod(void) const { return m_color_method; }
        
        // -[ Descriptor information functions ]-----------------------------------------------------------------------------------------------------
        /// Returns the number of spatial bins of the descriptor.
        inline unsigned int getNumberOfSpatialBins(void) const { return m_number_of_spatial_bins; }
        /// Returns the number of image feature bins per spatial bin of the descriptor.
        inline unsigned int getNumberOfFeatureBins(void) const { return m_number_of_feature_bins; }
        /// Returns the number of bins of the descriptors.
        virtual unsigned int getNumberOfBins(unsigned int number_of_channels) const = 0;
        /// Returns a boolean flag which is true when the local descriptors have negative values.
        inline bool hasDescriptorNegativeValues(void) const { return m_descriptor_has_negative_values; }
        
        // -[ Descriptor extraction functions ]------------------------------------------------------------------------------------------------------
        inline unsigned int calculateNumberOfRegions(unsigned int width, unsigned int height) const
        {
            unsigned int total_number_of_regions;
            
            total_number_of_regions = 0;
            for (unsigned int scale = 0; scale < m_number_of_scales; ++scale)
                total_number_of_regions += m_scales_information[scale].calculateNumberOfRegions(width, height);
            return total_number_of_regions;
        }
        /// This function return the location where the dense descriptors are going to be calculated in the image.
        template <template <class> class IMAGE, class T>
        inline void descriptorLocations(const IMAGE<T> &image, VectorDense<DescriptorLocation> &locations) const { descriptorLocations(image.getWidth(), image.getHeight(), locations); }
        /// This function return the location where the dense descriptors are going to be calculated in the image.
        inline void descriptorLocations(unsigned int width, unsigned int height, VectorDense<DescriptorLocation> &locations) const
        {
            if ((width > 0) && (height > 0))
                descriptorLocations(0U, 0U, width - 1, height - 1, locations);
            else locations.set(0);
        }
        /// This function return the location where the dense descriptors are going to be calculated in the image.
        void descriptorLocations(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, VectorDense<DescriptorLocation> &locations) const;
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in,out] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location. This function only allocates memory for the descriptors array when the pointer variable points to 0, otherwise it expects the descriptor array to be allocated.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        template <template <class> class IMAGE, class T, class N>
        inline void extract(const IMAGE<unsigned char> &input, VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * &descriptors, double descriptor_norm_threshold, unsigned int number_of_threads) const { if (descriptors == 0) { descriptorLocations(input, locations); descriptors = new VectorDense<T, N>[locations.size()]; } mainExtract(input, locations, descriptors, descriptor_norm_threshold, 0, number_of_threads); }
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in,out] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location. This function only allocates memory for the descriptors array when the pointer variable points to 0, otherwise it expects the descriptor array to be allocated.
         *  \param[out] descriptors_norms norm of each descriptor prior to its normalization.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        template <template <class> class IMAGE, class T, class N>
        inline void extract(const IMAGE<unsigned char> &input, VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * &descriptors, VectorDense<double> &descriptors_norms, unsigned int number_of_threads) const { if (descriptors == 0) { descriptorLocations(input, locations); descriptors = new VectorDense<T, N>[locations.size()]; } mainExtract(input, locations, descriptors, -1.0, &descriptors_norms, number_of_threads); }
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in,out] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        template <template <class> class IMAGE, class T, class N>
        inline void extract(const IMAGE<unsigned char> &input, const VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * descriptors, double descriptor_norm_threshold, unsigned int number_of_threads) const { mainExtract(input, locations, descriptors, descriptor_norm_threshold, 0, number_of_threads); }
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in,out] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location.
         *  \param[out] descriptors_norms norm of each descriptor prior to its normalization.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        template <template <class> class IMAGE, class T, class N>
        inline void extract(const IMAGE<unsigned char> &input, const VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * descriptors, VectorDense<double> &descriptors_norms, unsigned int number_of_threads) const { mainExtract(input, locations, descriptors, -1.0, &descriptors_norms, number_of_threads); }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the dense descriptor information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the dense descriptor information from a XML object.
        void convertFromXML(XmlParser &parser);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        virtual DenseDescriptor<TFILTER, TFEATURE> * duplicate(void) const = 0;
        ///// /// Returns the class identifier for the current dense descriptor method.
        ///// inline static DDESCRIPTOR_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the dense descriptor.
        ///// inline static DenseDescriptor<TFILTER, TFEATURE> * generateObject(void) { return (DenseDescriptor<TFILTER, TFEATURE> *)new (); }
        ///// /// Returns a flag which states if the dense descriptor class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the dense descriptor type identifier of the current object.
        virtual DDESCRIPTOR_IDENTIFIER getIdentifier(void) const = 0;
        
    protected:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        DenseDescriptor(void);
        /// Copy constructor.
        DenseDescriptor(const DenseDescriptor<TFILTER, TFEATURE> &other);
        /// Assignation operator.
        DenseDescriptor<TFILTER, TFEATURE>& operator=(const DenseDescriptor<TFILTER, TFEATURE> &other);
        
        // -[ Access functions to base region sampling parameters ]----------------------------------------------------------------------------------
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information array of tuples where the first element is the size of the region and the second is the step both in pixels.
         *  \param[in] number_of_scales number of elements in the array.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        void setScalesParametersProtected(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio);
        
        // -[ Virtual descriptor extraction functions ]----------------------------------------------------------------------------------------------
        /** Changes the color space of the input image when needed.
         *  \param[in] input input image.
         *  \param[out] work_image_auxiliary auxiliary image needed to store the converted image when needed.
         *  \param[out] work_image sub-image with the resulting converted image.
         *  \param[in] number_of_threads number of threads used to process the images concurrently.
         */
        template <template <class> class IMAGE>
        void processColor(const IMAGE<unsigned char> &input, Image<unsigned char> &work_image_auxiliary, ConstantSubImage<unsigned char> &work_image, unsigned int number_of_threads) const;
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        virtual void protectedExtract(const ConstantSubImage<unsigned char> &image, const VectorDense<DescriptorLocation> &locations, VectorDense<double> * descriptors, unsigned int number_of_threads) const = 0;
        
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in,out] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location. This function only allocates memory for the descriptors array when the pointer variable points to 0, otherwise it expects the descriptor array to be allocated.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[out] descriptor_norms optional vector which contains the norm of each descriptor.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        template <template <class> class IMAGE, class T, class N>
        void mainExtract(const IMAGE<unsigned char> &input, const VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const;
        /** This function normalizes the descriptors and rejects descriptors which are below the descriptor threshold norm.
         *  \param[in] number_of_channels number of channels of the work image.
         *  \param[in,out] original_descriptors array with descriptors which are normalized.
         *  \param[in] number_of_descriptors total number of descriptors in both arrays.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[out] descriptor_norm optional vector which stores the norm of each descriptor.
         *  \param[in] number_of_threads number of threads used to concurrently normalized and quantize the descriptors.
         */
        virtual void normalizeDescriptors(unsigned int number_of_channels, VectorDense<double> * original_descriptors, unsigned int number_of_descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const = 0;
        /** Quantizes the descriptor.
         *  \param[in] original_descriptors array with the normalized descriptors.
         *  \param[out] result_descriptors array with the resulting quantized descriptors.
         *  \param[in] number_of_descriptors total number of descriptors in both arrays.
         *  \param[in] number_of_channels number of channels of the work image.
         *  \param[in] number_of_threads number of threads used to concurrently normalized and quantize the descriptors.
         */
        template <class T, class N>
        void rescaleDescriptors(VectorDense<double> * original_descriptors, VectorDense<T, N> * result_descriptors, unsigned int number_of_descriptors, unsigned int number_of_channels, unsigned int number_of_threads) const;
        
        // -[ Virtual XML functions ]----------------------------------------------------------------------------------------------------------------
        /// Virtual function which stores the attributes of the derived dense descriptor into the XML object.
        virtual void convertAttributesToXML(XmlParser &parser) const = 0;
        /// Virtual function which stores the data information of the derived dense descriptor into the XML object.
        virtual void convertDataToXML(XmlParser &parser) const = 0;
        /// Virtual function which frees the structures before loading the new information from the XML object.
        virtual void freeXML(void) = 0;
        /// Virtual function which retrieves the attributes of the derived dense descriptor from the XML object.
        virtual void convertAttributesFromXML(XmlParser &parser) = 0;
        /// Virtual function which initializes the dense descriptor structures once all attributes have been retrieved from the XML object.
        virtual void initializeXML(void) = 0;
        /// Virtual function which retrieves the data information of the derived dense descriptor from the XML object.
        virtual bool convertDataFromXML(XmlParser &parser) = 0;
        /// Virtual function which is called once the data and the attributes have been retrieved from the XML object.
        virtual void processXML(void) = 0;
        
        // -[ Region sampling ]----------------------------------------------------------------------------------------------------------------------
        /// Minimum size of the regions extracted from the image.
        unsigned int m_minimum_region_size;
        /// Maximum size of the regions extracted from the image.
        unsigned int m_maximum_region_size;
        /// Growth factor of regions at consecutive levels of the scale space.
        double m_growth_factor;
        /// Enables the use of relative step ratio (i.e.\ the region step is calculated relative to the region size).
        bool m_enable_relative_step;
        /// When the relative step is enabled this variable contains the ratio between the region size and the displacement step of the densely sampled regions. Otherwise, it contains the constant step of the regions.
        double m_step;
        /// Ratio between the region size and the size of image features which are used to calculate the descriptor (e.g. this ratio is used to calculate the sigma of the image derivatives).
        double m_sigma_ratio;
        /// Array with information of the densely sampled regions of each scale.
        ScaleInformation * m_scales_information;
        /// Number of scales of the descriptors extracted from the image.
        unsigned int m_number_of_scales;
        
        // -[ Image derivatives ]--------------------------------------------------------------------------------------------------------------------
        /// Method used to deal with images with multiple channels.
        DDESCRIPTOR_COLOR m_color_method;
        
        // -[ Descriptor information variables ]-----------------------------------------------------------------------------------------------------
        /// Number of spatial bins of the descriptor.
        unsigned int m_number_of_spatial_bins;
        /// Number of image feature bins per spatial bin of the descriptor.
        unsigned int m_number_of_feature_bins;
        /// Boolean flag which is true when the local descriptors have negative values.
        bool m_descriptor_has_negative_values;
        
        ///// // -[ Factory variables ]--------------------------------------------------------------------------------------------------------------------
        ///// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE DESCRIPTOR CLASS               |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    DenseDescriptor<TFILTER, TFEATURE>::DenseDescriptor(void) :
        m_minimum_region_size(0),
        m_maximum_region_size(0),
        m_growth_factor(0),
        m_enable_relative_step(false),
        m_step(0),
        m_sigma_ratio(0),
        m_scales_information(0),
        m_number_of_scales(0),
        m_color_method(DD_GRAYSCALE),
        m_number_of_spatial_bins(0),
        m_number_of_feature_bins(0),
        m_descriptor_has_negative_values(false)
    {
    }
    
    template <class TFILTER, class TFEATURE>
    DenseDescriptor<TFILTER, TFEATURE>::DenseDescriptor(const DenseDescriptor<TFILTER, TFEATURE> &other) :
        m_minimum_region_size(other.m_minimum_region_size),
        m_maximum_region_size(other.m_maximum_region_size),
        m_growth_factor(other.m_growth_factor),
        m_enable_relative_step(other.m_enable_relative_step),
        m_step(other.m_step),
        m_sigma_ratio(other.m_sigma_ratio),
        m_scales_information((other.m_number_of_scales > 0)?new ScaleInformation[other.m_number_of_scales]:0),
        m_number_of_scales(other.m_number_of_scales),
        m_color_method(other.m_color_method),
        m_number_of_spatial_bins(other.m_number_of_spatial_bins),
        m_number_of_feature_bins(other.m_number_of_feature_bins),
        m_descriptor_has_negative_values(other.m_descriptor_has_negative_values)
    {
        for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
            m_scales_information[i] = other.m_scales_information[i];
    }
    
    template <class TFILTER, class TFEATURE>
    DenseDescriptor<TFILTER, TFEATURE>::~DenseDescriptor(void)
    {
        if (m_scales_information != 0) delete [] m_scales_information;
    }
    
    template <class TFILTER, class TFEATURE>
    DenseDescriptor<TFILTER, TFEATURE>& DenseDescriptor<TFILTER, TFEATURE>::operator=(const DenseDescriptor<TFILTER, TFEATURE> &other)
    {
        if (this != &other)
        {
            // Free ...............................................................................
            if (m_scales_information != 0) delete [] m_scales_information;
            
            // Copy region sampling ...............................................................
            m_minimum_region_size = other.m_minimum_region_size;
            m_maximum_region_size = other.m_maximum_region_size;
            m_growth_factor = other.m_growth_factor;
            m_enable_relative_step = other.m_enable_relative_step;
            m_step = other.m_step;
            m_sigma_ratio = other.m_sigma_ratio;
            if (other.m_number_of_scales > 0)
            {
                m_scales_information = new ScaleInformation[other.m_number_of_scales];
                for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
                    m_scales_information[i] = other.m_scales_information[i];
            }
            else m_scales_information = 0;
            m_number_of_scales = other.m_number_of_scales;
            m_color_method = other.m_color_method;
            
            // Copy descriptor general information ................................................
            m_number_of_spatial_bins = other.m_number_of_spatial_bins;
            m_number_of_feature_bins = other.m_number_of_feature_bins;
            m_descriptor_has_negative_values = other.m_descriptor_has_negative_values;
        }
        
        return *this;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ ACCESS FUNCTIONS ]=========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void DenseDescriptor<TFILTER, TFEATURE>::setScalesParameters(unsigned int minimum_region_size, unsigned int maximum_region_size, double growth_factor, bool relative_step, double step, double sigma_ratio)
    {
        Tuple<unsigned int, unsigned int> * scales_information;
        unsigned int number_of_scales;
        
        number_of_scales = 0;
        for (double current_size = (double)minimum_region_size; current_size <= maximum_region_size; current_size = srvMax<double>(current_size + 1.0, current_size * growth_factor))
            ++number_of_scales;
        
        if (number_of_scales > 0)
        {
            double current_size;
            
            scales_information = new Tuple<unsigned int, unsigned int>[number_of_scales];
            current_size = (double)minimum_region_size;
            for (unsigned int i = 0; i < number_of_scales; ++i)
            {
                unsigned int current_step;
                
                current_step = srvMax<unsigned int>(1, (unsigned int)((relative_step)?(step * current_size):step));
                scales_information[i].setData((unsigned int)current_size, current_step);
                current_size = srvMax<double>(current_size + 1.0, current_size * growth_factor);
            }
        }
        else scales_information = 0;
        
        this->setScalesParameters(scales_information, number_of_scales, sigma_ratio);
        
        if (scales_information != 0) delete [] scales_information;
    }
    
    template <class TFILTER, class TFEATURE>
    void DenseDescriptor<TFILTER, TFEATURE>::setScalesParametersProtected(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio)
    {
        if (m_scales_information != 0) delete [] m_scales_information;
        
        m_sigma_ratio = sigma_ratio;
        if (number_of_scales > 0)
        {
            m_maximum_region_size = m_minimum_region_size = scales_information[0].getFirst();
            for (unsigned int i = 1; i < number_of_scales; ++i)
            {
                if (scales_information[i].getFirst() < m_minimum_region_size) m_minimum_region_size = scales_information[i].getFirst();
                if (scales_information[i].getFirst() > m_maximum_region_size) m_maximum_region_size = scales_information[i].getFirst();
            }
            if (number_of_scales > 1) m_growth_factor = pow((double)m_maximum_region_size / (double)m_minimum_region_size, 1.0 / (double)(number_of_scales - 1));
            else m_growth_factor = 1.0;
            m_enable_relative_step = false;
            m_step = scales_information[0].getSecond();
            m_scales_information = new ScaleInformation[number_of_scales];
            for (unsigned int i = 0; i < number_of_scales; ++i)
                m_scales_information[i].set(scales_information[i].getFirst(), scales_information[i].getSecond(), (double)scales_information[i].getFirst() * sigma_ratio);
            m_number_of_scales = number_of_scales;
        }
        else
        {
            m_minimum_region_size = 0;
            m_maximum_region_size = 0;
            m_growth_factor = 0;
            m_enable_relative_step = false;
            m_step = 0;
            m_scales_information = 0;
            m_number_of_scales = 0;
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ PROCESSING FUNCTIONS ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void DenseDescriptor<TFILTER, TFEATURE>::descriptorLocations(unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, VectorDense<DescriptorLocation> &locations) const
    {
        unsigned int number_of_regions_x, number_of_regions_y, region_offset_x, region_offset_y, number_of_regions, total_number_of_regions;
        unsigned int width, height;
        
        // Calculate the number of local regions ..............................................
        width = (x1 > x0)?(x1 - x0 + 1):0;
        height = (y1 > y0)?(y1 - y0 + 1):0;
        total_number_of_regions = 0;
        for (unsigned int scale = 0; scale < m_number_of_scales; ++scale)
            total_number_of_regions += m_scales_information[scale].calculateNumberOfRegions(width, height);
        
        // Calculate the region locations .....................................................
        if (total_number_of_regions > 0)
        {
            locations.set(total_number_of_regions);
            for (unsigned int scale = 0, index = 0; scale < m_number_of_scales; ++scale)
            {
                const unsigned int region_step = m_scales_information[scale].getRegionStep();
                m_scales_information[scale].calculateNumberOfRegions(width, height, number_of_regions_x, number_of_regions_y, region_offset_x, region_offset_y, number_of_regions);
                for (unsigned int y = 0, iy = region_offset_y; y < number_of_regions_y; ++y, iy += region_step)
                    for (unsigned int x = 0, ix = region_offset_x; x < number_of_regions_x; ++x, ix += region_step, ++index)
                        locations[index].set(ix + x0, iy + y0, (unsigned short)scale);
            }
        }
        else locations.set(0);
    }
    
    template <class TFILTER, class TFEATURE>
    template <template <class> class IMAGE>
    void DenseDescriptor<TFILTER, TFEATURE>::processColor(const IMAGE<unsigned char> &input, Image<unsigned char> &work_image_auxiliary, ConstantSubImage<unsigned char> &work_image, unsigned int number_of_threads) const
    {
        // If the channels are processed independently or the maximum response of each channel
        // is accumulated while creating the descriptor, the image is not modified.
        if ((m_color_method == DD_INDEPENDENT_CHANNELS) || (m_color_method == DD_MAXIMUM_CHANNEL)) work_image = input;
        else
        {
            if (m_color_method < DD_MAXIMUM_CHANNEL)
            {
                if (input.getNumberOfChannels() == 1) work_image = input; // Nothing to be done, directly use the input image.
                else
                {
                    work_image_auxiliary.setGeometry(input.getWidth(), input.getHeight(), 1);
                    if (input.getNumberOfChannels() == 3)
                    {
                        if      (m_color_method == DD_GRAYSCALE) ImageRGB2Gray(input, work_image_auxiliary, number_of_threads);
                        else if (m_color_method == DD_LUMINANCE) ImageRGB2Luminance(input, work_image_auxiliary, number_of_threads);
                        else throw Exception("The selected color method has not been implemented yet.");
                    }
                    else if (input.getNumberOfChannels() == 4)
                    {
                        if      (m_color_method == DD_GRAYSCALE) ImageRGB2Gray(input.subimage(0, 2), work_image_auxiliary, number_of_threads);
                        else if (m_color_method == DD_LUMINANCE) ImageRGB2Luminance(input.subimage(0, 2), work_image_auxiliary, number_of_threads);
                        else throw Exception("The selected color method has not been implemented yet.");
                    }
                    else throw Exception("Input image has an incorrect number of channels. The selected color method requires 1-, 3- or 4-channel images.");
                    work_image = work_image_auxiliary;
                }
            }
            else
            {
                ConstantSubImage<unsigned char> input_subimage;
                
                if (input.getNumberOfChannels() == 1)
                {
                    work_image_auxiliary.setGeometry(input.getWidth(), input.getHeight(), 3);
                    ImageGray2RGB(input, work_image_auxiliary, number_of_threads);
                    input_subimage = work_image_auxiliary;
                }
                else if (input.getNumberOfChannels() == 3) input_subimage = input;
                else if (input.getNumberOfChannels() == 4) input_subimage = input.subimage(0, 2);
                else throw Exception("Input image has an incorrect number of channels. The selected color method requires 1-, 3- or 4-channel images.");
                
                if (m_color_method == DD_COLOR_RGB) work_image = input_subimage; // Nothing to be done.
                else
                {
                    work_image_auxiliary.setGeometry(input_subimage);
                    if      (m_color_method == DD_COLOR_OPPONENT           ) ImageRGB2Opponent(input_subimage, work_image_auxiliary, number_of_threads);
                    else if (m_color_method == DD_COLOR_OPPONENT_NORMALIZED) ImageRGB2NormalizedOpponent(input_subimage, work_image_auxiliary, number_of_threads);
                    else throw Exception("Color method not yet implemented.");
                    work_image = work_image_auxiliary;
                }
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    template <template <class> class IMAGE, class T, class N>
    void DenseDescriptor<TFILTER, TFEATURE>::mainExtract(const IMAGE<unsigned char> &input, const VectorDense<DescriptorLocation> &locations, VectorDense<T, N> * descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const
    {
        ConstantSubImage<unsigned char> work_image;
        Image<unsigned char> work_image_auxiliary;
        VectorDense<double> * descriptors_output;
        
        // Allocate internal structures ...........................................................
        descriptors_output = new VectorDense<double>[locations.size()];
        // Image color conversion .................................................................
        processColor(input, work_image_auxiliary, work_image, number_of_threads);
        
        // Call the virtual extract method ........................................................
        this->protectedExtract(work_image, locations, descriptors_output, number_of_threads);
        
        // Normalize the descriptors and quantize them ............................................
        this->normalizeDescriptors(work_image.getNumberOfChannels(), descriptors_output, locations.size(), descriptor_norm_threshold, descriptor_norms, number_of_threads);
        rescaleDescriptors(descriptors_output, descriptors, locations.size(), work_image.getNumberOfChannels(), number_of_threads);
        
        // Free allocated memory ..................................................................
        delete [] descriptors_output;
    }
    
    template <class TFILTER, class TFEATURE>
    template <class T, class N>
    void DenseDescriptor<TFILTER, TFEATURE>::rescaleDescriptors(VectorDense<double> * original_descriptors, VectorDense<T, N> * result_descriptors, unsigned int number_of_descriptors, unsigned int number_of_channels, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = this->getNumberOfBins(number_of_channels);
        double descriptor_scale_offset, descriptor_scale_factor;
        
        // Calculate the quantization factors of the descriptor ...................................
        if (m_descriptor_has_negative_values && !std::numeric_limits<T>::is_signed)
        {
            descriptor_scale_offset = 1.0;
            if (sizeof(T) == 1)
                descriptor_scale_factor = (double)std::numeric_limits<T>::max() / 2.0;
            else if (sizeof(T) == 2)
                descriptor_scale_factor = 500.0;
            else
            {
                if (std::numeric_limits<T>::is_integer) descriptor_scale_factor = 5000.0;
                else descriptor_scale_factor = 1.0;
            }
        }
        else
        {
            descriptor_scale_offset = 0;
            if (sizeof(T) == 1)
            {
                if (std::numeric_limits<T>::is_signed) descriptor_scale_factor = srvMin<double>(-(double)std::numeric_limits<T>::min(), (double)std::numeric_limits<T>::max());
                else descriptor_scale_factor = (double)std::numeric_limits<T>::max();
            }
            else if (sizeof(T) == 2)
                descriptor_scale_factor = 1000.0;
            else
            {
                if (std::numeric_limits<T>::is_integer) descriptor_scale_factor = 100000.0;
                else descriptor_scale_factor = 1.0;
            }
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int idx = omp_get_thread_num(); idx < number_of_descriptors; idx += number_of_threads)
            {
                if (original_descriptors[idx].size() > 0)
                {
                    // Scale and store the descriptor in the resulting descriptor array.
                    result_descriptors[idx].set(number_of_dimensions);
                    for (unsigned int i = 0; i < number_of_dimensions; ++i)
                        result_descriptors[idx][i] = (T)((original_descriptors[idx][i] + descriptor_scale_offset) * descriptor_scale_factor);
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void DenseDescriptor<TFILTER, TFEATURE>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Dense_Descriptor_Information");
        parser.setAttribute("Identifier", (int)this->getIdentifier());
        parser.setAttribute("Minimum_Region_Size", m_minimum_region_size);
        parser.setAttribute("Maximum_Region_Size", m_maximum_region_size);
        parser.setAttribute("Growth_Factor", m_growth_factor);
        parser.setAttribute("Enable_Relative_Step", m_enable_relative_step);
        parser.setAttribute("Step", m_step);
        parser.setAttribute("Sigma_Ratio", m_sigma_ratio);
        parser.setAttribute("Number_Of_Scales", m_number_of_scales);
        parser.setAttribute("Color_Method", (int)m_color_method);
        parser.setAttribute("Number_Of_Spatial_Bins", m_number_of_spatial_bins);
        this->convertAttributesToXML(parser);
        
        parser.addChildren();
        if (m_number_of_scales > 0)
        {
            parser.openTag("Scales_Region_Information");
            parser.addChildren();
            for (unsigned int i = 0; i < m_number_of_scales; ++i)
            {
                parser.setSucceedingAttribute("ID", i);
                m_scales_information[i].convertToXML(parser);
            }
            parser.closeTag();
        }
        this->convertDataToXML(parser);
        
        parser.closeTag();
    }
    
    template <class TFILTER, class TFEATURE>
    void DenseDescriptor<TFILTER, TFEATURE>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Dense_Descriptor_Information"))
        {
            DDESCRIPTOR_IDENTIFIER identifier = (DDESCRIPTOR_IDENTIFIER)((int)parser.getAttribute("Identifier"));
            if (identifier != this->getIdentifier())
                throw Exception("The dense descriptor object in the XML has identifier '%d' which does not corresponds to actual class identifier.", (int)identifier);
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_scales_information != 0) delete [] m_scales_information;
            this->freeXML();
            
            // -[ Retrieve attributes ]--------------------------------------------------------------------------------------------------------------
            m_minimum_region_size = parser.getAttribute("Minimum_Region_Size");
            m_maximum_region_size = parser.getAttribute("Maximum_Region_Size");
            m_growth_factor = parser.getAttribute("Growth_Factor");
            m_enable_relative_step = parser.getAttribute("Enable_Relative_Step");
            m_step = parser.getAttribute("Step");
            m_sigma_ratio = parser.getAttribute("Sigma_Ratio");
            m_number_of_scales = parser.getAttribute("Number_Of_Scales");
            m_color_method = (DDESCRIPTOR_COLOR)((int)parser.getAttribute("Color_Method"));
            m_number_of_spatial_bins = parser.getAttribute("Number_Of_Spatial_Bins");
            this->convertAttributesFromXML(parser);                                                         // Get derived class attributes.
            
            // -[ Create structures ]----------------------------------------------------------------------------------------------------------------
            if (m_number_of_scales > 0) m_scales_information = new ScaleInformation[m_number_of_scales];
            else m_scales_information = 0;
            this->initializeXML();                                                                          // Create derived class structures.
            
            // -[ Load descriptors information ]-----------------------------------------------------------------------------------------------------
            m_number_of_feature_bins = 0;
            m_descriptor_has_negative_values = false;
            while (!(parser.isTagIdentifier("Dense_Descriptor_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Scales_Region_Information"))
                {
                    while (!(parser.isTagIdentifier("Scales_Region_Information") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Scale_Information"))
                        {
                            unsigned int index;
                            
                            index = parser.getAttribute("ID");
                            m_scales_information[index].convertFromXML(parser);
                        }
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (!this->convertDataFromXML(parser))                                                 // Retrieve derived class XML objects.
                    parser.getNext();
            }
            parser.getNext();
            
            this->processXML();                                                                             // Finish the initialization of the derived class once all XML information has retrieved.
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE DESCRIPTOR FACTORY             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    struct DenseDescriptorFactory { typedef Factory<DenseDescriptor<TFILTER, TFEATURE>, DDESCRIPTOR_IDENTIFIER> Type; };
    
    #define ADD_DENSE_DESCRIPTOR_FACTORY(CLASS_NAME, TFILTER, TFEATURE) \
        ADD_FACTORY(TEMPLATE_2P(DenseDescriptorFactory, TFILTER, TFEATURE), TEMPLATE_2P(CLASS_NAME, TFILTER, TFEATURE))
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

