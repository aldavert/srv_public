// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_DENSE_DESCRIPTOR_DEFINITIONS_HEADER_FILE__
#define __SRV_DENSE_DESCRIPTOR_DEFINITIONS_HEADER_FILE__

namespace srv
{
    //                   +--------------------------------------+
    //                   | ENUMERATES WITH THE DEFINITIONS OF   |
    //                   | THE DIFFERENT METHODS USED TO        |
    //                   | CALCULATE THE DESCRIPTORS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \enum DDESCRIPTOR_IDENTIFIER
     *  \brief Enumerate with the different types of dense descriptors algorithms which can be used to extract the descriptors from the images.
     */
    enum DDESCRIPTOR_IDENTIFIER { DD_UPRIGHT_DESCRIPTOR = 35911,   ///< Identifier of the upright descriptor algorithms.
                                  DD_INTEGRAL_HOG                  ///< Identifier of the approximate descriptor which implements the histogram of oriented gradients.
    };
    /** \enum DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE
     *  \brief Enumerate with the different features extracted from the image in order to generate the descriptors using the upright algorithm.
     */
    enum DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE { DD_UPRIGHT_GRADIENT_MODULE = 35101,   ///< Identifier of the module of the gradient features.
                                             DD_UPRIGHT_GRADIENT_ORIENTATION,      ///< Identifier of the gradient orientation features.
                                             DD_UPRIGHT_GRADIENT,                  ///< Identifier of the gradient features.
                                             DD_UPRIGHT_DERIVATIVES,               ///< Identifier of the image derivatives features.
                                             DD_UPRIGHT_ROTATED_DERIVATIVES,       ///< Identifier of the 45 degrees rotated image derivatives features.
                                             DD_UPRIGHT_LINE_ORIENTATION           ///< Identifier of the line orientation features.
    };
    
    /** \enum DDESCRIPTOR_GAUSSIAN_KERNEL
     *  \brief Different methods which can be used to calculate the image derivatives with Gaussian kernels.
     */
    enum DDESCRIPTOR_GAUSSIAN_KERNEL { DD_STANDARD_GAUSSIAN = 35201,   ///< Uses a Gaussian kernel to calculate the image derivatives.
                                       DD_DERICHE_RECURSIVE,           ///< Uses the Deriche's recursive filter to approximate the Gaussian kernel to calculate the image derivatives.
                                       DD_VLIET_3RD_RECURSIVE,         ///< Uses the 3rd order Vliet's recursive filter to approximate the Gaussian kernel to calculate the image derivatives.
                                       DD_VLIET_4TH_RECURSIVE,         ///< Uses the 4th order Vliet's recursive filter to approximate the Gaussian kernel to calculate the image derivatives.
                                       DD_VLIET_5TH_RECURSIVE          ///< Uses the 5th order Vliet's recursive filter to approximate the Gaussian kernel to calculate the image derivatives.
    };
    
    /** \enum DDESCRIPTOR_FILTER
     *  \brief Enumerate with the indexes of the different filters which are convolved with the images in order to generate the image features.
     */
    enum DDESCRIPTOR_FILTER { DD_GAUSSIAN = 0,                ///< Generate image features by convolving a Gaussian kernel over the image.
                              DD_GAUSSIAN_DX,                 ///< Generate image features by convolving a 1st derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DY,                 ///< Generate image features by convolving a 1st derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DX_DY,              ///< Generate image features by convolving a 1st derivative in the X and Y directions Gaussian kernel over the image.
                              DD_GAUSSIAN_DX_DY2,             ///< Generate image features by convolving a 1st derivative in the X direction and 2on derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DX2_DY,             ///< Generate image features by convolving a 1st derivative in the Y direction and 2on derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DX2,                ///< Generate image features by convolving a 2on derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DY2,                ///< Generate image features by convolving a 2on derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_DX2_DY2,            ///< Generate image features by convolving a 2on derivative in the X and Y directions Gaussian kernel over the image. 
                              DD_GAUSSIAN_ROTATED45_DX,       ///< Generate image features by convolving a 45 degree rotated 1st derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DY,       ///< Generate image features by convolving a 45 degree rotated 1st derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DX_DY,    ///< Generate image features by convolving a 45 degree rotated 1st derivative in the X and Y directions Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DX_DY2,   ///< Generate image features by convolving a 45 degree rotated 1st derivative in the X direction and 2on derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DX2_DY,   ///< Generate image features by convolving a 45 degree rotated 1st derivative in the Y direction and 2on derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DX2,      ///< Generate image features by convolving a 45 degree rotated 2on derivative in the X direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DY2,      ///< Generate image features by convolving a 45 degree rotated 2on derivative in the Y direction Gaussian kernel over the image.
                              DD_GAUSSIAN_ROTATED45_DX2_DY2   ///< Generate image features by convolving a 45 degree rotated 2on derivative in the X and Y directions Gaussian kernel over the image. 
    };
    /// Number of image filters used to generate the image features.
    const unsigned int DDESCRIPTOR_NUMBER_FILTERS = 17;
    
    /** \enum DDESCRIPTOR_COLOR
     *  \brief Different options to manage color information by the descriptors.
     */
    enum DDESCRIPTOR_COLOR { DD_GRAYSCALE = 35401,         ///< Color patch is converted to gray-scale.
                             DD_LUMINANCE,                 ///< Color patch is converted to a single luminance channel.
                             DD_MAXIMUM_CHANNEL,           ///< Channels are processed separately and the maximum feature response is selected.
                             DD_COLOR_RGB,                 ///< Process each RGB channel independently. Therefore, the dimensionality of the descriptor can be three times larger for some descriptors.
                             DD_COLOR_OPPONENT,            ///< Local patch is converted to the opponent color space and each channel is processed independently. Therefore, the dimensionality of the descriptor can be three times larger for some descriptors.
                             DD_COLOR_OPPONENT_NORMALIZED, ///< Local patch is converted to the normalized opponent color space and each channel is processed independently. Therefore, the dimensionality of the descriptor can be three times larger for some descriptors.
                             DD_INDEPENDENT_CHANNELS       ///< Process all the channels of the image independently.
    };
    
    /** \enum DDESCRIPTOR_SPATIAL
     *  \brief Shape of the spatial grid used to accumulate the local features of the descriptor into different spatial bins.
     */
    enum DDESCRIPTOR_SPATIAL { DD_GRID_CARTESIAN = 35501,            ///< The spatial bins of the local descriptor follow a rectangular grid distribution.
                               DD_GRID_POLAR                         ///< The spatial bins of the local descriptor follow a polar distribution.
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

