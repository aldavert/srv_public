// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_FEATURE_LINES_HEADER_FILE__
#define __SRV_IMAGE_FEATURE_LINES_HEADER_FILE__

#include "srv_image_feature_base.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GRADIENT LINE FEATURE                |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// This class implements the image derivatives image features.
    template <class TFILTER, class TFEATURE>
    class LineImageFeature : public ImageFeatureBase<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LineImageFeature(void) :
            m_number_of_bins(8),
            m_interpolation(false),
            m_factor1(new double[8]),
            m_factor2(new double[8]),
            m_factor3(new double[8])
        {
            const double step = 6.283185307179586 / 8.0;
            for (unsigned int i = 0; i < 8; ++i)
            {
                double angle = step * (double)i - 6.283185307179586 / 2.0;
                m_factor1[i] =       cos(angle) * cos(angle);
                m_factor2[i] = 2.0 * cos(angle) * sin(angle);
                m_factor3[i] =       sin(angle) * sin(angle);
            }
        }
        /** Creates an gradient orientation feature object with the given number of orientation.
         *  \param[in] number_of_bins number of gradient orientation features.
         *  \param[in] interpolation enables or disables the interpolation between gradient orientation features.
         */
        LineImageFeature(unsigned int number_of_bins, bool interpolation) :
            m_number_of_bins(number_of_bins),
            m_interpolation(interpolation),
            m_factor1(new double[number_of_bins]),
            m_factor2(new double[number_of_bins]),
            m_factor3(new double[number_of_bins])
        {
            const double step = 6.283185307179586 / ((double)number_of_bins);
            for (unsigned int i = 0; i < number_of_bins; ++i)
            {
                double angle = step * (double)i - 6.283185307179586 / 2.0;
                m_factor1[i] =       cos(angle) * cos(angle);
                m_factor2[i] = 2.0 * cos(angle) * sin(angle);
                m_factor3[i] =       sin(angle) * sin(angle);
            }
        }
        /// Copy constructor.
        LineImageFeature(const LineImageFeature<TFILTER, TFEATURE> &other) :
            m_number_of_bins(other.m_number_of_bins),
            m_interpolation(other.m_interpolation),
            m_factor1(new double[other.m_number_of_bins]),
            m_factor2(new double[other.m_number_of_bins]),
            m_factor3(new double[other.m_number_of_bins])
        {
            for (unsigned int i = 0; i < other.m_number_of_bins; ++i)
            {
                m_factor1[i] = other.m_factor1[i];
                m_factor2[i] = other.m_factor2[i];
                m_factor3[i] = other.m_factor3[i];
            }
        }
        /// Destructor.
        ~LineImageFeature(void)
        {
            delete [] m_factor1;
            delete [] m_factor2;
            delete [] m_factor3;
        }
        /// Assignation operator.
        LineImageFeature<TFILTER, TFEATURE>& operator=(const LineImageFeature<TFILTER, TFEATURE> &other);
        
        // -[ Image feature access functions ]-------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which is true when the contribution of the gradient is interpolated between gradient bins.
        inline bool getInterpolation(void) const { return m_interpolation; }
        /** Sets the number of orientation of the gradient orientation features.
         *  \param[in] number_of_bins number of gradient orientation features.
         *  \param[in] interpolation enables or disables the interpolation between gradient orientation features.
         */
        void set(unsigned int number_of_bins, bool interpolation);
        /// Returns the number of image features created at each pixel of the image.
        inline unsigned int getNumberOfFeaturesPerPixel(void) const { return (m_interpolation)?m_number_of_bins:1; }
        /// Returns the total number of image features created.
        inline unsigned int getNumberOfFeatures(void) const { return 2 * m_number_of_bins; }
        /// Selects the filters needed by the object to calculate the image features.
        inline void selectFilters(bool * enable_vector) const
        {
            enable_vector[DD_GAUSSIAN_DX_DY] = true;
            enable_vector[DD_GAUSSIAN_DX2] = true;
            enable_vector[DD_GAUSSIAN_DY2] = true;
        }
        /// Returns a boolean flag which is true when the negative features are generated.
        inline bool hasNegativeFeatures(void) const { return false; }
        
        // -[ Image feature map generation functions ]-----------------------------------------------------------------------------------------------
        /// Generates the image map from the filter images.
        void generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        inline ImageFeatureBase<TFILTER, TFEATURE> * duplicate(void) const { return (ImageFeatureBase<TFILTER, TFEATURE> *)new LineImageFeature(*this); }
        /// Returns the class identifier for the current descriptor feature method.
        inline static DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getClassIdentifier(void) { return DD_UPRIGHT_LINE_ORIENTATION; }
        /// Generates a new empty instance of the image feature.
        inline static ImageFeatureBase<TFILTER, TFEATURE> * generateObject(void) { return (ImageFeatureBase<TFILTER, TFEATURE> *)new LineImageFeature(); }
        /// Returns a flag which states if the image feature class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the image feature type identifier of the current object.
        inline DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getIdentifier(void) const { return DD_UPRIGHT_LINE_ORIENTATION; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the image feature object into a XML object.
        void convertAttributesToXML(XmlParser &parser) const;
        /// Retrieves the image feature object into a XML object.
        void convertAttributesFromXML(XmlParser &parser);
        
    protected:
        // -[ Parameters of the oriented gradient filters ]------------------------------------------------------------------------------------------
        /// Number of orientation bins.
        unsigned int m_number_of_bins;
        /// Interpolation between orientation bins.
        bool m_interpolation;
        /// Squared cosines of each oriented filter.
        double * m_factor1;
        /// Double sine times cosine of each oriented filter.
        double * m_factor2;
        /// Squared sine of each oriented filter.
        double * m_factor3;
        
        // -[ Static variables of the factory ]------------------------------------------------------------------------------------------------------
        /// Flag which indicates that the object has been added to the factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GRADIENT LINE FEATURE                |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    
    template <class TFILTER, class TFEATURE>
    LineImageFeature<TFILTER, TFEATURE>& LineImageFeature<TFILTER, TFEATURE>::operator=(const LineImageFeature<TFILTER, TFEATURE> &other)
    {
        if (this != &other)
        {
            delete [] m_factor1;
            delete [] m_factor2;
            delete [] m_factor3;
            
            m_number_of_bins = other.m_number_of_bins;
            m_interpolation = other.m_interpolation;
            m_factor1 = new double[other.m_number_of_bins];
            m_factor2 = new double[other.m_number_of_bins];
            m_factor3 = new double[other.m_number_of_bins];
            for (unsigned int i = 0; i < other.m_number_of_bins; ++i)
            {
                m_factor1[i] = other.m_factor1[i];
                m_factor2[i] = other.m_factor2[i];
                m_factor3[i] = other.m_factor3[i];
            }
        }
        
        return *this;
    }
    
    template <class TFILTER, class TFEATURE>
    void LineImageFeature<TFILTER, TFEATURE>::generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const
    {
        if (m_interpolation)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                    {
                        const TFILTER * __restrict__ gradient_dxdx_ptr = filter_images[DD_GAUSSIAN_DX2]->get(y, c);
                        const TFILTER * __restrict__ gradient_dydy_ptr = filter_images[DD_GAUSSIAN_DY2]->get(y, c);
                        const TFILTER * __restrict__ gradient_dxdy_ptr = filter_images[DD_GAUSSIAN_DX_DY]->get(y, c);
                        Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                        
                        for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                        {
                            double c1, c2, c3;
                            
                            c1 = -1.51072 * (double)*gradient_dxdx_ptr;
                            c2 =           -(double)*gradient_dxdy_ptr;
                            c3 = -1.51072 * (double)*gradient_dydy_ptr;
                            for (unsigned int k = 0; k < m_number_of_bins; ++k)
                            {
                                const double weight = m_factor1[k] * c1 - m_factor2[k] * c2 + m_factor3[k] * c3;
                                
                                if (weight > 0) feature_map_ptr[offset + k].setData((TFEATURE)weight   , feature_offset + 2 * k    );
                                else            feature_map_ptr[offset + k].setData((TFEATURE)(-weight), feature_offset + 2 * k + 1);
                            }
                            
                            ++gradient_dxdx_ptr;
                            ++gradient_dxdy_ptr;
                            ++gradient_dydy_ptr;
                            feature_map_ptr += feature_step;
                        }
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                    {
                        const TFILTER * __restrict__ gradient_dxdx_ptr = filter_images[DD_GAUSSIAN_DX2]->get(y, c);
                        const TFILTER * __restrict__ gradient_dydy_ptr = filter_images[DD_GAUSSIAN_DY2]->get(y, c);
                        const TFILTER * __restrict__ gradient_dxdy_ptr = filter_images[DD_GAUSSIAN_DX_DY]->get(y, c);
                        Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                        
                        for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                        {
                            double c1, c2, c3, maximum_weight;
                            unsigned int maximum_index;
                            
                            maximum_weight = 0.0;
                            maximum_index = 0;
                            c1 = -1.51072 * (double)*gradient_dxdx_ptr;
                            c2 =           -(double)*gradient_dxdy_ptr;
                            c3 = -1.51072 * (double)*gradient_dydy_ptr;
                            for (unsigned int k = 0; k < m_number_of_bins; ++k)
                            {
                                const double weight = m_factor1[k] * c1 - m_factor2[k] * c2 + m_factor3[k] * c3;
                                
                                if (weight > 0)
                                {
                                    if (weight > maximum_weight)
                                    {
                                        maximum_weight = weight;
                                        maximum_index = 2 * k;
                                    }
                                }
                                else
                                {
                                    if (-weight > maximum_weight)
                                    {
                                        maximum_weight = -weight;
                                        maximum_index = 2 * k + 1;
                                    }
                                }
                            }
                            feature_map_ptr[offset].setData((TFEATURE)maximum_weight, feature_offset + maximum_index);
                            
                            ++gradient_dxdx_ptr;
                            ++gradient_dxdy_ptr;
                            ++gradient_dydy_ptr;
                            feature_map_ptr += feature_step;
                        }
                    }
                }
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void LineImageFeature<TFILTER, TFEATURE>::set(unsigned int number_of_bins, bool interpolation)
    {
        const double step = c_pi / ((double)number_of_bins);
        
        delete [] m_factor1;
        delete [] m_factor2;
        delete [] m_factor3;
        
        m_number_of_bins = number_of_bins;
        m_interpolation = interpolation;
        m_factor1 = new double[number_of_bins];
        m_factor2 = new double[number_of_bins];
        m_factor3 = new double[number_of_bins];
        for (unsigned int i = 0; i < number_of_bins; ++i)
        {
            double angle = step * (double)i;
            m_factor1[i] =       cos(angle) * cos(angle);
            m_factor2[i] = 2.0 * cos(angle) * sin(angle);
            m_factor3[i] =       sin(angle) * sin(angle);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void LineImageFeature<TFILTER, TFEATURE>::convertAttributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Number_Bins", m_number_of_bins);
        parser.setAttribute("Interpolation", m_interpolation);
    }
    
    template <class TFILTER, class TFEATURE>
    void LineImageFeature<TFILTER, TFEATURE>::convertAttributesFromXML(XmlParser &parser)
    {
        unsigned int number_of_bins;
        bool interpolation;
        
        number_of_bins = parser.getAttribute("Number_Bins");
        interpolation = parser.getAttribute("Interpolation");
        set(number_of_bins, interpolation);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

