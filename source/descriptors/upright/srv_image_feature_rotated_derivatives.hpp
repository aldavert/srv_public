// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_FEATURE_ROTATED_DERIVATIVES_HEADER_FILE__
#define __SRV_IMAGE_FEATURE_ROTATED_DERIVATIVES_HEADER_FILE__

#include "srv_image_feature_base.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GRADIENT IMAGE FEATURE               |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// This class implements the 45 degrees rotated image derivatives image features.
    template <class TFILTER, class TFEATURE>
    class RotatedDerivativeImageFeature : public ImageFeatureBase<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        RotatedDerivativeImageFeature(void);
        /** Constructor which selects the derivatives which are enabled to generate the image features.
         *  \param[in] dx enables the use of the 'dX' derivative.
         *  \param[in] dy enables the use of the 'dY' derivative.
         *  \param[in] dxdy enables the use of the 'dXdY' derivative.
         *  \param[in] dx2 enables the use of the 'dX2' derivative.
         *  \param[in] dy2 enables the use of the 'dY2' derivative.
         *  \param[in] dx2dy enables the use of the 'dX2dY' derivative.
         *  \param[in] dxdy2 enables the use of the 'dXdY2' derivative.
         *  \param[in] dx2dy2 enables the use of the 'dX2dY2' derivative.
         */
        RotatedDerivativeImageFeature(bool dx, bool dy, bool dxdy, bool dx2, bool dy2, bool dx2dy, bool dxdy2, bool dx2dy2);
        /// Destructor.
        ~RotatedDerivativeImageFeature(void) {}
        /// Function which selects the derivatives which are enabled to generate the image features.
        void set(bool dx, bool dy, bool dxdy, bool dx2, bool dy2, bool dx2dy, bool dxdy2, bool dx2dy2);
        
        // -[ Pure virtual functions ]---------------------------------------------------------------------------------------------------------------
        /// Returns the number of image features created at each pixel of the image.
        inline unsigned int getNumberOfFeaturesPerPixel(void) const { return m_number_of_enabled_derivatives; }
        /// Returns the total number of image features created.
        inline unsigned int getNumberOfFeatures(void) const { return 2 * m_number_of_enabled_derivatives; }
        /// Selects the filters needed by the object to calculate the image features.
        void selectFilters(bool * enable_vector) const;
        /// Generates the image map from the filter images.
        void generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const;
        /// Returns a boolean flag which is true when the negative features are generated.
        inline bool hasNegativeFeatures(void) const { return false; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the value of the boolean flag which indicates if the derivative 'dX' is enabled.
        inline bool getDxEnabled(void) const { return m_dx_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dY' is enabled.
        inline bool getDyEnabled(void) const { return m_dy_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dXdY' is enabled.
        inline bool getDxDyEnabled(void) const { return m_dxdy_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dX2' is enabled.
        inline bool getDx2Enabled(void) const { return m_dx2_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dY2' is enabled.
        inline bool getDy2Enabled(void) const { return m_dy2_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dX2dY' is enabled.
        inline bool getDx2DyEnabled(void) const { return m_dx2dy_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dXdY2' is enabled.
        inline bool getDxDy2Enabled(void) const { return m_dxdy2_enabled; }
        /// Returns the value of the boolean flag which indicates if the derivative 'dX2dY2' is enabled.
        inline bool getDx2Dy2Enabled(void) const { return m_dx2dy2_enabled; }
        /// Returns the total number of enabled derivatives.
        inline unsigned int getNumberOfEnabledDerivatives(void) const { return m_number_of_enabled_derivatives; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        inline ImageFeatureBase<TFILTER, TFEATURE> * duplicate(void) const { return (ImageFeatureBase<TFILTER, TFEATURE> *)new RotatedDerivativeImageFeature(*this); }
        /// Returns the class identifier for the current descriptor feature method.
        inline static DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getClassIdentifier(void) { return DD_UPRIGHT_ROTATED_DERIVATIVES; }
        /// Generates a new empty instance of the image feature.
        inline static ImageFeatureBase<TFILTER, TFEATURE> * generateObject(void) { return (ImageFeatureBase<TFILTER, TFEATURE> *)new RotatedDerivativeImageFeature(); }
        /// Returns a flag which states if the image feature class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the image feature type identifier of the current object.
        inline DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getIdentifier(void) const { return DD_UPRIGHT_ROTATED_DERIVATIVES; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the image feature object into a XML object.
        void convertAttributesToXML(XmlParser &parser) const;
        /// Retrieves the image feature object into a XML object.
        void convertAttributesFromXML(XmlParser &parser);
        
    protected:
        // -[ Derivative flags ]---------------------------------------------------------------------------------------------------------------------
        /// Boolean flag which indicates if the derivative 'dX' is enabled.
        bool m_dx_enabled;
        /// Boolean flag which indicates if the derivative 'dY' is enabled.
        bool m_dy_enabled;
        /// Boolean flag which indicates if the derivative 'dXdY' is enabled.
        bool m_dxdy_enabled;
        /// Boolean flag which indicates if the derivative 'dX2' is enabled.
        bool m_dx2_enabled;
        /// Boolean flag which indicates if the derivative 'dY2' is enabled.
        bool m_dy2_enabled;
        /// Boolean flag which indicates if the derivative 'dX2dY' is enabled.
        bool m_dx2dy_enabled;
        /// Boolean flag which indicates if the derivative 'dXdY2' is enabled.
        bool m_dxdy2_enabled;
        /// Boolean flag which indicates if the derivative 'dX2dY2' is enabled.
        bool m_dx2dy2_enabled;
        /// Number of enabled derivatives.
        unsigned int m_number_of_enabled_derivatives;
        
        // -[ Static variables of the factory ]------------------------------------------------------------------------------------------------------
        /// Flag which indicates that the object has been added to the factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GRADIENT IMAGE FEATURE               |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    RotatedDerivativeImageFeature<TFILTER, TFEATURE>::RotatedDerivativeImageFeature(void) :
        ImageFeatureBase<TFILTER, TFEATURE>(),
        m_dx_enabled(true),
        m_dy_enabled(true),
        m_dxdy_enabled(true),
        m_dx2_enabled(true),
        m_dy2_enabled(true),
        m_dx2dy_enabled(true),
        m_dxdy2_enabled(true),
        m_dx2dy2_enabled(true),
        m_number_of_enabled_derivatives(8)
    {
    }
    
    template <class TFILTER, class TFEATURE>
    RotatedDerivativeImageFeature<TFILTER, TFEATURE>::RotatedDerivativeImageFeature(bool dx, bool dy, bool dxdy, bool dx2, bool dy2, bool dx2dy, bool dxdy2, bool dx2dy2) :
        m_dx_enabled(dx),
        m_dy_enabled(dy),
        m_dxdy_enabled(dxdy),
        m_dx2_enabled(dx2),
        m_dy2_enabled(dy2),
        m_dx2dy_enabled(dx2dy),
        m_dxdy2_enabled(dxdy2),
        m_dx2dy2_enabled(dx2dy2),
        m_number_of_enabled_derivatives(0)
    {
        if (dx) ++m_number_of_enabled_derivatives;
        if (dy) ++m_number_of_enabled_derivatives;
        if (dxdy) ++m_number_of_enabled_derivatives;
        if (dx2) ++m_number_of_enabled_derivatives;
        if (dy2) ++m_number_of_enabled_derivatives;
        if (dx2dy) ++m_number_of_enabled_derivatives;
        if (dxdy2) ++m_number_of_enabled_derivatives;
        if (dx2dy2) ++m_number_of_enabled_derivatives;
    }
    
    template <class TFILTER, class TFEATURE>
    void RotatedDerivativeImageFeature<TFILTER, TFEATURE>::set(bool dx, bool dy, bool dxdy, bool dx2, bool dy2, bool dx2dy, bool dxdy2, bool dx2dy2)
    {
        m_number_of_enabled_derivatives = 0;
        m_dx_enabled = dx;
        m_dy_enabled = dy;
        m_dxdy_enabled = dxdy;
        m_dx2_enabled = dx2;
        m_dy2_enabled = dy2;
        m_dx2dy_enabled = dx2dy;
        m_dxdy2_enabled = dxdy2;
        m_dx2dy2_enabled = dx2dy2;
        if (dx) ++m_number_of_enabled_derivatives;
        if (dy) ++m_number_of_enabled_derivatives;
        if (dxdy) ++m_number_of_enabled_derivatives;
        if (dx2) ++m_number_of_enabled_derivatives;
        if (dy2) ++m_number_of_enabled_derivatives;
        if (dx2dy) ++m_number_of_enabled_derivatives;
        if (dxdy2) ++m_number_of_enabled_derivatives;
        if (dx2dy2) ++m_number_of_enabled_derivatives;
    }
    
    template <class TFILTER, class TFEATURE>
    void RotatedDerivativeImageFeature<TFILTER, TFEATURE>::selectFilters(bool * enable_vector) const
    {
        if (m_dx_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX] = true;
        if (m_dy_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DY] = true;
        if (m_dxdy_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX_DY] = true;
        if (m_dxdy2_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX_DY2] = true;
        if (m_dx2dy_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX2_DY] = true;
        if (m_dx2_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX2] = true;
        if (m_dy2_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DY2] = true;
        if (m_dx2dy2_enabled)
            enable_vector[DD_GAUSSIAN_ROTATED45_DX2_DY2] = true;
    }
    
    template <class TFILTER, class TFEATURE>
    void RotatedDerivativeImageFeature<TFILTER, TFEATURE>::generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const
    {
        const bool enabled[] = { m_dx_enabled, m_dy_enabled, m_dxdy_enabled, m_dxdy2_enabled, m_dx2dy_enabled, m_dx2_enabled, m_dy2_enabled, m_dx2dy2_enabled };
        const DDESCRIPTOR_FILTER filter_ids[] = { DD_GAUSSIAN_ROTATED45_DX, DD_GAUSSIAN_ROTATED45_DY, DD_GAUSSIAN_ROTATED45_DX_DY, DD_GAUSSIAN_ROTATED45_DX_DY2,
                                                  DD_GAUSSIAN_ROTATED45_DX2_DY, DD_GAUSSIAN_ROTATED45_DX2, DD_GAUSSIAN_ROTATED45_DY2, DD_GAUSSIAN_ROTATED45_DX2_DY2 };
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            VectorDense<const TFILTER * > filter_ptrs(m_number_of_enabled_derivatives);
            
            for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                {
                    for (unsigned int k = 0, m = 0; k < 8; ++k)
                        if (enabled[k])
                            filter_ptrs[m++] = filter_images[filter_ids[k]]->get(y, c);
                    Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                    
                    for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                    {
                        for (unsigned int m = 0; m < m_number_of_enabled_derivatives; ++m)
                        {
                            if (*filter_ptrs[m] < 0)
                                 feature_map_ptr[offset + m].setData((TFEATURE)-*filter_ptrs[m], feature_offset + 2 * m    );
                            else feature_map_ptr[offset + m].setData((TFEATURE) *filter_ptrs[m], feature_offset + 2 * m + 1);
                            ++filter_ptrs[m];
                        }
                        feature_map_ptr += feature_step;
                    }
                }
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void RotatedDerivativeImageFeature<TFILTER, TFEATURE>::convertAttributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("dX", m_dx_enabled);
        parser.setAttribute("dY", m_dy_enabled);
        parser.setAttribute("dXdY", m_dxdy_enabled);
        parser.setAttribute("dX2", m_dx2_enabled);
        parser.setAttribute("dY2", m_dy2_enabled);
        parser.setAttribute("dX2dY", m_dx2dy_enabled);
        parser.setAttribute("dXdY2", m_dxdy2_enabled);
        parser.setAttribute("dX2dY2", m_dx2dy2_enabled);
    }
    
    template <class TFILTER, class TFEATURE>
    void RotatedDerivativeImageFeature<TFILTER, TFEATURE>::convertAttributesFromXML(XmlParser &parser)
    {
        m_dx_enabled = parser.getAttribute("dX");
        m_dy_enabled = parser.getAttribute("dY");
        m_dxdy_enabled = parser.getAttribute("dXdY");
        m_dx2_enabled = parser.getAttribute("dX2");
        m_dy2_enabled = parser.getAttribute("dY2");
        m_dx2dy_enabled = parser.getAttribute("dX2dY");
        m_dxdy2_enabled = parser.getAttribute("dXdY2");
        m_dx2dy2_enabled = parser.getAttribute("dX2dY2");
        m_number_of_enabled_derivatives = 0;
        if (m_dx_enabled) ++m_number_of_enabled_derivatives;
        if (m_dy_enabled) ++m_number_of_enabled_derivatives;
        if (m_dxdy_enabled) ++m_number_of_enabled_derivatives;
        if (m_dx2_enabled) ++m_number_of_enabled_derivatives;
        if (m_dy2_enabled) ++m_number_of_enabled_derivatives;
        if (m_dx2dy_enabled) ++m_number_of_enabled_derivatives;
        if (m_dxdy2_enabled) ++m_number_of_enabled_derivatives;
        if (m_dx2dy2_enabled) ++m_number_of_enabled_derivatives;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

