// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_FEATURE_BASE_HEADER_FILE__
#define __SRV_IMAGE_FEATURE_BASE_HEADER_FILE__

#include "../srv_dense_descriptors_definitions.hpp"
#include <map>
#include <vector>
#include <limits>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DENSE DESCRIPTOR FEATURES PURE       |
    //                   | VIRTUAL CLASS                        |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Base dense descriptor feature extracted from images.
    template <class TFILTER, class TFEATURE>
    class ImageFeatureBase
    {
    public:
        /// Destructor.
        virtual ~ImageFeatureBase(void) {}
        
        // -[ Pure virtual functions ]---------------------------------------------------------------------------------------------------------------
        /// Returns the number of image features created at each pixel of the image.
        virtual unsigned int getNumberOfFeaturesPerPixel(void) const = 0;
        /// Returns the total number of image features created.
        virtual unsigned int getNumberOfFeatures(void) const = 0;
        /// Selects the filters needed by the object to calculate the image features.
        virtual void selectFilters(bool * enable_vector) const = 0;
        /// Generates the image map from the filter images.
        virtual void generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const = 0;
        /// Returns a boolean flag which is true when the negative features are generated.
        virtual bool hasNegativeFeatures(void) const = 0;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        virtual ImageFeatureBase<TFILTER, TFEATURE> * duplicate(void) const = 0;
        ///// /// Returns the class identifier for the current descriptor feature method.
        ///// inline static DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the image feature.
        ///// inline static ImageFeatureBase<TFILTER, TFEATURE> * generateObject(void) { return (ImageFeatureBase<TFILTER, TFEATURE> *)new ImageFeature(); }
        ///// /// Returns a flag which states if the image feature class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the image feature type identifier of the current object.
        virtual DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the image feature object into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the image feature object into a XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Virtual XML functions ]----------------------------------------------------------------------------------------------------------------
        /// Function which stores the attributes of the derived class into the XML object.
        virtual void convertAttributesToXML(XmlParser &parser) const = 0;
        /// Function which retrieves the attributes of the derived class from the XML object.
        virtual void convertAttributesFromXML(XmlParser &parser) = 0;
        
        // -[ Static variables of the factory ]------------------------------------------------------------------------------------------------------
        ///// /// Flag which indicates that the object has been added to the factory.
        ///// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DENSE DESCRIPTOR FEATURES PURE       |
    //                   | VIRTUAL CLASS                        |
    //                   | METHODS IMPLEMENTATION               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    void ImageFeatureBase<TFILTER, TFEATURE>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Image_Feature_Information");
        parser.setAttribute("Identifier", this->getIdentifier());
        this->convertAttributesToXML(parser);
        parser.closeTag();
    }
    
    template <class TFILTER, class TFEATURE>
    void ImageFeatureBase<TFILTER, TFEATURE>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Image_Feature_Information"))
        {
            const DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE current_id = (DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE)((int)parser.getAttribute("Identifier"));
            if (current_id != this->getIdentifier())
                throw Exception("The given XML object does not contains information about a 'derivatives' image feature but the information of another image feature with identifier '%d'.", (int)current_id);
            this->convertAttributesFromXML(parser);
            while (!(parser.isTagIdentifier("Image_Feature_Information") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | UPRIGHT DESCRIPTOR FEATURES FACTORY  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    struct ImageFeatureFactory { typedef Factory<ImageFeatureBase<TFILTER, TFEATURE> , DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE > Type; };
    
    #define ADD_IMAGE_FEATURE_FACTORY(CLASS_NAME, TFILTER, TFEATURE) \
        ADD_FACTORY(TEMPLATE_2P(ImageFeatureFactory, TFILTER, TFEATURE), TEMPLATE_2P(CLASS_NAME, TFILTER, TFEATURE))
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

