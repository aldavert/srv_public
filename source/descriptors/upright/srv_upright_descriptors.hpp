// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_UPRIGHT_DESCRIPTORS_HEADER_FILE__
#define __SRV_UPRIGHT_DESCRIPTORS_HEADER_FILE__

#include "../srv_dense_descriptors.hpp"
#include "srv_image_feature_base.hpp"
#include "srv_image_feature_gradient_module.hpp"
#include "srv_image_feature_gradient_orientation.hpp"
#include "srv_image_feature_gradient.hpp"
#include "srv_image_feature_derivatives.hpp"
#include "srv_image_feature_rotated_derivatives.hpp"
#include "srv_image_feature_lines.hpp"
#include <map>
#include <vector>
#include <limits>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | UPRIGHT DESCRIPTOR CLASS             |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Upright descriptors class.
     *  This class implements the upright descriptors extractor which extract upright descriptors (i.e. descriptors without
     *  rotation invariance) from images. The rotation variance allows to densely sample descriptors from the image
     *  more efficiently since we can exploit the redundancy between neighboring descriptors. The extracted descriptors
     *  are also isometric, i.e. descriptors are extracted from a squared regions.
     *  \tparam TFILTER Type of the Gaussian filter images.
     *  \tparam TFEATURE Type of the feature images.
     */
    template <class TFILTER, class TFEATURE>
    class UprightDescriptor : public DenseDescriptor<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        UprightDescriptor(void);
        /// Copy constructor.
        UprightDescriptor(const UprightDescriptor<TFILTER, TFEATURE> &other);
        /// Destructor.
        ~UprightDescriptor(void);
        /// Assignation operator.
        UprightDescriptor<TFILTER, TFEATURE>& operator=(const UprightDescriptor<TFILTER, TFEATURE> &other);
        
        // -[ Access functions to region sampling parameters ]---------------------------------------------------------------------------------------
        /** Sets the region sampling parameters. This functions sets the number of scales, their size and their step from
         *  the values given by the user.
         *  \param[in] scales_information array of tuples where the first element is the size of the region and the second is the step both in pixels.
         *  \param[in] number_of_scales number of elements in the array.
         *  \param[in] sigma_ratio ratio between the region size and the sigma of the kernel used to extract features from the image.
         */
        void setScalesParameters(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio);
        
        // -[ Access functions to the image derivatives parameters ]---------------------------------------------------------------------------------
        /// Sets the algorithm used to calculate the Gaussian kernel derivatives of the image.
        inline void setDerivativesGaussian(DDESCRIPTOR_GAUSSIAN_KERNEL method) { m_gaussian_method = method; }
        /// Returns the type of Gaussian kernel used to calculate the derivatives of the image.
        inline DDESCRIPTOR_GAUSSIAN_KERNEL getGaussianMethod(void) const { return m_gaussian_method; }
        
        // -[ Access functions to the image derivatives ]--------------------------------------------------------------------------------------------
        /// Returns the boolean flag which is true when the selected image filter is used to extract image features.
        inline bool getFilterEnable(DDESCRIPTOR_FILTER filter_identifier) const { return m_filter_enable[(unsigned int)filter_identifier]; }
        /// Returns a constant pointer to the index-th filter image.
        inline const Image<TFILTER> * getFilterImage(unsigned int index) const { return m_filter_images[index]; }
        
        // -[ Access functions to the image features parameters ]------------------------------------------------------------------------------------
        /// Sets the feature objects used to extract the image features of the descriptor.
        void setFeatureObjects(ImageFeatureBase<TFILTER, TFEATURE> const * const * features, unsigned int number_of_features);
        /// Returns a constant pointer to the index-th image feature object.
        const ImageFeatureBase<TFILTER, TFEATURE> * getFeatureObject(unsigned int index) const { return m_features[index]; }
        /// Returns the number of image features generated by the index feature object used to create the descriptor.
        inline unsigned int getFeatureBinsPerPixel(unsigned int index) const { return m_features_number_of_bins_per_pixel[index]; }
        /// Returns a constant reference to the features image of the index-th scale.
        inline const Image<Tuple<TFEATURE, unsigned int> >& getImageFeatures(unsigned int index) const { return m_image_features[index]; }
        
        // -[ Access functions to the image features ]-----------------------------------------------------------------------------------------------
        /// Returns the number of dimensions or elements needed to represent the index-th feature of the descriptor.
        inline unsigned int getFeaturesBins(unsigned int index) const { return m_features_number_of_bins[index]; }
        /// Returns the number of feature types used to extract the image features which are accumulated into the descriptor.
        inline unsigned int getNumberOfFeatureObjects(void) const { return m_number_of_features; }
        
        // -[ Access functions to the descriptor spatial bins parameters ]---------------------------------------------------------------------------
        /** Sets the parameters of the spatial pyramid which defines the spatial bins of the descriptor.
         *  \param[in] spatial_method type of spatial partition used to add spatial information to the descriptor.
         *  \param[in] number_of_levels number of levels of the spatial pyramid which generates the histogram pyramid.
         *  \param[in] initial_x initial number of partitions in the X-direction of the spatial pyramid.
         *  \param[in] initial_y initial number of partitions in the Y-direction of the spatial pyramid.
         *  \param[in] degree_x growth factor of the spatial pyramid in the X-direction.
         *  \param[in] degree_y growth factor of the spatial pyramid in the Y-direction.
         *  \param[in] interpolation flag which enables or disables the use of bilinear interpolation to weight the contribution of image features between the spatial bins of the descriptor.
         *  \param[in] gaussian flag which enables or disables the use of a Gaussian weighting function.
         *  \param[in] gaussian_sigma sigma relative to the region size of the Gaussian weighting function.
         */
        void setSpatialParameters(DDESCRIPTOR_SPATIAL spatial_method, unsigned int number_of_levels, unsigned int initial_x, unsigned int initial_y, unsigned int degree_x, unsigned int degree_y, bool interpolation, bool gaussian, double gaussian_sigma);
        /// Returns the type of spatial partition used to add spatial information to the descriptor.
        inline DDESCRIPTOR_SPATIAL getSpatialMethod(void) const { return m_spatial_method; }
        /// Returns the flag which enables or disables the use of bilinear interpolation to weight the contribution of image features between the spatial bins of the descriptor.
        inline bool getSpatialInterpolation(void) const { return m_spatial_interpolation; }
        /// Returns the flag which enables or disables the use of a Gaussian weighting function.
        inline bool getSpatialGaussian(void) const { return m_spatial_gaussian; }
        /// Returns the sigma relative to the region size of the Gaussian weighting function.
        inline double getSpatialGaussianSigma(void) const { return m_spatial_gaussian_sigma; }
        /// Returns a constant pointer to the spatial mask of the index-th scale.
        inline const Tuple<float, unsigned int> * getSpatialMask(unsigned int index) const { return m_spatial_mask[index]; }
        /// Returns the number of spatial bins used to represent a single pixel.
        inline unsigned int getNumberOfSpatialBinsPerPixel(void) const { return m_number_of_spatial_bins_per_pixel; }
        
        // -[ Access functions to the descriptor spatial bins parameters ]---------------------------------------------------------------------------
        /// Returns the number of levels of the spatial pyramid which generates the histogram pyramid.
        inline unsigned int getNumberOfSpatialLevels(void) const { return m_number_of_spatial_levels; }
        /// Returns the initial number of partitions in the X-direction of the spatial pyramid.
        inline unsigned int getSpatialInitialX(void) const { return m_spatial_initial_x; }
        /// Returns the initial number of partitions in the Y-direction of the spatial pyramid.
        inline unsigned int getSpatialInitialY(void) const { return m_spatial_initial_y; }
        /// Returns the growth factor of the spatial pyramid in the X-direction.
        inline unsigned int getSpatialDegreeX(void) const { return m_spatial_degree_x; }
        /// Returns the growth factor of the spatial pyramid in the Y-direction.
        inline unsigned int getSpatialDegreeY(void) const { return m_spatial_degree_y; }
        /// Returns the number of spatial bins at the index-th level.
        inline unsigned int getNumberOfSpatialBins(unsigned int index) const { return m_number_of_spatial_bins_per_level[index]; }
        /// Returns the number of bins of the descriptors.
        inline unsigned int getNumberOfBins(unsigned int number_of_channels = 1) const
        {
            unsigned int channels_size_modifier;
            
            if      (this->m_color_method <= DD_MAXIMUM_CHANNEL     ) channels_size_modifier = 1;
            else if (this->m_color_method <  DD_INDEPENDENT_CHANNELS) channels_size_modifier = 3;
            else channels_size_modifier = number_of_channels;
            return channels_size_modifier * this->m_number_of_feature_bins * this->m_number_of_spatial_bins;
        }
        
        // -[ Access functions to the descriptor normalization parameters ]--------------------------------------------------------------------------
        /** Sets the normalization parameters of the descriptor.
         *  \param[in] spatial_norm order of the norm applied at each level of the spatial pyramid.
         *  \param[in] norm order of the global descriptor norm.
         *  \param[in] cutoff cutoff value applied to each bin of the descriptor.
         *  \param[in] power power factor applied to each bin of the descriptor.
         */
        inline void setNormalizationParameters(const VectorNorm &spatial_norm, const VectorNorm &norm, double cutoff, double power)
        {
            m_spatial_norm = spatial_norm;
            m_norm = norm;
            m_cutoff = cutoff;
            m_power = power;
        }
        /// Returns a constant reference to the normalization object applied at each level of the spatial pyramid.
        inline const VectorNorm& getSpatialNorm(void) const { return m_spatial_norm; }
        /// Sets the normalization object applied at each level of the spatial pyramid.
        inline void setSpatialNorm(const VectorNorm &spatial_norm) { m_spatial_norm = spatial_norm; }
        /// Returns a constant reference of the normalization object applied to the whole descriptor.
        inline const VectorNorm& getNorm(void) const { return m_norm; }
        /// Sets the normalization object applied to the whole descriptor.
        inline void setNorm(const VectorNorm &norm) { m_norm = norm; }
        /// Returns the cutoff value applied to each bin of the descriptor.
        inline double getCutoff(void) const { return m_cutoff; }
        /// Sets the cutoff value applied to each bin of the descriptor.
        inline void setCutoff(double cutoff) { m_cutoff = cutoff; }
        /// Returns the power factor applied to each bin of the descriptor.
        inline double getPowerFactor(void) const { return m_power; }
        /// Sets the power factor applied to each bin of the descriptor.
        inline void setPowerFactor(double power) { m_power = power; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        DenseDescriptor<TFILTER, TFEATURE> * duplicate(void) const { return (DenseDescriptor<TFILTER, TFEATURE> *)new UprightDescriptor<TFILTER, TFEATURE>(*this); }
        /// Returns the class identifier for the current dense descriptor method.
        inline static DDESCRIPTOR_IDENTIFIER getClassIdentifier(void) { return DD_UPRIGHT_DESCRIPTOR; }
        /// Generates a new empty instance of the dense descriptor.
        inline static DenseDescriptor<TFILTER, TFEATURE> * generateObject(void) { return (DenseDescriptor<TFILTER, TFEATURE> *)new UprightDescriptor<TFILTER, TFEATURE>(); }
        /// Returns a flag which states if the dense descriptor class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the dense descriptor type identifier of the current object.
        DDESCRIPTOR_IDENTIFIER getIdentifier(void) const { return DD_UPRIGHT_DESCRIPTOR; }
        
    protected:
        typedef typename DenseDescriptor<TFILTER, TFEATURE>::ScaleInformation ScaleInformation;
        // -[ Auxiliary initialization functions ]---------------------------------------------------------------------------------------------------
        /// Creates the spatial masks for each scale.
        void initSpatialMask(void);
        /** Creates the filter images for each scale.
         *  \param[out] filter_images array with the filter images used to extract the basic image features.
         *  \param[out] image_features array with the feature images extracted at each scale used to build the descriptors.
         */
        void initFilterImages(Image<TFILTER> * filter_images[DDESCRIPTOR_NUMBER_FILTERS], Image<Tuple<TFEATURE, unsigned int> > * &image_features) const;
        /// Frees the spatial masks for each scale.
        void freeSpatialMask(void);
        /** Frees the filter images for each scale.
         *  \param[out] filter_images array with the filter images used to extract the basic image features.
         *  \param[out] image_features array with the feature images extracted at each scale used to build the descriptors.
         */
        void freeFilterImages(Image<TFILTER> * filter_images[DDESCRIPTOR_NUMBER_FILTERS], Image<Tuple<TFEATURE, unsigned int> > * &image_features) const;
        /// Frees the structures used to store the image feature objects.
        void freeFeatures(void);
        // -[ Auxiliary processing functions ]-------------------------------------------------------------------------------------------------------
        /** Extracts the descriptors from the image.
         *  \param[in] image image used to generate the feature maps.
         *  \param[in] locations locations of the local descriptor in the image.
         *  \param[out] descriptors descriptors extracted at each location.
         *  \param[in] number_of_threads number of threads used to calculate concurrently the local descriptors.
         */
        void protectedExtract(const ConstantSubImage<unsigned char> &image, const VectorDense<DescriptorLocation> &locations, VectorDense<double> * descriptors, unsigned int number_of_threads) const;
        /** Normalize and quantize the descriptors.
         *  \param[in] number_of_channels number of channels of the work image.
         *  \param[in,out] original_descriptors array with descriptors which are normalized.
         *  \param[out] result_descriptors array with the resulting normalized and quantized descriptors.
         *  \param[in] number_of_descriptors total number of descriptors in both arrays.
         *  \param[in] descriptor_norm_threshold minimum value of the accumulated descriptor spatial norm threshold. Descriptors with a norm lower than this threshold are set to zeros.
         *  \param[out] descriptor_norm optional vector which stores the norm of each descriptor.
         *  \param[in] number_of_threads number of threads used to concurrently normalized and quantize the descriptors.
         */
        void normalizeDescriptors(unsigned int number_of_channels, VectorDense<double> * original_descriptors, unsigned int number_of_descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Function which stores the attributes of the derived dense descriptor into the XML object.
        void convertAttributesToXML(XmlParser &parser) const;
        /// Function which stores the data information of the derived dense descriptor into the XML object.
        void convertDataToXML(XmlParser &parser) const;
        /// Function which frees the structures before loading the new information from the XML object.
        void freeXML(void);
        /// Function which retrieves the attributes of the derived dense descriptor from the XML object.
        void convertAttributesFromXML(XmlParser &parser);
        /// Function which initializes the dense descriptor structures once all attributes have been retrieved from the XML object.
        void initializeXML(void);
        /// Function which retrieves the data information of the derived dense descriptor from the XML object.
        bool convertDataFromXML(XmlParser &parser);
        /// Function which is called once the data and the attributes have been retrieved from the XML object.
        void processXML(void);
        
        // -[ Image derivatives ]--------------------------------------------------------------------------------------------------------------------
        /// Array of boolean flags which indicates which image filters are needed in order to extract the image features.
        bool m_filter_enable[DDESCRIPTOR_NUMBER_FILTERS];
        /// Filtered images with the different kernels used to extract the image features which are pooled into the local descriptors.
        Image<TFILTER> * m_filter_images[DDESCRIPTOR_NUMBER_FILTERS];
        /// Type of Gaussian kernel used to calculate the derivatives of the image.
        DDESCRIPTOR_GAUSSIAN_KERNEL m_gaussian_method;
        
        // -[ Image features ]-----------------------------------------------------------------------------------------------------------------------
        /// Array with the total number of image features generated by each feature object.
        unsigned int * m_features_number_of_bins;
        /// Number of feature objects used to create the image features accumulated into the descriptor.
        unsigned int m_number_of_features;
        /// Pointer to the feature objects which are accumulated into the descriptor.
        ImageFeatureBase<TFILTER, TFEATURE> * * m_features;
        /// Array with the number of image features generated by each feature object at an image pixel.
        unsigned int * m_features_number_of_bins_per_pixel;
        /// Number of bins necessary to store all features generated at each pixel of the image.
        unsigned int m_number_of_feature_bins_per_pixel;
        /// Image with the features of each pixel of the image.
        Image<Tuple<TFEATURE, unsigned int> > * m_image_features;
        
        // -[ Spatial mask information ]-------------------------------------------------------------------------------------------------------------
        /// Type of spatial partition used to add spatial information to the descriptor.
        DDESCRIPTOR_SPATIAL m_spatial_method;
        /// Flag which enables or disables the use of bilinear interpolation to weight the contribution of image features between the spatial bins of the descriptor.
        bool m_spatial_interpolation;
        /// Flag which enables or disables the use of a Gaussian weighting function.
        bool m_spatial_gaussian;
        /// Sigma relative to the region size of the Gaussian weighting function.
        double m_spatial_gaussian_sigma;
        /// Mask with the weight and index of the spatial bins assigned to each pixel of the local region.
        Tuple<float, unsigned int> * * m_spatial_mask;
        /// Number of spatial weights for each pixel of the local region.
        unsigned int m_number_of_spatial_bins_per_pixel;
        
        // -[ Descriptor spatial ]-------------------------------------------------------------------------------------------------------------------
        /// Number of levels of the spatial pyramid which generates the histogram pyramid.
        unsigned int m_number_of_spatial_levels;
        /// Initial number of partitions in the X-direction of the spatial pyramid.
        unsigned int m_spatial_initial_x;
        /// Initial number of partitions in the Y-direction of the spatial pyramid.
        unsigned int m_spatial_initial_y;
        /// Growth factor of the spatial pyramid in the X-direction.
        unsigned int m_spatial_degree_x;
        /// Growth factor of the spatial pyramid in the Y-direction.
        unsigned int m_spatial_degree_y;
        /// Number of spatial bins at each level of the spatial pyramid.
        unsigned int * m_number_of_spatial_bins_per_level;
        
        // -[ Descriptor normalization ]-------------------------------------------------------------------------------------------------------------
        /// Order of the norm applied at each level of the spatial pyramid.
        VectorNorm m_spatial_norm;
        /// Order of the global descriptor norm.
        VectorNorm m_norm;
        /// Cutoff value applied to each bin of the descriptor.
        double m_cutoff;
        /// Power factor applied to each bin of the descriptor.
        double m_power;
        
        // -[ Factory variables ]--------------------------------------------------------------------------------------------------------------------
        static int m_is_initialized;
    };
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | UPRIGHT DESCRIPTOR CLASS             |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    UprightDescriptor<TFILTER, TFEATURE>::UprightDescriptor(void) :
        DenseDescriptor<TFILTER, TFEATURE>(),
        m_gaussian_method(DD_VLIET_3RD_RECURSIVE),
        m_features_number_of_bins(0),
        m_number_of_features(0),
        m_features(0),
        m_features_number_of_bins_per_pixel(0),
        m_number_of_feature_bins_per_pixel(0),
        m_image_features(0),
        m_spatial_method(DD_GRID_CARTESIAN),
        m_spatial_interpolation(false),
        m_spatial_gaussian(false),
        m_spatial_gaussian_sigma(1.414213562),
        m_spatial_mask(0),
        m_number_of_spatial_bins_per_pixel(0),
        m_number_of_spatial_levels(0),
        m_spatial_initial_x(0),
        m_spatial_initial_y(0),
        m_spatial_degree_x(0),
        m_spatial_degree_y(0),
        m_number_of_spatial_bins_per_level(0),
        m_spatial_norm(MANHATTAN_NORM),
        m_norm(EUCLIDEAN_NORM),
        m_cutoff(0),
        m_power(0)
    {
        for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
        {
            m_filter_enable[i] = false;
            m_filter_images[i] = 0;
        }
    }
    
    template <class TFILTER, class TFEATURE>
    UprightDescriptor<TFILTER, TFEATURE>::UprightDescriptor(const UprightDescriptor<TFILTER, TFEATURE> &other) :
        DenseDescriptor<TFILTER, TFEATURE>(other),
        m_gaussian_method(other.m_gaussian_method),
        m_features_number_of_bins((other.m_number_of_features > 0)?new unsigned int[other.m_number_of_features]:0),
        m_number_of_features(other.m_number_of_features),
        m_features((other.m_number_of_features > 0)?new ImageFeatureBase<TFILTER, TFEATURE> * [other.m_number_of_features]:0),
        m_features_number_of_bins_per_pixel((other.m_number_of_features > 0)?new unsigned int[other.m_number_of_features]:0),
        m_number_of_feature_bins_per_pixel(other.m_number_of_feature_bins_per_pixel),
        m_image_features((other.m_number_of_scales > 0)?new Image<Tuple<TFEATURE, unsigned int> >[other.m_number_of_scales]:0),
        m_spatial_method(other.m_spatial_method),
        m_spatial_interpolation(other.m_spatial_interpolation),
        m_spatial_gaussian(other.m_spatial_gaussian),
        m_spatial_gaussian_sigma(other.m_spatial_gaussian_sigma),
        m_spatial_mask((other.m_number_of_scales > 0)?new Tuple<float, unsigned int> * [other.m_number_of_scales]:0),
        m_number_of_spatial_bins_per_pixel(other.m_number_of_spatial_bins_per_pixel),
        m_number_of_spatial_levels(other.m_number_of_spatial_levels),
        m_spatial_initial_x(other.m_spatial_initial_x),
        m_spatial_initial_y(other.m_spatial_initial_y),
        m_spatial_degree_x(other.m_spatial_degree_x),
        m_spatial_degree_y(other.m_spatial_degree_y),
        m_number_of_spatial_bins_per_level((other.m_number_of_spatial_levels > 0)?new unsigned int[other.m_number_of_spatial_levels]:0),
        m_spatial_norm(other.m_spatial_norm),
        m_norm(other.m_norm),
        m_cutoff(other.m_cutoff),
        m_power(other.m_power)
    {
        for (unsigned int scale_idx = 0; scale_idx < other.m_number_of_scales; ++scale_idx)
        {
            const unsigned int region_size = other.m_scales_information[scale_idx].getRegionSize();
            const unsigned int spatial_size = region_size * region_size * other.m_number_of_spatial_bins_per_pixel;
            
            m_image_features[scale_idx] = other.m_image_features[scale_idx];
            m_spatial_mask[scale_idx] = new Tuple<float, unsigned int>[spatial_size];
            for (unsigned int k = 0; k < spatial_size; ++k)
                m_spatial_mask[scale_idx][k] = other.m_spatial_mask[scale_idx][k];
        }
        for (unsigned int feature_idx = 0; feature_idx < other.m_number_of_features; ++feature_idx)
        {
            m_features[feature_idx] = other.m_features[feature_idx]->duplicate();
            m_features_number_of_bins_per_pixel[feature_idx] = other.m_features_number_of_bins_per_pixel[feature_idx];
            m_features_number_of_bins[feature_idx] = other.m_features_number_of_bins[feature_idx];
        }
        for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
        {
            m_filter_enable[i] = other.m_filter_enable[i];
            if (other.m_filter_images[i] != 0)
                m_filter_images[i] = new Image<TFILTER>(*other.m_filter_images[i]);
            else m_filter_images[i] = 0;
        }
        for (unsigned int i = 0; i < other.m_number_of_spatial_levels; ++i)
            m_number_of_spatial_bins_per_level[i] = other.m_number_of_spatial_bins_per_level[i];
    }
    
    template <class TFILTER, class TFEATURE>
    UprightDescriptor<TFILTER, TFEATURE>::~UprightDescriptor(void)
    {
        if (m_features != 0)
        {
            for (unsigned int feature_idx = 0; feature_idx < this->m_number_of_features; ++feature_idx)
                delete m_features[feature_idx];
            delete [] m_features;
        }
        if (m_features_number_of_bins_per_pixel != 0) delete [] m_features_number_of_bins_per_pixel;
        if (m_image_features != 0) delete [] m_image_features;
        if (m_spatial_mask != 0)
        {
            for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
                delete [] m_spatial_mask[scale_idx];
            delete [] m_spatial_mask;
        }
        if (m_features_number_of_bins != 0) delete [] m_features_number_of_bins;
        for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
            if (m_filter_images[i] != 0) delete m_filter_images[i];
        if (m_number_of_spatial_bins_per_level != 0) delete [] m_number_of_spatial_bins_per_level;
    }
    
    template <class TFILTER, class TFEATURE>
    UprightDescriptor<TFILTER, TFEATURE>& UprightDescriptor<TFILTER, TFEATURE>::operator=(const UprightDescriptor<TFILTER, TFEATURE> &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_features != 0)
            {
                for (unsigned int feature_idx = 0; feature_idx < this->m_number_of_features; ++feature_idx)
                    delete m_features[feature_idx];
                delete [] m_features;
            }
            if (m_features_number_of_bins_per_pixel != 0) delete [] m_features_number_of_bins_per_pixel;
            if (m_image_features != 0) delete [] m_image_features;
            if (m_spatial_mask != 0)
            {
                for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
                    delete [] m_spatial_mask[scale_idx];
                delete [] m_spatial_mask;
            }
            if (m_features_number_of_bins != 0) delete [] m_features_number_of_bins;
            if (m_number_of_spatial_bins_per_level != 0) delete [] m_number_of_spatial_bins_per_level;
            for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
                if (m_filter_images[i] != 0) delete m_filter_images[i];
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            DenseDescriptor<TFILTER, TFEATURE>::operator=(other);
            m_gaussian_method = other.m_gaussian_method;
            m_spatial_method = other.m_spatial_method;
            m_spatial_interpolation = other.m_spatial_interpolation;
            m_spatial_gaussian = other.m_spatial_gaussian;
            m_spatial_gaussian_sigma = other.m_spatial_gaussian_sigma;
            m_number_of_spatial_bins_per_pixel = other.m_number_of_spatial_bins_per_pixel;
            m_number_of_feature_bins_per_pixel = other.m_number_of_feature_bins_per_pixel;
            
            // Copy image derivatives .............................................................
            for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
            {
                m_filter_enable[i] = other.m_filter_enable[i];
                if (other.m_filter_images[i] != 0)
                    m_filter_images[i] = new Image<TFILTER>(*other.m_filter_images[i]);
                else m_filter_images[i] = 0;
            }
            
            // Copy the image features and spatial masks ..........................................
            if (other.m_number_of_scales > 0)
            {
                m_image_features = new Image<Tuple<TFEATURE, unsigned int> >[other.m_number_of_scales];
                m_spatial_mask = new Tuple<float, unsigned int> * [other.m_number_of_scales];
                
                for (unsigned int scale_idx = 0; scale_idx < other.m_number_of_scales; ++scale_idx)
                {
                    const unsigned int region_size = other.m_scales_information[scale_idx].getRegionSize();
                    const unsigned int spatial_size = region_size * region_size * other.m_number_of_spatial_bins_per_pixel;
                    
                    m_image_features[scale_idx] = other.m_image_features[scale_idx];
                    m_spatial_mask[scale_idx] = new Tuple<float, unsigned int>[spatial_size];
                    for (unsigned int k = 0; k < spatial_size; ++k)
                        m_spatial_mask[scale_idx][k] = other.m_spatial_mask[scale_idx][k];
                }
            }
            else
            {
                m_image_features = 0;
                m_spatial_mask = 0;
            }
            
            // Copy descriptor spatial ............................................................
            m_number_of_spatial_levels = other.m_number_of_spatial_levels;
            m_spatial_initial_x = other.m_spatial_initial_x;
            m_spatial_initial_y = other.m_spatial_initial_y;
            m_spatial_degree_x = other.m_spatial_degree_x;
            m_spatial_degree_y = other.m_spatial_degree_y;
            if (other.m_number_of_spatial_levels > 0)
            {
                m_number_of_spatial_bins_per_level = new unsigned int[other.m_number_of_spatial_levels];
                for (unsigned int i = 0; i < other.m_number_of_spatial_levels; ++i)
                    m_number_of_spatial_bins_per_level[i] = other.m_number_of_spatial_bins_per_level[i];
            }
            else m_number_of_spatial_bins_per_level = 0;
            
            // Copy the descriptor feature objects ................................................
            if (other.m_number_of_features > 0)
            {
                m_features_number_of_bins = new unsigned int[other.m_number_of_features];
                m_number_of_features = other.m_number_of_features;
                for (unsigned int i = 0; i < other.m_number_of_features; ++i)
                    m_features_number_of_bins[i] = other.m_features_number_of_bins[i];
                m_features = new ImageFeatureBase<TFILTER, TFEATURE> * [other.m_number_of_features];
                m_features_number_of_bins_per_pixel = new unsigned int[other.m_number_of_features];
                for (unsigned int feature_idx = 0; feature_idx < other.m_number_of_features; ++feature_idx)
                {
                    m_features[feature_idx] = other.m_features[feature_idx]->duplicate();
                    m_features_number_of_bins_per_pixel[feature_idx] = other.m_features_number_of_bins_per_pixel[feature_idx];
                }
            }
            else
            {
                m_features_number_of_bins = 0;
                m_number_of_features = 0;
                m_features = 0;
                m_features_number_of_bins_per_pixel = 0;
            }
            
            // Copy descriptor normalization ......................................................
            m_spatial_norm = other.m_spatial_norm;
            m_norm = other.m_norm;
            m_cutoff = other.m_cutoff;
            m_power = other.m_power;
        }
        
        return *this;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ ACCESS FUNCTIONS ]=========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::setScalesParameters(const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales, double sigma_ratio)
    {
        // Free ...................................................................................
        freeSpatialMask();
        freeFilterImages(this->m_filter_images, m_image_features);
        
        // Set the parameters of the scales .......................................................
        this->setScalesParametersProtected(scales_information, number_of_scales, sigma_ratio);
        
        // Set the parameters of the descriptor extraction objects of each scale ..................
        if (number_of_scales > 0)
        {
            // Initialize the image structures ....................................................
            initFilterImages(this->m_filter_images, m_image_features);
            // Create the spatial mask ............................................................
            initSpatialMask();
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::setFeatureObjects(ImageFeatureBase<TFILTER, TFEATURE> const * const * features, unsigned int number_of_features)
    {
        // Free ...................................................................................
        freeFeatures();
        freeFilterImages(this->m_filter_images, m_image_features);
        
        // Set the new features to the descriptor .................................................
        this->m_number_of_features = number_of_features;
        if (number_of_features > 0)
        {
            m_features_number_of_bins_per_pixel = new unsigned int[number_of_features];
            this->m_features_number_of_bins = new unsigned int[number_of_features];
            m_features = new ImageFeatureBase<TFILTER, TFEATURE> * [number_of_features];
            
            // Set each image feature object ......................................................
            this->m_descriptor_has_negative_values = false;
            for (unsigned int feature_idx = 0; feature_idx < number_of_features; ++feature_idx)
            {
                m_features[feature_idx] = features[feature_idx]->duplicate();
                m_features_number_of_bins_per_pixel[feature_idx] = features[feature_idx]->getNumberOfFeaturesPerPixel();
                this->m_features_number_of_bins[feature_idx] = features[feature_idx]->getNumberOfFeatures();
                this->m_descriptor_has_negative_values = this->m_descriptor_has_negative_values || features[feature_idx]->hasNegativeFeatures();
                features[feature_idx]->selectFilters(this->m_filter_enable);
                
                m_number_of_feature_bins_per_pixel += m_features_number_of_bins_per_pixel[feature_idx];
                this->m_number_of_feature_bins += this->m_features_number_of_bins[feature_idx];
            }
            
            // Set the filter images ..............................................................
            initFilterImages(this->m_filter_images, m_image_features);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::setSpatialParameters(DDESCRIPTOR_SPATIAL spatial_method, unsigned int number_of_levels, unsigned int initial_x, unsigned int initial_y, unsigned int degree_x, unsigned int degree_y, bool interpolation, bool gaussian, double gaussian_sigma)
    {
        // Free the spatial mask ..................................................................
        freeSpatialMask();
        
        // Set the parameters of the spatial pyramid ..............................................
        m_spatial_method = spatial_method;
        this->m_number_of_spatial_levels = number_of_levels;
        this->m_spatial_initial_x = initial_x;
        this->m_spatial_initial_y = initial_y;
        this->m_spatial_degree_x = degree_x;
        this->m_spatial_degree_y = degree_y;
        m_spatial_interpolation = interpolation;
        m_spatial_gaussian = gaussian;
        m_spatial_gaussian_sigma = gaussian_sigma;
        
        // Calculate the total number of spatial bins .............................................
        this->m_number_of_spatial_bins = 0;
        for (unsigned int level = 0, partitions_x = initial_x, partitions_y = initial_y; level < number_of_levels; ++level, partitions_x *= degree_x, partitions_y *= degree_y)
        {
            if (spatial_method == DD_GRID_POLAR)
                this->m_number_of_spatial_bins += partitions_x * (partitions_y - 1) + 1;
            else this->m_number_of_spatial_bins += partitions_x * partitions_y;
        }
        if (this->m_number_of_spatial_bins == 0)
            throw Exception("The selected spatial bins configuration does not generate any spatial bin. At least a single spatial bin is required.");
        
        // Create the spatial mask ................................................................
        initSpatialMask();
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ AUXILIARY INITIALIZATION FUNCTIONS ]=======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::initSpatialMask(void)
    {
        // NOTE: This auxiliary function does not free any memory. The management of the memory structures used
        //       to extract descriptors depends on the functions which call it.
        const double sigma2 = this->m_spatial_gaussian_sigma * this->m_spatial_gaussian_sigma;
        const double pi = 3.141592653589793;
        unsigned int number_of_spatial_bins;
        
        // Check if the spatial mask can be created ...............................................
        if (this->m_number_of_scales == 0)
            return;//throw Exception("The descriptor cannot be extracted from any scale, so that, the spatial mask also cannot be created.");
        if (this->m_number_of_spatial_levels == 0)
            return;//throw Exception("The descriptor does not has any spatial level, so that, the spatial mask cannot be created.");
        
        // Calculate the number of spatial bins ...................................................
        this->m_number_of_spatial_bins_per_level = new unsigned int[this->m_number_of_spatial_levels];
        number_of_spatial_bins = 0;
        for (unsigned int current_level = 0, partitions_x = this->m_spatial_initial_x, partitions_y = this->m_spatial_initial_y; current_level < this->m_number_of_spatial_levels; ++current_level, partitions_x *= this->m_spatial_degree_x, partitions_y *= this->m_spatial_degree_y)
        {
            unsigned int current_number_of_spatial_bins;
            
            if (m_spatial_method == DD_GRID_POLAR) current_number_of_spatial_bins = partitions_x * (partitions_y - 1) + 1;
            else current_number_of_spatial_bins = partitions_x * partitions_y;
            this->m_number_of_spatial_bins_per_level[current_level] = current_number_of_spatial_bins;
            number_of_spatial_bins += current_number_of_spatial_bins;
        }
        if (number_of_spatial_bins == 0)
            return;//throw Exception("The selected spatial configuration of the descriptor ends up generating zero spatial bins. At least a single spatial bin is needed to extract the local descriptor.");
        
        if (this->m_spatial_interpolation) m_number_of_spatial_bins_per_pixel = 4 * this->m_number_of_spatial_levels;
        else m_number_of_spatial_bins_per_pixel = this->m_number_of_spatial_levels;
        m_spatial_mask = new Tuple<float, unsigned int> * [this->m_number_of_scales];
        
        // Create the spatial mask for each scale .................................................
        for (unsigned int scale = 0; scale < this->m_number_of_scales; ++scale)
        {
            const unsigned int region_size = this->m_scales_information[scale].getRegionSize();
            double bin_x, bin_y, dx, dy, angle, module;
            int cx, cy, cx0, cx1;
            
            // Check that the spatial configuration is correct ....................................
            if (region_size == 0)
                throw Exception("The spatial weight contribution mask cannot be created for regions of size 0");
            
            m_spatial_mask[scale] = new Tuple<float, unsigned int>[region_size * region_size * m_number_of_spatial_bins_per_pixel];
            
            // Calculate the contribution of the spatial mask for each pixel of the local region ..
            for (unsigned int y = 0, spatial_index = 0; y < region_size; ++y)
            {
                const double ry = ((double)y - (double)region_size / 2.0) / (double)region_size;
                for (unsigned int x = 0; x < region_size; ++x)
                {
                    const double rx = ((double)x - (double)region_size / 2.0) / (double)region_size;
                    double gaussian_weight;
                    
                    // Weight of the Gaussian function ............................................
                    if (this->m_spatial_gaussian) gaussian_weight = exp(-(4 * rx * rx + 4 * ry * ry) / (2.0 * sigma2));
                    else gaussian_weight = 1.0;
                    
                    // Calculate the weights for each partition at the coordinates <x, y> of the
                    // local patch ................................................................
                    for (unsigned int level = 0, partitions_x = this->m_spatial_initial_x, partitions_y = this->m_spatial_initial_y, partition_offset = 0; level < this->m_number_of_spatial_levels; ++level, partitions_x *= this->m_spatial_degree_x, partitions_y *= this->m_spatial_degree_y)
                    {
                        if (this->m_spatial_interpolation) // INTERPOLATION .......................
                        {
                            if (m_spatial_method == DD_GRID_POLAR) // POLAR .................
                            {
                                angle = srvMin<double>(2 * pi, srvMax<double>(0, (atan2(ry, rx) + pi)));
                                module = sqrt(rx * rx + ry * ry) / 0.5;
                                
                                if (module < 1.0) // Only the pixels which are within the unit circle are processed.
                                {
                                    bin_x = angle / (2.0 * pi + 0.00001) * (double)partitions_x - 0.5;
                                    bin_y = module * (double)partitions_y - 0.5;
                                    
                                    if (bin_x < 0) bin_x += (double)partitions_x;
                                    
                                    cx = (int)bin_x;
                                    cy = (int)bin_y;
                                    dx = bin_x - (double)cx;
                                    dy = bin_y - (double)cy;
                                    cx0 = (int)bin_x;
                                    cx1 = (cx0 + 1) % partitions_x;
                                    
                                    if (bin_y <= 0.0) // Points which are close to the center are only assigned to the central bin.
                                    {
                                        m_spatial_mask[scale][spatial_index + 0].setData((float)(gaussian_weight * 0.25), partition_offset);
                                        m_spatial_mask[scale][spatial_index + 1].setData((float)(gaussian_weight * 0.25), partition_offset);
                                        m_spatial_mask[scale][spatial_index + 2].setData((float)(gaussian_weight * 0.25), partition_offset);
                                        m_spatial_mask[scale][spatial_index + 3].setData((float)(gaussian_weight * 0.25), partition_offset);
                                    }
                                    else if (bin_y <= 1.0) // Points which are between the center and the first circle of the polar grid.
                                    {
                                        m_spatial_mask[scale][spatial_index + 0].setData((float)(gaussian_weight * (1.0 - dy) / 2.0),           partition_offset);
                                        m_spatial_mask[scale][spatial_index + 1].setData((float)(gaussian_weight * (1.0 - dy) / 2.0),           partition_offset);
                                        m_spatial_mask[scale][spatial_index + 2].setData((float)(gaussian_weight * dy * (1.0 - dx)) , cx0 + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 3].setData((float)(gaussian_weight * dy * dx)         , cx1 + 1 + partition_offset);
                                    }
                                    else if (bin_y >= (double)partitions_y - 1.0) // Points which only contribute to the outer circle of the polar grid.
                                    {
                                        m_spatial_mask[scale][spatial_index + 0].setData((float)(gaussian_weight * (1.0 - dx) / 2.0), cx0 + (cy - 1) * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 1].setData((float)(gaussian_weight * dx / 2.0)        , cx1 + (cy - 1) * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 2].setData((float)(gaussian_weight * (1.0 - dx) / 2.0), cx0 + (cy - 1) * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 3].setData((float)(gaussian_weight * dx / 2.0)        , cx1 + (cy - 1) * partitions_x + 1 + partition_offset);
                                    }
                                    else // Remaining points.
                                    {
                                        m_spatial_mask[scale][spatial_index + 0].setData((float)(gaussian_weight * (1.0 - dy) * (1.0 - dx)), cx0 + (cy - 1) * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 1].setData((float)(gaussian_weight * (1.0 - dy) * dx)        , cx1 + (cy - 1) * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 2].setData((float)(gaussian_weight * dy * (1.0 - dx))        , cx0 +  cy      * partitions_x + 1 + partition_offset);
                                        m_spatial_mask[scale][spatial_index + 3].setData((float)(gaussian_weight * dy * dx)                , cx1 +  cy      * partitions_x + 1 + partition_offset);
                                    }
                                }
                                else
                                {
                                    m_spatial_mask[scale][spatial_index + 0].setData((float)0, partition_offset);
                                    m_spatial_mask[scale][spatial_index + 1].setData((float)0, partition_offset);
                                    m_spatial_mask[scale][spatial_index + 2].setData((float)0, partition_offset);
                                    m_spatial_mask[scale][spatial_index + 3].setData((float)0, partition_offset);
                                }
                            }
                            else // CARTESIAN .....................................................
                            {
                                bin_x = srvMax<double>(0.0, srvMin<double>(0.999, (rx + 0.5))) * (double)partitions_x - 0.5;
                                bin_y = srvMax<double>(0.0, srvMin<double>(0.999, (ry + 0.5))) * (double)partitions_y - 0.5;
                                if (bin_x < 0.0) bin_x = 0.0;
                                if (bin_y < 0.0) bin_y = 0.0;
                                    
                                cx = (int)bin_x;
                                cy = (int)bin_y;
                                dx = bin_x - (double)cx;
                                dy = bin_y - (double)cy;
                                if (cx >= (int)partitions_x - 1) { cx = (int)partitions_x - 2; dx = 1.0; }
                                if (cy >= (int)partitions_y - 1) { cy = (int)partitions_y - 2; dy = 1.0; }
                                
                                m_spatial_mask[scale][spatial_index + 0].setData((float)(gaussian_weight * (1.0 - dx) * (1.0 - dy)), (unsigned int)( cx      + partitions_x *  cy     ) + partition_offset);
                                m_spatial_mask[scale][spatial_index + 1].setData((float)(gaussian_weight *        dx  * (1.0 - dy)), (unsigned int)((cx + 1) + partitions_x *  cy     ) + partition_offset);
                                m_spatial_mask[scale][spatial_index + 2].setData((float)(gaussian_weight * (1.0 - dx) *        dy ), (unsigned int)( cx      + partitions_x * (cy + 1)) + partition_offset);
                                m_spatial_mask[scale][spatial_index + 3].setData((float)(gaussian_weight *        dx  *        dy ), (unsigned int)((cx + 1) + partitions_x * (cy + 1)) + partition_offset);
                            }
                            
                            spatial_index += 4;
                        }
                        else // WITHOUT SPATIAL INTERPOLATION .....................................
                        {
                            if (m_spatial_method == DD_GRID_POLAR) // POLAR .........................
                            {
                                angle = srvMin<double>(2 * pi, srvMax<double>(0, (atan2(ry, rx) + pi)));
                                module = sqrt(rx * rx + ry * ry) / 0.5;
                                
                                if (module < 1.0) // Only the pixels which are within the unit circle are processed.
                                {
                                    bin_x = angle / (2.0 * pi + 0.00001) * (double)partitions_x;
                                    bin_y = module * (double)partitions_y;
                                    cx = (int)bin_x;
                                    cy = (int)bin_y;
                                    
                                    if (cy == 0) m_spatial_mask[scale][spatial_index].setData((float)gaussian_weight,                                    partition_offset);
                                    else         m_spatial_mask[scale][spatial_index].setData((float)gaussian_weight, (cy - 1) * partitions_x + 1 + cx + partition_offset);
                                }
                                else m_spatial_mask[scale][spatial_index].setData((float)0, partition_offset);
                            }
                            else // CARTESIAN .....................................................
                            {
                                bin_x = srvMax<double>(0.0, srvMin<double>(0.999, (rx + 0.5))) * (double)partitions_x;
                                bin_y = srvMax<double>(0.0, srvMin<double>(0.999, (ry + 0.5))) * (double)partitions_y;
                                m_spatial_mask[scale][spatial_index].setData((float)gaussian_weight, (unsigned int)(srvMin<int>(partitions_x - 1, (int)bin_x) + srvMin<int>(partitions_y - 1, (int)bin_y) * partitions_x) + partition_offset);
                            }
                            
                            ++spatial_index;
                        }
                        
                        if (m_spatial_method == DD_GRID_POLAR) partition_offset += partitions_x * (partitions_y - 1) + 1;
                        else partition_offset += partitions_x * partitions_y;
                    }
                }
            }
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::initFilterImages(Image<TFILTER> * filter_images[DDESCRIPTOR_NUMBER_FILTERS], Image<Tuple<TFEATURE, unsigned int> > * &image_features) const
    {
        // NOTE: This auxiliary function does not free any memory. The management of the memory structures used
        //       to extract descriptors depends on the functions which call it.
        image_features = new Image<Tuple<TFEATURE, unsigned int> >[this->m_number_of_scales];
        for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
        {
            if (this->m_filter_enable[filter_idx])
                filter_images[filter_idx] = new Image<TFILTER>();
            else filter_images[filter_idx] = 0;
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::freeSpatialMask(void)
    {
        // Free memory ............................................................................
        if (m_spatial_mask != 0)
        {
            for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
                delete [] m_spatial_mask[scale_idx];
            delete [] m_spatial_mask;
        }
        if (this->m_number_of_spatial_bins_per_level != 0) delete [] this->m_number_of_spatial_bins_per_level;
        // Initialize variables ...................................................................
        m_spatial_mask = 0;
        this->m_number_of_spatial_bins_per_level = 0;
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::freeFilterImages(Image<TFILTER> * filter_images[DDESCRIPTOR_NUMBER_FILTERS], Image<Tuple<TFEATURE, unsigned int> > * &image_features) const
    {
        // Free memory ............................................................................
        for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
        {
            if (filter_images[filter_idx] != 0)
            {
                delete filter_images[filter_idx];
                filter_images[filter_idx] = 0;
            }
        }
        if (image_features != 0) delete [] image_features;
        // Initialize variables ...................................................................
        image_features = 0;
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::freeFeatures(void)
    {
        // Free ...................................................................................
        if (m_features != 0)
        {
            for (unsigned int i = 0; i < this->m_number_of_features; ++i)
                delete m_features[i];
            delete [] m_features;
        }
        if (m_features_number_of_bins_per_pixel != 0) delete [] m_features_number_of_bins_per_pixel;
        if (this->m_features_number_of_bins != 0) delete [] this->m_features_number_of_bins;
        // Reset values ...........................................................................
        m_features = 0;
        m_features_number_of_bins_per_pixel = 0;
        this->m_features_number_of_bins = 0;
        for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
            this->m_filter_enable[filter_idx] = false;
        m_number_of_feature_bins_per_pixel = 0;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ AUXILIARY PROCESSING FUNCTIONS ]===========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::protectedExtract(const ConstantSubImage<unsigned char> &image, const VectorDense<DescriptorLocation> &locations, VectorDense<double> * descriptors, unsigned int number_of_threads) const
    {
        // Constant definitions ...................................................................
        const unsigned int width = image.getWidth();
        const unsigned int height = image.getHeight();
        const unsigned int number_of_channels = image.getNumberOfChannels();
        const unsigned int number_of_bins = this->getNumberOfBins(number_of_channels);
        //                                       # ->  0  1  2  3  4  5  6  7  8| 9 10 11 12 13 14 15 16
        const unsigned int gaussian_derivative_x[] = { 0, 1, 0, 1, 1, 2, 2, 0, 2, 1, 0, 1, 1, 2, 2, 0, 2 };
        const unsigned int gaussian_derivative_y[] = { 0, 0, 1, 1, 2, 1, 0, 2, 2, 0, 1, 1, 2, 1, 0, 2, 2 };
        // Variables ..............................................................................
        Image<TFILTER> * filter_images[DDESCRIPTOR_NUMBER_FILTERS];
        Image<Tuple<TFEATURE, unsigned int> > * image_features;
        srv::VectorDense<int> features_block_id(this->m_number_of_feature_bins);
        srv::VectorDense<std::tuple<unsigned int, unsigned int, unsigned int, unsigned int> > features_endpoints;
        unsigned int features_block_counter;
        
        // Initialize local data structures .......................................................
        initFilterImages(filter_images, image_features);
        // Set the filter images geometry .........................................................
        for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
        {
            if (filter_images[filter_idx] != 0)
            {
                filter_images[filter_idx]->setGeometry(width, height, number_of_channels);
                filter_images[filter_idx]->setValue(0);
            }
        }
        for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
            image_features[scale_idx].setGeometry(width * m_number_of_feature_bins_per_pixel, height, number_of_channels);
        // Create the Gaussian filters of the image ...............................................
        if (m_gaussian_method == DD_STANDARD_GAUSSIAN)
        {
            Gaussian2DFilterKernel kernel;
            
            for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
            {
                // Calculate the image filters ....................................................
                for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
                {
                    if (filter_images[filter_idx] != 0)
                    {
                        kernel.set(this->m_scales_information[scale_idx].getScaleSigma(), gaussian_derivative_x[filter_idx], gaussian_derivative_y[filter_idx], filter_idx >= 9);
                        GaussianFilter(image, kernel, *filter_images[filter_idx], number_of_threads);
                    }
                }
                // Extract the descriptor features ................................................
                for (unsigned int feature_idx = 0, current_offset = 0, feature_offset = 0; feature_idx < this->m_number_of_features; ++feature_idx)
                {
                    m_features[feature_idx]->generateFeatureMap(filter_images, image_features[scale_idx], current_offset, feature_offset, m_number_of_feature_bins_per_pixel, number_of_threads);
                    current_offset += m_features_number_of_bins_per_pixel[feature_idx];
                    feature_offset += this->m_features_number_of_bins[feature_idx];
                }
            }
        }
        else
        {
            RECURSIVE_GAUSSIAN_METHOD recursive_method;
            Gaussian2DFilterRecursiveKernel kernel;
            
            // Initialize the recursive kernel parameters .........................................
            if      (m_gaussian_method == DD_DERICHE_RECURSIVE)   recursive_method = DERICHE;
            else if (m_gaussian_method == DD_VLIET_3RD_RECURSIVE) recursive_method = VLIET_3RD;
            else if (m_gaussian_method == DD_VLIET_4TH_RECURSIVE) recursive_method = VLIET_4TH;
            else if (m_gaussian_method == DD_VLIET_5TH_RECURSIVE) recursive_method = VLIET_5TH;
            else throw Exception("Selected recursive Gaussian kernel is not yet implemented.");
            
            for (unsigned int scale_idx = 0; scale_idx < this->m_number_of_scales; ++scale_idx)
            {
                // Calculate the image filters ....................................................
                for (unsigned int filter_idx = 0; filter_idx < DDESCRIPTOR_NUMBER_FILTERS; ++filter_idx)
                {
                    if (filter_images[filter_idx] != 0)
                    {
                        kernel.set(recursive_method, this->m_scales_information[scale_idx].getScaleSigma(), gaussian_derivative_x[filter_idx], gaussian_derivative_y[filter_idx], filter_idx >= 9);
                        GaussianFilter(image, kernel, *filter_images[filter_idx], number_of_threads);
                    }
                }
                // Extract the descriptor features ................................................
                for (unsigned int feature_idx = 0, current_offset = 0, feature_offset = 0; feature_idx < this->m_number_of_features; ++feature_idx)
                {
                    m_features[feature_idx]->generateFeatureMap(filter_images, image_features[scale_idx], current_offset, feature_offset, m_number_of_feature_bins_per_pixel, number_of_threads);
                    current_offset += m_features_number_of_bins_per_pixel[feature_idx];
                    feature_offset += this->m_features_number_of_bins[feature_idx];
                }
            }
        }
        
        // Orientation features may not be aligned, so they have to be processed different than the other features when the maximum is computed.
        features_block_counter = 0;
        for (unsigned int i = 0, j = 0; i < this->m_number_of_features; ++i)
        {
            if ((m_features[i]->getIdentifier() == DD_UPRIGHT_GRADIENT_ORIENTATION) || (m_features[i]->getIdentifier() == DD_UPRIGHT_LINE_ORIENTATION))
            {
                ++features_block_counter;
                for (unsigned int k = 0; k < m_features_number_of_bins_per_pixel[i]; ++k, ++j)
                    features_block_id[j] = features_block_counter;
            }
            else
            {
                for (unsigned int k = 0; k < m_features_number_of_bins_per_pixel[i]; ++k, ++j)
                    features_block_id[j] = 0;
            }
        }
        features_endpoints.set(features_block_counter + 1, std::make_tuple(0, 0, 0, 0));
        features_block_counter = 0;
        for (unsigned int i = 0, j = 0, k = 0; i < this->m_number_of_features; ++i)
        {
            if ((m_features[i]->getIdentifier() == DD_UPRIGHT_GRADIENT_ORIENTATION) || (m_features[i]->getIdentifier() == DD_UPRIGHT_LINE_ORIENTATION))
            {
                ++features_block_counter;
                features_endpoints[features_block_counter] = std::make_tuple(j, j + m_features_number_of_bins[i], k, k + m_features_number_of_bins_per_pixel[i]);
            }
            j += m_features_number_of_bins[i];
            k += m_features_number_of_bins_per_pixel[i];
        }
        ++features_block_counter;
        
        // Call the generic extraction function ...................................................
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<const Tuple<TFEATURE, unsigned int> * > feature_ptrs(number_of_channels);
            srv::VectorDense<double> features_maximum_value(features_block_counter, 0), features_current_value(features_block_counter, 0);
            
            if (this->m_color_method == DD_MAXIMUM_CHANNEL)
            {
                ////std::cout << "-----------------------------------------------------------" << std::endl;
                ////std::cout << features_block_counter << std::endl;
                ////for (unsigned int k = 1; k < features_block_counter; ++k)
                ////    std::cout << std::get<0>(features_endpoints[k]) << " " << std::get<1>(features_endpoints[k]) << " " << std::get<2>(features_endpoints[k]) << " " << std::get<3>(features_endpoints[k]) << std::endl;
                ////std::cout << "-----------------------------------------------------------" << std::endl;
                VectorDense<double> pixel_values(this->m_number_of_feature_bins);
                
                for (unsigned int idx = omp_get_thread_num(); idx < locations.size(); idx += number_of_threads)
                {
                    const unsigned int current_scale = locations[idx].getScale();
                    const unsigned int region_size = this->m_scales_information[current_scale].getRegionSize();
                    const Tuple<float, unsigned int> * __restrict__ spatial_ptr = m_spatial_mask[current_scale];
                    
                    descriptors[idx].set(number_of_bins, 0.0);
                    for (unsigned int y = locations[idx].getY(), cy = 0; cy < region_size; ++cy, ++y)
                    {
                        // Initialize the features for the current row ............................
                        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                            feature_ptrs[channel] = image_features[current_scale].get(locations[idx].getX() * m_number_of_feature_bins_per_pixel, y, channel);
                        
                        // Process the features of the current row ................................
                        for (unsigned int cx = 0; cx < region_size; ++cx)
                        {
                            for (unsigned int spatial_index = 0; spatial_index < m_number_of_spatial_bins_per_pixel; ++spatial_index, ++spatial_ptr)
                            {
                                const float spatial_weight = spatial_ptr->getFirst();
                                
                                if (spatial_weight > 0)
                                {
                                    const unsigned int spatial_bin = spatial_ptr->getSecond() * this->m_number_of_feature_bins;
                                    double * descriptor_ptr = &descriptors[idx][spatial_bin];
                                    
                                    for (unsigned int k = 0; k < this->m_number_of_feature_bins; ++k)
                                        pixel_values[k] = 0.0;
                                    features_maximum_value.setValue(0.0);
                                    for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                                    {
                                        for (unsigned int k = 1; k < features_block_counter; ++k) features_current_value[k] = 0.0;
                                        for (unsigned int feature_index = 0; feature_index < m_number_of_feature_bins_per_pixel; ++feature_index)
                                        {
                                            double current_value = (double)feature_ptrs[channel][feature_index].getFirst();
                                            if (features_block_id[feature_index] == 0)
                                            {
                                                unsigned int current_index = feature_ptrs[channel][feature_index].getSecond();
                                                if (srvAbs<double>(current_value) > srvAbs<double>(pixel_values[current_index]))
                                                    pixel_values[current_index] = current_value;
                                            }
                                            else features_current_value[features_block_id[feature_index]] += srvAbs<double>(current_value);
                                        }
                                        for (unsigned int k = 1; k < features_block_counter; ++k)
                                        {
                                            // If the accumulated strength of the feature is bigger than current feature strength...
                                            if (features_current_value[k] > features_maximum_value[k])
                                            {
                                                // Update the strength of the feature.
                                                features_maximum_value[k] = features_current_value[k];
                                                // Set the descriptor update values of the feature to zero.
                                                for (unsigned int m = std::get<0>(features_endpoints[k]); m < std::get<1>(features_endpoints[k]); ++m)
                                                    pixel_values[m] = 0;
                                                // Put the values of the feature of the current channel in the pixel values.
                                                for (unsigned int m = std::get<2>(features_endpoints[k]); m < std::get<3>(features_endpoints[k]); ++m)
                                                    pixel_values[feature_ptrs[channel][m].getSecond()] = (double)feature_ptrs[channel][m].getFirst();
                                            }
                                        }
                                    }
                                    for (unsigned int feature_index = 0; feature_index < this->m_number_of_feature_bins; ++feature_index)
                                        descriptor_ptr[feature_index] += pixel_values[feature_index] * (double)spatial_weight;
                                }
                            }
                            
                            // Move the feature to the next pixel .................................
                            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                                feature_ptrs[channel] += m_number_of_feature_bins_per_pixel;
                        }
                    }
                    ////std::cout << descriptors[idx] << std::endl;
                }
            }
            else // General case ..................................................................
            {
                const unsigned int number_of_bins_per_channel = this->m_number_of_feature_bins * number_of_channels;
                /***
                if (number_of_channels == 3)
                {
                    srv::Figure figure_r("debug_r"), figure_g("debug_g"), figure_b("debug_b");
                    Image<short> image_debug_r, image_debug_g, image_debug_b;
                    image_debug_r.setGeometry(image_features[0].getWidth(), image_features[0].getHeight(), 1);
                    image_debug_g.setGeometry(image_features[0].getWidth(), image_features[0].getHeight(), 1);
                    image_debug_b.setGeometry(image_features[0].getWidth(), image_features[0].getHeight(), 1);
                    for (unsigned int y = 0; y < image_features[0].getHeight(); ++y)
                    {
                        const Tuple<TFEATURE, unsigned int> * feature_r = image_features[0].get(y, 0);
                        const Tuple<TFEATURE, unsigned int> * feature_g = image_features[0].get(y, 1);
                        const Tuple<TFEATURE, unsigned int> * feature_b = image_features[0].get(y, 2);
                        short * out_r = image_debug_r.get(y, 0);
                        short * out_g = image_debug_g.get(y, 0);
                        short * out_b = image_debug_b.get(y, 0);
                        for (unsigned int x = 0; x < image_features[0].getWidth(); ++x, ++feature_r, ++feature_g, ++feature_b, ++out_r, ++out_g, ++out_b)
                        {
                            *out_r = feature_r->getFirst();
                            *out_g = feature_g->getFirst();
                            *out_b = feature_b->getFirst();
                        }
                    }
                    figure_r(image_debug_r);
                    figure_g(image_debug_g);
                    figure_b(image_debug_b);
                    for (char key = 0; key != 27; key = srv::Figure::wait());
                }
                else
                {
                    srv::Figure figure("debug");
                    Image<short> image_debug;
                    image_debug.setGeometry(image_features[0].getWidth(), image_features[0].getHeight(), 1);
                    for (unsigned int y = 0; y < image_features[0].getHeight(); ++y)
                    {
                        const Tuple<TFEATURE, unsigned int> * feature = image_features[0].get(y, 0);
                        short * out = image_debug.get(y, 0);
                        for (unsigned int x = 0; x < image_features[0].getWidth(); ++x, ++feature, ++out)
                            *out = feature->getFirst();
                    }
                    figure(image_debug);
                    for (char key = 0; key != 27; key = srv::Figure::wait());
                }
                */
                
                for (unsigned int idx = omp_get_thread_num(); idx < locations.size(); idx += number_of_threads)
                {
                    const unsigned int current_scale = locations[idx].getScale();
                    const unsigned int region_size = this->m_scales_information[current_scale].getRegionSize();
                    const Tuple<float, unsigned int> * __restrict__ spatial_ptr = m_spatial_mask[current_scale];
                    
                    descriptors[idx].set(number_of_bins, 0.0);
                    for (unsigned int y = locations[idx].getY(), cy = 0; cy < region_size; ++cy, ++y)
                    {
                        // Initialize the features for the current row ............................
                        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                            feature_ptrs[channel] = image_features[current_scale].get(locations[idx].getX() * m_number_of_feature_bins_per_pixel, y, channel);
                        
                        // Process the features of the current row ................................
                        for (unsigned int cx = 0; cx < region_size; ++cx)
                        {
                            for (unsigned int spatial_index = 0; spatial_index < m_number_of_spatial_bins_per_pixel; ++spatial_index, ++spatial_ptr)
                            {
                                const float spatial_weight = spatial_ptr->getFirst();
                                
                                if (spatial_weight > 0)
                                {
                                    const unsigned int spatial_bin = spatial_ptr->getSecond() * number_of_bins_per_channel;
                                    
                                    for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                                    {
                                        double * descriptor_ptr = &descriptors[idx][spatial_bin + channel * this->m_number_of_feature_bins];
                                        
                                        for (unsigned int feature_index = 0; feature_index < m_number_of_feature_bins_per_pixel; ++feature_index)
                                            descriptor_ptr[feature_ptrs[channel][feature_index].getSecond()] += (double)feature_ptrs[channel][feature_index].getFirst() * (double)spatial_weight;
                                    }
                                }
                            }
                            
                            // Move the feature to the next pixel .................................
                            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                                feature_ptrs[channel] += m_number_of_feature_bins_per_pixel;
                        }
                    }
                    //std::cout << descriptors[idx] << std::endl;
                }
            }
        }
        
        // Free allocated memory ..................................................................
        freeFilterImages(filter_images, image_features);
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::normalizeDescriptors(unsigned int number_of_channels, VectorDense<double> * original_descriptors, unsigned int number_of_descriptors, double descriptor_norm_threshold, VectorDense<double> * descriptor_norms, unsigned int number_of_threads) const
    {
        unsigned int channels_size_modifier;
        
        if      (this->m_color_method <= DD_MAXIMUM_CHANNEL     ) channels_size_modifier = 1;
        else if (this->m_color_method <  DD_INDEPENDENT_CHANNELS) channels_size_modifier = 3;
        else channels_size_modifier = number_of_channels;
        
            if (descriptor_norms != 0) descriptor_norms->set(number_of_descriptors);
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int idx = omp_get_thread_num(); idx < number_of_descriptors; idx += number_of_threads)
            {
                VectorDense<double> &current_descriptor = original_descriptors[idx];
                double accumulated_norm;
                
                // Normalize the descriptor spatially.
                accumulated_norm = 0;
                for (unsigned int i = 0, j = 0; i < m_number_of_spatial_levels; ++i)
                {
                    const unsigned int current_size = channels_size_modifier * this->m_number_of_feature_bins * m_number_of_spatial_bins_per_level[i];
                    SubVectorDense<double> current_spatial_subvector(&current_descriptor[j], current_size);
                    double current_norm;
                    
                    current_norm = m_spatial_norm.norm(current_spatial_subvector);
                    current_spatial_subvector /= current_norm;
                    accumulated_norm += current_norm;
                    j += current_size;
                }
                
                if (descriptor_norms != 0) (*descriptor_norms)[idx] = accumulated_norm;
                if (accumulated_norm >= descriptor_norm_threshold) // Is a valid descriptor.
                {
                    // Normalize using the global norm.
                    current_descriptor /= m_norm.norm(current_descriptor);
                    // Apply the cutoff threshold when needed.
                    if (m_cutoff < 1.0)
                    {
                        for (unsigned int i = 0; i < current_descriptor.size(); ++i)
                        {
                            if ((current_descriptor[i] >= 0) && (current_descriptor[i] > m_cutoff))
                                current_descriptor[i] = m_cutoff;
                            else if ((current_descriptor[i] < 0) && (current_descriptor[i] < -m_cutoff))
                                current_descriptor[i] = -m_cutoff;
                        }
                        current_descriptor /= m_norm.norm(current_descriptor);
                    }
                    // Apply the power factor when needed.
                    if (m_power < 1.0)
                    {
                        for (unsigned int i = 0; i < current_descriptor.size(); ++i)
                        {
                            if (current_descriptor[i] >= 0)
                                current_descriptor[i] = pow(current_descriptor[i], m_power);
                            else current_descriptor[i] = -pow(-current_descriptor[i], m_power);
                        }
                        current_descriptor /= m_norm.norm(current_descriptor);
                    }
                }
                else original_descriptors[idx].set(0); // The descriptor has been rejected!
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::convertAttributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Number_Of_Features", m_number_of_features);
        parser.setAttribute("Number_Of_Spatial_Levels", m_number_of_spatial_levels);
        parser.setAttribute("Spatial_Initial_X", m_spatial_initial_x);
        parser.setAttribute("Spatial_Initial_Y", m_spatial_initial_y);
        parser.setAttribute("Spatial_Degree_X", m_spatial_degree_x);
        parser.setAttribute("Spatial_Degree_Y", m_spatial_degree_y);
        parser.setAttribute("Cutoff", m_cutoff);
        parser.setAttribute("Power", m_power);
        parser.setAttribute("Gaussian_Method", (int)m_gaussian_method);
        parser.setAttribute("Spatial_Method", (int)m_spatial_method);
        parser.setAttribute("Spatial_Interpolation", m_spatial_interpolation);
        parser.setAttribute("Spatial_Gaussian", m_spatial_gaussian);
        parser.setAttribute("Spatial_Gaussian_Sigma", m_spatial_gaussian_sigma);
        parser.setAttribute("Number_Of_Spatial_Bins_Per_Pixel", m_number_of_spatial_bins_per_pixel);
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::convertDataToXML(XmlParser &parser) const
    {
        parser.setSucceedingAttribute("Spatial_Norm", true);
        m_spatial_norm.convertToXML(parser);
        parser.setSucceedingAttribute("Spatial_Norm", false);
        m_norm.convertToXML(parser);
        for (unsigned int i = 0; i < this->m_number_of_features; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_features[i]->convertToXML(parser);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::freeXML(void)
    {
        if (m_features_number_of_bins != 0) delete [] m_features_number_of_bins;
        if (m_number_of_spatial_bins_per_level != 0) delete [] m_number_of_spatial_bins_per_level;
        for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
            if (m_filter_images[i] != 0) delete m_filter_images[i];
        freeSpatialMask();
        freeFilterImages(this->m_filter_images, m_image_features);
        freeFeatures();
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::convertAttributesFromXML(XmlParser &parser)
    {
        m_number_of_features = parser.getAttribute("Number_Of_Features");
        m_number_of_spatial_levels = parser.getAttribute("Number_Of_Spatial_Levels");
        m_spatial_initial_x = parser.getAttribute("Spatial_Initial_X");
        m_spatial_initial_y = parser.getAttribute("Spatial_Initial_Y");
        m_spatial_degree_x = parser.getAttribute("Spatial_Degree_X");
        m_spatial_degree_y = parser.getAttribute("Spatial_Degree_Y");
        m_cutoff = parser.getAttribute("Cutoff");
        m_power = parser.getAttribute("Power");
        m_gaussian_method = (DDESCRIPTOR_GAUSSIAN_KERNEL)((int)parser.getAttribute("Gaussian_Method"));
        m_spatial_method = (DDESCRIPTOR_SPATIAL)((int)parser.getAttribute("Spatial_Method"));
        m_spatial_interpolation = parser.getAttribute("Spatial_Interpolation");
        m_spatial_gaussian = parser.getAttribute("Spatial_Gaussian");
        m_spatial_gaussian_sigma = parser.getAttribute("Spatial_Gaussian_Sigma");
        m_number_of_spatial_bins_per_pixel = parser.getAttribute("Number_Of_Spatial_Bins_Per_Pixel");
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::initializeXML(void)
    {
        for (unsigned int i = 0; i < DDESCRIPTOR_NUMBER_FILTERS; ++i)
        {
            m_filter_enable[i] = false;
             m_filter_images[i] = 0;
        }
        if (m_number_of_features > 0)
        {
            m_features_number_of_bins = new unsigned int[m_number_of_features];
            for (unsigned int i = 0; i < m_number_of_features; ++i)
                m_features_number_of_bins[i] = 0;
        }
        else m_features_number_of_bins = 0;
        m_features = new ImageFeatureBase<TFILTER, TFEATURE> * [this->m_number_of_features];
        m_features_number_of_bins_per_pixel = new unsigned int[this->m_number_of_features];
    }
    
    template <class TFILTER, class TFEATURE>
    bool UprightDescriptor<TFILTER, TFEATURE>::convertDataFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Vector_Norm"))
        {
            bool spatial_norm;
            
            spatial_norm = parser.getAttribute("Spatial_Norm");
            if (spatial_norm) m_spatial_norm.convertFromXML(parser);
            else m_norm.convertFromXML(parser);
            return true;
        }
        else if (parser.isTagIdentifier("Image_Feature_Information"))
        {
            unsigned int index = parser.getAttribute("ID");
            
            m_features[index] = ImageFeatureFactory<TFILTER, TFEATURE>::Type::getInstantiator((DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE)((int)parser.getAttribute("Identifier")));
            m_features[index]->convertFromXML(parser);
            m_features_number_of_bins_per_pixel[index] = m_features[index]->getNumberOfFeaturesPerPixel();
            m_features[index]->selectFilters(this->m_filter_enable);
            this->m_features_number_of_bins[index] = m_features[index]->getNumberOfFeatures();
            this->m_descriptor_has_negative_values = this->m_descriptor_has_negative_values || m_features[index]->hasNegativeFeatures();
            m_number_of_feature_bins_per_pixel += m_features_number_of_bins_per_pixel[index];
            this->m_number_of_feature_bins += this->m_features_number_of_bins[index];
            
            return true;
        }
        else return false;
    }
    
    template <class TFILTER, class TFEATURE>
    void UprightDescriptor<TFILTER, TFEATURE>::processXML(void)
    {
        initSpatialMask();
        initFilterImages(this->m_filter_images, m_image_features);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

