// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_FEATURE_GRADIENT_HEADER_FILE__
#define __SRV_IMAGE_FEATURE_GRADIENT_HEADER_FILE__

#include "srv_image_feature_base.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GRADIENT IMAGE FEATURE               |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// This class implements the gradient based image features.
    template <class TFILTER, class TFEATURE>
    class GradientImageFeature : public ImageFeatureBase<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        GradientImageFeature(void) : ImageFeatureBase<TFILTER, TFEATURE>() {}
        /// Destructor.
        ~GradientImageFeature(void) {}
        
        // -[ Pure virtual functions ]---------------------------------------------------------------------------------------------------------------
        /// Returns the number of image features created at each pixel of the image.
        inline unsigned int getNumberOfFeaturesPerPixel(void) const { return 4; }
        /// Returns the total number of image features created.
        inline unsigned int getNumberOfFeatures(void) const { return 4; }
        /// Selects the filters needed by the object to calculate the image features.
        void selectFilters(bool * enable_vector) const;
        /// Generates the image map from the filter images.
        void generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const;
        /// Returns a boolean flag which is true when the negative features are generated.
        inline bool hasNegativeFeatures(void) const { return true; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        inline ImageFeatureBase<TFILTER, TFEATURE> * duplicate(void) const { return (ImageFeatureBase<TFILTER, TFEATURE> *)new GradientImageFeature(*this); }
        /// Returns the class identifier for the current descriptor feature method.
        inline static DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getClassIdentifier(void) { return DD_UPRIGHT_GRADIENT; }
        /// Generates a new empty instance of the image feature.
        inline static ImageFeatureBase<TFILTER, TFEATURE> * generateObject(void) { return (ImageFeatureBase<TFILTER, TFEATURE> *)new GradientImageFeature(); }
        /// Returns a flag which states if the image feature class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the image feature type identifier of the current object.
        inline DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getIdentifier(void) const { return DD_UPRIGHT_GRADIENT; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the image feature object into a XML object.
        inline void convertAttributesToXML(XmlParser &/*parser*/) const {}
        /// Retrieves the image feature object into a XML object.
        inline void convertAttributesFromXML(XmlParser &/*parser*/) {}
        
    protected:
        // -[ Static variables of the factory ]------------------------------------------------------------------------------------------------------
        /// Flag which indicates that the object has been added to the factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GRADIENT IMAGE FEATURE               |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    void GradientImageFeature<TFILTER, TFEATURE>::selectFilters(bool * enable_vector) const
    {
        enable_vector[DD_GAUSSIAN_DX] = true;
        enable_vector[DD_GAUSSIAN_DY] = true;
    }
    
    template <class TFILTER, class TFEATURE>
    void GradientImageFeature<TFILTER, TFEATURE>::generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            
            for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                {
                    const TFILTER * __restrict__ gradient_dx_ptr = filter_images[DD_GAUSSIAN_DX]->get(y, c);
                    const TFILTER * __restrict__ gradient_dy_ptr = filter_images[DD_GAUSSIAN_DY]->get(y, c);
                    Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                    
                    for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                    {
                        feature_map_ptr[offset + 0].setData((TFEATURE)*gradient_dx_ptr,                          feature_offset + 0);
                        feature_map_ptr[offset + 1].setData((TFEATURE)srvAbs<TFILTER>(*gradient_dx_ptr), feature_offset + 1);
                        feature_map_ptr[offset + 2].setData((TFEATURE)*gradient_dy_ptr,                          feature_offset + 2);
                        feature_map_ptr[offset + 3].setData((TFEATURE)srvAbs<TFILTER>(*gradient_dy_ptr), feature_offset + 3);
                        ++gradient_dx_ptr;
                        ++gradient_dy_ptr;
                        feature_map_ptr += feature_step;
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

