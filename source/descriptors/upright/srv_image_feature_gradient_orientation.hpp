// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_FEATURE_GRADIENT_ORIENTATION_HEADER_FILE__
#define __SRV_IMAGE_FEATURE_GRADIENT_ORIENTATION_HEADER_FILE__

#include "srv_image_feature_base.hpp"
#include <map>
#include <vector>
#include <limits>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | ORIENTATION OF THE GRADIENT IMAGE    |
    //                   | FEATURE - DECLARATION                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Gradient Orientation feature.
     *  Generate image features from the orientation of the gradient of the image. These features are used by descriptors like SIFT or GLOH.
     */
    template <class TFILTER, class TFEATURE>
    class GradientOrientationImageFeature : public ImageFeatureBase<TFILTER, TFEATURE>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        GradientOrientationImageFeature(void) :
            m_number_of_bins(8),
            m_interpolation(false),
            m_cosine(new double[8]),
            m_sine(new double[8])
        {
            const double step = 6.283185307179586 / 8.0;
            for (unsigned int i = 0; i < 8; ++i)
            {
                double angle = step * (double)i - 6.283185307179586 / 2.0;
                m_cosine[i] = cos(angle);
                m_sine[i] = sin(angle);
            }
        }
        /** Creates an gradient orientation feature object with the given number of orientation.
         *  \param[in] number_of_bins number of gradient orientation features.
         *  \param[in] interpolation enables or disables the interpolation between gradient orientation features.
         */
        GradientOrientationImageFeature(unsigned int number_of_bins, bool interpolation) :
            m_number_of_bins(number_of_bins),
            m_interpolation(interpolation),
            m_cosine(new double[number_of_bins]),
            m_sine(new double[number_of_bins])
        {
            const double step = 6.283185307179586 / ((double)number_of_bins);
            for (unsigned int i = 0; i < number_of_bins; ++i)
            {
                double angle = step * (double)i - 6.283185307179586 / 2.0;
                m_cosine[i] = cos(angle);
                m_sine[i] = sin(angle);
            }
        }
        /// Copy constructor.
        GradientOrientationImageFeature(const GradientOrientationImageFeature<TFILTER, TFEATURE> &other) :
            m_number_of_bins(other.m_number_of_bins),
            m_interpolation(other.m_interpolation),
            m_cosine(new double[other.m_number_of_bins]),
            m_sine(new double[other.m_number_of_bins])
        {
            for (unsigned int i = 0; i < other.m_number_of_bins; ++i)
            {
                m_cosine[i] = other.m_cosine[i];
                m_sine[i] = other.m_sine[i];
            }
        }
        /// Destructor.
        ~GradientOrientationImageFeature(void)
        {
            delete [] m_cosine;
            delete [] m_sine;
        }
        /// Assignation operator.
        GradientOrientationImageFeature<TFILTER, TFEATURE>& operator=(const GradientOrientationImageFeature<TFILTER, TFEATURE> &other);
        
        // -[ Image feature access functions ]-------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which is true when the contribution of the gradient is interpolated between gradient bins.
        inline bool getInterpolation(void) const { return m_interpolation; }
        /** Sets the number of orientation of the gradient orientation features.
         *  \param[in] number_of_bins number of gradient orientation features.
         *  \param[in] interpolation enables or disables the interpolation between gradient orientation features.
         */
        void set(unsigned int number_of_bins, bool interpolation);
        /// Returns the number of image features created at each pixel of the image.
        inline unsigned int getNumberOfFeaturesPerPixel(void) const { return (m_interpolation)?m_number_of_bins:1; }
        /// Returns the total number of image features created.
        inline unsigned int getNumberOfFeatures(void) const { return m_number_of_bins; }
        /// Selects the filters needed by the object to calculate the image features.
        inline void selectFilters(bool * enable_vector) const
        {
            enable_vector[DD_GAUSSIAN_DX] = true;
            enable_vector[DD_GAUSSIAN_DY] = true;
        }
        /// Returns a boolean flag which is true when the negative features are generated.
        inline bool hasNegativeFeatures(void) const { return false; }
        
        // -[ Image feature map generation functions ]-----------------------------------------------------------------------------------------------
        /// Generates the image map from the filter images.
        void generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Creates an exact copy of the object (virtual constructor).
        inline ImageFeatureBase<TFILTER, TFEATURE> * duplicate(void) const { return (ImageFeatureBase<TFILTER, TFEATURE> *)new GradientOrientationImageFeature(*this); }
        /// Returns the class identifier for the current descriptor feature method.
        inline static DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getClassIdentifier(void) { return DD_UPRIGHT_GRADIENT_ORIENTATION; }
        /// Generates a new empty instance of the image feature.
        inline static ImageFeatureBase<TFILTER, TFEATURE> * generateObject(void) { return (ImageFeatureBase<TFILTER, TFEATURE> *)new GradientOrientationImageFeature(); }
        /// Returns a flag which states if the image feature class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the image feature type identifier of the current object.
        inline DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE getIdentifier(void) const { return DD_UPRIGHT_GRADIENT_ORIENTATION; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the image feature object into a XML object.
        void convertAttributesToXML(XmlParser &parser) const;
        /// Retrieves the image feature object into a XML object.
        void convertAttributesFromXML(XmlParser &parser);
        
    protected:
        // -[ Parameters of the oriented gradient filters ]------------------------------------------------------------------------------------------
        /// Number of orientation bins.
        unsigned int m_number_of_bins;
        /// Interpolation between orientation bins.
        bool m_interpolation;
        /// Cosines of each oriented filter.
        double * m_cosine;
        /// Sines of each oriented filter.
        double * m_sine;
        
        // -[ Static variables of the factory ]------------------------------------------------------------------------------------------------------
        /// Flag which indicates that the object has been added to the factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | ORIENTATION OF THE GRADIENT IMAGE    |
    //                   | FEATURE - IMPLEMENTATION             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TFILTER, class TFEATURE>
    GradientOrientationImageFeature<TFILTER, TFEATURE>& GradientOrientationImageFeature<TFILTER, TFEATURE>::operator=(const GradientOrientationImageFeature<TFILTER, TFEATURE> &other)
    {
        if (this != &other)
        {
            delete [] m_cosine;
            delete [] m_sine;
            
            m_number_of_bins = other.m_number_of_bins;
            m_interpolation = other.m_interpolation;
            m_cosine = new double[other.m_number_of_bins];
            m_sine = new double[other.m_number_of_bins];
            for (unsigned int i = 0; i < other.m_number_of_bins; ++i)
            {
                m_cosine[i] = other.m_cosine[i];
                m_sine[i] = other.m_sine[i];
            }
        }
        
        return *this;
    }
    
    template <class TFILTER, class TFEATURE>
    void GradientOrientationImageFeature<TFILTER, TFEATURE>::generateFeatureMap(Image<TFILTER> const * const * filter_images, Image<Tuple<TFEATURE, unsigned int> > &image_features, unsigned int offset, unsigned int feature_offset, unsigned int feature_step, unsigned int number_of_threads) const
    {
        ////srv::Figure figure_dx("DX"), figure_dy("DY");;
        ////figure_dx(ImageMaxcon((*filter_images[DD_GAUSSIAN_DX] + 255.0) / 2.0));
        ////figure_dy(ImageMaxcon((*filter_images[DD_GAUSSIAN_DY] + 255.0) / 2.0));
        ////srv::Figure::wait();
        ////std::cout << image_features.getWidth() << " " << feature_step << " " << image_features.getWidth() / feature_step << std::endl;
        ////std::cout << filter_images[DD_GAUSSIAN_DX]->getWidth() << " " << filter_images[DD_GAUSSIAN_DX]->getHeight() << " " << filter_images[DD_GAUSSIAN_DX]->getNumberOfChannels() << std::endl;
        if (m_interpolation)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                    {
                        const TFILTER * __restrict__ gradient_dx_ptr = filter_images[DD_GAUSSIAN_DX]->get(y, c);
                        const TFILTER * __restrict__ gradient_dy_ptr = filter_images[DD_GAUSSIAN_DY]->get(y, c);
                        Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                        
                        for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                        {
                            for (unsigned int k = 0; k < m_number_of_bins; ++k)
                            {
                                const double weight = (double)*gradient_dx_ptr * m_cosine[k] + (double)*gradient_dy_ptr * m_sine[k];
                                
                                if (weight > 0)
                                    feature_map_ptr[offset + k].setData((TFEATURE)weight, feature_offset + k);
                                else  feature_map_ptr[offset + k].setData(0, feature_offset + k);
                            }
                            
                            ++gradient_dx_ptr;
                            ++gradient_dy_ptr;
                            feature_map_ptr += feature_step;
                        }
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < image_features.getHeight(); y += number_of_threads)
                    {
                        const TFILTER * __restrict__ gradient_dx_ptr = filter_images[DD_GAUSSIAN_DX]->get(y, c);
                        const TFILTER * __restrict__ gradient_dy_ptr = filter_images[DD_GAUSSIAN_DY]->get(y, c);
                        Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
                        
                        for (unsigned int x = 0; x < image_features.getWidth(); x += feature_step)
                        {
                            double maximum_weight = 0;
                            unsigned int maximum_index = 0;
                            
                            for (unsigned int k = 0; k < m_number_of_bins; ++k)
                            {
                                const double weight = (double)*gradient_dx_ptr * m_cosine[k] + (double)*gradient_dy_ptr * m_sine[k];
                                if (weight > maximum_weight)
                                {
                                    maximum_weight = weight;
                                    maximum_index = k;
                                }
                            }
                            feature_map_ptr[offset].setData((TFEATURE)maximum_weight, feature_offset + maximum_index);
                            
                            ++gradient_dx_ptr;
                            ++gradient_dy_ptr;
                            feature_map_ptr += feature_step;
                        }
                    }
                }
            }
        }
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
        ////// Figure debug("Debug");
        ////// const unsigned int current_width = filter_images[DD_GAUSSIAN_DX]->getWidth();
        ////// const unsigned int current_height = filter_images[DD_GAUSSIAN_DX]->getHeight();
        ////// Image<unsigned char> feature_map(current_width, current_height, 1);
        ////// Image<unsigned char> color_feature_map(current_width, current_height, 3);
        ////// TFEATURE maximum_value;
        ////// for (unsigned int current_bin = 0; current_bin < m_number_of_bins; ++current_bin)
        ////// {
        //////     for (unsigned int c = 0; c < image_features.getNumberOfChannels(); ++c)
        //////     {
        //////         maximum_value = 0;
        //////         for (unsigned int y = 0; y < image_features.getHeight(); ++y)
        //////         {
        //////             Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
        //////             for (unsigned int x = 0; x < image_features.getWidth(); ++x, ++feature_map_ptr)
        //////             {
        //////                 if (feature_map_ptr->getSecond() == offset + current_bin)
        //////                 {
        //////                     if (feature_map_ptr->getFirst() > maximum_value)
        //////                         maximum_value = feature_map_ptr->getFirst();
        //////                 }
        //////             }
        //////         }
        //////         feature_map.setValue(0);
        //////         for (unsigned int y = 0; y < image_features.getHeight(); ++y)
        //////         {
        //////             Tuple<TFEATURE, unsigned int> * __restrict__ feature_map_ptr = image_features.get(y, c);
        //////             for (unsigned int x = 0; x < image_features.getWidth(); ++x, ++feature_map_ptr)
        //////             {
        //////                 if (feature_map_ptr->getSecond() == offset + current_bin)
        //////                 {
        //////                     const unsigned int actual_x = x / feature_step;
        //////                     *feature_map.get(actual_x, y, 0) = (unsigned char)(255.0 * (double)feature_map_ptr->getFirst() / (double)maximum_value);
        //////                 }
        //////             }
        //////         }
        //////         std::cout << "Bin = " << current_bin + offset << "; Channel = " << c << "; Maximum value = " << maximum_value << std::endl;
        //////         
        //////         //color_feature_map = ImageGray2HueColormap(feature_map);
        //////         debug(feature_map);
        //////         Figure::wait();
        //////     }
        ////// }
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
        ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
    }
    
    template <class TFILTER, class TFEATURE>
    void GradientOrientationImageFeature<TFILTER, TFEATURE>::set(unsigned int number_of_bins, bool interpolation)
    {
        const double step = 6.283185307179586 / ((double)number_of_bins);
        
        delete [] m_cosine;
        delete [] m_sine;
        
        m_number_of_bins = number_of_bins;
        m_interpolation = interpolation;
        m_cosine = new double[number_of_bins];
        m_sine = new double[number_of_bins];
        for (unsigned int i = 0; i < number_of_bins; ++i)
        {
            double angle = step * (double)i - 6.283185307179586 / 2.0;
            m_cosine[i] = cos(angle);
            m_sine[i] = sin(angle);
        }
    }
    
    template <class TFILTER, class TFEATURE>
    void GradientOrientationImageFeature<TFILTER, TFEATURE>::convertAttributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Number_Bins", m_number_of_bins);
        parser.setAttribute("Interpolation", m_interpolation);
    }
    
    template <class TFILTER, class TFEATURE>
    void GradientOrientationImageFeature<TFILTER, TFEATURE>::convertAttributesFromXML(XmlParser &parser)
    {
        unsigned int number_of_bins;
        bool interpolation;
        
        number_of_bins = parser.getAttribute("Number_Bins");
        interpolation = parser.getAttribute("Interpolation");
        set(number_of_bins, interpolation);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

