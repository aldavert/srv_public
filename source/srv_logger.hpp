// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_LOG_INFORMATION_HEADER_FILE__
#define __SRV_LOG_INFORMATION_HEADER_FILE__

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include "srv_utilities.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | BASE LOGGER CLASS                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which defines the base logger information object.
    class BaseLogger
    {
    public:
        /// Default constructor.
        BaseLogger(void) {}
        /// Virtual destructor.
        virtual ~BaseLogger(void) {}
        /// Sens a message to the logger.
        void log(const char * format, ...);
    protected:
        /// Pure virtual function which implements the log function.
        virtual void process_message(void) = 0;
        /// Array of characters which stores the message to be logged.
        char m_message[4096];
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | OUTPUT STREAM LOGGER CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class StreamLogger : public BaseLogger
    {
    public:
        /// Default constructor which initializes a stream logger.
        StreamLogger(std::ostream * out) :
            BaseLogger(),
            m_disable_timer(false),
            m_output_stream(out) { m_chronometer.start(); }
        /// Destructor
        ~StreamLogger(void) {}
        /// Returns a constant reference to the chronometer.
        inline const Chronometer& getChrono(void) const { return m_chronometer; }
        /// Sets the value of the boolean flag which disables the timer.
        inline void setDisableTimer(bool disable_timer) { m_disable_timer = disable_timer; }
        /// Returns the value of the boolean flag which is true when the timer is disabled.
        inline bool getDisableTimer(void) const { return m_disable_timer; }
    protected:
        /// Sends the logger message to the output stream.
        void process_message(void);
        
        /// Boolean flag which disables the time timer.
        bool m_disable_timer;
        /// Output stream where the logger stream is redirected to.
        std::ostream * m_output_stream;
        /// Pointer to a global chronometer used to show time information.
        Chronometer m_chronometer;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

