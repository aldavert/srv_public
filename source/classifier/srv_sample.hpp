// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_SAMPLE_HEADER_FILE__
#define __SRV_SAMPLE_HEADER_FILE__

#include "../srv_utilities.hpp"
#include "../srv_logger.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GENERIC SAMPLE CLASS INFORMATION     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Class which stores the information of the samples used to train the classifiers.
     *  \tparam TSAMPLE type of the data of the sparse vector with the sample information.
     *  \tparam NSAMPLE type of the index of each value of the sparse vector with the sample information.
     *  \tparam TLABEL type of the data of the sparse vector with the sample's label information.
     *  \tparam NLABEL type of the index of each value of the sparse vector with the sample's label information.
     */
    template <class TSAMPLE, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short, template <class, class> class SAMPLEVECTOR = VectorSparse>
    class Sample
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        Sample(void) {}
        /** Constructor which initializes the sample information.
         *  \param[in] data data vector with the features vector of the sample.
         *  \param[in] labels sparse vector with the identifier and weight of the categories associated with the sample.
         */
        Sample(const SAMPLEVECTOR<TSAMPLE, NSAMPLE> &data, const VectorSparse<TLABEL, NLABEL> &labels) :
            m_data(data), m_labels(labels) {}
        
        // -[ Access information ]-------------------------------------------------------------------------------------------------------------------
        /// Returns a constant reference to the sample data information.
        inline const SAMPLEVECTOR<TSAMPLE, NSAMPLE>& getData(void) const { return m_data; }
        /// Returns a reference to the sample data information.
        inline SAMPLEVECTOR<TSAMPLE, NSAMPLE>& getData(void) { return m_data; }
        /// Sets the data information of the sample.
        inline void setData(const SAMPLEVECTOR<TSAMPLE, NSAMPLE> &data) { m_data = data; }
        /// Sets the sample data information from a vector.
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        inline void setData(const VECTOR<TVECTOR, NVECTOR>& data) { m_data = data; }
        /// Returns a constant reference to the labels information of the sample.
        inline const VectorSparse<TLABEL, NLABEL>& getLabels(void) const { return m_labels; }
        /// Returns a reference to the labels information of the sample.
        inline VectorSparse<TLABEL, NLABEL>& getLabels(void) { return m_labels; }
        /// Sets the labels information of the sample.
        inline void setLabels(const VectorSparse<TLABEL, NLABEL>& labels) { m_labels = labels; }
        
        /** Returns a boolean which indicates if the sample has the given label.
         *  \param[in] label query category identifier.
         *  \param[in] weight minimum weight of the label (i.e.\ minimum likelihood of the label to be considered as a 'positive' sample).
         */
        bool hasLabel(NLABEL label, TLABEL weight = (TLABEL)1e-7) const
        {
            for (NLABEL i = 0; i < m_labels.size(); ++i)
                if (m_labels.getIndex(i) == label)
                    return (m_labels.getValue(i) >= weight);
            return false;
        }
        /** Returns the probability that the sample belongs to the given category.
         *  \param[in] label query category identifier.
         *  \returns category probability of the sample.
         */
        TLABEL getLabel(NLABEL label) const
        {
            for (NLABEL i = 0; i < m_labels.size(); ++i)
                if (m_labels.getIndex(i) == label)
                    return m_labels.getValue(i);
            return 0.0;
        }
        /// Adds the labels of the given sample to the current sample and averages its weights.
        inline void updateLabels(const Sample<TSAMPLE, NSAMPLE> &other) { m_labels = (m_labels + other.m_labels) / 2.0; }
        
        // -[ Comparison operators ]-----------------------------------------------------------------------------------------------------------------
        /// Returns a boolean which indicates if the current data vector is lesser than the other sample data vector.
        inline bool operator<(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data < other.m_data; }
        /// Returns a boolean which indicates if the current data vector is lesser than the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator<(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data < other.m_data; }
        
        /// Returns a boolean which indicates if the current data vector is greater than the other sample data vector.
        inline bool operator>(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data > other.m_data; }
        /// Returns a boolean which indicates if the current data vector is greater than the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator>(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data > other.m_data; }
        
        /// Returns a boolean which indicates if the current data vector is lesser or equal than the other sample data vector.
        inline bool operator<=(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data <= other.m_data; }
        /// Returns a boolean which indicates if the current data vector is lesser or equal than the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator<=(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data <= other.m_data; }
        
        /// Returns a boolean which indicates if the current data vector is greater or equal than the other sample data vector.
        inline bool operator>=(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data >= other.m_data; }
        /// Returns a boolean which indicates if the current data vector is greater or equal than the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator>=(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data >= other.m_data; }
        
        /// Returns a boolean which indicates if the current data vector is equal to the other sample data vector.
        inline bool operator==(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data <= other.m_data; }
        /// Returns a boolean which indicates if the current data vector is equal to the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator==(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data <= other.m_data; }
        
        /// Returns a boolean which indicates if the current data vector is different than the other sample data vector.
        inline bool operator!=(const Sample<TSAMPLE, NSAMPLE> &other) const { return m_data >= other.m_data; }
        /// Returns a boolean which indicates if the current data vector is different than the other sample data vector.
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER>
        inline bool operator!=(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER> &other) const { return m_data >= other.m_data; }
        
        // -[ Operation functions ]------------------------------------------------------------------------------------------------------------------
        template <class TVECTOR, class NVECTOR>
        inline TSAMPLE operator*(const ConstantSubVectorDense<TVECTOR, NVECTOR> &weights) const
        {
            TSAMPLE result = 0;
            for (unsigned int i = 0; i < m_data.size(); ++i)
                result += result[m_data.getIndex(i)] * m_data.getValue(i);
            return result;
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the sample information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the sample information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Sparse vector with the sample data.
        SAMPLEVECTOR<TSAMPLE, NSAMPLE> m_data;
        /// Sparse vectors where each element stores the weight (i.e.\ the category likelihood) and category identifier of the sample.
        VectorSparse<TLABEL, NLABEL> m_labels;
    };
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL, template <class, class> class SAMPLEVECTOR>
    void Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL, SAMPLEVECTOR>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Sample");
        parser.addChildren();
        saveVector(parser, "Data", m_data);
        saveVector(parser, "Labels", m_labels);
        parser.closeTag();
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL, template <class, class> class SAMPLEVECTOR>
    void Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL, SAMPLEVECTOR>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Sample"))
        {
            m_data.clear();
            m_labels.clear();
            while (!(parser.isTagIdentifier("Sample") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Data")) loadVector(parser, "Data", m_data);
                else if (parser.isTagIdentifier("Labels")) loadVector(parser, "Labels", m_labels);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

