// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_BASE_CLASSIFIER_HEADER_FILE__
#define __SRV_BASE_CLASSIFIER_HEADER_FILE__

#include <list>
#include <algorithm>
#include <cmath>
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "../srv_xml.hpp"
#include "../srv_utilities.hpp"
#include "srv_classifier_definitions.hpp"
#include "srv_classifier_parameters.hpp"
#include "srv_classifier_validation.hpp"
#include "srv_sample.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | LINEAR BASE CLASSIFIER               |
    //                   | *** DECLARATION ***                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Base pure virtual class for the linear classifiers.
     *  \tparam TCLASSIFIER type used by the classifier weight vectors.
     *  \tparam TSAMPLE type of the data of the sparse vector with the sample information.
     *  \tparam NSAMPLE type of the index of each value of the sparse vector with the sample information.
     *  \tparam TLABEL type of the data of the sparse vector with the sample's label information.
     *  \tparam NLABEL type of the index of each value of the sparse vector with the sample's label information.
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class LinearLearnerBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LinearLearnerBase(void);
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         */
        LinearLearnerBase(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult);
        /// Copy constructor.
        LinearLearnerBase(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        LinearLearnerBase(const LinearLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        /// Destructor.
        virtual ~LinearLearnerBase(void);
        /// Assignation operator.
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const LinearLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** This function sets the dimensionality and number of categories of the linear classifiers.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifiers.
         *  \param[in] categories_identifiers array with the identifiers of each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of linear classifiers.
         */
        virtual void setClassifierParameters(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories) = 0;
        /// Returns the dimensionality of the vectors categorized by the linear classifiers (i.e.\ the dimensionality of the weight vectors of the linear classifiers).
        inline NSAMPLE getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns the number of different categories of the classifier (i.e.\ the number of linear classifiers).
        inline unsigned int getNumberOfCategories(void) const { return m_number_of_categories; }
        /// Returns a constant pointer to the array containing the category identifier of each classifier.
        inline const NLABEL * getCategoriesIdentifiers(void) const { return m_categories_identifiers; }
        /// Returns the category identifier of the index-th linear classifier.
        inline NLABEL getCategoryIdentifier(unsigned int index) const { return m_categories_identifiers[index]; }
        /// Returns a constant pointer to the array with the linear classifiers.
        inline const VectorDense<TCLASSIFIER, NSAMPLE> * getClassifiers(void) const { return m_classifiers; }
        /// Returns a constant reference to the weights of the index-th linear classifier.
        inline const VectorDense<TCLASSIFIER, NSAMPLE>& getClassifier(unsigned int index) const { return m_classifiers[index]; }
        /// Returns a constant pointer to the array with the classifier base parameters.
        inline const ClassifierBaseParameters<TCLASSIFIER> * getParameters(void) const { return m_base_parameters; }
        /// Returns a constant reference to the index-th classifier base parameter.
        inline const ClassifierBaseParameters<TCLASSIFIER>& getParameter(unsigned int index) const { return m_base_parameters[index]; }
        /// Returns the minimum probability of a sample label to set it as a positive sample.
        inline TLABEL getPositiveProbability(void) const { return m_positive_probability; }
        /// Sets the minimum probability of a sample label to set it as a positive sample.
        inline void setPositiveProbability(TLABEL positive_probability) { m_positive_probability = positive_probability; }
        /// Returns the maximum probability of the samples tagged as difficult.
        inline TLABEL getDifficultProbability(void) const { return m_difficult_probability; }
        /// Sets the maximum probability of the samples tagged as difficult.
        inline void setDifficultProbability(TLABEL difficult_probability) { m_difficult_probability = difficult_probability; }
        /// Returns the boolean flag disables the use of samples tagged as difficult.
        inline bool getIgnoreDifficult(void) const { return m_ignore_difficult; }
        /// Sets the boolean flag disables the use of samples tagged as difficult.
        inline void setIgnoreDifficult(bool ignore_difficult) { m_ignore_difficult = ignore_difficult; }
        /// Clears the model information of the linear classifiers of each category.
        virtual void clear(void);
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Pure virtual function which trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        virtual void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger) = 0;
        /** Function which trains the linear classifiers for a set of training samples with a different regularization factor for each classifier.
         *  \param[in] train_samples array of training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        inline void train(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *> train_samples_ptr(number_of_train_samples);
            
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                train_samples_ptr[i] = &train_samples[i];
            train(train_samples_ptr.getData(), number_of_train_samples, base_parameters, number_of_threads, logger);
        }
        /** Function which trains the linear classifiers for the given set of training samples and a regularization factor.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters base parameters applied to all classifiers.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        inline void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> &base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters_vector(m_number_of_categories, base_parameters);
            
            train(train_samples, number_of_train_samples, base_parameters_vector.getData(), number_of_threads, logger);
        }
        /** Function which trains the linear classifiers for the given set of training samples and a regularization factor.
         *  \param[in] train_samples array of training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters base parameters applied to all classifiers.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        inline void train(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> &base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters_vector(m_number_of_categories, base_parameters);
            VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > train_samples_ptr(number_of_train_samples);
            
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                train_samples_ptr[i] = &train_samples[i];
            train(train_samples_ptr.getData(), number_of_train_samples, base_parameters_vector.getData(), number_of_threads, logger);
        }
        
        // -[ Train functions which selects the regularization parameters ]--------------------------------------------------------------------------
        /** This function trains the linear classifiers using a validation set of samples to automatically select the best regularization parameter.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] validation_samples array of pointers to the validation samples.
         *  \param[in] number_of_validation_samples number of elements in the validation samples array.
         *  \param[in] validation_parameters object with the different validation parameters evaluated .
         *  \param[in] enable_all_samples enable to train the final classifier using both train and validation samples.
         *  \param[in] disable_final_classifier disables the generation of the final classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool enable_all_samples, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** This function trains the linear classifiers using folds to automatically select the best regularization parameter.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] number_of_folds number of folds used to obtain a validation set.
         *  \param[in] validation_parameters object with the different validation parameters evaluated .
         *  \param[in] disable_final_classifier disables the generation of the final classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, unsigned int number_of_folds, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** This function trains the linear classifiers using a validation set of samples to automatically select the best regularization parameter.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] validation_samples array of pointers to the validation samples.
         *  \param[in] number_of_validation_samples number of elements in the validation samples array.
         *  \param[in] validation_parameters object with the different validation parameters evaluated .
         *  \param[in] enable_all_samples enable to train the final classifier using both train and validation samples.
         *  \param[in] disable_final_classifier disables the generation of the final classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        inline void train(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_train_samples, const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool enable_all_samples, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *> train_samples_ptrs(number_of_train_samples), validation_samples_ptrs(number_of_validation_samples);
            
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                train_samples_ptrs[i] = &train_samples[i];
            for (unsigned int i = 0; i < number_of_validation_samples; ++i)
                validation_samples_ptrs[i] = &validation_samples[i];
            train(train_samples_ptrs.getData(), number_of_train_samples, validation_samples_ptrs.getData(), number_of_validation_samples, validation_parameters, enable_all_samples, disable_final_classifier, number_of_threads, logger);
        }
        /** This function trains the linear classifiers using folds to automatically select the best regularization parameter.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] number_of_folds number of folds used to obtain a validation set.
         *  \param[in] validation_parameters object with the different validation parameters evaluated .
         *  \param[in] disable_final_classifier disables the generation of the final classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        inline void train(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_train_samples, unsigned int number_of_folds, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *> train_samples_ptrs(number_of_train_samples);
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                train_samples_ptrs[i] = &train_samples[i];
            train(train_samples_ptrs.getData(), number_of_train_samples, number_of_folds, validation_parameters, disable_final_classifier, number_of_threads, logger);
        }
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Calculate the classification score of each linear classifier for the given sample vector.
         *  \param[in] sample vector with sample to classify.
         *  \param[out] scores dense vector with the classification score of each classifier.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(const VECTOR<TVECTOR, NVECTOR> &sample, VectorDense<TCLASSIFIER> &scores) const;
        
        /** Calculate the classification score of each linear classifier for a set of sample vectors.
         *  \param[in] samples array with the vectors to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(const VECTOR<TVECTOR, NVECTOR> * samples, VectorDense<TCLASSIFIER> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        
        /** Calculate the classification score of each linear classifier for a set of sample vectors.
         *  \param[in] samples array of pointers to the vectors to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(VECTOR<TVECTOR, NVECTOR> const * const * samples, VectorDense<TCLASSIFIER> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        
        /** Calculate the classification score of each linear classifier for the given sample.
         *  \param[in] sample vector with sample to classify.
         *  \param[out] scores dense vector with the classification score of each classifier.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void predict(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &sample, VectorDense<TCLASSIFIER> &scores) const;
        
        /** Calculate the classification score of each linear classifier for a set of samples.
         *  \param[in] samples array with the samples to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void predict(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, VectorDense<TCLASSIFIER> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        
        /** Calculate the classification score of each linear classifier for a set of samples.
         *  \param[in] samples array of pointers to the samples to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void predict(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, VectorDense<TCLASSIFIER> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        /** Returns a boolean value for each category of the linear classifier which is true when the given sample is a positive sample and false otherwise.
         *  \param[in] current_sample sample which positive classifiers are searched.
         *  \param[out] belonging_class array where the belonging class booleans are returned.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        inline void positiveClassifiers(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &current_sample, bool * belonging_class) const
        {
            for (unsigned int i = 0; i < m_number_of_categories; ++i)
                belonging_class[i] = current_sample.hasLabel(m_categories_identifiers[i], m_positive_probability);
        }
        /** Returns a boolean value for each category of the linear classifier which is true when the given sample is a positive sample and false otherwise.
         *  \param[in] current_sample sample which positive classifiers are searched.
         *  \param[out] belonging_class array where the belonging class booleans are returned.
         */
        template <class TLABEL_OTHER, class NLABEL_OTHER>
        inline void positiveClassifiers(const VectorSparse<TLABEL_OTHER, NLABEL_OTHER> &current_labels, bool * belonging_class) const
        {
            for (unsigned int i = 0; i < m_number_of_categories; ++i)
            {
                belonging_class[i] = false;
                for (NLABEL_OTHER j = 0; j < current_labels.size(); ++j)
                {
                    if (current_labels.getIndex(j) == (TLABEL_OTHER)m_categories_identifiers[i])
                    {
                        belonging_class[i] = current_labels.getValue(j) >= m_positive_probability;
                        break;
                    }
                }
            }
        }
        /** Returns a dense vector where each value corresponds to the boolean value which indicates if the sample is a positive sample of the position-th classifier.
         *  \param[in] current_sample sample which positive classifiers are searched.
         *  \param[out] belonging_class dense vector where the belonging class booleans are returned.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        inline void positiveClassifiers(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &current_sample, VectorDense<bool> &belonging_class) const
        {
            belonging_class.set(m_number_of_categories, false);
            for (unsigned int i = 0; i < (unsigned int)current_sample.getLabels().size(); ++i)
                belonging_class[current_sample.getLabels().getIndex(i)] = current_sample.getLabels().getValue(i) >= m_positive_probability;
        }
        
        // -[ Evaluation functions ]-----------------------------------------------------------------------------------------------------------------
        /** Calculates the number of positive and negative samples for each classifier.
         *  \param[in] samples an array of pointers to the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] positive vector with the number of positive samples for each classifier.
         *  \param[out] negative vector with the number of negative samples for each classifier.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculatePositiveNegative(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int> &positive, VectorDense<unsigned int> &negative) const;
        /** Calculates the number of positive and negative samples for each classifier.
         *  \param[in] samples an array of with the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] positive vector with the number of positive samples for each classifier.
         *  \param[out] negative vector with the number of negative samples for each classifier.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculatePositiveNegative(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<unsigned int> &positive, VectorDense<unsigned int> &negative) const;
        /** Calculates the mean average precision score for each classifier for the given set of samples.
         *  \param[in] samples an array of pointers to the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] mean_average_precision a dense vector where the performance scores of each classifier are going to be stored.
         *  \param[in] number_of_threads number of threads used to evaluate the classifiers concurrently.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculateMeanAveragePrecision(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<double> &mean_average_precision, unsigned int number_of_threads) const;
        /** Calculates the mean average precision score for each classifier for the given set of samples.
         *  \param[in] samples an array with the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] mean_average_precision a dense vector where the performance scores of each classifier are going to be stored.
         *  \param[in] number_of_threads number of threads used to evaluate the classifiers concurrently.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculateMeanAveragePrecision(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<double> &mean_average_precision, unsigned int number_of_threads) const;
        /** Calculates the accuracy geometric mean score for each classifier for the given set of samples.
         *  \param[in] samples an array of pointers to the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] accuracy_geometric_mean a dense vector where the performance scores of each classifier are going to be stored.
         *  \param[in] number_of_threads number of threads used to evaluate the classifiers concurrently.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculateAccuracyGeometricMean(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<double> &accuracy_geometric_mean, unsigned int number_of_threads) const;
        /** Calculates the accuracy geometric mean score for each classifier for the given set of samples.
         *  \param[in] samples an array with the evaluation samples.
         *  \param[in] number_of_samples number of elements of the array.
         *  \param[out] accuracy_geometric_mean a dense vector where the performance scores of each classifier are going to be stored.
         *  \param[in] number_of_threads number of threads used to evaluate the classifiers concurrently.
         */
        template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        void calculateAccuracyGeometricMean(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<double> &accuracy_geometric_mean, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        virtual LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const = 0;
        ///// /// Returns the class identifier for the linear learner method.
        ///// inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the linear learner.
        ///// inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new (); }
        ///// /// Returns a flag which states if the linear learner class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        virtual LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the learner information into an XML object.
        virtual void convertToXML(XmlParser &parser) const = 0;
        /// Retrieves the learner information from an XML object.
        virtual void convertFromXML(XmlParser &parser) = 0;
    protected:
        // -[ Protected functions ]------------------------------------------------------------------------------------------------------------------
        /** Initializes the classifier basic parameters, i.e.\ the dimensionality of the samples categorized by the classifier and the categories information.
         *  \param[in] number_of_dimensions number of dimensions of the vectors categorized by the linear classifiers.
         *  \param[in] categories_identifiers category identifier of each classifier.
         *  \param[in] number_of_classifiers number of linear classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         */
        void set(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult);
        /** Private function which calculates the weighted addition between a dense vector and an sparse vector. This operator is commonly used to
         *  update the weight vectors of a linear classifier
         *  \param[in,out] weight_vector weight vector to be updated.
         *  \param[in] sample sample sparse vector used to update the weight vector.
         *  \param[in] weight value used to weight the contribution of the sample.
         */
        inline void weightVectorUpdate(VectorDense<TCLASSIFIER , NSAMPLE> &weight_vector, const VectorSparse<TSAMPLE, NSAMPLE> &sample, const TCLASSIFIER &weight) const
        {
            for (NSAMPLE i = 0; i < sample.size(); ++i)
                weight_vector[sample.getIndex(i)] += (TCLASSIFIER)sample.getValue(i) * weight;
        }
        /** Private function which calculates the classification score for the given set of regularization and balancing factor values.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] validation_samples array of pointers to the validation samples.
         *  \param[in] number_of_validation_samples number of elements in the validation samples array.
         *  \param[in] validation_parameters array with the validation parameters evaluated for each classifier.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[out] score resulting vector with the classification score attained by each set of validation parameters.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        void validate(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> * validation_parameters, VectorDense<ClassifierBaseParameters<TCLASSIFIER> > &base_parameters, VectorDense<VectorDense<double> > &score, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Private function which calculates the classification score for the given set of regularization and balancing factor values.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the train samples array.
         *  \param[in] validation_samples array of pointers to the validation samples.
         *  \param[in] number_of_validation_samples number of elements in the validation samples array.
         *  \param[in] validation_parameters validation parameters evaluated for the classifiers.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[out] score resulting vector with the classification score attained by each set of validation parameters.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the validation process (set to 0 to disable validation log information).
         */
        void validate(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, VectorDense<ClassifierBaseParameters<TCLASSIFIER> > &base_parameters, VectorDense<VectorDense<double> > &score, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Calculates the dot-product between the weight vector of a classifier and a sample.
         *  \param[in] classifier classifier weight vector.
         *  \param[in] sample sample feature vector.
         *  \returns dot product between the classifier and the sample feature vector.
         */
        inline TCLASSIFIER classifierDot(const VectorDense<TCLASSIFIER, NSAMPLE> &classifier, const VectorSparse<TSAMPLE, NSAMPLE> &sample) const
        {
            TCLASSIFIER dot = 0;
            for (NSAMPLE i = 0; i < sample.size(); ++i)
                dot += classifier[sample.getIndex(i)] * (TCLASSIFIER)sample.getValue(i);
            return dot;
        }
        /** Calculates the dot-product between two vectors.
         *  \param[in] left left vector operand.
         *  \param[in] right right vector operand.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the dot product.
         *  \returns dot product between the left and right vectors.
         */
        inline TCLASSIFIER classifierDot(const VectorDense<TCLASSIFIER, NSAMPLE> &left, const VectorDense<TCLASSIFIER, NSAMPLE> &right, unsigned int number_of_threads) const
        {
            TCLASSIFIER dot = 0;
            #pragma omp parallel num_threads(number_of_threads) reduction(+:dot)
            {
                for (NSAMPLE i = (NSAMPLE)omp_get_thread_num(); i < m_number_of_dimensions; i = (NSAMPLE)(i + number_of_threads))
                    dot += left[i] * right[i];
            }
            return dot;
        }
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Number of dimensions of the samples categorized by the classifiers.
        NSAMPLE m_number_of_dimensions;
        /// Array with the identifiers of each classifier category.
        NLABEL * m_categories_identifiers;
        /// Array of the dense vectors which store the weights of the linear classifiers.
        VectorDense<TCLASSIFIER, NSAMPLE> * m_classifiers;
        /// Number of categories (i.e.\ the number of linear classifiers).
        unsigned int m_number_of_categories;
        /// Base parameters of each classifier.
        ClassifierBaseParameters<TCLASSIFIER> * m_base_parameters;
        /// Minimum probability of a label to consider it a positive sample. Samples with a class label but a probability lower than this threshold will be considered negative samples.
        TLABEL m_positive_probability;
        /// Maximum probability of the samples tagged as difficult.
        TLABEL m_difficult_probability;
        /// Boolean flag disables the use of samples tagged as difficult.
        bool m_ignore_difficult;
        //// /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | LINEAR BASE CLASSIFIER               |
    //                   | *** IMPLEMENTATION ***               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearLearnerBase(void) :
        m_number_of_dimensions(0),
        m_categories_identifiers(0),
        m_classifiers(0),
        m_number_of_categories(0),
        m_base_parameters(0),
        m_positive_probability((TLABEL)0.2),
        m_difficult_probability((TLABEL)0.5),
        m_ignore_difficult(false)
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearLearnerBase(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult) :
        m_number_of_dimensions(number_of_dimensions),
        m_categories_identifiers((number_of_categories > 0)?new NLABEL[number_of_categories]:0),
        m_classifiers((number_of_categories > 0)?new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_categories]:0),
        m_number_of_categories(number_of_categories),
        m_base_parameters((number_of_categories > 0)?new ClassifierBaseParameters<TCLASSIFIER>[number_of_categories]:0),
        m_positive_probability(positive_probability),
        m_difficult_probability(difficult_probability),
        m_ignore_difficult(ignore_difficult)
    {
        for (unsigned int i = 0; i < number_of_categories; ++i)
        {
            m_categories_identifiers[i] = categories_identifiers[i];
            m_classifiers[i].set(number_of_dimensions, (TCLASSIFIER)0);
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearLearnerBase(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_categories_identifiers((other.m_categories_identifiers != 0)?new NLABEL[other.m_number_of_categories]:0),
        m_classifiers((other.m_classifiers != 0)?new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_categories]:0),
        m_number_of_categories(other.m_number_of_categories),
        m_base_parameters((other.m_base_parameters != 0)?new ClassifierBaseParameters<TCLASSIFIER>[other.m_number_of_categories]:0),
        m_positive_probability(other.m_positive_probability),
        m_difficult_probability(other.m_difficult_probability),
        m_ignore_difficult(other.m_ignore_difficult)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_categories_identifiers[i] = other.m_categories_identifiers[i];
            m_classifiers[i] = other.m_classifiers[i];
            m_base_parameters[i] = other.m_base_parameters[i];
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearLearnerBase(const LinearLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
        m_number_of_dimensions(other.getNumberOfDimensions()),
        m_categories_identifiers((other.getNumberOfCategories() != 0)?new NLABEL[other.getNumberOfCategories()]:0),
        m_classifiers((other.getNumberOfCategories() != 0)?new VectorDense<TCLASSIFIER, NSAMPLE>[other.getNumberOfCategories()]:0),
        m_number_of_categories(other.getNumberOfCategories()),
        m_base_parameters((other.getNumberOfCategories() != 0)?new ClassifierBaseParameters<TCLASSIFIER>[other.getNumberOfCategories()]:0),
        m_positive_probability(other.getPositiveProbability()),
        m_difficult_probability(other.getDifficultProbability()),
        m_ignore_difficult(other.getIgnoreDifficult())
    {
        for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
        {
            m_categories_identifiers[i] = other.getCategoryIdentifier(i);
            m_classifiers[i] = other.getClassifier(i);
            m_base_parameters[i] = other.getParameter(i);
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::~LinearLearnerBase(void)
    {
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_base_parameters != 0) delete [] m_base_parameters;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
    {
        if (this != &other)
        {
            // -[ Free allocated memory ]------------------------------------------------------------------------------------------------------------
            if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
            if (m_classifiers != 0) delete [] m_classifiers;
            if (m_base_parameters != 0) delete [] m_base_parameters;
            
            // -[ Copy content ]---------------------------------------------------------------------------------------------------------------------
            m_number_of_dimensions = other.m_number_of_dimensions;
            if (other.m_number_of_categories > 0)
            {
                m_categories_identifiers = new NLABEL[other.m_number_of_categories];
                m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_categories];
                m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[other.m_number_of_categories];
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_categories_identifiers[i] = other.m_categories_identifiers[i];
                    m_classifiers[i] = other.m_classifiers[i];
                    m_base_parameters[i] = other.m_base_parameters[i];
                }
                m_number_of_categories = other.m_number_of_categories;
            }
            else
            {
                m_categories_identifiers = 0;
                m_classifiers = 0;
                m_base_parameters = 0;
                m_number_of_categories = 0;
            }
            m_positive_probability = other.m_positive_probability;
            m_difficult_probability = other.m_difficult_probability;
            m_ignore_difficult = other.m_ignore_difficult;
        }
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const LinearLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
    {
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_base_parameters != 0) delete [] m_base_parameters;
        
        // -[ Copy content ]-------------------------------------------------------------------------------------------------------------------------
        m_number_of_dimensions = other.getNumberOfDimensions();
        if (other.getNumberOfCategories() > 0)
        {
            m_categories_identifiers = new NLABEL[other.getNumberOfCategories()];
            m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[other.getNumberOfCategories()];
            m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[other.getNumberOfCategories()];
            for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
            {
                m_categories_identifiers[i] = other.getCategoryIdentifier(i);
                m_classifiers[i] = other.getClassifier(i);
                m_base_parameters[i] = other.getParameter(i);
            }
            m_number_of_categories = other.getNumberOfCategories();
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_base_parameters = 0;
            m_number_of_categories = 0;
        }
        m_positive_probability = other.getPositiveProbability();
        m_difficult_probability = other.getDifficultProbability();
        m_ignore_difficult= other.getIgnoreDifficult();
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::clear(void)
    {
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
            m_classifiers[i].setValue(0);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::set(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult)
    {
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_base_parameters != 0) delete [] m_base_parameters;
        
        // -[ Allocate memory for new content ]------------------------------------------------------------------------------------------------------
        m_number_of_dimensions = number_of_dimensions;
        if (number_of_categories > 0)
        {
            m_categories_identifiers = new NLABEL[number_of_categories];
            m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_categories];
            m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[number_of_categories];
            for (unsigned int i = 0; i < number_of_categories; ++i)
            {
                m_categories_identifiers[i] = categories_identifiers[i];
                m_classifiers[i].set(number_of_dimensions, (TCLASSIFIER)0);
            }
            m_number_of_categories = number_of_categories;
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_base_parameters = 0;
            m_number_of_categories = 0;
        }
        m_positive_probability = positive_probability;
        m_difficult_probability = difficult_probability;
        m_ignore_difficult = ignore_difficult;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN WITH VALIDATION FUNCTIONS ]==========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::validate(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, VectorDense<ClassifierBaseParameters<TCLASSIFIER> > &base_parameters, VectorDense<VectorDense<double> > &score, unsigned int number_of_threads, BaseLogger * logger)
    {
        const CLASSIFIER_VALIDATION_IDENTIFIER learner_validation_identifier = validation_parameters.getValidationIdentifier();
        
        for (unsigned int i = 0; i < score.size(); ++i)
        {
            // 1) Initialize the classifier base parameters with the current validation parameters.
            if (logger != 0)
                logger->log("Validation parameter %d of %d: Regularization Factor=%f; Balancing Factor=%f.", i + 1, score.size(), (double)validation_parameters.getRegularizationFactor(i), (double)validation_parameters.getBalancingFactor(i));
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                base_parameters[c].setRegularizationFactor(validation_parameters.getRegularizationFactor(i));
                base_parameters[c].setBalancingFactor(validation_parameters.getBalancingFactor(i));
            }
            // 2) Train the classifier with the current parameters.
            train(train_samples, number_of_train_samples, base_parameters.getData(), number_of_threads, logger);
            // 3) Evaluate its performance.
            if (logger != 0) logger->log("Validating the classifier...");
            if (learner_validation_identifier == CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION)
                calculateMeanAveragePrecision(validation_samples, number_of_validation_samples, score[i], number_of_threads);
            else if (learner_validation_identifier == CLASSIFIER_VALIDATION_AVERAGE_GEOMETRIC_MEAN)
                calculateAccuracyGeometricMean(validation_samples, number_of_validation_samples, score[i], number_of_threads);
            else throw Exception("Validation type %d not yet implemented or not applicable.", (int)learner_validation_identifier);
            
            if (logger != 0)
            {
                double mean_score;
                
                logger->log("-----------------------------------------------");
                mean_score = 0;
                for (unsigned int j = 0; j < score[i].size(); ++j)
                {
                    logger->log("  · Category %04d: %f", j, score[i][j]);
                    mean_score += score[i][j];
                }
                logger->log("      mean score: %f", mean_score / (double)score[i].size());
                logger->log("-----------------------------------------------");
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::validate(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> * validation_parameters, VectorDense<ClassifierBaseParameters<TCLASSIFIER> > &base_parameters, VectorDense<VectorDense<double> > &score, unsigned int number_of_threads, BaseLogger * logger)
    {
        const CLASSIFIER_VALIDATION_IDENTIFIER learner_validation_identifier = validation_parameters[0].getValidationIdentifier();
        
        for (unsigned int i = 0; i < score.size(); ++i)
        {
            // 1) Initialize the classifier base parameters with the current validation parameters.
            if (logger != 0)
            {
                logger->log("Validation parameter %d of %d:", i + 1, score.size());
                for (unsigned int c = 0; c < m_number_of_categories; ++c)
                    logger->log("   ·Category %04d: Regularization factor=%f; Balancing factor=%f.", c, (double)validation_parameters[c].getRegularizationFactor(i % validation_parameters[c].getNumberOfFactors()), (double)validation_parameters[c].getBalancingFactor(i % validation_parameters[c].getNumberOfFactors()));
            }
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                base_parameters[c].setRegularizationFactor(validation_parameters[c].getRegularizationFactor(i % validation_parameters[c].getNumberOfFactors()));
                base_parameters[c].setBalancingFactor(validation_parameters[c].getBalancingFactor(i % validation_parameters[c].getNumberOfFactors()));
            }
            // 2) Train the classifier with the current parameters.
            train(train_samples, number_of_train_samples, base_parameters.getData(), number_of_threads, logger);
            // 3) Evaluate its performance.
            if (logger != 0) logger->log("Validating the classifier...");
            if (learner_validation_identifier == CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION)
                calculateMeanAveragePrecision(validation_samples, number_of_validation_samples, score[i], number_of_threads);
            else if (learner_validation_identifier == CLASSIFIER_VALIDATION_AVERAGE_GEOMETRIC_MEAN)
                calculateAccuracyGeometricMean(validation_samples, number_of_validation_samples, score[i], number_of_threads);
            else throw Exception("Validation type %d not yet implemented or not applicable.", (int)learner_validation_identifier);
            
            if (logger != 0)
            {
                double mean_score;
                
                logger->log("-----------------------------------------------");
                mean_score = 0;
                for (unsigned int j = 0; j < score[i].size(); ++j)
                {
                    logger->log("  · Category %04d: %f", j, score[i][j]);
                    mean_score += score[i][j];
                }
                logger->log("      mean score: %f", mean_score / (double)score[i].size());
                logger->log("-----------------------------------------------");
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * validation_samples, unsigned int number_of_validation_samples, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool enable_all_samples, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<unsigned int> number_of_positive_samples, number_of_negative_samples;
        VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters(m_number_of_categories);
        
        // 1) Calculate the variables evaluated during the validation process: the regularization values and the re-weighting values.
        calculatePositiveNegative(train_samples, number_of_train_samples, number_of_positive_samples, number_of_negative_samples);
        for (unsigned int c = 0; c < m_number_of_categories; ++c)
        {
            base_parameters[c].setPositiveWeight((TCLASSIFIER)(1.0 / (double)number_of_positive_samples[c]));
            base_parameters[c].setNegativeWeight((TCLASSIFIER)(1.0 / (double)number_of_negative_samples[c]));
            m_base_parameters[c] = base_parameters[c];
        }
        
        if (this->getIdentifier() == SVM_MULTICLASS) // Classifiers which base parameters are the same for all categories.
        {
            VectorDense<VectorDense<double> > current_score;
            ClassifierValidationParameters<TCLASSIFIER> current_parameters(validation_parameters);
            double best_value, mean_score;
            unsigned int selected_value;
            bool update_parameters;
            
            // The multi-class SVM processes all samples together. Therefore, no balancing factor is needed.
            current_parameters.setBalancingFactorParameters((TCLASSIFIER)0.5, (TCLASSIFIER)0.5, (TCLASSIFIER)1, PARTITION_UNIFORM);
            current_score.set(current_parameters.getNumberOfFactors());
            for (unsigned int i = 0; i < current_score.size(); ++i)
                current_score[i].set(m_number_of_categories);
            
            // For each level of the validation space....
            best_value = -1;
            selected_value = -1;
            for (unsigned int level = 0; level < validation_parameters.getNumberOfLevels(); ++level)
            {
                // ... validate the classifiers with the current level parameters ...
                validate(train_samples, number_of_train_samples, validation_samples, number_of_validation_samples, current_parameters, base_parameters, current_score, number_of_threads, logger);
                
                // ... and select the best parameters.
                update_parameters = false;
                for (unsigned int i = 0; i < current_parameters.getNumberOfFactors(); ++i)
                {
                    mean_score = 0;
                    for (unsigned int j = 0; j < m_number_of_categories; ++j)
                        mean_score += current_score[i][j];
                    mean_score /= (double)m_number_of_categories;
                    
                    if (mean_score > best_value)
                    {
                        best_value = mean_score;
                        selected_value = i;
                        update_parameters = true;
                    }
                }
                
                if (update_parameters)
                {
                    // Set the currently selected parameters as the base parameters of the classifiers.
                    for (unsigned int i = 0; i < m_number_of_categories; ++i)
                    {
                        m_base_parameters[i].setRegularizationFactor(current_parameters.getRegularizationFactor(selected_value));
                        m_base_parameters[i].setBalancingFactor(current_parameters.getBalancingFactor(selected_value));
                    }
                    // Finally, calculate the parameter space for the next level of the pyramid.
                    current_parameters.setNextLevel(current_parameters.getRegularizationFactor(selected_value), current_parameters.getBalancingFactor(selected_value));
                }
                else current_parameters.setNextLevel(m_base_parameters[0].getRegularizationFactor(), m_base_parameters[0].getBalancingFactor());
            }
        }
        else // Classifiers which may have different base parameters for each category classifier.
        {
            VectorDense<VectorDense<double> > current_score(validation_parameters.getNumberOfFactors());
            VectorDense<ClassifierValidationParameters<TCLASSIFIER> > current_parameters(m_number_of_categories, validation_parameters);
            VectorDense<unsigned int> selected_values(m_number_of_categories, -1);
            VectorDense<double> best_scores(m_number_of_categories, -1);
            VectorDense<bool> update_parameters(m_number_of_categories);
            unsigned int score_size;
            bool resize_score;
            
            // For each level of the validation space....
            for (unsigned int i = 0; i < current_score.size(); ++i)
                current_score[i].set(m_number_of_categories);
            score_size = current_score.size();
            resize_score = false;
            for (unsigned int level = 0; level < validation_parameters.getNumberOfLevels(); ++level)
            {
                // ... validate the classifiers with the current level parameters ...
                validate(train_samples, number_of_train_samples, validation_samples, number_of_validation_samples, current_parameters.getData(), base_parameters, current_score, number_of_threads, logger);
                
                // ... and select the best parameters.
                update_parameters.setValue(false);
                for (unsigned int i = 0; i < validation_parameters.getNumberOfFactors(); ++i)
                {
                    for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    {
                        if (current_score[i][j] > best_scores[j])
                        {
                            best_scores[j] = current_score[i][j];
                            update_parameters[j] = true;
                            selected_values[j] = i % current_parameters[j].getNumberOfFactors();
                        }
                    }
                }
                
                // Set the currently selected parameters as the base parameters of the classifiers and
                // calculate the parameters for the next precision level.
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    if (update_parameters[i])
                    {
                        m_base_parameters[i].setRegularizationFactor(current_parameters[i].getRegularizationFactor(selected_values[i]));
                        m_base_parameters[i].setBalancingFactor(current_parameters[i].getBalancingFactor(selected_values[i]));
                        current_parameters[i].setNextLevel(current_parameters[i].getRegularizationFactor(selected_values[i]), current_parameters[i].getBalancingFactor(selected_values[i]));
                    }
                    else current_parameters[i].setNextLevel(m_base_parameters[i].getRegularizationFactor(), m_base_parameters[i].getBalancingFactor());
                    if (current_parameters[i].getNumberOfFactors() > score_size)
                    {
                        score_size = current_parameters[i].getNumberOfFactors();
                        resize_score = true;
                    }
                }
                if (resize_score)
                {
                    current_score.set(score_size);
                    for (unsigned int i = 0; i < score_size; ++i)
                        current_score[i].set(m_number_of_categories);
                    resize_score = false;
                }
            }
        }
        
        if (!disable_final_classifier)
        {
            // Re-train all classifiers with the best parameter selection.
            if (logger != 0) logger->log("Re-training the classifiers with the best selected parameters.");
            if (enable_all_samples)
            {
                VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *> train_samples_ptrs(number_of_train_samples + number_of_validation_samples);
                unsigned int index;
                
                index = 0;
                for (unsigned int i = 0; i < number_of_train_samples; ++i, ++index)
                    train_samples_ptrs[index] = train_samples[i];
                for (unsigned int i = 0; i < number_of_validation_samples; ++i, ++index)
                    train_samples_ptrs[index] = validation_samples[i];
                train(train_samples_ptrs.getData(), train_samples_ptrs.size(), m_base_parameters, number_of_threads, logger);
            }
            else train(train_samples, number_of_train_samples, m_base_parameters, number_of_threads, logger);
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, unsigned int number_of_folds, const ClassifierValidationParameters<TCLASSIFIER> &validation_parameters, bool disable_final_classifier, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > > sample_folds(number_of_folds);
        VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > current_train_samples;
        VectorDense<std::list<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > > folds_list(number_of_folds);
        VectorDense<VectorDense<unsigned int> > fold_category_histogram(number_of_folds);
        VectorDense<unsigned int> number_of_positive_samples, number_of_negative_samples;
        VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters(m_number_of_categories);
        
        // 1) Split the whole training samples set into the selected number of folds.
        if (logger != 0) logger->log("Splitting the training samples into %d folds.", number_of_folds);
        for (unsigned int fold = 0; fold < number_of_folds; ++fold)
            fold_category_histogram[fold].set(m_number_of_categories + 1, 0);
        for (unsigned int i = 0; i < number_of_train_samples; ++i)
        {
            unsigned int selected_fold, selected_category, frequency;
            bool background;
            
            frequency = number_of_train_samples;
            background = true;
            selected_fold = selected_category = 0;
            for (unsigned int j = 0; j < (unsigned int)train_samples[i]->getLabels().size(); ++j)
            {
                TLABEL probability;
                
                probability = train_samples[i]->getLabels().getValue((NLABEL)j);
                if ((probability > m_difficult_probability) || (!m_ignore_difficult && (probability > m_positive_probability)))
                {
                    for (unsigned int c = 0; c < m_number_of_categories; ++c)
                    {
                        if (train_samples[i]->getLabels().getIndex((NLABEL)j) == m_categories_identifiers[c])
                        {
                            for (unsigned int fold = 0; fold < number_of_folds; ++fold)
                            {
                                if (fold_category_histogram[fold][c] < frequency)
                                {
                                    selected_fold = fold;
                                    selected_category = c;
                                    frequency = fold_category_histogram[fold][c];
                                    background = false;
                                }
                            }
                        }
                    }
                }
            }
            
            if (background)
            {
                for (unsigned int fold = 0; fold < number_of_folds; ++fold)
                {
                    if (fold_category_histogram[fold][m_number_of_categories] < frequency)
                    {
                        selected_fold = fold;
                        frequency = fold_category_histogram[fold][m_number_of_categories];
                    }
                }
                ++fold_category_histogram[selected_fold][m_number_of_categories];
                folds_list[selected_fold].push_back(train_samples[i]);
            }
            else
            {
                ++fold_category_histogram[selected_fold][selected_category];
                folds_list[selected_fold].push_back(train_samples[i]);
            }
        }
        for (unsigned int fold = 0; fold < number_of_folds; ++fold)
        {
            unsigned int vector_index;
            
            vector_index = 0;
            sample_folds[fold].set((unsigned int)folds_list[fold].size());
            for (typename std::list<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *>::iterator begin = folds_list[fold].begin(), end = folds_list[fold].end(); begin != end; ++begin, ++vector_index)
                sample_folds[fold][vector_index] = *begin;
            folds_list[fold].clear();
        }
        
        if (this->getIdentifier() == SVM_MULTICLASS) // Classifiers which base parameters are the same for all categories.
        {
            VectorDense<VectorDense<double> > current_score, accumulated_score;
            ClassifierValidationParameters<TCLASSIFIER> current_parameters(validation_parameters);
            double best_value, mean_score;
            unsigned int selected_value;
            bool update_parameters;
            
            // The multi-class SVM processes all samples together. Therefore, no balancing factor is needed.
            current_parameters.setBalancingFactorParameters((TCLASSIFIER)0.5, (TCLASSIFIER)0.5, (TCLASSIFIER)1, PARTITION_UNIFORM);
            current_score.set(current_parameters.getNumberOfFactors());
            accumulated_score.set(current_parameters.getNumberOfFactors());
            for (unsigned int i = 0; i < current_score.size(); ++i)
            {
                current_score[i].set(m_number_of_categories);
                accumulated_score[i].set(m_number_of_categories);
            }
            
            // For each level of the validation space....
            best_value = -1;
            selected_value = -1;
            for (unsigned int level = 0; level < validation_parameters.getNumberOfLevels(); ++level)
            {
                unsigned int current_number_of_train_samples;
                for (unsigned int i = 0; i < accumulated_score.size(); ++i)
                    accumulated_score[i].setValue(0);
                
                // ... validate the classifiers with the current level parameters ...
                for (unsigned int fold = 0; fold < number_of_folds; ++fold)
                {
                    current_number_of_train_samples = 0;
                    for (unsigned int j = 0; j < number_of_folds; ++j)
                        if (fold != j)
                            current_number_of_train_samples += sample_folds[j].size();
                    current_train_samples.set(current_number_of_train_samples);
                    current_number_of_train_samples = 0;
                    for (unsigned int j = 0; j < number_of_folds; ++j)
                        if (fold != j)
                            for (unsigned int k = 0; k < sample_folds[j].size(); ++k, ++current_number_of_train_samples)
                                current_train_samples[current_number_of_train_samples] = sample_folds[j][k];
                    
                    validate(current_train_samples.getData(), current_train_samples.size(), sample_folds[fold].getData(), sample_folds[fold].size(), current_parameters, base_parameters, current_score, number_of_threads, logger);
                    for (unsigned int i = 0; i < accumulated_score.size(); ++i)
                        accumulated_score[i] += current_score[i];
                }
                
                // ... and select the best parameters.
                for (unsigned int i = 0; i < current_parameters.getNumberOfFactors(); ++i)
                {
                    mean_score = 0;
                    for (unsigned int j = 0; j < m_number_of_categories; ++j)
                        mean_score += accumulated_score[i][j];
                    mean_score /= (double)m_number_of_categories;
                    
                    if (mean_score > best_value)
                    {
                        best_value = mean_score;
                        selected_value = i;
                        update_parameters = true;
                    }
                }
                
                if (update_parameters)
                {
                    // Set the currently selected parameters as the base parameters of the classifiers.
                    for (unsigned int i = 0; i < m_number_of_categories; ++i)
                    {
                        m_base_parameters[i].setRegularizationFactor(current_parameters.getRegularizationFactor(selected_value));
                        m_base_parameters[i].setBalancingFactor(current_parameters.getBalancingFactor(selected_value));
                    }
                    // Finally, calculate the parameter space for the next level of the pyramid.
                    current_parameters.setNextLevel(current_parameters.getRegularizationFactor(selected_value), current_parameters.getBalancingFactor(selected_value));
                }
                else current_parameters.setNextLevel(m_base_parameters[0].getRegularizationFactor(), m_base_parameters[0].getBalancingFactor());
            }
        }
        else // Classifiers which may have different base parameters for each category classifier.
        {
            VectorDense<VectorDense<double> > current_score(validation_parameters.getNumberOfFactors()), accumulated_score(validation_parameters.getNumberOfFactors());
            VectorDense<ClassifierValidationParameters<TCLASSIFIER> > current_parameters(m_number_of_categories, validation_parameters);
            VectorDense<unsigned int> selected_values(m_number_of_categories, -1);
            VectorDense<double> best_scores(m_number_of_categories, -1);
            VectorDense<bool> update_parameters(m_number_of_categories);
            unsigned int score_size;
            bool resize_score;
            
            // For each level of the validation space....
            for (unsigned int i = 0; i < current_score.size(); ++i)
            {
                current_score[i].set(m_number_of_categories);
                accumulated_score[i].set(m_number_of_categories);
            }
            score_size = current_score.size();
            resize_score = false;
            for (unsigned int level = 0; level < validation_parameters.getNumberOfLevels(); ++level)
            {
                unsigned int current_number_of_train_samples;
                for (unsigned int i = 0; i < accumulated_score.size(); ++i)
                    accumulated_score[i].setValue(0);
                
                // ... validate the classifiers with the current level parameters ...
                for (unsigned int fold = 0; fold < number_of_folds; ++fold)
                {
                    current_number_of_train_samples = 0;
                    for (unsigned int j = 0; j < number_of_folds; ++j)
                        if (fold != j)
                            current_number_of_train_samples += sample_folds[j].size();
                    current_train_samples.set(current_number_of_train_samples);
                    current_number_of_train_samples = 0;
                    for (unsigned int j = 0; j < number_of_folds; ++j)
                        if (fold != j)
                            for (unsigned int k = 0; k < sample_folds[j].size(); ++k, ++current_number_of_train_samples)
                                current_train_samples[current_number_of_train_samples] = sample_folds[j][k];
                    
                    validate(current_train_samples.getData(), current_train_samples.size(), sample_folds[fold].getData(), sample_folds[fold].size(), current_parameters.getData(), base_parameters, current_score, number_of_threads, logger);
                    for (unsigned int i = 0; i < accumulated_score.size(); ++i)
                        accumulated_score[i] += current_score[i];
                }
                
                // ... and select the best parameters.
                update_parameters.setValue(false);
                for (unsigned int i = 0; i < validation_parameters.getNumberOfFactors(); ++i)
                {
                    for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    {
                        if (accumulated_score[i][j] > best_scores[j])
                        {
                            best_scores[j] = accumulated_score[i][j];
                            selected_values[j] = i % current_parameters[j].getNumberOfFactors();
                            update_parameters[j] = true;
                        }
                    }
                }
                
                // Set the currently selected parameters as the base parameters of the classifiers and
                // calculate the parameters for the next precision level.
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    if (update_parameters[i])
                    {
                        m_base_parameters[i].setRegularizationFactor(current_parameters[i].getRegularizationFactor(selected_values[i]));
                        m_base_parameters[i].setBalancingFactor(current_parameters[i].getBalancingFactor(selected_values[i]));
                        current_parameters[i].setNextLevel(current_parameters[i].getRegularizationFactor(selected_values[i]), current_parameters[i].getBalancingFactor(selected_values[i]));
                    }
                    else current_parameters[i].setNextLevel(m_base_parameters[i].getRegularizationFactor(), m_base_parameters[i].getBalancingFactor());
                    if (current_parameters[i].getNumberOfFactors() > score_size)
                    {
                        score_size = current_parameters[i].getNumberOfFactors();
                        resize_score = true;
                    }
                }
                if (resize_score)
                {
                    current_score.set(score_size);
                    accumulated_score.set(score_size);
                    for (unsigned int i = 0; i < score_size; ++i)
                    {
                        current_score[i].set(m_number_of_categories);
                        accumulated_score[i].set(m_number_of_categories);
                    }
                    resize_score = false;
                }
            }
        }
        
        if (!disable_final_classifier)
        {
            // Re-train all classifiers with the best parameter selection.
            train(train_samples, number_of_train_samples, m_base_parameters, number_of_threads, logger);
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ PREDICTION FUNCTIONS ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(const VECTOR<TVECTOR, NVECTOR> &sample, VectorDense<TCLASSIFIER> &output) const
    {
        output.set(m_number_of_categories);
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
            output[i] = classifierDot(m_classifiers[i], sample);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(const VECTOR<TVECTOR, NVECTOR> * sample, VectorDense<TCLASSIFIER> * output, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
            {
                output[i].set(m_number_of_categories);
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    output[i][j] = classifierDot(m_classifiers[j], sample[i]);
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(VECTOR<TVECTOR, NVECTOR> const * const * sample, VectorDense<TCLASSIFIER> * output, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
            {
                output[i].set(m_number_of_categories);
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    output[i][j] = classifierDot(m_classifiers[j], *(sample[i]));
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &sample, VectorDense<TCLASSIFIER> &output) const
    {
        output.set(m_number_of_categories);
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
            output[i] = classifierDot(m_classifiers[i], sample.getData());
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * sample, VectorDense<TCLASSIFIER> * output, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
            {
                output[i].set(m_number_of_categories);
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    output[i][j] = classifierDot(m_classifiers[j], sample[i].getData());
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::predict(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * sample, VectorDense<TCLASSIFIER> * output, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned long i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
            {
                output[i].set(m_number_of_categories);
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    output[i][j] = classifierDot(m_classifiers[j], sample[i]->getData());
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ EVALUATION FUNCTIONS ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculatePositiveNegative(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int> &positive, VectorDense<unsigned int> &negative) const
    {
        positive.set(m_number_of_categories, 0);
        negative.set(m_number_of_categories, 0);
        for (unsigned int i = 0; i < number_of_samples; ++i)
        {
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                TLABEL probability;
                
                probability = (TLABEL)samples[i]->getLabel(m_categories_identifiers[c]);
                if (m_ignore_difficult)
                {
                    if (probability >= m_difficult_probability) ++positive[c];
                    else if (probability < m_positive_probability) ++negative[c];
                }
                else
                {
                    if (probability >= m_positive_probability) ++positive[c];
                    else ++negative[c];
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculatePositiveNegative(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<unsigned int> &positive, VectorDense<unsigned int> &negative) const
    {
        positive.set(m_number_of_categories, 0);
        negative.set(m_number_of_categories, 0);
        for (unsigned int i = 0; i < number_of_samples; ++i)
        {
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                TLABEL probability;
                
                probability = (TLABEL)samples[i].getLabel(m_categories_identifiers[c]);
                if (m_ignore_difficult)
                {
                    if (probability >= m_difficult_probability) ++positive[c];
                    else if (probability < m_positive_probability) ++negative[c];
                }
                else
                {
                    if (probability >= m_positive_probability) ++positive[c];
                    else ++negative[c];
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculateMeanAveragePrecision(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<double> &mean_average_precision, unsigned int number_of_threads) const
    {
        Tuple<TCLASSIFIER, float> * * scores;
        
        mean_average_precision.set(m_number_of_categories, 0);
        scores = new Tuple<TCLASSIFIER, float> * [m_number_of_categories];
        for (unsigned int c = 0; c < m_number_of_categories; ++c)
            scores[c] = new Tuple<TCLASSIFIER, float>[number_of_samples];
        
        // The probability that a sample belong to a certain category is stored inverted in the scores array, so that,
        // the positive samples are penalized for a set of samples with the same score.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = 0, index = omp_get_thread_num(); c < m_number_of_categories; ++c)
                for (unsigned int i = 0; i < number_of_samples; ++i, ++index)
                    if (index % number_of_threads == 0)
                        scores[c][i].setData(classifierDot(m_classifiers[c], samples[i]->getData()), -(float)samples[i]->getLabel(m_categories_identifiers[c]));
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = omp_get_thread_num(); c < m_number_of_categories; c += number_of_threads)
            {
                unsigned int correct;
                double current_map;
                
                std::sort(&scores[c][0], &scores[c][number_of_samples]);
                correct = 0;
                current_map = 0;
                if (m_ignore_difficult)
                {
                    for (unsigned int i = number_of_samples - 1, j = 0, k = 0; k < number_of_samples; --i, ++k)
                    {
                        const TLABEL category_probability = (TLABEL)(-scores[c][i].getSecond());
                        
                        if (category_probability >= m_difficult_probability)
                        {
                            current_map += (double)(correct + 1) / (double)(j + 1);
                            ++correct;
                            ++j;
                        }
                        else if (category_probability < m_positive_probability) ++j;
                    }
                }
                else
                {
                    for (unsigned int i = number_of_samples - 1, j = 0; j < number_of_samples; ++j, --i)
                    {
                        const TLABEL category_probability = (TLABEL)(-scores[c][i].getSecond());
                        
                        if (category_probability >= m_positive_probability)
                        {
                            current_map += (double)(correct + 1) / (double)(j + 1);
                            ++correct;
                        }
                    }
                }
                mean_average_precision[c] = current_map / (double)correct;
            }
        }
        
        for (unsigned int c = 0; c < m_number_of_categories; ++c) delete [] scores[c];
        delete [] scores;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculateMeanAveragePrecision(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<double> &mean_average_precision, unsigned int number_of_threads) const
    {
        Tuple<TCLASSIFIER, float> * * scores;
        
        mean_average_precision.set(m_number_of_categories, 0);
        scores = new Tuple<TCLASSIFIER, float> * [m_number_of_categories];
        for (unsigned int c = 0; c < m_number_of_categories; ++c)
            scores[c] = new Tuple<TCLASSIFIER, float>[number_of_samples];
        
        // The probability that a sample belong to a certain category is stored inverted in the scores array, so that,
        // the positive samples are penalized for a set of samples with the same score.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = 0, index = omp_get_thread_num(); c < m_number_of_categories; ++c)
                for (unsigned int i = 0; i < number_of_samples; ++i, ++index)
                    if (index % number_of_threads == 0)
                        scores[c][i].setData(classifierDot(m_classifiers[c], samples[i].getData()), -(float)samples[i].getLabel(m_categories_identifiers[c]));
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = omp_get_thread_num(); c < m_number_of_categories; c += number_of_threads)
            {
                unsigned int correct;
                double current_map;
                
                std::sort(&scores[c][0], &scores[c][number_of_samples]);
                correct = 0;
                current_map = 0;
                if (m_ignore_difficult)
                {
                    for (unsigned int i = number_of_samples - 1, j = 0, k = 0; k < number_of_samples; --i, ++k)
                    {
                        const TLABEL category_probability = (TLABEL)(-scores[c][i].getSecond());
                        
                        if (category_probability >= m_difficult_probability)
                        {
                            current_map += (double)(correct + 1) / (double)(j + 1);
                            ++correct;
                            ++j;
                        }
                        else if (category_probability < m_positive_probability) ++j;
                    }
                }
                else
                {
                    for (unsigned int i = number_of_samples - 1, j = 0; j < number_of_samples; ++j, --i)
                    {
                        const TLABEL category_probability = (TLABEL)(-scores[c][i].getSecond());
                        
                        if (category_probability >= m_positive_probability)
                        {
                            current_map += (double)(correct + 1) / (double)(j + 1);
                            ++correct;
                        }
                    }
                }
                mean_average_precision[c] = current_map / (double)correct;
            }
        }
        
        for (unsigned int c = 0; c < m_number_of_categories; ++c) delete [] scores[c];
        delete [] scores;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculateAccuracyGeometricMean(Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> const * const * samples, unsigned int number_of_samples, VectorDense<double> &accuracy_geometric_mean, unsigned int number_of_threads) const
    {
        unsigned int * * correct_positive, * * correct_negative, * * total_positive, * * total_negative;
        
        correct_positive = new unsigned int * [number_of_threads];
        correct_negative = new unsigned int * [number_of_threads];
        total_positive = new unsigned int * [number_of_threads];
        total_negative = new unsigned int * [number_of_threads];
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            correct_positive[t] = new unsigned int[m_number_of_categories];
            correct_negative[t] = new unsigned int[m_number_of_categories];
            total_positive[t] = new unsigned int[m_number_of_categories];
            total_negative[t] = new unsigned int[m_number_of_categories];
            
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                correct_positive[t][c] = 0;
                correct_negative[t][c] = 0;
                total_positive[t][c] = 0;
                total_negative[t][c] = 0;
            }
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int c = 0, index = thread_id; c < m_number_of_categories; ++c)
            {
                for (unsigned int i = 0; i < number_of_samples; ++i, ++index)
                {
                    TLABEL probability;
                    
                    probability = samples[i]->getLabel(m_categories_identifiers[c]);
                    if (m_ignore_difficult)
                    {
                        if (probability >= m_difficult_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i]->getData()) > 0)
                                ++correct_positive[thread_id][c];
                            ++total_positive[thread_id][c];
                        }
                        else if (probability < m_positive_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i]->getData()) <= 0)
                                ++correct_negative[thread_id][c];
                            ++total_negative[thread_id][c];
                        }
                    }
                    else
                    {
                        if (probability >= m_positive_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i]->getData()) > 0)
                                ++correct_positive[thread_id][c];
                            ++total_positive[thread_id][c];
                        }
                        else
                        {
                            if (classifierDot(m_classifiers[c], samples[i]->getData()) <= 0)
                                ++correct_negative[thread_id][c];
                            ++total_negative[thread_id][c];
                        }
                    }
                }
            }
        }
        accuracy_geometric_mean.set(m_number_of_categories, 0);
        for (unsigned int t = 1; t < number_of_threads; ++t)
        {
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                correct_positive[0][c] += correct_positive[t][c];
                correct_negative[0][c] += correct_negative[t][c];
                total_positive[0][c] += total_positive[t][c];
                total_negative[0][c] += total_negative[t][c];
            }
        }
        for (unsigned int c = 0; c < m_number_of_categories; ++c)
        {
            if ((total_positive[0][c] > 0) && (total_negative[0][c] > 0))
            {
                double positive_accuracy, negative_accuracy;
                
                positive_accuracy = (double)correct_positive[0][c] / (double)total_positive[0][c];
                negative_accuracy = (double)correct_negative[0][c] / (double)total_negative[0][c];
                accuracy_geometric_mean[c] = sqrt(positive_accuracy * negative_accuracy);
            }
            else accuracy_geometric_mean[c] = 0;
        }
        
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            delete [] correct_positive[t];
            delete [] correct_negative[t];
            delete [] total_positive[t];
            delete [] total_negative[t];
        }
        delete [] correct_positive;
        delete [] correct_negative;
        delete [] total_positive;
        delete [] total_negative;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    void LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::calculateAccuracyGeometricMean(const Sample<TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> * samples, unsigned int number_of_samples, VectorDense<double> &accuracy_geometric_mean, unsigned int number_of_threads) const
    {
        unsigned int * * correct_positive, * * correct_negative, * * total_positive, * * total_negative;
        
        correct_positive = new unsigned int * [number_of_threads];
        correct_negative = new unsigned int * [number_of_threads];
        total_positive = new unsigned int * [number_of_threads];
        total_negative = new unsigned int * [number_of_threads];
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            correct_positive[t] = new unsigned int[m_number_of_categories];
            correct_negative[t] = new unsigned int[m_number_of_categories];
            total_positive[t] = new unsigned int[m_number_of_categories];
            total_negative[t] = new unsigned int[m_number_of_categories];
            
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                correct_positive[t][c] = 0;
                correct_negative[t][c] = 0;
                total_positive[t][c] = 0;
                total_negative[t][c] = 0;
            }
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int c = 0, index = thread_id; c < m_number_of_categories; ++c)
            {
                for (unsigned int i = 0; i < number_of_samples; ++i, ++index)
                {
                    TLABEL probability;
                    
                    probability = samples[i].getLabel(m_categories_identifiers[c]);
                    if (m_ignore_difficult)
                    {
                        if (probability >= m_difficult_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i].getData()) > 0)
                                ++correct_positive[thread_id][c];
                            ++total_positive[thread_id][c];
                        }
                        else if (probability < m_positive_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i].getData()) <= 0)
                                ++correct_negative[thread_id][c];
                            ++total_negative[thread_id][c];
                        }
                    }
                    else
                    {
                        if (probability >= m_positive_probability)
                        {
                            if (classifierDot(m_classifiers[c], samples[i].getData()) > 0)
                                ++correct_positive[thread_id][c];
                            ++total_positive[thread_id][c];
                        }
                        else
                        {
                            if (classifierDot(m_classifiers[c], samples[i].getData()) <= 0)
                                ++correct_negative[thread_id][c];
                            ++total_negative[thread_id][c];
                        }
                    }
                }
            }
        }
        accuracy_geometric_mean.set(m_number_of_categories, 0);
        for (unsigned int t = 1; t < number_of_threads; ++t)
        {
            for (unsigned int c = 0; c < m_number_of_categories; ++c)
            {
                correct_positive[0][c] += correct_positive[t][c];
                correct_negative[0][c] += correct_negative[t][c];
                total_positive[0][c] += total_positive[t][c];
                total_negative[0][c] += total_negative[t][c];
            }
        }
        for (unsigned int c = 0; c < m_number_of_categories; ++c)
        {
            if ((total_positive[0][c] > 0) && (total_negative[0][c] > 0))
            {
                double positive_accuracy, negative_accuracy;
                
                // MODIFIED: G-Mean: sqrt((TP / (TP + FN)) * (TN / (TN + FP)))
                positive_accuracy = (double)correct_positive[0][c] / (double)total_positive[0][c];
                negative_accuracy = (double)correct_negative[0][c] / (double)total_negative[0][c];
                accuracy_geometric_mean[c] = sqrt(positive_accuracy * negative_accuracy);
            }
            else accuracy_geometric_mean[c] = 0;
        }
        
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            delete [] correct_positive[t];
            delete [] correct_negative[t];
            delete [] total_positive[t];
            delete [] total_negative[t];
        }
        delete [] correct_positive;
        delete [] correct_negative;
        delete [] total_positive;
        delete [] total_negative;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | LINEAR LEARNER FACTORY               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    struct LinearLearnerFactory { typedef Factory<LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> , LINEAR_LEARNER_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | QUANTIZED LINEAR CLASSIFIER          |
    //                   | *** DECLARATION ***                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class quantizes the weights of the linear classifiers and stores them using another data type.
     *  \tparam TQUANTIZED type used by the quantized classifier weight vectors.
     */
    template <class TQUANTIZED>
    class LinearQuantized
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LinearQuantized(void);
        /** Constructor which initializes the quantized classifier from an original linear classifier.
         *  \param[in] original_learner linear classifier which is quantized.
         *  \param[in] number_of_quantization_bins number of bins used by the quantized classifier.
         *  \param[in] absolute_maximum boolean flag which enables or disables the use of absolute value in order to define the quantization range.
         */
        template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
        LinearQuantized(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * original_learner, unsigned int number_of_quantization_bins, bool absolute_maximum);
        /** Constructor which initializes the quantized classifier from the given linear classifier information.
         *  \param[in] number_of_dimensions dimensionality of the classifier.
         *  \param[in] classifier_weights array to the weight vectors of the classifier.
         *  \param[in] classifier_identifiers array with the label identifier of each classifier.
         *  \param[in] number_of_categories number of linear classifiers.
         *  \param[in] number_of_quantization_bins number of bins used by the quantized classifier.
         *  \param[in] absolute_maximum boolean flag which enables or disables the use of absolute value in order to define the quantization range.
         */
        template <class TCLASSIFIER, class NCLASSIFIER, class NLABEL>
        LinearQuantized(unsigned int number_of_dimensions, const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, const NLABEL * classifier_identifiers, unsigned int number_of_categories, unsigned int number_of_quantization_bins, bool absolute_maximum);
        /** Constructor which initializes the quantized classifier structures.
         *  \param[in] number_of_dimensions dimensionality of the classifier.
         *  \param[in] classifier_identifiers array with the label identifier of each classifier.
         *  \param[in] number_of_categories number of linear classifiers.
         */
        template <class NLABEL>
        LinearQuantized(unsigned int number_of_dimensions, const NLABEL * classifier_identifiers, unsigned int number_of_categories);
        /// Copy constructor.
        LinearQuantized(const LinearQuantized<TQUANTIZED> &other);
        /// Copy constructor from a quantized linear classifier from another data type.
        template <class TQUANTIZED_OTHER>
        LinearQuantized(const LinearQuantized<TQUANTIZED_OTHER> &other);
        /// Destructor.
        ~LinearQuantized(void);
        /// Assignation operator.
        LinearQuantized<TQUANTIZED>& operator=(const LinearQuantized<TQUANTIZED> &other);
        /// Assignation operator from a quantized linear classifier from another data type.
        template <class TQUANTIZED_OTHER>
        LinearQuantized<TQUANTIZED>& operator=(const LinearQuantized<TQUANTIZED_OTHER> &other);
        /** Function which sets the quantized classifier values from the given linear classifier object.
         *  \param[in] original_learner linear classifier which is quantized.
         *  \param[in] number_of_quantization_bins number of bins used by the quantized classifier.
         *  \param[in] absolute_maximum boolean flag which enables or disables the use of absolute value in order to define the quantization range.
         */
        template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
        void set(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * original_learner, unsigned int number_of_quantization_bins, bool absolute_maximum);
        /** Function which sets the quantized classifier values from the given linear classifier structures.
         *  \param[in] number_of_dimensions dimensionality of the classifier.
         *  \param[in] classifier_weights array to the weight vectors of the classifier.
         *  \param[in] classifier_identifiers array with the label identifier of each classifier.
         *  \param[in] number_of_categories number of linear classifiers.
         *  \param[in] number_of_quantization_bins number of bins used by the quantized classifier.
         *  \param[in] absolute_maximum boolean flag which enables or disables the use of absolute value in order to define the quantization range.
         */
        template <class TCLASSIFIER, class NCLASSIFIER, class NLABEL>
        void set(unsigned int number_of_dimensions, const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, const NLABEL * classifier_identifiers, unsigned int number_of_categories, unsigned int number_of_quantization_bins, bool absolute_maximum);
        /** Function which sets the structures of the quantized linear classifier.
         *  \param[in] number_of_dimensions dimensionality of the classifier.
         *  \param[in] classifier_identifiers array with the label identifier of each classifier.
         *  \param[in] number_of_categories number of linear classifiers.
         */
        template <class NLABEL>
        void set(unsigned int number_of_dimensions, const NLABEL * classifier_identifiers, unsigned int number_of_categories);
        /** Function which quantizes the given linear classifier structures. The quantized classifier must be previously initialized.
         *  \param[in] classifier_weights array to the weight vectors of the classifier.
         *  \param[in] number_of_quantization_bins number of bins used by the quantized classifier.
         *  \param[in] absolute_maximum boolean flag which enables or disables the use of absolute value in order to define the quantization range.
         */
        template <class TCLASSIFIER, class NCLASSIFIER>
        void quantize(const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, unsigned int number_of_quantization_bins, bool absolute_maximum);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the dimensionality of the vectors categorized by the linear classifiers (i.e.\ the dimensionality of the weight vectors of the linear classifiers).
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns the number of different categories of the classifier (i.e.\ the number of linear classifiers).
        inline unsigned int getNumberOfCategories(void) const { return m_number_of_categories; }
        /// Returns a constant pointer to the array containing the category identifier of each classifier.
        inline const int * getCategoriesIdentifiers(void) const { return m_categories_identifiers; }
        /// Returns the category identifier of the index-th linear classifier.
        inline int getCategoryIdentifier(unsigned int index) const { return m_categories_identifiers[index]; }
        /// Returns a constant pointer to the array with the quantized linear classifiers.
        inline const VectorDense<TQUANTIZED> * getClassifiers(void) const { return m_classifiers; }
        /// Returns a constant reference to the quantized weights of the index-th linear classifier.
        inline const VectorDense<TQUANTIZED>& getClassifier(unsigned int index) const { return m_classifiers[index]; }
        /// Returns a constant pointer to the array with the minimum value of each classifier.
        inline const double * getMinimumValues(void) const { return m_minimum_values; }
        /// Returns the minimum value of the index-th classifier weight vector.
        inline double getMinimumValue(unsigned int index) const { return m_minimum_values[index]; }
        /// Returns a constant pointer to the array with the maximum value of each classifier.
        inline const double * getMaximumValues(void) const { return m_maximum_values; }
        /// Returns the maximum value of the index-th classifier weight vector.
        inline double getMaximumValue(unsigned int index) const { return m_maximum_values[index]; }
        /// Returns the quantization error of the index-th classifier.
        inline double getQuantizationError(unsigned int index) const { return (m_maximum_values[index] - m_minimum_values[index]) / (double)m_number_of_quantization_bins; }
        /// Returns the number of quantization bins, i.e.\ the number of possible values of the quantized classifiers.
        inline unsigned int getNumberOfQuantizationBins(void) const { return m_number_of_quantization_bins; }
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Calculate the classification score of each linear classifier for the given sample vector.
         *  \param[in] sample vector with sample to classify.
         *  \param[out] scores dense vector with the classification score of each classifier.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(const VECTOR<TVECTOR, NVECTOR> &sample, VectorDense<double> &scores) const;
        /** Calculate the classification score of each linear classifier for a set of sample vectors.
         *  \param[in] samples array with the vectors to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(const VECTOR<TVECTOR, NVECTOR> * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        /** Calculate the classification score of each linear classifier for a set of sample vectors.
         *  \param[in] samples array of pointers to the vectors to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
        void predict(VECTOR<TVECTOR, NVECTOR> const * const * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        /** Calculate the classification score of each linear classifier for the given sample.
         *  \param[in] sample vector with sample to classify.
         *  \param[out] scores dense vector with the classification score of each classifier.
         */
        template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
        void predict(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &sample, VectorDense<double> &scores) const;
        /** Calculate the classification score of each linear classifier for a set of samples.
         *  \param[in] samples array with the samples to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
        void predict(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        /** Calculate the classification score of each linear classifier for a set of samples.
         *  \param[in] samples array of pointers to the samples to be classified.
         *  \param[out] scores array of dense vectors with the resulting classification scores for each classifier.
         *  \param[in] number_of_samples number of elements in both samples and scores arrays.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the classification scores of the given samples.
         */
        template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
        void predict(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the quantized linear classifiers information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the quantized linear classifiers information from an XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Number of dimensions of the samples categorized by the classifiers.
        unsigned int m_number_of_dimensions;
        /// Array with the identifiers of each classifier category.
        int * m_categories_identifiers;
        /// Array with the quantized weight vectors of the classifiers.
        VectorDense<TQUANTIZED> * m_classifiers;
        /// Array with the minimum value of each classifier weight vector.
        double * m_minimum_values;
        /// Array with the maximum value of each classifier weight vector.
        double * m_maximum_values;
        /// Number of quantization bins, i.e.\ the number of possible values of the quantized classifiers.
        unsigned int m_number_of_quantization_bins;
        /// Number of categories (i.e.\ the number of linear classifiers).
        unsigned int m_number_of_categories;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | QUANTIZED LINEAR CLASSIFIER          |
    //                   | *** IMPLEMENTATION ***               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TQUANTIZED>
    LinearQuantized<TQUANTIZED>::LinearQuantized(void) :
        m_number_of_dimensions(0),
        m_categories_identifiers(0),
        m_classifiers(0),
        m_minimum_values(0),
        m_maximum_values(0),
        m_number_of_quantization_bins(0),
        m_number_of_categories(0)
    {
    }
    
    template <class TQUANTIZED>
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearQuantized<TQUANTIZED>::LinearQuantized(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * original_learner, unsigned int number_of_quantization_bins, bool absolute_maximum) :
        m_number_of_dimensions(original_learner->getNumberOfDimensions()),
        m_categories_identifiers((original_learner->getNumberOfCategories() > 0)?new int[original_learner->getNumberOfCategories()]:0),
        m_classifiers((original_learner->getNumberOfCategories() > 0)?new VectorDense<TQUANTIZED>[original_learner->getNumberOfCategories()]:0),
        m_minimum_values((original_learner->getNumberOfCategories() > 0)?new double[original_learner->getNumberOfCategories()]:0),
        m_maximum_values((original_learner->getNumberOfCategories() > 0)?new double[original_learner->getNumberOfCategories()]:0),
        m_number_of_quantization_bins(number_of_quantization_bins),
        m_number_of_categories(original_learner->getNumberOfCategories())
    {
        for (unsigned int category_id = 0; category_id < original_learner->getNumberOfCategories(); ++category_id)
        {
            const VectorDense<TCLASSIFIER, NSAMPLE> * current_classifier = &original_learner->getClassifier(category_id);
            double minimum_value, maximum_value, current_value;
            
            m_categories_identifiers[category_id] = original_learner->getCategoryIdentifier(category_id);
            minimum_value = maximum_value = (double)current_classifier->getData(0);
            for (unsigned int d = 1; d < original_learner->getNumberOfDimensions(); ++d)
            {
                current_value = (double)current_classifier->getData(d);
                if (current_value > maximum_value) maximum_value = current_value;
                if (current_value < minimum_value) minimum_value = current_value;
            }
            if (absolute_maximum)
            {
                if (srvAbs<double>(maximum_value) > srvAbs<double>(minimum_value))
                {
                    maximum_value = srvAbs<double>(maximum_value);
                    minimum_value = -srvAbs<double>(maximum_value);
                }
                else
                {
                    maximum_value = srvAbs<double>(minimum_value);
                    minimum_value = -srvAbs<double>(minimum_value);
                }
            }
            m_minimum_values[category_id] = minimum_value;
            m_maximum_values[category_id] = maximum_value;
            m_classifiers[category_id].set(original_learner->getNumberOfDimensions());
            
            for (unsigned int d = 0; d < original_learner->getNumberOfDimensions(); ++d)
            {
                current_value = ((double)current_classifier->getData(d) - minimum_value) / (maximum_value - minimum_value);
                m_classifiers[category_id][d] = (TQUANTIZED)srvMax<double>(0.0, srvMin<double>((double)number_of_quantization_bins - 1.0, current_value * (double)number_of_quantization_bins));
            }
        }
    }
    
    template <class TQUANTIZED>
    template <class TCLASSIFIER, class NCLASSIFIER, class NLABEL>
    LinearQuantized<TQUANTIZED>::LinearQuantized(unsigned int number_of_dimensions, const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, const NLABEL * classifier_identifiers, unsigned int number_of_categories, unsigned int number_of_quantization_bins, bool absolute_maximum) :
        m_number_of_dimensions(number_of_dimensions),
        m_categories_identifiers((number_of_categories > 0)?new int[number_of_categories]:0),
        m_classifiers((number_of_categories > 0)?new VectorDense<TQUANTIZED>[number_of_categories]:0),
        m_minimum_values((number_of_categories > 0)?new double[number_of_categories]:0),
        m_maximum_values((number_of_categories > 0)?new double[number_of_categories]:0),
        m_number_of_quantization_bins(number_of_quantization_bins),
        m_number_of_categories(number_of_categories)
    {
        for (unsigned int category_id = 0; category_id < number_of_categories; ++category_id)
        {
            double minimum_value, maximum_value, current_value;
            
            m_categories_identifiers[category_id] = (int)classifier_identifiers[category_id];
            minimum_value = maximum_value = (double)classifier_weights[category_id][0];
            for (unsigned int d = 1; d < number_of_dimensions; ++d)
            {
                current_value = (double)classifier_weights[category_id][d];
                if (current_value > maximum_value) maximum_value = current_value;
                if (current_value < minimum_value) minimum_value = current_value;
            }
            if (absolute_maximum)
            {
                if (srvAbs<double>(maximum_value) > srvAbs<double>(minimum_value))
                {
                    maximum_value = srvAbs<double>(maximum_value);
                    minimum_value = -srvAbs<double>(maximum_value);
                }
                else
                {
                    maximum_value = srvAbs<double>(minimum_value);
                    minimum_value = -srvAbs<double>(minimum_value);
                }
            }
            m_minimum_values[category_id] = minimum_value;
            m_maximum_values[category_id] = maximum_value;
            m_classifiers[category_id].set(number_of_dimensions);
            
            for (unsigned int d = 0; d < number_of_dimensions; ++d)
            {
                current_value = ((double)classifier_weights[category_id][d] - minimum_value) / (maximum_value - minimum_value);
                m_classifiers[category_id][d] = (TQUANTIZED)srvMax<double>(0.0, srvMin<double>((double)number_of_quantization_bins - 1.0, current_value * (double)number_of_quantization_bins));
            }
        }
    }
    
    template <class TQUANTIZED>
    template <class NLABEL>
    LinearQuantized<TQUANTIZED>::LinearQuantized(unsigned int number_of_dimensions, const NLABEL * classifier_identifiers, unsigned int number_of_categories) :
        m_number_of_dimensions(number_of_dimensions),
        m_categories_identifiers((number_of_categories > 0)?new int[number_of_categories]:0),
        m_classifiers((number_of_categories > 0)?new VectorDense<TQUANTIZED>[number_of_categories]:0),
        m_minimum_values((number_of_categories > 0)?new double[number_of_categories]:0),
        m_maximum_values((number_of_categories > 0)?new double[number_of_categories]:0),
        m_number_of_quantization_bins(0),
        m_number_of_categories(number_of_categories)
    {
        for (unsigned int category_id = 0; category_id < number_of_categories; ++category_id)
        {
            m_categories_identifiers[category_id] = (int)classifier_identifiers[category_id];
            m_minimum_values[category_id] = 0;
            m_maximum_values[category_id] = 0;
            m_classifiers[category_id].set(number_of_dimensions, 0);
        }
    }
    
    template <class TQUANTIZED>
    LinearQuantized<TQUANTIZED>::LinearQuantized(const LinearQuantized<TQUANTIZED> &other) :
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_categories_identifiers((other.m_number_of_categories > 0)?new int[other.m_number_of_categories]:0),
        m_classifiers((other.m_number_of_categories > 0)?new VectorDense<TQUANTIZED>[other.m_number_of_categories]:0),
        m_minimum_values((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_maximum_values((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_number_of_quantization_bins(other.m_number_of_quantization_bins),
        m_number_of_categories(other.m_number_of_categories)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_categories_identifiers[i] = other.m_categories_identifiers[i];
            m_classifiers[i] = other.m_classifiers[i];
            m_minimum_values[i] = other.m_minimum_values[i];
            m_maximum_values[i] = other.m_maximum_values[i];
        }
    }
    
    template <class TQUANTIZED>
    template <class TQUANTIZED_OTHER>
    LinearQuantized<TQUANTIZED>::LinearQuantized(const LinearQuantized<TQUANTIZED_OTHER> &other) :
        m_number_of_dimensions(other.getNumberOfDimensions()),
        m_categories_identifiers((other.getNumberOfCategories() > 0)?new int[other.getNumberOfCategories()]:0),
        m_classifiers((other.getNumberOfCategories() > 0)?new VectorDense<TQUANTIZED>[other.getNumberOfCategories()]:0),
        m_minimum_values((other.getNumberOfCategories() > 0)?new double[other.getNumberOfCategories()]:0),
        m_maximum_values((other.getNumberOfCategories() > 0)?new double[other.getNumberOfCategories()]:0),
        m_number_of_quantization_bins(other.getNumberOfQuantizationBins()),
        m_number_of_categories(other.getNumberOfCategories())
    {
        for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
        {
            m_categories_identifiers[i] = other.getCategoryIdentifier(i);
            m_classifiers[i] = other.getClassifier(i);
            m_minimum_values[i] = other.getMinimumValue(i);
            m_maximum_values[i] = other.getMaximumValue(i);
        }
    }
    
    template <class TQUANTIZED>
    LinearQuantized<TQUANTIZED>::~LinearQuantized(void)
    {
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_minimum_values != 0) delete [] m_minimum_values;
        if (m_maximum_values != 0) delete [] m_maximum_values;
    }
    
    template <class TQUANTIZED>
    LinearQuantized<TQUANTIZED>& LinearQuantized<TQUANTIZED>::operator=(const LinearQuantized<TQUANTIZED> &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
            if (m_classifiers != 0) delete [] m_classifiers;
            if (m_minimum_values != 0) delete [] m_minimum_values;
            if (m_maximum_values != 0) delete [] m_maximum_values;
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            m_number_of_dimensions = other.m_number_of_dimensions;
            m_number_of_quantization_bins = other.m_number_of_quantization_bins;
            
            if (other.m_number_of_categories > 0)
            {
                m_categories_identifiers = new int[other.m_number_of_categories];
                m_classifiers = new VectorDense<TQUANTIZED>[other.m_number_of_categories];
                m_minimum_values = new double[other.m_number_of_categories];
                m_maximum_values = new double[other.m_number_of_categories];
                m_number_of_categories = other.m_number_of_categories;
                
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_categories_identifiers[i] = other.m_categories_identifiers[i];
                    m_classifiers[i] = other.m_classifiers[i];
                    m_minimum_values[i] = other.m_minimum_values[i];
                    m_maximum_values[i] = other.m_maximum_values[i];
                }
            }
            else
            {
                m_categories_identifiers = 0;
                m_classifiers = 0;
                m_minimum_values = 0;
                m_maximum_values = 0;
                m_number_of_categories = 0;
            }
        }
        
        return *this;
    }
    
    template <class TQUANTIZED>
    template <class TQUANTIZED_OTHER>
    LinearQuantized<TQUANTIZED>& LinearQuantized<TQUANTIZED>::operator=(const LinearQuantized<TQUANTIZED_OTHER> &other)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_minimum_values != 0) delete [] m_minimum_values;
        if (m_maximum_values != 0) delete [] m_maximum_values;
        
        // -[ Copy ]---------------------------------------------------------------------------------------------------------------------------------
        m_number_of_dimensions = other.getNumberOfDimensions();
        m_number_of_quantization_bins = other.getNumberOfQuantizationBins();
        if (other.getNumberOfCategories() > 0)
        {
            m_categories_identifiers = new int[other.getNumberOfCategories()];
            m_classifiers = new VectorDense<TQUANTIZED>[other.getNumberOfCategories()];
            m_minimum_values = new double[other.getNumberOfCategories()];
            m_maximum_values = new double[other.getNumberOfCategories()];
            m_number_of_categories = other.getNumberOfCategories();
            
            for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
            {
                m_categories_identifiers[i] = other.getCategoryIdentifier(i);
                m_classifiers[i] = other.getClassifier(i);
                m_minimum_values[i] = other.getMinimumValue(i);
                m_maximum_values[i] = other.getMaximumValue(i);
            }
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_minimum_values = 0;
            m_maximum_values = 0;
            m_number_of_categories = 0;
        }
        
        return *this;
    }
    
    template <class TQUANTIZED>
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearQuantized<TQUANTIZED>::set(const LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * original_learner, unsigned int number_of_quantization_bins, bool absolute_maximum)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_minimum_values != 0) delete [] m_minimum_values;
        if (m_maximum_values != 0) delete [] m_maximum_values;
        
        // -[ Create the quantized classifiers ]-----------------------------------------------------------------------------------------------------
        m_number_of_dimensions = original_learner->getNumberOfDimensions();
        m_number_of_quantization_bins = number_of_quantization_bins;
        m_number_of_categories = original_learner->getNumberOfCategories();
        if (original_learner->getNumberOfCategories() > 0)
        {
            m_categories_identifiers = new int[original_learner->getNumberOfCategories()];
            m_classifiers = new VectorDense<TQUANTIZED>[original_learner->getNumberOfCategories()];
            m_minimum_values = new double[original_learner->getNumberOfCategories()];
            m_maximum_values = new double[original_learner->getNumberOfCategories()];
            
            for (unsigned int category_id = 0; category_id < original_learner->getNumberOfCategories(); ++category_id)
            {
                const VectorDense<TCLASSIFIER, NSAMPLE> * current_classifier = &original_learner->getClassifier(category_id);
                double minimum_value, maximum_value, current_value;
                
                m_categories_identifiers[category_id] = original_learner->getCategoryIdentifier(category_id);
                minimum_value = maximum_value = (double)current_classifier->getData(0);
                for (unsigned int d = 1; d < original_learner->getNumberOfDimensions(); ++d)
                {
                    current_value = (double)current_classifier->getData(d);
                    if (current_value > maximum_value) maximum_value = current_value;
                    if (current_value < minimum_value) minimum_value = current_value;
                }
                if (absolute_maximum)
                {
                    if (srvAbs<double>(maximum_value) > srvAbs<double>(minimum_value))
                    {
                        maximum_value = srvAbs<double>(maximum_value);
                        minimum_value = -srvAbs<double>(maximum_value);
                    }
                    else
                    {
                        maximum_value = srvAbs<double>(minimum_value);
                        minimum_value = -srvAbs<double>(minimum_value);
                    }
                }
                m_minimum_values[category_id] = minimum_value;
                m_maximum_values[category_id] = maximum_value;
                m_classifiers[category_id].set(original_learner->getNumberOfDimensions());
                
                for (unsigned int d = 0; d < original_learner->getNumberOfDimensions(); ++d)
                {
                    current_value = ((double)current_classifier->getData(d) - minimum_value) / (maximum_value - minimum_value);
                    m_classifiers[category_id][d] = (TQUANTIZED)srvMax<double>(0.0, srvMin<double>((double)number_of_quantization_bins - 1.0, current_value * (double)number_of_quantization_bins));
                }
            }
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_minimum_values = 0;
            m_maximum_values = 0;
        }
    }
    
    template <class TQUANTIZED>
    template <class TCLASSIFIER, class NCLASSIFIER, class NLABEL>
    void LinearQuantized<TQUANTIZED>::set(unsigned int number_of_dimensions, const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, const NLABEL * classifier_identifiers, unsigned int number_of_categories, unsigned int number_of_quantization_bins, bool absolute_maximum)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_minimum_values != 0) delete [] m_minimum_values;
        if (m_maximum_values != 0) delete [] m_maximum_values;
        
        // -[ Create the quantized classifiers ]-----------------------------------------------------------------------------------------------------
        m_number_of_dimensions = number_of_dimensions;
        m_number_of_quantization_bins = number_of_quantization_bins;
        m_number_of_categories = number_of_categories;
        if (number_of_categories > 0)
        {
            m_categories_identifiers = new int[number_of_categories];
            m_classifiers = new VectorDense<TQUANTIZED>[number_of_categories];
            m_minimum_values = new double[number_of_categories];
            m_maximum_values = new double[number_of_categories];
            
            for (unsigned int category_id = 0; category_id < number_of_categories; ++category_id)
            {
                double minimum_value, maximum_value, current_value;
                
                m_categories_identifiers[category_id] = (int)classifier_identifiers[category_id];
                minimum_value = maximum_value = (double)classifier_weights[category_id][0];
                for (unsigned int d = 1; d < number_of_dimensions; ++d)
                {
                    current_value = (double)classifier_weights[category_id][d];
                    if (current_value > maximum_value) maximum_value = current_value;
                    if (current_value < minimum_value) minimum_value = current_value;
                }
                if (absolute_maximum)
                {
                    if (srvAbs<double>(maximum_value) > srvAbs<double>(minimum_value))
                    {
                        maximum_value = srvAbs<double>(maximum_value);
                        minimum_value = -srvAbs<double>(maximum_value);
                    }
                    else
                    {
                        maximum_value = srvAbs<double>(minimum_value);
                        minimum_value = -srvAbs<double>(minimum_value);
                    }
                }
                m_minimum_values[category_id] = minimum_value;
                m_maximum_values[category_id] = maximum_value;
                m_classifiers[category_id].set(number_of_dimensions);
                
                for (unsigned int d = 0; d < number_of_dimensions; ++d)
                {
                    current_value = ((double)classifier_weights[category_id][d] - minimum_value) / (maximum_value - minimum_value);
                    m_classifiers[category_id][d] = (TQUANTIZED)srvMax<double>(0.0, srvMin<double>((double)number_of_quantization_bins - 1.0, current_value * (double)number_of_quantization_bins));
                }
            }
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_minimum_values = 0;
            m_maximum_values = 0;
        }
    }
    
    template <class TQUANTIZED>
    template <class NLABEL>
    void LinearQuantized<TQUANTIZED>::set(unsigned int number_of_dimensions, const NLABEL * classifier_identifiers, unsigned int number_of_categories)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
        if (m_classifiers != 0) delete [] m_classifiers;
        if (m_minimum_values != 0) delete [] m_minimum_values;
        if (m_maximum_values != 0) delete [] m_maximum_values;
        
        // -[ Create the quantized classifiers ]-----------------------------------------------------------------------------------------------------
        m_number_of_dimensions = number_of_dimensions;
        m_number_of_quantization_bins = 0;
        m_number_of_categories = number_of_categories;
        if (number_of_categories > 0)
        {
            m_categories_identifiers = new int[number_of_categories];
            m_classifiers = new VectorDense<TQUANTIZED>[number_of_categories];
            m_minimum_values = new double[number_of_categories];
            m_maximum_values = new double[number_of_categories];
            
            for (unsigned int category_id = 0; category_id < number_of_categories; ++category_id)
            {
                m_categories_identifiers[category_id] = (int)classifier_identifiers[category_id];
                m_minimum_values[category_id] = 0;
                m_maximum_values[category_id] = 0;
                m_classifiers[category_id].set(number_of_dimensions, 0);
            }
        }
        else
        {
            m_categories_identifiers = 0;
            m_classifiers = 0;
            m_minimum_values = 0;
            m_maximum_values = 0;
        }
    }
    
    template <class TQUANTIZED>
    template <class TCLASSIFIER, class NCLASSIFIER>
    void LinearQuantized<TQUANTIZED>::quantize(const VectorDense<TCLASSIFIER, NCLASSIFIER> * classifier_weights, unsigned int number_of_quantization_bins, bool absolute_maximum)
    {
        m_number_of_quantization_bins = number_of_quantization_bins;
        for (unsigned int category_id = 0; category_id < m_number_of_categories; ++category_id)
        {
            double minimum_value, maximum_value, current_value;
            
            minimum_value = maximum_value = (double)classifier_weights[category_id][0];
            for (unsigned int d = 1; d < m_number_of_dimensions; ++d)
            {
                current_value = (double)classifier_weights[category_id][d];
                if (current_value > maximum_value) maximum_value = current_value;
                if (current_value < minimum_value) minimum_value = current_value;
            }
            if (absolute_maximum)
            {
                if (srvAbs<double>(maximum_value) > srvAbs<double>(minimum_value))
                {
                    maximum_value = srvAbs<double>(maximum_value);
                    minimum_value = -srvAbs<double>(maximum_value);
                }
                else
                {
                    maximum_value = srvAbs<double>(minimum_value);
                    minimum_value = -srvAbs<double>(minimum_value);
                }
            }
            m_minimum_values[category_id] = minimum_value;
            m_maximum_values[category_id] = maximum_value;
            
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                current_value = ((double)classifier_weights[category_id][d] - minimum_value) / (maximum_value - minimum_value);
                m_classifiers[category_id][d] = (TQUANTIZED)srvMax<double>(0.0, srvMin<double>((double)number_of_quantization_bins - 1.0, current_value * (double)number_of_quantization_bins));
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ PREDICTION FUNCTIONS ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TQUANTIZED>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearQuantized<TQUANTIZED>::predict(const VECTOR<TVECTOR, NVECTOR> &sample, VectorDense<double> &scores) const
    {
        scores.set(m_number_of_categories, 0.0);
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
        {
            const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
            double dot_product, sum;
            
            sum = dot_product = 0.0;
            VectorSum(sample, sum);
            VectorDot(m_classifiers[i], sample, dot_product);
            scores[i] = dot_product / constant + m_minimum_values[i] * sum;
        }
    }
    
    template <class TQUANTIZED>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearQuantized<TQUANTIZED>::predict(const VECTOR<TVECTOR, NVECTOR> * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int k = omp_get_thread_num(); k < number_of_samples; k += number_of_threads)
            {
                scores[k].set(m_number_of_categories, 0.0);
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
                    double dot_product, sum;
                    
                    VectorSum(samples[k], sum);
                    VectorDot(m_classifiers[i], samples[k], dot_product);
                    scores[k][i] = dot_product / constant + m_minimum_values[i] * sum;
                }
            }
        }
    }
    
    template <class TQUANTIZED>
    template <template <class, class> class VECTOR, class TVECTOR, class NVECTOR>
    void LinearQuantized<TQUANTIZED>::predict(VECTOR<TVECTOR, NVECTOR> const * const * samples, VectorDense<double> *scores, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int k = omp_get_thread_num(); k < number_of_samples; k += number_of_threads)
            {
                scores[k].set(m_number_of_categories, 0.0);
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
                    double dot_product, sum;
                    
                    VectorSum(*samples[k], sum);
                    VectorDot(m_classifiers[i], *samples[k], dot_product);
                    scores[k][i] = dot_product / constant + m_minimum_values[i] * sum;
                }
            }
        }
    }
    
    template <class TQUANTIZED>
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearQuantized<TQUANTIZED>::predict(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &sample, VectorDense<double> &scores) const
    {
        scores.set(m_number_of_categories, 0.0);
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
        {
            const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
            double dot_product, sum;
            
            sum = dot_product = 0.0;
            for (NSAMPLE d = 0; d < sample.getData().size(); ++d)
            {
                dot_product += (double)sample.getData().getValue(d) * (double)m_classifiers[i][sample.getData().getIndex(d)];
                sum += (double)sample.getData().getValue(d);
            }
            scores[i] = dot_product / constant + m_minimum_values[i] * sum;
        }
    }
    
    template <class TQUANTIZED>
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearQuantized<TQUANTIZED>::predict(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int k = omp_get_thread_num(); k < number_of_samples; k += number_of_threads)
            {
                scores[k].set(m_number_of_categories, 0.0);
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
                    double dot_product, sum;
                    
                    sum = dot_product = 0.0;
                    for (NSAMPLE d = 0; d < samples[k].getData().size(); ++d)
                    {
                        dot_product += (double)samples[k].getData().getValue(d) * (double)m_classifiers[i][samples[k].getData().getIndex(d)];
                        sum += (double)samples[k].getData().getValue(d);
                    }
                    scores[k][i] = dot_product / constant + m_minimum_values[i] * sum;
                }
            }
        }
    }
    
    template <class TQUANTIZED>
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearQuantized<TQUANTIZED>::predict(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, VectorDense<double> * scores, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int k = omp_get_thread_num(); k < number_of_samples; k += number_of_threads)
            {
                scores[k].set(m_number_of_categories, 0.0);
                for (unsigned int i = 0; i < m_number_of_categories; ++i)
                {
                    const double constant = (double)m_number_of_quantization_bins / (m_maximum_values[i] - m_minimum_values[i]);
                    double dot_product, sum;
                    
                    sum = dot_product = 0.0;
                    for (NSAMPLE d = 0; d < samples[k]->getData().size(); ++d)
                    {
                        dot_product += (double)samples[k]->getData().getValue(d) * (double)m_classifiers[i][samples[k]->getData().getIndex(d)];
                        sum += (double)samples[k]->getData().getValue(d);
                    }
                    scores[k][i] = dot_product / constant + m_minimum_values[i] * sum;
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML CONVERSION FUNCTIONS ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TQUANTIZED>
    void LinearQuantized<TQUANTIZED>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Quantized_Classifier_Information");
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Number_Of_Quantization_Bins", m_number_of_quantization_bins);
        parser.setAttribute("Number_Of_Categories", m_number_of_categories);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
        {
            parser.openTag("Quantized_Classifier");
            parser.setAttribute("ID", i);
            parser.setAttribute("Identifier", m_categories_identifiers[i]);
            parser.setAttribute("Minimum_Value", m_minimum_values[i]);
            parser.setAttribute("Maximum_Value", m_maximum_values[i]);
            parser.addChildren();
            saveVector(parser, "Weights", m_classifiers[i]);
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    template <class TQUANTIZED>
    void LinearQuantized<TQUANTIZED>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Quantized_Classifier_Information"))
        {
            //-[ Free ]------------------------------------------------------------------------------------------------------------------------------
            if (m_categories_identifiers != 0) delete [] m_categories_identifiers;
            if (m_classifiers != 0) delete [] m_classifiers;
            if (m_minimum_values != 0) delete [] m_minimum_values;
            if (m_maximum_values != 0) delete [] m_maximum_values;
            
            //-[ Retrieve information from the XML object ]------------------------------------------------------------------------------------------
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_number_of_quantization_bins = parser.getAttribute("Number_Of_Quantization_Bins");
            m_number_of_categories = parser.getAttribute("Number_Of_Categories");
            
            if (m_number_of_categories > 0)
            {
                m_categories_identifiers = new int[m_number_of_categories];
                m_classifiers = new VectorDense<TQUANTIZED>[m_number_of_categories];
                m_minimum_values = new double[m_number_of_categories];
                m_maximum_values = new double[m_number_of_categories];
            }
            else
            {
                m_categories_identifiers = 0;
                m_classifiers = 0;
                m_minimum_values = 0;
                m_maximum_values = 0;
            }
            
            while (!(parser.isTagIdentifier("Quantized_Classifier_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Quantized_Classifier"))
                {
                    unsigned int current_index = parser.getAttribute("ID");
                    m_categories_identifiers[current_index] = parser.getAttribute("Identifier");
                    m_minimum_values[current_index] = parser.getAttribute("Minimum_Value");
                    m_maximum_values[current_index] = parser.getAttribute("Maximum_Value");
                    
                    while (!(parser.isTagIdentifier("Quantized_Classifier") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Weights"))
                            loadVector(parser, "Weights", m_classifiers[current_index]);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

