// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_SIGMOID_PROBABILISTIC_SCORE_HEADER_FILE__
#define __SRV_SIGMOID_PROBABILISTIC_SCORE_HEADER_FILE__

#include "srv_probabilistic_score.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Uses a sigmoid function to approximate the probabilistic score of the classifiers. This class
     *  implements the method proposed by John C. Platt in 'Platt, J.C., "Probabilistic Outputs for
     *  Support Vector Machines and Comparisons to Regularized Likelihood Methods", Advances in Large
     *  Margin Classifiers, MIT Press 1999'.
     */
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticSigmoid : public ProbabilisticScoreBase<TSCORE, TPROBABILITY>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticSigmoid(void);
        /** Constructor which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        ProbabilisticSigmoid(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        /// Copy constructor.
        ProbabilisticSigmoid(const ProbabilisticSigmoid<TSCORE, TPROBABILITY> &other);
        /// Destructor.
        virtual ~ProbabilisticSigmoid(void);
        /// Assignation operator.
        ProbabilisticSigmoid<TSCORE, TPROBABILITY>& operator=(const ProbabilisticSigmoid<TSCORE, TPROBABILITY> &other);
        /** Function which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        void set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array with the scale factor of the sigmoid functions used to approximate the probabilistic score of each classifier.
        inline const double * getSigmoidScales(void) const { return m_sigmoid_scale; }
        /// Returns the scale factor of the sigmoid functions used to approximate the probabilistic score of index-th classifier.
        inline double getSigmoidScale(unsigned int index) const { return m_sigmoid_scale[index]; }
        /// Sets the scale factor of the sigmoid functions used to approximate the probabilistic score of index-th classifier.
        inline void setSigmoidScale(const double &sigmoid_scale, unsigned int index) const { m_sigmoid_scale[index] = sigmoid_scale; }
        /// Returns a constant pointer to the array with the offset value of the sigmoid functions used to approximate the probabilistic score of each classifier.
        inline const double * getSigmoidOffsets(void) const { return m_sigmoid_offset; }
        /// Returns the offset value of the sigmoid functions used to approximate the probabilistic score of index-th classifier.
        inline double getSigmoidOffset(unsigned int index) const { return m_sigmoid_offset[index]; }
        /// Sets the offset value of the sigmoid functions used to approximate the probabilistic score of index-th classifier.
        inline void setSigmoidOffset(const double &sigmoid_offset, unsigned int index) const { m_sigmoid_offset[index] = sigmoid_offset; }
        /// Returns the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline bool getBalance(void) const { return m_balance; }
        /// Sets the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline void setBalance(bool balance) { m_balance = balance; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] score a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        inline ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY> *)new ProbabilisticSigmoid(*this); }
        /// Returns the class identifier for the probabilistic score method.
        inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return SIGMOID_ALGORITHM; }
        /// Generates a new empty instance of the probabilistic score object.
        inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new ProbabilisticSigmoid(); }
        /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        inline PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const { return SIGMOID_ALGORITHM; }
        
    protected:
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        void attributesToXML(XmlParser &parser) const;
        /// Stores the probabilistic object information into the data of the XML object.
        void dataToXML(XmlParser &parser) const;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        void freeDataXML(void);
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        void attributesFromXML(XmlParser &parser);
        /// Retrieves the probabilistic object information into the data of the XML object.
        bool dataFromXML(XmlParser &parser);
        
        // -[ Other protected functions ]------------------------------------------------------------------------------------------------------------
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Array with the minimum scores of each classifier.
        double * m_sigmoid_scale;
        /// Array with the maximum scores of each classifier.
        double * m_sigmoid_offset;
        /// Boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        bool m_balance;
        
        /// Static integer which indicates if the class has been initialized in the probability score factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ACCESS FUNCTIONS ]============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticSigmoid<TSCORE, TPROBABILITY>::ProbabilisticSigmoid(void) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(),
        m_sigmoid_scale(0),
        m_sigmoid_offset(0),
        m_balance(false)
    {}
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticSigmoid<TSCORE, TPROBABILITY>::ProbabilisticSigmoid(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(number_of_categories, multiclass, probability_factor),
        m_sigmoid_scale((number_of_categories > 0)?new double[number_of_categories]:0),
        m_sigmoid_offset((number_of_categories > 0)?new double[number_of_categories]:0),
        m_balance(balance)
    {
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticSigmoid<TSCORE, TPROBABILITY>::ProbabilisticSigmoid(const ProbabilisticSigmoid<TSCORE, TPROBABILITY> &other) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(other),
        m_sigmoid_scale((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_sigmoid_offset((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_balance(other.m_balance)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_sigmoid_scale[i] = other.m_sigmoid_scale[i];
            m_sigmoid_offset[i] = other.m_sigmoid_offset[i];
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticSigmoid<TSCORE, TPROBABILITY>::~ProbabilisticSigmoid(void)
    {
        if (m_sigmoid_scale != 0) delete [] m_sigmoid_scale;
        if (m_sigmoid_offset != 0) delete [] m_sigmoid_offset;
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticSigmoid<TSCORE, TPROBABILITY>& ProbabilisticSigmoid<TSCORE, TPROBABILITY>::operator=(const ProbabilisticSigmoid<TSCORE, TPROBABILITY> &other)
    {
        if (this != &other)
        {
            if (m_sigmoid_scale != 0) delete [] m_sigmoid_scale;
            if (m_sigmoid_offset != 0) delete [] m_sigmoid_offset;
            
            ProbabilisticScoreBase<TSCORE, TPROBABILITY>::operator=(other);
            m_balance = other.m_balance;
            if (other.m_number_of_categories > 0)
            {
                m_sigmoid_scale = new double[other.m_number_of_categories];
                m_sigmoid_offset = new double[other.m_number_of_categories];
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_sigmoid_scale[i] = other.m_sigmoid_scale[i];
                    m_sigmoid_offset[i] = other.m_sigmoid_offset[i];
                }
            }
            else
            {
                m_sigmoid_scale = 0;
                m_sigmoid_offset = 0;
            }
        }
        
        return *this;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance)
    {
        if (m_sigmoid_scale != 0) delete [] m_sigmoid_scale;
        if (m_sigmoid_offset != 0) delete [] m_sigmoid_offset;
        
        this->setBase(number_of_categories, multiclass, probability_factor);
        m_sigmoid_scale = new double[number_of_categories];
        m_sigmoid_offset = new double[number_of_categories];
        m_balance = balance;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::attributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Balance", m_balance);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::attributesFromXML(XmlParser &parser)
    {
        m_balance = parser.getAttribute("Balance");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::dataToXML(XmlParser &parser) const
    {
        saveVector(parser, "Sigmoid_Scale", ConstantSubVectorDense<double, unsigned int>(m_sigmoid_scale, this->m_number_of_categories));
        saveVector(parser, "Sigmoid_Offset", ConstantSubVectorDense<double, unsigned int>(m_sigmoid_offset, this->m_number_of_categories));
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::freeDataXML(void)
    {
        if (m_sigmoid_scale != 0) { delete [] m_sigmoid_scale; m_sigmoid_scale = 0; }
        if (m_sigmoid_offset != 0) { delete [] m_sigmoid_offset; m_sigmoid_offset = 0; }
    }
    
    template <class TSCORE, class TPROBABILITY>
    bool ProbabilisticSigmoid<TSCORE, TPROBABILITY>::dataFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Sigmoid_Scale"))
        {
            VectorDense<double, unsigned int> data;
            
            loadVector(parser, "Sigmoid_Scale", data);
            m_sigmoid_scale = new double[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_sigmoid_scale[i] = data[i];
            
            return true;
        }
        else if (parser.isTagIdentifier("Sigmoid_Offset"))
        {
            VectorDense<double, unsigned int> data;
            
            loadVector(parser, "Sigmoid_Offset", data);
            m_sigmoid_offset = new double[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_sigmoid_offset[i] = data[i];
            
            return true;
        }
        else return false;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN AND RESCALE FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            Tuple<TSCORE, bool> * current_scores, * current_scores_copy;
            unsigned int current_number_of_samples;
            bool free_scores_copy;
            double * probability;
            
            current_scores = new Tuple<TSCORE, bool>[number_of_samples];
            
            for (unsigned int category = thread_id; category < this->m_number_of_categories; category += number_of_threads)
            {
                unsigned int prior1, prior0;
                double hiTarget, loTarget, lambda, olderr;
                
                if ((logger != 0) && (thread_id == 0))
                    logger->log("Calibrating the classifier %d of %d.", category + 1, this->m_number_of_categories);
                prior1 = prior0 = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    bool current_positive;
                    
                    current_positive = false;
                    for (unsigned int l = 0; (!current_positive) && (l < label[i].size()); ++l)
                        current_positive = (label[i][l] == category);
                    
                    current_scores[i].setData(score[i][category], current_positive);
                    if (current_positive) ++prior1;
                    else ++prior0;
                }
                
                // Balance the number of positive and negative samples when specified ...............................................................
                if (m_balance && (prior1 > prior0))
                {
                    TSCORE * auxiliary_scores;
                    
                    current_number_of_samples = 2 * prior1;
                    free_scores_copy = true;
                    current_scores_copy = new Tuple<TSCORE, bool>[current_number_of_samples];
                    auxiliary_scores = new TSCORE[prior0];
                    
                    prior0 = 0;
                    for (unsigned int i = 0; i < number_of_samples; ++i)
                    {
                        current_scores_copy[i] = current_scores[i];
                        if (!current_scores[i].getSecond())
                        {
                            auxiliary_scores[prior0] = current_scores[i].getFirst();
                            ++prior0;
                        }
                    }
                    for (unsigned int i = number_of_samples; i < current_number_of_samples; ++i)
                        current_scores_copy[i].setData(auxiliary_scores[i % prior0], false);
                    prior0 = prior1;
                    delete [] auxiliary_scores;
                }
                else if (m_balance && (prior1 < prior0))
                {
                    TSCORE * auxiliary_scores;
                    
                    current_number_of_samples = 2 * prior0;
                    free_scores_copy = true;
                    current_scores_copy = new Tuple<TSCORE, bool>[current_number_of_samples];
                    auxiliary_scores = new TSCORE[prior1];
                    
                    prior1 = 0;
                    for (unsigned int i = 0; i < number_of_samples; ++i)
                    {
                        current_scores_copy[i] = current_scores[i];
                        if (current_scores[i].getSecond())
                        {
                            auxiliary_scores[prior1] = current_scores[i].getFirst();
                            ++prior1;
                        }
                    }
                    for (unsigned int i = number_of_samples; i < current_number_of_samples; ++i)
                        current_scores_copy[i].setData(auxiliary_scores[i % prior1], true);
                    prior1 = prior0;
                    delete [] auxiliary_scores;
                }
                else
                {
                    current_number_of_samples = prior1 + prior0;
                    current_scores_copy = current_scores;
                    free_scores_copy = false;
                }
                
                // Continue with the Platt's algorithm ..............................................................................................
                probability = new double[current_number_of_samples];
                m_sigmoid_scale[category] = 0.0;
                m_sigmoid_offset[category] = log(((double)prior0 + 1.0) / ((double)prior1 + 1.0));
                hiTarget = (double)(prior1 + 1) / (double)(prior1 + 2.0);
                loTarget = 1.0 / (prior0 + 1);
                lambda = 1e-3;
                olderr = 1e300;
                
                for (unsigned int i = 0; i < current_number_of_samples; ++i)
                    probability[i] = (double)(prior1 + 1) / (double)(prior0 + prior1 + 2);
                for (unsigned int iteration = 0, count = 0; iteration < 100; ++iteration)
                {
                    double a, b, c, d, e, old_scale, old_offset, err, diff, scale;
                    
                    // 1) Compute the Hessian and the gradient of the error function with respect the
                    //    sigmoid scale and offset values.
                    a = b = c = d = e = 0.0;
                    for (unsigned int j = 0; j < current_number_of_samples; ++j)
                    {
                        double t, d1, d2;
                        
                        if (current_scores_copy[j].getSecond()) t = hiTarget;
                        else t = loTarget;
                        d1 = probability[j] - t;
                        d2 = probability[j] * (1.0 - probability[j]);
                        a += current_scores_copy[j].getFirst() * current_scores_copy[j].getFirst() * d2;
                        b += d2;
                        c += current_scores_copy[j].getFirst() * d2;
                        d += current_scores_copy[j].getFirst() * d1;
                        e += d1;
                    }
                    // 2) Stop when the gradient is too small.
                    if ((srvAbs<double>(d) < 1e-9) && (srvAbs<double>(e) < 1e-9))
                        break;
                    old_scale = m_sigmoid_scale[category];
                    old_offset = m_sigmoid_offset[category];
                    // Loop until until the model fits better the data.
                    while (true)
                    {
                        double det;
                        
                        det = (a + lambda) * (b + lambda) - c * c;
                        // If the determinant of the Hessian is zero, then increase the stabilizer.
                        if (det == 0)
                        {
                            lambda *= 10.0;
                            continue;
                        }
                        
                        m_sigmoid_scale[category] = old_scale + ((b + lambda) * d - c * e) / det;
                        m_sigmoid_offset[category] = old_offset + ((a + lambda) * e - c * d) / det;
                        
                        // Calculate how good is the approximation between the sigmoid function and the data.
                        err = 0;
                        for (unsigned int j = 0; j < current_number_of_samples; ++j)
                        {
                            double t, aux0, aux1;
                            probability[j] = 1.0 / (1.0 + std::exp(m_sigmoid_scale[category] * current_scores_copy[j].getFirst() + m_sigmoid_offset[category]));
                            
                            if (current_scores_copy[j].getSecond()) t = hiTarget;
                            else t = loTarget;
                            
                            aux0 = log(probability[j]);
                            aux1 = log(1 - probability[j]);
                            if (aux0 < -200) aux0 = -200;
                            if (aux1 < -200) aux1 = -200;
                            err -= t * aux0 + (1 - t) * aux1;
                        }
                        if (err < olderr * (1 + 1e-7))
                        {
                            lambda *= 0.1;
                            break;
                        }
                        // When the error does not decrease, then increase the stabilizer by a factor
                        // of 10 and try again.
                        lambda *= 10;
                        if (lambda >= 1e6) // The lambda is too large as data cannot be fitted, then stop.
                            break;
                    }
                    diff = err - olderr;
                    scale = 0.5 * (err + olderr + 1.0);
                    if ((diff > -1e-3 * scale) && (diff < 1e-7 * scale))
                        ++count;
                    else count = 0;
                    olderr = err;
                    if (count == 3)
                        break;
                }
                
                if (free_scores_copy) delete [] current_scores_copy;
                delete [] probability;
            }
            
            delete [] current_scores;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger)
    {
        const unsigned int number_of_bins = bin_scores.size();
        
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            double * probability;
            
            probability = new double[number_of_bins];
            for (unsigned int category = omp_get_thread_num(); category < this->m_number_of_categories; category += number_of_threads)
            {
                double hiTarget, loTarget, lambda, olderr, positive_factor, negative_factor;
                unsigned int prior1, prior0;
                
                prior0 = prior1 = 0;
                for (unsigned int bin = 0; bin < number_of_bins; ++bin)
                {
                    prior1 += positive_histogram[category][bin];
                    prior0 += negative_histogram[category][bin];
                }
                
                m_sigmoid_scale[category] = 0.0;
                m_sigmoid_offset[category] = log(((double)prior0 + 1.0) / ((double)prior1 + 1.0));
                hiTarget = (double)(prior1 + 1) / (double)(prior1 + 2.0);
                loTarget = 1.0 / (prior0 + 1);
                lambda = 1e-3;
                olderr = 1e300;
                for (unsigned int bin = 0; bin < number_of_bins; ++bin)
                    probability[bin] = (double)(prior1 + 1) / (double)(prior0 + prior1 + 2);
                if (this->m_balance)
                {
                    if (prior1 > prior0)
                    {
                        positive_factor = 1.0;
                        negative_factor = (double)prior1 / (double)prior0;
                    }
                    else
                    {
                        positive_factor = (double)prior0 / (double)prior1;
                        negative_factor = 1.0;
                    }
                }
                else positive_factor = negative_factor = 1.0;
                
                for (unsigned int iteration = 0, count = 0; iteration < 100; ++iteration)
                {
                    double a, b, c, d, e, old_scale, old_offset, err, diff, scale;
                    
                    // 1) Compute the Hessian and the gradient of the error function with respect the
                    //    sigmoid scale and offset values.
                    a = b = c = d = e = 0.0;
                    for (unsigned int bin = 0; bin < number_of_bins; ++bin)
                    {
                        const double weight_positive = positive_factor * (double)positive_histogram[category][bin];
                        const double weight_negative = negative_factor * (double)negative_histogram[category][bin];
                        const double cs = (double)bin_scores[bin];
                        double d1, d2;
                        
                        // Positive samples .......................................................
                        d1 = probability[bin] - hiTarget;
                        d2 = probability[bin] * (1.0 - probability[bin]);
                        a += weight_positive * cs * cs * d2;
                        b += weight_positive * d2;
                        c += weight_positive * cs * d2;
                        d += weight_positive * cs * d1;
                        e += weight_positive * d1;
                        
                        // Negative samples .......................................................
                        d1 = probability[bin] - loTarget;
                        d2 = probability[bin] * (1.0 - probability[bin]);
                        a += weight_negative * cs * cs * d2;
                        b += weight_negative * d2;
                        c += weight_negative * cs * d2;
                        d += weight_negative * cs * d1;
                        e += weight_negative * d1;
                    }
                    // 2) Stop when the gradient is too small.
                    if ((srvAbs<double>(d) < 1e-9) && (srvAbs<double>(e) < 1e-9))
                        break;
                    old_scale = m_sigmoid_scale[category];
                    old_offset = m_sigmoid_offset[category];
                    // Loop until until the model fits better the data.
                    while (true)
                    {
                        double det;
                        
                        det = (a + lambda) * (b + lambda) - c * c;
                        // If the determinant of the Hessian is zero, then increase the stabilizer.
                        if (det == 0)
                        {
                            lambda *= 10.0;
                            continue;
                        }
                        m_sigmoid_scale[category] = old_scale + ((b + lambda) * d - c * e) / det;
                        m_sigmoid_offset[category] = old_offset + ((a + lambda) * e - c * d) / det;
                        
                        // Calculate how good is the approximation between the sigmoid function and the data.
                        err = 0;
                        for (unsigned int bin = 0; bin < number_of_bins; ++bin)
                        {
                            const double weight_positive = positive_factor * (double)positive_histogram[category][bin];
                            const double weight_negative = negative_factor * (double)negative_histogram[category][bin];
                            double aux0, aux1;
                            
                            probability[bin] = 1.0 / (1.0 + std::exp(m_sigmoid_scale[category] * bin_scores[bin] + m_sigmoid_offset[category]));
                            aux0 = srvMax(-200.0, std::log(probability[bin]));
                            aux1 = srvMax(-200.0, std::log(1.0 - probability[bin]));
                            
                            err -= weight_positive * (hiTarget * aux0 + (1.0 - hiTarget) * aux1);
                            err -= weight_negative * (loTarget * aux0 + (1.0 - loTarget) * aux1);
                        }
                        if (err < olderr * (1 + 1e-7))
                        {
                            lambda *= 0.1;
                            break;
                        }
                        // When the error does not decrease, then increase the stabilizer by a factor
                        // of 10 and try again.
                        lambda *= 10;
                        if (lambda >= 1e6) // The lambda is too large as data cannot be fitted, then stop.
                            break;
                    }
                    diff = err - olderr;
                    scale = 0.5 * (err + olderr + 1.0);
                    if ((diff > -1e-3 * scale) && (diff < 1e-7 * scale)) ++count;
                    else count = 0;
                    olderr = err;
                    if (count == 3) break;
                }
            }
            delete [] probability;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
            {
                if ((logger != 0) && (thread_id == 0) && (i % (10 * number_of_threads) == 0))
                    logger->log("Processing the sample %d of %d.", i + 1, number_of_samples);
                probability[i].set(this->m_number_of_categories);
                
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                    probability[i][c] = (TPROBABILITY)(this->m_probability_factor * 1.0 / (1.0 + std::exp(m_sigmoid_scale[c] * score[i][c] + m_sigmoid_offset[c])));
            }
        }
        
        if (this->m_multiclass)
        {
            if (logger != 0) logger->log("Converting probabilities into multi-class estimates.");
            this->protectedMulticlass(probability, number_of_samples, number_of_threads);
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        if ((void*)&score == (void*)&probability)
        {
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        TPROBABILITY * __restrict__ score_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr)
                            *score_ptr = (TPROBABILITY)(this->m_probability_factor * 1.0 / (1.0 + std::exp(m_sigmoid_scale[c] * *score_ptr + m_sigmoid_offset[c])));
                    }
                }
            }
        }
        else
        {
            probability.setGeometry(score);
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        const TSCORE * __restrict__ score_ptr = score.get(y, c);
                        TPROBABILITY * __restrict__ prob_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr, ++prob_ptr)
                            *prob_ptr = (TPROBABILITY)(this->m_probability_factor * 1.0 / (1.0 + std::exp(m_sigmoid_scale[c] * *score_ptr + m_sigmoid_offset[c])));
                    }
                }
            }
        }
        
        if (this->m_multiclass) this->protectedMulticlass(probability, number_of_threads);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticSigmoid<TSCORE, TPROBABILITY>::scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const
    {
        probability = srvMin<double>(1.0 - 1e-12, srvMax<double>(1e-12, probability));
        score.set(this->m_number_of_categories);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < this->m_number_of_categories; i += number_of_threads)
                score[i] = (log((1.0 - probability) / probability) - m_sigmoid_offset[i]) / m_sigmoid_scale[i];
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

