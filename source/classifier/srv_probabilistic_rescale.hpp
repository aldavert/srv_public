// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_RESCALE_PROBABILISTIC_SCORE_HEADER_FILE__
#define __SRV_RESCALE_PROBABILISTIC_SCORE_HEADER_FILE__

#include "srv_probabilistic_score.hpp"
#include "../srv_matrix.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which converts the score obtained by a classifier into a probabilistic estimation.
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticRescale : public ProbabilisticScoreBase<TSCORE, TPROBABILITY>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticRescale(void);
        /** Constructor which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] window_size number of elements close to a given score used to calculate its probability.
         */
        ProbabilisticRescale(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int window_size);
        /// Copy constructor.
        ProbabilisticRescale(const ProbabilisticRescale<TSCORE, TPROBABILITY> &other);
        /// Destructor.
        virtual ~ProbabilisticRescale(void);
        /// Assignation operator.
        ProbabilisticRescale<TSCORE, TPROBABILITY>& operator=(const ProbabilisticRescale<TSCORE, TPROBABILITY> &other);
        /** Function which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] window_size number of elements close to an score used to calculate its probability.
         */
        void set(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int window_size);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the minimum scores of the classifiers.
        inline const TSCORE * getMinimumScore(void) const { return m_minimum_score; }
        /// Returns the minimum score of the index-th classifier.
        inline TSCORE getMinimumScore(unsigned int index) const { return m_minimum_score[index]; }
        /// Sets the minimum score of the index-th classifier.
        inline void setMinimumScore(const TSCORE &minimum_score, unsigned int index) const { m_minimum_score[index] = minimum_score; }
        /// Returns a constant pointer to the maximum scores of the classifiers.
        inline const TSCORE * getMaximumScore(void) const { return m_maximum_score; }
        /// Returns the maximum score of the index-th classifier.
        inline TSCORE getMaximumScore(unsigned int index) const { return m_maximum_score[index]; }
        /// Sets the maximum score of the index-th classifier.
        inline void setMaximumScore(const TSCORE &maximum_score, unsigned int index) const { m_maximum_score[index] = maximum_score; }
        
        /// Returns the number of elements close to an score used to calculate its probability.
        inline unsigned int getWindowSize(void) const { return m_window_size; }
        /// Sets the number of elements close to an score used to calculate its probability.
        inline void setWindowSize(unsigned int window_size) { m_window_size = window_size; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] scores a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        inline ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY> *)new ProbabilisticRescale(*this); }
        /// Returns the class identifier for the probabilistic score method.
        inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return RESCALING_ALGORITHM; }
        /// Generates a new empty instance of the probabilistic score object.
        inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new ProbabilisticRescale(); }
        /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        inline PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const { return RESCALING_ALGORITHM; }
        
    protected:
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        void attributesToXML(XmlParser &parser) const;
        /// Stores the probabilistic object information into the data of the XML object.
        void dataToXML(XmlParser &parser) const;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        void freeDataXML(void);
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        void attributesFromXML(XmlParser &parser);
        /// Retrieves the probabilistic object information into the data of the XML object.
        bool dataFromXML(XmlParser &parser);
        
        // -[ Other protected functions ]------------------------------------------------------------------------------------------------------------
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Array with the minimum scores of each classifier.
        TSCORE * m_minimum_score;
        /// Array with the maximum scores of each classifier.
        TSCORE * m_maximum_score;
        /// Number of elements close to an score used to calculate its probability.
        unsigned int m_window_size;
        
        /// Static integer which indicates if the class has been initialized in the probability score factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ACCESS FUNCTIONS ]============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticRescale<TSCORE, TPROBABILITY>::ProbabilisticRescale(void) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(),
        m_minimum_score(0),
        m_maximum_score(0),
        m_window_size(20) {}
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticRescale<TSCORE, TPROBABILITY>::ProbabilisticRescale(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int window_size) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(number_of_categories, multiclass, probability_factor),
        m_minimum_score((number_of_categories > 0)?new TSCORE[number_of_categories]:0),
        m_maximum_score((number_of_categories > 0)?new TSCORE[number_of_categories]:0),
        m_window_size(window_size)
    {
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticRescale<TSCORE, TPROBABILITY>::ProbabilisticRescale(const ProbabilisticRescale<TSCORE, TPROBABILITY> &other) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(other),
        m_minimum_score((other.m_number_of_categories > 0)?new TSCORE[other.m_number_of_categories]:0),
        m_maximum_score((other.m_number_of_categories > 0)?new TSCORE[other.m_number_of_categories]:0),
        m_window_size(other.m_window_size)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_minimum_score[i] = other.m_minimum_score[i];
            m_maximum_score[i] = other.m_maximum_score[i];
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticRescale<TSCORE, TPROBABILITY>::~ProbabilisticRescale(void)
    {
        if (m_minimum_score != 0) delete [] m_minimum_score;
        if (m_maximum_score != 0) delete [] m_maximum_score;
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticRescale<TSCORE, TPROBABILITY>& ProbabilisticRescale<TSCORE, TPROBABILITY>::operator=(const ProbabilisticRescale<TSCORE, TPROBABILITY> &other)
    {
        if (this != &other)
        {
            if (m_minimum_score != 0) delete [] m_minimum_score;
            if (m_maximum_score != 0) delete [] m_maximum_score;
            
            ProbabilisticScoreBase<TSCORE, TPROBABILITY>::operator=(other);
            m_window_size = other.m_window_size;
            if (other.m_number_of_categories > 0)
            {
                m_minimum_score = new TSCORE[other.m_number_of_categories];
                m_maximum_score = new TSCORE[other.m_number_of_categories];
                
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_minimum_score[i] = other.m_minimum_score[i];
                    m_maximum_score[i] = other.m_maximum_score[i];
                }
            }
            else
            {
                m_minimum_score = 0;
                m_maximum_score = 0;
            }
        }
        
        return *this;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::set(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int window_size)
    {
        if (m_minimum_score != 0) delete [] m_minimum_score;
        if (m_maximum_score != 0) delete [] m_maximum_score;
        
        this->setBase(number_of_categories, multiclass, probability_factor);
        m_window_size = window_size;
        
        if (number_of_categories > 0)
        {
            m_minimum_score = new TSCORE[number_of_categories];
            m_maximum_score = new TSCORE[number_of_categories];
        }
        else
        {
            m_minimum_score = 0;
            m_maximum_score = 0;
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::attributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Window_Size", m_window_size);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::attributesFromXML(XmlParser &parser)
    {
        m_window_size = parser.getAttribute("Window_Size");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::dataToXML(XmlParser &parser) const
    {
        if (this->m_number_of_categories > 0)
        {
            saveVector(parser, "Minimum_Score", ConstantSubVectorDense<TSCORE, unsigned int>(m_minimum_score, this->m_number_of_categories));
            saveVector(parser, "Maximum_Score", ConstantSubVectorDense<TSCORE, unsigned int>(m_maximum_score, this->m_number_of_categories));
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::freeDataXML(void)
    {
        if (m_minimum_score != 0) { delete [] m_minimum_score; m_minimum_score = 0; }
        if (m_maximum_score != 0) { delete [] m_maximum_score; m_maximum_score = 0; }
    }
    
    template <class TSCORE, class TPROBABILITY>
    bool ProbabilisticRescale<TSCORE, TPROBABILITY>::dataFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Minimum_Score"))
        {
            VectorDense<TSCORE, unsigned int> data;
            
            loadVector(parser, "Minimum_Score", data);
            m_minimum_score = new TSCORE[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_minimum_score[i] = data[i];
            
            return true;
        }
        else if (parser.isTagIdentifier("Maximum_Score"))
        {
            VectorDense<TSCORE, unsigned int> data;
            
            loadVector(parser, "Maximum_Score", data);
            m_maximum_score = new TSCORE[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_maximum_score[i] = data[i];
            
            return true;
        }
        else return false;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN AND RESCALE FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger)
    {
        const double minimum_probability = 1e-12;
        const double maximum_probability = 1.0 - minimum_probability;
        if (logger != 0) logger->log("Calculating the rescaling parameters.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            Tuple<TSCORE, bool> * current_scores;
            Matrix<double> A, S, AA(2, 2), iAA(2, 2), At, solution(2, 1);
            VectorDense<double> window_probabilities(number_of_samples - m_window_size + 1);
            VectorDense<double> window_scores(number_of_samples - m_window_size + 1);
            
            current_scores = new Tuple<TSCORE, bool>[number_of_samples];
            
            for (unsigned int category = thread_id; category < this->m_number_of_categories; category += number_of_threads)
            {
                unsigned int accumulated_positive, number_of_probabilities;
                
                if ((logger != 0) && (thread_id == 0))
                    logger->log("Calibrating the classifier %d of %d.", category + 1, this->m_number_of_categories);
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    bool current_positive;
                    
                    current_positive = false;
                    for (unsigned int l = 0; (!current_positive) && (l < label[i].size()); ++l)
                        current_positive = (label[i][l] == category);
                    
                    current_scores[i].setData(score[i][category], current_positive);
                }
                
                std::sort(current_scores, current_scores + number_of_samples);
                
                accumulated_positive = 0;
                for (unsigned int i = 0; i < m_window_size; ++i)
                    if (current_scores[i].getSecond()) ++accumulated_positive;
                
                // Calculate the probability at each score.
                window_probabilities[0] = (double)accumulated_positive / (double)m_window_size;
                window_scores[0] = (double)current_scores[m_window_size / 2].getFirst();
                
                for (unsigned int i = m_window_size, j = m_window_size / 2 + 1, k = 0; i < number_of_samples; ++i, ++j, ++k)
                {
                    if (current_scores[k].getSecond()) --accumulated_positive;
                    if (current_scores[i].getSecond()) ++accumulated_positive;
                    window_probabilities[k + 1] = (double)accumulated_positive / (double)m_window_size;
                    window_scores[k + 1] = (double)current_scores[j].getFirst();
                }
                // Filter the scores which probability is 0 or 1.
                number_of_probabilities = 0;
                for (unsigned int i = 0; i < window_probabilities.size(); ++i)
                    if ((minimum_probability <= window_probabilities[i]) && (window_probabilities[i] <= maximum_probability))
                        ++number_of_probabilities;
                A.set(number_of_probabilities, 2);
                S.set(number_of_probabilities, 1);
                At.set(2, number_of_probabilities);
                number_of_probabilities = 0;
                for (unsigned int i = 0; i < window_probabilities.size(); ++i)
                {
                    if ((minimum_probability <= window_probabilities[i]) && (window_probabilities[i] <= maximum_probability))
                    {
                        A(number_of_probabilities, 0) = (1.0 - window_probabilities[i]);
                        A(number_of_probabilities, 1) = window_probabilities[i];
                        S(number_of_probabilities, 0) = (double)window_scores[i];
                        ++number_of_probabilities;
                    }
                }
                // Calculate the line parameters which better fit the probabilistic scores.
                MatrixMultiplication(A, true, A, false, AA);
                MatrixInverse(AA, iAA);
                MatrixMultiplication(iAA, false, A, true, At);
                MatrixMultiplication(At, S, solution);
                
                m_minimum_score[category] = (TSCORE)solution(0, 0);
                m_maximum_score[category] = (TSCORE)(solution(1, 0) + solution(0, 0));
            }
            
            delete [] current_scores;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger)
    {
        const unsigned int number_of_bins = bin_scores.size();
        const double minimum_probability = 1e-12;
        const double maximum_probability = 1.0 - minimum_probability;
        
        if (logger != 0) logger->log("Calculating the rescaling parameters.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            Matrix<double> A, S, AA(2, 2), iAA(2, 2), At, solution(2, 1);
            VectorDense<double> window_probabilities(number_of_bins - m_window_size + 1);
            VectorDense<double> window_scores(number_of_bins - m_window_size + 1);
            
            for (unsigned int category = omp_get_thread_num(); category < this->m_number_of_categories; category += number_of_threads)
            {
                unsigned int accumulated_positive, accumulated_negative, number_of_probabilities;
                
                accumulated_positive = accumulated_negative = 0;
                for (unsigned int b = 0; b < m_window_size; ++b)
                {
                    accumulated_positive += positive_histogram[category][b];
                    accumulated_negative += negative_histogram[category][b];
                }
                
                // Calculate the probability at each score.
                window_probabilities[0] = (accumulated_positive + accumulated_negative > 0)?((double)accumulated_positive / (double)(accumulated_positive + accumulated_negative)):0.5;
                window_scores[0] = (double)bin_scores[m_window_size / 2];
                for (unsigned int i = m_window_size, j = m_window_size / 2 + 1, k = 0; i < number_of_bins; ++i, ++j, ++k)
                {
                    accumulated_positive -= positive_histogram[category][k];
                    accumulated_negative -= negative_histogram[category][k];
                    accumulated_positive += positive_histogram[category][i];
                    accumulated_negative += negative_histogram[category][i];
                    window_probabilities[k + 1] = (accumulated_positive + accumulated_negative > 0)?((double)accumulated_positive / (double)(accumulated_positive + accumulated_negative)):0.5;
                    window_scores[k + 1] = (double)bin_scores[j];
                }
                // Filter the scores which probability is 0 or 1.
                number_of_probabilities = 0;
                for (unsigned int i = 0; i < window_probabilities.size(); ++i)
                    if ((minimum_probability <= window_probabilities[i]) && (window_probabilities[i] <= maximum_probability))
                        ++number_of_probabilities;
                A.set(number_of_probabilities, 2);
                S.set(number_of_probabilities, 1);
                At.set(2, number_of_probabilities);
                number_of_probabilities = 0;
                for (unsigned int i = 0; i < window_probabilities.size(); ++i)
                {
                    if ((minimum_probability <= window_probabilities[i]) && (window_probabilities[i] <= maximum_probability))
                    {
                        A(number_of_probabilities, 0) = (1.0 - window_probabilities[i]);
                        A(number_of_probabilities, 1) = window_probabilities[i];
                        S(number_of_probabilities, 0) = (double)window_scores[i];
                        ++number_of_probabilities;
                    }
                }
                // Calculate the line parameters which better fit the probabilistic scores.
                MatrixMultiplication(A, true, A, false, AA);
                MatrixInverse(AA, iAA);
                MatrixMultiplication(iAA, false, A, true, At);
                MatrixMultiplication(At, S, solution);
                
                m_minimum_score[category] = (TSCORE)solution(0, 0);
                m_maximum_score[category] = (TSCORE)(solution(1, 0) + solution(0, 0));
            }
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<TSCORE> difference(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            difference[i] = m_maximum_score[i] - m_minimum_score[i];
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
            {
                if ((logger != 0) && (thread_id == 0) && (i % (10 * number_of_threads) == 0))
                    logger->log("Processing the sample %d of %d.", i + 1, number_of_samples);
                probability[i].set(this->m_number_of_categories);
                
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                {
                    if (score[i][c] < m_minimum_score[c]) probability[i][c] = 0;
                    else if (score[i][c] > m_maximum_score[c]) probability[i][c] = (TPROBABILITY)this->m_probability_factor;
                    else probability[i][c] = (TPROBABILITY)(this->m_probability_factor * (score[i][c] - m_minimum_score[c]) / difference[c]);
                }
            }
        }
        
        if (this->m_multiclass)
        {
            if (logger != 0) logger->log("Converting probabilities into multi-class estimates.");
            this->protectedMulticlass(probability, number_of_samples, number_of_threads);
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        VectorDense<TSCORE> difference(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            difference[i] = m_maximum_score[i] - m_minimum_score[i];
        
        if ((void*)&score == (void*)&probability)
        {
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        TPROBABILITY * __restrict__ score_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr)
                        {
                            if (*score_ptr < m_minimum_score[c]) *score_ptr = 0;
                            else if (*score_ptr > m_maximum_score[c]) *score_ptr = (TPROBABILITY)this->m_probability_factor;
                            else *score_ptr = (TPROBABILITY)(this->m_probability_factor * (*score_ptr - m_minimum_score[c]) / difference[c]);
                        }
                    }
                }
            }
        }
        else
        {
            probability.setGeometry(score);
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        const TSCORE * __restrict__ score_ptr = score.get(y, c);
                        TPROBABILITY * __restrict__ prob_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr, ++prob_ptr)
                        {
                            if (*score_ptr < m_minimum_score[c]) *prob_ptr = 0;
                            else if (*score_ptr > m_maximum_score[c]) *prob_ptr = (TPROBABILITY)this->m_probability_factor;
                            else *prob_ptr = (TPROBABILITY)(this->m_probability_factor * (*score_ptr - m_minimum_score[c]) / difference[c]);
                        }
                    }
                }
            }
        }
        
        if (this->m_multiclass) this->protectedMulticlass(probability, number_of_threads);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticRescale<TSCORE, TPROBABILITY>::scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const
    {
        probability = srvMin<double>(1.0 - 1e-12, srvMax<double>(1e-12, probability));
        score.set(this->m_number_of_categories);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < this->m_number_of_categories; i += number_of_threads)
                score[i] = (TSCORE)(probability * (TPROBABILITY)(m_maximum_score[i] - m_minimum_score[i]) + (TPROBABILITY)m_minimum_score[i]);
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

