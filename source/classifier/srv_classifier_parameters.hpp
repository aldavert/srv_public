// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLASSIFIER_PARAMETERS_HEADER_FILE__
#define __SRV_CLASSIFIER_PARAMETERS_HEADER_FILE__

#include "../srv_utilities.hpp"
#include "../srv_logger.hpp"
#include "../srv_xml.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CLASSIFIER REGULARIZATION AND        |
    //                   | BALANCING PARAMETERS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Class which stores the base classifier parameters which are used by the classifier training algorithm.
     *  Since this parameters can be modified in a validation process, they are packed together to make it easier to manage them.
     *  \tparam TCLASSIFIER type used by the weight vectors of the classifier which uses the base parameters object.
     */
    template <class TCLASSIFIER>
    class ClassifierBaseParameters
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClassifierBaseParameters(void) : m_regularization_factor(1.0), m_rebalancing_factor(0.5), m_positive_weight(1.0), m_negative_weight(1.0) {}
        /// Constructor which only initializes the regularization factor.
        ClassifierBaseParameters(TCLASSIFIER regularization_factor) : m_regularization_factor(regularization_factor), m_rebalancing_factor(0.5), m_positive_weight(1.0), m_negative_weight(1.0) {}
        /** Constructor which initializes the classifier base parameters.
         *  \param[in] regularization_factor regularization factor which balances the contribution of the regularization and the loss function in the classifier.
         *  \param[in] rebalancing_factor factor which balances the weight of the positive and negative samples.
         *  \param[in] positive_weight weight of the positive samples.
         *  \param[in] negative_weight weight of the negative samples.
         */
        ClassifierBaseParameters(TCLASSIFIER regularization_factor, TCLASSIFIER rebalancing_factor, TCLASSIFIER positive_weight, TCLASSIFIER negative_weight) :
            m_regularization_factor(regularization_factor),
            m_rebalancing_factor(rebalancing_factor),
            m_positive_weight(positive_weight),
            m_negative_weight(negative_weight) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the regularization factor of the classifier.
        inline TCLASSIFIER getRegularizationFactor(void) const { return m_regularization_factor; }
        /// Returns a reference to the regularization factor of the classifier.
        inline TCLASSIFIER& getRegularizationFactor(void) { return m_regularization_factor; }
        /// Sets the regularization factor of the classifier.
        inline void setRegularizationFactor(TCLASSIFIER regularization_factor) { m_regularization_factor = regularization_factor; }
        
        /// Returns the balancing factor between the positive and negative samples.
        inline TCLASSIFIER getBalancingFactor(void) const { return m_rebalancing_factor; }
        /// Returns a reference to the balancing factor between the positive and negative samples.
        inline TCLASSIFIER& getBalancingFactor(void) { return m_rebalancing_factor; }
        /// Sets the balancing factor between the positive and negative samples.
        inline void setBalancingFactor(TCLASSIFIER rebalancing_factor) { m_rebalancing_factor = rebalancing_factor; }
        
        /// Returns the weight of the contribution of the positive samples.
        inline TCLASSIFIER getPositiveWeight(void) const { return m_positive_weight; }
        /// Returns a reference to the weight of the contribution of the positive samples.
        inline TCLASSIFIER& getPositiveWeight(void) { return m_positive_weight; }
        /// Sets the weight of the contribution of the positive samples.
        inline void setPositiveWeight(TCLASSIFIER positive_weight) { m_positive_weight = positive_weight; }
        
        /// Returns the weight of the contribution of the negative samples.
        inline TCLASSIFIER getNegativeWeight(void) const { return m_negative_weight; }
        /// Returns a reference to the weight of the contribution of the negative samples.
        inline TCLASSIFIER& getNegativeWeight(void) { return m_negative_weight; }
        /// Sets the weight of the contribution of the negative samples.
        inline void setNegativeWeight(TCLASSIFIER negative_weight) { m_negative_weight = negative_weight; }
        
        /** Sets the base parameters of a linear classifier.
         *  \param[in] regularization_factor regularization factor which balances the contribution of the regularization and the loss function in the classifier.
         *  \param[in] rebalancing_factor factor which balances the weight of the positive and negative samples.
         *  \param[in] positive_weight weight of the positive samples.
         *  \param[in] negative_weight weight of the negative samples.
         */
        inline void set(TCLASSIFIER regularization_factor, TCLASSIFIER rebalancing_factor, TCLASSIFIER positive_weight, TCLASSIFIER negative_weight)
        {
            m_regularization_factor = regularization_factor;
            m_rebalancing_factor = rebalancing_factor;
            m_positive_weight = positive_weight;
            m_negative_weight = negative_weight;
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the classifier base parameters into an XML object.
        inline void convertToXML(XmlParser &parser) const
        {
            parser.openTag("Base_Parameters");
            parser.setAttribute("Regularization_Factor", m_regularization_factor);
            parser.setAttribute("Balancing_Factor", m_rebalancing_factor);
            parser.setAttribute("Positive_Weight", m_positive_weight);
            parser.setAttribute("Negative_Weight", m_negative_weight);
            parser.closeTag();
        }
        /// Retrieves the classifier base parameters from an XML object.
        inline void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Base_Parameters"))
            {
                m_regularization_factor = parser.getAttribute("Regularization_Factor");
                m_rebalancing_factor = parser.getAttribute("Balancing_Factor");
                m_positive_weight = parser.getAttribute("Positive_Weight");
                m_negative_weight = parser.getAttribute("Negative_Weight");
                while (!(parser.isTagIdentifier("Base_Parameters") && parser.isCloseTag())) parser.getNext();
                parser.getNext();
            }
        }
    private:
        /// Regularization factor of the classifier.
        TCLASSIFIER m_regularization_factor;
        /// Balancing factor between the positive and negative samples.
        TCLASSIFIER m_rebalancing_factor;
        /// Weight of the contribution of the positive samples.
        TCLASSIFIER m_positive_weight;
        /// Weight of the contribution of the negative samples.
        TCLASSIFIER m_negative_weight;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

