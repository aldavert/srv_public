// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_PROBABILISTIC_SCORE_BASE_HEADER_FILE__
#define __SRV_PROBABILISTIC_SCORE_BASE_HEADER_FILE__

#include <list>
#include <algorithm>
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "../srv_xml.hpp"
#include "../srv_utilities.hpp"
#include "../srv_matrix.hpp"
#include "../srv_image.hpp"
#include "srv_classifier_definitions.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PROBABILITIES SCORES BASE VIRTUAL    |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which converts the score obtained by a classifier into a probabilistic estimation.
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticScoreBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticScoreBase(void) :
            m_number_of_categories(0),
            m_multiclass(false),
            m_probability_factor(1.0) {}
        /** Constructor which initializes the variables of the base class.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         */
        ProbabilisticScoreBase(unsigned int number_of_categories, bool multiclass, double probability_factor) :
            m_number_of_categories(number_of_categories),
            m_multiclass(multiclass),
            m_probability_factor(probability_factor) {}
        /// Copy constructor.
        ProbabilisticScoreBase(const ProbabilisticScoreBase<TSCORE, TPROBABILITY> &other) :
            m_number_of_categories(other.m_number_of_categories),
            m_multiclass(other.m_multiclass),
            m_probability_factor(other.m_probability_factor) {}
        /// Destructor.
        virtual ~ProbabilisticScoreBase(void) {}
        /// Assignation operator.
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>& operator=(const ProbabilisticScoreBase<TSCORE, TPROBABILITY> &other)
        {
            if (this != &other)
            {
                m_number_of_categories = other.m_number_of_categories;
                m_multiclass = other.m_multiclass;
                m_probability_factor = other.m_probability_factor;
            }
            return *this;
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the number of categories expected of a sample.
        inline unsigned int getNumberOfCategories(void) const { return m_number_of_categories; }
        /// Returns a boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
        inline bool getMulticlass(void) const { return m_multiclass; }
        /// Sets the boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
        inline void setMulticlass(bool multiclass) { this->m_multiclass = multiclass; }
        /// Returns the multiplicative factor which rescales the probabilities scores from [0, 1] to [0, factor].
        inline double getProbabilityFactor(void) const { return m_probability_factor; }
        /// Sets the multiplicative factor which rescales the probabilities scores from [0, 1] to [0, factor].
        inline void setProbabilityFactor(double probability_factor) const { m_probability_factor = probability_factor; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        virtual void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        virtual void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        virtual void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        virtual void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const = 0;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] score a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        virtual void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const = 0;
        
        /** Function which rescales the scores of a single sample.
         *  \param[in] score scores of the sample obtained by each classifier.
         *  \param[out] probability resulting probabilities of the given scores.
         */
        template <template <class, class> class VECTOR>
        void probabilities(const VECTOR<TSCORE, unsigned int> &score, VectorDense<TPROBABILITY, unsigned int> &probability) const
        {
            VectorDense<TSCORE, unsigned int> score_copy(m_number_of_categories, 0);
            score_copy.copy(score);
            probability(&score_copy, probability, 1, 1);
        }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        virtual ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const = 0;
        ///// /// Returns the class identifier for the probabilistic score method.
        ///// inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the probabilistic score object.
        ///// inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new (); }
        ///// /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        virtual PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Converts the information of the probabilistic score object into a XML information.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the information of the probabilistic score object from a XML information.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Protected functions ]------------------------------------------------------------------------------------------------------------------
        /// Sets the number of categories expected from a sample.
        inline void setNumberOfCategories(unsigned int number_of_categories) { m_number_of_categories = number_of_categories; }
        /// Sets the parameters of the base probabilistic score class.
        inline void setBase(unsigned int number_of_categories, bool multiclass, double probability_factor)
        {
            m_number_of_categories = number_of_categories;
            m_multiclass = multiclass;
            m_probability_factor = probability_factor;
        }
        /** Auxiliary function which converts the single-class probability estimations of an array of vectors into basic multi-class estimations.
         *  \param[in,out] probability array of vectors with the probabilities of each class.
         *  \param[in] number_of_samples number of samples in the probabilities vectors.
         *  \param[in] number_of_threads number of threads used to calculate the multi-class probabilities concurrently.
         */
        void protectedMulticlass(VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads) const;
        /** Auxiliary function which converts the single-class probability estimations of an image into basic multi-class estimations.
         *  \param[in,out] probability image with the probabilities of each class.
         *  \param[in] number_of_threads number of threads used to calculate the multi-class probabilities concurrently.
         */
        void protectedMulticlass(Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        /** Auxiliary function which calculates the minimum and maximum scores of a histogram scores distribution.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[out] minimum_score minimum score of the histograms.
         *  \param[out] maximum_score maximum score of the histograms.
         */
        inline void getMaximumMinimumScores(const VectorDense<unsigned int, unsigned int> &positive_histogram, const VectorDense<unsigned int, unsigned int> &negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, TSCORE &minimum_score, TSCORE &maximum_score) const
        {
            const unsigned int number_of_bins = bin_scores.size();
            unsigned int begin_idx, end_idx;
            
            for (begin_idx = 0; begin_idx < number_of_bins; ++begin_idx)
                if ((positive_histogram[begin_idx] > 0) && (negative_histogram[begin_idx] > 0)) break;
            for (end_idx = number_of_bins - 1; end_idx > 0; --end_idx)
                if ((positive_histogram[end_idx] > 0) && (negative_histogram[end_idx] > 0)) break;
            minimum_score = bin_scores[begin_idx];
            maximum_score = bin_scores[end_idx];
            if (minimum_score > maximum_score) maximum_score = minimum_score;
        }
        
        // -[ Virtual XML functions ]----------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        virtual void attributesToXML(XmlParser &parser) const = 0;
        /// Stores the probabilistic object information into the data of the XML object.
        virtual void dataToXML(XmlParser &parser) const = 0;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        virtual void freeDataXML(void) = 0;
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        virtual void attributesFromXML(XmlParser &parser) = 0;
        /// Retrieves the probabilistic object information into the data of the XML object.
        virtual bool dataFromXML(XmlParser &parser) = 0;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Number of categories (i.e.\ number of scores expected for each sample).
        unsigned int m_number_of_categories;
        /// Boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
        bool m_multiclass;
        /// Factor which rescales the probabilities scores from [0, 1] to [0, factor].
        double m_probability_factor;
        
        //// /// Static integer which indicates if the class has been initialized in the probability score factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PROBABILITIES SCORES BASE VIRTUAL    |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticScoreBase<TSCORE, TPROBABILITY>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Probabilistic_Score");
        parser.setAttribute("Identifier", (int)this->getIdentifier());
        parser.setAttribute("Number_Of_Categories", m_number_of_categories);
        parser.setAttribute("Multiclass", m_multiclass);
        parser.setAttribute("Probability_Factor", m_probability_factor);
        attributesToXML(parser);
        parser.addChildren();
        dataToXML(parser);
        parser.closeTag();
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticScoreBase<TSCORE, TPROBABILITY>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Probabilistic_Score") && ((PROBABILITY_METHOD_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getIdentifier()))
        {
            // Free data ............................................................................................................................
            freeDataXML();
            
            // Load attributes ......................................................................................................................
            m_number_of_categories = parser.getAttribute("Number_Of_Categories");
            m_multiclass = parser.getAttribute("Multiclass");
            m_probability_factor = parser.getAttribute("Probability_Factor");
            attributesFromXML(parser);
            
            // Load data ............................................................................................................................
            while (!(parser.isTagIdentifier("Probabilistic_Score") && parser.isCloseTag()))
            {
                if (!dataFromXML(parser)) parser.getNext();
            }
            parser.getNext();
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticScoreBase<TSCORE, TPROBABILITY>::protectedMulticlass(VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads) const
    {
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
                {
                    double accumulated_probability = 0;
                    for (unsigned int c = 0; c < m_number_of_categories; ++c)
                        accumulated_probability += (double)probability[c][i];
                    for (unsigned int c = 0; c < m_number_of_categories; ++c)
                        probability[c][i] = (TPROBABILITY)(m_probability_factor * (double)probability[c][i] / accumulated_probability);
                }
            }
        }
        else
        {
            for (unsigned int i = 0; i < number_of_samples; ++i)
            {
                double accumulated_probability = 0;
                for (unsigned int c = 0; c < m_number_of_categories; ++c)
                    accumulated_probability += (double)probability[c][i];
                for (unsigned int c = 0; c < m_number_of_categories; ++c)
                    probability[c][i] = (TPROBABILITY)(m_probability_factor * (double)probability[c][i] / accumulated_probability);
            }
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void  ProbabilisticScoreBase<TSCORE, TPROBABILITY>::protectedMulticlass(Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TPROBABILITY *> prob_ptrs(this->m_number_of_categories);
            
            for (unsigned int y = omp_get_thread_num(); y < probability.getHeight(); y += number_of_threads)
            {
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c) prob_ptrs[c] = probability.get(y, c);
                for (unsigned int x = 0; x < probability.getWidth(); ++x)
                {
                    double accumulated_probability = 0;
                    for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                        accumulated_probability += (double)*prob_ptrs[c];
                    for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                    {
                        *prob_ptrs[c] = (TPROBABILITY)(m_probability_factor * (double)*prob_ptrs[c] / accumulated_probability);
                        ++prob_ptrs[c];
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PROBABILITIES SCORES FACTORY         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TSCORE, class TPROBABILITY>
    struct ProbabilisticScoreFactory { typedef Factory<ProbabilisticScoreBase<TSCORE, TPROBABILITY>, PROBABILITY_METHOD_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

