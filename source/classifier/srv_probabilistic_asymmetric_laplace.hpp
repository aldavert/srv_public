// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_ASIMMETRIC_LAPLACE_PROBABILISTIC_SCORE_HEADER_FILE__
#define __SRV_ASIMMETRIC_LAPLACE_PROBABILISTIC_SCORE_HEADER_FILE__

#include "srv_probabilistic_score.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PROBABILISTIC SCORE USING ASYMMETRIC |
    //                   | GAUSSIAN FITTING                     |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Uses an asymmetric Laplace distribution to convert classifier scores into probabilities. This
     *  class implements the method proposed by Paul N. Bennett in 'Bennett, P.N., "Using Asymmetric
     *  Distribution to Improve Text Classifier Probability Estimates", SIGIR 2003'.
     */
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticAsymmetricLaplace : public ProbabilisticScoreBase<TSCORE, TPROBABILITY>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticAsymmetricLaplace(void);
        /** Constructor which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        ProbabilisticAsymmetricLaplace(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        /// Copy constructor.
        ProbabilisticAsymmetricLaplace(const ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> &other);
        /// Destructor.
        virtual ~ProbabilisticAsymmetricLaplace(void);
        /// Assignation operator.
        ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>& operator=(const ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> &other);
        /** Function which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        void set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array with the mode of the Laplace distribution of the negative samples for each classifier.
        inline const double * getNegativeModes(void) const { return m_negative_mode; }
        /// Returns the mode of the Laplace distribution of the negative samples of the index-th classifier.
        inline double getNegativeMode(unsigned int index) const { return m_negative_mode[index]; }
        /// Sets the mode of the Laplace distribution of the negative samples of the index-th classifier.
        inline void setNegativeMode(double negative_mode, unsigned int index) { m_negative_mode[index] = negative_mode; }
        /// Returns a constant pointer to the array with the gamma value of the Laplace distribution of the negative samples for each classifier.
        inline const double * getNegativeGammas(void) const { return m_negative_gamma; }
        /// Returns the gamma value of the Laplace distribution of the negative samples of the index-th classifier.
        inline double getNegativeGamma(unsigned int index) const { return m_negative_gamma[index]; }
        /// Sets the gamma value of the Laplace distribution of the negative samples of the index-th classifier.
        inline void getNegativeGamma(double negative_gamma, unsigned int index) { m_negative_gamma[index] = negative_gamma; }
        /// Returns a constant pointer to the array with the beta value of the Laplace distribution of the negative samples for each classifier.
        inline const double * getNegativeBeta(void) const { return m_negative_beta; }
        /// Returns the beta value of the Laplace distribution of the negative samples of the index-th classifier.
        inline double getNegativeBeta(unsigned int index) const { return m_negative_beta[index]; }
        /// Sets the beta value of the Laplace distribution of the negative samples of the index-th classifier.
        inline void setNegativeBeta(double negative_beta, unsigned int index) { m_negative_beta[index] = negative_beta; }
        /// Returns a constant pointer to the array with the prior probability of the negative probability.
        inline const double * getNegativePrior(void) const { return m_negative_prior; }
        /// Returns the prior of the negative probability of index-th classifier.
        inline double getNegativePrior(unsigned int index) const { return m_negative_prior[index]; }
        /// Sets the prior of the negative probability of index-th classifier.
        inline void setNegativePrior(double negative_prior, unsigned int index) { m_negative_prior[index] = negative_prior; }
        
        /// Returns a constant pointer to the array with the mode of the Laplace distribution of the positive samples for each classifier.
        inline const double * getPositiveModes(void) const { return m_positive_mode; }
        /// Returns the mode of the Laplace distribution of the positive samples of the index-th classifier.
        inline double getPositiveMode(unsigned int index) const { return m_positive_mode[index]; }
        /// Sets the mode of the Laplace distribution of the positive samples of the index-th classifier.
        inline void setPositiveMode(double positive_mode, unsigned int index) { m_positive_mode[index] = positive_mode; }
        /// Returns a constant pointer to the array with the gamma value of the Laplace distribution of the positive samples for each classifier.
        inline const double * getPositiveGammas(void) const { return m_positive_gamma; }
        /// Returns the gamma value of the Laplace distribution of the positive samples of the index-th classifier.
        inline double getPositiveGamma(unsigned int index) const { return m_positive_gamma[index]; }
        /// Sets the gamma value of the Laplace distribution of the positive samples of the index-th classifier.
        inline void getPositiveGamma(double positive_gamma, unsigned int index) { m_positive_gamma[index] = positive_gamma; }
        /// Returns a constant pointer to the array with the beta value of the Laplace distribution of the positive samples for each classifier.
        inline const double * getPositiveBeta(void) const { return m_positive_beta; }
        /// Returns the beta value of the Laplace distribution of the positive samples of the index-th classifier.
        inline double getPositiveBeta(unsigned int index) const { return m_positive_beta[index]; }
        /// Sets the beta value of the Laplace distribution of the positive samples of the index-th classifier.
        inline void setPositiveBeta(double positive_beta, unsigned int index) { m_positive_beta[index] = positive_beta; }
        /// Returns a constant pointer to the array with the prior probability of the positive probability.
        inline const double * getPositivePrior(void) const { return m_positive_prior; }
        /// Returns the prior of the positive probability of index-th classifier.
        inline double getPositivePrior(unsigned int index) const { return m_positive_prior[index]; }
        /// Sets the prior of the positive probability of index-th classifier.
        inline void setPositivePrior(double positive_prior, unsigned int index) { m_positive_prior[index] = positive_prior; }
        
        /// Returns the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline bool getBalance(void) const { return m_balance; }
        /// Sets the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline void setBalance(bool balance) { m_balance = balance; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] score a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        inline ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY> *)new ProbabilisticAsymmetricLaplace(*this); }
        /// Returns the class identifier for the probabilistic score method.
        inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return ASYMMETRIC_LAPLACE_ALGORITHM; }
        /// Generates a new empty instance of the probabilistic score object.
        inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new ProbabilisticAsymmetricLaplace(); }
        /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        inline PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const { return ASYMMETRIC_LAPLACE_ALGORITHM; }
        
    protected:
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        void attributesToXML(XmlParser &parser) const;
        /// Stores the probabilistic object information into the data of the XML object.
        void dataToXML(XmlParser &parser) const;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        void freeDataXML(void);
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        void attributesFromXML(XmlParser &parser);
        /// Retrieves the probabilistic object information into the data of the XML object.
        bool dataFromXML(XmlParser &parser);
        
        // -[ Other protected functions ]------------------------------------------------------------------------------------------------------------
        /** Calculates the parameters of the Laplace Asymmetric model.
         *  \param[in] scores array with the sorted scores of the positive or negative samples.
         *  \param[in] number_of_scores number of values in the scores array.
         *  \param[in] minimum_theta minimum mode of the Laplace distribution evaluated.
         *  \param[in] maximum_theta maximum mode of the Laplace distribution evaluated.
         *  \param[in] num_interval_slices number of divisions between consecutive scores.
         *  \param[out] final_theta resulting mode of the Laplace distribution.
         *  \param[out] final_beta resulting beta value of the Laplace distribution.
         *  \param[out] final_gamma resulting gamma value of the Laplace distribution.
         */
        void modelParameters(const TSCORE * current_scores, unsigned int number_of_scores, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_beta, double &final_gamma);
        /** Calculates the parameters of the Gaussian Asymmetric model for data stored into a histogram.
         *  \param[in] histogram histogram of scores.
         *  \param[in] bin_scores vector with the scores associated to each histogram bin.
         *  \param[in] scores_factor multiplier factor applied to the number of scores.
         *  \param[in] minimum_theta minimum mode of the Gaussian distribution evaluated.
         *  \param[in] maximum_theta maximum mode of the Gaussian distribution evaluated.
         *  \param[in] num_interval_slices number of divisions between consecutive scores.
         *  \param[out] final_theta resulting mode of the Gaussian distribution.
         *  \param[out] final_left_sigma resulting left sigma value of the Gaussian distribution.
         *  \param[out] final_right_sigma resulting right sigma value of the Gaussian distribution.
         */
        void modelParameters(const VectorDense<unsigned int, unsigned int> &histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, double scores_factor, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_beta, double &final_gamma);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Array with the mode of the Laplace distribution of the negative samples for each classifier.
        double * m_negative_mode;
        /// Array with the gamma value of the Laplace distribution of the negative samples for each classifier.
        double * m_negative_gamma;
        /// Array with the beta value of the Laplace distribution of the negative samples for each classifier.
        double * m_negative_beta;
        /// Array with the prior probability of the negative probability.
        double * m_negative_prior;
        /// Array with the mode of the Laplace distribution of the positive samples for each classifier.
        double * m_positive_mode;
        /// Array with the gamma value of the Laplace distribution of the positive samples for each classifier.
        double * m_positive_gamma;
        /// Array with the beta value of the Laplace distribution of the positive samples for each classifier.
        double * m_positive_beta;
        /// Array with the prior probability of the positive probability.
        double * m_positive_prior;
        /// Boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        bool m_balance;
        
        /// Static integer which indicates if the class has been initialized in the probability score factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PROBABILISTIC SCORE USING ASYMMETRIC |
    //                   | GAUSSIAN FITTING                     |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ACCESS FUNCTIONS ]============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricLaplace(void) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(),
        m_negative_mode(0),
        m_negative_gamma(0),
        m_negative_beta(0),
        m_negative_prior(0),
        m_positive_mode(0),
        m_positive_gamma(0),
        m_positive_beta(0),
        m_positive_prior(0),
        m_balance(false)
    {}
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricLaplace(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(number_of_categories, multiclass, probability_factor),
        m_negative_mode((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_gamma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_beta((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_prior((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_mode((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_gamma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_beta((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_prior((number_of_categories > 0)?new double[number_of_categories]:0),
        m_balance(balance)
    {
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricLaplace(const ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> &other) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(other),
        m_negative_mode((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_gamma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_beta((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_prior((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_mode((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_gamma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_beta((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_prior((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_balance(other.m_balance)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_negative_mode[i] = other.m_negative_mode[i];
            m_negative_gamma[i] = other.m_negative_gamma[i];
            m_negative_beta[i] = other.m_negative_beta[i];
            m_negative_prior[i] = other.m_negative_prior[i];
            m_positive_mode[i] = other.m_positive_mode[i];
            m_positive_gamma[i] = other.m_positive_gamma[i];
            m_positive_beta[i] = other.m_positive_beta[i];
            m_positive_prior[i] = other.m_positive_prior[i];
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::~ProbabilisticAsymmetricLaplace(void)
    {
        if (m_negative_mode != 0) delete [] m_negative_mode;
        if (m_negative_gamma != 0) delete [] m_negative_gamma;
        if (m_negative_beta != 0) delete [] m_negative_beta;
        if (m_negative_prior != 0) delete [] m_negative_prior;
        if (m_positive_mode != 0) delete [] m_positive_mode;
        if (m_positive_gamma != 0) delete [] m_positive_gamma;
        if (m_positive_beta != 0) delete [] m_positive_beta;
        if (m_positive_prior != 0) delete [] m_positive_prior;
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>& ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::operator=(const ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY> &other)
    {
        if (this != &other)
        {
            if (m_negative_mode != 0) delete [] m_negative_mode;
            if (m_negative_gamma != 0) delete [] m_negative_gamma;
            if (m_negative_beta != 0) delete [] m_negative_beta;
            if (m_negative_prior != 0) delete [] m_negative_prior;
            if (m_positive_mode != 0) delete [] m_positive_mode;
            if (m_positive_gamma != 0) delete [] m_positive_gamma;
            if (m_positive_beta != 0) delete [] m_positive_beta;
            if (m_positive_prior != 0) delete [] m_positive_prior;
            
            ProbabilisticScoreBase<TSCORE, TPROBABILITY>::operator=(other);
            m_balance = other.m_balance;
            if (other.m_number_of_categories > 0)
            {
                m_negative_mode = new double[other.m_number_of_categories];
                m_negative_gamma = new double[other.m_number_of_categories];
                m_negative_beta = new double[other.m_number_of_categories];
                m_negative_prior = new double[other.m_number_of_categories];
                m_positive_mode = new double[other.m_number_of_categories];
                m_positive_gamma = new double[other.m_number_of_categories];
                m_positive_beta = new double[other.m_number_of_categories];
                m_positive_prior = new double[other.m_number_of_categories];
                
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_negative_mode[i] = other.m_negative_mode[i];
                    m_negative_gamma[i] = other.m_negative_gamma[i];
                    m_negative_beta[i] = other.m_negative_beta[i];
                    m_negative_prior[i] = other.m_negative_prior[i];
                    m_positive_mode[i] = other.m_positive_mode[i];
                    m_positive_gamma[i] = other.m_positive_gamma[i];
                    m_positive_beta[i] = other.m_positive_beta[i];
                    m_positive_prior[i] = other.m_positive_prior[i];
                }
            }
            else
            {
                m_negative_mode = 0;
                m_negative_gamma = 0;
                m_negative_beta = 0;
                m_negative_prior = 0;
                m_positive_mode = 0;
                m_positive_gamma = 0;
                m_positive_beta = 0;
                m_positive_prior = 0;
            }
        }
        
        return *this;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance)
    {
        if (m_negative_mode != 0) delete [] m_negative_mode;
        if (m_negative_gamma != 0) delete [] m_negative_gamma;
        if (m_negative_beta != 0) delete [] m_negative_beta;
        if (m_negative_prior != 0) delete [] m_negative_prior;
        if (m_positive_mode != 0) delete [] m_positive_mode;
        if (m_positive_gamma != 0) delete [] m_positive_gamma;
        if (m_positive_beta != 0) delete [] m_positive_beta;
        if (m_positive_prior != 0) delete [] m_positive_prior;
        
        this->setBase(number_of_categories, multiclass, probability_factor);
        m_balance = balance;
        m_negative_mode = new double[number_of_categories];
        m_negative_gamma = new double[number_of_categories];
        m_negative_beta = new double[number_of_categories];
        m_negative_prior = new double[number_of_categories];
        m_positive_mode = new double[number_of_categories];
        m_positive_gamma = new double[number_of_categories];
        m_positive_beta = new double[number_of_categories];
        m_positive_prior = new double[number_of_categories];
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::attributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Balance", m_balance);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::attributesFromXML(XmlParser &parser)
    {
        m_balance = parser.getAttribute("Balance");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::dataToXML(XmlParser &parser) const
    {
        saveVector(parser, "Negative_Modes", ConstantSubVectorDense<double, unsigned int>(m_negative_mode, this->m_number_of_categories));
        saveVector(parser, "Negative_Gamma", ConstantSubVectorDense<double, unsigned int>(m_negative_gamma, this->m_number_of_categories));
        saveVector(parser, "Negative_Beta", ConstantSubVectorDense<double, unsigned int>(m_negative_beta, this->m_number_of_categories));
        saveVector(parser, "Negative_Prior", ConstantSubVectorDense<double, unsigned int>(m_negative_prior, this->m_number_of_categories));
        saveVector(parser, "Positive_Modes", ConstantSubVectorDense<double, unsigned int>(m_positive_mode, this->m_number_of_categories));
        saveVector(parser, "Positive_Gamma", ConstantSubVectorDense<double, unsigned int>(m_positive_gamma, this->m_number_of_categories));
        saveVector(parser, "Positive_Beta", ConstantSubVectorDense<double, unsigned int>(m_positive_beta, this->m_number_of_categories));
        saveVector(parser, "Positive_Prior", ConstantSubVectorDense<double, unsigned int>(m_positive_prior, this->m_number_of_categories));
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::freeDataXML(void)
    {
        if (m_negative_mode != 0) { delete [] m_negative_mode; m_negative_mode = 0; }
        if (m_negative_gamma != 0) { delete [] m_negative_gamma; m_negative_gamma = 0; }
        if (m_negative_beta != 0) { delete [] m_negative_beta; m_negative_beta = 0; }
        if (m_negative_prior != 0) { delete [] m_negative_prior; m_negative_prior = 0; }
        if (m_positive_mode != 0) { delete [] m_positive_mode; m_positive_mode = 0; }
        if (m_positive_gamma != 0) { delete [] m_positive_gamma; m_positive_gamma = 0; }
        if (m_positive_beta != 0) { delete [] m_positive_beta; m_positive_beta = 0; }
        if (m_positive_prior != 0) { delete [] m_positive_prior; m_positive_prior = 0; }
    }
    
    template <class TSCORE, class TPROBABILITY>
    bool ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::dataFromXML(XmlParser &parser)
    {
        VectorDense<double, unsigned int> data;
        
        if (parser.isTagIdentifier("Negative_Modes"))
        {
            loadVector(parser, "Negative_Modes", data);
            m_negative_mode = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_mode[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Gamma"))
        {
            loadVector(parser, "Negative_Gamma", data);
            m_negative_gamma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_gamma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Beta"))
        {
            loadVector(parser, "Negative_Beta", data);
            m_negative_beta = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_beta[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Prior"))
        {
            loadVector(parser, "Negative_Prior", data);
            m_negative_prior = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_prior[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Modes"))
        {
            loadVector(parser, "Positive_Modes", data);
            m_positive_mode = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_mode[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Gamma"))
        {
            loadVector(parser, "Positive_Gamma", data);
            m_positive_gamma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_gamma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Beta"))
        {
            loadVector(parser, "Positive_Beta", data);
            m_positive_beta = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_beta[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Prior"))
        {
            loadVector(parser, "Positive_Prior", data);
            m_positive_prior = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_prior[i] = data[i];
            return true;
        }
        else return false;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN AND RESCALE FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::modelParameters(const TSCORE * current_scores, unsigned int number_of_scores, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_beta, double &final_gamma)
    {
        double max_ln_posterior, ln_posterior, prev_score_theta, theta, beta, gamma, sum_left;
        double sum_right, diff_left, diff_right;
        unsigned int num_left, num_right, slice_num, i;
        bool max_init;
        
        max_ln_posterior = 0.0;
        max_init = false;
        prev_score_theta = minimum_theta;
        theta = prev_score_theta;
        num_left = 0;
        num_right = number_of_scores;
        sum_left = 0.0;
        sum_right = 0.0;
        slice_num = 0;
        
        for (i = 0; i < number_of_scores; ++i) sum_right += (double)current_scores[i];
        i = 0;
        while (true)
        {
            double next;
            
            // Update sufficient statistics.
            for (; (i < number_of_scores) && ((double)current_scores[i] <= theta); ++i)
            {
                ++num_left;
                --num_right;
                sum_left += (double)current_scores[i];
                sum_right -= (double)current_scores[i];
            }
            diff_left = srvMax<double>(0.0, (double)num_left * theta - sum_left);
            diff_right = srvMax<double>(0.0, sum_right - (double)num_right * theta);
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            ////// std::cout << "A) Sum_Left=" << sum_left << " Sum_Right=" << sum_right << " Diff_Left=" << diff_left << " Diff_Right=" << diff_right << std::endl;
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            
            // Compute beta.
            if (diff_left == 0) beta = 1e40;
            else beta = (double)number_of_scores / (diff_left + sqrt(diff_left) * sqrt(diff_right));
            // Compute gamma.
            if (diff_right == 0) gamma = 1e40;
            else gamma = (double)number_of_scores / (diff_right + sqrt(diff_left) * sqrt(diff_right));
            
            // Compute log posterior.
            ln_posterior = ((double)number_of_scores * (log(beta * gamma) - log(beta + gamma)) - beta * diff_left - gamma * diff_right);
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            ////// std::cout << "B) Theta=" << theta << " Beta=" << beta << " Gamma=" << gamma << " Ln_Posterior=" << ln_posterior << std::endl;
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            
            // Update set of best parameters.
            if (max_init)
            {
                if (ln_posterior > max_ln_posterior)
                {
                    final_theta = theta;
                    final_beta = beta;
                    final_gamma = gamma;
                    max_ln_posterior = ln_posterior;
                    ///// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
                    ///// std::cout << "NEW MAXIMUM FOUND:" << std::endl;
                    ///// std::cout << "  * THETA: " << final_theta << std::endl;
                    ///// std::cout << "  * BETA: " << final_beta << std::endl;
                    ///// std::cout << "  * GAMMA: " << final_gamma << std::endl;
                    ///// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
                }
            }
            else
            {
                final_theta = theta;
                final_beta = beta;
                final_gamma = gamma;
                max_ln_posterior = ln_posterior;
                max_init = true;
            }
            
            // Get new choice for theta.
            if (theta == maximum_theta) break;
            
            next = maximum_theta;
            if (i != number_of_scores)
                next = current_scores[i];
            ++slice_num;
            if ((slice_num % num_interval_slices) == 0)
            {
                prev_score_theta = next;
                theta = prev_score_theta;
                slice_num = 0;
            }
            else theta = slice_num * ((next - prev_score_theta) / (double)num_interval_slices) + prev_score_theta;
            if (theta > maximum_theta) break;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::modelParameters(const VectorDense<unsigned int, unsigned int> &histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, double scores_factor, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_beta, double &final_gamma)
    {
        const unsigned int number_of_bins = bin_scores.size();
        double max_ln_posterior, ln_posterior, prev_score_theta, theta, beta, gamma, sum_left;
        double sum_right, diff_left, diff_right, number_of_scores, num_right, num_left;
        unsigned int slice_num;
        bool max_init;
        
        number_of_scores = 0.0;
        for (unsigned int b = 0; b < number_of_bins; ++b)
            number_of_scores += (double)histogram[b];
        number_of_scores *= scores_factor;
        
        max_ln_posterior = 0.0;
        max_init = false;
        prev_score_theta = minimum_theta;
        theta = prev_score_theta;
        num_left = 0.0;
        num_right = number_of_scores;
        sum_left = 0.0;
        sum_right = 0.0;
        slice_num = 0;
        
        for (unsigned int b = 0; b < number_of_bins; ++b) sum_right += scores_factor * (double)histogram[b] * bin_scores[b];
        for (unsigned int b = 0; true;)
        {
            double next;
            
            // Update sufficient statistics .........................................................................................................
            for (; (b < number_of_bins) && ((double)bin_scores[b] <= theta); ++b)
            {
                const double current_factor = scores_factor * (double)histogram[b];
                
                num_left  = srvMin(number_of_scores,  num_left + current_factor);
                num_right = srvMax(             0.0, num_right - current_factor);
                sum_left  += (double)bin_scores[b] * current_factor;
                sum_right -= (double)bin_scores[b] * current_factor;
            }
            diff_left  = srvMax<double>(0.0, (double)num_left * theta - sum_left);
            diff_right = srvMax<double>(0.0, sum_right - (double)num_right * theta);
            
            // Compute beta, gamma and log posterior ................................................................................................
            beta  = (diff_left  == 0)?1e40:((double)number_of_scores / (diff_left  + std::sqrt(diff_left) * std::sqrt(diff_right)));
            gamma = (diff_right == 0)?1e40:((double)number_of_scores / (diff_right + std::sqrt(diff_left) * std::sqrt(diff_right)));
            ln_posterior = ((double)number_of_scores * (std::log(beta * gamma) - std::log(beta + gamma)) - beta * diff_left - gamma * diff_right);
            
            // Update set of best parameters ........................................................................................................
            if (max_init)
            {
                if (ln_posterior > max_ln_posterior)
                {
                    final_theta = theta;
                    final_beta = beta;
                    final_gamma = gamma;
                    max_ln_posterior = ln_posterior;
                }
            }
            else
            {
                final_theta = theta;
                final_beta = beta;
                final_gamma = gamma;
                max_ln_posterior = ln_posterior;
                max_init = true;
            }
            
            // Get new choice for theta .............................................................................................................
            if (theta == maximum_theta) break;
            
            next = (b != number_of_bins)?bin_scores[b]:maximum_theta;
            ++slice_num;
            if ((slice_num % num_interval_slices) == 0)
            {
                prev_score_theta = next;
                theta = prev_score_theta;
                slice_num = 0;
            }
            else theta = slice_num * ((next - prev_score_theta) / (double)num_interval_slices) + prev_score_theta;
            if (theta > maximum_theta) break;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int number_of_positive_scores, number_of_negative_scores;
            const unsigned int thread_id = omp_get_thread_num();
            TSCORE * positive_negative_scores;
            Tuple<TSCORE, bool> * current_scores;
            
            current_scores = new Tuple<TSCORE, bool>[number_of_samples];
            
            for (unsigned int category = thread_id; category < this->m_number_of_categories; category += number_of_threads)
            {
                unsigned int maximum_number_of_scores;
                if ((logger != 0) && (thread_id == 0))
                    logger->log("Calibrating the classifier %d of %d.", category + 1, this->m_number_of_categories);
                
                number_of_positive_scores = number_of_negative_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    bool current_positive;
                    
                    current_positive = false;
                    for (unsigned int l = 0; (!current_positive) && (l < label[i].size()); ++l)
                        current_positive = (label[i][l] == category);
                    
                    current_scores[i].setData(score[i][category], current_positive);
                    if (current_positive) ++number_of_positive_scores;
                    else ++number_of_negative_scores;
                }
                std::sort(current_scores, current_scores + number_of_samples);
                m_positive_prior[category] = (double)number_of_positive_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                m_negative_prior[category] = (double)number_of_negative_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                
                maximum_number_of_scores = srvMax<unsigned int>(number_of_positive_scores, number_of_negative_scores);
                positive_negative_scores = new TSCORE[maximum_number_of_scores];
                
                number_of_positive_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    if (current_scores[i].getSecond())
                    {
                        positive_negative_scores[number_of_positive_scores] = current_scores[i].getFirst();
                        ++number_of_positive_scores;
                    }
                }
                if (m_balance && (number_of_positive_scores < maximum_number_of_scores))
                {
                    unsigned int index;
                    for (index = number_of_positive_scores; index < maximum_number_of_scores; ++index)
                        positive_negative_scores[index] = positive_negative_scores[index % number_of_positive_scores];
                    number_of_positive_scores = maximum_number_of_scores;
                    std::sort(positive_negative_scores, positive_negative_scores + maximum_number_of_scores);
                }
                modelParameters(positive_negative_scores, number_of_positive_scores, (double)positive_negative_scores[0], (double)positive_negative_scores[number_of_positive_scores - 1], 10, m_positive_mode[category], m_positive_beta[category], m_positive_gamma[category]);
                
                number_of_negative_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    if (!current_scores[i].getSecond())
                    {
                        positive_negative_scores[number_of_negative_scores] = current_scores[i].getFirst();
                        ++number_of_negative_scores;
                    }
                }
                if (m_balance && (number_of_negative_scores < maximum_number_of_scores))
                {
                    unsigned int index;
                    for (index = number_of_negative_scores; index < maximum_number_of_scores; ++index)
                        positive_negative_scores[index] = positive_negative_scores[index % number_of_negative_scores];
                    number_of_negative_scores = maximum_number_of_scores;
                    std::sort(positive_negative_scores, positive_negative_scores + maximum_number_of_scores);
                }
                modelParameters(positive_negative_scores, number_of_negative_scores, (double)positive_negative_scores[0], (double)positive_negative_scores[number_of_negative_scores - 1], 10, m_negative_mode[category], m_negative_beta[category], m_negative_gamma[category]);
                if ((logger != 0) && (thread_id == 0))
                {
                    logger->log("Parameters of the Laplace distribution over the POSITIVE samples:");
                    logger->log(" · Mode: %f", m_positive_mode[category]);
                    logger->log(" · Gamma: %f", m_positive_gamma[category]);
                    logger->log(" · Beta: %f", m_positive_beta[category]);
                    logger->log("Parameters of the Laplace distribution over the NEGATIVE samples:");
                    logger->log(" · Mode: %f", m_negative_mode[category]);
                    logger->log(" · Gamma: %f", m_negative_gamma[category]);
                    logger->log(" · Beta: %f", m_negative_beta[category]);
                    logger->log("Prior probabilities");
                    logger->log("Positive: %f", m_positive_prior[category]);
                    logger->log("Negative: %f", m_negative_prior[category]);
                }
                
                delete [] positive_negative_scores;
            }
            
            delete [] current_scores;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int number_of_bins = bin_scores.size();
            unsigned int number_of_positive_scores, number_of_negative_scores;
            
            for (unsigned int category = omp_get_thread_num(); category < this->m_number_of_categories; category += number_of_threads)
            {
                double scores_factor, maximum_number_of_scores;
                number_of_positive_scores = number_of_negative_scores = 0;
                for (unsigned int b = 0; b < number_of_bins; ++b)
                {
                    number_of_positive_scores += positive_histogram[category][b];
                    number_of_negative_scores += negative_histogram[category][b];
                }
                m_positive_prior[category] = (double)number_of_positive_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                m_negative_prior[category] = (double)number_of_negative_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                maximum_number_of_scores = (double)srvMax<unsigned int>(number_of_positive_scores, number_of_negative_scores);
                
                scores_factor = (m_balance)?(maximum_number_of_scores / (double)number_of_positive_scores):1.0;
                modelParameters(positive_histogram[category], bin_scores, scores_factor, bin_scores[0], bin_scores[number_of_bins - 1], 10, m_positive_mode[category], m_positive_beta[category], m_positive_gamma[category]);
                scores_factor = (m_balance)?(maximum_number_of_scores / (double)number_of_negative_scores):1.0;
                modelParameters(negative_histogram[category], bin_scores, scores_factor, bin_scores[0], bin_scores[number_of_bins - 1], 10, m_negative_mode[category], m_negative_beta[category], m_negative_gamma[category]);
                
                if ((logger != 0) && (category % number_of_threads == 0))
                {
                    logger->log("Parameters of the Laplace distribution over the POSITIVE samples:");
                    logger->log(" · Mode: %f", m_positive_mode[category]);
                    logger->log(" · Gamma: %f", m_positive_gamma[category]);
                    logger->log(" · Beta: %f", m_positive_beta[category]);
                    logger->log("Parameters of the Laplace distribution over the NEGATIVE samples:");
                    logger->log(" · Mode: %f", m_negative_mode[category]);
                    logger->log(" · Gamma: %f", m_negative_gamma[category]);
                    logger->log(" · Beta: %f", m_negative_beta[category]);
                    logger->log("Prior probabilities");
                    logger->log("Positive: %f", m_positive_prior[category]);
                    logger->log("Negative: %f", m_negative_prior[category]);
                }
            }
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<TPROBABILITY> constant_positive(this->m_number_of_categories), constant_negative(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            constant_positive[i] = m_positive_beta[i] * m_positive_gamma[i] / (m_positive_beta[i] + m_positive_gamma[i]);
            constant_negative[i] = m_negative_beta[i] * m_negative_gamma[i] / (m_negative_beta[i] + m_negative_gamma[i]);
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
            {
                if ((logger != 0) && (thread_id == 0) && (i % (10 * number_of_threads) == 0))
                    logger->log("Processing the sample %d of %d.", i + 1, number_of_samples);
                probability[i].set(this->m_number_of_categories);
                
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                {
                    double positive_probability, negative_probability;
                    
                    if (score[i][c] <= m_positive_mode[c])
                        positive_probability = constant_positive[c] * std::exp(-m_positive_beta[c] * (m_positive_mode[c] - (double)score[i][c]));
                    else positive_probability = constant_positive[c] * std::exp(-m_positive_gamma[c] * ((double)score[i][c] - m_positive_mode[c]));
                    
                    if (score[i][c] <= m_negative_mode[c])
                        negative_probability = constant_negative[c] * std::exp(-m_negative_beta[c] * (m_negative_mode[c] - (double)score[i][c]));
                    else negative_probability = constant_negative[c] * std::exp(-m_negative_gamma[c] * ((double)score[i][c] - m_negative_mode[c]));
                    
                    ///// std::cout << "Score: " << score[i][c] << std::endl;
                    ///// std::cout << "Positive Probability: " << positive_probability << std::endl;
                    ///// std::cout << "Negative Probability: " << negative_probability << std::endl;
                    probability[i][c] = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                    ///// std::cout << "Joint probability: " << probability[i][c] << std::endl;
                }
            }
        }
        
        if (this->m_multiclass)
        {
            if (logger != 0) logger->log("Converting probabilities into multi-class estimates.");
            this->protectedMulticlass(probability, number_of_samples, number_of_threads);
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        VectorDense<TPROBABILITY> constant_positive(this->m_number_of_categories), constant_negative(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            constant_positive[i] = m_positive_beta[i] * m_positive_gamma[i] / (m_positive_beta[i] + m_positive_gamma[i]);
            constant_negative[i] = m_negative_beta[i] * m_negative_gamma[i] / (m_negative_beta[i] + m_negative_gamma[i]);
        }
        
        if ((void*)&score == (void*)&probability)
        {
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        TPROBABILITY * __restrict__ score_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr)
                        {
                            double positive_probability, negative_probability;
                            
                            if (*score_ptr <= m_positive_mode[c])
                                positive_probability = constant_positive[c] * std::exp(-m_positive_beta[c] * (m_positive_mode[c] - (double)*score_ptr));
                            else positive_probability = constant_positive[c] * std::exp(-m_positive_gamma[c] * ((double)*score_ptr - m_positive_mode[c]));
                            
                            if (*score_ptr <= m_negative_mode[c])
                                negative_probability = constant_negative[c] * std::exp(-m_negative_beta[c] * (m_negative_mode[c] - (double)*score_ptr));
                            else negative_probability = constant_negative[c] * std::exp(-m_negative_gamma[c] * ((double)*score_ptr - m_negative_mode[c]));
                            
                            *score_ptr = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                        }
                    }
                }
            }
        }
        else
        {
            probability.setGeometry(score);
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        const TSCORE * __restrict__ score_ptr = score.get(y, c);
                        TPROBABILITY * __restrict__ prob_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr, ++prob_ptr)
                        {
                            double positive_probability, negative_probability;
                            
                            if (*score_ptr <= m_positive_mode[c])
                                positive_probability = constant_positive[c] * std::exp(-m_positive_beta[c] * (m_positive_mode[c] - (double)*score_ptr));
                            else positive_probability = constant_positive[c] * std::exp(-m_positive_gamma[c] * ((double)*score_ptr - m_positive_mode[c]));
                            
                            if (*score_ptr <= m_negative_mode[c])
                                negative_probability = constant_negative[c] * std::exp(-m_negative_beta[c] * (m_negative_mode[c] - (double)*score_ptr));
                            else negative_probability = constant_negative[c] * std::exp(-m_negative_gamma[c] * ((double)*score_ptr - m_negative_mode[c]));
                            
                            *prob_ptr = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                        }
                    }
                }
            }
        }
        
        if (this->m_multiclass) this->protectedMulticlass(probability, number_of_threads);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricLaplace<TSCORE, TPROBABILITY>::scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const
    {
        VectorDense<double> values(this->m_number_of_categories);
        probability = srvMin<double>(1.0 - 1e-12, srvMax<double>(1e-12, probability));
        score.set(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            double constant_positive, constant_negative;
            
            constant_positive = srvMax(1e-10, m_positive_prior[i] * (1.0 - probability) * (m_positive_beta[i] * m_positive_gamma[i] / (m_positive_beta[i] + m_positive_gamma[i])));
            constant_negative = srvMax(1e-10, m_negative_prior[i] * probability * (m_negative_beta[i] * m_negative_gamma[i] / (m_negative_beta[i] + m_negative_gamma[i])));
            values[i] = log(constant_negative) - log(constant_positive);
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < this->m_number_of_categories; i += number_of_threads)
            {
                double x[4], p, n;
                
                n = m_negative_beta[i];
                p = m_positive_beta[i];
                if (p - n != 0) x[0] = (values[i] - n * m_negative_mode[i] + p * m_positive_mode[i]) / (p - n);
                else x[0] = std::numeric_limits<double>::infinity();
                if (m_positive_mode[i] > m_negative_mode[i])
                {
                    n = -m_negative_gamma[i];
                    p = m_positive_beta[i];
                    if (p - n != 0) x[1] = (values[i] - n * m_negative_mode[i] + p * m_positive_mode[i]) / (p - n);
                    else x[1] = std::numeric_limits<double>::infinity();
                }
                else x[1] = std::numeric_limits<double>::infinity();
                if (m_positive_mode[i] < m_negative_mode[i])
                {
                    n = m_negative_beta[i];
                    p = -m_positive_gamma[i];
                    if (p - n != 0) x[2] = (values[i] - n * m_negative_mode[i] + p * m_positive_mode[i]) / (p - n);
                    else x[2] = std::numeric_limits<double>::infinity();
                }
                else x[2] = std::numeric_limits<double>::infinity();
                n = -m_negative_gamma[i];
                p = -m_positive_gamma[i];
                if (p - n != 0) x[3] = (values[i] - n * m_negative_mode[i] + p * m_positive_mode[i]) / (p - n);
                else x[3] = std::numeric_limits<double>::infinity();
                
                score[i] = x[0];
                for (unsigned int k = 0; k < 4; ++k)
                {
                    if (srvAbs<double>(x[k]) < srvAbs<double>(score[i]))
                        score[i] = x[k];
                }
                if (score[i] == std::numeric_limits<double>::infinity())
                {
                    if (probability < 0.5) score[i] = m_negative_mode[i];
                    else score[i] = m_positive_mode[i];
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

