// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_MULTICLASS_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_MULTICLASS_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"
#include <algorithm>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | MULTICLASS SVM CLASS                 |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the Multi-class Linear classifier.
     *  Information about the implemented algorithm can be found in the following papers:<br/>
     *  <br/>
     *  K. Crammer, Y. Singer, <b>"On the Learnability and Design of Output Codes for Multiclass Problems"</b>, <i>Machine Learning, 47, 201--233, 2002.</i>.<br/>
     *  S.S. Keerthi, S. Sundararajan, K-W. Chang, C-J. Hsieh, C-J. Lin, <b>"A Sequential Dual Method for Large Scale Multi-Class Linear SVMs"</b>, <i>In Proceedings of the fourteenth ACM SIGKDD International Conference on Knowledge Discovery and DAta Mining, 408--416, 2008</i>.<br/>
     *  R.E. Fan, K.W, Chang, C.J. Hsieh, X.R. Wang, C.J. Lin, <b>"LIBLINEAR: A Library for Large Scale Classification"</b>, <i>Journal of Machine Learning Research, 9, 1871--1874, 2008.</i>.<br/>
     *  <br/>
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class MulticlassSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        MulticlassSVM(void) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         */
        MulticlassSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations) {}
        /// Copy constructor.
        MulticlassSVM(const MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        MulticlassSVM(const MulticlassSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Destructor.
        virtual ~MulticlassSVM(void) {}
        /// Assignation operator.
        inline MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other) LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const MulticlassSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_MULTICLASS; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_MULTICLASS; }
        
    protected:
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | MULTICLASS SVM CLASS                 |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<VectorDense<TCLASSIFIER, unsigned int> > alpha(number_of_train_samples);
        VectorDense<VectorDense<unsigned int, unsigned int> > alpha_index(number_of_train_samples);
        VectorDense<VectorDense<char, unsigned int> > sample_sign(number_of_train_samples);
        VectorDense<TCLASSIFIER, unsigned int> alpha_new(this->m_number_of_categories), QD(number_of_train_samples), B(this->m_number_of_categories), D(this->m_number_of_categories);
        VectorDense<TCLASSIFIER, unsigned int> G(this->m_number_of_categories), positive_weights(this->m_number_of_categories);
        VectorDense<unsigned int, unsigned int> index(number_of_train_samples), alpha_active_size(number_of_train_samples), total_alpha_size(number_of_train_samples);
        VectorSparse<TCLASSIFIER, unsigned int> difference_values(this->m_number_of_categories);
        unsigned int active_size, iteration;
        bool start_from_all;
        TCLASSIFIER epsilon_shrink;
        
        if (logger != 0)
            logger->log("Initializing multi-class SVM structures.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int category_id = thread_id; category_id < this->m_number_of_categories; category_id += number_of_threads)
            {
                positive_weights[category_id] = base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                this->m_classifiers[category_id].setValue(_C(0));
            }
            for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
            {
                alpha[i].set(this->m_number_of_categories, _C(0));
                alpha_index[i].set(this->m_number_of_categories);
                sample_sign[i].set(this->m_number_of_categories);
                index[i] = i;
                QD[i] = _C(0);
                for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                {
                    const TCLASSIFIER current_value = _C(train_samples[i]->getData().getValue(p));
                    QD[i] += current_value * current_value;
                }
                
                alpha_active_size[i] = 0;
                for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
                {
                    TLABEL probability;
                    
                    probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                    if (this->m_ignore_difficult)
                    {
                        if (probability >= this->m_difficult_probability)
                        {
                            sample_sign[i][category_id] = 1;
                            alpha_index[i][alpha_active_size[i]] = category_id;
                            ++alpha_active_size[i];
                        }
                        else if (probability < this->m_positive_probability)
                        {
                            sample_sign[i][category_id] = -1;
                            alpha_index[i][alpha_active_size[i]] = category_id;
                            ++alpha_active_size[i];
                        }
                        else sample_sign[i][category_id] = 0;
                    }
                    else
                    {
                        if (probability >= this->m_positive_probability) sample_sign[i][category_id] = 1;
                        else sample_sign[i][category_id] = -1;
                        alpha_index[i][category_id] = category_id;
                        ++alpha_active_size[i];
                    }
                }
                total_alpha_size[i] = alpha_active_size[i];
            }
        }
        
        active_size = number_of_train_samples;
        epsilon_shrink = srvMax<TCLASSIFIER>(_C(10.0) * this->m_epsilon, _C(1.0));
        start_from_all = true;
        for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
        {
            TCLASSIFIER stopping;
            
            stopping = -std::numeric_limits<TCLASSIFIER>::max();
            for (unsigned int i = 0; i < active_size; ++i) srvSwap(index[i], index[i + rand() % (active_size - i)]);
            for (unsigned int i = 0; i < active_size; ++i)
            {
                const unsigned int current_index = index[i];
                unsigned int D_index;
                TCLASSIFIER Ai;
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(train_samples[current_index]->getData());
                SubVectorDense<char, unsigned int> current_sign(sample_sign[current_index].getData(), sample_sign[current_index].size());
                SubVectorDense<TCLASSIFIER, unsigned int> current_alpha(alpha[current_index].getData(), alpha[current_index].size());
                SubVectorDense<unsigned int, unsigned int> current_alpha_index(alpha_index[current_index].getData(), alpha_index[current_index].size());
                std::sort(&current_alpha_index[0], &current_alpha_index[alpha_active_size[current_index]]);
                
                Ai = QD[current_index];
                if (Ai > _C(0))
                {
                    TCLASSIFIER minimum_g, maximum_g, beta;
                    unsigned int difference_index, r;
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_id = omp_get_thread_num();
                        for (unsigned int j = thread_id; j < alpha_active_size[current_index]; j += number_of_threads)
                        {
                            const unsigned int selected_category = current_alpha_index[j];
                            TCLASSIFIER score;
                            
                            if (current_sign[selected_category] < 0) G[j] = _C(1); // Set negative samples with G = 1,
                            else G[j] = _C(0); // and positive samples with G = 0.
                            
                            score = this->classifierDot(this->m_classifiers[selected_category], current_sample);
                            G[j] += score;
                        }
                    }
                    
                    minimum_g = std::numeric_limits<TCLASSIFIER>::max();
                    maximum_g = -std::numeric_limits<TCLASSIFIER>::max();
                    
                    for (unsigned int j = 0; j < alpha_active_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        if ((current_alpha[selected_category] < _C(0)) && (G[j] < minimum_g)) minimum_g = G[j];
                        if ((current_sign[selected_category] > 0) && (current_alpha[selected_category] < positive_weights[selected_category]) && (G[j] < minimum_g)) minimum_g = G[j];
                        if (G[j] > maximum_g) maximum_g = G[j];
                    }
                    
                    for (unsigned int j = 0; j < alpha_active_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        TCLASSIFIER bound;
                        
                        if (current_sign[selected_category] > 0) bound = positive_weights[selected_category];
                        else bound = _C(0);
                        if ((current_alpha[selected_category] == bound) && (G[j] < minimum_g))
                        {
                            --alpha_active_size[current_index];
                            srvSwap(current_alpha_index[j], current_alpha_index[alpha_active_size[current_index]]);
                            srvSwap(G[j], G[alpha_active_size[current_index]]);
                            --j;
                        }
                    }
                    
                    if (alpha_active_size[current_index] <= 1)
                    {
                        --active_size;
                        srvSwap(index[i], index[active_size]);
                        --i;
                        continue;
                    }
                    if (maximum_g - minimum_g <= _C(1e-12)) continue;
                    else stopping = srvMax<TCLASSIFIER>(maximum_g - minimum_g, stopping);
                    
                    for (unsigned int j = 0; j < alpha_active_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        
                        D[j] = B[j] = G[j] - Ai * current_alpha[selected_category];
                        if (current_sign[selected_category] > 0) D[j] += Ai * positive_weights[selected_category];
                    }
                    // ------------------------------[ SOLVE SUB-PROBLEM BEGIN ]------------------------------
                    std::sort(&D[0], &D[alpha_active_size[current_index]]);
                    D_index = alpha_active_size[current_index] - 1;
                    beta = D[D_index];
                    for (unsigned int j = 0; j < total_alpha_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        if (current_sign[selected_category] > 0)
                            beta -= Ai * positive_weights[selected_category];
                    }
                    --D_index;
                    for (r = 1; (r < alpha_active_size[current_index]) && (beta < _C(r) * D[D_index]); ++r, --D_index)
                        beta += D[D_index];
                    beta /= _C(r);
                    for (unsigned int j = 0; j < alpha_active_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        if (current_sign[selected_category] > 0) alpha_new[j] = srvMin<TCLASSIFIER>(positive_weights[selected_category], (beta - B[j]) / Ai);
                        else alpha_new[j] = srvMin<TCLASSIFIER>(_C(0.0), (beta - B[j]) / Ai);
                    }
                    // -------------------------------[ SOLVE SUB-PROBLEM END ]-------------------------------
                    difference_index = 0;
                    for (unsigned int j = 0; j < alpha_active_size[current_index]; ++j)
                    {
                        const unsigned int selected_category = current_alpha_index[j];
                        TCLASSIFIER current_difference;
                        
                        current_difference = alpha_new[j] - current_alpha[selected_category];
                        current_alpha[selected_category] = alpha_new[j];
                        if (srvAbs<TCLASSIFIER>(current_difference) >= _C(1e-12))
                        {
                            difference_values[difference_index].setData(current_difference, selected_category);
                            ++difference_index;
                        }
                    }
                    
                    for (NSAMPLE k = 0; k < current_sample.size(); ++k)
                        for (unsigned int di = 0; di < difference_index; ++di)
                            this->m_classifiers[difference_values.getIndex(di)][(unsigned int)current_sample.getIndex(k)] += difference_values.getValue(di) * _C(current_sample.getValue(k));
                }
            }
            // ======================== END OF ACTIVE SIZE LOOP ========================
            
            if ((logger != 0) && (iteration % 10 == 0))
                logger->log("Iteration %d: Stopping value=%e, #Samples=%d", iteration, (double)stopping, active_size);
            
            if (stopping < epsilon_shrink)
            {
                if (start_from_all && (stopping < this->m_epsilon)) break;
                else
                {
                    active_size = number_of_train_samples;
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            alpha_active_size[i] = total_alpha_size[i];
                    }
                    epsilon_shrink = srvMax<TCLASSIFIER>(epsilon_shrink / _C(2), this->m_epsilon);
                    start_from_all = true;
                    if (logger != 0)
                        logger->log("Enabling all alpha values again.");
                }
            }
            else start_from_all = false;
        }
        
        if (logger != 0)
        {
            unsigned int non_zero;
            TCLASSIFIER v;
            
            logger->log("Optimization finished in %d iterations.", iteration);
            if (iteration >= this->m_maximum_number_of_iterations)
                logger->log("WARNING: Reaching the maximum number of iterations.");
            
            v = _C(0);
            non_zero = 0;
            
            #pragma omp parallel num_threads(number_of_threads) reduction(+:v) reduction(+:non_zero)
            {
                const unsigned int thread_id = omp_get_thread_num();
                for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                {
                    for (unsigned int j = 0; j < total_alpha_size[i]; ++j)
                    {
                        const unsigned int selected_category = alpha_index[i][j];
                        if (sample_sign[i][selected_category] < 0) v += alpha[i][selected_category];
                        if (srvAbs<TCLASSIFIER>(alpha[i][selected_category]) > _C(0)) ++non_zero;
                    }
                }
            }
            
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                for (NSAMPLE j = 0; j < this->m_number_of_dimensions; ++j)
                    v += this->m_classifiers[i][j] * this->m_classifiers[i][j];
            v *= _C(0.5);
            
            logger->log("Objective value = %lf, non-zero alphas = %d", (double)v, non_zero);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

