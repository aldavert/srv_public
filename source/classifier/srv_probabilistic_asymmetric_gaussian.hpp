// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_ASIMMETRIC_GAUSSIAN_PROBABILISTIC_SCORE_HEADER_FILE__
#define __SRV_ASIMMETRIC_GAUSSIAN_PROBABILISTIC_SCORE_HEADER_FILE__

#include "srv_probabilistic_score.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PROBABILISTIC SCORE USING ASYMMETRIC |
    //                   | GAUSSIAN FITTING                     |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Uses an asymmetric Gaussian distribution to convert classifier scores into probabilities. This
     *  class implements the method proposed by Paul N. Bennett in 'Bennett, P.N., "Using Asymmetric
     *  Distribution to Improve Text Classifier Probability Estimates", SIGIR 2003'.
     */
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticAsymmetricGaussian : public ProbabilisticScoreBase<TSCORE, TPROBABILITY>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticAsymmetricGaussian(void);
        /** Constructor which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        ProbabilisticAsymmetricGaussian(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        /// Copy constructor.
        ProbabilisticAsymmetricGaussian(const ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> &other);
        /// Destructor.
        virtual ~ProbabilisticAsymmetricGaussian(void);
        /// Assignation operator.
        ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>& operator=(const ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> &other);
        /** Function which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] balance boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
         */
        void set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array with the mode of the Gaussian distribution of the negative samples for each classifier.
        inline const double * getNegativeModes(void) const { return m_negative_mode; }
        /// Returns the mode of the Gaussian distribution of the negative samples of the index-th classifier.
        inline double getNegativeMode(unsigned int index) const { return m_negative_mode[index]; }
        /// Sets the mode of the Gaussian distribution of the negative samples of the index-th classifier.
        inline void setNegativeMode(double negative_mode, unsigned int index) { m_negative_mode[index] = negative_mode; }
        /// Returns a constant pointer to the array with the right sigma value of the Gaussian distribution of the negative samples for each classifier.
        inline const double * getNegativeRightSigmas(void) const { return m_negative_right_sigma; }
        /// Returns the right sigma value of the Gaussian distribution of the negative samples of the index-th classifier.
        inline double getNegativeRightSigma(unsigned int index) const { return m_negative_right_sigma[index]; }
        /// Sets the right sigma value of the Gaussian distribution of the negative samples of the index-th classifier.
        inline void getNegativeRightSigma(double negative_right_sigma, unsigned int index) { m_negative_right_sigma[index] = negative_right_sigma; }
        /// Returns a constant pointer to the array with the left sigma value of the Gaussian distribution of the negative samples for each classifier.
        inline const double * getNegativeLeftSigma(void) const { return m_negative_left_sigma; }
        /// Returns the left sigma value of the Gaussian distribution of the negative samples of the index-th classifier.
        inline double getNegativeLeftSigma(unsigned int index) const { return m_negative_left_sigma[index]; }
        /// Sets the left sigma value of the Gaussian distribution of the negative samples of the index-th classifier.
        inline void setNegativeLeftSigma(double negative_left_sigma, unsigned int index) { m_negative_left_sigma[index] = negative_left_sigma; }
        /// Returns a constant pointer to the array with the prior probability of the negative probability.
        inline const double * getNegativePrior(void) const { return m_negative_prior; }
        /// Returns the prior of the negative probability of index-th classifier.
        inline double getNegativePrior(unsigned int index) const { return m_negative_prior[index]; }
        /// Sets the prior of the negative probability of index-th classifier.
        inline void setNegativePrior(double negative_prior, unsigned int index) { m_negative_prior[index] = negative_prior; }
        
        /// Returns a constant pointer to the array with the mode of the Gaussian distribution of the positive samples for each classifier.
        inline const double * getPositiveModes(void) const { return m_positive_mode; }
        /// Returns the mode of the Gaussian distribution of the positive samples of the index-th classifier.
        inline double getPositiveMode(unsigned int index) const { return m_positive_mode[index]; }
        /// Sets the mode of the Gaussian distribution of the positive samples of the index-th classifier.
        inline void setPositiveMode(double positive_mode, unsigned int index) { m_positive_mode[index] = positive_mode; }
        /// Returns a constant pointer to the array with the right sigma value of the Gaussian distribution of the positive samples for each classifier.
        inline const double * getPositiveRightSigmas(void) const { return m_positive_right_sigma; }
        /// Returns the right sigma value of the Gaussian distribution of the positive samples of the index-th classifier.
        inline double getPositiveRightSigma(unsigned int index) const { return m_positive_right_sigma[index]; }
        /// Sets the right sigma value of the Gaussian distribution of the positive samples of the index-th classifier.
        inline void getPositiveRightSigma(double positive_right_sigma, unsigned int index) { m_positive_right_sigma[index] = positive_right_sigma; }
        /// Returns a constant pointer to the array with the left sigma value of the Gaussian distribution of the positive samples for each classifier.
        inline const double * getPositiveLeftSigma(void) const { return m_positive_left_sigma; }
        /// Returns the left sigma value of the Gaussian distribution of the positive samples of the index-th classifier.
        inline double getPositiveLeftSigma(unsigned int index) const { return m_positive_left_sigma[index]; }
        /// Sets the left sigma value of the Gaussian distribution of the positive samples of the index-th classifier.
        inline void setPositiveLeftSigma(double positive_left_sigma, unsigned int index) { m_positive_left_sigma[index] = positive_left_sigma; }
        /// Returns a constant pointer to the array with the prior probability of the positive probability.
        inline const double * getPositivePrior(void) const { return m_positive_prior; }
        /// Returns the prior of the positive probability of index-th classifier.
        inline double getPositivePrior(unsigned int index) const { return m_positive_prior[index]; }
        /// Sets the prior of the positive probability of index-th classifier.
        inline void setPositivePrior(double positive_prior, unsigned int index) { m_positive_prior[index] = positive_prior; }
        
        /// Returns the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline bool getBalance(void) const { return m_balance; }
        /// Sets the value of the boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        inline void setBalance(bool balance) { m_balance = balance; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] score a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        inline ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY> *)new ProbabilisticAsymmetricGaussian(*this); }
        /// Returns the class identifier for the probabilistic score method.
        inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return ASYMMETRIC_GAUSSIAN_ALGORITHM; }
        /// Generates a new empty instance of the probabilistic score object.
        inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new ProbabilisticAsymmetricGaussian(); }
        /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        inline PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const { return ASYMMETRIC_GAUSSIAN_ALGORITHM; }
        
    protected:
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        void attributesToXML(XmlParser &parser) const;
        /// Stores the probabilistic object information into the data of the XML object.
        void dataToXML(XmlParser &parser) const;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        void freeDataXML(void);
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        void attributesFromXML(XmlParser &parser);
        /// Retrieves the probabilistic object information into the data of the XML object.
        bool dataFromXML(XmlParser &parser);
        
        // -[ Other protected functions ]------------------------------------------------------------------------------------------------------------
        /** Calculates the parameters of the Gaussian Asymmetric model.
         *  \param[in] scores array with the sorted scores of the positive or negative samples.
         *  \param[in] number_of_scores number of values in the scores array.
         *  \param[in] minimum_theta minimum mode of the Gaussian distribution evaluated.
         *  \param[in] maximum_theta maximum mode of the Gaussian distribution evaluated.
         *  \param[in] num_interval_slices number of divisions between consecutive scores.
         *  \param[out] final_theta resulting mode of the Gaussian distribution.
         *  \param[out] final_left_sigma resulting left sigma value of the Gaussian distribution.
         *  \param[out] final_right_sigma resulting right sigma value of the Gaussian distribution.
         */
        void modelParameters(const TSCORE * current_scores, unsigned int number_of_scores, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_left_sigma, double &final_right_sigma);
        /** Calculates the parameters of the Gaussian Asymmetric model for data stored into a histogram.
         *  \param[in] histogram histogram of scores.
         *  \param[in] bin_scores vector with the scores associated to each histogram bin.
         *  \param[in] scores_factor multiplier factor applied to the number of scores.
         *  \param[in] minimum_theta minimum mode of the Gaussian distribution evaluated.
         *  \param[in] maximum_theta maximum mode of the Gaussian distribution evaluated.
         *  \param[in] num_interval_slices number of divisions between consecutive scores.
         *  \param[out] final_theta resulting mode of the Gaussian distribution.
         *  \param[out] final_left_sigma resulting left sigma value of the Gaussian distribution.
         *  \param[out] final_right_sigma resulting right sigma value of the Gaussian distribution.
         */
        void modelParameters(const VectorDense<unsigned int, unsigned int> &histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, double scores_factor, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_left_sigma, double &final_right_sigma);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Array with the mode of the Gaussian distribution of the negative samples for each classifier.
        double * m_negative_mode;
        /// Array with the right sigma value of the Gaussian distribution of the negative samples for each classifier.
        double * m_negative_right_sigma;
        /// Array with the left sigma value of the Gaussian distribution of the negative samples for each classifier.
        double * m_negative_left_sigma;
        /// Array with the prior probability of the negative probability.
        double * m_negative_prior;
        /// Array with the mode of the Gaussian distribution of the positive samples for each classifier.
        double * m_positive_mode;
        /// Array with the right sigma value of the Gaussian distribution of the positive samples for each classifier.
        double * m_positive_right_sigma;
        /// Array with the left sigma value of the Gaussian distribution of the positive samples for each classifier.
        double * m_positive_left_sigma;
        /// Array with the prior probability of the positive probability.
        double * m_positive_prior;
        /// Boolean flag which when true allows to balance the positive and negative samples while creating the probabilistic model.
        bool m_balance;
        
        /// Static integer which indicates if the class has been initialized in the probability score factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PROBABILISTIC SCORE USING ASYMMETRIC |
    //                   | GAUSSIAN FITTING                     |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ACCESS FUNCTIONS ]============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricGaussian(void) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(),
        m_negative_mode(0),
        m_negative_right_sigma(0),
        m_negative_left_sigma(0),
        m_negative_prior(0),
        m_positive_mode(0),
        m_positive_right_sigma(0),
        m_positive_left_sigma(0),
        m_positive_prior(0),
        m_balance(false)
    {}
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricGaussian(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(number_of_categories, multiclass, probability_factor),
        m_negative_mode((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_right_sigma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_left_sigma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_negative_prior((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_mode((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_right_sigma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_left_sigma((number_of_categories > 0)?new double[number_of_categories]:0),
        m_positive_prior((number_of_categories > 0)?new double[number_of_categories]:0),
        m_balance(balance)
    {
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::ProbabilisticAsymmetricGaussian(const ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> &other) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(other),
        m_negative_mode((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_right_sigma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_left_sigma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_negative_prior((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_mode((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_right_sigma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_left_sigma((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_positive_prior((other.m_number_of_categories > 0)?new double[other.m_number_of_categories]:0),
        m_balance(other.m_balance)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_negative_mode[i] = other.m_negative_mode[i];
            m_negative_right_sigma[i] = other.m_negative_right_sigma[i];
            m_negative_left_sigma[i] = other.m_negative_left_sigma[i];
            m_negative_prior[i] = other.m_negative_prior[i];
            m_positive_mode[i] = other.m_positive_mode[i];
            m_positive_right_sigma[i] = other.m_positive_right_sigma[i];
            m_positive_left_sigma[i] = other.m_positive_left_sigma[i];
            m_positive_prior[i] = other.m_positive_prior[i];
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::~ProbabilisticAsymmetricGaussian(void)
    {
        if (m_negative_mode != 0) delete [] m_negative_mode;
        if (m_negative_right_sigma != 0) delete [] m_negative_right_sigma;
        if (m_negative_left_sigma != 0) delete [] m_negative_left_sigma;
        if (m_negative_prior != 0) delete [] m_negative_prior;
        if (m_positive_mode != 0) delete [] m_positive_mode;
        if (m_positive_right_sigma != 0) delete [] m_positive_right_sigma;
        if (m_positive_left_sigma != 0) delete [] m_positive_left_sigma;
        if (m_positive_prior != 0) delete [] m_positive_prior;
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>& ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::operator=(const ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY> &other)
    {
        if (this != &other)
        {
            if (m_negative_mode != 0) delete [] m_negative_mode;
            if (m_negative_right_sigma != 0) delete [] m_negative_right_sigma;
            if (m_negative_left_sigma != 0) delete [] m_negative_left_sigma;
            if (m_negative_prior != 0) delete [] m_negative_prior;
            if (m_positive_mode != 0) delete [] m_positive_mode;
            if (m_positive_right_sigma != 0) delete [] m_positive_right_sigma;
            if (m_positive_left_sigma != 0) delete [] m_positive_left_sigma;
            if (m_positive_prior != 0) delete [] m_positive_prior;
            
            ProbabilisticScoreBase<TSCORE, TPROBABILITY>::operator=(other);
            m_balance = other.m_balance;
            if (other.m_number_of_categories > 0)
            {
                m_negative_mode = new double[other.m_number_of_categories];
                m_negative_right_sigma = new double[other.m_number_of_categories];
                m_negative_left_sigma = new double[other.m_number_of_categories];
                m_negative_prior = new double[other.m_number_of_categories];
                m_positive_mode = new double[other.m_number_of_categories];
                m_positive_right_sigma = new double[other.m_number_of_categories];
                m_positive_left_sigma = new double[other.m_number_of_categories];
                m_positive_prior = new double[other.m_number_of_categories];
                
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_negative_mode[i] = other.m_negative_mode[i];
                    m_negative_right_sigma[i] = other.m_negative_right_sigma[i];
                    m_negative_left_sigma[i] = other.m_negative_left_sigma[i];
                    m_negative_prior[i] = other.m_negative_prior[i];
                    m_positive_mode[i] = other.m_positive_mode[i];
                    m_positive_right_sigma[i] = other.m_positive_right_sigma[i];
                    m_positive_left_sigma[i] = other.m_positive_left_sigma[i];
                    m_positive_prior[i] = other.m_positive_prior[i];
                }
            }
            else
            {
                m_negative_mode = 0;
                m_negative_right_sigma = 0;
                m_negative_left_sigma = 0;
                m_negative_prior = 0;
                m_positive_mode = 0;
                m_positive_right_sigma = 0;
                m_positive_left_sigma = 0;
                m_positive_prior = 0;
            }
        }
        
        return *this;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::set(unsigned int number_of_categories, bool multiclass, double probability_factor, bool balance)
    {
        if (m_negative_mode != 0) delete [] m_negative_mode;
        if (m_negative_right_sigma != 0) delete [] m_negative_right_sigma;
        if (m_negative_left_sigma != 0) delete [] m_negative_left_sigma;
        if (m_negative_prior != 0) delete [] m_negative_prior;
        if (m_positive_mode != 0) delete [] m_positive_mode;
        if (m_positive_right_sigma != 0) delete [] m_positive_right_sigma;
        if (m_positive_left_sigma != 0) delete [] m_positive_left_sigma;
        if (m_positive_prior != 0) delete [] m_positive_prior;
        
        this->setBase(number_of_categories, multiclass, probability_factor);
        m_balance = balance;
        m_negative_mode = new double[number_of_categories];
        m_negative_right_sigma = new double[number_of_categories];
        m_negative_left_sigma = new double[number_of_categories];
        m_negative_prior = new double[number_of_categories];
        m_positive_mode = new double[number_of_categories];
        m_positive_right_sigma = new double[number_of_categories];
        m_positive_left_sigma = new double[number_of_categories];
        m_positive_prior = new double[number_of_categories];
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::attributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Balance", m_balance);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::attributesFromXML(XmlParser &parser)
    {
        m_balance = parser.getAttribute("Balance");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::dataToXML(XmlParser &parser) const
    {
        saveVector(parser, "Negative_Modes", ConstantSubVectorDense<double, unsigned int>(m_negative_mode, this->m_number_of_categories));
        saveVector(parser, "Negative_Right_Sigma", ConstantSubVectorDense<double, unsigned int>(m_negative_right_sigma, this->m_number_of_categories));
        saveVector(parser, "Negative_Left_Sigma", ConstantSubVectorDense<double, unsigned int>(m_negative_left_sigma, this->m_number_of_categories));
        saveVector(parser, "Negative_Prior", ConstantSubVectorDense<double, unsigned int>(m_negative_prior, this->m_number_of_categories));
        saveVector(parser, "Positive_Modes", ConstantSubVectorDense<double, unsigned int>(m_positive_mode, this->m_number_of_categories));
        saveVector(parser, "Positive_Right_Sigma", ConstantSubVectorDense<double, unsigned int>(m_positive_right_sigma, this->m_number_of_categories));
        saveVector(parser, "Positive_Left_Sigma", ConstantSubVectorDense<double, unsigned int>(m_positive_left_sigma, this->m_number_of_categories));
        saveVector(parser, "Positive_Prior", ConstantSubVectorDense<double, unsigned int>(m_positive_prior, this->m_number_of_categories));
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::freeDataXML(void)
    {
        if (m_negative_mode != 0) { delete [] m_negative_mode; m_negative_mode = 0; }
        if (m_negative_right_sigma != 0) { delete [] m_negative_right_sigma; m_negative_right_sigma = 0; }
        if (m_negative_left_sigma != 0) { delete [] m_negative_left_sigma; m_negative_left_sigma = 0; }
        if (m_negative_prior != 0) { delete [] m_negative_prior; m_negative_prior = 0; }
        if (m_positive_mode != 0) { delete [] m_positive_mode; m_positive_mode = 0; }
        if (m_positive_right_sigma != 0) { delete [] m_positive_right_sigma; m_positive_right_sigma = 0; }
        if (m_positive_left_sigma != 0) { delete [] m_positive_left_sigma; m_positive_left_sigma = 0; }
        if (m_positive_prior != 0) { delete [] m_positive_prior; m_positive_prior = 0; }
    }
    
    template <class TSCORE, class TPROBABILITY>
    bool ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::dataFromXML(XmlParser &parser)
    {
        VectorDense<double, unsigned int> data;
        
        if (parser.isTagIdentifier("Negative_Modes"))
        {
            loadVector(parser, "Negative_Modes", data);
            m_negative_mode = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_mode[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Right_Sigma"))
        {
            loadVector(parser, "Negative_Right_Sigma", data);
            m_negative_right_sigma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_right_sigma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Left_Sigma"))
        {
            loadVector(parser, "Negative_Left_Sigma", data);
            m_negative_left_sigma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_left_sigma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Negative_Prior"))
        {
            loadVector(parser, "Negative_Prior", data);
            m_negative_prior = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_negative_prior[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Modes"))
        {
            loadVector(parser, "Positive_Modes", data);
            m_positive_mode = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_mode[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Right_Sigma"))
        {
            loadVector(parser, "Positive_Right_Sigma", data);
            m_positive_right_sigma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_right_sigma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Left_Sigma"))
        {
            loadVector(parser, "Positive_Left_Sigma", data);
            m_positive_left_sigma = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_left_sigma[i] = data[i];
            return true;
        }
        else if (parser.isTagIdentifier("Positive_Prior"))
        {
            loadVector(parser, "Positive_Prior", data);
            m_positive_prior = new double[this->m_number_of_categories];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_positive_prior[i] = data[i];
            return true;
        }
        else return false;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN AND RESCALE FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::modelParameters(const TSCORE * current_scores, unsigned int number_of_scores, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_left_sigma, double &final_right_sigma)
    {
        double max_ln_posterior, ln_posterior, prev_score_theta, theta, left_sigma, right_sigma, sum_right;
        double sum_left, diff_right, diff_left, diff_right_3root, diff_left_3root, sum_squares_right;
        double sum_squares_left;
        unsigned int num_right, num_left, slice_num, i;
        bool max_init;
        
        max_ln_posterior = 0.0;
        max_init = false;
        prev_score_theta = minimum_theta;
        theta = prev_score_theta;
        num_left = 0;
        num_right = number_of_scores;
        sum_right = 0.0;
        sum_left = 0.0;
        slice_num = 0;
        sum_squares_left = 0.0;
        sum_squares_right = 0.0;
        
        for (i = 0; i < number_of_scores; ++i)
        {
            sum_right += (double)current_scores[i];
            sum_squares_right += (double)current_scores[i] * (double)current_scores[i];
        }
        i = 0;
        while (true)
        {
            double next;
            
            // Update sufficient statistics.
            for (; (i < number_of_scores) && ((double)current_scores[i] <= theta); ++i)
            {
                const double score_square = (double)current_scores[i] * (double)current_scores[i];
                ++num_left;
                --num_right;
                sum_left += (double)current_scores[i];
                sum_right -= (double)current_scores[i];
                sum_squares_left += score_square;
                sum_squares_right -= score_square;
            }
            diff_left = sum_squares_left - 2 * sum_left * theta + theta * theta * (double)num_left;
            diff_right = sum_squares_right - 2 * sum_right * theta + theta * theta * (double)num_right;
            diff_left_3root = cbrt(diff_left);
            diff_right_3root = cbrt(diff_right);
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            ////// std::cout << "A) Sum_Right=" << sum_right << " Sum_Left=" << sum_left << " Diff_Right=" << diff_right << " Diff_Left=" << diff_left << std::endl;
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            
            // Compute left_sigma.
            if (diff_left == 0) left_sigma = 1e-10;
            else left_sigma = sqrt(diff_left + diff_left_3root * diff_left_3root * diff_right_3root) / sqrt((double)number_of_scores);
            // Compute right_sigma.
            if (diff_right == 0) right_sigma = 1e-10;
            else right_sigma = sqrt(diff_right + diff_right_3root * diff_right_3root * diff_left_3root) / sqrt((double)number_of_scores);
            
            // Compute log posterior.
            ln_posterior = (double)number_of_scores * (-log(left_sigma + right_sigma)) - 0.5 * diff_left / (left_sigma * left_sigma) - 0.5 * diff_right / (right_sigma * right_sigma);
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            ////// std::cout << "B) Theta=" << theta << " Left Sigma=" << left_sigma << " Right Sigma=" << right_sigma << " Ln_Posterior=" << ln_posterior << std::endl;
            ////// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
            
            // Update set of best parameters.
            if (max_init)
            {
                if (ln_posterior > max_ln_posterior)
                {
                    final_theta = theta;
                    final_left_sigma = left_sigma;
                    final_right_sigma = right_sigma;
                    max_ln_posterior = ln_posterior;
                    ///// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
                    ///// std::cout << "NEW MAXIMUM FOUND:" << std::endl;
                    ///// std::cout << "  * THETA: " << final_theta << std::endl;
                    ///// std::cout << "  * LEFT SIGMA: " << final_left_sigma << std::endl;
                    ///// std::cout << "  * RIGHT SIGMA: " << final_right_sigma << std::endl;
                    ///// // DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG-DEBUG
                }
            }
            else
            {
                final_theta = theta;
                final_left_sigma = left_sigma;
                final_right_sigma = right_sigma;
                max_ln_posterior = ln_posterior;
                max_init = true;
            }
            
            // Get new choice for theta.
            if (theta == maximum_theta) break;
            
            next = maximum_theta;
            if (i != number_of_scores)
                next = current_scores[i];
            ++slice_num;
            if ((slice_num % num_interval_slices) == 0)
            {
                prev_score_theta = next;
                theta = prev_score_theta;
                slice_num = 0;
            }
            else theta = slice_num * ((next - prev_score_theta) / (double)num_interval_slices) + prev_score_theta;
            if (theta > maximum_theta) break;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::modelParameters(const VectorDense<unsigned int, unsigned int> &histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, double scores_factor, double minimum_theta, double maximum_theta, unsigned int num_interval_slices, double &final_theta, double &final_left_sigma, double &final_right_sigma)
    {
        const unsigned int number_of_bins = bin_scores.size();
        double max_ln_posterior, ln_posterior, prev_score_theta, theta, left_sigma, right_sigma, sum_right;
        double sum_left, diff_right, diff_left, diff_right_3root, diff_left_3root, sum_squares_right;
        double sum_squares_left, number_of_scores, num_right, num_left;
        unsigned int slice_num;
        bool max_init;
        
        number_of_scores = 0.0;
        for (unsigned int b = 0; b < number_of_bins; ++b)
            number_of_scores += (double)histogram[b];
        number_of_scores *= scores_factor;
        
        max_ln_posterior = 0.0;
        max_init = false;
        prev_score_theta = minimum_theta;
        theta = prev_score_theta;
        num_left = 0.0;
        num_right = number_of_scores;
        sum_right = 0.0;
        sum_left = 0.0;
        sum_squares_left = 0.0;
        sum_squares_right = 0.0;
        slice_num = 0;
        
        for (unsigned int b = 0; b < number_of_bins; ++b)
        {
            const double current_factor = scores_factor * (double)histogram[b];
            
            sum_right += current_factor * bin_scores[b];
            sum_squares_right += current_factor * bin_scores[b] * bin_scores[b];
        }
        
        for (unsigned int b = 0; true;)
        {
            double next;
            
            // Update sufficient statistics .........................................................................................................
            for (; (b < number_of_bins) && ((double)bin_scores[b] <= theta); ++b)
            {
                const double current_factor = scores_factor * (double)histogram[b];
                const double score_square = (double)bin_scores[b] * (double)bin_scores[b];
                
                num_left  = srvMin(number_of_scores,  num_left + current_factor);
                num_right = srvMax(             0.0, num_right - current_factor);
                sum_left  += (double)bin_scores[b] * current_factor;
                sum_right -= (double)bin_scores[b] * current_factor;
                sum_squares_left  += current_factor * score_square;
                sum_squares_right -= current_factor * score_square;
            }
            diff_left  = sum_squares_left  - 2 * sum_left  * theta + theta * theta * num_left;
            diff_right = sum_squares_right - 2 * sum_right * theta + theta * theta * num_right;
            diff_left_3root  = cbrt(diff_left);
            diff_right_3root = cbrt(diff_right);
            
            // Compute left, right sigmas and log posterior .........................................................................................
            left_sigma  = (diff_left  == 0)?1e-10:(std::sqrt(diff_left  + diff_left_3root  * diff_left_3root  * diff_right_3root) / std::sqrt(number_of_scores));
            right_sigma = (diff_right == 0)?1e-10:(std::sqrt(diff_right + diff_right_3root * diff_right_3root * diff_left_3root ) / std::sqrt(number_of_scores));
            ln_posterior = number_of_scores * (-std::log(left_sigma + right_sigma)) - 0.5 * diff_left / (left_sigma * left_sigma) - 0.5 * diff_right / (right_sigma * right_sigma);
            
            // Update set of best parameters ........................................................................................................
            if (max_init)
            {
                if (ln_posterior > max_ln_posterior)
                {
                    final_theta = theta;
                    final_left_sigma = left_sigma;
                    final_right_sigma = right_sigma;
                    max_ln_posterior = ln_posterior;
                }
            }
            else
            {
                final_theta = theta;
                final_left_sigma = left_sigma;
                final_right_sigma = right_sigma;
                max_ln_posterior = ln_posterior;
                max_init = true;
            }
            
            // Get new choice for theta .............................................................................................................
            if (theta == maximum_theta) break;
            
            next = (b != number_of_bins)?bin_scores[b]:maximum_theta;
            ++slice_num;
            if ((slice_num % num_interval_slices) == 0)
            {
                prev_score_theta = next;
                theta = prev_score_theta;
                slice_num = 0;
            }
            else theta = slice_num * ((next - prev_score_theta) / (double)num_interval_slices) + prev_score_theta;
            if (theta > maximum_theta) break;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            unsigned int number_of_positive_scores, number_of_negative_scores;
            const unsigned int thread_id = omp_get_thread_num();
            TSCORE * positive_negative_scores;
            Tuple<TSCORE, bool> * current_scores;
            
            current_scores = new Tuple<TSCORE, bool>[number_of_samples];
            
            for (unsigned int category = thread_id; category < this->m_number_of_categories; category += number_of_threads)
            {
                unsigned int maximum_number_of_scores;
                if ((logger != 0) && (thread_id == 0))
                    logger->log("Calibrating the classifier %d of %d.", category + 1, this->m_number_of_categories);
                
                number_of_positive_scores = number_of_negative_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    bool current_positive;
                    
                    current_positive = false;
                    for (unsigned int l = 0; (!current_positive) && (l < label[i].size()); ++l)
                        current_positive = (label[i][l] == category);
                    
                    current_scores[i].setData(score[i][category], current_positive);
                    if (current_positive) ++number_of_positive_scores;
                    else ++number_of_negative_scores;
                }
                std::sort(current_scores, current_scores + number_of_samples);
                m_positive_prior[category] = (double)number_of_positive_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                m_negative_prior[category] = (double)number_of_negative_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                
                maximum_number_of_scores = srvMax<unsigned int>(number_of_positive_scores, number_of_negative_scores);
                positive_negative_scores = new TSCORE[maximum_number_of_scores];
                
                number_of_positive_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    if (current_scores[i].getSecond())
                    {
                        positive_negative_scores[number_of_positive_scores] = current_scores[i].getFirst();
                        ++number_of_positive_scores;
                    }
                }
                if (m_balance && (number_of_positive_scores < maximum_number_of_scores))
                {
                    unsigned int index;
                    for (index = number_of_positive_scores; index < maximum_number_of_scores; ++index)
                        positive_negative_scores[index] = positive_negative_scores[index % number_of_positive_scores];
                    number_of_positive_scores = maximum_number_of_scores;
                    std::sort(positive_negative_scores, positive_negative_scores + maximum_number_of_scores);
                }
                modelParameters(positive_negative_scores, number_of_positive_scores, (double)positive_negative_scores[0], (double)positive_negative_scores[number_of_positive_scores - 1], 10, m_positive_mode[category], m_positive_left_sigma[category], m_positive_right_sigma[category]);
                
                number_of_negative_scores = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    if (!current_scores[i].getSecond())
                    {
                        positive_negative_scores[number_of_negative_scores] = current_scores[i].getFirst();
                        ++number_of_negative_scores;
                    }
                }
                if (m_balance && (number_of_negative_scores < maximum_number_of_scores))
                {
                    unsigned int index;
                    for (index = number_of_negative_scores; index < maximum_number_of_scores; ++index)
                        positive_negative_scores[index] = positive_negative_scores[index % number_of_negative_scores];
                    number_of_negative_scores = maximum_number_of_scores;
                    std::sort(positive_negative_scores, positive_negative_scores + maximum_number_of_scores);
                }
                modelParameters(positive_negative_scores, number_of_negative_scores, (double)positive_negative_scores[0], (double)positive_negative_scores[number_of_negative_scores - 1], 10, m_negative_mode[category], m_negative_left_sigma[category], m_negative_right_sigma[category]);
                if ((logger != 0) && (thread_id == 0))
                {
                    logger->log("Parameters of the Gaussian distribution over the POSITIVE samples:");
                    logger->log(" · Mode: %f", m_positive_mode[category]);
                    logger->log(" · Right Sigma: %f", m_positive_right_sigma[category]);
                    logger->log(" · Left Sigma: %f", m_positive_left_sigma[category]);
                    logger->log("Parameters of the Gaussian distribution over the NEGATIVE samples:");
                    logger->log(" · Mode: %f", m_negative_mode[category]);
                    logger->log(" · Right Sigma: %f", m_negative_right_sigma[category]);
                    logger->log(" · Left Sigma: %f", m_negative_left_sigma[category]);
                    logger->log("Prior probabilities");
                    logger->log("Positive: %f", m_positive_prior[category]);
                    logger->log("Negative: %f", m_negative_prior[category]);
                }
                
                delete [] positive_negative_scores;
            }
            
            delete [] current_scores;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (logger != 0) logger->log("Fitting the sigmoid functions.");
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int number_of_bins = bin_scores.size();
            unsigned int number_of_positive_scores, number_of_negative_scores;
            
            for (unsigned int category = omp_get_thread_num(); category < this->m_number_of_categories; category += number_of_threads)
            {
                double scores_factor, maximum_number_of_scores;
                number_of_positive_scores = number_of_negative_scores = 0;
                for (unsigned int b = 0; b < number_of_bins; ++b)
                {
                    number_of_positive_scores += positive_histogram[category][b];
                    number_of_negative_scores += negative_histogram[category][b];
                }
                m_positive_prior[category] = (double)number_of_positive_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                m_negative_prior[category] = (double)number_of_negative_scores / (double)(number_of_positive_scores + number_of_negative_scores);
                maximum_number_of_scores = (double)srvMax<unsigned int>(number_of_positive_scores, number_of_negative_scores);
                
                scores_factor = (m_balance)?((double)maximum_number_of_scores / (double)number_of_positive_scores):1.0;
                modelParameters(positive_histogram[category], bin_scores, scores_factor, bin_scores[0], bin_scores[number_of_bins - 1], 10, m_positive_mode[category], m_positive_left_sigma[category], m_positive_right_sigma[category]);
                scores_factor = (m_balance)?((double)maximum_number_of_scores / (double)number_of_negative_scores):1.0;
                modelParameters(negative_histogram[category], bin_scores, scores_factor, bin_scores[0], bin_scores[number_of_bins - 1], 10, m_negative_mode[category], m_negative_left_sigma[category], m_negative_right_sigma[category]);
                
                if ((logger != 0) && (category % number_of_threads == 0))
                {
                    logger->log("Parameters of the Gaussian distribution over the POSITIVE samples:");
                    logger->log(" · Mode: %f", m_positive_mode[category]);
                    logger->log(" · Right Sigma: %f", m_positive_right_sigma[category]);
                    logger->log(" · Left Sigma: %f", m_positive_left_sigma[category]);
                    logger->log("Parameters of the Gaussian distribution over the NEGATIVE samples:");
                    logger->log(" · Mode: %f", m_negative_mode[category]);
                    logger->log(" · Right Sigma: %f", m_negative_right_sigma[category]);
                    logger->log(" · Left Sigma: %f", m_negative_left_sigma[category]);
                    logger->log("Prior probabilities");
                    logger->log("Positive: %f", m_positive_prior[category]);
                    logger->log("Negative: %f", m_negative_prior[category]);
                }
            }
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<TPROBABILITY> constant_positive(this->m_number_of_categories), constant_negative(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            constant_positive[i] = 2.0 / (sqrt(2.0 * 3.1415926) * (m_positive_left_sigma[i] + m_positive_right_sigma[i]));
            constant_negative[i] = 2.0 / (sqrt(2.0 * 3.1415926) * (m_negative_left_sigma[i] + m_negative_right_sigma[i]));
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
            {
                if ((logger != 0) && (thread_id == 0) && (i % (10 * number_of_threads) == 0))
                    logger->log("Processing the sample %d of %d.", i + 1, number_of_samples);
                probability[i].set(this->m_number_of_categories);
                
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                {
                    double positive_probability, negative_probability, difference;
                    
                    difference = (double)score[i][c] - m_positive_mode[c];
                    if (score[i][c] <= m_positive_mode[c])
                        positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_left_sigma[c] * m_positive_left_sigma[c]));
                    else positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_right_sigma[c] * m_positive_right_sigma[c]));
                    
                    difference = (double)score[i][c] - m_negative_mode[c];
                    if (score[i][c] <= m_negative_mode[c])
                        negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_left_sigma[c] * m_negative_left_sigma[c]));
                    else negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_right_sigma[c] * m_negative_right_sigma[c]));
                    
                    ///// std::cout << "Score: " << score[i][c] << std::endl;
                    ///// std::cout << "Positive Probability: " << positive_probability << std::endl;
                    ///// std::cout << "Negative Probability: " << negative_probability << std::endl;
                    probability[i][c] = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                    ///// std::cout << "Joint probability: " << probability[i][c] << std::endl;
                }
            }
        }
        
        if (this->m_multiclass)
        {
            if (logger != 0) logger->log("Converting probabilities into multi-class estimates.");
            this->protectedMulticlass(probability, number_of_samples, number_of_threads);
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        VectorDense<TPROBABILITY> constant_positive(this->m_number_of_categories), constant_negative(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            constant_positive[i] = 2.0 / (sqrt(2.0 * 3.1415926) * (m_positive_left_sigma[i] + m_positive_right_sigma[i]));
            constant_negative[i] = 2.0 / (sqrt(2.0 * 3.1415926) * (m_negative_left_sigma[i] + m_negative_right_sigma[i]));
        }
        
        if ((void*)&score == (void*)&probability)
        {
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        TPROBABILITY * __restrict__ score_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr)
                        {
                            double positive_probability, negative_probability, difference;
                            
                            difference = (double)*score_ptr - m_positive_mode[c];
                            if (*score_ptr <= m_positive_mode[c])
                                positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_left_sigma[c] * m_positive_left_sigma[c]));
                            else positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_right_sigma[c] * m_positive_right_sigma[c]));
                            
                            difference = (double)*score_ptr - m_negative_mode[c];
                            if (*score_ptr <= m_negative_mode[c])
                                negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_left_sigma[c] * m_negative_left_sigma[c]));
                            else negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_right_sigma[c] * m_negative_right_sigma[c]));
                            
                            *score_ptr = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                        }
                    }
                }
            }
        }
        else
        {
            probability.setGeometry(score);
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        const TSCORE * __restrict__ score_ptr = score.get(y, c);
                        TPROBABILITY * __restrict__ prob_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr, ++prob_ptr)
                        {
                            double positive_probability, negative_probability, difference;
                            
                            difference = (double)*score_ptr - m_positive_mode[c];
                            if (*score_ptr <= m_positive_mode[c])
                                positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_left_sigma[c] * m_positive_left_sigma[c]));
                            else positive_probability = constant_positive[c] * std::exp(-difference * difference / (2 * m_positive_right_sigma[c] * m_positive_right_sigma[c]));
                            
                            difference = (double)*score_ptr - m_negative_mode[c];
                            if (*score_ptr <= m_negative_mode[c])
                                negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_left_sigma[c] * m_negative_left_sigma[c]));
                            else negative_probability = constant_negative[c] * std::exp(-difference * difference / (2 * m_negative_right_sigma[c] * m_negative_right_sigma[c]));
                            
                            *prob_ptr = (TPROBABILITY)(this->m_probability_factor * positive_probability * m_positive_prior[c] / (positive_probability * m_positive_prior[c] + negative_probability * m_negative_prior[c]));
                        }
                    }
                }
            }
        }
        
        if (this->m_multiclass) this->protectedMulticlass(probability, number_of_threads);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticAsymmetricGaussian<TSCORE, TPROBABILITY>::scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const
    {
        VectorDense<double> values(this->m_number_of_categories);
        probability = srvMin<double>(1.0 - 1e-12, srvMax<double>(1e-12, probability));
        score.set(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            double constant_positive, constant_negative;
            
            constant_positive = srvMax(1e-10, m_positive_prior[i] * (1.0 - probability) * (2.0 / (sqrt(2.0 * 3.1415926) * (m_positive_left_sigma[i] + m_positive_right_sigma[i]))));
            constant_negative = srvMax(1e-10, m_negative_prior[i] * probability * (2.0 / (sqrt(2.0 * 3.1415926) * (m_negative_left_sigma[i] + m_negative_right_sigma[i]))));
            values[i] = log(constant_negative) - log(constant_positive);
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < this->m_number_of_categories; i += number_of_threads)
            {
                double a, b, c, rx, x[8], sigma_n, sigma_p, det;
                
                // First case .......................................................................................................................
                sigma_p = m_positive_left_sigma[i] * m_positive_left_sigma[i];
                sigma_n = m_negative_left_sigma[i] * m_negative_left_sigma[i];
                a = sigma_p - sigma_n;
                b = 2 * sigma_n * m_positive_mode[i] - 2 * sigma_p * m_negative_mode[i];
                c = -values[i] + (sigma_p * m_negative_mode[i] * m_negative_mode[i] - sigma_n * m_positive_mode[i] * m_positive_mode[i]);
                det = b * b - 4 * a * c;
                if (det >= 0)
                {
                    rx = (-b + sqrt(det)) / (2 * a);
                    if ((rx <= m_positive_mode[i]) && (rx <= m_negative_mode[i])) x[0] = rx;
                    else x[0] = std::numeric_limits<double>::infinity();
                    rx = (-b - sqrt(det)) / (2 * a);
                    if ((rx <= m_positive_mode[i]) && (rx <= m_negative_mode[i])) x[1] = rx;
                    else x[1] = std::numeric_limits<double>::infinity();
                }
                else x[0] = x[1] = std::numeric_limits<double>::infinity();
                // Second case ......................................................................................................................
                if (m_positive_mode[i] > m_negative_mode[i])
                {
                    sigma_p = m_positive_left_sigma[i] * m_positive_left_sigma[i];
                    sigma_n = m_negative_right_sigma[i] * m_negative_right_sigma[i];
                    a = sigma_p - sigma_n;
                    b = 2 * sigma_n * m_positive_mode[i] - 2 * sigma_p * m_negative_mode[i];
                    c = -values[i] + (sigma_p * m_negative_mode[i] * m_negative_mode[i] - sigma_n * m_positive_mode[i] * m_positive_mode[i]);
                    det = b * b - 4 * a * c;
                    if (det >= 0)
                    {
                        rx = (-b + sqrt(det)) / (2 * a);
                        if ((rx <= m_positive_mode[i]) && (rx > m_negative_mode[i])) x[2] = rx;
                        else x[2] = std::numeric_limits<double>::infinity();
                        rx = (-b - sqrt(det)) / (2 * a);
                        if ((rx <= m_positive_mode[i]) && (rx > m_negative_mode[i])) x[3] = rx;
                        else x[3] = std::numeric_limits<double>::infinity();
                    }
                    else x[2] = x[3] = std::numeric_limits<double>::infinity();
                }
                else x[2] = x[3] = std::numeric_limits<double>::infinity();
                // Third case .......................................................................................................................
                if (m_positive_mode[i] < m_negative_mode[i])
                {
                    sigma_p = m_positive_right_sigma[i] * m_positive_right_sigma[i];
                    sigma_n = m_negative_left_sigma[i] * m_negative_left_sigma[i];
                    a = sigma_p - sigma_n;
                    b = 2 * sigma_n * m_positive_mode[i] - 2 * sigma_p * m_negative_mode[i];
                    c = -values[i] + (sigma_p * m_negative_mode[i] * m_negative_mode[i] - sigma_n * m_positive_mode[i] * m_positive_mode[i]);
                    det = b * b - 4 * a * c;
                    if (det >= 0)
                    {
                        rx = (-b + sqrt(det)) / (2 * a);
                        if ((rx > m_positive_mode[i]) && (rx <= m_negative_mode[i])) x[4] = rx;
                        else x[4] = std::numeric_limits<double>::infinity();
                        rx = (-b - sqrt(det)) / (2 * a);
                        if ((rx > m_positive_mode[i]) && (rx <= m_negative_mode[i])) x[5] = rx;
                        else x[5] = std::numeric_limits<double>::infinity();
                    }
                    else x[4] = x[5] = std::numeric_limits<double>::infinity();
                }
                else x[4] = x[5] = std::numeric_limits<double>::infinity();
                // Fourth case ......................................................................................................................
                sigma_p = m_positive_right_sigma[i] * m_positive_right_sigma[i];
                sigma_n = m_negative_right_sigma[i] * m_negative_right_sigma[i];
                a = sigma_p - sigma_n;
                b = 2 * sigma_n * m_positive_mode[i] - 2 * sigma_p * m_negative_mode[i];
                c = -values[i] + (sigma_p * m_negative_mode[i] * m_negative_mode[i] - sigma_n * m_positive_mode[i] * m_positive_mode[i]);
                det = b * b - 4 * a * c;
                if (det >= 0)
                {
                    rx = (-b + sqrt(det)) / (2 * a);
                    if ((rx > m_positive_mode[i]) && (rx > m_negative_mode[i])) x[6] = rx;
                    else x[6] = std::numeric_limits<double>::infinity();
                    rx = (-b - sqrt(det)) / (2 * a);
                    if ((rx > m_positive_mode[i]) && (rx > m_negative_mode[i])) x[7] = rx;
                    else x[7] = std::numeric_limits<double>::infinity();
                }
                else x[6] = x[7] = std::numeric_limits<double>::infinity();
                
                score[i] = x[0];
                for (unsigned int k = 0; k < 8; ++k)
                {
                    if (srvAbs<double>(x[k]) < srvAbs<double>(score[i]))
                        score[i] = x[k];
                }
                if (score[i] == std::numeric_limits<double>::infinity())
                {
                    if (probability < 0.5) score[i] = m_negative_mode[i];
                    else score[i] = m_positive_mode[i];
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

