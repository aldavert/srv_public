// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_L1_REGULARIZED_LOGISTIC_REGRESSION_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_L1_REGULARIZED_LOGISTIC_REGRESSION_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | L1 REGULARIZED LOGISTIC REGRESSION   |
    //                   | SVM - DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the L1-Regularized Logistic Regression.
     *  Details of the algorithm can be found at: G-X. Yuan, C-H. Ho, C-J. Lin, <b>"An Improved GLMNET for L1-Regularized
     *  Logistic Regression"</b>, <i>Proceedings of the 17th ACM SIGKDD International Conference on Knowledge Discovery and
     *  Data Mining, 33--41, 2011</i>.
     *
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class L1RegularizedLogisticRegressionSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        L1RegularizedLogisticRegressionSVM(void) : LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         *  \param[in] transpose_data boolean flag which enables to transpose the whole data set (i.e. store training samples by dimension instead of by sample).
         */
        L1RegularizedLogisticRegressionSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations, bool transpose_data) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations),
            m_transpose_data(transpose_data) {}
        /// Copy constructor.
        L1RegularizedLogisticRegressionSVM(const L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
            m_transpose_data(other.m_transpose_data) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L1RegularizedLogisticRegressionSVM(const L1RegularizedLogisticRegressionSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
            m_transpose_data(other.m_transpose_data) {}
        /// Destructor.
        virtual ~L1RegularizedLogisticRegressionSVM(void) {}
        /// Assignation operator.
        inline L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other)
            {
                LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
                m_transpose_data = other.m_transpose_data;
            }
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L1RegularizedLogisticRegressionSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            m_transpose_data = other.getTransposeData();
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which enables transposing data samples.
        inline bool getTransposeData(void) const { return m_transpose_data; }
        /// Returns a reference to the boolean flag which enables transposing data samples.
        inline bool& getTransposeData(void) { return m_transpose_data; }
        /// Sets the boolean flag which enables transposing data samples.
        inline void setTransposeData(bool transpose_data) { m_transpose_data = transpose_data; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_LOGISTIC_REGRESSION_L1_REGULARIZED; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_LOGISTIC_REGRESSION_L1_REGULARIZED; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of a linear support vector machine into an XML object.
        inline void convertToXML(XmlParser &parser) const
        {
            parser.setSucceedingAttribute("Transpose_Data", m_transpose_data);
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertToXML(parser);
        }
        /// Retrieves the information of a linear support vector machine from an XML object.
        inline void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Classifier_Information") && ((LINEAR_LEARNER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getIdentifier()))
            {
                m_transpose_data = parser.getAttribute("Transpose_Data");
                LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertFromXML(parser);
            }
        }
        
    protected:
        // -[ Auxiliary functions ]------------------------------------------------------------------------------------------------------------------
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        /// This function calculates the number of samples which have a non-zero value for each dimension of the descriptor.
        void counter(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int, NSAMPLE> &histogram, unsigned int number_of_threads);
        /// Returns the values of the selected dimension of a set of samples.
        void transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE selected_dimension, VectorSparse<TSAMPLE, unsigned int> &column);
        /// Transpose samples to access by dimension instead by sample index.
        void transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, const VectorDense<unsigned int> &samples_histogram, VectorDense<VectorSparse<TSAMPLE, unsigned int> > &transposed_data, unsigned int number_of_threads);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
        /// Boolean flag which enables transposing data samples (i.e.\ store them by dimensions instead of storing them by sample).
        bool m_transpose_data;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | L1 REGULARIZED LOGISTIC REGRESSION   |
    //                   | SVM - DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::counter(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int, NSAMPLE> &histogram, unsigned int number_of_threads)
    {
        histogram.set(this->m_number_of_dimensions, 0);
        if (number_of_threads > 1)
        {
            VectorDense<VectorDense<unsigned int> > thread_histogram(number_of_threads);
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                thread_histogram[thread_id].set(this->m_number_of_dimensions, 0);
                for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
                    for (NSAMPLE j = 0; j < samples[i]->getData().size(); ++j)
                        ++thread_histogram[thread_id][samples[i]->getData().getIndex(j)];
            }
            for (unsigned int i = 0; i < number_of_threads; ++i)
                histogram += thread_histogram[i];
        }
        else
        {
            for (unsigned int i = 0; i < number_of_samples; ++i)
                for (NSAMPLE j = 0; j < samples[i]->getData().size(); ++j)
                    ++histogram[samples[i]->getData().getIndex(j)];
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE selected_dimension, VectorSparse<TSAMPLE, unsigned int> &column)
    {
        for (unsigned int i = 0, index = 0; i < number_of_samples; ++i)
        {
            ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
            {
                if (current_sample.getIndex(p) == selected_dimension)
                {
                    column[index].setData(current_sample.getValue(p), i);
                    ++index;
                    break;
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, const VectorDense<unsigned int> &samples_histogram, VectorDense<VectorSparse<TSAMPLE, unsigned int> > &transposed_data, unsigned int number_of_threads)
    {
        if (number_of_threads > 1)
        {
            VectorDense<NSAMPLE> index(this->m_number_of_dimensions, 0);
            VectorDense<omp_lock_t> dimension_lock(this->m_number_of_dimensions);
            
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
            {
                transposed_data[i].set(samples_histogram[i]);
                omp_init_lock(&dimension_lock[i]);
            }
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
                {
                    ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
                    for (NSAMPLE j = 0; j < current_sample.size(); ++j)
                    {
                        const NSAMPLE current_index = current_sample.getIndex(j);
                        
                        omp_set_lock(&dimension_lock[current_index]);
                        transposed_data[current_index][index[current_index]].setData(current_sample.getValue(j), i);
                        ++index[current_index];
                        omp_unset_lock(&dimension_lock[current_index]);
                    }
                }
            }
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                omp_destroy_lock(&dimension_lock[i]);
        }
        else
        {
            VectorDense<NSAMPLE> index(this->m_number_of_dimensions, 0);
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                transposed_data[i].set(samples_histogram[i]);
            for (unsigned int i = 0; i < number_of_samples; ++i)
            {
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
                for (NSAMPLE j = 0; j < current_sample.size(); ++j)
                {
                    const NSAMPLE current_index = current_sample.getIndex(j);
                    transposed_data[current_index][index[current_index]].setData(current_sample.getValue(j), i);
                    ++index[current_index];
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        const unsigned int maximum_number_linesearchs = 20;
        const unsigned int maximum_number_of_newton_iterations = srvMax<unsigned int>(1, this->m_maximum_number_of_iterations / 10);
        const TCLASSIFIER sigma = _C(0.01);
        const TCLASSIFIER nu = _C(1e-12);
        VectorDense<bool, NSAMPLE> shrink_dimension(this->m_number_of_dimensions);
        VectorDense<NSAMPLE, NSAMPLE> dimension_index(this->m_number_of_dimensions);
        VectorDense<unsigned int, NSAMPLE> samples_histogram(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> hessian_diagonal(this->m_number_of_dimensions), gradient(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> weights(this->m_number_of_dimensions), * negative_sum;
        VectorDense<TCLASSIFIER, unsigned int> exponential_score(number_of_train_samples), exponential_score_new(number_of_train_samples);
        VectorDense<TCLASSIFIER, unsigned int> tau(number_of_train_samples), D(number_of_train_samples), xd(number_of_train_samples);
        VectorDense<TCLASSIFIER, unsigned int> thread_maximum_value(number_of_threads);
        VectorDense<char> samples_sign(number_of_train_samples);
        unsigned int active_sample_size, iteration, newton_iteration;
        NSAMPLE active_dimension_size;
        TCLASSIFIER classifier_norm, maximum_g_old, initial_norm_g, inner_epsilon;
        
        if (logger != 0)
            logger->log("Counting the number of samples which have a non-zero element for each dimension.");
        negative_sum = new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_threads];
        for (unsigned int t = 0; t < number_of_threads; ++t)
            negative_sum[t].set(this->m_number_of_dimensions);
        counter(train_samples, number_of_train_samples, samples_histogram, number_of_threads);
        if (m_transpose_data)
        {
            VectorDense<VectorSparse<TSAMPLE, unsigned int> > transposed_data(this->m_number_of_dimensions);
            
            if (logger != 0)
                logger->log("Transpose the training data.");
            transpose(train_samples, number_of_train_samples, samples_histogram, transposed_data, number_of_threads);
            
            for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                unsigned int positive_samples, negative_samples;
                TCLASSIFIER current_epsilon;
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                
                // [INITIALIZATION] 1.- Calculate the label sign for each sample for the current classifier. (MULTI THREAD)
                active_sample_size = 0;
                inner_epsilon = _C(1.0);
                classifier_norm = _C(0.0);
                positive_samples = 0;
                negative_samples = 0;
                #pragma omp parallel num_threads(number_of_threads) reduction(+:active_sample_size) reduction(+:positive_samples) reduction(+:negative_samples)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    negative_sum[thread_id].setValue(_C(0));
                    for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        exponential_score_new[i] = exponential_score[i] = _C(1);
                        if (this->m_ignore_difficult)
                        {
                            if (probability >= this->m_difficult_probability)
                            {
                                samples_sign[i] = 1;
                                tau[i] = positive_weight * _C(0.5);
                                D[i] = positive_weight * _C(0.25);
                                ++active_sample_size;
                                ++positive_samples;
                            }
                            else if (probability < this->m_positive_probability)
                            {
                                samples_sign[i] = -1;
                                tau[i] = negative_weight * _C(0.5);
                                D[i] = negative_weight * _C(0.25);
                                for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                                    negative_sum[thread_id][train_samples[i]->getData().getIndex(p)] += negative_weight * _C(train_samples[i]->getData().getValue(p));
                                ++active_sample_size;
                                ++negative_samples;
                            }
                            else
                            {
                                samples_sign[i] = 0;
                                tau[i] = _C(0);
                                D[i] = _C(0);
                            }
                        }
                        else
                        {
                            if (probability >= this->m_positive_probability)
                            {
                                samples_sign[i] = 1;
                                tau[i] = positive_weight * _C(0.5);
                                D[i] = positive_weight * _C(0.25);
                                ++positive_samples;
                            }
                            else
                            {
                                samples_sign[i] = -1;
                                tau[i] = negative_weight * _C(0.5);
                                D[i] = negative_weight * _C(0.25);
                                for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                                    negative_sum[thread_id][train_samples[i]->getData().getIndex(p)] += negative_weight * _C(train_samples[i]->getData().getValue(p));
                                ++negative_samples;
                            }
                            ++active_sample_size;
                        }
                    }
                    
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                    {
                        this->m_classifiers[category_id][dimension] = 0;
                        weights[dimension] = _C(0);
                        dimension_index[dimension] = dimension;
                        for (unsigned int t = 1; t < number_of_threads; ++t)
                            negative_sum[0][dimension] += negative_sum[t][dimension];
                    }
                }
                
                current_epsilon = this->m_epsilon * srvMax<TCLASSIFIER>(_C(1.0), _C(srvMin<unsigned int>(positive_samples, negative_samples))) / _C(active_sample_size);
                initial_norm_g = _C(0);
                maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                active_dimension_size = this->m_number_of_dimensions;
                for (newton_iteration = 0; newton_iteration < maximum_number_of_newton_iterations; ++newton_iteration)
                {
                    TCLASSIFIER maximum_g, norm_g, qp_maximum_g_old, qp_maximum_g, qp_norm_g, delta, classifier_norm_new;
                    TCLASSIFIER negative_sum_xd;
                    NSAMPLE qp_active_size;
                    unsigned int number_linesearch;
                    
                    // [SOLVER ALGORITHM] Calculate the gradient and Hessian diagonal vectors and select which dimensions are going to be updated.
                    norm_g = 0;
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:norm_g)
                    {
                        const unsigned int thread_id = omp_get_thread_num();
                        
                        thread_maximum_value[thread_id] = _C(0);
                        for (NSAMPLE dimension = (NSAMPLE)thread_id; dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                        {
                            const NSAMPLE current_dimension = dimension_index[dimension];
                            TCLASSIFIER tmp, hessian_value, gradient_positive, gradient_negative, violation;
                            ConstantSubVectorSparse<TSAMPLE, unsigned int> current_samples(transposed_data[current_dimension]);
                            
                            shrink_dimension[dimension] = false;
                            gradient[current_dimension] = _C(0);
                            tmp = _C(0);
                            hessian_value = nu;
                            for (unsigned int i = 0; i < current_samples.size(); ++i)
                            {
                                if (samples_sign[current_samples.getIndex(i)] != 0)
                                {
                                    const TCLASSIFIER current_value = _C(current_samples.getValue(i));
                                    hessian_value += current_value * current_value * D[current_samples.getIndex(i)];
                                    tmp += current_value * tau[current_samples.getIndex(i)];
                                }
                            }
                            hessian_diagonal[current_dimension] = hessian_value;
                            gradient[current_dimension] = -tmp + negative_sum[0][current_dimension];
                            gradient_positive = gradient[current_dimension] + _C(1);
                            gradient_negative = gradient[current_dimension] - _C(1);
                            violation = _C(0);
                            
                            if (this->m_classifiers[category_id][current_dimension] == _C(0))
                            {
                                if (gradient_positive < _C(0)) violation = -gradient_positive;
                                else if (gradient_negative > _C(0)) violation = gradient_negative;
                                else if ((gradient_positive > maximum_g_old / _C(active_sample_size)) && (gradient_negative < -maximum_g_old / _C(active_sample_size)))
                                {
                                    shrink_dimension[dimension] = true;
                                    continue;
                                }
                            }
                            else if (this->m_classifiers[category_id][current_dimension] > _C(0)) violation = srvAbs<TCLASSIFIER>(gradient_positive);
                            else violation = srvAbs<TCLASSIFIER>(gradient_negative);
                            thread_maximum_value[thread_id] = srvMax<TCLASSIFIER>(thread_maximum_value[thread_id], violation);
                            norm_g += violation;
                        }
                    }
                    maximum_g = thread_maximum_value[0];
                    for (unsigned int thread_id = 1; thread_id < number_of_threads; ++thread_id)
                        maximum_g = srvMax<TCLASSIFIER>(thread_maximum_value[thread_id], maximum_g);
                    
                    active_dimension_size = this->m_number_of_dimensions;
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                    {
                        if (shrink_dimension[dimension])
                        {
                            --active_dimension_size;
                            srvSwap(dimension_index[dimension], dimension_index[active_dimension_size]);
                            srvSwap(shrink_dimension[dimension], shrink_dimension[active_dimension_size]);
                            --dimension;
                        }
                    }
                    // -------------------------------------- LOOP END ------------------------------------------
                    
                    if (newton_iteration == 0) initial_norm_g = norm_g;
                    if (norm_g <= current_epsilon * initial_norm_g) break;
                    qp_maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                    qp_active_size = active_dimension_size;
                    xd.setValue(_C(0));
                    
                    for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                    {
                        qp_maximum_g = _C(0);
                        qp_norm_g = _C(0);
                        
                        // [SOLVER INNER LOOP] Re-sort the dimension orders randomly.
                        ////for (unsigned int dimension = 0; dimension < qp_active_size; ++dimension)
                        ////    srvSwap(dimension_index[dimension], dimension_index[dimension + rand() % (qp_active_size - dimension)]);
                        
                        // [SOLVER INNER LOOP] Update the model for each dimension of the classifier.
                        for (NSAMPLE dimension = 0; dimension < qp_active_size; ++dimension)
                        {
                            const NSAMPLE current_dimension = dimension_index[dimension];
                            ConstantSubVectorSparse<TSAMPLE, unsigned int> current_samples(transposed_data[current_dimension]);
                            TCLASSIFIER current_hessian_value, current_gradient, positive_gradient, negative_gradient, violation, z;
                            
                            current_hessian_value = hessian_diagonal[current_dimension];
                            current_gradient = _C(0);
                            #pragma omp parallel num_threads(number_of_threads) reduction(+:current_gradient)
                            {
                                for (unsigned int p = omp_get_thread_num(); p < current_samples.size(); p += number_of_threads)
                                    if (samples_sign[current_samples.getIndex(p)] != 0)
                                        current_gradient += _C(current_samples.getValue(p)) * D[current_samples.getIndex(p)] * xd[current_samples.getIndex(p)];
                            }
                            current_gradient += gradient[current_dimension] + (weights[current_dimension] - this->m_classifiers[category_id][current_dimension]) * nu;
                            
                            positive_gradient = current_gradient + _C(1);
                            negative_gradient = current_gradient - _C(1);
                            violation = _C(0);
                            
                            if (weights[current_dimension] == _C(0))
                            {
                                if (positive_gradient < _C(0)) violation = -positive_gradient;
                                else if (negative_gradient > _C(0)) violation = negative_gradient;
                                else if ((positive_gradient > qp_maximum_g_old / _C(active_sample_size)) && (negative_gradient < -qp_maximum_g_old / _C(active_sample_size)))
                                {
                                    --qp_active_size;
                                    srvSwap(dimension_index[dimension], dimension_index[qp_active_size]);
                                    --dimension;
                                    continue;
                                }
                            }
                            else if (weights[current_dimension] > _C(0)) violation = srvAbs<TCLASSIFIER>(positive_gradient);
                            else violation = srvAbs<TCLASSIFIER>(negative_gradient);
                            
                            qp_maximum_g = srvMax<TCLASSIFIER>(qp_maximum_g, violation);
                            qp_norm_g += violation;
                            
                            // Obtain solution of one-variable problem.
                            if (positive_gradient < current_hessian_value * weights[current_dimension]) z = -positive_gradient / current_hessian_value;
                            else if (negative_gradient > current_hessian_value * weights[current_dimension]) z = -negative_gradient / current_hessian_value;
                            else z = -weights[current_dimension];
                            
                            if (srvAbs<TCLASSIFIER>(z) < _C(1.0e-12)) continue;
                            z = srvMin<TCLASSIFIER>(srvMax<TCLASSIFIER>(z, -_C(10.0)), _C(10.0));
                            weights[current_dimension] += z;
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                for (unsigned int p = omp_get_thread_num(); p < current_samples.size(); p += number_of_threads)
                                    if (samples_sign[current_samples.getIndex(p)] != 0)
                                        xd[current_samples.getIndex(p)] += _C(current_samples.getValue(p)) * z;
                            }
                        }
                        // ---------------------------- END DIMENSION LOOP --------------------------------------
                        
                        if (qp_norm_g <= inner_epsilon * initial_norm_g)
                        {
                            if (qp_active_size == active_dimension_size) break; // Inner stopping
                            else // Active set reactivation
                            {
                                qp_active_size = active_dimension_size;
                                qp_maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                                continue;
                            }
                        }
                        qp_maximum_g_old = qp_maximum_g;
                    }
                    if ((logger != 0) && (iteration >= this->m_maximum_number_of_iterations))
                        logger->log("WARNING: Reached maximum number of iterations %d.", this->m_maximum_number_of_iterations);
                    // --------------------------------- END OF INNER LOOP --------------------------------------
                    
                    delta = _C(0);
                    classifier_norm_new = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:delta) reduction(+:classifier_norm_new)
                    {
                        for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                        {
                            delta += gradient[dimension] * (weights[dimension] - this->m_classifiers[category_id][dimension]);
                            classifier_norm_new += srvAbs<TCLASSIFIER>(weights[dimension]);
                        }
                    }
                    delta += classifier_norm_new - classifier_norm;
                    
                    negative_sum_xd = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:negative_sum_xd)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            if (samples_sign[i] < 0)
                                negative_sum_xd += negative_weight * xd[i];
                    }
                    
                    for (number_linesearch = 0; number_linesearch < maximum_number_linesearchs; ++number_linesearch)
                    {
                        TCLASSIFIER cond;
                        
                        cond = _C(0);
                        #pragma omp parallel num_threads(number_of_threads) reduction(+:cond)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            {
                                if (samples_sign[i] > 0)
                                {
                                    const TCLASSIFIER exp_xd = std::exp(xd[i]);
                                    exponential_score_new[i] = exponential_score[i] * exp_xd;
                                    cond += positive_weight * std::log((_C(1) + exponential_score_new[i]) / (exp_xd + exponential_score_new[i]));
                                }
                                else if (samples_sign[i] < 0)
                                {
                                    const TCLASSIFIER exp_xd = std::exp(xd[i]);
                                    exponential_score_new[i] = exponential_score[i] * exp_xd;
                                    cond += negative_weight * std::log((_C(1) + exponential_score_new[i]) / (exp_xd + exponential_score_new[i]));
                                }
                            }
                        }
                        cond += classifier_norm_new - classifier_norm + negative_sum_xd - sigma * delta;
                        
                        if (cond <= 0)
                        {
                            classifier_norm = classifier_norm_new;
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                const unsigned int thread_id = omp_get_thread_num();
                                for (NSAMPLE d = (NSAMPLE)thread_id; d < this->m_number_of_dimensions; d = (NSAMPLE)(d + number_of_threads))
                                    this->m_classifiers[category_id][d] = weights[d];
                                for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                                {
                                    TCLASSIFIER tau_tmp;
                                    
                                    if (samples_sign[i] > 0)
                                    {
                                        exponential_score[i] = exponential_score_new[i];
                                        tau_tmp = _C(1.0) / (_C(1.0) + exponential_score[i]);
                                        tau[i] = positive_weight * tau_tmp;
                                        D[i] = positive_weight * exponential_score[i] * tau_tmp * tau_tmp;
                                    }
                                    else if (samples_sign[i] < 0)
                                    {
                                        exponential_score[i] = exponential_score_new[i];
                                        tau_tmp = _C(1.0) / (_C(1.0) + exponential_score[i]);
                                        tau[i] = negative_weight * tau_tmp;
                                        D[i] = negative_weight * exponential_score[i] * tau_tmp * tau_tmp;
                                    }
                                }
                            }
                            break;
                        }
                        else
                        {
                            classifier_norm_new = _C(0);
                            delta *= _C(0.5);
                            negative_sum_xd *= _C(0.5);
                            #pragma omp parallel num_threads(number_of_threads) reduction(+:classifier_norm_new)
                            {
                                const unsigned int thread_id = omp_get_thread_num();
                                for (NSAMPLE dimension = (NSAMPLE)thread_id; dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                                {
                                    weights[dimension] = (this->m_classifiers[category_id][dimension] + weights[dimension]) * _C(0.5);
                                    classifier_norm_new += srvAbs<TCLASSIFIER>(weights[dimension]);
                                }
                                
                                for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                                    if (samples_sign[i] != 0)
                                        xd[i] *= _C(0.5);
                            }
                        }
                    }
                    // ------------------------------ END OF LINE SEARCH LOOP -----------------------------------
                    
                    // Recompute some information due to too many line search steps.
                    if (number_linesearch >= maximum_number_linesearchs)
                    {
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            {
                                if (samples_sign[i] != 0)
                                {
                                    TCLASSIFIER score;
                                    
                                    score = this->classifierDot(this->m_classifiers[category_id], train_samples[i]->getData());
                                    exponential_score[i] = std::exp(score);
                                }
                            }
                        }
                    }
                    
                    if (iteration == 0) inner_epsilon *= _C(0.25);
                    maximum_g_old = maximum_g;
                    if (logger != 0)
                        logger->log("Iteration: %d; CD cycles=%d.", newton_iteration + 1, iteration + 1);
                }
                // -------------------------------- END OF NEWTON ITERATION LOOP --------------------------------
                
                if (logger != 0)
                {
                    unsigned int non_zero;
                    TCLASSIFIER v;
                    
                    logger->log("Optimization process finished after %d iterations.", newton_iteration);
                    if (newton_iteration >= maximum_number_of_newton_iterations)
                        logger->log("WARNING: Optimization process reached the maximum number of iterations.");
                    
                    v = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:v)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                        {
                            if (samples_sign[i] > 0) v += positive_weight * std::log(_C(1.0) + _C(1.0) / exponential_score[i]);
                            else if (samples_sign[i] < 0) v += negative_weight * std::log(_C(1.0) + exponential_score[i]);
                        }
                    }
                    non_zero = 0;
                    for (NSAMPLE dimension = 0; dimension < this->m_number_of_dimensions; ++dimension)
                    {
                        if (this->m_classifiers[category_id][dimension] != _C(0))
                        {
                            v += srvAbs<TCLASSIFIER>(this->m_classifiers[category_id][dimension]);
                            ++non_zero;
                        }
                    }
                    
                    logger->log("Objective value = %e; Non-zero elements in the classifier vector = %d.", (double)v, non_zero);
                }
            }
        }
        else
        {
            for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                unsigned int positive_samples, negative_samples;
                TCLASSIFIER current_epsilon;
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                
                // [INITIALIZATION] 1.- Calculate the label sign for each sample for the current classifier. (MULTI THREAD)
                active_sample_size = 0;
                positive_samples = 0;
                negative_samples = 0;
                inner_epsilon = _C(1.0);
                classifier_norm = _C(0.0);
                #pragma omp parallel num_threads(number_of_threads) reduction(+:active_sample_size) reduction(+:positive_samples) reduction(+:negative_samples)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    negative_sum[thread_id].setValue(_C(0));
                    for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        exponential_score_new[i] = exponential_score[i] = _C(1);
                        if (this->m_ignore_difficult)
                        {
                            if (probability >= this->m_difficult_probability)
                            {
                                samples_sign[i] = 1;
                                tau[i] = positive_weight * _C(0.5);
                                D[i] = positive_weight * _C(0.25);
                                ++active_sample_size;
                                ++positive_samples;
                            }
                            else if (probability < this->m_positive_probability)
                            {
                                samples_sign[i] = -1;
                                tau[i] = negative_weight * _C(0.5);
                                D[i] = negative_weight * _C(0.25);
                                for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                                    negative_sum[thread_id][train_samples[i]->getData().getIndex(p)] += negative_weight * _C(train_samples[i]->getData().getValue(p));
                                ++active_sample_size;
                                ++negative_samples;
                            }
                            else
                            {
                                samples_sign[i] = 0;
                                tau[i] = _C(0);
                                D[i] = _C(0);
                            }
                        }
                        else
                        {
                            if (probability >= this->m_positive_probability)
                            {
                                samples_sign[i] = 1;
                                tau[i] = positive_weight * _C(0.5);
                                D[i] = positive_weight * _C(0.25);
                                ++positive_samples;
                            }
                            else
                            {
                                samples_sign[i] = -1;
                                tau[i] = negative_weight * _C(0.5);
                                D[i] = negative_weight * _C(0.25);
                                for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                                    negative_sum[thread_id][train_samples[i]->getData().getIndex(p)] += negative_weight * _C(train_samples[i]->getData().getValue(p));
                                ++negative_samples;
                            }
                            ++active_sample_size;
                        }
                    }
                    
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                    {
                        this->m_classifiers[category_id][dimension] = 0;
                        weights[dimension] = 0;
                        dimension_index[dimension] = dimension;
                        for (unsigned int t = 1; t < number_of_threads; ++t)
                            negative_sum[0][dimension] += negative_sum[t][dimension];
                    }
                }
                
                current_epsilon = this->m_epsilon * srvMax<TCLASSIFIER>(_C(1.0), _C(srvMin<unsigned int>(positive_samples, negative_samples))) / _C(active_sample_size);
                initial_norm_g = _C(0);
                maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                active_dimension_size = this->m_number_of_dimensions;
                for (newton_iteration = 0; newton_iteration < maximum_number_of_newton_iterations; ++newton_iteration)
                {
                    TCLASSIFIER maximum_g, norm_g, qp_maximum_g_old, qp_maximum_g, qp_norm_g, delta, classifier_norm_new;
                    TCLASSIFIER negative_sum_xd;
                    unsigned int number_linesearch;
                    NSAMPLE qp_active_size;
                    
                    // [SOLVER ALGORITHM] Calculate the gradient and Hessian diagonal vectors and select which dimensions are going to be updated.
                    norm_g = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:norm_g)
                    {
                        const unsigned int thread_id = omp_get_thread_num();
                        
                        thread_maximum_value[thread_id] = _C(0);
                        for (NSAMPLE dimension = (NSAMPLE)thread_id; dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                        {
                            const NSAMPLE current_dimension = dimension_index[dimension];
                            TCLASSIFIER tmp, hessian_value, gradient_positive, gradient_negative, violation;
                            VectorSparse<TSAMPLE, unsigned int> current_samples(samples_histogram[current_dimension]);
                            transpose(train_samples, number_of_train_samples, (NSAMPLE)current_dimension, current_samples);
                            
                            shrink_dimension[dimension] = false;
                            gradient[current_dimension] = _C(0);
                            tmp = _C(0);
                            hessian_value = nu;
                            for (unsigned int i = 0; i < current_samples.size(); ++i)
                            {
                                if (samples_sign[current_samples.getIndex(i)] != 0)
                                {
                                    const TCLASSIFIER current_value = _C(current_samples.getValue(i));
                                    hessian_value += current_value * current_value * D[current_samples.getIndex(i)];
                                    tmp += current_value * tau[current_samples.getIndex(i)];
                                }
                            }
                            hessian_diagonal[current_dimension] = hessian_value;
                            gradient[current_dimension] = -tmp + negative_sum[0][current_dimension];
                            gradient_positive = gradient[current_dimension] + _C(1);
                            gradient_negative = gradient[current_dimension] - _C(1);
                            violation = _C(0);
                            
                            if (this->m_classifiers[category_id][current_dimension] == 0)
                            {
                                if (gradient_positive < _C(0)) violation = -gradient_positive;
                                else if (gradient_negative > _C(0)) violation = gradient_negative;
                                else if ((gradient_positive > maximum_g_old / _C(active_sample_size)) && (gradient_negative < -maximum_g_old / _C(active_sample_size)))
                                {
                                    shrink_dimension[dimension] = true;
                                    continue;
                                }
                            }
                            else if (this->m_classifiers[category_id][current_dimension] > _C(0)) violation = srvAbs<TCLASSIFIER>(gradient_positive);
                            else violation = srvAbs<TCLASSIFIER>(gradient_negative);
                            thread_maximum_value[thread_id] = srvMax<TCLASSIFIER>(thread_maximum_value[thread_id], violation);
                            norm_g += violation;
                        }
                    }
                    maximum_g = thread_maximum_value[0];
                    for (unsigned int thread_id = 1; thread_id < number_of_threads; ++thread_id)
                        maximum_g = srvMax<TCLASSIFIER>(thread_maximum_value[thread_id], maximum_g);
                    
                    active_dimension_size = this->m_number_of_dimensions;
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                    {
                        if (shrink_dimension[dimension])
                        {
                            --active_dimension_size;
                            srvSwap(dimension_index[dimension], dimension_index[active_dimension_size]);
                            srvSwap(shrink_dimension[dimension], shrink_dimension[active_dimension_size]);
                            --dimension;
                        }
                    }
                    // -------------------------------------- LOOP END ------------------------------------------
                    
                    if (newton_iteration == 0) initial_norm_g = norm_g;
                    if (norm_g <= current_epsilon * initial_norm_g) break;
                    qp_maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                    qp_active_size = active_dimension_size;
                    xd.setValue(_C(0));
                    
                    for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                    {
                        qp_maximum_g = _C(0);
                        qp_norm_g = _C(0);
                        
                        // [SOLVER INNER LOOP] Re-sort the dimension orders randomly.
                        for (NSAMPLE dimension = 0; dimension < qp_active_size; ++dimension)
                            srvSwap(dimension_index[dimension], dimension_index[(NSAMPLE)(dimension + rand() % (qp_active_size - dimension))]);
                        
                        // [SOLVER INNER LOOP] Update the model for each dimension of the classifier.
                        for (NSAMPLE dimension = 0; dimension < qp_active_size; ++dimension)
                        {
                            const NSAMPLE current_dimension = dimension_index[dimension];
                            TCLASSIFIER current_hessian_value, current_gradient, positive_gradient, negative_gradient, violation, z;
                            VectorSparse<TSAMPLE, unsigned int> current_samples(samples_histogram[current_dimension]);
                            transpose(train_samples, number_of_train_samples, (NSAMPLE)current_dimension, current_samples);
                            
                            current_hessian_value = hessian_diagonal[current_dimension];
                            current_gradient = _C(0);
                            #pragma omp parallel num_threads(number_of_threads) reduction(+:current_gradient)
                            {
                                for (unsigned int p = omp_get_thread_num(); p < current_samples.size(); p += number_of_threads)
                                    if (samples_sign[current_samples.getIndex(p)] != 0)
                                        current_gradient += _C(current_samples.getValue(p)) * D[current_samples.getIndex(p)] * xd[current_samples.getIndex(p)];
                            }
                            current_gradient += gradient[current_dimension] + (weights[current_dimension] - this->m_classifiers[category_id][current_dimension]) * nu;
                            
                            positive_gradient = current_gradient + _C(1);
                            negative_gradient = current_gradient - _C(1);
                            violation = _C(0);
                            
                            if (weights[current_dimension] == _C(0))
                            {
                                if (positive_gradient < _C(0)) violation = -positive_gradient;
                                else if (negative_gradient > _C(0)) violation = negative_gradient;
                                else if ((positive_gradient > qp_maximum_g_old / _C(active_sample_size)) && (negative_gradient < -qp_maximum_g_old / _C(active_sample_size)))
                                {
                                    --qp_active_size;
                                    srvSwap(dimension_index[dimension], dimension_index[qp_active_size]);
                                    --dimension;
                                    continue;
                                }
                            }
                            else if (weights[current_dimension] > _C(0)) violation = srvAbs<TCLASSIFIER>(positive_gradient);
                            else violation = srvAbs<TCLASSIFIER>(negative_gradient);
                            
                            qp_maximum_g = srvMax<TCLASSIFIER>(qp_maximum_g, violation);
                            qp_norm_g += violation;
                            
                            // Obtain solution of one-variable problem.
                            if (positive_gradient < current_hessian_value * weights[current_dimension]) z = -positive_gradient / current_hessian_value;
                            else if (negative_gradient > current_hessian_value * weights[current_dimension]) z = -negative_gradient / current_hessian_value;
                            else z = -weights[current_dimension];
                            
                            if (srvAbs<TCLASSIFIER>(z) < _C(1.0e-12)) continue;
                            z = srvMin<TCLASSIFIER>(srvMax<TCLASSIFIER>(z, -_C(10.0)), _C(10.0));
                            weights[current_dimension] += z;
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                for (unsigned int p = omp_get_thread_num(); p < current_samples.size(); p += number_of_threads)
                                    if (samples_sign[current_samples.getIndex(p)] != 0)
                                        xd[current_samples.getIndex(p)] += _C(current_samples.getValue(p)) * z;
                            }
                        }
                        // ---------------------------- END DIMENSION LOOP --------------------------------------
                        
                        if (qp_norm_g <= inner_epsilon * initial_norm_g)
                        {
                            if (qp_active_size == active_dimension_size) break; // Inner stopping
                            else // Active set reactivation
                            {
                                qp_active_size = active_dimension_size;
                                qp_maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                                continue;
                            }
                        }
                        qp_maximum_g_old = qp_maximum_g;
                    }
                    if ((logger != 0) && (iteration >= this->m_maximum_number_of_iterations))
                        logger->log("WARNING: Reached maximum number of iterations %d.", this->m_maximum_number_of_iterations);
                    // --------------------------------- END OF INNER LOOP --------------------------------------
                    
                    delta = _C(0);
                    classifier_norm_new = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:delta) reduction(+:classifier_norm_new)
                    {
                        for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                        {
                            delta += gradient[dimension] * (weights[dimension] - this->m_classifiers[category_id][dimension]);
                            classifier_norm_new += srvAbs<TCLASSIFIER>(weights[dimension]);
                        }
                    }
                    delta += classifier_norm_new - classifier_norm;
                    
                    negative_sum_xd = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:negative_sum_xd)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            if (samples_sign[i] < 0)
                                negative_sum_xd += negative_weight * xd[i];
                    }
                    
                    for (number_linesearch = 0; number_linesearch < maximum_number_linesearchs; ++number_linesearch)
                    {
                        TCLASSIFIER cond;
                        
                        cond = _C(0);
                        #pragma omp parallel num_threads(number_of_threads) reduction(+:cond)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            {
                                if (samples_sign[i] > 0)
                                {
                                    const TCLASSIFIER exp_xd = std::exp(xd[i]);
                                    exponential_score_new[i] = exponential_score[i] * exp_xd;
                                    cond += positive_weight * std::log((_C(1) + exponential_score_new[i]) / (exp_xd + exponential_score_new[i]));
                                }
                                else if (samples_sign[i] < 0)
                                {
                                    const TCLASSIFIER exp_xd = std::exp(xd[i]);
                                    exponential_score_new[i] = exponential_score[i] * exp_xd;
                                    cond += negative_weight * std::log((_C(1) + exponential_score_new[i]) / (exp_xd + exponential_score_new[i]));
                                }
                            }
                        }
                        cond += classifier_norm_new - classifier_norm + negative_sum_xd - sigma * delta;
                        
                        if (cond <= _C(0))
                        {
                            classifier_norm = classifier_norm_new;
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                const unsigned int thread_id = omp_get_thread_num();
                                for (NSAMPLE d = (NSAMPLE)thread_id; d < this->m_number_of_dimensions; d = (NSAMPLE)(d + number_of_threads))
                                    this->m_classifiers[category_id][d] = weights[d];
                                for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                                {
                                    TCLASSIFIER tau_tmp;
                                    
                                    if (samples_sign[i] > 0)
                                    {
                                        exponential_score[i] = exponential_score_new[i];
                                        tau_tmp = _C(1.0) / (_C(1.0) + exponential_score[i]);
                                        tau[i] = positive_weight * tau_tmp;
                                        D[i] = positive_weight * exponential_score[i] * tau_tmp * tau_tmp;
                                    }
                                    else if (samples_sign[i] < 0)
                                    {
                                        exponential_score[i] = exponential_score_new[i];
                                        tau_tmp = _C(1.0) / (_C(1.0) + exponential_score[i]);
                                        tau[i] = negative_weight * tau_tmp;
                                        D[i] = negative_weight * exponential_score[i] * tau_tmp * tau_tmp;
                                    }
                                }
                            }
                            break;
                        }
                        else
                        {
                            classifier_norm_new = _C(0);
                            delta *= _C(0.5);
                            negative_sum_xd *= _C(0.5);
                            #pragma omp parallel num_threads(number_of_threads) reduction(+:classifier_norm_new)
                            {
                                const unsigned int thread_id = omp_get_thread_num();
                                for (NSAMPLE dimension = (NSAMPLE)thread_id; dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                                {
                                    weights[dimension] = (this->m_classifiers[category_id][dimension] + weights[dimension]) * _C(0.5);
                                    classifier_norm_new += srvAbs<TCLASSIFIER>(weights[dimension]);
                                }
                                
                                for (unsigned int i = thread_id; i < number_of_train_samples; i += number_of_threads)
                                    if (samples_sign[i] != 0)
                                        xd[i] *= _C(0.5);
                            }
                        }
                    }
                    // ------------------------------ END OF LINE SEARCH LOOP -----------------------------------
                    
                    // Recompute some information due to too many line search steps.
                    if (number_linesearch >= maximum_number_linesearchs)
                    {
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                            {
                                if (samples_sign[i] != 0)
                                {
                                    TCLASSIFIER score;
                                    
                                    score = this->classifierDot(this->m_classifiers[category_id], train_samples[i]->getData());
                                    exponential_score[i] = std::exp(score);
                                }
                            }
                        }
                    }
                    
                    if (iteration == 0) inner_epsilon *= _C(0.25);
                    maximum_g_old = maximum_g;
                    if (logger != 0)
                        logger->log("Iteration: %d; CD cycles=%d.", newton_iteration + 1, iteration + 1);
                }
                // -------------------------------- END OF NEWTON ITERATION LOOP --------------------------------
                
                if (logger != 0)
                {
                    unsigned int non_zero;
                    TCLASSIFIER v;
                    
                    logger->log("Optimization process finished after %d iterations.", newton_iteration);
                    if (newton_iteration >= maximum_number_of_newton_iterations)
                        logger->log("WARNING: Optimization process reached the maximum number of iterations.");
                    
                    v = _C(0);
                    #pragma omp parallel num_threads(number_of_threads) reduction(+:v)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                        {
                            if (samples_sign[i] > 0) v += positive_weight * std::log(_C(1.0) + _C(1.0) / exponential_score[i]);
                            else if (samples_sign[i] < 0) v += negative_weight * std::log(_C(1.0) + exponential_score[i]);
                        }
                    }
                    non_zero = 0;
                    for (NSAMPLE dimension = 0; dimension < this->m_number_of_dimensions; ++dimension)
                    {
                        if (this->m_classifiers[category_id][dimension] != _C(0))
                        {
                            v += srvAbs<TCLASSIFIER>(this->m_classifiers[category_id][dimension]);
                            ++non_zero;
                        }
                    }
                    
                    logger->log("Objective value = %e; Non-zero elements in the classifier vector = %d.", (double)v, non_zero);
                }
            }
        }
        delete [] negative_sum;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

