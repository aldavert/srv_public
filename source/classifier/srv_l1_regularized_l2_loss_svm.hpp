// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_L1_REGULARIZED_L2_LOSS_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_L1_REGULARIZED_L2_LOSS_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | L1 REGULARIZED L2 LOSS SVM CLASS     |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the L1-Regularized L2-Loss. Details of
     *  the algorithm can be found at: G.X. Yuan, K.W. Chang, C.J. Hsieh, C.J. Lin, <b>"A Comparison of Optimization Methods
     *  and Software for Large-scale L1-regularized Linear Classifier"</b>, <i>Journal of Machine Learning Research (JMLR), 11,
     *  3183--3234, 2010</i>.
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class L1RegularizedL2LossSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        L1RegularizedL2LossSVM(void) : LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(), m_transpose_data(false) {}
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         *  \param[in] transpose_data boolean flag which enables to transpose the whole data set (i.e. store training samples by dimension instead of by sample).
         */
        L1RegularizedL2LossSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations, bool transpose_data) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations),
            m_transpose_data(transpose_data) {}
        /// Copy constructor.
        L1RegularizedL2LossSVM(const L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
            m_transpose_data(other.m_transpose_data) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L1RegularizedL2LossSVM(const L1RegularizedL2LossSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
            m_transpose_data(other.m_transpose_data) {}
        /// Destructor.
        virtual ~L1RegularizedL2LossSVM(void) {}
        /// Assignation operator.
        inline L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other)
            {
                LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
                m_transpose_data = other.m_transpose_data;
            }
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L1RegularizedL2LossSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            m_transpose_data = other.getTransposeData();
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which enables transposing data samples.
        inline bool getTransposeData(void) const { return m_transpose_data; }
        /// Returns a reference to the boolean flag which enables transposing data samples.
        inline bool& getTransposeData(void) { return m_transpose_data; }
        /// Sets the boolean flag which enables transposing data samples.
        inline void setTransposeData(bool transpose_data) { m_transpose_data = transpose_data; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_L1_REGULARIZED_L2_LOSS; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_L1_REGULARIZED_L2_LOSS; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of a linear support vector machine into an XML object.
        inline void convertToXML(XmlParser &parser) const
        {
            parser.setSucceedingAttribute("Transpose_Data", m_transpose_data);
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertToXML(parser);
        }
        /// Retrieves the information of a linear support vector machine from an XML object.
        inline void convertFromXML(XmlParser &parser)
        {
            if (parser.isTagIdentifier("Classifier_Information") && ((LINEAR_LEARNER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getIdentifier()))
            {
                m_transpose_data = parser.getAttribute("Transpose_Data");
                LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertFromXML(parser);
            }
        }
        
    protected:
        // -[ Auxiliary functions ]------------------------------------------------------------------------------------------------------------------
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        /// This function calculates the number of samples which have a non-zero value for each dimension of the descriptor.
        void counter(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int, NSAMPLE> &histogram, unsigned int number_of_threads);
        /// Returns the values of the selected dimension of a set of samples.
        void transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE selected_dimension, VectorSparse<TSAMPLE, unsigned int> &column);
        /// Transpose samples to access by dimension instead by sample index.
        void transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, const VectorDense<unsigned int> &samples_histogram, VectorDense<VectorSparse<TSAMPLE, unsigned int> > &transposed_data, unsigned int number_of_threads);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
        /// Boolean flag which enables transposing data samples (i.e.\ store them by dimensions instead of storing them by sample).
        bool m_transpose_data;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | L1 REGULARIZED L2 LOSS SVM CLASS     |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::counter(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, VectorDense<unsigned int, NSAMPLE> &histogram, unsigned int number_of_threads)
    {
        histogram.set(this->m_number_of_dimensions, 0);
        if (number_of_threads > 1)
        {
            VectorDense<VectorDense<unsigned int> > thread_histogram(number_of_threads);
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                thread_histogram[thread_id].set(this->m_number_of_dimensions, 0);
                for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
                    for (NSAMPLE j = 0; j < samples[i]->getData().size(); ++j)
                        ++thread_histogram[thread_id][samples[i]->getData().getIndex(j)];
            }
            for (unsigned int i = 0; i < number_of_threads; ++i)
                histogram += thread_histogram[i];
        }
        else
        {
            for (unsigned int i = 0; i < number_of_samples; ++i)
                for (NSAMPLE j = 0; j < samples[i]->getData().size(); ++j)
                    ++histogram[samples[i]->getData().getIndex(j)];
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE selected_dimension, VectorSparse<TSAMPLE, unsigned int> &column)
    {
        for (unsigned int i = 0, index = 0; i < number_of_samples; ++i)
        {
            ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
            {
                if (current_sample.getIndex(p) == selected_dimension)
                {
                    column[index].setData(current_sample.getValue(p), i);
                    ++index;
                    break;
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::transpose(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, const VectorDense<unsigned int> &samples_histogram, VectorDense<VectorSparse<TSAMPLE, unsigned int> > &transposed_data, unsigned int number_of_threads)
    {
        if (number_of_threads > 1)
        {
            VectorDense<unsigned int> index(this->m_number_of_dimensions, 0);
            VectorDense<omp_lock_t> dimension_lock(this->m_number_of_dimensions);
            
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
            {
                transposed_data[i].set(samples_histogram[i]);
                omp_init_lock(&dimension_lock[i]);
            }
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_samples; i += number_of_threads)
                {
                    ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
                    for (NSAMPLE j = 0; j < current_sample.size(); ++j)
                    {
                        const NSAMPLE current_index = current_sample.getIndex(j);
                        
                        omp_set_lock(&dimension_lock[current_index]);
                        transposed_data[current_index][index[current_index]].setData(current_sample.getValue(j), i);
                        ++index[current_index];
                        omp_unset_lock(&dimension_lock[current_index]);
                    }
                }
            }
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                omp_destroy_lock(&dimension_lock[i]);
        }
        else
        {
            VectorDense<unsigned int> index(this->m_number_of_dimensions, 0);
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                transposed_data[i].set(samples_histogram[i]);
            for (unsigned int i = 0; i < number_of_samples; ++i)
            {
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(samples[i]->getData());
                for (NSAMPLE j = 0; j < current_sample.size(); ++j)
                {
                    const unsigned int current_index = (unsigned int)current_sample.getIndex(j);
                    transposed_data[current_index][index[current_index]].setData(current_sample.getValue(j), i);
                    ++index[current_index];
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<char, unsigned int> samples_sign(number_of_train_samples);
        VectorDense<NSAMPLE, NSAMPLE> dimension_index(this->m_number_of_dimensions);
        VectorDense<unsigned int, NSAMPLE> samples_histogram(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, unsigned int> b(number_of_train_samples);
        VectorDense<TCLASSIFIER, NSAMPLE> accumulated_square(this->m_number_of_dimensions);
        const unsigned int maximum_number_linesearchs = 20;
        const TCLASSIFIER sigma = _C(0.001);
        
        if (logger != 0)
            logger->log("Counting the number of samples which have a non-zero element for each dimension.");
        counter(train_samples, number_of_train_samples, samples_histogram, number_of_threads);
        if (m_transpose_data)
        {
            VectorDense<VectorSparse<TSAMPLE, unsigned int> > transposed_data(this->m_number_of_dimensions);
            unsigned int active_sample_size, iteration;
            NSAMPLE active_dimension_size;
            TCLASSIFIER maximum_g_old, initial_norm_g;
            
            if (logger != 0)
                logger->log("Transpose the training data.");
            transpose(train_samples, number_of_train_samples, samples_histogram, transposed_data, number_of_threads);
            
            for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                unsigned int positive_samples, negative_samples;
                TCLASSIFIER current_epsilon;
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                
                // [INITIALIZATION] 1.- Calculate the label sign for each sample for the current classifier. (MULTI THREAD)
                active_sample_size = 0;
                positive_samples = 0;
                negative_samples = 0;
                #pragma omp parallel num_threads(number_of_threads) reduction(+:active_sample_size) reduction(+:positive_samples) reduction(+:negative_samples)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        if (this->m_ignore_difficult)
                        {
                            if (probability >= this->m_difficult_probability)
                            {
                                samples_sign[i] = 1;
                                ++active_sample_size;
                                ++positive_samples;
                            }
                            else if (probability < this->m_positive_probability)
                            {
                                samples_sign[i] = -1;
                                ++active_sample_size;
                                ++negative_samples;
                            }
                            else samples_sign[i] = 0;
                        }
                        else
                        {
                            if (probability >= this->m_positive_probability)
                            {
                                samples_sign[i] = 1;
                                ++positive_samples;
                            }
                            else
                            {
                                samples_sign[i] = -1;
                                ++negative_samples;
                            }
                            ++active_sample_size;
                        }
                        b[i] = _C(1);
                    }
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                    {
                        ConstantSubVectorSparse<TSAMPLE, unsigned int> current_values(transposed_data[dimension]);
                        TCLASSIFIER current_value;
                        
                        this->m_classifiers[category_id][dimension] = _C(0);
                        dimension_index[dimension] = dimension;
                        current_value = _C(0);
                        for (unsigned int i = 0; i < current_values.size(); ++i)
                        {
                            const TCLASSIFIER bin_value = _C(current_values.getValue(i));
                            if (samples_sign[current_values.getIndex(i)] > 0)
                                current_value += positive_weight * bin_value * bin_value;
                            else if (samples_sign[current_values.getIndex(i)] < 0)
                                current_value += negative_weight * bin_value * bin_value;
                        }
                        accumulated_square[dimension] = current_value;
                    }
                }
                
                current_epsilon = this->m_epsilon * srvMax<TCLASSIFIER>(_C(1.0), _C(srvMin<unsigned int>(positive_samples, negative_samples))) / _C(active_sample_size);
                initial_norm_g = _C(0);
                maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                active_dimension_size = this->m_number_of_dimensions;
                for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                {
                    TCLASSIFIER maximum_g, norm_g, loss_g, h, positive_g, negative_g, g, violation, d, d_old, delta, loss_old, loss_new;
                    unsigned int number_line_search;
                    
                    if ((logger != 0) && (iteration % 10 == 0))
                        logger->log("Iteration %d: maximum_g=%e", iteration + 1, (double)maximum_g_old);
                    maximum_g = _C(0);
                    norm_g = _C(0);
                    
                    // [SOLVER ALGORITHM] Re-sort the indexes of the classifier dimensions.
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                        srvSwap(dimension_index[dimension], dimension_index[(NSAMPLE)(dimension + rand() % (active_dimension_size - dimension))]);
                    
                    // [SOLVER ALGORITHM] Minimize the cost function iterating over each dimension of the classifier.
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                    {
                        const NSAMPLE current_dimension = dimension_index[dimension];
                        const TCLASSIFIER current_weight = this->m_classifiers[category_id][current_dimension];
                        ConstantSubVectorSparse<TSAMPLE, unsigned int> current_values(transposed_data[current_dimension]);
                        
                        loss_g = _C(0);
                        h = _C(0);
                        #pragma omp parallel num_threads(number_of_threads) reduction(-:loss_g) reduction(+:h)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                            {
                                const unsigned int current_sample_index = current_values.getIndex(i);
                                
                                if (b[current_sample_index] > _C(0))
                                {
                                    const TCLASSIFIER current_value = _C(current_values.getValue(i));
                                    
                                    if (samples_sign[current_sample_index] > 0)
                                    {
                                        loss_g -= positive_weight * current_value * b[current_sample_index];
                                        h += positive_weight * current_value * current_value;
                                    }
                                    else if (samples_sign[current_sample_index] < 0)
                                    {
                                        loss_g += negative_weight * current_value * b[current_sample_index];
                                        h += negative_weight * current_value * current_value;
                                    }
                                }
                            }
                        }
                        loss_g *= _C(2);
                        g = loss_g;
                        h = srvMax<TCLASSIFIER>(h * _C(2), _C(1e-12));
                        
                        positive_g = g + _C(1);
                        negative_g = g - _C(1);
                        violation = _C(0);
                        
                        if (current_weight == _C(0))
                        {
                            if (positive_g < _C(0)) violation = -positive_g;
                            else if (negative_g > _C(0)) violation = negative_g;
                            else if ((positive_g > maximum_g_old / _C(active_sample_size)) && (negative_g < -maximum_g_old / _C(active_sample_size)))
                            {
                                --active_dimension_size;
                                srvSwap(dimension_index[dimension], dimension_index[active_dimension_size]);
                                --dimension;
                                continue;
                            }
                        }
                        else if (current_weight > 0) violation = srvAbs<TCLASSIFIER>(positive_g);
                        else violation = srvAbs<TCLASSIFIER>(negative_g);
                        maximum_g = srvMax<TCLASSIFIER>(maximum_g, violation);
                        norm_g += violation;
                        
                        // [SOLVER ALGORITHM] Obtain Newton direction 'd'.
                        if (positive_g < h * current_weight) d = -positive_g / h;
                        else if (negative_g > h * current_weight) d = -negative_g / h;
                        else d = -current_weight;
                        if (srvAbs<TCLASSIFIER>(d) < _C(1.0e-12)) continue;
                        delta = srvAbs<TCLASSIFIER>(current_weight + d) - srvAbs<TCLASSIFIER>(current_weight) + g * d;
                        d_old = _C(0);
                        loss_old = _C(0);
                        loss_new = _C(0);
                        for (number_line_search = 0; number_line_search < maximum_number_linesearchs; ++number_line_search)
                        {
                            TCLASSIFIER d_diff, cond, appxcond;
                            
                            d_diff = d_old - d;
                            cond = srvAbs<TCLASSIFIER>(current_weight + d) - srvAbs<TCLASSIFIER>(current_weight) - sigma * delta;
                            appxcond = accumulated_square[current_dimension] * d * d + loss_g * d + cond;
                            
                            if (appxcond <= _C(0))
                            {
                                #pragma omp parallel num_threads(number_of_threads)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                            b[current_sample_index] += d_diff * _C(current_values.getValue(i));
                                        else if (samples_sign[current_sample_index] < 0)
                                            b[current_sample_index] -= d_diff * _C(current_values.getValue(i));
                                    }
                                }
                                break;
                            }
                            
                            if (number_line_search == 0)
                            {
                                loss_old = _C(0);
                                loss_new = _C(0);
                                #pragma omp parallel num_threads(number_of_threads) reduction(+:loss_old) reduction(+:loss_new)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] + d_diff * _C(current_values.getValue(i));
                                            if (b[current_sample_index] > _C(0))
                                                loss_old += positive_weight * b[current_sample_index] * b[current_sample_index];
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += positive_weight * b_new * b_new;
                                        }
                                        else if (samples_sign[current_sample_index] < 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] - d_diff * _C(current_values.getValue(i));
                                            if (b[current_sample_index] > _C(0))
                                                loss_old += negative_weight * b[current_sample_index] * b[current_sample_index];
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += negative_weight * b_new * b_new;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                #pragma omp parallel num_threads(number_of_threads) reduction(+:loss_new)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] + d_diff * _C(current_values.getValue(i));
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += positive_weight * b_new * b_new;
                                        }
                                        else if (samples_sign[current_sample_index] < 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] - d_diff * _C(current_values.getValue(i));
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += negative_weight * b_new * b_new;
                                        }
                                    }
                                }
                            }
                            
                            cond = cond + loss_new - loss_old;
                            if (cond <= _C(0)) break;
                            else
                            {
                                d_old = d;
                                d *= _C(0.5);
                                delta *= _C(0.5);
                            }
                        }
                        this->m_classifiers[category_id][current_dimension] += d;
                        
                        // [SOLVER ALGORITHM] Recompute 'b' if line search takes too many steps.
                        if (number_line_search >= maximum_number_linesearchs)
                        {
                            if (logger != 0)
                                logger->log("Line search needed to many steps, recomputing b.");
                            
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                                {
                                    if (samples_sign[i] != 0)
                                    {
                                        b[i] = this->classifierDot(this->m_classifiers[category_id], train_samples[i]->getData());
                                        if (samples_sign[i] > 0) b[i] = _C(1.0) - b[i];
                                        else b[i] = _C(1.0) + b[i];
                                    }
                                    else b[i] = _C(0);
                                }
                            }
                        }
                    }
                    
                    if (iteration == 0) initial_norm_g = norm_g;
                    if (norm_g <= current_epsilon * initial_norm_g)
                    {
                        if (active_dimension_size == this->m_number_of_dimensions) break;
                        else
                        {
                            active_dimension_size = this->m_number_of_dimensions;
                            if (logger != 0)
                                logger->log("Re-enabling all dimensions.");
                            maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                            continue;
                        }
                    }
                    maximum_g_old = maximum_g;
                }
                
                if (logger != 0)
                {
                    unsigned int non_zero;
                    TCLASSIFIER v;
                    
                    logger->log("Optimization process finished in %d iterations.", iteration + 1);
                    if (iteration >= this->m_maximum_number_of_iterations)
                        logger->log("WARNING: Reached the maximum number of iterations.");
                    v = _C(0);
                    non_zero = 0;
                    for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                    {
                        if (this->m_classifiers[category_id][i] != _C(0))
                        {
                            v += srvAbs<TCLASSIFIER>(this->m_classifiers[category_id][i]);
                            ++non_zero;
                        }
                    }
                    for (unsigned int i = 0; i < number_of_train_samples; ++i)
                    {
                        if ((samples_sign[i] != 0) && (b[i] > _C(0)))
                        {
                            if (samples_sign[i] > 0)
                               v += positive_weight * b[i] * b[i];
                            else v += negative_weight * b[i] * b[i];
                        }
                    }
                    logger->log("Non-zero dimensions in the classifier: %d of %d. Objective value %lf.", non_zero, this->m_number_of_dimensions, (double)v);
                }
            }
        }
        else
        {
            unsigned int active_sample_size, iteration;
            TCLASSIFIER maximum_g_old, initial_norm_g;
            NSAMPLE active_dimension_size;
            
            for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                unsigned int positive_samples, negative_samples;
                TCLASSIFIER current_epsilon;
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                
                // [INITIALIZATION] 1.- Calculate the label sign for each sample for the current classifier. (MULTI THREAD)
                active_sample_size = 0;
                positive_samples = 0;
                negative_samples = 0;
                #pragma omp parallel num_threads(number_of_threads) reduction(+:active_sample_size) reduction(+:positive_samples) reduction(+:negative_samples)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        if (this->m_ignore_difficult)
                        {
                            if (probability >= this->m_difficult_probability)
                            {
                                samples_sign[i] = 1;
                                ++active_sample_size;
                                ++positive_samples;
                            }
                            else if (probability < this->m_positive_probability)
                            {
                                samples_sign[i] = -1;
                                ++active_sample_size;
                                ++negative_samples;
                            }
                            else samples_sign[i] = 0;
                        }
                        else
                        {
                            if (probability >= this->m_positive_probability)
                            {
                                samples_sign[i] = 1;
                                ++positive_samples;
                            }
                            else
                            {
                                samples_sign[i] = -1;
                                ++negative_samples;
                            }
                            ++active_sample_size;
                        }
                        b[i] = _C(1);
                    }
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (NSAMPLE dimension = (NSAMPLE)omp_get_thread_num(); dimension < this->m_number_of_dimensions; dimension = (NSAMPLE)(dimension + number_of_threads))
                    {
                        VectorSparse<TSAMPLE, unsigned int> current_values(samples_histogram[dimension]);
                        TCLASSIFIER current_value;
                        
                        transpose(train_samples, number_of_train_samples, dimension, current_values);
                        this->m_classifiers[category_id][dimension] = _C(0);
                        dimension_index[dimension] = dimension;
                        current_value = _C(0);
                        for (unsigned int i = 0; i < current_values.size(); ++i)
                        {
                            const TCLASSIFIER bin_value = _C(current_values.getValue(i));
                            if (samples_sign[current_values.getIndex(i)] > 0)
                                current_value += positive_weight * bin_value * bin_value;
                            else if (samples_sign[current_values.getIndex(i)] < 0)
                                current_value += negative_weight * bin_value * bin_value;
                        }
                        accumulated_square[dimension] = current_value;
                    }
                }
                
                current_epsilon = this->m_epsilon * srvMax<TCLASSIFIER>(_C(1.0), _C(srvMin<unsigned int>(positive_samples, negative_samples))) / _C(active_sample_size);
                initial_norm_g = _C(0);
                maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                active_dimension_size = this->m_number_of_dimensions;
                for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                {
                    TCLASSIFIER maximum_g, norm_g, loss_g, h, positive_g, negative_g, g, violation, d, d_old, delta, loss_old, loss_new;
                    unsigned int number_line_search;
                    
                    if ((logger != 0) && (iteration % 10 == 0))
                        logger->log("Iteration %d: maximum_g=%e", iteration + 1, (double)maximum_g_old);
                    maximum_g = _C(0);
                    norm_g = _C(0);
                    
                    // [SOLVER ALGORITHM] Re-sort the indexes of the classifier dimensions.
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                        srvSwap(dimension_index[dimension], dimension_index[(NSAMPLE)(dimension + rand() % (active_dimension_size - dimension))]);
                    
                    // [SOLVER ALGORITHM] Minimize the cost function iterating over each dimension of the classifier.
                    for (NSAMPLE dimension = 0; dimension < active_dimension_size; ++dimension)
                    {
                        const NSAMPLE current_dimension = dimension_index[dimension];
                        const TCLASSIFIER current_weight = this->m_classifiers[category_id][current_dimension];
                        VectorSparse<TSAMPLE, unsigned int> current_values(samples_histogram[current_dimension]);
                        
                        transpose(train_samples, number_of_train_samples, current_dimension, current_values);
                        loss_g = _C(0);
                        h = _C(0);
                        #pragma omp parallel num_threads(number_of_threads) reduction(-:loss_g) reduction(+:h)
                        {
                            for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                            {
                                const unsigned int current_sample_index = current_values.getIndex(i);
                                
                                if (b[current_sample_index] > 0)
                                {
                                    const TCLASSIFIER current_value = _C(current_values.getValue(i));
                                    
                                    if (samples_sign[current_sample_index] > 0)
                                    {
                                        loss_g -= positive_weight * current_value * b[current_sample_index];
                                        h += positive_weight * current_value * current_value;
                                    }
                                    else if (samples_sign[current_sample_index] < 0)
                                    {
                                        loss_g += negative_weight * current_value * b[current_sample_index];
                                        h += negative_weight * current_value * current_value;
                                    }
                                }
                            }
                        }
                        loss_g *= _C(2);
                        g = loss_g;
                        h = srvMax<TCLASSIFIER>(h * _C(2.0), _C(1e-12));
                        
                        positive_g = g + _C(1);
                        negative_g = g - _C(1);
                        violation = _C(0);
                        
                        if (current_weight == _C(0))
                        {
                            if (positive_g < _C(0)) violation = -positive_g;
                            else if (negative_g > _C(0)) violation = negative_g;
                            else if ((positive_g > maximum_g_old / _C(active_sample_size)) && (negative_g < -maximum_g_old / _C(active_sample_size)))
                            {
                                --active_dimension_size;
                                srvSwap(dimension_index[dimension], dimension_index[active_dimension_size]);
                                --dimension;
                                continue;
                            }
                        }
                        else if (current_weight > 0) violation = srvAbs<TCLASSIFIER>(positive_g);
                        else violation = srvAbs<TCLASSIFIER>(negative_g);
                        maximum_g = srvMax<TCLASSIFIER>(maximum_g, violation);
                        norm_g += violation;
                        
                        // [SOLVER ALGORITHM] Obtain Newton direction 'd'.
                        if (positive_g < h * current_weight) d = -positive_g / h;
                        else if (negative_g > h * current_weight) d = -negative_g / h;
                        else d = -current_weight;
                        if (srvAbs<TCLASSIFIER>(d) < _C(1.0e-12)) continue;
                        delta = srvAbs<TCLASSIFIER>(current_weight + d) - srvAbs<TCLASSIFIER>(current_weight) + g * d;
                        d_old = _C(0);
                        loss_old = _C(0);
                        loss_new = _C(0);
                        for (number_line_search = 0; number_line_search < maximum_number_linesearchs; ++number_line_search)
                        {
                            TCLASSIFIER d_diff, cond, appxcond;
                            
                            d_diff = d_old - d;
                            cond = srvAbs<TCLASSIFIER>(current_weight + d) - srvAbs<TCLASSIFIER>(current_weight) - sigma * delta;
                            appxcond = accumulated_square[current_dimension] * d * d + loss_g * d + cond;
                            
                            if (appxcond <= _C(0))
                            {
                                #pragma omp parallel num_threads(number_of_threads)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                            b[current_sample_index] += d_diff * _C(current_values.getValue(i));
                                        else if (samples_sign[current_sample_index] < 0)
                                            b[current_sample_index] -= d_diff * _C(current_values.getValue(i));
                                    }
                                }
                                break;
                            }
                            
                            if (number_line_search == 0)
                            {
                                loss_old = _C(0);
                                loss_new = _C(0);
                                #pragma omp parallel num_threads(number_of_threads) reduction(+:loss_old) reduction(+:loss_new)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] + d_diff * _C(current_values.getValue(i));
                                            if (b[current_sample_index] > 0)
                                                loss_old += positive_weight * b[current_sample_index] * b[current_sample_index];
                                            b[current_sample_index] = b_new;
                                            if (b_new > 0) loss_new += positive_weight * b_new * b_new;
                                        }
                                        else if (samples_sign[current_sample_index] < 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] - d_diff * _C(current_values.getValue(i));
                                            if (b[current_sample_index] > _C(0))
                                                loss_old += negative_weight * b[current_sample_index] * b[current_sample_index];
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += negative_weight * b_new * b_new;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                #pragma omp parallel num_threads(number_of_threads) reduction(+:loss_new)
                                {
                                    for (unsigned int i = omp_get_thread_num(); i < current_values.size(); i += number_of_threads)
                                    {
                                        const unsigned int current_sample_index = current_values.getIndex(i);
                                        if (samples_sign[current_sample_index] > 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] + d_diff * _C(current_values.getValue(i));
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += positive_weight * b_new * b_new;
                                        }
                                        else if (samples_sign[current_sample_index] < 0)
                                        {
                                            const TCLASSIFIER b_new = b[current_sample_index] - d_diff * _C(current_values.getValue(i));
                                            b[current_sample_index] = b_new;
                                            if (b_new > _C(0)) loss_new += negative_weight * b_new * b_new;
                                        }
                                    }
                                }
                            }
                            
                            cond = cond + loss_new - loss_old;
                            if (cond <= _C(0)) break;
                            else
                            {
                                d_old = d;
                                d *= _C(0.5);
                                delta *= _C(0.5);
                            }
                        }
                        this->m_classifiers[category_id][current_dimension] += d;
                        
                        // [SOLVER ALGORITHM] Recompute 'b' if line search takes too many steps.
                        if (number_line_search >= maximum_number_linesearchs)
                        {
                            if (logger != 0)
                                logger->log("Line search needed to many steps, recomputing b.");
                            
                            #pragma omp parallel num_threads(number_of_threads)
                            {
                                for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
                                {
                                    if (samples_sign[i] != 0)
                                    {
                                        b[i] = this->classifierDot(this->m_classifiers[category_id], train_samples[i]->getData());
                                        if (samples_sign[i] > 0) b[i] = _C(1.0) - b[i];
                                        else b[i] = _C(1.0) + b[i];
                                    }
                                    else b[i] = _C(0);
                                }
                            }
                        }
                    }
                    
                    if (iteration == 0) initial_norm_g = norm_g;
                    if (norm_g <= current_epsilon * initial_norm_g)
                    {
                        if (active_dimension_size == this->m_number_of_dimensions) break;
                        else
                        {
                            active_dimension_size = this->m_number_of_dimensions;
                            if (logger != 0)
                                logger->log("Re-enabling all dimensions.");
                            maximum_g_old = std::numeric_limits<TCLASSIFIER>::max();
                            continue;
                        }
                    }
                    maximum_g_old = maximum_g;
                }
                
                if (logger != 0)
                {
                    unsigned int non_zero;
                    TCLASSIFIER v;
                    
                    logger->log("Optimization process finished in %d iterations.", iteration + 1);
                    if (iteration >= this->m_maximum_number_of_iterations)
                        logger->log("WARNING: Reached the maximum number of iterations.");
                    v = _C(0);
                    non_zero = 0;
                    for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                    {
                        if (this->m_classifiers[category_id][i] != _C(0))
                        {
                            v += srvAbs<TCLASSIFIER>(this->m_classifiers[category_id][i]);
                            ++non_zero;
                        }
                    }
                    for (unsigned int i = 0; i < number_of_train_samples; ++i)
                    {
                        if ((samples_sign[i] != 0) && (b[i] > _C(0)))
                        {
                            if (samples_sign[i] > 0)
                               v += positive_weight * b[i] * b[i];
                            else v += negative_weight * b[i] * b[i];
                        }
                    }
                    logger->log("Non-zero dimensions in the classifier: %d of %d. Objective value %lf.", non_zero, this->m_number_of_dimensions, (double)v);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

