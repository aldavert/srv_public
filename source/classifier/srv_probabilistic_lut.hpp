// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_LUT_PROBABILISTIC_SCORE_HEADER_FILE__
#define __SRV_LUT_PROBABILISTIC_SCORE_HEADER_FILE__

#include "srv_probabilistic_score.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which converts the score obtained by a classifier into a probabilistic estimation.
    template <class TSCORE, class TPROBABILITY = TSCORE>
    class ProbabilisticLUT : public ProbabilisticScoreBase<TSCORE, TPROBABILITY>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ProbabilisticLUT(void);
        /** Constructor which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] number_of_bins number of bins used to approximate the probabilities returned by the isotonic regression.
         */
        ProbabilisticLUT(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int number_of_bins);
        /// Copy constructor.
        ProbabilisticLUT(const ProbabilisticLUT<TSCORE, TPROBABILITY> &other);
        /// Destructor.
        virtual ~ProbabilisticLUT(void);
        /// Assignation operator.
        ProbabilisticLUT<TSCORE, TPROBABILITY>& operator=(const ProbabilisticLUT<TSCORE, TPROBABILITY> &other);
        /** Function which set the parameters of the probabilistic score object.
         *  \param[in] number_of_categories number of categories of the classifier used to categorize the samples.
         *  \param[in] multiclass boolean flag which is true when the probabilities are from a multi-class distribution or false when the scores are converted to probabilities independently.
         *  \param[in] probability_factor factor applied over the probabilistic scores to rescale them from [0, 1] to [0, factor].
         *  \param[in] number_of_bins number of bins used to approximate the probabilities returned by the isotonic regression.
         */
        void set(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int number_of_bins);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the minimum scores of the classifiers.
        inline const TSCORE * getMinimumScore(void) const { return m_minimum_score; }
        /// Returns the minimum score of the index-th classifier.
        inline TSCORE getMinimumScore(unsigned int index) const { return m_minimum_score[index]; }
        /// Sets the minimum score of the index-th classifier.
        inline void setMinimumScore(const TSCORE &minimum_score, unsigned int index) const { m_minimum_score[index] = minimum_score; }
        /// Returns a constant pointer to the maximum scores of the classifiers.
        inline const TSCORE * getMaximumScore(void) const { return m_maximum_score; }
        /// Returns the maximum score of the index-th classifier.
        inline TSCORE getMaximumScore(unsigned int index) const { return m_maximum_score[index]; }
        /// Sets the maximum score of the index-th classifier.
        inline void setMaximumScore(const TSCORE &maximum_score, unsigned int index) const { m_maximum_score[index] = maximum_score; }
        
        /// Returns the number of bins used to approximate the probabilities returned by the isotonic regression.
        inline unsigned int getNumberOfBins(void) const { return m_number_of_bins; }
        /// Returns a constant array to the probabilities array of the index-th category.
        inline const TPROBABILITY * getProbabilities(unsigned int index) const { return m_probabilities[index]; }
        
        // -[ Train functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] score array of vectors with the scores of each training sample.
         *  \param[in] label array of vectors with the labels of each sample.
         *  \param[in] number_of_samples number of samples used to create the model, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<TSCORE, unsigned int> * score, const VectorDense<unsigned int, unsigned int> * label, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for classifier stores grouped into histograms of positive and negative samples.
         *  \param[in] positive_histogram histogram with the positive samples scores.
         *  \param[in] negative_histogram histogram with the negative samples scores.
         *  \param[in] bin_scores vector with the scores of each bin of the histogram.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(const VectorDense<unsigned int, unsigned int> * positive_histogram, const VectorDense<unsigned int, unsigned int> * negative_histogram, const VectorDense<TSCORE, unsigned int> &bin_scores, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which trains the probabilistic model for the given classifier scores.
         *  \param[in] model model which is approximated by the look-up-table.
         *  \param[in] number_of_threads number of threads used to concurrently create the probabilistic model.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void approximate(const ProbabilisticScoreBase<TSCORE, TPROBABILITY> * model, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Query functions ]----------------------------------------------------------------------------------------------------------------------
        /** Function which converts the raw classifier scores into probabilities.
         *  \param[in] score array of vectors with the scores of each sample.
         *  \param[out] probability array of vectors with the resulting probabilities for each sample.
         *  \param[in] number_of_samples number of sample scores converted to probabilities, i.e. number of elements both in the scores and labels pointer arrays.
         *  \param[in] number_of_threads number of threads used to concurrently rescale the scores to probabilities.
         *  \param[out] logger pointer to the logger used to show information about the rescaling process (set to 0 to disable the log information).
         */
        void probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Function which converts the classifier scores from an image into probabilities.
         *  \param[in] score image with the scores.
         *  \param[out] probability image with the resulting probabilities. This image and the scores image can be the same.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        void probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const;
        
        /** This function returns the first score which generates the given probability for all categories.
         *  \param[in] probability input probability.
         *  \param[out] scores a vector with the scores of each category which generate the specified score.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the scores.
         */
        void scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the probabilistic score object (virtual copy constructor).
        inline ProbabilisticScoreBase<TSCORE, TPROBABILITY>* duplicate(void) const { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY> *)new ProbabilisticLUT(*this); }
        /// Returns the class identifier for the probabilistic score method.
        inline static PROBABILITY_METHOD_IDENTIFIER getClassIdentifier(void) { return PROBABILITIES_LUT_ALGORITHM; }
        /// Generates a new empty instance of the probabilistic score object.
        inline static ProbabilisticScoreBase<TSCORE, TPROBABILITY>* generateObject(void) { return (ProbabilisticScoreBase<TSCORE, TPROBABILITY>*)new ProbabilisticLUT(); }
        /// Returns a flag which states if the probabilistic score class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the probabilistic score type identifier of the current object.
        inline PROBABILITY_METHOD_IDENTIFIER getIdentifier(void) const { return PROBABILITIES_LUT_ALGORITHM; }
        
    protected:
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the probabilistic object information into the attributes of the XML object.
        void attributesToXML(XmlParser &parser) const;
        /// Stores the probabilistic object information into the data of the XML object.
        void dataToXML(XmlParser &parser) const;
        /// Releases the memory allocated by the derived classes while loading the probabilistic object from a XML object.
        void freeDataXML(void);
        /// Retrieves the probabilistic object information into the attributes of the XML object.
        void attributesFromXML(XmlParser &parser);
        /// Retrieves the probabilistic object information into the data of the XML object.
        bool dataFromXML(XmlParser &parser);
        
        // -[ Other protected functions ]------------------------------------------------------------------------------------------------------------
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Array with the minimum scores of each classifier.
        TSCORE * m_minimum_score;
        /// Array with the maximum scores of each classifier.
        TSCORE * m_maximum_score;
        /// Arrays with the probabilities calculated the partitions of each classifier.
        TPROBABILITY * * m_probabilities;
        /// Number of bins used to approximate the probabilities returned by the isotonic regression.
        unsigned int m_number_of_bins;
        
        /// Static integer which indicates if the class has been initialized in the probability score factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RESCALE PROBABILISTIC SCORE          |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ACCESS FUNCTIONS ]============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticLUT<TSCORE, TPROBABILITY>::ProbabilisticLUT(void) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(),
        m_minimum_score(0),
        m_maximum_score(0),
        m_probabilities(0),
        m_number_of_bins(20) {}
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticLUT<TSCORE, TPROBABILITY>::ProbabilisticLUT(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int number_of_bins) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(number_of_categories, multiclass, probability_factor),
        m_minimum_score((number_of_categories > 0)?new TSCORE[number_of_categories]:0),
        m_maximum_score((number_of_categories > 0)?new TSCORE[number_of_categories]:0),
        m_probabilities((number_of_categories > 0)?new TPROBABILITY * [number_of_categories]:0),
        m_number_of_bins(number_of_bins)
    {
        for (unsigned int i = 0; i < number_of_categories; ++i)
        {
            if (number_of_bins > 0) m_probabilities[i] = new TPROBABILITY[number_of_bins];
            else m_probabilities[i] = 0;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticLUT<TSCORE, TPROBABILITY>::ProbabilisticLUT(const ProbabilisticLUT<TSCORE, TPROBABILITY> &other) :
        ProbabilisticScoreBase<TSCORE, TPROBABILITY>(other),
        m_minimum_score((other.m_number_of_categories > 0)?new TSCORE[other.m_number_of_categories]:0),
        m_maximum_score((other.m_number_of_categories > 0)?new TSCORE[other.m_number_of_categories]:0),
        m_probabilities((other.m_number_of_categories > 0)?new TPROBABILITY * [other.m_number_of_categories]:0),
        m_number_of_bins(other.m_number_of_bins)
    {
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            m_minimum_score[i] = other.m_minimum_score[i];
            m_maximum_score[i] = other.m_maximum_score[i];
            if (other.m_number_of_bins > 0)
            {
                m_probabilities[i] = new TPROBABILITY[other.m_number_of_bins];
                for (unsigned int j = 0; j < other.m_number_of_bins; ++j)
                    m_probabilities[i][j] = other.m_probabilities[i][j];
            }
            else m_probabilities[i] = 0;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticLUT<TSCORE, TPROBABILITY>::~ProbabilisticLUT(void)
    {
        if (m_minimum_score != 0) delete [] m_minimum_score;
        if (m_maximum_score != 0) delete [] m_maximum_score;
        if (m_probabilities != 0)
        {
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                if (m_probabilities[i] != 0) delete [] m_probabilities[i];
            delete [] m_probabilities;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    ProbabilisticLUT<TSCORE, TPROBABILITY>& ProbabilisticLUT<TSCORE, TPROBABILITY>::operator=(const ProbabilisticLUT<TSCORE, TPROBABILITY> &other)
    {
        if (this != &other)
        {
            if (m_minimum_score != 0) delete [] m_minimum_score;
            if (m_maximum_score != 0) delete [] m_maximum_score;
            if (m_probabilities != 0)
            {
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    if (m_probabilities[i] != 0) delete [] m_probabilities[i];
                delete [] m_probabilities;
            }
            
            ProbabilisticScoreBase<TSCORE, TPROBABILITY>::operator=(other);
            m_number_of_bins = other.m_number_of_bins;
            if (other.m_number_of_categories > 0)
            {
                m_minimum_score = new TSCORE[other.m_number_of_categories];
                m_maximum_score = new TSCORE[other.m_number_of_categories];
                m_probabilities = new TPROBABILITY * [other.m_number_of_categories];
                
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_minimum_score[i] = other.m_minimum_score[i];
                    m_maximum_score[i] = other.m_maximum_score[i];
                    if (other.m_number_of_bins > 0)
                    {
                        m_probabilities[i] = new TPROBABILITY[other.m_number_of_bins];
                        for (unsigned int j = 0; j < other.m_number_of_bins; ++j)
                            m_probabilities[i][j] = other.m_probabilities[i][j];
                    }
                    else m_probabilities[i] = 0;
                }
            }
            else
            {
                m_minimum_score = 0;
                m_maximum_score = 0;
                m_probabilities = 0;
            }
        }
        
        return *this;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::set(unsigned int number_of_categories, bool multiclass, double probability_factor, unsigned int number_of_bins)
    {
        if (m_minimum_score != 0) delete [] m_minimum_score;
        if (m_maximum_score != 0) delete [] m_maximum_score;
        if (m_probabilities != 0)
        {
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                if (m_probabilities[i] != 0) delete [] m_probabilities[i];
            delete [] m_probabilities;
        }
        
        this->setBase(number_of_categories, multiclass, probability_factor);
        m_number_of_bins = number_of_bins;
        
        if (number_of_categories > 0)
        {
            m_minimum_score = new TSCORE[number_of_categories];
            m_maximum_score = new TSCORE[number_of_categories];
            m_probabilities = new TPROBABILITY * [number_of_categories];
            for (unsigned int i = 0; i < number_of_categories; ++i)
            {
                if (number_of_bins > 0) m_probabilities[i] = new TPROBABILITY[number_of_bins];
                else m_probabilities[i] = 0;
            }
        }
        else
        {
            m_minimum_score = 0;
            m_maximum_score = 0;
            m_probabilities = 0;
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::attributesToXML(XmlParser &parser) const
    {
        parser.setAttribute("Number_Of_Bins", m_number_of_bins);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::attributesFromXML(XmlParser &parser)
    {
        m_number_of_bins = parser.getAttribute("Number_Of_Bins");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::dataToXML(XmlParser &parser) const
    {
        if (this->m_number_of_categories > 0)
        {
            saveVector(parser, "Minimum_Score", ConstantSubVectorDense<TSCORE, unsigned int>(m_minimum_score, this->m_number_of_categories));
            saveVector(parser, "Maximum_Score", ConstantSubVectorDense<TSCORE, unsigned int>(m_maximum_score, this->m_number_of_categories));
            if (m_number_of_bins > 0)
            {
                for (unsigned int i = 0; this->m_number_of_categories; ++i)
                {
                    parser.setSucceedingAttribute("Category", i);
                    saveVector(parser, "Probabilities", ConstantSubVectorDense<TPROBABILITY, unsigned int>(m_probabilities[i], m_number_of_bins));
                }
            }
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::freeDataXML(void)
    {
        if (m_minimum_score != 0) { delete [] m_minimum_score; m_minimum_score = 0; }
        if (m_maximum_score != 0) { delete [] m_maximum_score; m_maximum_score = 0; }
        if (m_probabilities != 0)
        {
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                if (m_probabilities[i] != 0) delete [] m_probabilities[i];
            delete [] m_probabilities;
            m_probabilities = 0;
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    bool ProbabilisticLUT<TSCORE, TPROBABILITY>::dataFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Minimum_Score"))
        {
            VectorDense<TSCORE, unsigned int> data;
            
            loadVector(parser, "Minimum_Score", data);
            m_minimum_score = new TSCORE[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_minimum_score[i] = data[i];
            
            return true;
        }
        else if (parser.isTagIdentifier("Maximum_Score"))
        {
            VectorDense<TSCORE, unsigned int> data;
            
            loadVector(parser, "Maximum_Score", data);
            m_maximum_score = new TSCORE[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_maximum_score[i] = data[i];
            
            return true;
        }
        else if (parser.isTagIdentifier("Probabilities"))
        {
            VectorDense<TPROBABILITY, unsigned int> data;
            unsigned int category_index;
            
            if (m_probabilities != 0)
            {
                m_probabilities = new TPROBABILITY * [this->m_number_of_categories];
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    m_probabilities[i] = 0;
            }
            
            category_index = parser.getAttribute("Category");
            loadVector(parser, "Probabilities", data);
            m_probabilities[category_index] = new TPROBABILITY[data.size()];
            for (unsigned int i = 0; i < data.size(); ++i)
                m_probabilities[category_index][i] = data[i];
            
            return true;
        }
        else return false;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ TRAIN AND RESCALE FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::train(const VectorDense<TSCORE, unsigned int> * /* score */, const VectorDense<unsigned int, unsigned int> * /* label */, unsigned int /* number_of_samples */, unsigned int /* number_of_threads */, BaseLogger * /* logger */)
    {
        throw srv::Exception("The probabilistic LUT only approximates an already trained probabilistic object, but it does not create the model on its own.");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::train(const VectorDense<unsigned int, unsigned int> * /* positive_histogram */, const VectorDense<unsigned int, unsigned int> * /* negative_histogram */, const VectorDense<TSCORE, unsigned int> &/*bin_scores*/, unsigned int /*number_of_threads*/, BaseLogger * /*logger*/)
    {
        throw srv::Exception("The probabilistic LUT only approximates an already trained probabilistic object, but it does not create the model on its own.");
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::approximate(const ProbabilisticScoreBase<TSCORE, TPROBABILITY> * model, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<TSCORE> minimum_scores, maximum_scores;
        VectorDense<TSCORE> * current_scores;
        VectorDense<TPROBABILITY> * current_probabilities;
        
        model->scores(0.0, minimum_scores, number_of_threads);
        model->scores(1.0, maximum_scores, number_of_threads);
        current_scores = new VectorDense<TSCORE>[m_number_of_bins];
        current_probabilities = new VectorDense<TPROBABILITY>[m_number_of_bins];
        
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            m_minimum_score[i] = (minimum_scores[i] < 0)?(minimum_scores[i] * 1.05):(minimum_scores[i] / 1.05);
            m_maximum_score[i] = (maximum_scores[i] > 0)?(maximum_scores[i] * 1.05):(maximum_scores[i] * 1.05);
        }
        for (unsigned int i = 0; i < m_number_of_bins; ++i)
        {
            current_scores[i].set(this->m_number_of_categories);
            for (unsigned int j = 0; j < this->m_number_of_categories; ++j)
                current_scores[i][j] = (TSCORE)(((double)i / (double)m_number_of_bins) * (double)(m_maximum_score[j] - m_minimum_score[j]) + (double)m_minimum_score[j]);
        }
        model->probabilities(current_scores, current_probabilities, m_number_of_bins, number_of_threads, logger);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            for (unsigned int j = 0; j < m_number_of_bins; ++j)
                m_probabilities[i][j] = current_probabilities[j][i];
        
        delete [] current_probabilities;
        delete [] current_scores;
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::probabilities(const VectorDense<TSCORE, unsigned int> * score, VectorDense<TPROBABILITY, unsigned int> * probability, unsigned int number_of_samples, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<TSCORE> difference(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            difference[i] = m_maximum_score[i] - m_minimum_score[i];
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            
            for (unsigned int i = thread_id; i < number_of_samples; i += number_of_threads)
            {
                if ((logger != 0) && (thread_id == 0) && (i % (10 * number_of_threads) == 0))
                    logger->log("Processing the sample %d of %d.", i + 1, number_of_samples);
                probability[i].set(this->m_number_of_categories);
                
                for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
                {
                    if (score[i][c] <= m_minimum_score[c]) probability[i][c] = 0;
                    else if (score[i][c] >= m_maximum_score[c]) probability[i][c] = (TPROBABILITY)this->m_probability_factor;
                    else probability[i][c] = (TPROBABILITY)(this->m_probability_factor * m_probabilities[c][(unsigned int)srvMax<double>(0.0, srvMin<double>((double)m_number_of_bins - 1.0, (double)m_number_of_bins * ((score[i][c] - m_minimum_score[c]) / difference[c])))]);
                }
            }
        }
        
        if (this->m_multiclass)
        {
            if (logger != 0) logger->log("Converting probabilities into multi-class estimates.");
            this->protectedMulticlass(probability, number_of_samples, number_of_threads);
        }
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::probabilities(const Image<TSCORE> &score, Image<TPROBABILITY> &probability, unsigned int number_of_threads) const
    {
        VectorDense<TSCORE> difference(this->m_number_of_categories);
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            difference[i] = m_maximum_score[i] - m_minimum_score[i];
        
        if ((void*)&score == (void*)&probability)
        {
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        TPROBABILITY * __restrict__ score_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr)
                        {
                            if (*score_ptr <= m_minimum_score[c]) *score_ptr = 0;
                            else if (*score_ptr >= m_maximum_score[c]) *score_ptr = (TPROBABILITY)this->m_probability_factor;
                            else *score_ptr = (TPROBABILITY)(this->m_probability_factor * m_probabilities[c][(unsigned int)srvMax<double>(0.0, srvMin<double>((double)m_number_of_bins - 1.0, (double)m_number_of_bins * ((*score_ptr - m_minimum_score[c]) / difference[c])))]);
                        }
                    }
                }
            }
        }
        else
        {
            probability.setGeometry(score);
            for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < score.getHeight(); y += number_of_threads)
                    {
                        const TSCORE * __restrict__ score_ptr = score.get(y, c);
                        TPROBABILITY * __restrict__ prob_ptr = probability.get(y, c);
                        for (unsigned int x = 0; x < score.getWidth(); ++x, ++score_ptr, ++prob_ptr)
                        {
                            if (*score_ptr <= m_minimum_score[c]) *prob_ptr = 0;
                            else if (*score_ptr >= m_maximum_score[c]) *prob_ptr = (TPROBABILITY)this->m_probability_factor;
                            else *prob_ptr = (TPROBABILITY)(this->m_probability_factor * m_probabilities[c][(unsigned int)srvMax<double>(0.0, srvMin<double>((double)m_number_of_bins - 1.0, (double)m_number_of_bins * ((*score_ptr - m_minimum_score[c]) / difference[c])))]);
                        }
                    }
                }
            }
        }
        
        if (this->m_multiclass) this->protectedMulticlass(probability, number_of_threads);
    }
    
    template <class TSCORE, class TPROBABILITY>
    void ProbabilisticLUT<TSCORE, TPROBABILITY>::scores(double probability, VectorDense<TSCORE, unsigned int> &score, unsigned int number_of_threads) const
    {
        probability = srvMin<double>(1.0 - 1e-12, srvMax<double>(1e-12, probability));
        score.set(this->m_number_of_categories);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < this->m_number_of_categories; i += number_of_threads)
            {
                unsigned int selected;
                double nearest;
                
                nearest = srvAbs<double>((double)probability - (double)m_probabilities[i][0]);
                selected = 0;
                for (unsigned int j = 1; j < m_number_of_bins; ++j)
                {
                    double current = srvAbs<double>((double)probability - (double)m_probabilities[i][j]);
                    if (current < nearest)
                    {
                        nearest = current;
                        selected = j;
                    }
                }
                score[i] = (TSCORE)(((double)selected / (double)m_number_of_bins) * (double)(m_maximum_score[i] - m_minimum_score[i]) + (double)m_minimum_score[i]);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

