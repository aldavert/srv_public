// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_BASE_SUPPORT_VECTOR_MACHINE_CLASSIFIER_HEADER_FILE__
#define __SRV_BASE_SUPPORT_VECTOR_MACHINE_CLASSIFIER_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | BASE SUPPORT VECTOR MACHINE          |
    //                   | CLASSIFIER CLASS - DECLARATION       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Implementation of the base functionalities of the linear support vector machines.
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class LinearSupportVectorMachineBase : public LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LinearSupportVectorMachineBase(void) : LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor which initializes the dimensionality and the category label of each classifier.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         */
        LinearSupportVectorMachineBase(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations) :
            LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult),
            m_epsilon(epsilon),
            m_maximum_number_of_iterations(maximum_number_of_iterations) {}
        /// Copy constructor.
        LinearSupportVectorMachineBase(const LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
            m_epsilon(other.m_epsilon),
            m_maximum_number_of_iterations(other.m_maximum_number_of_iterations) {}
        /// Destructor.
        virtual ~LinearSupportVectorMachineBase(void) {};
        /// Assignation operator.
        LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other)
            {
                LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
                m_epsilon = other.m_epsilon;
                m_maximum_number_of_iterations = other.m_maximum_number_of_iterations;
            }
            
            return *this;
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** This function sets the dimensionality and number of categories of the linear classifiers.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifiers.
         *  \param[in] categories_identifiers array with the identifiers of each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of linear classifiers.
         */
        void setClassifierParameters(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories);
        /// Returns the tolerance of the termination criterion.
        inline TCLASSIFIER getEpsilon(void) const { return m_epsilon; }
        /// Returns a reference to the tolerance of the termination criterion.
        inline TCLASSIFIER& getEpsilon(void) { return m_epsilon; }
        /// Sets the tolerance of the termination criterion.
        inline void setEpsilon(TCLASSIFIER epsilon) { m_epsilon = epsilon; }
        
        /// Returns the maximum number of iterations of the optimization algorithm.
        inline unsigned int getMaximumNumberOfIterations(void) const { return m_maximum_number_of_iterations; }
        /// Returns a reference to the maximum number of iterations of the optimization algorithm.
        inline unsigned int& getMaximumNumberOfIterations(void) { return m_maximum_number_of_iterations; }
        /// Sets the maximum number of iterations of the optimization algorithm.
        inline void setMaximumNumberOfIterations(unsigned int maximum_number_of_iterations) { m_maximum_number_of_iterations = maximum_number_of_iterations; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of a linear support vector machine into an XML object.
        virtual void convertToXML(XmlParser &parser) const;
        /// Retrieves the information of a linear support vector machine from an XML object.
        virtual void convertFromXML(XmlParser &parser);
    protected:
        /// Tolerance of the termination criterion.
        TCLASSIFIER m_epsilon;
        /// Maximum number of iterations of the optimization algorithm.
        unsigned int m_maximum_number_of_iterations;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BASE SUPPORT VECTOR MACHINE          |
    //                   | CLASSIFIER CLASS - IMPLEMENTATION    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::setClassifierParameters(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (this->m_categories_identifiers != 0) { delete [] this->m_categories_identifiers; this->m_categories_identifiers = 0; }
        if (this->m_classifiers != 0) { delete [] this->m_classifiers; this->m_classifiers = 0; }
        if (this->m_base_parameters != 0) { delete [] this->m_base_parameters; this->m_base_parameters = 0; }
        
        // -[ Set the new parameters ]---------------------------------------------------------------------------------------------------------------
        this->m_number_of_dimensions = number_of_dimensions;
        this->m_number_of_categories = number_of_categories;
        if (number_of_categories > 0)
        {
            this->m_categories_identifiers = new NLABEL[number_of_categories];
            this->m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_categories];
            this->m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[number_of_categories];
            
            for (unsigned int i = 0; i < number_of_categories; ++i)
            {
                this->m_categories_identifiers[i] = categories_identifiers[i];
                this->m_classifiers[i].set(number_of_dimensions, (TCLASSIFIER)0);
                this->m_base_parameters[i].set((TCLASSIFIER)1.0, (TCLASSIFIER)0.5, (TCLASSIFIER)1.0, (TCLASSIFIER)1.0);
            }
        }
        else
        {
            this->m_categories_identifiers = 0;
            this->m_classifiers = 0;
            this->m_base_parameters = 0;
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Classifier_Information");
        parser.setAttribute("Identifier", this->getIdentifier());
        parser.setAttribute("Number_Of_Dimensions", this->m_number_of_dimensions);
        parser.setAttribute("Number_Of_Categories", this->m_number_of_categories);
        parser.setAttribute("Positive_Probability", this->m_positive_probability);
        parser.setAttribute("Difficult_Probability", this->m_difficult_probability);
        parser.setAttribute("Ignore_Difficult", this->m_ignore_difficult);
        parser.setAttribute("Epsilon", m_epsilon);
        parser.setAttribute("Maximum_Iteration", m_maximum_number_of_iterations);
        if (this->m_number_of_categories > 0)
        {
            parser.addChildren();
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            {
                parser.openTag("Category_Classifier");
                parser.setAttribute("Category_Index", i);
                parser.setAttribute("Category_Identifier", this->m_categories_identifiers[i]);
                parser.addChildren();
                this->m_base_parameters[i].convertToXML(parser);
                saveVector(parser, "Weights", this->m_classifiers[i]);
                parser.closeTag();
            }
        }
        parser.closeTag();
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Classifier_Information") && ((LINEAR_LEARNER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getIdentifier()))
        {
            // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
            if (this->m_categories_identifiers != 0) { delete [] this->m_categories_identifiers; this->m_categories_identifiers = 0; }
            if (this->m_classifiers != 0) { delete [] this->m_classifiers; this->m_classifiers = 0; }
            if (this->m_base_parameters != 0) { delete [] this->m_base_parameters; this->m_base_parameters = 0; }
            
            // -[ Set the new parameters ]---------------------------------------------------------------------------------------------------------------
            this->m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            this->m_number_of_categories = parser.getAttribute("Number_Of_Categories");
            this->m_positive_probability = parser.getAttribute("Positive_Probability");
            this->m_difficult_probability = parser.getAttribute("Difficult_Probability");
            this->m_ignore_difficult = parser.getAttribute("Ignore_Difficult");
            m_epsilon = parser.getAttribute("Epsilon");
            m_maximum_number_of_iterations = parser.getAttribute("Maximum_Iteration");
            
            if (this->m_number_of_categories > 0)
            {
                this->m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[this->m_number_of_categories];
                this->m_categories_identifiers = new NLABEL[this->m_number_of_categories];
                this->m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[this->m_number_of_categories];
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                {
                    this->m_classifiers[i].set(this->m_number_of_dimensions, (TCLASSIFIER)0);
                    this->m_categories_identifiers[i] = (NLABEL)i;
                }
            }
            
            // -[ Retrieve the information from the XML object ]-----------------------------------------------------------------------------------------
            while (!(parser.isTagIdentifier("Classifier_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Category_Classifier"))
                {
                    unsigned int current_index;
                    
                    current_index = parser.getAttribute("Category_Index");
                    this->m_categories_identifiers[current_index] = parser.getAttribute("Category_Identifier");
                    
                    while (!(parser.isTagIdentifier("Category_Classifier") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Base_Parameters"))
                            this->m_base_parameters[current_index].convertFromXML(parser);
                        else if (parser.isTagIdentifier("Weights"))
                            loadVector(parser, "Weights", this->m_classifiers[current_index]);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

