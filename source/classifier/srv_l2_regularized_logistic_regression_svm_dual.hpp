// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_L2_REGULARIZED_LOGISTIC_REGRESSION_DUAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_L2_REGULARIZED_LOGISTIC_REGRESSION_DUAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED LOGISTIC REGRESSION   |
    //                   | IN THE DUAL SVM - DECLARATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the L2-Regularized Logistic Regression
     *  problem solved in the dual proposed in: H.F. Yu, F.L. Huang, C.J. Lin, <b>"Dual Coordinate Descent Methods for
     *  Logistic Regression and Maximum Entropy Models"</b>, <i>Machine Learning, 85(1-2):41-75, October 2011.</i>.
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class L2RegularizedLogisticRegressionDualSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        L2RegularizedLogisticRegressionDualSVM(void) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         */
        L2RegularizedLogisticRegressionDualSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations) {}
        /// Copy constructor.
        L2RegularizedLogisticRegressionDualSVM(const L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedLogisticRegressionDualSVM(const L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Destructor.
        virtual ~L2RegularizedLogisticRegressionDualSVM(void) {}
        /// Assignation operator.
        inline L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other) LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         *  \note This algorithm is only parallelized by category, therefore no parallelization is done when the classifier has a single category.
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_DUAL; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_DUAL; }
    protected:
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED LOGISTIC REGRESSION   |
    //                   | IN THE DUAL SVM - IMPLEMENTATION     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        VectorDense<TCLASSIFIER, unsigned int> sample_norm(number_of_train_samples);
        TCLASSIFIER inner_epsilon = _C(1e-2);
        const unsigned int maximum_inner_iteration = 100;
        const TCLASSIFIER minimum_inner_epsilon = srvMin<TCLASSIFIER>(inner_epsilon, _C(1e-8));
        const TCLASSIFIER eta = _C(0.1);
        
        if ((number_of_threads > 1) && (logger != 0))
            logger->log("WARNING: %d threads are used to concurrently calculate the classifiers of the %d categories, therefore output information messages can be mixed.", number_of_threads, this->m_number_of_categories);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
            {
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(train_samples[i]->getData());
                
                sample_norm[i] = _C(0);
                for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                {
                    const TCLASSIFIER current_value = _C(current_sample.getValue(p));
                    sample_norm[i] += current_value * current_value;
                }
            }
        }
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<char> samples_sign(number_of_train_samples);
            VectorDense<TCLASSIFIER, unsigned int> alpha(2 * number_of_train_samples);
            VectorDense<unsigned int> index(number_of_train_samples);
            unsigned int active_size, iteration, newton_iteration, inner_iteration;
            TCLASSIFIER Gmax;
            
            for (unsigned int category_id = omp_get_thread_num(); category_id < this->m_number_of_categories; category_id += number_of_threads)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                
                active_size = 0;
                this->m_classifiers[category_id].setValue(_C(0));
                for (unsigned int i = 0; i < number_of_train_samples; ++i)
                {
                    ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(train_samples[i]->getData());
                    TLABEL probability;
                    
                    probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                    
                    if (this->m_ignore_difficult)
                    {
                        if (probability >= this->m_difficult_probability)
                        {
                            alpha[2 * i] = srvMin<TCLASSIFIER>(_C(0.001) * positive_weight, _C(1e-8));
                            alpha[2 * i + 1] = positive_weight - alpha[2 * i];
                            samples_sign[i] = 1;
                            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                this->m_classifiers[category_id][current_sample.getIndex(p)] += alpha[2 * i] * _C(current_sample.getValue(p));
                            index[active_size] = i;
                            ++active_size;
                        }
                        else if (probability < this->m_positive_probability)
                        {
                            alpha[2 * i] = srvMin<TCLASSIFIER>(_C(0.001) * negative_weight, _C(1e-8));
                            alpha[2 * i + 1] = negative_weight - alpha[2 * i];
                            samples_sign[i] = -1;
                            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                this->m_classifiers[category_id][current_sample.getIndex(p)] -= alpha[2 * i] * _C(current_sample.getValue(p));
                            index[active_size] = i;
                            ++active_size;
                        }
                        else samples_sign[i] = 0;
                    }
                    else
                    {
                        if (probability >= this->m_positive_probability)
                        {
                            alpha[2 * i] = srvMin<TCLASSIFIER>(_C(0.001) * positive_weight, _C(1e-8));
                            alpha[2 * i + 1] = positive_weight - alpha[2 * i];
                            samples_sign[i] = 1;
                            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                this->m_classifiers[category_id][current_sample.getIndex(p)] += alpha[2 * i] * _C(current_sample.getValue(p));
                        }
                        else
                        {
                            alpha[2 * i] = srvMin<TCLASSIFIER>(_C(0.001) * negative_weight, _C(1e-8));
                            alpha[2 * i + 1] = negative_weight - alpha[2 * i];
                            samples_sign[i] = -1;
                            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                this->m_classifiers[category_id][current_sample.getIndex(p)] -= alpha[2 * i] * _C(current_sample.getValue(p));
                        }
                        index[active_size] = i;
                        ++active_size;
                    }
                }
                
                for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                {
                    Gmax = _C(0);
                    newton_iteration = 0;
                    for (unsigned int i = 0; i < active_size; ++i)
                        srvSwap(index[i], index[i + rand() % (active_size - i)]);
                    
                    for (unsigned int i = 0; i < active_size; ++i)
                    {
                        const unsigned int current_index = index[i];
                        ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(train_samples[current_index]->getData());
                        unsigned int first_index, second_index;
                        TCLASSIFIER score, C, a, b, sign, alpha_old, z, gp;
                        
                        score = this->classifierDot(this->m_classifiers[category_id], current_sample);
                        if (samples_sign[current_index] > 0) C = positive_weight;
                        else
                        {
                            C = negative_weight;
                            score = -score;
                        }
                        a = sample_norm[current_index];
                        b = score;
                        sign = _C(1.0);
                        first_index = 2 * current_index;
                        second_index = 2 * current_index + 1;
                        if (_C(0.5) * a * (alpha[second_index] - alpha[first_index]) + b < _C(0))
                        {
                            srvSwap(first_index, second_index);
                            sign = -_C(1.0);
                        }
                        z = alpha_old = alpha[first_index];
                        if (C - z < _C(0.5) * C) z = _C(0.1) * z;
                        gp = a * (z - alpha_old) + sign * b + std::log(z / (C - z));
                        Gmax = srvMax<TCLASSIFIER>(Gmax, srvAbs<TCLASSIFIER>(gp));
                        for (inner_iteration = 0; inner_iteration < maximum_inner_iteration; ++inner_iteration)
                        {
                            TCLASSIFIER gpp, tmpz;
                            
                            if (srvAbs<TCLASSIFIER>(gp) < inner_epsilon)
                                break;
                            gpp = a + C / (C - z) / z;
                            tmpz = z - gp / gpp;
                            if (tmpz <= _C(0))  z *= eta;
                            else z = tmpz;
                            gp = a * (z - alpha_old) + sign * b + std::log(z / (C - z));
                            
                            ++newton_iteration;
                        }
                        
                        if (inner_iteration > 0)
                        {
                            const TCLASSIFIER value = sign * (z  - alpha_old);
                            alpha[first_index] = z;
                            alpha[second_index] = C - z;
                            if (samples_sign[current_index] > 0)
                                for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                    this->m_classifiers[category_id][current_sample.getIndex(p)] += value * _C(current_sample.getValue(p));
                            else
                                for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                    this->m_classifiers[category_id][current_sample.getIndex(p)] -= value * _C(current_sample.getValue(p));
                        }
                    }
                    
                    if (Gmax < this->m_epsilon) break;
                    if (newton_iteration <= active_size / 10) inner_epsilon = srvMax<TCLASSIFIER>(minimum_inner_epsilon, _C(0.1) * inner_epsilon);
                    if ((iteration % 10 == 0) && (logger != 0))
                        logger->log("Iteration %d: Gmax=%e Newton iterations=%d.", iteration + 1, (double)Gmax, newton_iteration + 1);
                }
                
                if (logger != 0)
                {
                    TCLASSIFIER v = _C(0);
                    
                    logger->log("Optimization algorithm finished in %d iterations.", iteration);
                    if (iteration >= this->m_maximum_number_of_iterations)
                        logger->log("WARNING: Reaching maximum number of iterations.");
                    
                    // calculate objective value
                    for (NSAMPLE i = 0; i < this->m_classifiers[category_id].size(); ++i)
                        v += this->m_classifiers[category_id][i] * this->m_classifiers[category_id][i];
                    v *= _C(0.5);
                    for (unsigned int i = 0; i < active_size; ++i)
                    {
                        const unsigned int current_index = index[i];
                        TCLASSIFIER C;
                        
                        if (samples_sign[current_index] > 0) C = positive_weight;
                        else C = negative_weight;
                        v += alpha[2 * current_index] * std::log(alpha[2 * current_index]) + alpha[2 * current_index + 1] * std::log(alpha[2 * current_index + 1]) - C * std::log(C);
                    }
                    logger->log("Objective value = %lf", (double)v);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

