// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_L2_REGULARIZED_LOGISTIC_REGRESSION_PRIMAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_L2_REGULARIZED_LOGISTIC_REGRESSION_PRIMAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED LOGISTIC REGRESSION   |
    //                   | IN THE PRIMAL SVM - DECLARATION      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the L2-Regularized Logistic Regression
     *  problem solved in the primal proposed in: C.J. Lin, R.C. Weng, S.S. Keerthi, <b>"Trust Region Newton Method for Large-Scale
     *  Logistic Regression"</b>, <i>Journal of Machine Learning Research, 9:627-650, 2008.</i>.
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class L2RegularizedLogisticRegressionPrimalSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        L2RegularizedLogisticRegressionPrimalSVM(void) : LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor which initializes the dimensionality and the category label of each classifier.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         */
        L2RegularizedLogisticRegressionPrimalSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations) {}
        /// Copy constructor.
        L2RegularizedLogisticRegressionPrimalSVM(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedLogisticRegressionPrimalSVM(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Destructor.
        virtual ~L2RegularizedLogisticRegressionPrimalSVM(void) {}
        /// Assignation operator.
        inline L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other) LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_PRIMAL; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_PRIMAL; }
    protected:
        // -[ Auxiliary functions ]------------------------------------------------------------------------------------------------------------------
        /// Object which stores the information of the problem solved by the classifier.
        class Problem
        {
        public:
            // -[ Constructors, destructor and assignation operators ]-------------------------------------------------------------------------------
            /// Default constructor.
            Problem(void);
            /** Constructor which initializes the problem object.
             *  \param[in] samples array of constant pointers to the samples used to train the linear classifier.
             *  \param[in] number_of_samples number of elements in the samples array.
             *  \param[in] number_of_dimensions number of dimensions of the classifier weight vector.
             *  \param[in] number_of_threads number of threads used to concurrently process the sample vectors.
             */
            Problem(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE number_of_dimensions, unsigned int number_of_threads);
            /// Copy constructor.
            Problem(const Problem &other);
            /// Destructor.
            ~Problem(void);
            /// Assignation operator.
            Problem& operator=(const Problem &other);
            /** Function which sets the problem object.
             *  \param[in] samples array of constant pointers to the samples used to train the linear classifier.
             *  \param[in] number_of_samples number of elements in the samples array.
             *  \param[in] number_of_dimensions number of dimensions of the classifier weight vector.
             *  \param[in] number_of_threads number of threads used to concurrently process the sample vectors.
             */
            void set(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE number_of_dimensions, unsigned int number_of_threads);
            
            // -[ Problem solve functions ]----------------------------------------------------------------------------------------------------------
            /** Initializes the training problem for the selected category.
             *  \param[in] parameters base parameters of the classifier.
             *  \param[in] positive_label identifier of the positive label.
             *  \param[in] positive_probability minimum probability to assign a sample to the positive set of samples.
             *  \param[in] difficult_probability maximum probability of a sample to be tagged as difficult.
             *  \param[in] ignore_difficult the algorithm ignores the difficult samples when true.
             */
            void initialize(const ClassifierBaseParameters<TCLASSIFIER> &parameters, NLABEL positive_label, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult);
            /// Calculate the classification score of each training sample and returns a classification error measure.
            TCLASSIFIER calculateError(const VectorDense<TCLASSIFIER, NSAMPLE> &current_classifier);
            /// Calculates the gradient of the classification vector.
            void calculateGradient(const VectorDense<TCLASSIFIER, NSAMPLE> &current_classifier, VectorDense<TCLASSIFIER, NSAMPLE> &gradient);
            /// Calculates the Hessian-vector product values.
            void calculateHv(const VectorDense<TCLASSIFIER, NSAMPLE> &classifier_update, VectorDense<TCLASSIFIER, NSAMPLE> &Hs);
            /// Returns the 'normalized' epsilon value.
            TCLASSIFIER calculateEpsilon(TCLASSIFIER epsilon) const;
        private:
            /// Auxiliary function which converts any value to the classifier vector type format.
            template <typename VALUE>
            inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
            /** Calculates the dot-product between the weight vector of a classifier and a sample.
             *  \param[in] classifier classifier weight vector.
             *  \param[in] sample sample feature vector.
             *  \returns dot product between the classifier and the sample feature vector.
             */
            inline TCLASSIFIER classifierDot(const VectorDense<TCLASSIFIER, NSAMPLE> &classifier, const VectorSparse<TSAMPLE, NSAMPLE> &sample) const
            {
                TCLASSIFIER dot = _C(0);
                for (NSAMPLE i = 0; i < sample.size(); ++i)
                    dot += classifier[sample.getIndex(i)] * _C(sample.getValue(i));
                return dot;
            }
            /// Constant vector to the training samples of the classification problem.
            ConstantSubVectorDense<Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const *> m_samples;
            /// Score of the linear classifier and the loss function.
            TCLASSIFIER * m_z;
            /// Weight of the gradient updates.
            TCLASSIFIER * m_D;
            /// Array which stores the sign of the training samples.
            char * m_samples_sign;
            /// Number of dimensions of the classifier.
            NSAMPLE m_number_of_dimensions;
            /// Pointer to the parameters of the current classifier problem.
            const ClassifierBaseParameters<TCLASSIFIER> * m_parameters;
            /// Auxiliary array of vectors used to process samples concurrently.
            VectorDense<TCLASSIFIER, NSAMPLE> * m_thread_vectors;
            /// Auxiliary array of values used to process samples concurrently.
            TCLASSIFIER * m_thread_values;
            /// Number of threads used to concurrently process the training samples.
            unsigned int m_number_of_threads;
        };
        /// Implements the conjugate gradient procedure to approximately solve the trust region sub-problem.
        unsigned int trustRegionConjugateGradient(Problem &problem, TCLASSIFIER delta, VectorDense<TCLASSIFIER, NSAMPLE> &gradient, VectorDense<TCLASSIFIER, NSAMPLE> &update, VectorDense<TCLASSIFIER, NSAMPLE> &r, VectorDense<TCLASSIFIER, NSAMPLE> &d, VectorDense<TCLASSIFIER, NSAMPLE> &Hd, unsigned int number_of_threads, BaseLogger * logger);
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED LOGISTIC REGRESSION   |
    //                   | IN THE PRIMAL SVM - IMPLEMENTATION   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::Problem(void) :
        m_samples(0, 0),
        m_z(0),
        m_D(0),
        m_samples_sign(0),
        m_number_of_dimensions(0),
        m_parameters(0),
        m_thread_vectors(0),
        m_thread_values(0),
        m_number_of_threads(1)
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::Problem(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE number_of_dimensions, unsigned int number_of_threads) :
        m_samples(samples, number_of_samples),
        m_z((number_of_samples > 0)?new TCLASSIFIER[number_of_samples]:0),
        m_D((number_of_samples > 0)?new TCLASSIFIER[number_of_samples]:0),
        m_samples_sign((number_of_samples > 0)?new char[number_of_samples]:0),
        m_number_of_dimensions(number_of_dimensions),
        m_parameters(0),
        m_thread_vectors((number_of_threads > 1)?new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_threads]:0),
        m_thread_values((number_of_threads > 1)?new TCLASSIFIER[number_of_threads]:0),
        m_number_of_threads(number_of_threads)
    {
        if (number_of_threads > 1)
            for (unsigned int i = 0; i < number_of_threads; ++i)
                m_thread_vectors[i].set(number_of_dimensions);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::Problem(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem &other) :
        m_samples(other.m_samples),
        m_z((other.m_z != 0)?new TCLASSIFIER[other.m_samples.size()]:0),
        m_D((other.m_D != 0)?new TCLASSIFIER[other.m_samples.size()]:0),
        m_samples_sign((other.m_samples_sign != 0)?new char[other.m_samples.size()]:0),
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_parameters(other.m_parameters),
        m_thread_vectors((other.m_thread_vectors != 0)?new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_threads]:0),
        m_thread_values((other.m_thread_values != 0)?new TCLASSIFIER[other.m_number_of_threads]:0),
        m_number_of_threads(other.m_number_of_threads)
    {
        for (unsigned int i = 0; i < other.m_samples.size(); ++i)
        {
            m_z[i] = other.m_z[i];
            m_D[i] = other.m_D[i];
            m_samples_sign[i] = other.m_samples_sign[i];
        }
        if (other.m_number_of_threads > 1)
        {
            for (unsigned int i = 0; i < other.m_number_of_threads; ++i)
            {
                m_thread_vectors[i] = other.m_thread_vectors[i];
                m_thread_values[i] = other.m_thread_values[i];
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::~Problem(void)
    {
        if (m_z != 0) delete [] m_z;
        if (m_D != 0) delete [] m_D;
        if (m_samples_sign != 0) delete [] m_samples_sign;
        if (m_thread_vectors != 0) delete [] m_thread_vectors;
        if (m_thread_values != 0) delete [] m_thread_values;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    typename L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem& L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::operator=(const L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem &other)
    {
        if (this != &other)
        {
            if (m_z != 0) delete [] m_z;
            if (m_D != 0) delete [] m_D;
            if (m_samples_sign != 0) delete [] m_samples_sign;
            if (m_thread_vectors != 0) delete [] m_thread_vectors;
            if (m_thread_values != 0) delete [] m_thread_values;
            
            m_number_of_dimensions = other.m_number_of_dimensions;
            m_parameters = other.m_parameters;
            if (other.m_samples.size() > 0)
            {
                m_samples = other.m_samples;
                m_z = new TCLASSIFIER[other.m_samples.size()];
                m_D = new TCLASSIFIER[other.m_samples.size()];
                m_samples_sign = new bool[other.m_samples.size()];
                for (unsigned int i = 0; i < other.m_samples.size(); ++i)
                {
                    m_z[i] = other.m_z[i];
                    m_D[i] = other.m_D[i];
                    m_samples_sign[i] = other.m_samples_sign[i];
                }
            }
            else
            {
                m_samples.set(0, 0);
                m_z = 0;
                m_D = 0;
                m_samples_sign = 0;
            }
            m_number_of_threads = other.m_number_of_threads;
            if (other.m_number_of_threads > 1)
            {
                m_thread_vectors = new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_threads];
                m_thread_values = new TCLASSIFIER[other.m_number_of_threads];
                for (unsigned int i = 0; i < other.m_number_of_threads; ++i)
                {
                    m_thread_vectors[i] = other.m_thread_vectors[i];
                    m_thread_values[i] = other.m_thread_values[i];
                }
            }
            else
            {
                m_thread_vectors = 0;
                m_thread_values = 0;
            }
        }
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::set(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * samples, unsigned int number_of_samples, NSAMPLE number_of_dimensions, unsigned int number_of_threads)
    {
        if (m_z != 0) delete [] m_z;
        if (m_D != 0) delete [] m_D;
        if (m_samples_sign != 0) delete [] m_samples_sign;
        if (m_thread_vectors != 0) delete [] m_thread_vectors;
        if (m_thread_values != 0) delete [] m_thread_values;
            
        m_number_of_dimensions = number_of_dimensions;
        m_parameters = 0;
        if (number_of_samples > 0)
        {
            m_samples.set(samples, number_of_samples);
            m_z = new TCLASSIFIER[number_of_samples];
            m_D = new TCLASSIFIER[number_of_samples];
            m_samples_sign = new char[number_of_samples];
        }
        else
        {
            m_samples.set(0, 0);
            m_z = 0;
            m_D = 0;
            m_samples_sign = 0;
        }
        
        m_number_of_threads = number_of_threads;
        if (number_of_threads > 1)
        {
            m_thread_vectors = new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_threads];
            m_thread_values = new TCLASSIFIER[number_of_threads];
            for (unsigned int i = 0; i < number_of_threads; ++i)
                m_thread_vectors[i].set(number_of_dimensions);
        }
        else
        {
            m_thread_vectors = 0;
            m_thread_values = 0;
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::initialize(const ClassifierBaseParameters<TCLASSIFIER> &parameters, NLABEL positive_label, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult)
    {
        if (m_number_of_threads > 1)
        {
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < m_samples.size(); i += m_number_of_threads)
                {
                    TLABEL probability;
                    
                    probability = m_samples[i]->getLabel(positive_label);
                    if (ignore_difficult)
                    {
                        if (probability >= difficult_probability) m_samples_sign[i] = 1;
                        else if (probability < positive_probability) m_samples_sign[i] = -1;
                        else m_samples_sign[i] = 0;
                    }
                    else
                    {
                        if (probability >= positive_probability) m_samples_sign[i] = 1;
                        else m_samples_sign[i] = -1;
                    }
                }
            }
        }
        else
        {
            for (unsigned int i = 0; i < m_samples.size(); ++i)
            {
                TLABEL probability;
                
                probability = m_samples[i]->getLabel(positive_label);
                if (ignore_difficult)
                {
                    if (probability >= difficult_probability) m_samples_sign[i] = 1;
                    else if (probability < positive_probability) m_samples_sign[i] = -1;
                    else m_samples_sign[i] = 0;
                }
                else
                {
                    if (probability >= positive_probability) m_samples_sign[i] = 1;
                    else m_samples_sign[i] = -1;
                }
            }
        }
        m_parameters = &parameters;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    TCLASSIFIER L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::calculateError(const VectorDense<TCLASSIFIER, NSAMPLE> &current_classifier)
    {
        const TCLASSIFIER positive_weight = m_parameters->getBalancingFactor() * m_parameters->getPositiveWeight() * m_parameters->getRegularizationFactor();
        const TCLASSIFIER negative_weight = (_C(1.0) - m_parameters->getBalancingFactor()) * m_parameters->getNegativeWeight() * m_parameters->getRegularizationFactor();
        TCLASSIFIER error;
        
        if (m_number_of_threads > 1)
        {
            error = _C(0);
            
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                m_thread_values[thread_id] = 0;
                for (NSAMPLE index = (NSAMPLE)thread_id; index < m_number_of_dimensions; index = (NSAMPLE)(index + m_number_of_threads))
                    m_thread_values[thread_id] += current_classifier[index] * current_classifier[index];
                m_thread_values[thread_id] /= _C(2.0);
                
                for (unsigned int index = thread_id; index < m_samples.size(); index += m_number_of_threads)
                {
                    TCLASSIFIER sign_score, current_weight;
                    
                    m_z[index] = classifierDot(current_classifier, m_samples[index]->getData());
                    if (m_samples_sign[index] > 0)
                    {
                        sign_score = m_z[index];
                        current_weight = positive_weight;
                    }
                    else if (m_samples_sign[index] < 0)
                    {
                        sign_score = -m_z[index];
                        current_weight = negative_weight;
                    }
                    else
                    {
                        sign_score = _C(0);
                        current_weight = _C(0);
                        m_z[index] = _C(0);
                    }
                    if (sign_score >= _C(0)) m_thread_values[thread_id] += current_weight * std::log(_C(1.0) + std::exp(-sign_score));
                    else m_thread_values[thread_id] += current_weight * (-sign_score + std::log(_C(1.0) + std::exp(sign_score)));
                }
            }
            
            for (unsigned int thread_id = 0; thread_id < m_number_of_threads; ++thread_id)
                error += m_thread_values[thread_id];
        }
        else
        {
            error = _C(0);
            for (NSAMPLE index = 0; index < m_number_of_dimensions; ++index)
                error += current_classifier[index] * current_classifier[index];
            error /= _C(2.0);
            
            for (unsigned int index = 0; index < m_samples.size(); ++index)
            {
                TCLASSIFIER sign_score, current_weight;
                
                m_z[index] = classifierDot(current_classifier, m_samples[index]->getData());
                if (m_samples_sign[index] > 0)
                {
                    sign_score = m_z[index];
                    current_weight = positive_weight;
                }
                else if (m_samples_sign[index] < 0)
                {
                    sign_score = -m_z[index];
                    current_weight = negative_weight;
                }
                else
                {
                    sign_score = _C(0);
                    current_weight = _C(0);
                    m_z[index] = _C(0);
                }
                if (sign_score >= _C(0)) error += current_weight * std::log(_C(1.0) + std::exp(-sign_score));
                else error += current_weight * (-sign_score + std::log(_C(1.0) + std::exp(sign_score)));
            }
        }
        
        return error;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::calculateGradient(const VectorDense<TCLASSIFIER, NSAMPLE> &current_classifier, VectorDense<TCLASSIFIER, NSAMPLE> &gradient)
    {
        const TCLASSIFIER positive_weight = m_parameters->getBalancingFactor() * m_parameters->getPositiveWeight() * m_parameters->getRegularizationFactor();
        const TCLASSIFIER negative_weight = (_C(1.0) - m_parameters->getBalancingFactor()) * m_parameters->getNegativeWeight() * m_parameters->getRegularizationFactor();
        
        if (m_number_of_threads > 1)
        {
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                m_thread_vectors[thread_id].setValue(_C(0));
                for (unsigned int index = thread_id; index < m_samples.size(); index += m_number_of_threads)
                {
                    ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(m_samples[index]->getData());
                    
                    if (m_samples_sign[index] > 0)
                    {
                        m_z[index] = _C(1.0) / (_C(1.0) + std::exp(-m_z[index]));
                        m_D[index] = m_z[index] * (_C(1.0) - m_z[index]);
                        m_z[index] = positive_weight * (m_z[index] - _C(1.0));
                    }
                    else if (m_samples_sign[index] < 0)
                    {
                        m_z[index] = _C(1.0) / (_C(1.0) + std::exp(m_z[index]));
                        m_D[index] = m_z[index] * (_C(1.0) - m_z[index]);
                        m_z[index] = -negative_weight * (m_z[index] - _C(1.0));
                    }
                    else
                    {
                        m_z[index] = _C(0);
                        m_D[index] = _C(0);
                    }
                    
                    for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                        m_thread_vectors[thread_id][current_sample.getIndex(p)] += m_z[index] * _C(current_sample.getValue(p));
                }
            }
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                for (NSAMPLE index = (NSAMPLE)thread_id; index < m_number_of_dimensions; index = (NSAMPLE)(index + m_number_of_threads))
                {
                    gradient[index] = current_classifier[index];
                    for (unsigned int t = 0; t < m_number_of_threads; ++t)
                        gradient[index] += m_thread_vectors[t][index];
                }
            }
        }
        else
        {
            gradient.copy(current_classifier);
            VectorNorm debug_norm(srv::MANHATTAN_NORM);
            
            for (unsigned int index = 0; index < m_samples.size(); ++index)
            {
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(m_samples[index]->getData());
                
                if (m_samples_sign[index] > 0)
                {
                    m_z[index] = _C(1.0) / (_C(1.0) + std::exp(-m_z[index]));
                    m_D[index] = m_z[index] * (_C(1.0) - m_z[index]);
                    m_z[index] = positive_weight * (m_z[index] - _C(1.0));
                }
                else if (m_samples_sign[index] < 0)
                {
                    m_z[index] = _C(1.0) / (_C(1.0) + std::exp(m_z[index]));
                    m_D[index] = m_z[index] * (_C(1.0) - m_z[index]);
                    m_z[index] = -negative_weight * (m_z[index] - _C(1.0));
                }
                else
                {
                    m_z[index] = _C(0);
                    m_D[index] = _C(0);
                }
                for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                    gradient[current_sample.getIndex(p)] += m_z[index] * _C(current_sample.getValue(p));
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::calculateHv(const VectorDense<TCLASSIFIER, NSAMPLE> &classifier_update, VectorDense<TCLASSIFIER, NSAMPLE> &Hs)
    {
        const TCLASSIFIER positive_weight = m_parameters->getBalancingFactor() * m_parameters->getPositiveWeight() * m_parameters->getRegularizationFactor();
        const TCLASSIFIER negative_weight = (_C(1.0) - m_parameters->getBalancingFactor()) * m_parameters->getNegativeWeight() * m_parameters->getRegularizationFactor();
        
        if (m_number_of_threads > 1)
        {
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                m_thread_vectors[thread_id].setValue(0);
                for (unsigned int index = thread_id; index < m_samples.size(); index += m_number_of_threads)
                {
                    ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(m_samples[index]->getData());
                    TCLASSIFIER wa;
                    
                    wa = classifierDot(classifier_update, current_sample);
                    if (m_samples_sign[index] > 0) wa = positive_weight * _C(m_D[index]) * wa;
                    else if (m_samples_sign[index] < 0) wa = negative_weight * _C(m_D[index]) * wa;
                    else wa = _C(0);
                    
                    for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                        m_thread_vectors[thread_id][current_sample.getIndex(p)] += wa * _C(current_sample.getValue(p));
                }
            }
            
            #pragma omp parallel num_threads(m_number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                for (NSAMPLE index = (NSAMPLE)thread_id; index < m_number_of_dimensions; index = (NSAMPLE)(index + m_number_of_threads))
                {
                    Hs[index] = classifier_update[index];
                    for (unsigned int t = 0; t < m_number_of_threads; ++t)
                        Hs[index] += m_thread_vectors[t][index];
                }
            }
        }
        else
        {
            Hs.copy(classifier_update);
            
            for (unsigned int index = 0; index < m_samples.size(); ++index)
            {
                ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(m_samples[index]->getData());
                TCLASSIFIER wa;
                
                wa = classifierDot(classifier_update, current_sample);
                if (m_samples_sign[index] > 0) wa = positive_weight * m_D[index] * wa;
                else if (m_samples_sign[index] < 0) wa = negative_weight * m_D[index] * wa;
                else wa = 0;
                
                for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                    Hs[current_sample.getIndex(p)] += wa * _C(current_sample.getValue(p));
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    TCLASSIFIER L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Problem::calculateEpsilon(TCLASSIFIER epsilon) const
    {
        unsigned int positive_samples, negative_samples, active_sample_size;
        positive_samples = 0;
        negative_samples = 0;
        active_sample_size = 0;
        if (m_number_of_threads > 1)
        {
            #pragma omp parallel num_threads(m_number_of_threads) reduction(+:positive_samples) reduction(+:negative_samples) reduction(+:active_sample_size)
            {
                for (unsigned int i = omp_get_thread_num(); i < m_samples.size(); i += m_number_of_threads)
                {
                    if (m_samples_sign[i] > 0)
                    {
                        ++positive_samples;
                        ++active_sample_size;
                    }
                    else if (m_samples_sign[i] < 0)
                    {
                        ++negative_samples;
                        ++active_sample_size;
                    }
                }
            }
        }
        else
        {
            for (unsigned int i = 0; i < m_samples.size(); ++i)
            {
                if (m_samples_sign[i] > 0)
                {
                    ++positive_samples;
                    ++active_sample_size;
                }
                else if (m_samples_sign[i] < 0)
                {
                    ++negative_samples;
                    ++active_sample_size;
                }
            }
        }
        return epsilon * srvMax<TCLASSIFIER>(_C(1.0), _C(srvMin<unsigned int>(positive_samples, negative_samples))) / _C(active_sample_size);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        // -[ Algorithms constants declarations ]----------------------------------------------------------------------------------------------------
        // Parameters for updating the iterates.
        const TCLASSIFIER eta0 = _C(1e-4);
        const TCLASSIFIER eta1 = _C(0.25);
        const TCLASSIFIER eta2 = _C(0.75);
        // Parameters for updating the trust region size delta.
        const TCLASSIFIER sigma1 = _C(0.25);
        const TCLASSIFIER sigma2 = _C(0.5);
        const TCLASSIFIER sigma3 = _C(4.0);
        
        // -[ Variables declaration ]----------------------------------------------------------------------------------------------------------------
        Problem problem(train_samples, number_of_train_samples, this->m_number_of_dimensions, number_of_threads);
        VectorDense<TCLASSIFIER, NSAMPLE> classifier_update(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> r(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> classifier_new(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> gradient(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> d(this->m_number_of_dimensions);
        VectorDense<TCLASSIFIER, NSAMPLE> Hd(this->m_number_of_dimensions);
        VectorNorm norm(EUCLIDEAN_NORM);
        TCLASSIFIER alpha;
        
        for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
        {
            TCLASSIFIER error, error_new, delta, gradient_norm, initial_gradient_norm, gradient_dot_update;
            TCLASSIFIER sr, prered, actred, classifier_update_norm, current_epsilon;
            
            // Get the label sign of each sample and initialize the weight vector.
            this->m_classifiers[category_id].set(this->m_number_of_dimensions, _C(0));
            problem.initialize(base_parameters[category_id], this->m_categories_identifiers[category_id], this->m_positive_probability, this->m_difficult_probability, this->m_ignore_difficult);
            current_epsilon = problem.calculateEpsilon(this->m_epsilon);
            
            error = problem.calculateError(this->m_classifiers[category_id]);
            problem.calculateGradient(this->m_classifiers[category_id], gradient);
            norm.norm(gradient, delta);
            gradient_norm = initial_gradient_norm = delta;
            
            if (!(gradient_norm < current_epsilon * initial_gradient_norm))
            {
                for (unsigned int iteration = 0; iteration < this->m_maximum_number_of_iterations;)
                {
                    unsigned int cg_iteration;
                    
                    cg_iteration = trustRegionConjugateGradient(problem, delta, gradient, classifier_update, r, d, Hd, number_of_threads, logger);
                    
                    classifier_new.copy(this->m_classifiers[category_id]);
                    classifier_new += classifier_update;
                    gradient_dot_update = this->classifierDot(gradient, classifier_update, number_of_threads);
                    sr = this->classifierDot(classifier_update, r, number_of_threads);
                    prered = -_C(0.5) * (gradient_dot_update - sr);
                    error_new = problem.calculateError(classifier_new);
                    
                    // Calculate the reduction.
                    actred = error - error_new;
                    
                    // On the first iteration, adjust the initial step bound.
                    norm.norm(classifier_update, classifier_update_norm);
                    if (iteration == 0) delta = srvMin<TCLASSIFIER>(delta, classifier_update_norm);
                    
                    // Compute prediction alpha * <classifier update norm> of the step.
                    if (error_new - error - gradient_dot_update <= 0) alpha = sigma3;
                    else alpha = srvMax<TCLASSIFIER>(sigma1, -_C(0.5) * (gradient_dot_update / (error_new - error - gradient_dot_update)));
                    
                    // Update the trust region bound according to the ratio of actual to predicted reduction.
                    if (actred < eta0 * prered)
                        delta = srvMin<TCLASSIFIER>(srvMax<TCLASSIFIER>(alpha, sigma1) * classifier_update_norm, sigma2 * delta);
                    else if (actred < eta1 * prered)
                        delta = srvMax<TCLASSIFIER>(sigma1 * delta, srvMin<TCLASSIFIER>(alpha * classifier_update_norm, sigma2 * delta));
                    else if (actred < eta2 * prered)
                        delta = srvMax<TCLASSIFIER>(sigma1 * delta, srvMin<TCLASSIFIER>(alpha * classifier_update_norm, sigma3 * delta));
                    else
                        delta = srvMax<TCLASSIFIER>(delta, srvMin<TCLASSIFIER>(alpha * classifier_update_norm, sigma3 * delta));
                    
                    if (logger != 0)
                        logger->log("iteration %04d: act %10.8e pre %10.8e delta %10.8e f %10.8e |g| %10.8e CG %3d", iteration + 1, (double)actred, (double)prered, (double)delta, (double)error, (double)gradient_norm, cg_iteration);
                    
                    if (actred > eta0 * prered)
                    {
                        ++iteration;
                        
                        this->m_classifiers[category_id].copy(classifier_new);
                        error = error_new;
                        problem.calculateGradient(classifier_new, gradient);
                        norm.norm(gradient, gradient_norm);
                        if (gradient_norm <= current_epsilon * initial_gradient_norm) break;
                    }
                    if (error < _C(-1.0e+32))
                    {
                        if (logger != 0)
                            logger->log("WARNING: f < -1.0e+32");
                        break;
                    }
                    if ((srvAbs<TCLASSIFIER>(actred) <= _C(0)) && (prered <= _C(0)))
                    {
                        if (logger != 0)
                            logger->log("WARNING: actred and prered <= 0");
                        break;
                    }
                    if ((srvAbs<TCLASSIFIER>(actred) <= _C(1.0e-12) * srvAbs<TCLASSIFIER>(error)) && (srvAbs<TCLASSIFIER>(prered) <= _C(1.0e-12) * srvAbs<TCLASSIFIER>(error)))
                    {
                        if (logger != 0)
                            logger->log("WARNING: actred and prered too small");
                        break;
                    }
                }
            }
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    unsigned int L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::trustRegionConjugateGradient(Problem &problem, TCLASSIFIER delta, VectorDense<TCLASSIFIER, NSAMPLE> &gradient, VectorDense<TCLASSIFIER, NSAMPLE> &update, VectorDense<TCLASSIFIER, NSAMPLE> &r, VectorDense<TCLASSIFIER, NSAMPLE> &d, VectorDense<TCLASSIFIER, NSAMPLE> &Hd, unsigned int number_of_threads, BaseLogger * logger)
    {
        TCLASSIFIER current_norm, current_dot, alpha, conjugate_gradient_tolerance, dot_r, dot_r_new;
        unsigned int iteration;
        VectorNorm norm(EUCLIDEAN_NORM);
        
        for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
        {
            update[i] = _C(0);
            r[i] = -gradient[i];
            d[i] = r[i];
        }
        norm.norm(gradient, current_norm);
        conjugate_gradient_tolerance = _C(0.1) * current_norm;
        
        dot_r = this->classifierDot(r, r, number_of_threads);
        for (iteration = 0; true; ++iteration)
        {
            //// if |r| < cgtool then break.
            norm.norm(r, current_norm);
            if (current_norm <= conjugate_gradient_tolerance) break;
            problem.calculateHv(d, Hd);
            
            //// alpha = <r, r> / <d, Hd>
            current_dot = this->classifierDot(d, Hd, number_of_threads);
            alpha = dot_r / current_dot;
            //// update = update + d * alpha;
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                update[i] += d[i] * alpha;
            
            //// if |update| > delta then,
            norm.norm(update, current_norm);
            if (current_norm > delta)
            {
                TCLASSIFIER update_dot_d, update_dot, d_dot, squared_delta, rad;
                
                if (logger != 0)
                    logger->log("Conjugate Gradient reaches trust region boundary");
                //// update = update - d * alpha;
                for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                    update[i] -= d[i] * alpha;
                
                update_dot_d = this->classifierDot(update, d, number_of_threads);
                update_dot = this->classifierDot(update, update, number_of_threads);
                d_dot = this->classifierDot(d, d, number_of_threads);
                squared_delta = delta * delta;
                rad = std::sqrt(update_dot_d * update_dot_d + d_dot * (squared_delta - update_dot));
                
                if (update_dot_d >= _C(0)) alpha = (squared_delta - update_dot) / (update_dot_d + rad);
                else alpha = (rad - update_dot_d) / d_dot;
                
                for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                {
                    update[i] += d[i] * alpha;
                    r[i] -= Hd[i] * alpha;
                }
                
                break;
            }
            
            for (NSAMPLE i = 0; i < this->m_number_of_dimensions; ++i)
                r[i] -= Hd[i] * alpha;
            dot_r_new = this->classifierDot(r, r, number_of_threads);
            d *= dot_r_new / dot_r;
            d += r;
            dot_r = dot_r_new;
        }
        
        return iteration;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

