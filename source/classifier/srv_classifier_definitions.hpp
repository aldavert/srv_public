// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLASSIFIER_DEFINITIONS_HEADER_FILE__
#define __SRV_CLASSIFIER_DEFINITIONS_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DEFINITIONS FOR THE CLASSIFIERS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Enumerate with the different partition methods used to calculate the classifier validation factors.
    enum PARTITION_SCALE {
        PARTITION_UNIFORM = 12401, ///< The partitions are uniformly distributed.
        PARTITION_LOG2,            ///< The partitions are distributed over a 2on base logarithmic scale.
        PARTITION_LOG10            ///< The partitions are distributed over a 10th base logarithmic scale.
    };
    
    /// Enumerate with the identifiers of different linear learner methods available in the linear learner factory.
    enum LINEAR_LEARNER_IDENTIFIER {
        ONLINE_PEGASOS_ID = 11001, ///< Identifier of the Pegasos online learning algorithm.
        ONLINE_FIRST_ORDER_SGD_ID, ///< Identifier of the First Order Stochastic Gradient Descent online learning algorithm.
        ONLINE_QN_SGD_ID, ///< Identifier of the Quasi-Newton Stochastic Gradient Descent online learning algorithm.
        ONLINE_PERCEPTRON_ID, ///< Identifier of the Perceptron online learning algorithm
        ONLINE_MULTIPLE_PERCEPTRON_ID, ///< Identifier of the Multiple Perceptron online learning algorithm.
        SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_PRIMAL, ///< Identifier of the Logistic Regression batch learning algorithm in the primal.
        SVM_LOGISTIC_REGRESSION_L2_REGULARIZED_DUAL, ///< Identifier of the L2-regularized Logistic Regression batch learning algorithm in the dual.
        SVM_LOGISTIC_REGRESSION_L1_REGULARIZED, ///< Identifier of the L1-regularized Logistic Regression batch learning algorithm.
        SVM_MULTICLASS, ///< Identifier of the multi-class batch learning algorithm by Crammer and Singer.
        SVM_L2_REGULARIZED_L2_LOSS_DUAL, ///< Identifier of the L2-regularized L2-loss batch learning algorithm in the dual.
        SVM_L2_REGULARIZED_L2_LOSS_PRIMAL, ///< Identifier of the L2-regularized L2-loss batch learning algorithm in the primal.
        SVM_L2_REGULARIZED_L1_LOSS_SVM_DUAL, ///< Identifier of the L2-regularized L1-loss batch learning algorithm in the dual.
        SVM_L1_REGULARIZED_L2_LOSS ///< Identifier of the L1-regularized L2-loss batch learning algorithm.
    };
    
    /// Enumerate which contains the identifiers of the different loss functions available.
    enum LOSS_FUNCTION_IDENTIFIER { LOGISTIC_LOSS = 11201, MARGIN_LOGISTIC_LOSS, SMOOTH_HINGE_LOSS, SQUARE_HINGE_LOSS, HINGE_LOSS };
    
    /// Enumerate with the identifiers of different probabilistic score methods available in the probailistic score factory.
    enum PROBABILITY_METHOD_IDENTIFIER {
        RESCALING_ALGORITHM = 12001,        ///< This method simply rescales the scores to range between 0 and 1.
        SIGMOID_ALGORITHM,                  ///< Uses the Platt's algorithm to fit a sigmoid function which rescales the scores to probabilities.
        ASYMMETRIC_LAPLACE_ALGORITHM,       ///< Uses two Asymmetric Laplace distribution to fit the probabilistic distribution of the samples and map them from scores to probabilities.
        ASYMMETRIC_GAUSSIAN_ALGORITHM,      ///< Uses two Asymmetric Gaussian distribution to fit the probabilistic distribution of the samples and map them from scores to probabilities.
        BINNING_ALGORITHM,                  ///< Uses a binning algorithm to approximate the probabilities assigned the scores of each classifier.
        ISOTONIC_REGRESSION_ALGORITHM,      ///< Uses an isotonic regression algorithm to approximate the probabilities assigned to the scores of each classifier.
        PROBABILITIES_LUT_ALGORITHM         ///< Uses a look-up table to map the classifier scores to the probabilities generated by any other method.
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

