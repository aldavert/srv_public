// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLASSIFIER_VALIDATION_HEADER_FILE__
#define __SRV_CLASSIFIER_VALIDATION_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DEFINITIONS                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Enumerate with the different performance measures which can be used by the validation training functions.
    enum CLASSIFIER_VALIDATION_IDENTIFIER {
        CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION = 94000,                  ///< Identifier of the mean Average Precision (mAP) measure.
        CLASSIFIER_VALIDATION_AVERAGE_GEOMETRIC_MEAN,                          ///< Identifier of the Average Geometric Mean (AGM) measure.
        CLASSIFIER_VALIDATION_PRECISION_RECALL_EQUAL_ERROR_RATE,               ///< Identifier of the Precision Recall at Equal Error Rate (PR@EER) measure.
        CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION_ONLY_CATEGORY,            ///< Identifier of the mean Average Precision (mAP) measure calculated at the images where the classifier category can be found.
        CLASSIFIER_VALIDATION_AVERAGE_GEOMETRIC_MEAN_ONLY_CATEGORY,            ///< Identifier of the Average Geometric Mean (AGM) measure calculated at the images where the classifier category can be found.
        CLASSIFIER_VALIDATION_PRECISION_RECALL_EQUAL_ERROR_RATE_ONLY_CATEGORY  ///< Identifier of the Precision Recall at Equal Error Rate (PR@EER) measure calculated at the images where the classifier category can be found.
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VALIDATION PARAMETERS CLASS          |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Class which contains the regularization and positive-negative balancing factor evaluated during the classifier validation process.
     *  \tparam TCLASSIFIER type used by the weight vectors of the classifier which uses the validation parameters object.
     */
    template <class TCLASSIFIER>
    class ClassifierValidationParameters
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClassifierValidationParameters(void);
        /** Constructor which initializes the validation parameters and calculates all the validation parameters.
         *  \param[in] minimum_regularization_factor minimum regularization factor evaluated.
         *  \param[in] maximum_regularization_factor maximum regularization factor evaluated.
         *  \param[in] number_of_regularization_partitions total number of regularization factors tested.
         *  \param[in] regularization_partition_scale sets the scale used to calculate the regularization values.
         *  \param[in] minimum_balancing_factor minimum positive-negative balancing factor.
         *  \param[in] maximum_balanding_factor maximum positive-negative balancing factor.
         *  \param[in] number_of_balancing_partitions number of positive-negative balancing factors evaluated.
         *  \param[in] balancing_partition_scale sets the scale used to calculate the balancing values.
         *  \param[in] number_of_levels number of levels of precision in the validation process.
         *  \param[in] regularization_factor_dwindling decrease factor of the range of values evaluated for the regularization factor.
         *  \param[in] balancing_factor_dwindling decrease factor of the range of values evaluated for the balancing factor.
         *  \param[in] validation_type measure used to evaluate the performance of the algorithm during the validation process.
         */
        ClassifierValidationParameters(TCLASSIFIER minimum_regularization_factor, TCLASSIFIER maximum_regularization_factor, unsigned int number_of_regularization_partitions, PARTITION_SCALE regularization_partition_scale, TCLASSIFIER minimum_balancing_factor, TCLASSIFIER maximum_balancing_factor, unsigned int number_of_balancing_partitions, PARTITION_SCALE balancing_partition_scale, unsigned int number_of_levels, TCLASSIFIER regularization_factor_dwindling, TCLASSIFIER balancing_factor_dwindling, CLASSIFIER_VALIDATION_IDENTIFIER validation_type);
        /** This function initializes the validation parameters and calculates all the validation parameters.
         *  \param[in] minimum_regularization_factor minimum regularization factor evaluated.
         *  \param[in] maximum_regularization_factor maximum regularization factor evaluated.
         *  \param[in] number_of_regularization_partitions total number of regularization factors tested.
         *  \param[in] regularization_partition_scale sets the scale used to calculate the regularization values.
         *  \param[in] minimum_balancing_factor minimum positive-negative balancing factor.
         *  \param[in] maximum_balanding_factor maximum positive-negative balancing factor.
         *  \param[in] number_of_balancing_partitions number of positive-negative balancing factors evaluated.
         *  \param[in] balancing_partition_scale sets the scale used to calculate the balancing values.
         *  \param[in] number_of_levels number of levels of precision in the validation process.
         *  \param[in] regularization_factor_dwindling decrease factor of the range of values evaluated for the regularization factor.
         *  \param[in] balancing_factor_dwindling decrease factor of the range of values evaluated for the balancing factor.
         *  \param[in] validation_type measure used to evaluate the performance of the algorithm during the validation process.
         */
        void set(TCLASSIFIER minimum_regularization_factor, TCLASSIFIER maximum_regularization_factor, unsigned int number_of_regularization_partitions, PARTITION_SCALE regularization_partition_scale, TCLASSIFIER minimum_balancing_factor, TCLASSIFIER maximum_balancing_factor, unsigned int number_of_balancing_partitions, PARTITION_SCALE balancing_partition_scale, unsigned int number_of_levels, TCLASSIFIER regularization_factor_dwindling, TCLASSIFIER balancing_factor_dwindling, CLASSIFIER_VALIDATION_IDENTIFIER validation_type);
        
        // -[ Access to the parameters functions ]---------------------------------------------------------------------------------------------------
        /// Returns the minimum regularization factor.
        inline TCLASSIFIER getMinimumRegularizationFactor(void) const { return m_minimum_regularization_factor; }
        /// Sets the minimum regularization factor.
        inline void setMinimumRegularizationFactor(TCLASSIFIER minimum_regularization_factor) { m_minimum_regularization_factor = minimum_regularization_factor; generate(); }
        /// Returns the maximum regularization factor.
        inline TCLASSIFIER getMaximumRegularizationFactor(void) const { return m_maximum_regularization_factor; }
        /// Sets the maximum regularization factor.
        inline void setMaximumRegularizationFactor(TCLASSIFIER maximum_regularization_factor) { m_maximum_regularization_factor = maximum_regularization_factor; generate(); }
        /// Returns the number of regularization factors evaluated.
        inline unsigned int getNumberOfRegularizationPartitions(void) const { return m_number_of_regularization_partitions; }
        /// Sets the number of regularization factors evaluated.
        inline void setNumberOfRegularizationPartitions(unsigned int number_of_regularization_partitions) { m_number_of_regularization_partitions = number_of_regularization_partitions; generate(); }
        /// Returns the type of scale used to generate the regularization factor partitions.
        inline PARTITION_SCALE getRegularizationPartitionScale(void) const { return m_regularization_partition_scale; }
        /// Sets the type of scale used to generate the regularization factor partitions.
        inline void setRegularizationPartitionScale(PARTITION_SCALE regularization_partition_scale) { m_regularization_partition_scale = regularization_partition_scale; generate(); }
        /** Sets the regularization factor parameters.
         *  \param[in] minimum_factor minimum value of the regularization factor.
         *  \param[in] maximum_factor maximum value of the regularization factor.
         *  \param[in] number_of_partitions number of regularization factors generated.
         *  \param[in] scale type of scale used to generate the regularization factors.
         */
        inline void setRegularizationFactorParameters(TCLASSIFIER minimum_factor, TCLASSIFIER maximum_factor, unsigned int number_of_partitions, PARTITION_SCALE scale)
        {
            m_minimum_regularization_factor = minimum_factor;
            m_maximum_regularization_factor = maximum_factor;
            m_number_of_regularization_partitions = number_of_partitions;
            m_regularization_partition_scale = scale;
            generate();
        }
        
        /// Returns the minimum balancing factor.
        inline TCLASSIFIER getMinimumBalancingFactor(void) const { return m_minimum_balancing_factor; }
        /// Sets the minimum balancing factor.
        inline void setMinimumBalancingFactor(TCLASSIFIER minimum_factor) { m_minimum_balancing_factor = minimum_factor; generate(); }
        /// Returns the maximum balancing factor.
        inline TCLASSIFIER getMaximumBalancingFactor(void) const { return m_maximum_balancing_factor; }
        /// Sets the maximum balancing factor.
        inline void setMaximumBalancingFactor(TCLASSIFIER maximum_factor) { m_maximum_balancing_factor = maximum_factor; generate(); }
        /// Returns the number of balancing factors generated.
        inline unsigned int getNumberOfBalancingPartitions(void) const { return m_number_of_balancing_partitions; }
        /// Sets the number of balancing factors generated.
        inline void setNumberOfBalancingPartitions(unsigned int number_of_partitions) { m_number_of_balancing_partitions = number_of_partitions; generate(); }
        /// Returns the type of scale used to generate the balancing factor partitions.
        inline PARTITION_SCALE getBalancingPartitionScale(void) const { return m_balancing_partition_scale; }
        /// Sets the type of scale used to generate the balancing factor partitions.
        inline void setBalancingPartitionScale(PARTITION_SCALE balancing_partition_scale) { m_balancing_partition_scale = balancing_partition_scale; generate(); }
        /** Sets the balancing factor parameters.
         *  \param[in] minimum_factor minimum value of the balancing factor.
         *  \param[in] maximum_factor maximum value of the balancing factor.
         *  \param[in] number_of_partitions number of balancing factors generated.
         *  \param[in] scale type of scale used to generate the balancing factors.
         */
        inline void setBalancingFactorParameters(TCLASSIFIER minimum_factor, TCLASSIFIER maximum_factor, unsigned int number_of_partitions, PARTITION_SCALE scale)
        {
            m_minimum_balancing_factor = minimum_factor;
            m_maximum_balancing_factor = maximum_factor;
            m_number_of_balancing_partitions = number_of_partitions;
            m_balancing_partition_scale = scale;
            generate();
        }
        
        /// Returns the number of levels of precision in the validation process.
        inline unsigned int getNumberOfLevels(void) const { return m_number_of_levels; }
        /// Sets the number of levels of precision in the validation process.
        inline void setNumberOfLevels(unsigned int number_of_levels) { m_number_of_levels = number_of_levels; }
        /// Returns the decrease factor of the range of values evaluated for the regularization factor.
        inline TCLASSIFIER getRegularizationFactorDwindling(void) const { return m_regularization_factor_dwindling; }
        /// Sets the decrease factor of the range of values evaluated for the regularization factor.
        inline void setRegularizationFactorDwindling(TCLASSIFIER regularization_factor_dwindling) { m_regularization_factor_dwindling = regularization_factor_dwindling; }
        /// Returns the decrease factor of the range of values evaluated for the balancing factor.
        inline TCLASSIFIER getBalancingFactorDwindling(void) const { return m_balancing_factor_dwindling; }
        /// Sets the decrease factor of the range of values evaluated for the balancing factor.
        inline void setBalancingFactorDwindling(TCLASSIFIER balancing_factor_dwindling) { m_balancing_factor_dwindling = balancing_factor_dwindling; }
        /// Sets the validation parameters at the next level of precision centered at the given regularization-balancing point.
        void setNextLevel(TCLASSIFIER regularization_factor, TCLASSIFIER balancing_factor);
        
        /// Returns the identifier of the validation method used to calculate the score of the classifier.
        inline CLASSIFIER_VALIDATION_IDENTIFIER getValidationIdentifier(void) const { return m_validation_type; }
        /// Sets the identifier of the validation method used to calculate the score of the classifier.
        inline void setValidationIdentifier(CLASSIFIER_VALIDATION_IDENTIFIER validation_type) { m_validation_type = validation_type; }
        
        // -[ Access to the validation values functions ]--------------------------------------------------------------------------------------------
        /// Returns a constant reference to the vector storing all validation regularization-balancing tuples.
        inline const VectorDense<Tuple<TCLASSIFIER, TCLASSIFIER> >& getValidationValues(void) const { return m_validation_values; }
        /// Returns the number of validation parameters tested.
        inline unsigned int getNumberOfFactors(void) const { return m_validation_values.size(); }
        /// Returns a constant reference to the index-th pair of regularization-balancing validation values.
        inline const Tuple<TCLASSIFIER, TCLASSIFIER>& getFactors(unsigned int index) const { return m_validation_values[index]; }
        /// Returns the regularization factor of the index-th test.
        inline TCLASSIFIER getRegularizationFactor(unsigned int index) const { return m_validation_values[index].getFirst(); }
        /// Returns the balancing factor to the index-th test.
        inline TCLASSIFIER getBalancingFactor(unsigned int index) const { return m_validation_values[index].getSecond(); }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the validation information into an XML object.
        void convertToXML(XmlParser &parser);
        /// Retrieves the validation information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        // -[ Private functions ]--------------------------------------------------------------------------------------------------------------------
        /// Private function which generates the vector of regularization-balancing tuples.
        void generate(void);
        
        // -[ Regularization factor variables ]------------------------------------------------------------------------------------------------------
        /// Minimum regularization factor.
        TCLASSIFIER m_minimum_regularization_factor;
        /// Maximum regularization factor.
        TCLASSIFIER m_maximum_regularization_factor;
        /// Number of partitions between the minimum and the maximum regularization factors.
        unsigned int m_number_of_regularization_partitions;
        /// Scale used to calculate the evaluated regularization factors.
        PARTITION_SCALE m_regularization_partition_scale;
        
        // -[ Balancing factor variables ]-----------------------------------------------------------------------------------------------------------
        /// Minimum balancing factor.
        TCLASSIFIER m_minimum_balancing_factor;
        /// Maximum balancing factor.
        TCLASSIFIER m_maximum_balancing_factor;
        /// Number of positive vs.\ negative samples balancing factors.
        unsigned int m_number_of_balancing_partitions;
        /// Scale used to calculate the evaluated balancing factors.
        PARTITION_SCALE m_balancing_partition_scale;
        
        // -[ Validation tree variables ]------------------------------------------------------------------------------------------------------------
        /// Number of levels of precision in the validation process (i.e.\ the different resolutions).
        unsigned int m_number_of_levels;
        /// Decrease factor of the range of values evaluated for the regularization factor.
        TCLASSIFIER m_regularization_factor_dwindling;
        /// Decrease factor of the range of values evaluated for the balancing factor.
        TCLASSIFIER m_balancing_factor_dwindling;
        
        // -[ Validation method variables ]----------------------------------------------------------------------------------------------------------
        /// Method used to calculate the validation score.
        CLASSIFIER_VALIDATION_IDENTIFIER m_validation_type;
        
        // -[ Validation parameters variables ]------------------------------------------------------------------------------------------------------
        /// Vector containing all validation parameters of the configuration.
        VectorDense<Tuple<TCLASSIFIER, TCLASSIFIER> > m_validation_values;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VALIDATION PARAMETERS CLASS          |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER>
    ClassifierValidationParameters<TCLASSIFIER>::ClassifierValidationParameters(void) :
        m_minimum_regularization_factor((TCLASSIFIER)1e-5),
        m_maximum_regularization_factor((TCLASSIFIER)1e5),
        m_number_of_regularization_partitions(9),
        m_regularization_partition_scale(PARTITION_LOG10),
        m_minimum_balancing_factor((TCLASSIFIER)0.5),
        m_maximum_balancing_factor((TCLASSIFIER)0.9),
        m_number_of_balancing_partitions(5),
        m_balancing_partition_scale(PARTITION_UNIFORM),
        m_number_of_levels(1),
        m_regularization_factor_dwindling((TCLASSIFIER)0.5),
        m_balancing_factor_dwindling((TCLASSIFIER)0.5),
        m_validation_type(CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION)
    {
        generate();
    }
    
    template <class TCLASSIFIER>
    ClassifierValidationParameters<TCLASSIFIER>::ClassifierValidationParameters(TCLASSIFIER minimum_regularization_factor, TCLASSIFIER maximum_regularization_factor, unsigned int number_of_regularization_partitions, PARTITION_SCALE regularization_partition_scale, TCLASSIFIER minimum_balancing_factor, TCLASSIFIER maximum_balancing_factor, unsigned int number_of_balancing_partitions, PARTITION_SCALE balancing_partition_scale, unsigned int number_of_levels, TCLASSIFIER regularization_factor_dwindling, TCLASSIFIER balancing_factor_dwindling, CLASSIFIER_VALIDATION_IDENTIFIER validation_type) :
        m_minimum_regularization_factor(minimum_regularization_factor),
        m_maximum_regularization_factor(maximum_regularization_factor),
        m_number_of_regularization_partitions(number_of_regularization_partitions),
        m_regularization_partition_scale(regularization_partition_scale),
        m_minimum_balancing_factor(minimum_balancing_factor),
        m_maximum_balancing_factor(maximum_balancing_factor),
        m_number_of_balancing_partitions(number_of_balancing_partitions),
        m_balancing_partition_scale(balancing_partition_scale),
        m_number_of_levels(number_of_levels),
        m_regularization_factor_dwindling(regularization_factor_dwindling),
        m_balancing_factor_dwindling(balancing_factor_dwindling),
        m_validation_type(validation_type)
    {
        generate();
    }
    
    template <class TCLASSIFIER>
    void ClassifierValidationParameters<TCLASSIFIER>::set(TCLASSIFIER minimum_regularization_factor, TCLASSIFIER maximum_regularization_factor, unsigned int number_of_regularization_partitions, PARTITION_SCALE regularization_partition_scale, TCLASSIFIER minimum_balancing_factor, TCLASSIFIER maximum_balancing_factor, unsigned int number_of_balancing_partitions, PARTITION_SCALE balancing_partition_scale, unsigned int number_of_levels, TCLASSIFIER regularization_factor_dwindling, TCLASSIFIER balancing_factor_dwindling, CLASSIFIER_VALIDATION_IDENTIFIER validation_type)
    {
        m_minimum_regularization_factor = minimum_regularization_factor;
        m_maximum_regularization_factor = maximum_regularization_factor;
        m_number_of_regularization_partitions = number_of_regularization_partitions;
        m_regularization_partition_scale = regularization_partition_scale;
        m_minimum_balancing_factor = minimum_balancing_factor;
        m_maximum_balancing_factor = maximum_balancing_factor;
        m_number_of_balancing_partitions = number_of_balancing_partitions;
        m_balancing_partition_scale = balancing_partition_scale;
        m_number_of_levels = number_of_levels;
        m_regularization_factor_dwindling = regularization_factor_dwindling;
        m_balancing_factor_dwindling = balancing_factor_dwindling;
        m_validation_type = validation_type;
        generate();
    }
    
    template <class TCLASSIFIER>
    void ClassifierValidationParameters<TCLASSIFIER>::convertToXML(XmlParser &parser)
    {
        parser.openTag("Validation_Parameters");
        parser.setAttribute("Minimum_Regularization", m_minimum_regularization_factor);
        parser.setAttribute("Maximum_Regularization", m_maximum_regularization_factor);
        parser.setAttribute("Regularization_Partitions", m_number_of_regularization_partitions);
        parser.setAttribute("Regularization_Scale", (int)m_regularization_partition_scale);
        parser.setAttribute("Minimum_Balancing", m_minimum_balancing_factor);
        parser.setAttribute("Maximum_Balancing", m_maximum_balancing_factor);
        parser.setAttribute("Balancing_Partitions", m_number_of_balancing_partitions);
        parser.setAttribute("Balancing_Scale", (int)m_balancing_partition_scale);
        parser.setAttribute("Number_Levels", m_number_of_levels);
        parser.setAttribute("Regularization_Dwindling", m_regularization_factor_dwindling);
        parser.setAttribute("Balancing_Dwindling", m_balancing_factor_dwindling);
        parser.setAttribute("Validation_Type", (int)m_validation_type);
        parser.closeTag();
    }
    
    template <class TCLASSIFIER>
    void ClassifierValidationParameters<TCLASSIFIER>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Validation_Parameters"))
        {
            m_minimum_regularization_factor = parser.getAttribute("Minimum_Regularization");
            m_maximum_regularization_factor = parser.getAttribute("Maximum_Regularization");
            m_number_of_regularization_partitions = parser.getAttribute("Regularization_Partitions");
            m_regularization_partition_scale = (PARTITION_SCALE)((int)parser.getAttribute("Regularization_Scale"));
            m_minimum_balancing_factor = parser.getAttribute("Minimum_Balancing");
            m_maximum_balancing_factor = parser.getAttribute("Maximum_Balancing");
            m_number_of_balancing_partitions = parser.getAttribute("Balancing_Partitions");
            m_balancing_partition_scale = (PARTITION_SCALE)((int)parser.getAttribute("Balancing_Scale"));
            m_number_of_levels = parser.getAttribute("Number_Levels");
            m_regularization_factor_dwindling = parser.getAttribute("Regularization_Dwindling");
            m_balancing_factor_dwindling = parser.getAttribute("Balancing_Dwindling");
            m_validation_type = (CLASSIFIER_VALIDATION_IDENTIFIER)((int)parser.getAttribute("Validation_Type"));
        }
    }
    
    template <class TCLASSIFIER>
    void ClassifierValidationParameters<TCLASSIFIER>::setNextLevel(TCLASSIFIER regularization_factor, TCLASSIFIER balancing_factor)
    {
        if (m_number_of_levels > 0)
        {
            TCLASSIFIER regularization_step, balancing_step;
            
            if (m_regularization_partition_scale == PARTITION_UNIFORM)
            {
                regularization_step = m_regularization_factor_dwindling * (m_maximum_regularization_factor - m_minimum_regularization_factor);
                m_minimum_regularization_factor = regularization_factor - regularization_step / (TCLASSIFIER)2.0;
                m_maximum_regularization_factor = regularization_factor + regularization_step / (TCLASSIFIER)2.0;
            }
            else if (m_regularization_partition_scale == PARTITION_LOG2)
            {
                const TCLASSIFIER log2 = std::log((TCLASSIFIER)2);
                const TCLASSIFIER v2 = (TCLASSIFIER)2;
                const TCLASSIFIER log_regularization = std::log(regularization_factor) / log2;
                
                regularization_step = m_regularization_factor_dwindling * (std::log(m_maximum_regularization_factor) - std::log(m_minimum_regularization_factor) + 1) / log2;
                m_minimum_regularization_factor = std::pow(v2, log_regularization - regularization_step / v2);
                m_maximum_regularization_factor = std::pow(v2, log_regularization + regularization_step / v2);
            }
            else if (m_regularization_partition_scale == PARTITION_LOG10)
            {
                const TCLASSIFIER log10 = std::log((TCLASSIFIER)10);
                const TCLASSIFIER v10 = (TCLASSIFIER)10;
                const TCLASSIFIER v2 = (TCLASSIFIER)2;
                const TCLASSIFIER log_regularization = std::log(regularization_factor) / log10;
                
                regularization_step = m_regularization_factor_dwindling * (std::log(m_maximum_regularization_factor) - std::log(m_minimum_regularization_factor) + 1) / log10;
                m_minimum_regularization_factor = std::pow(v10, log_regularization - regularization_step / v2);
                m_maximum_regularization_factor = std::pow(v10, log_regularization + regularization_step / v2);
            }
            if (m_balancing_partition_scale == PARTITION_UNIFORM)
            {
                balancing_step = m_balancing_factor_dwindling * (m_maximum_balancing_factor - m_minimum_balancing_factor);
                m_minimum_balancing_factor = balancing_factor - balancing_step / (TCLASSIFIER)2.0;
                m_maximum_balancing_factor = balancing_factor + balancing_step / (TCLASSIFIER)2.0;
            }
            else if (m_balancing_partition_scale == PARTITION_LOG2)
            {
                const TCLASSIFIER log2 = std::log((TCLASSIFIER)2);
                const TCLASSIFIER v2 = (TCLASSIFIER)2;
                const TCLASSIFIER log_balancing = std::log(balancing_factor) / log2;
                
                balancing_step = m_balancing_factor_dwindling * (std::log(m_maximum_balancing_factor) - std::log(m_minimum_balancing_factor) + 1) / log2;
                m_minimum_balancing_factor = std::pow(v2, log_balancing - balancing_step / v2);
                m_maximum_balancing_factor = std::pow(v2, log_balancing + balancing_step / v2);
            }
            else if (m_balancing_partition_scale == PARTITION_LOG10)
            {
                const TCLASSIFIER log10 = std::log((TCLASSIFIER)10);
                const TCLASSIFIER v10 = (TCLASSIFIER)10;
                const TCLASSIFIER v2 = (TCLASSIFIER)2;
                const TCLASSIFIER log_balancing = std::log(balancing_factor) / log10;
                
                balancing_step = m_balancing_factor_dwindling * (std::log(m_maximum_balancing_factor) - std::log(m_minimum_balancing_factor) + 1) / log10;
                m_minimum_balancing_factor = std::pow(v10, log_balancing - balancing_step / v2);
                m_maximum_balancing_factor = std::pow(v10, log_balancing + balancing_step / v2);
            }
            --m_number_of_levels;
            generate();
        }
    }
    
    template <class TCLASSIFIER>
    void ClassifierValidationParameters<TCLASSIFIER>::generate(void)
    {
        VectorDense<TCLASSIFIER> regularization_values, balancing_values;
        
        if (m_minimum_regularization_factor >= m_maximum_regularization_factor) m_number_of_regularization_partitions = 1;
        if (m_minimum_balancing_factor >= m_maximum_balancing_factor) m_number_of_balancing_partitions = 1;
        
        if (m_number_of_regularization_partitions < 1)
        {
            m_number_of_regularization_partitions = 1;
            regularization_values.set(1, (m_minimum_regularization_factor + m_maximum_regularization_factor) / (TCLASSIFIER)2.0);
        }
        else if (m_number_of_regularization_partitions == 1) regularization_values.set(1, (m_minimum_regularization_factor + m_maximum_regularization_factor) / (TCLASSIFIER)2.0);
        else
        {
            TCLASSIFIER begin, end, step;
            
            regularization_values.set(m_number_of_regularization_partitions);
            regularization_values[0] = m_minimum_regularization_factor;
            regularization_values[m_number_of_regularization_partitions - 1] = m_maximum_regularization_factor;
            
            if (m_regularization_partition_scale == PARTITION_UNIFORM)
            {
                begin = m_minimum_regularization_factor;
                end = m_maximum_regularization_factor;
                step = (end - begin) / ((TCLASSIFIER)m_number_of_regularization_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_regularization_partitions - 1; ++i)
                    regularization_values[i] = (TCLASSIFIER)i * step + begin;
            }
            else if (m_regularization_partition_scale == PARTITION_LOG2)
            {
                begin = (TCLASSIFIER)(log(m_minimum_regularization_factor) / log(2));
                end = (TCLASSIFIER)(log(m_maximum_regularization_factor) / log(2));
                step = (end - begin) / ((TCLASSIFIER)m_number_of_regularization_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_regularization_partitions - 1; ++i)
                    regularization_values[i] = (TCLASSIFIER)pow((TCLASSIFIER)2, (TCLASSIFIER)i * step + begin);
            }
            else if (m_regularization_partition_scale == PARTITION_LOG10)
            {
                begin = (TCLASSIFIER)(log(m_minimum_regularization_factor) / log(10));
                end = (TCLASSIFIER)(log(m_maximum_regularization_factor) / log(10));
                step = (end - begin) / ((TCLASSIFIER)m_number_of_regularization_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_regularization_partitions - 1; ++i)
                    regularization_values[i] = (TCLASSIFIER)pow((TCLASSIFIER)10, (TCLASSIFIER)i * step + begin);
            }
            else throw Exception("Selected partition method not implemented yet.");
        }
            
        if (m_number_of_balancing_partitions < 1)
        {
            m_number_of_balancing_partitions = 1;
            balancing_values.set(1, (m_minimum_balancing_factor + m_maximum_balancing_factor) / (TCLASSIFIER)2.0);
        }
        else if (m_number_of_balancing_partitions == 1) balancing_values.set(1, (m_minimum_balancing_factor + m_maximum_balancing_factor) / (TCLASSIFIER)2.0);
        else
        {
            TCLASSIFIER begin, end, step;
            
            balancing_values.set(m_number_of_balancing_partitions);
            balancing_values[0] = m_minimum_balancing_factor;
            balancing_values[m_number_of_balancing_partitions - 1] = m_maximum_balancing_factor;
            
            if (m_balancing_partition_scale == PARTITION_UNIFORM)
            {
                begin = m_minimum_balancing_factor;
                end = m_maximum_balancing_factor;
                step = (end - begin) / ((TCLASSIFIER)m_number_of_balancing_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_balancing_partitions - 1; ++i)
                    balancing_values[i] = (TCLASSIFIER)i * step + begin;
            }
            else if (m_balancing_partition_scale == PARTITION_LOG2)
            {
                begin = (TCLASSIFIER)(log(m_minimum_balancing_factor) / log(2));
                end = (TCLASSIFIER)(log(m_maximum_balancing_factor) / log(2));
                step = (end - begin) / ((TCLASSIFIER)m_number_of_balancing_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_balancing_partitions - 1; ++i)
                    balancing_values[i] = (TCLASSIFIER)pow((TCLASSIFIER)2, (TCLASSIFIER)i * step + begin);
            }
            else if (m_balancing_partition_scale == PARTITION_LOG10)
            {
                begin = (TCLASSIFIER)(log(m_minimum_balancing_factor) / log(10));
                end = (TCLASSIFIER)(log(m_maximum_balancing_factor) / log(10));
                step = (end - begin) / ((TCLASSIFIER)m_number_of_balancing_partitions - 1);
                for (unsigned int i = 1; i < m_number_of_balancing_partitions - 1; ++i)
                    balancing_values[i] = (TCLASSIFIER)pow((TCLASSIFIER)10, (TCLASSIFIER)i * step + begin);
            }
            else throw Exception("Selected partition method not implemented yet.");
        }
        
        m_validation_values.set(regularization_values.size() * balancing_values.size());
        for (unsigned int i = 0, k = 0; i < regularization_values.size(); ++i)
            for (unsigned int j = 0; j < balancing_values.size(); ++j, ++k)
                m_validation_values[k].setData(regularization_values[i], balancing_values[j]);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

