// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_L2_REGULARIZED_L1_LOSS_DUAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_L2_REGULARIZED_L1_LOSS_DUAL_SVM_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_base_classifier.hpp"
#include "srv_base_svm_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED L1 LOSS DUAL SVM      |
    //                   | CLASS DECLARATION                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class implements the Linear Support Vector Machine learning algorithm for the L2-Regularized L1-Loss problem
     *  solved in the dual proposed in: C.J. Hsieh, K.W. Chang, C.J. Lin, S.S. Keerthi, S. Sundararajan, <b>"A Dual
     *  Coordinate Descent Method for Large-Scale Linear SVM"</b>, <i>Proceedings of the 25th International Conference
     *  on Machine Learning (ICML), 408--415, 2008</i>.
     *  \note This code is derived from the LIBLINEAR library: http://www.csie.ntu.edu.tw/~cjlin/liblinear/
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class L2RegularizedL1LossDualSVM : public LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        /// Default constructor.
        L2RegularizedL1LossDualSVM(void) : LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>() {}
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] epsilon tolerance of the termination criterion.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the optimization algorithm.
         */
        L2RegularizedL1LossDualSVM(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, TCLASSIFIER epsilon, unsigned int maximum_number_of_iterations) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, epsilon, maximum_number_of_iterations) {}
        /// Copy constructor.
        L2RegularizedL1LossDualSVM(const L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Copy constructor for linear learning classifier of another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedL1LossDualSVM(const L2RegularizedL1LossDualSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other) {}
        /// Destructor.
        virtual ~L2RegularizedL1LossDualSVM(void) {}
        /// Assignation operator.
        inline L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
        {
            if (this != &other) LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        /// Assignation operator for linear learners of different types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const L2RegularizedL1LossDualSVM<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
        {
            LinearSupportVectorMachineBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            return *this;
        }
        
        // -[ Training functions ]-------------------------------------------------------------------------------------------------------------------
        /** Trains the linear classifiers for the given set of training samples.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         *  \note This algorithm is only parallelized by category, therefore no parallelization is done when the classifier has a single category.
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the linear learner object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the linear learner method.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return SVM_L2_REGULARIZED_L1_LOSS_SVM_DUAL; }
        /// Generates a new empty instance of the linear learner.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(); }
        /// Returns a flag which states if the linear learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the linear learner type identifier of the current object.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return SVM_L2_REGULARIZED_L1_LOSS_SVM_DUAL; }
        
    protected:
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | L2 REGULARIZED L1 LOSS DUAL SVM      |
    //                   | CLASS IMPLEMENTATION                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    ////        #pragma omp parallel num_threads(number_of_threads)
    ////        {
    ////                for (unsigned int i = omp_get_thread_num(); i < number_of_train_samples; i += number_of_threads)
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        if ((number_of_threads > 1) && (logger != 0))
            logger->log("WARNING: %d threads are used to concurrently calculate the classifiers of the %d categories, therefore output information messages can be mixed.", number_of_threads, this->m_number_of_categories);
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<char, unsigned int> samples_sign(number_of_train_samples);
            VectorDense<TCLASSIFIER, unsigned int> alpha(number_of_train_samples), QD(number_of_train_samples);
            VectorDense<unsigned int, unsigned int> index(number_of_train_samples);
            TCLASSIFIER projected_gradient, maximum_projected_gradient_old, minimum_projected_gradient_old, maximum_projected_gradient, minimum_projected_gradient;
            unsigned int active_size, iteration, total_number_of_samples;
            
            for (unsigned int category_id = omp_get_thread_num(); category_id < this->m_number_of_categories; category_id += number_of_threads)
            {
                const TCLASSIFIER positive_weight = base_parameters[category_id].getBalancingFactor() * base_parameters[category_id].getPositiveWeight() * base_parameters[category_id].getRegularizationFactor();
                const TCLASSIFIER negative_weight = (_C(1.0) - base_parameters[category_id].getBalancingFactor()) * base_parameters[category_id].getNegativeWeight() * base_parameters[category_id].getRegularizationFactor();
                
                if (logger != 0)
                    logger->log("Initializing the solver structures for the classifier %04d.", category_id);
                maximum_projected_gradient_old = std::numeric_limits<TCLASSIFIER>::max();
                minimum_projected_gradient_old = -std::numeric_limits<TCLASSIFIER>::max();
                this->m_classifiers[category_id].setValue(_C(0));
                if (this->m_ignore_difficult)
                {
                    active_size = 0;
                    for (unsigned int i = 0; i < number_of_train_samples; ++i)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        if (probability >= this->m_difficult_probability)
                        {
                            samples_sign[i] = 1;
                            index[active_size] = i;
                            ++active_size;
                        }
                        else if (probability < this->m_positive_probability)
                        {
                            samples_sign[i] = -1;
                            index[active_size] = i;
                            ++active_size;
                        }
                        else samples_sign[i] = 0;
                        
                        alpha[i] = _C(0);
                        QD[i] = _C(0);
                        for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                        {
                            const TCLASSIFIER bin_value = _C(train_samples[i]->getData().getValue(p));
                            QD[i] += bin_value * bin_value;
                        }
                    }
                }
                else
                {
                    active_size = number_of_train_samples;
                    for (unsigned int i = 0; i < number_of_train_samples; ++i)
                    {
                        TLABEL probability;
                        
                        probability = train_samples[i]->getLabel(this->m_categories_identifiers[category_id]);
                        if (probability >= this->m_positive_probability) samples_sign[i] = 1;
                        else samples_sign[i] = -1;
                        
                        alpha[i] = _C(0);
                        QD[i] = _C(0);
                        for (NSAMPLE p = 0; p < train_samples[i]->getData().size(); ++p)
                        {
                            const TCLASSIFIER bin_value = _C(train_samples[i]->getData().getValue(p));
                            QD[i] += bin_value * bin_value;
                        }
                        index[i] = i;
                    }
                }
                total_number_of_samples = active_size;
                
                for (iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
                {
                    maximum_projected_gradient = -std::numeric_limits<TCLASSIFIER>::max();
                    minimum_projected_gradient = std::numeric_limits<TCLASSIFIER>::max();
                    if ((iteration % 10 == 0) && (logger != 0))
                        logger->log("Iteration %d: Active size: %d || Maximum Projected Gradient: %5.3e, Minimum Projected Gradient: %5.3e.", iteration, active_size, (double)maximum_projected_gradient, (double)minimum_projected_gradient);
                    
                    for (unsigned int i = 0; i < active_size; ++i)
                        srvSwap(index[i], index[i + rand() % (active_size - i)]);
                    
                    for (unsigned int s = 0; s < active_size; ++s)
                    {
                        ConstantSubVectorSparse<TSAMPLE, NSAMPLE> current_sample(train_samples[index[s]]->getData());
                        TCLASSIFIER current_gradient, C;
                        
                        current_gradient = this->classifierDot(this->m_classifiers[category_id], current_sample);
                        
                        if (samples_sign[index[s]] > 0)
                        {
                            C = positive_weight;
                            current_gradient = (current_gradient - 1);
                        }
                        else
                        {
                            C = negative_weight;
                            current_gradient = (-current_gradient - 1);
                        }
                        
                        projected_gradient = _C(0);
                        if (alpha[index[s]] == _C(0))
                        {
                            if (current_gradient > maximum_projected_gradient_old)
                            {
                                --active_size;
                                srvSwap(index[s], index[active_size]);
                                --s;
                                continue;
                            }
                            else if (current_gradient < _C(0)) projected_gradient = current_gradient;
                        }
                        else if (alpha[index[s]] == C)
                        {
                            if (current_gradient < minimum_projected_gradient_old)
                            {
                                --active_size;
                                srvSwap(index[s], index[active_size]);
                                --s;
                                continue;
                            }
                            else if (current_gradient > _C(0)) projected_gradient = current_gradient;
                        }
                        else projected_gradient = current_gradient;
                        
                        maximum_projected_gradient = srvMax<TCLASSIFIER>(maximum_projected_gradient, projected_gradient);
                        minimum_projected_gradient = srvMin<TCLASSIFIER>(minimum_projected_gradient, projected_gradient);
                        
                        if (srvAbs<TCLASSIFIER>(projected_gradient) > _C(1.0e-12))
                        {
                            TCLASSIFIER alpha_old, d;
                            
                            alpha_old = alpha[index[s]];
                            alpha[index[s]] = srvMin<TCLASSIFIER>(srvMax<TCLASSIFIER>(alpha[index[s]] - current_gradient / QD[index[s]], _C(0.0)), C);
                            if (samples_sign[index[s]] > 0) d = (alpha[index[s]] - alpha_old);
                            else d = -(alpha[index[s]] - alpha_old);
                            
                            for (NSAMPLE p = 0; p < current_sample.size(); ++p)
                                this->m_classifiers[category_id][(unsigned int)current_sample.getIndex(p)] += d * _C(current_sample.getValue(p));
                        }
                    }
                    
                    if (maximum_projected_gradient - minimum_projected_gradient <= this->m_epsilon)
                    {
                        if (active_size == total_number_of_samples) break;
                        else
                        {
                            active_size = total_number_of_samples;
                            maximum_projected_gradient_old = std::numeric_limits<TCLASSIFIER>::max();
                            minimum_projected_gradient_old = -std::numeric_limits<TCLASSIFIER>::max();
                            continue;
                        }
                    }
                    maximum_projected_gradient_old = maximum_projected_gradient;
                    minimum_projected_gradient_old = minimum_projected_gradient;
                    if (maximum_projected_gradient_old <= _C(0)) maximum_projected_gradient_old = std::numeric_limits<TCLASSIFIER>::max();
                    if (minimum_projected_gradient_old >= _C(0)) minimum_projected_gradient_old = -std::numeric_limits<TCLASSIFIER>::max();
                }
                if (logger != 0)
                {
                    unsigned int nSV;
                    TCLASSIFIER v;
                    
                    logger->log("Optimization finished with %d iterations.", iteration);
                    if (iteration >= this->m_maximum_number_of_iterations)
                        logger->log("Reaching the maximum number of iterations %d.", this->m_maximum_number_of_iterations);
                    
                    nSV = 0;
                    v = _C(0);
                    for (NSAMPLE i = 0; i < this->m_classifiers[category_id].size(); ++i)
                        v += this->m_classifiers[category_id][i] * this->m_classifiers[category_id][i];
                    for (unsigned int i = 0; i < number_of_train_samples; ++i)
                    {
                        v -= _C(2.0) * alpha[i];
                        if (alpha[i] > 0) ++nSV;
                    }
                    logger->log("Objective value =%lf; nSV = %d", (double)v / 2.0, nSV);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

