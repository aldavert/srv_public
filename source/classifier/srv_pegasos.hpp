// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_PEGASOS_CLASSIFIER_IMPLEMENTATION_HPP_HEADER_FILE__
#define __SRV_PEGASOS_CLASSIFIER_IMPLEMENTATION_HPP_HEADER_FILE__

#include "srv_online_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | PEGASOS LEARNER CLASS                |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Implementation of the algorithm proposed in the paper: Shai Shalev-Shwartz, Yoram Singer, Natham Srebro, "Pegasos: Primal
     *  Estimated Sub-gradient Solver for SVM", Proc. of the 24th International Conference on Machine Learning, 807--814, 2007.
     */
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class Pegasos : public LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        Pegasos(void);
        /** Constructor which initializes the parameters of the Pegasos classifier.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] average_classifier enables or disables the use of averaged weight models for the linear classifiers generated by the online learning algorithm.
         *  \param[in] average_skip the number of iterations at which the average model is updated.
         *  \param[in] chunk_size number of samples trained at once (set to 0 to use all samples to update the classifier).
         *  \param[in] projection enables or disables the spherical projection optimization.
         *  \param[in] loss_identifier identifier of the loss function used by the Pegasos classifier.
         */
        Pegasos(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, bool average_classifier, unsigned int average_skip, unsigned int chunk_size, bool projection, LOSS_FUNCTION_IDENTIFIER loss_identifier);
        /// Copy constructor.
        Pegasos(const Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Copy constructor from a Pegasos from another data type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        Pegasos(const Pegasos<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        /// Destructor.
        ~Pegasos(void);
        /// Assignation operator.
        Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Assignation operator for Pegasos of different data types.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const Pegasos<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        
        // -[ Online learning functions which update the linear classifier model ]-------------------------------------------------------------------
        /// Initializes the Pegasos algorithm.
        void initialize(const ClassifierBaseParameters<TCLASSIFIER> * base_parameters);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** This function sets the dimensionality and number of categories of the linear classifiers.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifiers.
         *  \param[in] categories_identifiers array with the identifiers of each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of linear classifiers.
         */
        void setClassifierParameters(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories);
        /// Returns a constant pointer to the loss function of the Pegasos algorithm.
        inline const LossBase<TCLASSIFIER>* getLossFunction(void) const { return m_loss; }
        /// Sets the loss function used by the Pegasos algorithm.
        inline void setLossFunction(LOSS_FUNCTION_IDENTIFIER loss_identifier) { delete m_loss; m_loss = createLossObject<TCLASSIFIER>(HINGE_LOSS); }
        /// Returns the boolean flag which indicates if the spherical projection optimization is enabled or disabled.
        inline bool getProjection(void) const { return m_projection; }
        /// Sets the boolean flag which indicates if the spherical projection optimization is enabled or disabled.
        inline void setProjection(bool projection) { m_projection = projection; }
        /// Clears the model information of the linear classifiers of each category.
        virtual void clear(void);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the Pegasos object (virtual copy constructor).
        inline LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * duplicate(void) const { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(*this); }
        /// Returns the class identifier for the Pegasos learner algorithm.
        inline static LINEAR_LEARNER_IDENTIFIER getClassIdentifier(void) { return ONLINE_PEGASOS_ID; }
        /// Generates a new empty instance of the Pegasos learner algorithm.
        inline static LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * generateObject(void) { return (LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> *)new Pegasos(); }
        /// Returns a flag which states if the Pegasos learner class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the Pegasos learner type identifier.
        inline LINEAR_LEARNER_IDENTIFIER getIdentifier(void) const { return ONLINE_PEGASOS_ID; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the Pegasos classifier information in an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the Pegasos classifier information from an XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Auxiliary function which converts any value to the classifier vector type format.
        template <typename VALUE>
        inline TCLASSIFIER _C(const VALUE &value) const { return (TCLASSIFIER)value; }
        /** Function which updates the weights of the linear classifiers for the given set of samples using the Pegasos algorithm.
         *  \param[in] train_samples array of pointers to the samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in,out] weight_model array with the weight vectors of the linear classifiers updated by the function.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        void updateStep(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, VectorDense<TCLASSIFIER, NSAMPLE> * weight_model, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads);
        
        /** Pure virtual function which updates the online learning classifier model when there are no more training samples.
         *  This function triggers the algorithm steps which only happens at certain iterations of the algorithms and that will
         *  not be executed otherwise.
         *  \param[in,out] weight_model array with the weight vectors of the linear classifiers updated by the function.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        inline void updateStep(VectorDense<TCLASSIFIER, NSAMPLE> * /* weight_model */, const ClassifierBaseParameters<TCLASSIFIER> * /* base_parameters */, unsigned int /* number_of_threads */) {}
        
        /// Boolean flag which enables or disables the use of spherical projection optimization.
        bool m_projection;
        /// Loss function used by the Pegasos algorithm.
        LossBase<TCLASSIFIER> * m_loss;
        /// Static integer which indicates if the class has been initialized in the linear learner base factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PERCEPTRON LEARNER CLASS             |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Pegasos(void) :
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(),
        m_projection(true),
        m_loss(createLossObject<TCLASSIFIER>(HINGE_LOSS))
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Pegasos(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, bool average_classifier, unsigned int average_skip, unsigned int chunk_size, bool projection, LOSS_FUNCTION_IDENTIFIER loss_identifier) :
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult, average_classifier, average_skip, chunk_size),
        m_projection(projection),
        m_loss(createLossObject<TCLASSIFIER>(loss_identifier))
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Pegasos(const Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
        m_projection(other.m_projection),
        m_loss(other.m_loss->duplicate())
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::Pegasos(const Pegasos<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
        m_projection(other.getProjection()),
        m_loss(createLossObject<TCLASSIFIER>(other.getLossFunction()->getIdentifier()))
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::~Pegasos(void)
    {
        delete m_loss;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
    {
        if (this != &other)
        {
            delete m_loss;
            
            LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            m_projection = other.m_projection;
            m_loss = other.m_loss->duplicate();
        }
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const Pegasos<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
    {
        delete m_loss;
        
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
        m_projection = other.getProjection();
        m_loss = createLossObject<TCLASSIFIER>(other.getLossFunction()->getIdentifier());
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::clear(void)
    {
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::clear();
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::initialize(const ClassifierBaseParameters<TCLASSIFIER> * /* base_parameters */)
    {
        this->m_iteration = 0;
        this->m_number_of_averaged_models = 0;
        for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
        {
            this->m_classifiers[i].setValue(0);
            if (this->m_inner_classifiers != 0)
                this->m_inner_classifiers[i].setValue(0);
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Classifier_Information");
        // -[ All classifiers members ]--------------------------------------------------------------------------------------------------------------
        parser.setAttribute("Identifier", getIdentifier());
        parser.setAttribute("Number_Of_Dimensions", this->m_number_of_dimensions);
        parser.setAttribute("Number_Of_Categories", this->m_number_of_categories);
        parser.setAttribute("Positive_Probability", this->m_positive_probability);
        parser.setAttribute("Difficult_Probability", this->m_difficult_probability);
        parser.setAttribute("Ignore_Difficult", this->m_ignore_difficult);
        // -[ Online learning classifiers members ]--------------------------------------------------------------------------------------------------
        parser.setAttribute("Average_Classifier", this->m_average_classifier);
        parser.setAttribute("Number_Of_Averaged_Models", this->m_number_of_averaged_models);
        parser.setAttribute("Average_Skip", this->m_average_skip);
        parser.setAttribute("Iteration", this->m_iteration);
        parser.setAttribute("Chunk_Size", this->m_chunk_size);
        // -[ Current classifier information ]-------------------------------------------------------------------------------------------------------
        parser.setAttribute("Projection", m_projection);
        parser.setAttribute("Loss_Identifier", (int)m_loss->getIdentifier());
        if (this->m_number_of_categories > 0)
        {
            parser.addChildren();
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
            {
                // -[ All classifiers data ]---------------------------------------------------------------------------------------------------------
                parser.openTag("Category_Classifier");
                parser.setAttribute("Category_Index", i);
                parser.setAttribute("Category_Identifier", this->m_categories_identifiers[i]);
                parser.addChildren();
                this->m_base_parameters[i].convertToXML(parser);
                saveVector(parser, "Weights", this->m_classifiers[i]);
                if (this->m_inner_classifiers != 0)
                    saveVector(parser, "Inner_Weights", this->m_inner_classifiers[i]);
                parser.closeTag();
            }
        }
        parser.closeTag();
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Classifier_Information") && ((LINEAR_LEARNER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == getIdentifier()))
        {
            // -[ Free memory ]----------------------------------------------------------------------------------------------------------------------
            // ALL CLASSIFIERS ....................................................................
            if (this->m_classifiers != 0) { delete [] this->m_classifiers; this->m_classifiers = 0; }
            if (this->m_categories_identifiers != 0) { delete [] this->m_categories_identifiers; this->m_categories_identifiers = 0; }
            if (this->m_base_parameters != 0) { delete [] this->m_base_parameters; this->m_base_parameters = 0; }
            // ONLINE LEARNING CLASSIFIERS ONLY ...................................................
            if (this->m_inner_classifiers != 0) { delete [] this->m_inner_classifiers; this->m_inner_classifiers = 0; }
            // CURRENT CLASSIFIER ONLY ............................................................
            delete m_loss;
            
            // -[ Retrieve parameters ]--------------------------------------------------------------------------------------------------------------
            // ALL CLASSIFIERS ....................................................................
            this->m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            this->m_number_of_categories = parser.getAttribute("Number_Of_Categories");
            this->m_positive_probability = parser.getAttribute("Positive_Probability");
            this->m_difficult_probability = parser.getAttribute("Difficult_Probability");
            this->m_ignore_difficult = parser.getAttribute("Ignore_Difficult");
            // ONLINE LEARNING CLASSIFIERS ONLY ...................................................
            this->m_average_classifier = parser.getAttribute("Average_Classifier");
            this->m_number_of_averaged_models = parser.getAttribute("Number_Of_Averaged_Models");
            this->m_average_skip = parser.getAttribute("Average_Skip");
            this->m_iteration = parser.getAttribute("Iteration");
            this->m_chunk_size = parser.getAttribute("Chunk_Size");
            // CURRENT CLASSIFIER ONLY ............................................................
            m_projection = parser.getAttribute("Projection");
            m_loss = createLossObject<TCLASSIFIER>((LOSS_FUNCTION_IDENTIFIER)((int)parser.getAttribute("Loss_Identifier")));
            
            // -[ Allocate memory for the classifier structures ]------------------------------------------------------------------------------------
            // ALL CLASSIFIERS ....................................................................
            if (this->m_number_of_categories > 0)
            {
                this->m_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[this->m_number_of_categories];
                this->m_categories_identifiers = new NLABEL[this->m_number_of_categories];
                this->m_base_parameters = new ClassifierBaseParameters<TCLASSIFIER>[this->m_number_of_categories];
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                {
                    this->m_classifiers[i].set(this->m_number_of_dimensions, 0);
                    this->m_categories_identifiers[i] = (NLABEL)i;
                }
            }
            // ONLINE LEARNING CLASSIFIERS ONLY ...................................................
            if ((this->m_number_of_categories > 0) && this->m_average_classifier)
            {
                this->m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[this->m_number_of_categories];
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    this->m_inner_classifiers[i].set(this->m_number_of_dimensions, 0);
            }
            // CURRENT CLASSIFIER ONLY ............................................................
            
            while (!(parser.isTagIdentifier("Classifier_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Category_Classifier"))
                {
                    unsigned int current_index;
                    
                    current_index = parser.getAttribute("Category_Index");
                    this->m_categories_identifiers[current_index] = parser.getAttribute("Category_Identifier");
                    
                    while (!(parser.isTagIdentifier("Category_Classifier") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Base_Parameters"))
                            this->m_base_parameters[current_index].convertFromXML(parser);
                        else if (parser.isTagIdentifier("Weights"))
                            loadVector(parser, "Weights", this->m_classifiers[current_index]);
                        else if (parser.isTagIdentifier("Inner_Weights"))
                            loadVector(parser, "Inner_Weights", this->m_inner_classifiers[current_index]);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::setClassifierParameters(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories)
    {
        this->set(number_of_dimensions, categories_identifiers, number_of_categories, this->m_positive_probability, this->m_difficult_probability, this->m_ignore_difficult);
        this->set(this->m_average_classifier, this->m_average_skip, this->m_chunk_size);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::updateStep(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, VectorDense<TCLASSIFIER, NSAMPLE> * weight_model, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads)
    {
        const TCLASSIFIER classifier_update_weight = _C(1.0) / _C(this->m_iteration + 2);
        VectorDense<TCLASSIFIER, unsigned int> positive_weight(this->m_number_of_categories), negative_weight(this->m_number_of_categories);
        VectorDense<TCLASSIFIER, unsigned int> sample_update_weight(this->m_number_of_categories);
        VectorDense<unsigned int> positive_samples, negative_samples;
        
        this->calculatePositiveNegative(train_samples, number_of_samples, positive_samples, negative_samples);
        for (unsigned int c = 0; c < this->m_number_of_categories; ++c)
        {
            sample_update_weight[c] = classifier_update_weight / _C(positive_samples[c] + negative_samples[c]);
            positive_weight[c] = base_parameters[c].getPositiveWeight() * base_parameters[c].getBalancingFactor();
            negative_weight[c] = base_parameters[c].getNegativeWeight() * (_C(1.0) - base_parameters[c].getBalancingFactor());
        }
        
        if (number_of_threads > 1)
        {
            VectorDense<VectorDense<TCLASSIFIER, unsigned int> > sample_weight(this->m_number_of_categories);
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                sample_weight[i].set(number_of_samples);
            
            // Calculate the weight of each sample.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                for (unsigned int category_id = 0, index = thread_id; category_id < this->m_number_of_categories; ++category_id)
                {
                    const NLABEL classifier_label = this->m_categories_identifiers[category_id];
                    const TCLASSIFIER current_sample_update_weight = sample_update_weight[category_id] / base_parameters[category_id].getRegularizationFactor();
                    
                    for (unsigned int i = 0; i < number_of_samples; ++i, ++index)
                    {
                        if (index % number_of_threads == 0)
                        {
                            const TCLASSIFIER dot_product = this->classifierDot(weight_model[category_id], train_samples[i]->getData());
                            TLABEL probability;
                            
                            probability = train_samples[i]->getLabel(classifier_label);
                            if (this->m_ignore_difficult)
                            {
                                if (probability >= this->m_difficult_probability) // Positive sample.
                                    sample_weight[category_id][i] = positive_weight[category_id] * m_loss->derivative(dot_product) * current_sample_update_weight;
                                else if (probability < this->m_positive_probability) // Negative sample.
                                    sample_weight[category_id][i] = negative_weight[category_id] * -m_loss->derivative(-dot_product) * current_sample_update_weight;
                                else sample_weight[category_id][i] = _C(0); // Ignore the difficult.
                            }
                            else
                            {
                                if (probability >= this->m_positive_probability) // Positive sample.
                                    sample_weight[category_id][i] = positive_weight[category_id] * m_loss->derivative(dot_product) * current_sample_update_weight;
                                else // Negative sample.
                                    sample_weight[category_id][i] = negative_weight[category_id] * -m_loss->derivative(-dot_product) * current_sample_update_weight;
                            }
                        }
                    }
                }
            }
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                for (unsigned int category_id = thread_id; category_id < this->m_number_of_categories; category_id += number_of_threads)
                {
                    // 2) Update the classifier.
                    weight_model[category_id] *= _C(1.0) - classifier_update_weight;
                    for (unsigned int i = 0; i < number_of_samples; ++i)
                        this->weightVectorUpdate(weight_model[category_id], train_samples[i]->getData(), sample_weight[category_id][i]);
                    // 3) Optionally, use the projection optimization.
                    if (m_projection)
                    {
                        VectorNorm normal(EUCLIDEAN_NORM);
                        TCLASSIFIER classifier_normal;
                        
                        normal.norm(weight_model[category_id], classifier_normal);
                        if (std::sqrt(classifier_normal) > _C(1.0) / base_parameters[category_id].getRegularizationFactor())
                            weight_model[category_id] *= srvMin<TCLASSIFIER>(_C(1.0), std::sqrt(_C(1.0) / (base_parameters[category_id].getRegularizationFactor() * classifier_normal)));
                    }
                }
            }
        }
        else
        {
            VectorDense<TCLASSIFIER, unsigned int> sample_weight(number_of_samples);
            
            for (unsigned int category_id = 0; category_id < this->m_number_of_categories; ++category_id)
            {
                const NLABEL classifier_label = this->m_categories_identifiers[category_id];
                const TCLASSIFIER current_sample_update_weight = sample_update_weight[category_id] / base_parameters[category_id].getRegularizationFactor();
                
                // 1) Calculate the weights of the updates for each training example.
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    const TCLASSIFIER dot_product = this->classifierDot(weight_model[category_id], train_samples[i]->getData());
                    TLABEL probability;
                    
                    probability = train_samples[i]->getLabel(classifier_label);
                    if (this->m_ignore_difficult)
                    {
                        if (probability >= this->m_difficult_probability) // Positive weight.
                            sample_weight[i] = positive_weight[category_id] * m_loss->derivative(dot_product) * current_sample_update_weight;
                        else if (probability < this->m_positive_probability) // Negative weight.
                            sample_weight[i] = negative_weight[category_id] * -m_loss->derivative(-dot_product) * current_sample_update_weight;
                        else
                            sample_weight[i] = _C(0); // Ignore the difficult.
                    }
                    else
                    {
                        if (probability >= this->m_positive_probability) // Positive weight.
                            sample_weight[i] = positive_weight[category_id] * m_loss->derivative(dot_product) * current_sample_update_weight;
                        else // Negative weight.
                            sample_weight[i] = negative_weight[category_id] * -m_loss->derivative(-dot_product) * current_sample_update_weight;
                    }
                }
                // 2) Update the classifier.
                weight_model[category_id] *= _C(1.0) - classifier_update_weight;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                    this->weightVectorUpdate(weight_model[category_id], train_samples[i]->getData(), sample_weight[i]);
                // 3) Optionally, use the projection optimization.
                if (m_projection)
                {
                    VectorNorm normal(EUCLIDEAN_NORM);
                    TCLASSIFIER classifier_normal;
                    
                    normal.norm(weight_model[category_id], classifier_normal);
                    if (classifier_normal > _C(1.0) / base_parameters[category_id].getRegularizationFactor())
                        weight_model[category_id] *= srvMin<TCLASSIFIER>(_C(1.0), (_C(1.0) / std::sqrt(base_parameters[category_id].getRegularizationFactor())) / classifier_normal);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

