// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_ONLINE_CLASSIFIER_HEADER_FILE__
#define __SRV_ONLINE_CLASSIFIER_HEADER_FILE__

#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "../srv_xml.hpp"
#include "../srv_utilities.hpp"
#include "srv_classifier_definitions.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | LOSS FUNCTION CLASSES                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which defines a loss function.
    template <class T>
    class LossBase
    {
    public:
        /// Virtual destructor.
        virtual ~LossBase(void) {}
        /// Returns the output of the loss function at the specified value.
        virtual T function(const T &value) const = 0;
        /// Returns the output of the first derivative of the loss function at the specified value.
        virtual T derivative(const T &value) const = 0;
        /// Duplicates the current object (virtual constructor).
        virtual LossBase<T>* duplicate(void) const = 0;
        /// Returns the identifier of the loss function object class.
        virtual LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const = 0;
    protected:
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ LOGISTIC LOSS FUNCTION CLASS ]=============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Object which calculates the log loss output and first derivative values at a specified point
    template <class T>
    class LossLog : public LossBase<T>
    {
    public:
        /// Returns the output of the log loss function at the specified point.
        inline T function(const T &value) const
        {
            if (value >= (T)0.0) return (T)(log(1.0 + exp(-(double)value)));
            return -value + (T)log(1.0 + exp((double)value));
        }
        /// Returns the derivative of the log loss function at the specified value.
        inline T derivative(const T &value) const
        {
            if (value < (T)0.0) return (T)(1.0 / (exp((double)value) + 1.0));
            double ez = exp(-(double)value);
            return (T)(ez / (ez + 1.0));
        }
        /// Duplicates the current object (virtual constructor).
        inline LossBase<T>* duplicate(void) const { return (LossBase<T>*)new LossLog<T>(); }
        /// Returns the identifier of the loss function object class.
        inline LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const { return LOGISTIC_LOSS; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MARGIN LOGISTIC LOSS FUNCTION CLASS ]======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Object which calculates the log loss margin output and first derivative values at a specified point
    template <class T>
    class LossLogMargin : public LossBase<T>
    {
    public:
        /// Returns the output of the log loss margin function at the specified point.
        inline T function(const T &value) const
        {
            if (value >= (T)1.0) return (T)log(1.0 + exp(1.0 - (double)value));
            return (T)(1.0 - (double)value + log(1.0 + exp((double)value - 1.0)));
        }
        /// Returns the derivative of the log loss margin function at the specified value.
        inline T derivative(const T &value) const
        {
            if (value < (T)1.0) return (T)(1.0 / (exp((double)value - 1.0) + 1.0));
            double ez = exp(1.0 - (double)value);
            return (T)(ez / (ez + 1.0));
        }
        /// Duplicates the current object (virtual constructor).
        inline LossBase<T>* duplicate(void) const { return (LossBase<T>*)new LossLogMargin<T>(); }
        /// Returns the identifier of the loss function object class.
        inline LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const { return MARGIN_LOGISTIC_LOSS; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SMOOTH HINGE LOSS FUNCTION CLASS ]=========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Object which calculates the smooth hinge loss output and first derivative values at a specified point
    template <class T>
    class LossSmoothHinge : public LossBase<T>
    {
    public:
        /// Returns the output of the smooth hinge function at the specified point.
        inline T function(const T &value) const
        {
            if (value < (T)0.0) return (T)(0.5 - (double)value);
            else if (value < (T)1.0) return (T)(0.5 * (1 - (double)value) * (1 - (double)value));
            return 0;
        }
        /// Returns the derivative of the smooth hinge function at the specified value.
        inline T derivative(const T &value) const
        {
            if (value < (T)0.0) return 1;
            if (value < (T)1.0) return 1 - value;
            return 0;
        }
        /// Duplicates the current object (virtual constructor).
        inline LossBase<T>* duplicate(void) const { return (LossBase<T>*)new LossSmoothHinge<T>(); }
        /// Returns the identifier of the loss function object class.
        inline LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const { return SMOOTH_HINGE_LOSS; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SQUARE HINGE LOSS FUNCTION CLASS ]=========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Object which calculates the square hinge loss output and first derivative values at a specified point
    template <class T>
    class LossSquareHinge : public LossBase<T>
    {
    public:
        /// Returns the output of the square hinge function at the specified point.
        inline T function(const T &value) const
        {
            if (value < (T)1.0) return (T)(0.5 * (1.0 - (double)value) * (1.0 - (double)value));
            return 0;
        }
        /// Returns the derivative of the square hinge function at the specified value.
        inline T derivative(const T &value) const
        {
            if (value < (T)1.0) return 1 - value;
            return 0;
        }
        /// Duplicates the current object (virtual constructor).
        inline LossBase<T>* duplicate(void) const { return (LossBase<T>*)new LossSquareHinge<T>(); }
        /// Returns the identifier of the loss function object class.
        inline LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const { return SQUARE_HINGE_LOSS; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HINGE LOSS FUNCTION CLASS ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Object which calculates the hinge loss output and first derivative values at a specified point
    template <class T>
    class LossHinge : public LossBase<T>
    {
    public:
        /// Returns the output of the hinge function at the specified point.
        inline T function(const T &value) const
        {
            if (value < (T)1.0) return 1 - value;
            return 0;
        }
        /// Returns the derivative of the hinge function at the specified value.
        inline T derivative(const T &value) const
        {
            if (value < (T)1.0) return 1;
            return 0;
        }
        /// Duplicates the current object (virtual constructor).
        inline LossBase<T>* duplicate(void) const { return (LossBase<T>*)new LossHinge<T>(); }
        /// Returns the identifier of the loss function object class.
        inline LOSS_FUNCTION_IDENTIFIER getIdentifier(void) const { return HINGE_LOSS; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ FUNCTION WHICH GENERATES THE DIFFERENT LOSS FUNCTION OBJECTS ]=============================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Returns a new instance of the specified loss function object.
    template <class T>
    LossBase<T>* createLossObject(LOSS_FUNCTION_IDENTIFIER identifier)
    {
        if (identifier == LOGISTIC_LOSS) return (LossBase<T>*)new LossLog<T>();
        else if (identifier == MARGIN_LOGISTIC_LOSS) return (LossBase<T>*)new LossLogMargin<T>();
        else if (identifier == SMOOTH_HINGE_LOSS) return (LossBase<T>*)new LossSmoothHinge<T>();
        else if (identifier == SQUARE_HINGE_LOSS) return (LossBase<T>*)new LossSquareHinge<T>();
        else if (identifier == HINGE_LOSS) return (LossBase<T>*)new LossHinge<T>();
        else throw Exception("The selected loss is not known.");
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | ON-LINE LEARNING CLASSIFIER          |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Base pure virtual class for online learning linear classifiers.
    template <class TCLASSIFIER, class TSAMPLE = TCLASSIFIER, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class LinearOnlineLearnerBase : public LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LinearOnlineLearnerBase(void);
        /** Constructor where the dimensionality and the categories information is given as parameters.
         *  \param[in] number_of_dimensions dimensionality of the features categorized by the classifier.
         *  \param[in] categories_identifiers array with the identifiers for each classifier (i.e.\ the label associated with each classifier).
         *  \param[in] number_of_categories number of classifiers.
         *  \param[in] positive_probability minimum probability of a sample label to set it as positive.
         *  \param[in] difficult_probability maximum probability of the samples labeled as difficult.
         *  \param[in] ignore_difficult boolean flag which disables by the training algorithm the use of samples tagged as difficult.
         *  \param[in] average_classifier enables or disables the use of averaged weight models for the linear classifiers generated by the online learning algorithm.
         *  \param[in] average_skip the number of iterations at which the average model is updated.
         *  \param[in] chunk_size number of samples trained at once (set to 0 to use all samples to update the classifier).
         */
        LinearOnlineLearnerBase(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, bool average_classifier, unsigned int average_skip, unsigned int chunk_size);
        /// Copy constructor.
        LinearOnlineLearnerBase(const LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Copy constructor from a linear online learner method from another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        LinearOnlineLearnerBase(const LinearOnlineLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        /// Virtual destructor.
        virtual ~LinearOnlineLearnerBase(void);
        /// Assignation operator.
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Assignation operator from a linear online learner method from another type.
        template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
        LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const LinearOnlineLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other);
        
        // -[ Online learning functions which update the linear classifier model ]-------------------------------------------------------------------
        /// This function initializes the online learning algorithm.
        inline void initialize(void) { initialize(this->m_base_parameters); }
        /// Pure virtual function which initializes the online learning algorithm for the given regularization parameters.
        virtual void initialize(const ClassifierBaseParameters<TCLASSIFIER> * base_parameters) = 0;
        /** Updates the model of the online learning classifier.
         *  \param[in] train_samples array of pointers to the samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        void update(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads);
        /** Updates the model of the online learning classifier.
         *  \param[in] train_samples array of pointers to the samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in] base_parameters base parameters applied to all classifiers.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        inline void update(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> &base_parameters, unsigned int number_of_threads)
        {
            VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters_vector(this->m_number_of_categories, base_parameters);
            update(train_samples, number_of_samples, base_parameters_vector.getData(), number_of_threads);
        }
        /** Updates the model of the online learning classifier.
         *  \param[in] train_samples array of samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        void update(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads);
        /** Updates the model of the online learning classifier.
         *  \param[in] train_samples array of samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in] base_parameters base parameters applied to all classifiers.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        inline void update(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> &base_parameters, unsigned int number_of_threads)
        {
            VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters_vector(this->m_number_of_categories, base_parameters);
            update(train_samples, number_of_samples, base_parameters_vector.getData(), number_of_threads);
        }
        /** Updates the online learning classifier model when there are no more training samples. This function triggers the algorithm steps which only 
         *  happens at certain iterations of the algorithms and that will not be executed otherwise.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        void update(const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads);
        /** Updates the online learning classifier model when there are no more training samples. This function triggers the algorithm steps which only 
         *  happens at certain iterations of the algorithms and that will not be executed otherwise.
         *  \param[in] base_parameters base parameters applied to all classifiers.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        inline void update(const ClassifierBaseParameters<TCLASSIFIER> &base_parameters, unsigned int number_of_threads)
        {
            VectorDense<ClassifierBaseParameters<TCLASSIFIER> > base_parameters_vector(this->m_number_of_categories, base_parameters);
            update(base_parameters_vector.getData(), number_of_threads);
        }
        /** Function which trains the linear classifiers for the given set of training samples using the online learning algorithm as a batch algorithm.
         *  \param[in] train_samples array of pointers to the training samples.
         *  \param[in] number_of_train_samples number of elements in the training samples array.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to train the classifiers concurrently.
         *  \param[out] logger pointer to the logger used to show information about the training process (set to 0 to disable the log information).
         */
        void train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a boolean which is true when the classifiers weight model is averaged over iterations.
        inline bool isAverageClassifier(void) const { return m_average_classifier; }
        /// Enables or disables the use of average classifiers.
        void setAverageClassifier(bool average_classifier);
        /// Returns at which number of iterations the average weight model of the linear classifier is updated.
        inline unsigned int getAverageSkip(void) const { return m_average_skip; }
        /// Set at which number of iterations the average weight model of the linear classifier is updated.
        inline void setAverageSkip(unsigned int average_skip) { m_average_skip = srvMax<unsigned int>(1, average_skip); }
        /// Return the maximum number of samples used to update the linear classifier model.
        inline unsigned int getChunkSize(void) const { return m_chunk_size; }
        /// Sets the maximum number of samples used to update the linear classifier model (set to zero to process all samples at each iteration).
        inline void setChunkSize(unsigned int chunk_size) { m_chunk_size = chunk_size; }
        /// Returns the current iteration of the online learning algorithm.
        inline unsigned int getIteration(void) const { return m_iteration; }
        /// Returns the number of weight models which have been averaged by the online linear learner object.
        inline unsigned int getNumberOfAveraged(void) const { return m_number_of_averaged_models; }
        /// Returns a constant pointer to the internal weight models used by the linear classifier when the classifiers are averaged.
        inline const VectorDense<TCLASSIFIER, NSAMPLE> * getInnerClassifiers(void) const { return m_inner_classifiers; }
        /// Returns a constant reference to the index-th internal weight model used by the linear classifier when the classifiers are averaged.
        inline const VectorDense<TCLASSIFIER, NSAMPLE>& getInnerClassifier(unsigned int index) const { return m_inner_classifiers[index]; }
        /// Clears the model information of the linear classifiers of each category.
        virtual void clear(void);
        
    protected:
        /** Initializes the classifier basic parameters, i.e.\ the dimensionality of the samples categorized by the classifier and the categories information.
         *  \param[in] number_of_dimensions number of dimensions of the vectors categorized by the linear classifiers.
         *  \param[in] categories_identifiers category identifier of each classifier.
         *  \param[in] number_of_classifiers number of linear classifiers.
         */
        void set(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult);
        /** Initializes the specific parameters of the online learning classifier.
         *  \param[in] average_classifier enables or disables the use of an averaged weight model.
         *  \param[in] average_skip sets the number of iterations that the average model is updated.
         *  \param[in] chunk_size sets the size of the chunks used to train.
         *  \note this function resets the <b>iterations</b> and <b>number of averaged models</b> counters.
         */
        void set(bool average_classifier, unsigned int average_skip, unsigned int chunk_size);
        
        /** Pure virtual function which updates the model of the online learning classifier.
         *  \param[in] train_samples array of pointers to the samples used to update the linear classifiers model.
         *  \param[in] number_of_samples number of samples in the array.
         *  \param[in,out] weight_model array with the weight vectors of the linear classifiers updated by the function.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        virtual void updateStep(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, VectorDense<TCLASSIFIER, NSAMPLE> * weight_model, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads) = 0;
        /** Pure virtual function which updates the online learning classifier model when there are no more training samples.
         *  This function triggers the algorithm steps which only happens at certain iterations of the algorithms and that will
         *  not be executed otherwise.
         *  \param[in,out] weight_model array with the weight vectors of the linear classifiers updated by the function.
         *  \param[in] base_parameters array with the base parameters for each linear classifier.
         *  \param[in] number_of_threads number of threads used to concurrently process the training samples.
         */
        virtual void updateStep(VectorDense<TCLASSIFIER, NSAMPLE> * weight_model, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads) = 0;
        
        /// Flag which enables or disables the use of average classifiers (i.e. classifiers which average the weights of the linear classifiers models over iterations in order to obtain a softer estimation of the linear model).
        bool m_average_classifier;
        /// Counts the number of vectors which have been averaged.
        unsigned int m_number_of_averaged_models;
        /// Sets at which iterations the average model is updated.
        unsigned int m_average_skip;
        /// Array of auxiliary dense vectors to store the intermediate weights of the linear classifiers for the average model.
        VectorDense<TCLASSIFIER, NSAMPLE> * m_inner_classifiers;
        /// Iteration of the online learning algorithm.
        unsigned int m_iteration;
        /// Maximum number of training samples used at once to update the linear classifier models.
        unsigned int m_chunk_size;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | ON-LINE LEARNING CLASSIFIER          |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearOnlineLearnerBase(void) :
        m_average_classifier(0),
        m_number_of_averaged_models(0),
        m_average_skip(0),
        m_inner_classifiers(0),
        m_iteration(0),
        m_chunk_size(0)
    {
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearOnlineLearnerBase(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult, bool average_classifier, unsigned int average_skip, unsigned int chunk_size) :
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult),
        m_average_classifier(average_classifier),
        m_number_of_averaged_models(0),
        m_average_skip(average_skip),
        m_inner_classifiers(((number_of_categories > 0) && average_classifier)?new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_categories]:0),
        m_iteration(0),
        m_chunk_size(chunk_size)
    {
        if (average_classifier)
            for (unsigned int i = 0; i < number_of_categories; ++i)
                m_inner_classifiers[i].set(number_of_dimensions, (TCLASSIFIER)0);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearOnlineLearnerBase(const LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
        m_average_classifier(other.m_average_classifier),
        m_number_of_averaged_models(other.m_number_of_averaged_models),
        m_average_skip(other.m_average_skip),
        m_inner_classifiers((other.m_inner_classifiers != 0)?new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_categories]:0),
        m_iteration(other.m_iteration),
        m_chunk_size(other.m_chunk_size)
    {
        if (other.m_inner_classifiers != 0)
            for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                m_inner_classifiers[i] = other.m_inner_classifiers[i];
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LinearOnlineLearnerBase(const LinearOnlineLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other) :
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>(other),
        m_average_classifier(other.isAverageClassifier()),
        m_number_of_averaged_models(other.getNumberOfAveraged()),
        m_average_skip(other.getAverageSkip()),
        m_inner_classifiers((other.isAverageClassifier())?new VectorDense<TCLASSIFIER, NSAMPLE>[other.getNumberOfCategories()]:0),
        m_iteration(other.getIteration()),
        m_chunk_size(other.getChunkSize())
    {
        if (other.isAverageClassifier())
            for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
                m_inner_classifiers[i] = other.getInnerClassifier(i);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::~LinearOnlineLearnerBase(void)
    {
        if (m_inner_classifiers != 0) delete [] m_inner_classifiers;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
    {
        if (this != &other)
        {
            // Call the assignation operator of the mother class ....................................................................................
            LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
            
            // Free allocated memory ................................................................................................................
            if (m_inner_classifiers != 0) delete [] m_inner_classifiers;
            
            // Copy content .........................................................................................................................
            m_average_classifier = other.m_average_classifier;
            m_number_of_averaged_models = other.m_number_of_averaged_models;
            m_average_skip = other.m_average_skip;
            m_iteration = other.m_iteration;
            m_chunk_size = other.m_chunk_size;
            if (other.m_inner_classifiers != 0)
            {
                m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[other.m_number_of_categories];
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                    m_inner_classifiers[i] = other.m_inner_classifiers[i];
            }
            else m_inner_classifiers = 0;
        }
        
        return *this;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    template <class TCLASSIFIER_OTHER, class TSAMPLE_OTHER, class NSAMPLE_OTHER, class TLABEL_OTHER, class NLABEL_OTHER>
    LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>& LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const LinearOnlineLearnerBase<TCLASSIFIER_OTHER, TSAMPLE_OTHER, NSAMPLE_OTHER, TLABEL_OTHER, NLABEL_OTHER> &other)
    {
        // Call the assignation operator of the mother class ........................................................................................
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(other);
        
        // Free allocated memory ....................................................................................................................
        if (m_inner_classifiers != 0) delete [] m_inner_classifiers;
        
        // Copy content .............................................................................................................................
        m_average_classifier = other.isAverageClassifier();
        m_number_of_averaged_models = other.getNumberOfAveraged();
        m_average_skip = other.getAverageSkip();
        m_iteration = other.getIteration();
        m_chunk_size = other.getChunkSize();
        
        if (other.isAverageClassifier())
        {
            m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[other.getNumberOfCategories()];
            for (unsigned int i = 0; i < other.getNumberOfCategories(); ++i)
                m_inner_classifiers[i] = other.getInnerClassifier(i);
        }
        else m_inner_classifiers = 0;
        
        return *this;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ FUNCTIONS WHICH SET THE CLASSIFIERS PARAMETERS ]===========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::set(NSAMPLE number_of_dimensions, const NLABEL * categories_identifiers, unsigned int number_of_categories, TLABEL positive_probability, TLABEL difficult_probability, bool ignore_difficult)
    {
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::set(number_of_dimensions, categories_identifiers, number_of_categories, positive_probability, difficult_probability, ignore_difficult);
        if (m_inner_classifiers != 0)
        {
            delete [] m_inner_classifiers;
            
            m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[number_of_categories];
            for (unsigned int i = 0; i < number_of_categories; ++i)
                m_inner_classifiers[i].set(number_of_dimensions, (TCLASSIFIER)0);
            m_number_of_averaged_models = 0;
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::set(bool average_classifier, unsigned int average_skip, unsigned int chunk_size)
    {
        if (m_inner_classifiers != 0) delete [] m_inner_classifiers;
        
        if (average_classifier)
        {
            m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[this->m_number_of_categories];
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                m_inner_classifiers[i].set(this->m_number_of_dimensions, (TCLASSIFIER)0);
        }
        else m_inner_classifiers = 0;
        
        m_average_skip = average_skip;
        m_chunk_size = chunk_size;
        m_number_of_averaged_models = 0;
        m_iteration = 0;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::setAverageClassifier(bool average_classifier)
    {
        if (average_classifier != m_average_classifier)
        {
            if (average_classifier)
            {
                m_inner_classifiers = new VectorDense<TCLASSIFIER, NSAMPLE>[this->m_number_of_categories];
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    m_inner_classifiers[i] = this->m_classifiers[i];
                m_number_of_averaged_models = 1;
            }
            else
            {
                update(this->m_base_parameters, 1);
                delete [] m_inner_classifiers;
                m_inner_classifiers = 0;
            }
            m_average_classifier = false;
        }
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::clear(void)
    {
        LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::clear();
        m_number_of_averaged_models = 0;
        if (m_inner_classifiers != 0)
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                m_inner_classifiers[i].setValue(0);
        m_iteration = 0;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MODEL UPDATE FUNCTIONS ]===================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::update(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads)
    {
        if (m_average_classifier)
        {
            updateStep(train_samples, number_of_samples, m_inner_classifiers, base_parameters, number_of_threads);
            if (m_iteration % m_average_skip == 0)
            {
                const TCLASSIFIER n = (TCLASSIFIER)m_number_of_averaged_models;
                
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    this->m_classifiers[i] = (this->m_classifiers[i] * n + m_inner_classifiers[i]) / (n + 1);
                ++m_number_of_averaged_models;
            }
        }
        else updateStep(train_samples, number_of_samples, this->m_classifiers, base_parameters, number_of_threads);
        ++m_iteration;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::update(const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * train_samples, unsigned int number_of_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads)
    {
        VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> *> train_samples_ptrs(number_of_samples);
        for (unsigned int i = 0; i < number_of_samples; ++i) train_samples_ptrs[i] = &train_samples[i];
        if (m_average_classifier)
        {
            updateStep(train_samples_ptrs.getData(), number_of_samples, m_inner_classifiers, base_parameters, number_of_threads);
            if (m_iteration % m_average_skip == 0)
            {
                const TCLASSIFIER n = (TCLASSIFIER)m_number_of_averaged_models;
                
                for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                    this->m_classifiers[i] = (this->m_classifiers[i] * n + m_inner_classifiers[i]) / (n + 1);
                ++m_number_of_averaged_models;
            }
        }
        else updateStep(train_samples_ptrs.getData(), number_of_samples, this->m_classifiers, base_parameters, number_of_threads);
        ++m_iteration;
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::update(const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads)
    {
        if (m_average_classifier)
        {
            const TCLASSIFIER n = (TCLASSIFIER)m_number_of_averaged_models;
            
            // Forward the final update event to the derived class.
            updateStep(m_inner_classifiers, base_parameters, number_of_threads);
            // Average the classifier model.
            for (unsigned int i = 0; i < this->m_number_of_categories; ++i)
                this->m_classifiers[i] = (this->m_classifiers[i] * n + m_inner_classifiers[i]) / (n + 1);
            ++m_number_of_averaged_models;
        }
        else updateStep(this->m_classifiers, base_parameters, number_of_threads);
    }
    
    template <class TCLASSIFIER, class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void LinearOnlineLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>::train(Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> const * const * train_samples, unsigned int number_of_train_samples, const ClassifierBaseParameters<TCLASSIFIER> * base_parameters, unsigned int number_of_threads, BaseLogger * logger)
    {
        const unsigned int current_step = (m_chunk_size == 0)?number_of_train_samples:m_chunk_size;
        const unsigned int number_of_epochs = 51;
        //const unsigned int number_of_epochs = 50;
        VectorDense<const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > random_resorted_samples(number_of_train_samples);
        VectorDense<unsigned int> indexes(number_of_train_samples);
        VectorDense<unsigned int> positive, negative;
        
        // Initialize the model.
        initialize(base_parameters);
        for (unsigned int i = 0; i < number_of_train_samples; ++i) indexes[i] = i;
        
        for (unsigned int epoch = 0; epoch < number_of_epochs; ++epoch)
        {
            if (logger != 0)
                logger->log("Epoch %d of %d: Resorting the %d training samples and updating the classifier with chunks of %d samples.", epoch, number_of_epochs, number_of_train_samples, current_step);
            // Re-sort the training samples.
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                srvSwap(indexes[i], indexes[i + rand() % (number_of_train_samples - i)]);
            for (unsigned int i = 0; i < number_of_train_samples; ++i)
                random_resorted_samples[i] = train_samples[indexes[i]];
            
            // Update the classifiers model with the maximum size specified by the maximum chunk size.
            for (unsigned int current_index = 0; current_index < number_of_train_samples; current_index += current_step)
            {
                if (current_index + current_step <= number_of_train_samples)
                    update(&(random_resorted_samples.getData()[current_index]), current_step, base_parameters, number_of_threads);
                else
                    update(&(random_resorted_samples.getData()[current_index]), number_of_train_samples - current_index, base_parameters, number_of_threads);
            }
            // Send the final update event to the derived class.
            update(base_parameters, number_of_threads);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

