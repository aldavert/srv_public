// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_DOCUMENT_DATABASE_INFORMATION_HPP_HEADER_FILE__
#define __SRV_DOCUMENT_DATABASE_INFORMATION_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <list>
#include <set>
#include <map>
// -[ SRV header files]------------------------------------------------
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | SYMBOL DISABLE CLASS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Table with the symbols which are dismissed while creating a database or during the evaluation process.
    class SymbolDismissTable
    {
    public:
        /// Default constructor.
        SymbolDismissTable(void) {}
        /// Adds a new symbol to the dismissed table.
        inline void add(const char * identifier) { m_dismiss_table.insert(std::string(identifier)); }
        /// Adds a new symbol to the dismissed table.
        inline void add(const std::string &identifier) { m_dismiss_table.insert(identifier); }
        /// Clears the dismissed table.
        inline void clear(void) { m_dismiss_table.clear(); }
        /// Boolean function which returns true when the symbol identifier is in the dismissed table.
        inline bool isDismessed(const char * identifier) const { return m_dismiss_table.find(std::string(identifier)) != m_dismiss_table.end(); }
        /// Boolean function which returns true when the symbol identifier is in the dismissed table.
        inline bool isDismessed(const std::string &identifier) const { return m_dismiss_table.find(identifier) != m_dismiss_table.end(); }
        /// Stores the dismissed table information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the dismissed table information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        std::set<std::string> m_dismiss_table;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL EQUIVALENCE CLASS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Symbol equivalence class.
    class SymbolEquivalenceTable
    {
    public:
        /// Default constructor.
        SymbolEquivalenceTable(void) {}
        /// Adds a new element into the equivalence table.
        inline void add(const char * origin, const char * destination) { m_equivalence_table[std::string(origin)] = std::string(destination); }
        /// Adds a new element into the equivalence table.
        inline void add(const std::string &origin, const std::string &destination) { m_equivalence_table[origin] = destination; }
        /// Clears the equivalence table.
        inline void clear(void) { m_equivalence_table.clear(); }
        /// Returns the number of elements in the equivalence table.
        inline unsigned int getNumberOfElements(void) const { return (unsigned int)m_equivalence_table.size(); }
        /// Returns a boolean which is true when the given identifier has a equivalent value in the table.
        inline bool hasEquivalent(const char * identifier) const { return m_equivalence_table.find(std::string(identifier)) != m_equivalence_table.end(); }
        /// Returns the equivalent char array of the given identifier. This function generates an exception when the identifier is not found.
        inline const char * getEquivalent(const char * identifier) const
        {
            std::map<std::string, std::string>::const_iterator search = m_equivalence_table.find(std::string(identifier));
            if (search == m_equivalence_table.end()) throw Exception("Identifier '%s' not found in the table.", identifier);
            else return search->second.c_str();
        }
        /// Stores the equivalence table information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the equivalence table information from an XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Equivalence table which stores the conversion between the original identifier and the equivalent identifier.
        std::map<std::string, std::string> m_equivalence_table;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL INFORMATION CLASS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    const unsigned int SYMBOL_STRING_LENGTH = 64;
    
    /// Container class which stores the symbol annotation information.
    class SymbolInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SymbolInformation(void);
        /** Constructor which initializes the symbol information.
         *  \param[in] identifier unique identifier string of the symbol.
         *  \param[in] transcription Unicode transcription of the symbol.
         *  \param[in] symbol_identifier unique identifier of the symbol.
         */
        SymbolInformation(const char * identifier, unsigned int * transcription, unsigned int symbol_identifier);
        /** Constructor which initializes the symbol information without the transcription.
         *  \param[in] identifier unique identifier string of the symbol.
         *  \param[in] symbol_identifier unique identifier of the symbol.
         */
        SymbolInformation(const char * identifier, unsigned int symbol_identifier);
        /// Copy constructor.
        SymbolInformation(const SymbolInformation &other);
        /// Destructor.
        ~SymbolInformation(void);
        /// Assignation operator.
        SymbolInformation& operator=(const SymbolInformation &other);
        /** This function sets the symbol information.
         *  \param[in] identifier unique identifier string of the symbol.
         *  \param[in] transcription Unicode transcription of the symbol.
         *  \param[in] symbol_identifier unique identifier of the symbol.
         */
        void set(const char * identifier, unsigned int * transcription, unsigned int symbol_identifier);
        /** This function sets the symbol information without the transcription.
         *  \param[in] identifier unique identifier string of the symbol.
         *  \param[in] symbol_identifier unique identifier of the symbol.
         */
        void set(const char * identifier, unsigned int symbol_identifier);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the char array with the unique symbol identifier.
        inline const char * getIdentifier(void) const { return m_identifier; }
        /// Returns a pointer to the char array with the unique symbol identifier.
        inline char * getIdentifier(void) { return m_identifier; }
        /// Sets the unique identifier of the symbol.
        void setIdentifier(const char * identifier);
        
        /// Returns a constant pointer to the array with the Unicode transcription of the symbol.
        inline const unsigned int * getTranscription(void) const { return m_transcription; }
        /// Returns a pointer to the array with the Unicode transcription of the symbol.
        inline unsigned int * getTranscription(void) { return m_transcription; }
        /// Sets the Unicode transcription of the symbol.
        void setTranscription(const unsigned int * transcription);
        
        /// Returns the unique identifier of the symbol.
        inline unsigned int getSymbolIdentifier(void) const { return m_symbol_identifier; }
        /// Sets the unique identifier of the symbol.
        inline void setSymbolIdentifier(unsigned int symbol_identifier) { m_symbol_identifier = symbol_identifier; }
        
        // -[ Comparison functions ]-----------------------------------------------------------------------------------------------------------------
        /// Returns a boolean which is true when the current symbol identifier is smaller than the other symbol identifier.
        bool operator<(const SymbolInformation &other) const;
        /// Returns a boolean which is true when the current symbol identifier is smaller or equal than the other symbol identifier.
        bool operator<=(const SymbolInformation &other) const;
        /// Returns a boolean which is true when the current symbol identifier is larger than the other symbol identifier.
        bool operator>(const SymbolInformation &other) const;
        /// Returns a boolean which is true when the current symbol identifier is larger or equal than the other symbol identifier.
        bool operator>=(const SymbolInformation &other) const;
        /// Returns a boolean which is true when the current symbol identifier is equal than the other symbol identifier.
        bool operator==(const SymbolInformation &other) const;
        /// Returns a boolean which is true when the current symbol identifier is different than the other symbol identifier.
        bool operator!=(const SymbolInformation &other) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the symbol information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the symbol information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Unique char string which identifiers the symbol in the database.
        char m_identifier[SYMBOL_STRING_LENGTH];
        /// Unicode transcription of the symbol.
        unsigned int m_transcription[SYMBOL_STRING_LENGTH];
        /// Identifier assigned to the symbol.
        unsigned int m_symbol_identifier;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL LOCALIZATION CLASS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the information of the bounding box of a symbol in a document page.
    class SymbolLocalization
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SymbolLocalization(void);
        /** Constructor which sets the localization of the symbol in the page.
         *  \param[in] x X-coordinate of the center of the bounding box.
         *  \param[in] y Y-coordinate of the center of the bounding box.
         *  \param[in] width width of the bounding box.
         *  \param[in] height height of the bounding box.
         *  \param[in] orientation orientation of the bounding box.
         *  \param[in] symbol_identifier identifier of the symbol within the bounding box.
         */
        SymbolLocalization(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation, unsigned int symbol_identifier);
        /// Copy constructor.
        SymbolLocalization(const SymbolLocalization &other);
        /// Destructor.
        ~SymbolLocalization(void);
        /// Assignation operator.
        SymbolLocalization& operator=(const SymbolLocalization &other);
        /** Sets the localization information of the symbol in the document page.
         *  \param[in] x X-coordinate of the center of the bounding box.
         *  \param[in] y Y-coordinate of the center of the bounding box.
         *  \param[in] width width of the bounding box.
         *  \param[in] height height of the bounding box.
         *  \param[in] orientation orientation of the bounding box.
         *  \param[in] symbol_identifier identifier of the symbol within the bounding box.
         */
        void set(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation, unsigned int symbol_identifier);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the X-coordinate of the center of the bounding box of the symbol.
        inline unsigned int getX(void) const { return m_x; }
        /// Returns the Y-coordinate of the center of the bounding box of the symbol.
        inline unsigned int getY(void) const { return m_y; }
        /// Returns the width of the symbol bounding box.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the symbol bounding box.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the orientation of the bounding box.
        inline float getOrientation(void) const { return m_orientation; }
        /// Returns the identifier of the symbol within the bounding box.
        inline unsigned int getSymbolIdentifier(void) const { return m_symbol_identifier; }
        
        /** Sets the geometric information of the bounding box.
         *  \param[in] x X-coordinate of the center of the bounding box.
         *  \param[in] y Y-coordinate of the center of the bounding box.
         *  \param[in] width width of the bounding box.
         *  \param[in] height height of the bounding box.
         *  \param[in] orientation orientation of the bounding box.
         */
        void setGeometry(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation);
        /// Sets the identifier of the symbol of the bounding box.
        inline void setSymbolIdentifier(unsigned int symbol_identifier) { m_symbol_identifier = symbol_identifier; }
        
        // -[ Image access functions ]---------------------------------------------------------------------------------------------------------------
        /** Draws the bounding box in the image.
         *  \param[in] image draws the bounding box int the document page.
         *  \param[in] pencil pencil used to draw the bounding box.
         */
        template <template <class> class IMAGE, class T>
        inline void drawBoundingBox(IMAGE<T> &image, Draw::Pencil<T> &pencil) const { Draw::Rectangle(image, (int)m_x, (int)m_y, (int)m_width, (int)m_height, (double)m_orientation, pencil); }
        /** Copies the patch within the bounding box and returns it.
         *  \param[in] image original image or sub-image with the original page.
         *  \param[in] patch patch with the resulting bounding box.
         *  \param[in] factor_width resize factor applied to the width of symbol bounding box.
         *  \param[in] factor_height resize factor applied to the width of symbol bounding box.
         */
        template <template <class> class IMAGE, class T>
        void cutPatch(IMAGE<T> &image, Image<T> &patch, double factor_width, double factor_height) const;
        
        /** Copies the patch within the bounding box and returns it.
         *  \param[in] image original image or sub-image with the original page.
         *  \param[in] patch patch with the resulting bounding box.
         *  \param[in] factor_width resize factor applied to the symbol bounding box.
         */
        template <template <class> class IMAGE, class T>
        inline void cutPatch(IMAGE<T> &image, Image<T> &patch, double factor) const { cutPatch(image, patch, factor, factor); }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the symbol information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the symbol information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// X-coordinate of the center of the bounding box of the symbol.
        unsigned int m_x;
        /// Y-coordinate of the center of the bounding box of the symbol.
        unsigned int m_y;
        /// Width of the bounding box.
        unsigned int m_width;
        /// Height of the bounding box.
        unsigned int m_height;
        /// Orientation of the bounding box.
        float m_orientation;
        /// Identifier of the symbol of the bounding box.
        unsigned int m_symbol_identifier;
    };
    
    template <template <class> class IMAGE, class T>
    void SymbolLocalization::cutPatch(IMAGE<T> &image, Image<T> &patch, double factor_width, double factor_height) const
    {
        const int width = (int)(factor_width * (double)m_width / 2.0);
        const int height = (int)(factor_height * (double)m_height / 2.0);
        const double cos_a = cos(m_orientation);
        const double sin_a = sin(m_orientation);
        double rx, ry;
        int px, py;
        
        patch.setGeometry(2 * width + 1, 2 * height + 1, image.getNumberOfChannels());
        for (int y = -height, patch_y = 0; y <= height; ++y, ++patch_y)
        {
            for (int x = -width, patch_x = 0; x <= width; ++x, ++patch_x)
            {
                rx = ( (double)x * cos_a + (double)y * sin_a) + (double)m_x;
                ry = ((double)-x * sin_a + (double)y * cos_a) + (double)m_y;
                
                px = (int)rx;
                py = (int)ry;
                if ((px >= 0) && (py >= 0) && (px < (int)image.getWidth() - 1) && (py < (int)image.getHeight() - 1))
                {
                    double w00, w01, w10, w11, dx, dy;
                    
                    dx = (rx - (double)px);
                    dy = (ry - (double)py);
                    w00 = (1.0 - dx) * (1.0 - dy);
                    w01 = (1.0 - dx) * dy;
                    w10 = dx * (1.0 - dy);
                    w11 = dx * dy;
                    
                    for (unsigned int color = 0; color < image.getNumberOfChannels(); ++color)
                    {
                        const T * __restrict__ l0_ptr = image.get(px, py, color);
                        const T * __restrict__ l1_ptr = image.get(px, py, color);
                        *patch.get(patch_x, patch_y, color) = (T)((double)l0_ptr[0] * w00 + (double)l0_ptr[1] * w10 + (double)l1_ptr[0] * w01 + (double)l1_ptr[1] * w11);
                    }
                }
                else
                {
                    for (unsigned int color = 0; color < image.getNumberOfChannels(); ++color)
                        *patch.get(patch_x, patch_y, color) = 0;
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | LINE INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the information about a textual line.
    class LineInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        LineInformation(void);
        /** Constructor which sets the number of symbols at the line.
         *  \param[in] number_of_symbols number of symbols within the line.
         *  \param[in] line_number number of the line in the page.
         */
        LineInformation(unsigned int number_of_symbols, unsigned int line_number);
        /** Constructor which initializes the symbols stored in the line.
         *  \param[in] symbols array with the information of each symbol in the line.
         *  \param[in] number_of_symbols number of symbols within the line.
         *  \param[in] line_number number of the line in the page.
         */
        LineInformation(const SymbolLocalization * symbols, unsigned int number_of_symbols, unsigned int line_number);
        /// Copy constructor.
        LineInformation(const LineInformation &other);
        /// Destructor.
        ~LineInformation(void);
        /// Assignation operator.
        LineInformation& operator=(const LineInformation &other);
        /// This function initializes the symbols stores in the line.
        void set(const SymbolLocalization * symbols, unsigned int number_of_symbols, unsigned int line_number);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array of symbols.
        inline const SymbolLocalization * getSymbols(void) const { return m_symbols; }
        /// Returns a pointer to the array of symbols.
        inline SymbolLocalization * getSymbols(void) { return m_symbols; }
        /// Returns a constant reference to the index-th symbol.
        inline const SymbolLocalization& getSymbol(unsigned int index) const { return m_symbols[index]; }
        /// Returns a reference to the index-th symbol.
        inline SymbolLocalization& getSymbol(unsigned int index) { return m_symbols[index]; }
        /// Returns the number of symbols in the line.
        inline unsigned int getNumberOfSymbols(void) const { return m_number_of_symbols; }
        /// Sets the number of symbols in the line.
        void setNumberOfSymbols(unsigned int number_of_symbols);
        /// Returns the number of the line within the page.
        inline unsigned int getLineNumber(void) const { return m_line_number; }
        /// Sets the number of the line within the page.
        inline void setLineNumber(unsigned int line_number) { m_line_number = line_number; }
        
        /// Returns a constant reference to the index-th symbol of the line.
        inline const SymbolLocalization& operator[](unsigned int index) const { return m_symbols[index]; }
        /// Returns a reference to the index-th symbol of the line.
        inline SymbolLocalization& operator[](unsigned int index) { return m_symbols[index]; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of the line symbols into an XML object.
        void convertToXML(XmlParser &parser);
        /// Retrieves the information of the line symbols from an XML objects.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Array with the information of the words which form the line.
        SymbolLocalization * m_symbols;
        /// Number of words in the line.
        unsigned int m_number_of_symbols;
        /// Number of the line within the page.
        unsigned int m_line_number;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BLOCK INFORMATION CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the information of the different lines which can be grouped together.
    class BlockInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        BlockInformation(void);
        /** Constructor which sets the lines information of the whole block.
         *  \param[in] lines array with the information of the lines in the block.
         *  \param[in] number_of_lines number of lines in the block.
         *  \param[in] identifier char array with the identifier or definition of the block of text.
         */
        BlockInformation(const LineInformation * lines, unsigned int number_of_lines, const char * identifier);
        /** Constructor which initializes the number of lines in the block.
         *  \param[in] number_of_lines number of lines in the block.
         *  \param[in] identifier char array with the identifier or definition of the block of text.
         */
        BlockInformation(unsigned int number_of_lines, const char * identifier);
        /// Copy constructor.
        BlockInformation(const BlockInformation &other);
        /// Destructor.
        ~BlockInformation(void);
        /// Assignation operator.
        BlockInformation& operator=(const BlockInformation &other);
        /** This function sets the lines information of the whole block.
         *  \param[in] lines array with the information of the lines in the block.
         *  \param[in] number_of_lines number of lines in the block.
         *  \param[in] identifier char array with the identifier or definition of the block of text.
         */
        void set(const LineInformation * lines, unsigned int number_of_lines, const char * identifier);
        /** This function initializes the number of lines in the block.
         *  \param[in] number_of_lines number of lines in the block.
         *  \param[in] identifier char array with the identifier or definition of the block of text.
         */
        void set(unsigned int number_of_lines, const char * identifier);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array with the information of the lines of the block.
        inline const LineInformation * getLines(void) const { return m_lines; }
        /// Returns a constant reference to the index-th line information of the block.
        inline const LineInformation& getLine(unsigned int index) const { return m_lines[index]; }
        /// Returns a reference to the index-th line information of the block.
        inline LineInformation& getLine(unsigned int index) { return m_lines[index]; }
        /// Returns the number of lines in the block.
        inline unsigned int getNumberOfLines(void) const { return m_number_of_lines; }
        /// Sets the number of lines of the block.
        inline void setNumberOfLines(unsigned int number_of_lines);
        /// Returns a constant pointer to the char array with the identifier of the block.
        inline const char * getIdentifier(void) const { return m_identifier; }
        /// Returns a pointer to the char array with the identifier of the block.
        inline char * getIdentifier(void) { return m_identifier; }
        /// Sets the identifier of the block.
        void setIdentifier(const char * identifier);
        
        /// Returns a constant reference to the index-th line of the block.
        inline const LineInformation& operator[](unsigned int index) const { return m_lines[index]; }
        /// Returns a reference to the index-th line of the block.
        inline LineInformation& operator[](unsigned int index) { return m_lines[index]; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the block information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the block information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Array of lines which build the textual block.
        LineInformation * m_lines;
        /// Number of lines in the block
        unsigned int m_number_of_lines;
        /// Textual identifier of the block of text.
        char m_identifier[SYMBOL_STRING_LENGTH];
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PAGE INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the meta-data information of the whole document page.
    class PageInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        PageInformation(void);
        /** Constructor which initializes the information of the page image and its text blocks.
         *  \param[in] image_basename filename of the image of the document page.
         *  \param[in] width width of the page image.
         *  \param[in] height height of the page image.
         *  \param[in] blocks array with the information of the text blocks in the image.
         *  \param[in] number_of_blocks number of text information blocks of the page.
         */
        PageInformation(const char * image_basename, unsigned int width, unsigned int height, const BlockInformation * blocks, unsigned int number_of_blocks);
        /** Constructor which initializes the information of the page image and the number of text blocks.
         *  \param[in] image_basename filename of the image of the document page.
         *  \param[in] width width of the page image.
         *  \param[in] height height of the page image.
         *  \param[in] number_of_blocks number of text information blocks of the page.
         */
        PageInformation(const char * image_basename, unsigned int width, unsigned int height, unsigned int number_of_blocks);
        /// Copy constructor.
        PageInformation(const PageInformation &other);
        /// Destructor.
        ~PageInformation(void);
        /// Assignation operator.
        PageInformation& operator=(const PageInformation &other);
        /** This functions sets the information of the page image and its text blocks.
         *  \param[in] image_basename filename of the image of the document page.
         *  \param[in] width width of the page image.
         *  \param[in] height height of the page image.
         *  \param[in] blocks array with the information of the text blocks in the image.
         *  \param[in] number_of_blocks number of text information blocks of the page.
         */
        void set(const char * image_basename, unsigned int width, unsigned int height, const BlockInformation * blocks, unsigned int number_of_blocks);
        /** This function sets the information of the page image and the number of text blocks.
         *  \param[in] image_basename filename of the image of the document page.
         *  \param[in] width width of the page image.
         *  \param[in] height height of the page image.
         *  \param[in] number_of_blocks number of text information blocks of the page.
         */
        void set(const char * image_basename, unsigned int width, unsigned int height, unsigned int number_of_blocks);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the char array with filename of the page image in the database.
        inline const char * getImageBasename(void) const { return m_image_basename; }
        /// Sets the char array of the filename of the page image in the database.
        void setImageBasename(const char * image_basename);
        /// Returns a constant to pointer to the char array of the image page filename.
        inline const char * getImageFilename(void) const { return m_image_filename; }
        /// Sets the route to the database for the image page filename.
        void setRoute(const char * route);
        /// Returns the width of the image of the page.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the image of the page.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Sets the width of the image of the page.
        inline void setWidth(unsigned int width) { m_width = width; }
        /// Sets the height of the image of the page.
        inline void setHeight(unsigned int height) { m_height = height; }
        /// Sets the geometry of the image of the page.
        inline void setImageGeometry(unsigned int width, unsigned int height) { m_width = width; m_height = height; }
        /// Returns a constant pointer to the array of text blocks information of the page.
        inline const BlockInformation * getBlocks(void) const { return m_blocks; }
        /// Returns a constant reference to the index-th block information of the page.
        inline const BlockInformation& getBlock(unsigned int index) const { return m_blocks[index]; }
        /// Returns a reference to the index-th block information of the page.
        inline BlockInformation& getBlock(unsigned int index) { return m_blocks[index]; }
        /// Returns the number of text blocks in the image.
        inline unsigned int getNumberOfBlocks(void) const { return m_number_of_blocks; }
        /// Sets the number of text blocks in the image.
        void setNumberOfBlocks(unsigned int number_of_blocks);
        
        /// Returns a constant reference to the index-th block of the page.
        inline const BlockInformation& operator[](unsigned int index) const { return m_blocks[index]; }
        /// Returns a reference to the index-th block of the page.
        inline BlockInformation& operator[](unsigned int index) { return m_blocks[index]; }
        
        /// This function returns the image of the page.
        template <class T>
        inline void loadImage(Image<T> &image) const { image.load(m_image_filename); }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the page information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the page information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Base filename of the image of the page.
        char m_image_basename[256];
        /// Route to the image file where the page is stored.
        char m_image_filename[4096];
        /// Width of the image of the page.
        unsigned int m_width;
        /// Height of the image of the page.
        unsigned int m_height;
        /// Pointer to the block structures.
        BlockInformation * m_blocks;
        /// Number of text blocks in the page.
        unsigned int m_number_of_blocks;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PARAGRAPH INFORMATION CLASS          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the information of text paragraphs which can be found across several document pages.
    class ParagraphInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ParagraphInformation(void);
        /** Constructor which initializes the information of the paragraph.
         *  \param[in] pages array with the page index of each block of text of the paragraph.
         *  \param[in] blocks array with the block index in the page of each block of text of the paragraph.
         *  \param[in] number_of_blocks number of blocks of text in the paragraph.
         *  \param[in] transcription transcription of the text in the paragraph.
         */
        ParagraphInformation(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks, const std::string &transcription);
        /** Constructor which initializes the number of blocks and the transcription of the paragraph.
         *  \param[in] number_of_blocks number of blocks of text in the paragraph.
         *  \param[in] transcription transcription of the text in the paragraph.
         */
        ParagraphInformation(unsigned int number_of_blocks, std::string &transcription);
        /// Copy constructor.
        ParagraphInformation(const ParagraphInformation &other);
        /// Destructor.
        ~ParagraphInformation(void);
        /// Assignation operator.
        ParagraphInformation& operator=(const ParagraphInformation &other);
        /** This function sets the information of the paragraph.
         *  \param[in] pages array with the page index of each block of text of the paragraph.
         *  \param[in] blocks array with the block index in the page of each block of text of the paragraph.
         *  \param[in] number_of_blocks number of blocks of text in the paragraph.
         *  \param[in] transcription transcription of the text in the paragraph.
         */
        void set(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks, const std::string &transcription);
        /** This functions sets the number of blocks and the transcription of the paragraph.
         *  \param[in] number_of_blocks number of blocks of text in the paragraph.
         *  \param[in] transcription transcription of the text in the paragraph.
         */
        void set(unsigned int number_of_blocks, std::string &transcription);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant array to the indexes of the pages of each block of text of the paragraph.
        inline const unsigned int * getPageIndexes(void) const { return m_page_index; }
        /// Returns the page index of the index-th block of text of the paragraph.
        inline unsigned int getPageIndex(unsigned int index) const { return m_page_index[index]; }
        /// Returns a constant array to the indexes of the blocks at each page where the paragraph appears.
        inline const unsigned int * getBlockIndexes(void) const { return m_block_index; }
        /// Returns the block index in the page of each block of text of the paragraph.
        inline unsigned int getBlockIndex(unsigned int index) const { return m_block_index[index]; }
        /** Sets the page index and block index in the page of each block of text of the paragraph.
         *  \param[in] pages array with the page index of each block of text.
         *  \param[in] blocks array with the block index in the page of each block of text.
         *  \param[in] number_of_blocks number of blocks of text.
         */
        void setIndexes(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks);
        /** Sets the page and block indexes of the index-th block of text of the paragraph.
         *  \param[in] page index of the page of the block of text.
         *  \param[in] block index of the block in the page of the block of text.
         *  \param[in] index index of the modified block of text.
         */
        inline void setIndexes(unsigned int page, unsigned int block, unsigned int index) { m_page_index[index] = page; m_block_index[index] = block; }
        /// Returns the number of text blocks in the paragraph.
        inline unsigned int getNumberOfBlocks(void) const { return m_number_of_blocks; }
        /// Sets the number of text blocks in the paragraph.
        void setNumberOfBlocks(unsigned int number_of_blocks);
        /// Returns a constant reference to the string with the transcription of the paragraph.
        inline const std::string& getTranscription(void) const { return m_transcription; }
        /// Sets the transcription of the whole paragraph.
        inline void setTranscription(const std::string& transcription) { m_transcription = transcription; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the paragraph information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the paragraph information form an XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Index of the page of the text block.
        unsigned int * m_page_index;
        /// Index of the block in the page of the text block.
        unsigned int * m_block_index;
        /// Number of text blocks of the paragraph.
        unsigned int m_number_of_blocks;
        /// Transcription of the whole paragraph.
        std::string m_transcription;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT DATABASE CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the indexes needed to access to a certain symbol of the document.
    class SymbolAccessInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SymbolAccessInformation(void) : m_page(0), m_block(0), m_line(0), m_position(0) {}
        /** Constructor which sets the indexes to access to a certain symbol in the document.
         *  \param[in] page index of the page.
         *  \param[in] block index of the block of text in the page.
         *  \param[in] line number of the line in the block of text.
         *  \param[in] position index of the symbol in the line.
         */
        SymbolAccessInformation(unsigned int page, unsigned int block, unsigned int line, unsigned int position) :
            m_page(page), m_block(block), m_line(line), m_position(position) {}
        /// Copy constructor.
        SymbolAccessInformation(const SymbolAccessInformation &other) :
            m_page(other.m_page), m_block(other.m_block), m_line(other.m_line), m_position(other.m_position) {}
        /// Destructor.
        ~SymbolAccessInformation(void) {}
        /// Assignation operator.
        SymbolAccessInformation& operator=(const SymbolAccessInformation &other);
        /** This function sets the indexes to access to a certain symbol in the document.
         *  \param[in] page index of the page.
         *  \param[in] block index of the block of text in the page.
         *  \param[in] line number of the line in the block of text.
         *  \param[in] position index of the symbol in the line.
         */
        void set(unsigned int page, unsigned int block, unsigned int line, unsigned int position);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the page index of the symbol
        inline unsigned int getPage(void) const { return m_page; }
        /// Sets the page index of the symbol.
        inline void setPage(unsigned int page) { m_page = page; }
        /// Returns the index of the block of text in the page of the symbol.
        inline unsigned int getBlock(void) const { return m_block; }
        /// Sets the index of the block of text in the page of the symbol.
        inline void setBlock(unsigned int block) { m_block = block; }
        /// Returns the number of the line of the symbol in the block of text.
        inline unsigned int getLine(void) const { return m_line; }
        /// Sets the number of the line of the symbol in the block of text.
        inline void setLine(unsigned int line) { m_line = line; }
        /// Returns the symbol position within the line.
        inline unsigned int getPosition(void) const { return m_position; }
        /// Sets the symbol position within the line.
        inline void setPosition(unsigned int position) { m_position = position; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the symbol access information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the symbol access information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Index of the page where the symbol appears.
        unsigned int m_page;
        /// Index of the block text of the page where the symbol appears.
        unsigned int m_block;
        /// Number of line within the block where the symbol appears.
        unsigned int m_line;
        /// Position of the symbol in the line.
        unsigned int m_position;
    };
    
    
    /// Container class which stores the meta-data information of a document database.
    class DocumentDatabase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        DocumentDatabase(void);
        /// Constructor which creates a new database from the given pages and paragraphs information.
        DocumentDatabase(const SymbolInformation * symbols, unsigned int number_of_symbols, const PageInformation * pages, unsigned int number_of_pages, const ParagraphInformation * paragraphs, unsigned int number_of_paragraphs);
        /// Constructor which loads the database from a file.
        DocumentDatabase(const char * filename);
        /// Copy constructor.
        DocumentDatabase(const DocumentDatabase &other);
        /// Destructor.
        ~DocumentDatabase(void);
        /// Assignation operator.
        DocumentDatabase& operator=(const DocumentDatabase &other);
        /// Function which creates a new database from the given pages and paragraphs information.
        void create(const SymbolInformation * symbols, unsigned int number_of_symbols, const PageInformation * pages, unsigned int number_of_pages, const ParagraphInformation * paragraphs, unsigned int number_of_paragraphs);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array of pages of the database.
        inline const PageInformation * getPages(void) const { return m_pages; }
        /// Returns a constant reference to the index-th page of the database.
        inline const PageInformation& getPage(unsigned int index) const { return m_pages[index]; }
        /// Returns a constant reference to the index-th page of the database.
        inline const PageInformation& operator[](unsigned int index) const { return m_pages[index]; }
        /// Returns the number of pages in the database.
        inline unsigned int getNumberOfPages(void) const { return m_number_of_pages; }
        /// Returns a constant pointer to the paragraphs of the database.
        inline const ParagraphInformation * getParagraphs(void) const { return m_paragraphs; }
        /// Returns a constant reference to the index-th paragraph of the database.
        inline const ParagraphInformation& getParagraphs(unsigned int index) const { return m_paragraphs[index]; }
        /// Returns the number of paragraphs in the database.
        inline unsigned int getNumberOfParagraphs(void) const { return m_number_of_paragraphs; }
        /// Returns a constant pointer to the unique symbols array of the database.
        inline const SymbolInformation * getSymbols(void) const { return m_symbols; }
        /// Returns a constant reference to the index-th unique symbol of the database.
        inline const SymbolInformation& getSymbol(unsigned int index) const { return m_symbols[index]; }
        /// Returns a constant reference to the list with the information of each occurrence of the index-th symbol.
        inline const std::list<SymbolAccessInformation>& getSymbolAccess(unsigned int index) const { return m_symbol_access[index]; }
        /// Returns the number of unique symbols of the database.
        inline unsigned int getNumberOfSymbols(void) const { return m_number_of_symbols; }
        /** Returns the index-th of the given symbol identifier.
         *  \param[in] identifier char array with the string of the symbol identifier.
         *  \returns the index of the symbol identifier with the given string.
         *  \throw srv::Exception the symbol identifier is not found in the database.
         */
        unsigned int getSymbolIdentifier(const char * identifier) const
        {
            SymbolInformation query(identifier, 0);
            std::set<SymbolInformation *, compare_symbols_pointers>::const_iterator search = m_document_symbols.find(&query);
            if (search == m_document_symbols.end()) throw Exception("Symbol '%s' not found in the database.", identifier);
            return (*search)->getSymbolIdentifier();
        }
        /// Sets the route to the database for each page.
        void setRoute(const char * route);
        /// Loads the database from the given filename.
        void load(const char * filename);
        /// Saves the database to the given filename.
        void save(const char * filename) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the document database information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the document database information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Structure which allows to compare the pointed symbols instead of the memory addresses.
        struct compare_symbols_pointers { bool operator() (const SymbolInformation * first, const SymbolInformation * second) const { return *first < *second; } };
        
        /// Array with the unique symbols present at the document.
        SymbolInformation * m_symbols;
        /// Array with a list with the information where each unique symbol appears in the document.
        std::list<SymbolAccessInformation> * m_symbol_access;
        /// Inverted file to access to the symbol identifier for a word.
        std::set<SymbolInformation *, compare_symbols_pointers> m_document_symbols;
        /// Number of unique symbols in the document.
        unsigned int m_number_of_symbols;
        /// Array with the information of the pages of the document.
        PageInformation * m_pages;
        /// Number of pages in the document.
        unsigned int m_number_of_pages;
        /// Array with the information of the paragraph of the document.
        ParagraphInformation * m_paragraphs;
        /// Number of paragraphs in the document database.
        unsigned int m_number_of_paragraphs;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

