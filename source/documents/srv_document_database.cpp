// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_document_database.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | SYMBOL DISABLE CLASS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void SymbolDismissTable::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Dismiss_Table");
        parser.addChildren();
        for (std::set<std::string>::const_iterator begin = m_dismiss_table.begin(), end = m_dismiss_table.end(); begin != end; ++begin)
        {
            parser.openTag("Symbol");
            parser.setAttribute("Identifier", begin->c_str());
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    void SymbolDismissTable::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Dismiss_Table"))
        {
            m_dismiss_table.clear();
            while (!(parser.isTagIdentifier("Dismiss_Table") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Symbol"))
                {
                    m_dismiss_table.insert((const char *)parser.getAttribute("Identifier"));
                    while (!(parser.isTagIdentifier("Symbol") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL EQUIVALENCE CLASS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void SymbolEquivalenceTable::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Equivalence_Table");
        parser.addChildren();
        for (std::map<std::string, std::string>::const_iterator begin = m_equivalence_table.begin(), end = m_equivalence_table.end(); begin != end; ++begin)
        {
            parser.openTag("Symbol");
            parser.setAttribute("Origin", begin->first.c_str());
            parser.setAttribute("Destination", begin->second.c_str());
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    void SymbolEquivalenceTable::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Equivalence_Table"))
        {
            m_equivalence_table.clear();
            while (!(parser.isTagIdentifier("Equivalence_Table") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Symbol"))
                {
                    std::string origin, destination;
                    
                    origin = (const char *)parser.getAttribute("Origin");
                    destination = (const char *)parser.getAttribute("Destination");
                    m_equivalence_table[origin] = destination;
                    while (!(parser.isTagIdentifier("Symbol") && parser.isCloseTag()))
                        parser.getNext();
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL INFORMATION CLASS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    SymbolInformation::SymbolInformation(void) :
        m_symbol_identifier(0)
    {
        for (unsigned int index = 0; index < SYMBOL_STRING_LENGTH; ++index)
        {
            m_identifier[index] = '\0';
            m_transcription[index] = 0;
        }
    }
    
    SymbolInformation::SymbolInformation(const char * identifier, unsigned int * transcription, unsigned int symbol_identifier) :
        m_symbol_identifier(symbol_identifier)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (transcription[index] != 0); ++index)
            m_transcription[index] = transcription[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_transcription[index] = 0;
    }
    
    SymbolInformation::SymbolInformation(const char * identifier, unsigned int symbol_identifier) :
        m_symbol_identifier(symbol_identifier)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
        for (index = 0; index < SYMBOL_STRING_LENGTH; ++index)
            m_transcription[index] = 0;
    }
    
    SymbolInformation::SymbolInformation(const SymbolInformation &other) :
        m_symbol_identifier(other.m_symbol_identifier)
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            m_identifier[i] = other.m_identifier[i];
            m_transcription[i] = other.m_transcription[i];
        }
    }
    
    SymbolInformation::~SymbolInformation(void)
    {
    }
    
    SymbolInformation& SymbolInformation::operator=(const SymbolInformation &other)
    {
        if (this != &other)
        {
            for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
            {
                m_identifier[i] = other.m_identifier[i];
                m_transcription[i] = other.m_transcription[i];
            }
            m_symbol_identifier = other.m_symbol_identifier;
        }
        
        return *this;
    }
    
    void SymbolInformation::set(const char * identifier, unsigned int * transcription, unsigned int symbol_identifier)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (transcription[index] != '\0'); ++index)
            m_transcription[index] = transcription[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_transcription[index] = 0;
        m_symbol_identifier = symbol_identifier;
    }
    
    void SymbolInformation::set(const char * identifier, unsigned int symbol_identifier)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
        for (index = 0; index < SYMBOL_STRING_LENGTH; ++index)
            m_transcription[index] = 0;
        m_symbol_identifier = symbol_identifier;
    }
    
    void SymbolInformation::setIdentifier(const char * identifier)
    {
        unsigned int index;
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
    }
    
    void SymbolInformation::setTranscription(const unsigned int * transcription)
    {
        unsigned int index;
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (transcription[index] != '\0'); ++index)
            m_transcription[index] = transcription[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_transcription[index] = 0;
    }
    
    bool SymbolInformation::operator<(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return m_identifier[i] < other.m_identifier[i];
        }
        return false;
    }
    
    bool SymbolInformation::operator<=(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return m_identifier[i] < other.m_identifier[i];
        }
        return true;
    }
    
    bool SymbolInformation::operator>(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return m_identifier[i] > other.m_identifier[i];
        }
        return false;
    }
    
    bool SymbolInformation::operator>=(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return m_identifier[i] > other.m_identifier[i];
        }
        return true;
    }
    
    bool SymbolInformation::operator==(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return false;
        }
        return true;
    }
    
    bool SymbolInformation::operator!=(const SymbolInformation &other) const
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
        {
            if (m_identifier[i] == other.m_identifier[i]) continue;
            return true;
        }
        return false;
    }
    
    void SymbolInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Symbol_Information");
        parser.setAttribute("Symbol_Identifier", m_symbol_identifier);
        parser.addChildren();
        saveVector(parser, "Identifier", ConstantSubVectorDense<char, unsigned int>(m_identifier, SYMBOL_STRING_LENGTH));
        saveVector(parser, "Transcription", ConstantSubVectorDense<unsigned int, unsigned int>(m_transcription, SYMBOL_STRING_LENGTH));
        parser.closeTag();
    }
    
    void SymbolInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Symbol_Information"))
        {
            m_symbol_identifier = parser.getAttribute("Symbol_Identifier");
            for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
            {
                m_identifier[i] = '\0';
                m_transcription[i] = 0;
            }
            
            while (!(parser.isTagIdentifier("Symbol_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Identifier"))
                {
                    VectorDense<char> data;
                    unsigned int index;
                    
                    loadVector(parser, "Identifier", data);
                    for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (data[index] != '\0'); ++index)
                        m_identifier[index] = data[index];
                    for (; index < SYMBOL_STRING_LENGTH; ++index)
                        m_identifier[index] = '\0';
                }
                else if (parser.isTagIdentifier("Transcription"))
                {
                    VectorDense<unsigned int> data;
                    unsigned int index;
                    
                    loadVector(parser, "Transcription", data);
                    for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (data[index] != 0); ++index)
                        m_transcription[index] = data[index];
                    for (; index < SYMBOL_STRING_LENGTH; ++index)
                        m_transcription[index] = 0;
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SYMBOL LOCALIZATION CLASS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    SymbolLocalization::SymbolLocalization(void) :
        m_x(0),
        m_y(0),
        m_width(0),
        m_height(0),
        m_orientation(0),
        m_symbol_identifier(0)
    {
    }
    
    SymbolLocalization::SymbolLocalization(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation, unsigned int symbol_identifier) :
        m_x(x),
        m_y(y),
        m_width(width),
        m_height(height),
        m_orientation(orientation),
        m_symbol_identifier(symbol_identifier)
    {
    }
    
    SymbolLocalization::SymbolLocalization(const SymbolLocalization &other) :
        m_x(other.m_x),
        m_y(other.m_y),
        m_width(other.m_width),
        m_height(other.m_height),
        m_orientation(other.m_orientation),
        m_symbol_identifier(other.m_symbol_identifier)
    {
    }
    
    SymbolLocalization::~SymbolLocalization(void)
    {
    }
    
    SymbolLocalization& SymbolLocalization::operator=(const SymbolLocalization &other)
    {
        if (this != &other)
        {
            m_x = other.m_x;
            m_y = other.m_y;
            m_width = other.m_width;
            m_height = other.m_height;
            m_orientation = other.m_orientation;
            m_symbol_identifier = other.m_symbol_identifier;
        }
        
        return *this;
    }
    
    void SymbolLocalization::set(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation, unsigned int symbol_identifier)
    {
        m_x = x;
        m_y = y;
        m_width = width;
        m_height = height;
        m_orientation = orientation;
        m_symbol_identifier = symbol_identifier;
    }
    
    void SymbolLocalization::setGeometry(unsigned int x, unsigned int y, unsigned int width, unsigned int height, float orientation)
    {
        m_x = x;
        m_y = y;
        m_width = width;
        m_height = height;
        m_orientation = orientation;
    }
    
    void SymbolLocalization::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Symbol_Localization");
        parser.setAttribute("X", m_x);
        parser.setAttribute("Y", m_y);
        parser.setAttribute("Width", m_width);
        parser.setAttribute("Height", m_height);
        parser.setAttribute("Orientation", m_orientation);
        parser.setAttribute("Identifier", m_symbol_identifier);
        parser.closeTag();
    }
    
    void SymbolLocalization::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Symbol_Localization"))
        {
            m_x = parser.getAttribute("X");
            m_y = parser.getAttribute("Y");
            m_width = parser.getAttribute("Width");
            m_height = parser.getAttribute("Height");
            m_orientation = parser.getAttribute("Orientation");
            m_symbol_identifier = parser.getAttribute("Identifier");
            
            while (!(parser.isTagIdentifier("Symbol_Localization") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | LINE INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    LineInformation::LineInformation(void) :
        m_symbols(0),
        m_number_of_symbols(0),
        m_line_number(0)
    {
    }
    
    LineInformation::LineInformation(unsigned int number_of_symbols, unsigned int line_number) :
        m_symbols((number_of_symbols > 0)?new SymbolLocalization[number_of_symbols]:0),
        m_number_of_symbols(number_of_symbols),
        m_line_number(line_number)
    {
    }
    
    LineInformation::LineInformation(const SymbolLocalization * symbols, unsigned int number_of_symbols, unsigned int line_number) :
        m_symbols((number_of_symbols > 0)?new SymbolLocalization[number_of_symbols]:0),
        m_number_of_symbols(number_of_symbols),
        m_line_number(line_number)
    {
        for (unsigned int i = 0; i < number_of_symbols; ++i)
            m_symbols[i] = symbols[i];
    }
    
    LineInformation::LineInformation(const LineInformation &other) :
        m_symbols((other.m_symbols != 0)?new SymbolLocalization[other.m_number_of_symbols]:0),
        m_number_of_symbols(other.m_number_of_symbols),
        m_line_number(other.m_line_number)
    {
        for (unsigned int i = 0; i < other.m_number_of_symbols; ++i)
            m_symbols[i] = other.m_symbols[i];
    }
    
    LineInformation::~LineInformation(void)
    {
        if (m_symbols != 0) delete [] m_symbols;
    }
    
    LineInformation& LineInformation::operator=(const LineInformation &other)
    {
        if (this != &other)
        {
            if (m_symbols != 0) delete [] m_symbols;
            
            if (other.m_symbols != 0)
            {
                m_symbols = new SymbolLocalization[other.m_number_of_symbols];
                m_number_of_symbols = other.m_number_of_symbols;
                for (unsigned int i = 0; i < other.m_number_of_symbols; ++i)
                    m_symbols[i] = other.m_symbols[i];
            }
            else
            {
                m_symbols = 0;
                m_number_of_symbols = 0;
            }
            m_line_number = other.m_line_number;
        }
        
        return *this;
    }
    
    void LineInformation::set(const SymbolLocalization * symbols, unsigned int number_of_symbols, unsigned int line_number)
    {
        if (m_symbols != 0) delete [] m_symbols;
        
        m_line_number = line_number;
        if (number_of_symbols > 0)
        {
            m_symbols = new SymbolLocalization[number_of_symbols];
            m_number_of_symbols = number_of_symbols;
            for (unsigned int i = 0; i < number_of_symbols; ++i)
                m_symbols[i] = symbols[i];
        }
        else
        {
            m_symbols = 0;
            m_number_of_symbols = 0;
        }
    }
    
    void LineInformation::setNumberOfSymbols(unsigned int number_of_symbols)
    {
        if (m_symbols != 0) delete [] m_symbols;
        
        if (number_of_symbols) m_symbols = new SymbolLocalization[number_of_symbols];
        else m_symbols = 0;
        m_number_of_symbols = number_of_symbols;
    }
    
    void LineInformation::convertToXML(XmlParser &parser)
    {
        parser.openTag("Line_Information");
        parser.setAttribute("Number_Of_Symbols", m_number_of_symbols);
        parser.setAttribute("Line_Number", m_line_number);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_symbols; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_symbols[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void LineInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Line_Information"))
        {
            if (m_symbols != 0) delete [] m_symbols;
            
            m_number_of_symbols = parser.getAttribute("Number_Of_Symbols");
            m_line_number = parser.getAttribute("Line_Number");
            if (m_number_of_symbols > 0) m_symbols = new SymbolLocalization[m_number_of_symbols];
            else m_symbols = 0;
            
            while (!(parser.isTagIdentifier("Line_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Symbol_Localization"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_symbols[index].convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BLOCK INFORMATION CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    BlockInformation::BlockInformation(void) :
        m_lines(0),
        m_number_of_lines(0)
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
            m_identifier[i] = '\0';
    }
    
    BlockInformation::BlockInformation(const LineInformation * lines, unsigned int number_of_lines, const char * identifier) :
        m_lines((number_of_lines > 0)?new LineInformation[number_of_lines]:0),
        m_number_of_lines(number_of_lines)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] == '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
        for (index = 0; index < number_of_lines; ++index)
            m_lines[index] = lines[index];
    }
    
    BlockInformation::BlockInformation(unsigned int number_of_lines, const char * identifier) :
        m_lines((number_of_lines > 0)?new LineInformation[number_of_lines]:0),
        m_number_of_lines(number_of_lines)
    {
        unsigned int index;
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] == '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
    }
    
    BlockInformation::BlockInformation(const BlockInformation &other) :
        m_lines((other.m_number_of_lines > 0)?new LineInformation[other.m_number_of_lines]:0),
        m_number_of_lines(other.m_number_of_lines)
    {
        for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
            m_identifier[i] = other.m_identifier[i];
        for (unsigned int i = 0; i < other.m_number_of_lines; ++i)
            m_lines[i] = other.m_lines[i];
    }
    
    BlockInformation::~BlockInformation(void)
    {
        if (m_lines != 0) delete [] m_lines;
    }
    
    BlockInformation& BlockInformation::operator=(const BlockInformation &other)
    {
        if (this != &other)
        {
            if (other.m_number_of_lines > 0)
            {
                m_lines = new LineInformation[other.m_number_of_lines];
                m_number_of_lines = other.m_number_of_lines;
                for (unsigned int i = 0; i < other.m_number_of_lines; ++i)
                    m_lines[i] = other.m_lines[i];
            }
            else
            {
                m_lines = 0;
                m_number_of_lines = 0;
            }
            for (unsigned int i = 0; i < SYMBOL_STRING_LENGTH; ++i)
                m_identifier[i] = other.m_identifier[i];
        }
        
        return *this;
    }
    
    void BlockInformation::set(const LineInformation * lines, unsigned int number_of_lines, const char * identifier)
    {
        unsigned int index;
        
        if (m_lines != 0) delete [] m_lines;
        
        if (number_of_lines > 0)
        {
            m_lines = new LineInformation[number_of_lines];
            m_number_of_lines = number_of_lines;
            for (unsigned int i = 0; i < number_of_lines; ++i)
                m_lines[i] = lines[i];
        }
        else
        {
            m_lines = 0;
            m_number_of_lines = 0;
        }
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
    }
    
    void BlockInformation::set(unsigned int number_of_lines, const char * identifier)
    {
        unsigned int index;
        
        if (m_lines != 0) delete [] m_lines;
        
        if (number_of_lines > 0)
        {
            m_lines = new LineInformation[number_of_lines];
            m_number_of_lines = number_of_lines;
        }
        else
        {
            m_lines = 0;
            m_number_of_lines = 0;
        }
        
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
    }
    
    void BlockInformation::setIdentifier(const char * identifier)
    {
        unsigned int index;
        for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (identifier[index] != 0); ++index)
            m_identifier[index] = identifier[index];
        for (; index < SYMBOL_STRING_LENGTH; ++index)
            m_identifier[index] = '\0';
    }
    
    void BlockInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Block_Information");
        parser.setAttribute("Number_Of_Lines", m_number_of_lines);
        parser.addChildren();
        saveVector(parser, "Identifier", ConstantSubVectorDense<char, unsigned int>(m_identifier, SYMBOL_STRING_LENGTH));
        for (unsigned int i = 0; i < m_number_of_lines; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_lines[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void BlockInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Block_Information"))
        {
            if (m_lines != 0) delete [] m_lines;
            
            m_number_of_lines = parser.getAttribute("Number_Of_Lines");
            if (m_number_of_lines > 0) m_lines = new LineInformation[m_number_of_lines];
            else m_lines = 0;
            
            while (!(parser.isTagIdentifier("Block_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Identifier"))
                {
                    VectorDense<char, unsigned int> data;
                    unsigned int index;
                    
                    loadVector(parser, "Identifier", data);
                    for (index = 0; (index < SYMBOL_STRING_LENGTH - 1) && (data[index] != '\0'); ++index)
                        m_identifier[index] = data[index];
                    for (; index < SYMBOL_STRING_LENGTH; ++index)
                        m_identifier[index] = '\0';
                }
                else if (parser.isTagIdentifier("Line_Information"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_lines[index].convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PAGE INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    PageInformation::PageInformation(void) :
        m_width(0),
        m_height(0),
        m_blocks(0),
        m_number_of_blocks(0)
    {
        for (unsigned int i = 0; i < 256; ++i) m_image_basename[i] = '\0';
        for (unsigned int i = 0; i < 4096; ++i) m_image_filename[i] = '\0';
    }
    
    PageInformation::PageInformation(const char * image_basename, unsigned int width, unsigned int height, const BlockInformation * blocks, unsigned int number_of_blocks) :
        m_width(width),
        m_height(height),
        m_blocks((number_of_blocks > 0)?new BlockInformation[number_of_blocks]:0),
        m_number_of_blocks(number_of_blocks)
    {
        unsigned int index;
        
        for (index = 0; (index < 255) && (image_basename[index] != '\0'); ++index)
        {
            m_image_basename[index] = image_basename[index];
            m_image_filename[index] = image_basename[index];
        }
        for (; index < 256; ++index)
        {
            m_image_basename[index] = '\0';
            m_image_filename[index] = '\0';
        }
        for (; index < 4096; ++index)
            m_image_filename[index] = '\0';
        
        for (unsigned int i = 0; i < number_of_blocks; ++i)
            m_blocks[i] = blocks[i];
    }
    
    PageInformation::PageInformation(const char * image_basename, unsigned int width, unsigned int height, unsigned int number_of_blocks) :
        m_width(width),
        m_height(height),
        m_blocks((number_of_blocks > 0)?new BlockInformation[number_of_blocks]:0),
        m_number_of_blocks(number_of_blocks)
    {
        unsigned int index;
        
        for (index = 0; (index < 255) && (image_basename[index] != '\0'); ++index)
        {
            m_image_basename[index] = image_basename[index];
            m_image_filename[index] = image_basename[index];
        }
        for (; index < 256; ++index)
        {
            m_image_basename[index] = '\0';
            m_image_filename[index] = '\0';
        }
        for (; index < 4096; ++index)
            m_image_filename[index] = '\0';
    }
    
    PageInformation::PageInformation(const PageInformation &other) :
        m_width(other.m_width),
        m_height(other.m_height),
        m_blocks((other.m_number_of_blocks > 0)?new BlockInformation[other.m_number_of_blocks]:0),
        m_number_of_blocks(other.m_number_of_blocks)
    {
        for (unsigned int i = 0; i < 256; ++i) m_image_basename[i] = other.m_image_basename[i];
        for (unsigned int i = 0; i < 4096; ++i) m_image_filename[i] = other.m_image_filename[i];
        for (unsigned int i = 0; i < other.m_number_of_blocks; ++i)
            m_blocks[i] = other.m_blocks[i];
    }
    
    PageInformation::~PageInformation(void)
    {
        if (m_blocks != 0) delete [] m_blocks;
    }
    
    PageInformation& PageInformation::operator=(const PageInformation &other)
    {
        if (this != &other)
        {
            if (m_blocks != 0) delete [] m_blocks;
            
            for (unsigned int i = 0; i < 256; ++i) m_image_basename[i] = other.m_image_basename[i];
            for (unsigned int i = 0; i < 4096; ++i) m_image_filename[i] = other.m_image_filename[i];
            
            m_width = other.m_width;
            m_height = other.m_height;
            if (other.m_number_of_blocks > 0)
            {
                m_blocks = new BlockInformation[other.m_number_of_blocks];
                m_number_of_blocks = other.m_number_of_blocks;
                for (unsigned int i = 0; i < other.m_number_of_blocks; ++i)
                    m_blocks[i] = other.m_blocks[i];
            }
            else
            {
                m_blocks = 0;
                m_number_of_blocks = 0;
            }
        }
        
        return *this;
    }
    
    void PageInformation::set(const char * image_basename, unsigned int width, unsigned int height, const BlockInformation * blocks, unsigned int number_of_blocks)
    {
        unsigned int index;
        
        for (index = 0; (index < 255) && (image_basename[index] != '\0'); ++index)
        {
            m_image_basename[index] = image_basename[index];
            m_image_filename[index] = image_basename[index];
        }
        for (; index < 256; ++index)
        {
            m_image_basename[index] = '\0';
            m_image_filename[index] = '\0';
        }
        for (; index < 4096; ++index)
            m_image_filename[index] = '\0';
        
        m_width = width;
        m_height = height;
        
        if (m_blocks != 0) delete [] m_blocks;
        if (number_of_blocks > 0)
        {
            m_blocks = new BlockInformation[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
            for (unsigned int i = 0; i < number_of_blocks; ++i)
                m_blocks[i] = blocks[i];
        }
        else
        {
            m_blocks = 0;
            m_number_of_blocks = 0;
        }
    }
    
    void PageInformation::set(const char * image_basename, unsigned int width, unsigned int height, unsigned int number_of_blocks)
    {
        unsigned int index;
        
        for (index = 0; (index < 255) && (image_basename[index] != '\0'); ++index)
        {
            m_image_basename[index] = image_basename[index];
            m_image_filename[index] = image_basename[index];
        }
        for (; index < 256; ++index)
        {
            m_image_basename[index] = '\0';
            m_image_filename[index] = '\0';
        }
        for (; index < 4096; ++index)
            m_image_filename[index] = '\0';
        
        m_width = width;
        m_height = height;
        
        if (m_blocks != 0) delete [] m_blocks;
        if (number_of_blocks > 0)
        {
            m_blocks = new BlockInformation[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
        }
        else
        {
            m_blocks = 0;
            m_number_of_blocks = 0;
        }
    }
    
    void PageInformation::setImageBasename(const char * image_basename)
    {
        unsigned int index;
        for (index = 0; (index < 255) && (image_basename[index] != '\0'); ++index)
            m_image_basename[index] = image_basename[index];
        for (; index < 256; ++index)
            m_image_basename[index] = '\0';
    }
    
    void PageInformation::setRoute(const char * route)
    {
        unsigned int index;
        for (index = 0; (index < 4096 - 256 - 2) && route[index] != '\0'; ++index)
            m_image_filename[index] = route[index];
        if (index == 0)
        {
            m_image_filename[index] = '.';
            ++index;
        }
        if (m_image_filename[index - 1] != '/')
        {
            m_image_filename[index] = '/';
            ++index;
        }
        for (unsigned int k = 0; (k < 256) && (m_image_basename[k] != '\0'); ++k, ++index)
            m_image_filename[index] = m_image_basename[k];
        for (; index < 4096; ++index)
            m_image_filename[index] = '\0';
    }
    
    void PageInformation::setNumberOfBlocks(unsigned int number_of_blocks)
    {
        if (m_blocks != 0) delete [] m_blocks;
        
        if (number_of_blocks > 0)
        {
            m_blocks = new BlockInformation[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
        }
        else
        {
            m_blocks = 0;
            m_number_of_blocks = 0;
        }
    }
    
    void PageInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Page_Information");
        parser.setAttribute("Width", m_width);
        parser.setAttribute("Height", m_height);
        parser.setAttribute("Number_Of_Blocks", m_number_of_blocks);
        parser.addChildren();
        saveVector(parser, "Image_Filename", ConstantSubVectorDense<char, unsigned int>(m_image_basename, 256));
        for (unsigned int i = 0; i < m_number_of_blocks; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_blocks[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void PageInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Page_Information"))
        {
            if (m_blocks != 0) delete [] m_blocks;
            
            m_number_of_blocks = parser.getAttribute("Number_Of_Blocks");
            m_width = parser.getAttribute("Width");
            m_height = parser.getAttribute("Height");
            if (m_number_of_blocks > 0) m_blocks = new BlockInformation[m_number_of_blocks];
            else m_blocks = 0;
            
            while (!(parser.isTagIdentifier("Page_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Image_Filename"))
                {
                    VectorDense<char, unsigned int> data;
                    unsigned int index;
                    
                    loadVector(parser, "Image_Filename", data);
                    for (index = 0; (index < 255) && (data[index] != '\0'); ++index)
                    {
                        m_image_basename[index] = data[index];
                        m_image_filename[index] = data[index];
                    }
                    for (; index < 256; ++index)
                    {
                        m_image_basename[index] = '\0';
                        m_image_filename[index] = '\0';
                    }
                    for (; index < 4096; ++index)
                        m_image_filename[index] = '\0';
                }
                else if (parser.isTagIdentifier("Block_Information"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_blocks[index].convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PARAGRAPH INFORMATION CLASS          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ParagraphInformation::ParagraphInformation(void) :
        m_page_index(0),
        m_block_index(0),
        m_number_of_blocks(0)
    {
    }
    
    ParagraphInformation::ParagraphInformation(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks, const std::string &transcription) :
        m_page_index((number_of_blocks > 0)?new unsigned int[number_of_blocks]:0),
        m_block_index((number_of_blocks > 0)?new unsigned int[number_of_blocks]:0),
        m_number_of_blocks(number_of_blocks),
        m_transcription(transcription)
    {
        for (unsigned int i = 0; i < number_of_blocks; ++i)
        {
            m_page_index[i] = pages[i];
            m_block_index[i] = blocks[i];
        }
    }
    
    ParagraphInformation::ParagraphInformation(unsigned int number_of_blocks, std::string &transcription) :
        m_page_index((number_of_blocks > 0)?new unsigned int[number_of_blocks]:0),
        m_block_index((number_of_blocks > 0)?new unsigned int[number_of_blocks]:0),
        m_number_of_blocks(number_of_blocks),
        m_transcription(transcription)
    {
        for (unsigned int i = 0; i < number_of_blocks; ++i)
        {
            m_page_index[i] = 0;
            m_block_index[i] = 0;
        }
    }
    
    ParagraphInformation::ParagraphInformation(const ParagraphInformation &other) :
        m_page_index((other.m_number_of_blocks > 0)?new unsigned int[other.m_number_of_blocks]:0),
        m_block_index((other.m_number_of_blocks > 0)?new unsigned int[other.m_number_of_blocks]:0),
        m_number_of_blocks(other.m_number_of_blocks),
        m_transcription(other.m_transcription)
    {
    }
    
    ParagraphInformation::~ParagraphInformation(void)
    {
        if (m_page_index != 0) delete [] m_page_index;
        if (m_block_index != 0) delete [] m_block_index;
    }
    
    ParagraphInformation& ParagraphInformation::operator=(const ParagraphInformation &other)
    {
        if (this != &other)
        {
            if (m_page_index != 0) delete [] m_page_index;
            if (m_block_index != 0) delete [] m_block_index;
            
            if (other.m_number_of_blocks > 0)
            {
                m_page_index = new unsigned int[other.m_number_of_blocks];
                m_block_index = new unsigned int[other.m_number_of_blocks];
                for (unsigned int i = 0; i < other.m_number_of_blocks; ++i)
                {
                    m_page_index[i] = other.m_page_index[i];
                    m_block_index[i] = other.m_block_index[i];
                }
            }
            else
            {
                m_page_index = 0;
                m_block_index = 0;
                m_number_of_blocks = 0;
            }
            m_transcription = other.m_transcription;
        }
        
        return *this;
    }
    
    void ParagraphInformation::set(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks, const std::string &transcription)
    {
        if (m_page_index != 0) delete [] m_page_index;
        if (m_block_index != 0) delete [] m_block_index;
        
        if (number_of_blocks > 0)
        {
            m_page_index = new unsigned int[number_of_blocks];
            m_block_index = new unsigned int[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
            for (unsigned int i = 0; i < number_of_blocks; ++i)
            {
                m_page_index[i] = pages[i];
                m_block_index[i] = blocks[i];
            }
        }
        else
        {
            m_page_index = 0;
            m_block_index = 0;
            m_number_of_blocks = 0;
        }
        m_transcription = transcription;
    }
    
    void ParagraphInformation::set(unsigned int number_of_blocks, std::string &transcription)
    {
        if (m_page_index != 0) delete [] m_page_index;
        if (m_block_index != 0) delete [] m_block_index;
        
        if (number_of_blocks > 0)
        {
            m_page_index = new unsigned int[number_of_blocks];
            m_block_index = new unsigned int[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
            for (unsigned int i = 0; i < number_of_blocks; ++i)
            {
                m_page_index[i] = 0;
                m_block_index[i] = 0;
            }
        }
        else
        {
            m_page_index = 0;
            m_block_index = 0;
            m_number_of_blocks = 0;
        }
        m_transcription = transcription;
    }
    
    void ParagraphInformation::setIndexes(const unsigned int * pages, const unsigned int * blocks, unsigned int number_of_blocks)
    {
        if (m_page_index != 0) delete [] m_page_index;
        if (m_block_index != 0) delete [] m_block_index;
        
        if (number_of_blocks > 0)
        {
            m_page_index = new unsigned int[number_of_blocks];
            m_block_index = new unsigned int[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
            for (unsigned int i = 0; i < number_of_blocks; ++i)
            {
                m_page_index[i] = pages[i];
                m_block_index[i] = blocks[i];
            }
        }
        else
        {
            m_page_index = 0;
            m_block_index = 0;
            m_number_of_blocks = 0;
        }
    }
    
    void ParagraphInformation::setNumberOfBlocks(unsigned int number_of_blocks)
    {
        if (m_page_index != 0) delete [] m_page_index;
        if (m_block_index != 0) delete [] m_block_index;
        
        if (number_of_blocks > 0)
        {
            m_page_index = new unsigned int[number_of_blocks];
            m_block_index = new unsigned int[number_of_blocks];
            m_number_of_blocks = number_of_blocks;
            for (unsigned int i = 0; i < number_of_blocks; ++i)
            {
                m_page_index[i] = 0;
                m_block_index[i] = 0;
            }
        }
        else
        {
            m_page_index = 0;
            m_block_index = 0;
            m_number_of_blocks = 0;
        }
    }
    
    void ParagraphInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Paragraph_Information");
        parser.setAttribute("Number_Of_Blocks", m_number_of_blocks);
        parser.addChildren();
        saveVector(parser, "Page", ConstantSubVectorDense<unsigned int, unsigned int>(m_page_index, m_number_of_blocks));
        saveVector(parser, "Block", ConstantSubVectorDense<unsigned int, unsigned int>(m_block_index, m_number_of_blocks));
        saveVector(parser, "Transcription", ConstantSubVectorDense<char, unsigned int>(m_transcription.c_str(), (unsigned int)m_transcription.size()));
        parser.closeTag();
    }
    
    void ParagraphInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Paragraph_Information"))
        {
            if (m_page_index != 0) delete [] m_page_index;
            if (m_block_index != 0) delete [] m_block_index;
            
            m_number_of_blocks = parser.getAttribute("Number_Of_Blocks");
            if (m_number_of_blocks > 0)
            {
                m_page_index = new unsigned int[m_number_of_blocks];
                m_block_index = new unsigned int[m_number_of_blocks];
            }
            else
            {
                m_page_index = 0;
                m_block_index = 0;
            }
            
            while (!(parser.isTagIdentifier("Paragraph_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Page"))
                {
                    VectorDense<unsigned int, unsigned int> data;
                    
                    loadVector(parser, "Page", data);
                    for (unsigned int i = 0; i < m_number_of_blocks; ++i)
                        m_page_index[i] = data[i];
                }
                else if (parser.isTagIdentifier("Block"))
                {
                    VectorDense<unsigned int, unsigned int> data;
                    
                    loadVector(parser, "Block", data);
                    for (unsigned int i = 0; i < m_number_of_blocks; ++i)
                        m_block_index[i] = data[i];
                }
                else if (parser.isTagIdentifier("Transcription"))
                {
                    VectorDense<char, unsigned int> data;
                    
                    loadVector(parser, "Transcription", data);
                    m_transcription = data.getData();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT DATABASE CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    SymbolAccessInformation& SymbolAccessInformation::operator=(const SymbolAccessInformation &other)
    {
        if (this != &other)
        {
            m_page = other.m_page;
            m_block = other.m_block;
            m_line = other.m_line;
            m_position = other.m_position;
        }
        
        return *this;
    }
    
    void SymbolAccessInformation::set(unsigned int page, unsigned int block, unsigned int line, unsigned int position)
    {
        m_page = page;
        m_block = block;
        m_line = line;
        m_position = position;
    }
    
    void SymbolAccessInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Symbol_Access_Information");
        parser.setAttribute("Page", m_page);
        parser.setAttribute("Block", m_block);
        parser.setAttribute("Line", m_line);
        parser.setAttribute("Position", m_position);
        parser.closeTag();
    }
    
    void SymbolAccessInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Symbol_Access_Information"))
        {
            m_page = parser.getAttribute("Page");
            m_block = parser.getAttribute("Block");
            m_line = parser.getAttribute("Line");
            m_position = parser.getAttribute("Position");
            
            while (!(parser.isTagIdentifier("Symbol_Access_Information") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    DocumentDatabase::DocumentDatabase(void) :
        m_symbols(0),
        m_symbol_access(0),
        m_number_of_symbols(0),
        m_pages(0),
        m_number_of_pages(0),
        m_paragraphs(0),
        m_number_of_paragraphs(0)
    {
    }
    
    DocumentDatabase::DocumentDatabase(const SymbolInformation * symbols, unsigned int number_of_symbols, const PageInformation * pages, unsigned int number_of_pages, const ParagraphInformation * paragraphs, unsigned int number_of_paragraphs) :
        m_symbols((number_of_symbols > 0)?new SymbolInformation[number_of_symbols]:0),
        m_symbol_access((number_of_symbols > 0)?new std::list<SymbolAccessInformation>[number_of_symbols]:0),
        m_number_of_symbols(number_of_symbols),
        m_pages((number_of_pages > 0)?new PageInformation[number_of_pages]:0),
        m_number_of_pages(number_of_pages),
        m_paragraphs((number_of_paragraphs > 0)?new ParagraphInformation[number_of_paragraphs]:0),
        m_number_of_paragraphs(number_of_paragraphs)
    {
        std::map<unsigned int, unsigned int> symbol_lut;
        for (unsigned int i = 0; i < number_of_symbols; ++i)
        {
            m_symbols[i] = symbols[i];
            symbol_lut[symbols[i].getSymbolIdentifier()] = i;
            m_symbols[i].setSymbolIdentifier(i);
            m_document_symbols.insert(&m_symbols[i]);
        }
        for (unsigned int i = 0; i < number_of_pages; ++i)
            m_pages[i] = pages[i];
        for (unsigned int i = 0; i < number_of_paragraphs; ++i)
            m_paragraphs[i] = paragraphs[i];
        
        for (unsigned int page = 0; page < number_of_pages; ++page)
        {
            for (unsigned int block = 0; block < m_pages[page].getNumberOfBlocks(); ++block)
            {
                for (unsigned int line = 0; line < m_pages[page][block].getNumberOfLines(); ++line)
                {
                    const unsigned int current_number_of_symbols = m_pages[page][block][line].getNumberOfSymbols();
                    SymbolLocalization * __restrict__ symbols_ptr = m_pages[page][block][line].getSymbols();
                    
                    for (unsigned int symbol = 0; symbol < current_number_of_symbols; ++symbol)
                    {
                        symbols_ptr[symbol].setSymbolIdentifier(symbol_lut[symbols_ptr[symbol].getSymbolIdentifier()]);
                        m_symbol_access[symbols_ptr[symbol].getSymbolIdentifier()].push_back(SymbolAccessInformation(page, block, line, symbol));
                    }
                }
            }
        }
    }
    
    DocumentDatabase::DocumentDatabase(const char * filename) :
        m_symbols(0),
        m_symbol_access(0),
        m_number_of_symbols(0),
        m_pages(0),
        m_number_of_pages(0),
        m_paragraphs(0),
        m_number_of_paragraphs(0)
    {
        char database_route[4096];
        std::ifstream file(filename);
        XmlParser parser(&file);
        
        if (!file.is_open())
            throw Exception("Cannot open document database '%s'", filename);
        convertFromXML(parser);
        getFilenameRoute(filename, database_route);
        setRoute(database_route);
        file.close();
    }
    
    DocumentDatabase::DocumentDatabase(const DocumentDatabase &other) :
        m_symbols((other.m_number_of_symbols > 0)?new SymbolInformation[other.m_number_of_symbols]:0),
        m_symbol_access((other.m_number_of_symbols > 0)?new std::list<SymbolAccessInformation>[other.m_number_of_symbols]:0),
        m_number_of_symbols(other.m_number_of_symbols),
        m_pages((other.m_number_of_pages > 0)?new PageInformation[other.m_number_of_pages]:0),
        m_number_of_pages(other.m_number_of_pages),
        m_paragraphs((other.m_number_of_paragraphs > 0)?new ParagraphInformation[other.m_number_of_paragraphs]:0),
        m_number_of_paragraphs(other.m_number_of_paragraphs)
    {
        for (unsigned int i = 0; i < other.m_number_of_symbols; ++i)
        {
            m_symbols[i] = other.m_symbols[i];
            m_symbol_access[i] = other.m_symbol_access[i];
            m_document_symbols.insert(&m_symbols[i]);
        }
        for (unsigned int i = 0; i < other.m_number_of_pages; ++i)
            m_pages[i] = other.m_pages[i];
        for (unsigned int i = 0; i < other.m_number_of_paragraphs; ++i)
            m_paragraphs[i] = other.m_paragraphs[i];
    }
    
    DocumentDatabase::~DocumentDatabase(void)
    {
        if (m_symbols != 0) delete [] m_symbols;
        if (m_symbol_access != 0) delete [] m_symbol_access;
        if (m_pages != 0) delete [] m_pages;
        if (m_paragraphs != 0) delete [] m_paragraphs;
    }
    
    DocumentDatabase& DocumentDatabase::operator=(const DocumentDatabase &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_symbols != 0) delete [] m_symbols;
            if (m_symbol_access != 0) delete [] m_symbol_access;
            if (m_pages != 0) delete [] m_pages;
            if (m_paragraphs != 0) delete [] m_paragraphs;
            m_document_symbols.clear();
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            if (other.m_number_of_symbols > 0)
            {
                m_symbols = new SymbolInformation[other.m_number_of_symbols];
                m_symbol_access = new std::list<SymbolAccessInformation>[other.m_number_of_symbols];
                m_number_of_symbols = other.m_number_of_symbols;
                
                for (unsigned int i = 0; i < other.m_number_of_symbols; ++i)
                {
                    m_symbols[i] = other.m_symbols[i];
                    m_document_symbols.insert(&m_symbols[i]);
                    m_symbol_access[i] = other.m_symbol_access[i];
                }
            }
            else
            {
                m_symbols = 0;
                m_symbol_access = 0;
                m_number_of_symbols = 0;
            }
            
            if (other.m_number_of_pages > 0)
            {
                m_pages = new PageInformation[other.m_number_of_pages];
                m_number_of_pages = other.m_number_of_pages;
                for (unsigned int i = 0; i < other.m_number_of_pages; ++i)
                    m_pages[i] = other.m_pages[i];
            }
            else
            {
                m_pages = 0;
                m_number_of_pages = 0;
            }
            
            if (other.m_number_of_paragraphs > 0)
            {
                m_paragraphs = new ParagraphInformation[other.m_number_of_paragraphs];
                m_number_of_paragraphs = other.m_number_of_paragraphs;
                for (unsigned int i = 0; i < other.m_number_of_paragraphs; ++i)
                    m_paragraphs[i] = other.m_paragraphs[i];
            }
            else
            {
                m_paragraphs = 0;
                m_number_of_paragraphs = 0;
            }
        }
        
        return *this;
    }
    
    void DocumentDatabase::create(const SymbolInformation * symbols, unsigned int number_of_symbols, const PageInformation * pages, unsigned int number_of_pages, const ParagraphInformation * paragraphs, unsigned int number_of_paragraphs)
    {
        std::map<unsigned int, unsigned int> symbol_lut;
        
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_symbols != 0) delete [] m_symbols;
        if (m_symbol_access != 0) delete [] m_symbol_access;
        if (m_pages != 0) delete [] m_pages;
        if (m_paragraphs != 0) delete [] m_paragraphs;
        m_document_symbols.clear();
        
        // -[ Create ]-------------------------------------------------------------------------------------------------------------------------------
        if (number_of_symbols > 0)
        {
            m_symbols = new SymbolInformation[number_of_symbols];
            m_symbol_access = new std::list<SymbolAccessInformation>[number_of_symbols];
            m_number_of_symbols = number_of_symbols;
            
            for (unsigned int i = 0; i < number_of_symbols; ++i)
            {
                m_symbols[i] = symbols[i];
                symbol_lut[symbols[i].getSymbolIdentifier()] = i;
                m_symbols[i].setSymbolIdentifier(i);
                m_document_symbols.insert(&m_symbols[i]);
            }
        }
        else
        {
            m_symbols = 0;
            m_symbol_access = 0;
            m_number_of_symbols = 0;
        }
        if (number_of_pages > 0)
        {
            m_pages = new PageInformation[number_of_pages];
            m_number_of_pages = number_of_pages;
            for (unsigned int i = 0; i < number_of_pages; ++i)
                m_pages[i] = pages[i];
        }
        else
        {
            m_pages = 0;
            m_number_of_pages = 0;
        }
        
        if (number_of_paragraphs > 0)
        {
            m_paragraphs = new ParagraphInformation[number_of_paragraphs];
            m_number_of_paragraphs = number_of_paragraphs;
            for (unsigned int i = 0; i < number_of_paragraphs; ++i)
                m_paragraphs[i] = paragraphs[i];
        }
        else
        {
            m_paragraphs = 0;
            m_number_of_paragraphs = 0;
        }
        
        for (unsigned int page = 0; page < number_of_pages; ++page)
        {
            for (unsigned int block = 0; block < m_pages[page].getNumberOfBlocks(); ++block)
            {
                for (unsigned int line = 0; line < m_pages[page][block].getNumberOfLines(); ++line)
                {
                    const unsigned int current_number_of_symbols = m_pages[page][block][line].getNumberOfSymbols();
                    SymbolLocalization * __restrict__ symbols_ptr = m_pages[page][block][line].getSymbols();
                    
                    for (unsigned int symbol = 0; symbol < current_number_of_symbols; ++symbol)
                    {
                        symbols_ptr[symbol].setSymbolIdentifier(symbol_lut[symbols_ptr[symbol].getSymbolIdentifier()]);
                        m_symbol_access[symbols_ptr[symbol].getSymbolIdentifier()].push_back(SymbolAccessInformation(page, block, line, symbol));
                    }
                }
            }
        }
    }
    
    void DocumentDatabase::setRoute(const char * route)
    {
        for (unsigned int i = 0; i < m_number_of_pages; ++i)
            m_pages[i].setRoute(route);
    }
    
    void DocumentDatabase::load(const char * filename)
    {
        char database_route[4096];
        std::ifstream file(filename);
        XmlParser parser(&file);
        
        if (!file.is_open())
            throw Exception("Cannot open document database '%s'", filename);
        convertFromXML(parser);
        getFilenameRoute(filename, database_route);
        setRoute(database_route);
        file.close();
    }
    
    void DocumentDatabase::save(const char * filename) const
    {
        std::ofstream file(filename);
        XmlParser parser(&file);
        if (!file.is_open())
            throw Exception("Document database file '%s' cannot be stored.", filename);
        convertToXML(parser);
        file.close();
    }
    
    void DocumentDatabase::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Document_Database");
        parser.setAttribute("Number_Of_Symbols", m_number_of_symbols);
        parser.setAttribute("Number_Of_Pages", m_number_of_pages);
        parser.setAttribute("Number_Of_Paragraphs", m_number_of_paragraphs);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_symbols; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_symbols[i].convertToXML(parser);
        }
        for (unsigned int i = 0; i < m_number_of_pages; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_pages[i].convertToXML(parser);
        }
        for (unsigned int i = 0; i < m_number_of_paragraphs; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_paragraphs[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void DocumentDatabase::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Document_Database"))
        {
            std::map<unsigned int, unsigned int> symbol_lut;
            
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_symbols != 0) delete [] m_symbols;
            if (m_symbol_access != 0) delete [] m_symbol_access;
            if (m_pages != 0) delete [] m_pages;
            if (m_paragraphs != 0) delete [] m_paragraphs;
            m_document_symbols.clear();
            
            // -[ Retrieve attributes ]--------------------------------------------------------------------------------------------------------------
            m_number_of_symbols = parser.getAttribute("Number_Of_Symbols");
            m_number_of_pages = parser.getAttribute("Number_Of_Pages");
            m_number_of_paragraphs = parser.getAttribute("Number_Of_Paragraphs");
            
            if (m_number_of_symbols > 0)
            {
                m_symbols = new SymbolInformation[m_number_of_symbols];
                m_symbol_access = new std::list<SymbolAccessInformation>[m_number_of_symbols];
            }
            else
            {
                m_symbols = 0;
                m_symbol_access = 0;
            }
            if (m_number_of_pages > 0) m_pages = new PageInformation[m_number_of_pages];
            else m_pages = 0;
            if (m_number_of_paragraphs > 0) m_paragraphs = new ParagraphInformation[m_number_of_paragraphs];
            else m_paragraphs = 0;
            
            while (!(parser.isTagIdentifier("Document_Database") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Symbol_Information"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_symbols[index].convertFromXML(parser);
                    symbol_lut[m_symbols[index].getSymbolIdentifier()] = index;
                    m_symbols[index].setSymbolIdentifier(index);
                    m_document_symbols.insert(&m_symbols[index]);
                }
                else if (parser.isTagIdentifier("Page_Information"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_pages[index].convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("Paragraph_Information"))
                {
                    unsigned int index = parser.getAttribute("ID");
                    m_paragraphs[index].convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
            
            for (unsigned int page = 0; page < m_number_of_pages; ++page)
            {
                for (unsigned int block = 0; block < m_pages[page].getNumberOfBlocks(); ++block)
                {
                    for (unsigned int line = 0; line < m_pages[page][block].getNumberOfLines(); ++line)
                    {
                        const unsigned int number_of_symbols = m_pages[page][block][line].getNumberOfSymbols();
                        SymbolLocalization * __restrict__ symbols_ptr = m_pages[page][block][line].getSymbols();
                        
                        for (unsigned int symbol = 0; symbol < number_of_symbols; ++symbol)
                        {
                            symbols_ptr[symbol].setSymbolIdentifier(symbol_lut[symbols_ptr[symbol].getSymbolIdentifier()]);
                            m_symbol_access[symbols_ptr[symbol].getSymbolIdentifier()].push_back(SymbolAccessInformation(page, block, line, symbol));
                        }
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

