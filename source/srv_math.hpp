// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_MATH_HPP_HEADER_FILE__
#define __SRV_MATH_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <limits>
#include <cmath>
#ifdef __cplusplus
    extern "C"
    {
        #include <cblas.h>
    }
#else
    #include <cblas.h>
#endif
#include <omp.h>

namespace srv
{
    //                   +--------------------------------------+
    //                   | CONSTANT DEFINITIONS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    const double c_e        = 2.718281828459045235360287471352662498;  // e
    const double c_log2e    = 1.442695040888963407359924681001892137;  // log_2 (e)
    const double c_log10e   = 0.434294481903251827651128918916605082;  // log_10 (e)
    const double c_loge2    = 0.693147180559945309417232121458176568;  // log_e (2);
    const double c_loge10   = 2.302585092994045684017991454684364208;  // log_e (10)
    const double c_pi       = 3.141592653589793238462643383279502884;  // pi
    const double c_pi_2     = 1.570796326794896619231321691639751442;  // pi / 2
    const double c_pi_4     = 0.785398163397448309615660845819875721;  // pi / 4
    const double c_1_pi     = 0.318309886183790671537767526745028724;  // 1 / pi
    const double c_2_pi     = 0.636619772367581343075535053490057448;  // 2 / pi
    const double c_euler    = 0.577215664901532860606512090082402431;  // Euler constant.
    const double c_sqrt2    = 1.414213562373095048801688724209698079;  // sqrt(2)
    const double c_sqrt1_2  = 0.707106781186547524400844362104849039;  // 1 / sqrt(2)
    const double c_infinite = std::numeric_limits<double>::infinity(); // Infinite
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | MATH FUNCTIONS                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Evaluates a polynomial of degree N: y = C_0 + C_1 * x + C_2 * x^2 + ... + C_N * x^N
     *  \param[in] x point where the polynomial is evaluated.
     *  \param[in] coefficients coefficients of the polynomial.
     *  \param[in] size number of coefficients of the polynomial.
     *  \return returns the value of the polynomial at the evaluated point.
     */
    double polynomyEval(double x, const double * coefficients, unsigned int size);
    
    /** Evaluates a polynomial of degree N at different points.
     *  \param[in] x array with the points where the polynomial is evaluated.
     *  \param[out] y array with the values of the function at the different evaluated points.
     *  \param[in] size number of elements in the points (x) and results (y) arrays.
     *  \param[in] coefficients coefficients of the polynomial.
     *  \param[in] number_of_coefficients number of coefficients of the polynomial.
     *  \param[in] factor scale factor applied to the resulting values before assigning them to the result array (this is can be needed to rescale values for data types which have values different than double or float).
     */
    template <typename TINPUT, typename TOUTPUT>
    inline void polynomyEval(const TINPUT * x, TOUTPUT * y, unsigned int size, const double * coefficients, unsigned int number_of_coefficients, double factor = 1.0)
    {
        for (unsigned int i = 0; i < size; ++i)
            y[i] = (TOUTPUT)(factor * polynomyEval((double)x[i], coefficients, number_of_coefficients));
    }
    
    /** This function implements the psi (digamma) function which returns the logarithmic derivative of the gamma function.
     *  \param[in] x point where the psi function is evaluated.
     *  \return returns the value of the function at the evaluated point.
     */
    double psiFunction(double x);
    
    /** Evaluates the psi (digamma) function at different points.
     *  \param[in] x array with the points where the polynomial is evaluated.
     *  \param[out] y array with the values of the function at the different evaluated points.
     *  \param[in] size number of elements in the points (x) and results (y) arrays.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the psi function of the vector.
     */
    template <typename TINPUT, typename TOUTPUT>
    inline void psiFunction(const TINPUT * x, TOUTPUT * y, unsigned int size, double factor = 1.0, unsigned int number_of_threads = 1)
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
                y[i] = (TOUTPUT)(factor * psiFunction((double)x[i]));
        }
    }
    
    template <class T>
    inline T dotProduct(const T * left, const T * right, unsigned int size, unsigned int number_of_threads)
    {
        T dot = 0;
        #pragma omp parallel num_threads(number_of_threads) reduction(+:dot)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
                dot += left[i] * right[i];
        }
        return dot;
    }
    template <>
    inline double dotProduct(const double * left, const double * right, unsigned int size, unsigned int /*number_of_threads*/)
    {
        return cblas_ddot(size, left, 1, right, 1);
    }
    template <>
    inline float dotProduct(const float * left, const float * right, unsigned int size, unsigned int /*number_of_threads*/)
    {
        return cblas_sdot(size, left, 1, right, 1);
    }
    
    double erfinv(double x);
    double norminv(double p, double mu, double sigma);
    /** This function returns the probability of getting 'k' successes in 'n' trials given a binomial probability distribution:
     *  $$y = \binom{n}{k} p^k (1 - p)^{n - k}$$
     *  \param[in] k number of obtained successes.
     *  \param[in] N number of trials.
     *  \param[in] p probability of success in each trial.
     *  \returns resulting probability.
     */
    double binomialDistribution(unsigned int k, unsigned int N, double p);
    /** This function calculates the cumulative probability function of each number of successes up to 'k' using the given number of trials 'n' and the success probability in each trial 'p':
     *  $$y = \sum_{i = 0}^k \binom{n}{i} p^i (1 - p)^{n - i}$$
     *  \param[in] k number of obtained successes.
     *  \param[in] N number of trials.
     *  \param[in] p probability of success in each trial.
     *  \returns resulting cumulative probability.
     */
    double binomialCDF(unsigned int k, unsigned int N, double p);
    /** This function returns the probability that a given number of events occur in a fixed interval of time and or space if these events happen at a known average rate and independent of the last event:
     *  $$y = \frac{\lambda^x \operatorname{e}^{-\lambda}}{k!}$$
     *  \param[in] x number of events observed.
     *  \param[in] lambda average number of events per interval.
     *  \returns resulting probability
     */
    double poissonDistribution(unsigned int x, double lambda);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

