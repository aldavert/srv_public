// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CALIBRATION_PARAMETERS_HPP_HEADER_FILE__
#define __SRV_CALIBRATION_PARAMETERS_HPP_HEADER_FILE__

// -[ Other header files ]---------------------------------------------
#ifdef __ENABLE_LEVMAR__
    #include <levmar.h>
#endif
// -[ SRV header files ]-----------------------------------------------
#include "../srv_geometry.hpp"
#include "../srv_matrix.hpp"
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_image.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CAMERA PARAMETERS CLASS DECLARATION  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which manages and estimates the intrinsic camera parameters.
    class CameraParameters
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        CameraParameters(void);
        /** Constructor which given the image geometry and a set of calibration pattern correspondences,
         *  it estimates the intrinsic camera parameters.
         *  \param[in] width width of the camera images.
         *  \param[in] height height of the camera images.
         *  \param[in] pattern_points
         *  \param[in] image_points array with the coordinates of the calibration pattern points.
         *  \param[out] extrinsic array with the extrinsic parameters matrices of each calibration pattern.
         *  \param[in] number_of_images number of images used to calibrate the camera.
         *  \param[in] homography_method method used to fit the Homography.
         *  \param[in] fitting_method method used to fit the camera parameters.
         *  \param[in] calibration_options flag with the coefficients estimated while fitting the camera model.
         */
        CameraParameters(unsigned int width, unsigned int height, const Matrix<double> &pattern_points, Matrix<double> * image_points, Matrix<double> * extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method = HOMOGRAPHY_LMS, FITTING_IDENTIFIERS fitting_method = FITTING_DIRECT, int calibration_options = 0);
        /** Constructor which sets the intrinsic, radial and tangential parameters of the camera.
         *  \param[in] width width of the camera images.
         *  \param[in] height height of the camera images.
         *  \param[in] fx focal length coefficient in the X-direction.
         *  \param[in] fy focal length coefficient in the Y-direction.
         *  \param[in] s skew factor coefficient.
         *  \param[in] cx X-coordinate of the camera central point.
         *  \param[in] cy Y-coordinate of the camera central point.
         *  \param[in] k array with the radial and tangential coefficients (first the three radial and then the tangential parameters).
         */
        CameraParameters(unsigned int width, unsigned int height, double fx, double fy, double s, double cx, double cy, const double *k);
        /** Constructor which sets the intrinsic parameters of the camera.
         *  \param[in] width width of the camera images.
         *  \param[in] height height of the camera images.
         *  \param[in] fx focal length coefficient in the X-direction.
         *  \param[in] fy focal length coefficient in the Y-direction.
         *  \param[in] s skew factor coefficient.
         *  \param[in] cx X-coordinate of the camera central point.
         *  \param[in] cy Y-coordinate of the camera central point.
         */
        CameraParameters(unsigned int width, unsigned int height, double fx, double fy, double s, double cx, double cy);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the width of the camera image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Sets the width of the camera image.
        inline void setWidth(unsigned int width) { m_width = width; }
        /// Returns the height of the camera image.
        inline unsigned int getHeight(void) const {return m_height;}
        /// Sets the height of the camera image.
        inline void setHeight(unsigned int height) { m_height = height; }
        /// Returns the focal length coefficient the X-direction.
        inline double getFocalX(void) const { return m_fx; }
        /// Sets the focal length coefficient in the X-direction.
        inline void setFocalX(double fx) { m_fx = fx; }
        /// Returns the focal length coefficient in the Y-direction.
        inline double getFocalY(void) const { return m_fy; }
        /// Sets the focal length coefficient in the Y-direction.
        inline void setFocalY(double fy) { m_fy = fy; }
        /// Returns the skew coefficient.
        inline double getSkew(void) const { return m_s; }
        /// Sets the skew coefficient.
        inline void setSkew(double s) { m_s = s; }
        /// Returns the X-coordinate of the camera central point coefficient.
        inline double getCentralPointX(void) const { return m_cx; }
        /// Sets the X-coordinate of the camera central point coefficient.
        inline void setCentralPointX(double cx) { m_cx = cx; }
        /// Returns the Y-coordinate of the camera central point coefficient.
        inline double getCentralPointY(void) const { return m_cy; }
        /// Sets the Y-coordinate of the camera central point coefficient.
        inline void setCentralPointY(double cy) { m_cy = cy; }
        /// Returns a constant pointer to the array with the five distortion coefficients.
        inline const double * getDistortionParameters(void) const { return m_k; }
        /// Sets the five distortion coefficients of the camera (first the radial distortion, then the tangential distortion).
        inline void setDistortionParameters(const double *k) { m_k[0] = k[0]; m_k[1] = k[1]; m_k[2] = k[2]; m_k[3] = k[3]; m_k[4] = k[4]; }
        
        /// Returns the intrinsic camera parameters matrix.
        inline void getIntrinsic(Matrix<double> &mat) const
        {
            mat.set(3, 3, 0);
            mat(0, 0) = m_fx; mat(0, 1) = m_s; mat(0, 2) = m_cx; mat(1, 1) = m_fy; mat(1, 2) = m_cy; mat(2, 2) = 1.0;
        }
        /// Returns the intrinsic camera parameters matrix.
        inline Matrix<double> getIntrinsic(void) const
        {
            Matrix<double> mat(3, 3, 0);
            mat(0, 0) = m_fx; mat(0, 1) = m_s; mat(0, 2) = m_cx; mat(1, 1) = m_fy; mat(1, 2) = m_cy; mat(2, 2) = 1.0;
            return mat;
        }
        /** Given a set of calibration patterns, this function calculates the intrinsic camera parameters.
         */
        void calibrate(unsigned int width, unsigned int height, const Matrix<double> &pattern_points, Matrix<double> *image_points, Matrix<double> *extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method = HOMOGRAPHY_LMS, FITTING_IDENTIFIERS fitting_method = FITTING_DIRECT, int calibration_options = 0);
        /// Calculates the coordinates of each ideal rectified image pixel using the intrinsic camera parameters.
        void undistortMap(TransformValues &undistort_map) const;
        
        // -[ XML Functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the camera parameters into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the camera parameters from an XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        // -[ Auxiliary calibration functions ]------------------------------------------------------------------------------------------------------
        /** Calculates the intrinsic camera parameters using the Least Means Squares (LMS) algorithm.
         *  \param[in] pattern_points
         *  \param[in] image_points
         *  \param[in] number_of_images
         *  \param[in] homography_method
         *  \param[in] fitting_method
         */
        void LMSCalibration(const Matrix<double> &pattern_points, Matrix<double> * image_points, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method);
        /** Calculates the intrinsic camera parameters without the skew coefficient (which is set to 0)
         *  using the Least Means Squares (LMS) algorithm.
         */
        void LMSCalibrationWithoutSkew(const Matrix<double> &pattern_points, Matrix<double> * image_points, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method);
        /** Calculates the extrinsic parameters of the calibration pattern.
         */
        void LMSCameraExtrinsics(const Matrix<double> &pattern_points, Matrix<double> * image_points, Matrix<double> * extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method);
        /** Calculates the Homography equations for the all intrinsic camera parameters.
         *  \param[in] i first column of the Homography matrix.
         *  \param[in] j second column of the Homography matrix.
         *  \param[in] homography homography matrix.
         *  \param[out] vij resulting parameters product.
         */
        inline void getHomographyEquations(unsigned int i, unsigned int j, const Matrix<double> &homography, Matrix<double> &vij)
        {
            vij(0, 0) = homography(0, i) * homography(0, j);
            vij(1, 0) = homography(0, i) * homography(1, j) + homography(1, i) * homography(0, j);
            vij(2, 0) = homography(1, i) * homography(1, j);
            vij(3, 0) = homography(0, i) * homography(2, j) + homography(2, i) * homography(0, j);
            vij(4, 0) = homography(1, i) * homography(2, j) + homography(2, i) * homography(1, j);
            vij(5, 0) = homography(2, i) * homography(2, j);
        }
        /** Calculates the Homography equations for the intrinsic camera parameters without using
         *  the skew coefficient.
         *  \param[in] i first column of the Homography matrix.
         *  \param[in] j second column of the Homography matrix.
         *  \param[in] homography homography matrix.
         *  \param[out] vij resulting parameters product.
         */
        inline void getHomographyEquationsWS(unsigned int i, unsigned int j, const Matrix<double> &homography, Matrix<double> &vij)
        {
            vij(0, 0) = homography(0, i) * homography(0, j);
            vij(1, 0) = homography(1, i) * homography(1, j);
            vij(2, 0) = homography(0, i) * homography(2, j) + homography(2, i) * homography(0, j);
            vij(3, 0) = homography(1, i) * homography(2, j) + homography(2, i) * homography(1, j);
            vij(4, 0) = homography(2, i) * homography(2, j);
        }
        
        // -[ Auxiliary non-linear minimization functions ]------------------------------------------------------------------------------------------
        /// Auxiliary function which calculates the error for the LEVMAR library.
        static void calibrationError(double * parameters, double * estimated_measurements, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function which calculates the Jacobian for the LEVMAR library.
        static void calibrationJacobian(double * parameters, double * jacobian, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function which recovers the camera parameters from a vector array.
        static unsigned int recoverParameters(const double * parameters, int calibration_options, double &fx, double &fy, double &cx, double &cy, double &s, double k[5]);
        /// Auxiliary function which stores the camera parameters into a vector array.
        static unsigned int createParametersArray(double * &parameters, int calibration_options, double fx, double fy, double cx, double cy, double s, const double k[5], const double * rotation, const double * translation, unsigned int number_of_images);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Image width in pixels.
        unsigned int m_width;
        /// Image height in pixels.
        unsigned int m_height;
        /// Camera's focal length in the 'x' direction.
        double m_fx;
        /// Camera's focal length in the 'y' direction.
        double m_fy;
        /// Skew factor of the camera sensor.
        double m_s;
        /// x coordinate of the camera central point.
        double m_cx;
        /// y coordinate of the camera central point
        double m_cy;
        /// Radial and tangential camera lenses distortions ((k[0], k[1], k[4]) are radial distortions while (k[2], k[3]) are tangential distortions).
        double m_k[5];
        
        /// Structure which stores the pattern and calibration method options used by the static functions of the levmar methods.
        typedef struct
        {
            int calibration_options;
            double *pattern_points_x;
            double *pattern_points_y;
            unsigned int number_of_points;
        } calibration_information;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

