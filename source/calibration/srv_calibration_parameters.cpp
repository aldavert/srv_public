// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_calibration_parameters.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CAMERA PARAMETERS CLASS              |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    CameraParameters::CameraParameters(void) :
        m_width(0),
        m_height(0),
        m_fx(0),
        m_fy(0),
        m_s(0),
        m_cx(0),
        m_cy(0)
    {
        m_k[0] = m_k[1] = m_k[2] = m_k[3] = m_k[4] = 0.0;
    }
    
    CameraParameters::CameraParameters(unsigned int width, unsigned int height, const Matrix<double> &pattern_points, Matrix<double> * image_points, Matrix<double> * extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method, int calibration_options)
    {
        calibrate(width, height, pattern_points, image_points, extrinsic, number_of_images, homography_method, fitting_method, calibration_options);
    }
    
    CameraParameters::CameraParameters(unsigned int width, unsigned int height, double fx, double fy, double s, double cx, double cy, const double *k) :
        m_width(width),
        m_height(height),
        m_fx(fx),
        m_fy(fy),
        m_s(s),
        m_cx(cx),
        m_cy(cy)
    {
        m_k[0] = k[0]; m_k[1] = k[1]; m_k[2] = k[2]; m_k[3] = k[3]; m_k[4] = k[4];
    }
    
    CameraParameters::CameraParameters(unsigned int width, unsigned int height, double fx, double fy, double s, double cx, double cy) :
        m_width(width),
        m_height(height),
        m_fx(fx),
        m_fy(fy),
        m_s(s),
        m_cx(cx),
        m_cy(cy)
    {
        m_k[0] = m_k[1] = m_k[2] = m_k[3] = m_k[4] = 0.0;
    }
    
    void CameraParameters::calibrate(unsigned int width, unsigned int height, const Matrix<double> &pattern_points, Matrix<double> *image_points, Matrix<double> *extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method, int calibration_options)
    {
        unsigned int number_of_parameters, number_of_measurements, number_of_camera_parameters;
        Matrix<double> * camera_extrinsic;
        double * parameters, * measurements, * rotation, * translation, options[LM_OPTS_SZ], information[LM_INFO_SZ];
        calibration_information info;
        
        m_width = width;
        m_height = height;
        m_k[0] = m_k[1] = m_k[2] = m_k[3] = m_k[4] = 0.0;
        camera_extrinsic = new Matrix<double>[number_of_images];
        
        // Calculate the camera intrinsic parameters using LM minimization as a first approximation.
        if ((calibration_options & 0x01) == 0x01) LMSCalibration(pattern_points, image_points, number_of_images, homography_method, fitting_method); // With skew coeficient
        else LMSCalibrationWithoutSkew(pattern_points, image_points, number_of_images, homography_method, fitting_method); // Without skew coefficient (s = 0)
        LMSCameraExtrinsics(pattern_points, image_points, camera_extrinsic, number_of_images, homography_method, fitting_method);
        
        // Generate the paramters information
        rotation = new double[3 * number_of_images];
        translation = new double[3 * number_of_images];
        for (unsigned int i = 0; i < number_of_images; ++i)
        {
            rotation2angles(camera_extrinsic[i], rotation[3 * i], rotation[3 * i + 1], rotation[3 * i + 2]);
            translation[3 * i] = camera_extrinsic[i](0, 3);
            translation[3 * i + 1] = camera_extrinsic[i](1, 3);
            translation[3 * i + 2] = camera_extrinsic[i](2, 3);
        }
        number_of_parameters = createParametersArray(parameters, calibration_options, m_fx, m_fy, m_cx, m_cy, m_s, m_k, rotation, translation, number_of_images);
        delete [] rotation;
        delete [] translation;
        number_of_measurements = pattern_points.getNumberOfColumns() * number_of_images * 2;
        measurements = new double[pattern_points.getNumberOfColumns() * number_of_images * 2];
        info.number_of_points = pattern_points.getNumberOfColumns();
        info.pattern_points_x = new double[pattern_points.getNumberOfColumns()];
        info.pattern_points_y = new double[pattern_points.getNumberOfColumns()];
        info.calibration_options = calibration_options;
        
        for (int index = 0; index < pattern_points.getNumberOfColumns(); ++index)
        {
            info.pattern_points_x[index] = pattern_points(0, index);
            info.pattern_points_y[index] = pattern_points(1, index);
        }
        for (unsigned int i = 0, j = 0; i < number_of_images; ++i)
        {
            for (int k = 0; k < pattern_points.getNumberOfColumns(); ++k, j += 2)
            {
                measurements[j] = image_points[i](0, k);
                measurements[j + 1] = image_points[i](1, k);
            }
        }
        
        options[0] = LM_INIT_MU;
        options[1] = 1e-12;
        options[2] = 1e-12;
        options[3] = 1e-15;
        options[4] = LM_DIFF_DELTA;
        
        dlevmar_der(calibrationError, calibrationJacobian, parameters, measurements, number_of_parameters, number_of_measurements, 1000, options, information, 0, 0, &info);
        number_of_camera_parameters = recoverParameters(parameters, info.calibration_options, m_fx, m_fy, m_cx, m_cy, m_s, m_k);
        for (unsigned int i = number_of_camera_parameters, j = 0; i < (unsigned int)number_of_parameters; i += 6, ++j)
        {
            Matrix<double> R;
            angles2rotation(parameters[i], parameters[i + 1], parameters[i + 2], R);
            R.submatrix(0, 0, 2, 2, camera_extrinsic[j], 0, 0);
            camera_extrinsic[j](0, 3) = parameters[i + 3];
            camera_extrinsic[j](1, 3) = parameters[i + 4];
            camera_extrinsic[j](2, 3) = parameters[i + 5];
        }
        
        if (extrinsic != 0)
            for (unsigned int i = 0; i < number_of_images; ++i)
                extrinsic[i] = camera_extrinsic[i];
        
        delete [] info.pattern_points_x;
        delete [] info.pattern_points_y;
        delete [] camera_extrinsic;
        delete [] measurements;
        delete [] parameters;
    }
    
    void CameraParameters::undistortMap(TransformValues &undistort_map) const
    {
        double * xmap, * ymap, f;
        
        f = (m_fx + m_fy) / 2.0;
        xmap = new double[m_width * m_height];
        ymap = new double[m_width * m_height];
        for (unsigned int y = 0, index = 0; y < m_height; ++y)
        {
            for (unsigned int x = 0; x < m_width; ++x, ++index)
            {
                double rx, ry, nx, ny, r;
                
                // Image plane coordinates in the rectified image.
                rx = ((double)x - (double)m_width / 2.0) / f;
                ry = ((double)y - (double)m_height / 2.0) / f;
                
                // Image plane coordinates in the original image.
                r = rx * rx + ry * ry;
                nx = rx + rx * (m_k[0] * r + m_k[1] * r * r + m_k[4] * r * r * r) + 2 * m_k[2] * rx * ry + m_k[3] * (r + 2 * rx * rx);
                ny = ry + ry * (m_k[0] * r + m_k[1] * r * r + m_k[4] * r * r * r) + m_k[2] * (r + 2 * ry * ry) + 2 * m_k[3] * rx * ry;
                // Pixel coordinates in the original image.
                xmap[index] = nx * m_fx + ny * m_s + m_cx;
                ymap[index] = ny * m_fy + m_cy;
            }
        }
        undistort_map.set(xmap, ymap, m_width, m_height, m_width, m_height);
        delete [] xmap;
        delete [] ymap;
    }
    
    void CameraParameters::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Camera_Parameters");
        parser.setAttribute("Width", m_width);
        parser.setAttribute("Height", m_height);
        parser.setAttribute("Focal_X", m_fx);
        parser.setAttribute("Focal_Y", m_fy);
        parser.setAttribute("Skew", m_s);
        parser.setAttribute("Center_X", m_cx);
        parser.setAttribute("Center_Y", m_cy);
        parser.setAttribute("K0", m_k[0]);
        parser.setAttribute("K1", m_k[1]);
        parser.setAttribute("K2", m_k[2]);
        parser.setAttribute("K3", m_k[3]);
        parser.setAttribute("K4", m_k[4]);
        parser.closeTag();
    }
    
    void CameraParameters::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Camera_Parameters"))
        {
            m_width = parser.getAttribute("Width");
            m_height = parser.getAttribute("Height");
            m_fx = parser.getAttribute("Focal_X");
            m_fy = parser.getAttribute("Focal_Y");
            m_s = parser.getAttribute("Skew");
            m_cx = parser.getAttribute("Center_X");
            m_cy = parser.getAttribute("Center_Y");
            m_k[0] = parser.getAttribute("K0");
            m_k[1] = parser.getAttribute("K1");
            m_k[2] = parser.getAttribute("K2");
            m_k[3] = parser.getAttribute("K3");
            m_k[4] = parser.getAttribute("K4");
            
            while (!(parser.isTagIdentifier("Camera_Parameters") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    void CameraParameters::LMSCalibration(const Matrix<double> &pattern_points, Matrix<double> * image_points, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method)
    {
        Matrix<double> v01(6, 1), v00(6, 1), v11(6, 1), V(2 * number_of_images, 6), u, v;
        HomographyModel<double> homography(homography_method, 0.2);
        VectorDense<bool> inliers;
        double b00, b01, b11, b02, b12, b22, lambda;
        VectorDense<double> s;
        
        for (unsigned int i = 0; i < number_of_images; ++i)
        {
            homography.fit(image_points[i], pattern_points, fitting_method, inliers);
            getHomographyEquations(0, 0, homography.getModel(), v00);
            getHomographyEquations(0, 1, homography.getModel(), v01);
            getHomographyEquations(1, 1, homography.getModel(), v11);
            for (unsigned int column = 0; column < 6; ++column)
            {
                V(2 * i, column) = v01(column, 0);
                V(2 * i + 1, column) = v00(column, 0) - v11(column, 0);
            }
        }
        MatrixSVD(V, u, s, v);
        b00 = v(0, 5);
        b01 = v(1, 5);
        b11 = v(2, 5);
        b02 = v(3, 5);
        b12 = v(4, 5);
        b22 = v(5, 5);
        
        m_cy = (b01 * b02 - b00 * b12) / (b00 * b11 - b01 * b01);
        lambda = b22 - (b02 * b02 + m_cy * (b01 * b02 - b00 * b12)) / b00;
        m_fx = sqrt(lambda / b00);
        m_fy = sqrt((lambda * b00) / (b00 * b11 - b01 * b01));
        m_s = (-b01 * m_fx * m_fx * m_fy) / lambda;
        m_cx = (m_s * m_cy) / m_fy - (b02 * m_fx * m_fx) / lambda;
    }
    
    void CameraParameters::LMSCalibrationWithoutSkew(const Matrix<double> &pattern_points, Matrix<double> * image_points, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method)
    {
        Matrix<double> v01(5, 1), v00(5, 1), v11(5, 1), V(2 * number_of_images, 5), u, v;
        HomographyModel<double> homography(homography_method, 0.2);
        double b00, b11, b02, b12, b22, lambda;
        VectorDense<bool> inliers;
        VectorDense<double> s;
        
        for (unsigned int i = 0; i < number_of_images; ++i)
        {
            homography.fit(image_points[i], pattern_points, fitting_method, inliers);
            getHomographyEquationsWS(0, 0, homography.getModel(), v00);
            getHomographyEquationsWS(0, 1, homography.getModel(), v01);
            getHomographyEquationsWS(1, 1, homography.getModel(), v11);
            for (unsigned int column = 0; column < 5; ++column)
            {
                V(2 * i, column) = v01(column, 0);
                V(2 * i + 1, column) = v00(column, 0) - v11(column, 0);
            }
        }
        MatrixSVD(V, u, s, v);
        b00 = v(0, 4);
        b11 = v(1, 4);
        b02 = v(2, 4);
        b12 = v(3, 4);
        b22 = v(4, 4);
        m_cx = -b02 / b00;
        m_cy = -b12 / b11;
        lambda = 1.0 / (b22 - b00 * m_cx * m_cx - b11 * m_cy * m_cy);
        m_fx = sqrt(1.0 / (lambda * b00));
        m_fy = sqrt(1.0 / (lambda * b11));
        m_s = 0;
    }
    
    void CameraParameters::LMSCameraExtrinsics(const Matrix<double> &pattern_points, Matrix<double> * image_points, Matrix<double> * extrinsic, unsigned int number_of_images, HOMOGRAPHY_ALGORITHMS homography_method, FITTING_IDENTIFIERS fitting_method)
    {
        HomographyModel<double> homography(homography_method, 0.2);
        Matrix<double> K(3, 3), R(3, 3), t(3, 1), u, v;
        VectorDense<bool> inliers;
        VectorDense<double> s;
        
        K(0, 0) = m_fx;
        K(0, 1) = m_s;
        K(0, 2) = m_cx;
        K(1, 0) = 0.0;
        K(1, 1) = m_fy;
        K(1, 2) = m_cy;
        K(2, 0) = 0.0;
        K(2, 1) = 0.0;
        K(2, 2) = 1.0;
        MatrixInverse(K);
        
        for (unsigned int i = 0; i < number_of_images; ++i)
        {
            double lambda, prod[3];
            
            homography.fit(image_points[i], pattern_points, fitting_method, inliers);
            
            // *** lambda = 1.0 / norm(K * homography.col(0), 2);
            MatrixVectorMultiplication(K, homography.getModel()(0), prod);
            lambda = 1.0 / sqrt(prod[0] * prod[0] + prod[1] * prod[1] + prod[2] * prod[2]);
            // *** R.col(0) = lambda * K * homography.col(0);
            prod[0] *= lambda; prod[1] *= lambda; prod[2] *= lambda;
            R.copyColumn(0, prod);
            // *** R.col(1) = lambda * K * homography.col(1);
            MatrixVectorMultiplication(K, homography.getModel()(1), prod);
            prod[0] *= lambda; prod[1] *= lambda; prod[2] *= lambda;
            R.copyColumn(1, prod);
            // *** R.col(2) = cross(R.col(0), R.col(1));
            crossProduct3(R(0), R(1), R(2));
            // *** t = lambda * K * homography.col(2);
            MatrixVectorMultiplication(K, homography.getModel()(2), t(0));
            t *= lambda;
            
            MatrixSVD(R, u, s, v); // To ensure that the rotation matrix is orthonormal.
            // *** R = u * trans(v);
            MatrixMultiplication(u, false, v, true, R);
            // *** extrinsic[i] = join_rows(R, t);
            MatrixJoinColumns(R, t, extrinsic[i]);
        }
    }
    
    void CameraParameters::calibrationError(double * parameters, double * estimated_measurements, int /* number_of_parameters */, int number_of_measurements, void * additional_data)
    {
        double fx, fy, cx, cy, s, k[5], MX, MY, *rotation, *translation;
        unsigned int number_of_camera_parameters;
        calibration_information *info;
        
        // Initialize additional data needed for calibration.
        info = (calibration_information*)additional_data;
        number_of_camera_parameters = recoverParameters(parameters, info->calibration_options, fx, fy, cx, cy, s, k);
        for (unsigned int i = 0, index = 0; i < (unsigned int)number_of_measurements; i += 2)
        {
            double r, ax, ay, px, py, X, Y, Z;
            
            rotation = &parameters[number_of_camera_parameters + (i / (2 * info->number_of_points)) * 6];
            translation = &parameters[number_of_camera_parameters + (i / (2 * info->number_of_points)) * 6 + 3];
            ax = info->pattern_points_x[index];
            ay = info->pattern_points_y[index];
            X = (cos(rotation[1]) * cos(rotation[2])) * ax + (-cos(rotation[0]) * sin(rotation[2]) + cos(rotation[2]) * sin(rotation[0]) * sin(rotation[1])) * ay + translation[0];
            Y = (cos(rotation[1]) * sin(rotation[2])) * ax + (cos(rotation[0]) * cos(rotation[2]) + sin(rotation[0]) * sin(rotation[1]) * sin(rotation[2])) * ay + translation[1];
            Z = (-sin(rotation[1])) * ax + (cos(rotation[1]) * sin(rotation[0])) * ay + translation[2];
            
            MX = X / Z;
            MY = Y / Z;
            index = (index + 1) % info->number_of_points;
            
            // Parameters generated measurements here...
            r = MX * MX + MY * MY;
            px = MX + MX * (k[0] * r + k[1] * r * r + k[4] * r * r * r) + 2 * k[2] * MX * MY + k[3] * (r + 2 * MX * MX);
            py = MY + MY * (k[0] * r + k[1] * r * r + k[4] * r * r * r) + k[2] * (r + 2 * MY * MY) + 2 * k[3] * MX * MY;
            estimated_measurements[i] = px * fx + py * s + cx;
            estimated_measurements[i + 1] = py * fy + cy;
        }
    }
    
    void CameraParameters::calibrationJacobian(double * parameters, double * jacobian, int number_of_parameters, int number_of_measurements, void * additional_data)
    {
        double fx, fy, cx, cy, s, k[5], MX, MY, r0, r1, r2, t0, t1, t2, k0, k1, k2, k3, k4;
        unsigned int number_of_camera_parameters, jacobian_index, rottrans_index;
        calibration_information *info;
        double PJACOBIAN[2][16], rep[320];
        
        // Initialize additional data needed for calibration.
        info = (calibration_information*)additional_data;
        number_of_camera_parameters = recoverParameters(parameters, info->calibration_options, fx, fy, cx, cy, s, k);
        k0 = k[0];
        k1 = k[1];
        k2 = k[2];
        k3 = k[3];
        k4 = k[4];
        for (unsigned int i = 0, j = 0, index = 0; i < (unsigned int)number_of_measurements; i += 2, j += number_of_parameters * 2)
        {
            double X, Y, Z, r;
            rottrans_index = (i / (2 * info->number_of_points)) * 6;
            r0 = parameters[number_of_camera_parameters + rottrans_index];
            r1 = parameters[number_of_camera_parameters + rottrans_index + 1];
            r2 = parameters[number_of_camera_parameters + rottrans_index + 2];
            t0 = parameters[number_of_camera_parameters + rottrans_index + 3];
            t1 = parameters[number_of_camera_parameters + rottrans_index + 4];
            t2 = parameters[number_of_camera_parameters + rottrans_index + 5];
            MX = info->pattern_points_x[index];
            MY = info->pattern_points_y[index];
            index = (index + 1) % info->number_of_points;
            
            // Value 'r' is calculated directly from the point coordinates. When it is left to be calculated by the Jacobians
            // the resulting equation system is incorrect and the levmar function is unable to solve the system.
            X = (cos(r1) * cos(r2)) * MX + (-cos(r0) * sin(r2) + cos(r2) * sin(r0) * sin(r1)) * MY + t0;
            Y = (cos(r1) * sin(r2)) * MX + (cos(r0) * cos(r2) + sin(r0) * sin(r1) * sin(r2)) * MY + t1;
            Z = (-sin(r1)) * MX + (cos(r1) * sin(r0)) * MY + t2;
            
            X = X / Z;
            Y = Y / Z;
            r = X * X + Y * Y;
            
            // Jacobians generated using Python package 'sympy'. The amount of replacements generated by the 'cse' function
            // is larger than a carefully calculate Jacobians...
            // *************************************************************************************************************
            rep[1] = cos(r1);
            rep[2] = cos(r2);
            rep[3] = sin(r0);
            rep[4] = sin(r1);
            rep[5] = MY * rep[1] * rep[3];
            rep[6] = rep[5] + t2;
            rep[7] = MX * rep[4];
            rep[8] = rep[6] - rep[7];
            rep[9] = MX * rep[1] * rep[2];
            rep[10] = rep[2] * rep[3] * rep[4];
            rep[11] = cos(r0);
            rep[12] = sin(r2);
            rep[13] = rep[11] * rep[12];
            rep[14] = rep[10] - rep[13];
            rep[15] = MY * rep[14];
            rep[16] = rep[15] + rep[9] + t0;
            rep[17] = 1 / rep[8];
            rep[18] = rep[17] * rep[17];
            rep[19] = rep[12] * rep[3] * rep[4];
            rep[20] = rep[11] * rep[2];
            rep[21] = rep[19] + rep[20];
            rep[22] = MY * rep[21];
            rep[23] = MX * rep[12]*rep[1];
            rep[24] = rep[22] + rep[23] + t1;
            rep[25] = r * r * r;
            rep[26] = k4 * rep[25];
            rep[27] = k0 * r;
            rep[28] = pow(rep[25], 2.0 / 3.0);
            rep[29] = k1 * rep[28];
            rep[30] = rep[26] + rep[27] + rep[29];
            rep[31] = rep[12] * rep[3];
            rep[32] = rep[20] * rep[4];
            rep[33] = rep[31] + rep[32];
            rep[34] = rep[16] * rep[16];
            rep[35] = pow(rep[18], 3.0 / 2.0);
            rep[36] = rep[13] * rep[4];
            rep[37] = rep[2] * rep[3];
            rep[38] = rep[36] - rep[37];
            rep[39] = rep[24] * rep[24];
            rep[40] = MX * rep[1];
            rep[41] = MY * rep[3] * rep[4];
            rep[42] = rep[40] + rep[41];
            rep[43] = rep[2] * rep[5];
            rep[44] = rep[2] * rep[7];
            rep[45] = rep[43] - rep[44];
            rep[46] = rep[12] * rep[5];
            rep[47] = rep[12] * rep[7];
            rep[48] = rep[46] - rep[47];
            rep[49] = 2 * rep[40];
            rep[50] = 2 * rep[41];
            rep[51] = rep[49] + rep[50];
            rep[52] = -rep[21];
            rep[53] = MY * rep[52];
            rep[54] = rep[53] - rep[23];
            rep[55] = rep[15] + rep[9];
            rep[56] = 2 * rep[9];
            rep[57] = 2 * rep[15];
            rep[58] = 2 * t1;
            rep[59] = 2 * rep[23];
            rep[60] = 2 * rep[22];
            rep[61] = rep[58] + rep[59] + rep[60];
            rep[62] = rep[17] * rep[30];
            rep[63] = 2 * rep[18] * rep[39];
            rep[64] = r + rep[63];
            rep[65] = k2 * rep[64];
            rep[66] = rep[17] * rep[24];
            rep[67] = 2 * k3 * rep[16] * rep[18] * rep[24];
            rep[68] = rep[24] * rep[62];
            rep[69] = rep[65] + rep[66] + rep[67] + rep[68];
            rep[70] = 2 * MY * k3 * rep[18] * rep[24] * rep[33];
            rep[71] = MY * rep[17] * rep[38];
            rep[72] = 2 * MY * k3 * rep[16] * rep[18] * rep[38];
            rep[73] = -4 * MY * rep[11] * rep[17] * rep[18] * rep[1] * rep[39];
            rep[74] = 4 * MY * rep[18] * rep[24] * rep[38];
            rep[75] = rep[73] + rep[74];
            rep[76] = k2 * rep[75];
            rep[77] = MY * rep[38] * rep[62];
            rep[78] = rep[48] * rep[62];
            rep[79] = rep[18] * rep[24] * rep[42];
            rep[80] = 2 * k3 * rep[16] * rep[18] * rep[51] * rep[66];
            rep[81] = rep[30] * rep[79];
            rep[82] = rep[17] * rep[48];
            rep[83] = 2 * k3 * rep[18] * rep[24] * rep[45];
            rep[84] = rep[17] * rep[51] * rep[63];
            rep[85] = 2 * rep[46];
            rep[86] = 2 * rep[47];
            rep[87] = rep[85] - rep[86];
            rep[88] = rep[18] * rep[61] * rep[87];
            rep[89] = rep[84] + rep[88];
            rep[90] = k2 * rep[89];
            rep[91] = 2 * k3 * rep[18] * rep[24] * rep[54];
            rep[92] = rep[17] * rep[55];
            rep[93] = rep[56] + rep[57];
            rep[94] = 2 * k2 * rep[18] * rep[24] * rep[93];
            rep[95] = rep[55] * rep[62];
            rep[96] = 2 * k3 * rep[16] * rep[18] * rep[55];
            rep[97] = rep[91] + rep[92] + rep[94] + rep[95] + rep[96];
            rep[98] = 2 * k2 * rep[18] * rep[61];
            rep[99] = 2 * k3 * rep[16] * rep[18];
            rep[100] = rep[17] + rep[62] + rep[98] + rep[99];
            PJACOBIAN[0][0] = k3 * (r + 2 * rep[18] * rep[34]) + rep[16] * rep[17] + rep[16] * rep[62] + 2 * k2 * rep[16] * rep[18] * rep[24];
            PJACOBIAN[0][1] = 0;
            PJACOBIAN[0][2] = rep[69];
            PJACOBIAN[0][3] = 1;
            PJACOBIAN[0][4] = 0;
            PJACOBIAN[0][5] = fx * (k3 * (4 * MY * rep[16] * rep[18] * rep[33] - 4 * MY * rep[11] * rep[17] * rep[18] * rep[1] * rep[34]) + MY * rep[17] * rep[33] + MY * rep[33] * rep[62] - MY * rep[11] * rep[16] * rep[18] * rep[1] + 2 * MY * k2 * rep[16] * rep[18] * rep[38] + 2 * MY * k2 * rep[18] * rep[24] * rep[33] - MY * rep[11] * rep[16] * rep[18] * rep[1] * rep[30] - 4 * MY * k2 * rep[11] * rep[16] * rep[18] * rep[1] * rep[66]) + s * (rep[70] + rep[71] + rep[72] + rep[76] + rep[77] - MY * rep[11] * rep[18] * rep[1] * rep[24] - MY * rep[11] * rep[18] * rep[1] * rep[24] * rep[30] - 4 * MY * k3 * rep[11] * rep[16] * rep[18] * rep[1] * rep[66]);
            PJACOBIAN[0][6] = fx * (k3 * (rep[16] * rep[18] * (-4 * rep[44] + 4 * rep[43]) + rep[17] * rep[18] * rep[34] * (4 * rep[40] + 4 * rep[41])) + rep[17] * rep[45] + rep[45] * rep[62] + rep[16] * rep[18] * rep[42] + rep[16] * rep[18] * rep[30] * rep[42] + 2 * k2 * rep[16] * rep[18] * rep[48] + 2 * k2 * rep[18] * rep[24] * rep[45] + 2 * k2 * rep[16] * rep[18] * rep[51] * rep[66]) + s * (rep[78] + rep[79] + rep[80] + rep[81] + rep[82] + rep[83] + rep[90] + rep[48] * rep[99]);
            PJACOBIAN[0][7] = fx * (rep[17] * rep[54] + rep[54] * rep[62] + rep[99] * (-2 * rep[23] + 2 * rep[53]) + 2 * k2 * rep[16] * rep[18] * rep[55] + 2 * k2 * rep[18] * rep[24] * rep[54]) + rep[97] * s;
            PJACOBIAN[0][8] = fx * (rep[17] + rep[62] + 2 * k2 * rep[18] * rep[24] + 2 * k3 * rep[18] * (rep[93] + 2 * t0)) + 2 * k3 * rep[18] * rep[24] * s;
            PJACOBIAN[0][9] = rep[100] * s + 2 * fx * k2 * rep[16] * rep[18];
            PJACOBIAN[0][10] = fx * (-rep[16] * rep[18] - rep[16] * rep[18] * rep[30] - 4 * k2 * rep[16] * rep[18] * rep[66] - 4 * k3 * rep[17] * rep[18] * rep[34]) + s * (rep[18] * (-rep[22] - rep[23] - t1) - rep[18] * rep[24] * rep[30] - 4 * k2 * rep[17] * rep[18] * rep[39] - 4 * k3 * rep[16] * rep[18] * rep[66]);
            PJACOBIAN[0][11] = r * rep[66] * s + fx * r * rep[16] * rep[17];
            PJACOBIAN[0][12] = rep[28] * rep[66] * s + fx * rep[16] * rep[17] * rep[28];
            PJACOBIAN[0][13] = rep[64] * s + 2 * fx * rep[16] * rep[18] * rep[24];
            PJACOBIAN[0][14] = fx * (r + 2 * rep[18] * rep[34]) + 2 * rep[16] * rep[18] * rep[24] * s;
            PJACOBIAN[0][15] = rep[25] * rep[66] * s + fx * rep[16] * rep[17] * rep[25];
            PJACOBIAN[1][0] = 0;
            PJACOBIAN[1][1] = rep[69];
            PJACOBIAN[1][2] = 0;
            PJACOBIAN[1][3] = 0;
            PJACOBIAN[1][4] = 1;
            PJACOBIAN[1][5] = fy * (rep[70] + rep[71] + rep[72] + rep[76] + rep[77] - MY * rep[11] * rep[18] * rep[1] * rep[24] - MY * rep[11] * rep[18] * rep[1] * rep[24] * rep[30] - 4 * MY * k3 * rep[11] * rep[16] * rep[18] * rep[1] * rep[66]);
            PJACOBIAN[1][6] = fy * (rep[78] + rep[79] + rep[80] + rep[81] + rep[82] + rep[83] + rep[90] + rep[48] * rep[99]);
            PJACOBIAN[1][7] = fy * rep[97];
            PJACOBIAN[1][8] = 2 * fy * k3 * rep[18] * rep[24];
            PJACOBIAN[1][9] = fy * rep[100];
            PJACOBIAN[1][10] = fy * (-rep[18] * rep[24] - rep[18] * rep[24] * rep[30] - 4 * k2 * rep[17] * rep[18] * rep[39] - 4 * k3 * rep[16] * rep[18] * rep[66]);
            PJACOBIAN[1][11] = fy * r * rep[66];
            PJACOBIAN[1][12] = fy * rep[28] * rep[66];
            PJACOBIAN[1][13] = fy * rep[64];
            PJACOBIAN[1][14] = 2 * fy * rep[16] * rep[18] * rep[24];
            PJACOBIAN[1][15] = fy * rep[25] * rep[66];
            // *************************************************************************************************************
            
            // Once the point jacobians have been estimated, first set the jacobian of the current point to zero.
            for (unsigned int m = 0; m < (unsigned int)number_of_parameters * 2; ++m)
                jacobian[m + j] = 0.0;
            
            jacobian_index = j;
            jacobian[jacobian_index++] = PJACOBIAN[0][0]; // Fx
            jacobian[jacobian_index++] = PJACOBIAN[0][1]; // Fy
            jacobian[jacobian_index++] = PJACOBIAN[0][3]; // Cx
            jacobian[jacobian_index++] = PJACOBIAN[0][4]; // Cy
            if ((info->calibration_options & 0x01) == 0x01) jacobian[jacobian_index++] = PJACOBIAN[0][2]; // S
            if (((info->calibration_options & 0x04) == 0x04) || ((info->calibration_options & 0x02) == 0x02))
            {
                jacobian[jacobian_index++] = PJACOBIAN[0][11]; // K0
                jacobian[jacobian_index++] = PJACOBIAN[0][12]; // K1
            }
            if ((info->calibration_options & 0x08) == 0x08)
            {
                jacobian[jacobian_index++] = PJACOBIAN[0][13]; // K2
                jacobian[jacobian_index++] = PJACOBIAN[0][14]; // K3
            }
            if ((info->calibration_options & 0x04) == 0x04)
                jacobian[jacobian_index++] = PJACOBIAN[0][15]; // K4
            
            jacobian_index = j + number_of_camera_parameters + rottrans_index;
            jacobian[jacobian_index++] = PJACOBIAN[0][5]; // Rotation: Rx
            jacobian[jacobian_index++] = PJACOBIAN[0][6]; // Rotation: Ry
            jacobian[jacobian_index++] = PJACOBIAN[0][7]; // Rotation: Rz
            jacobian[jacobian_index++] = PJACOBIAN[0][8]; // Translation: Tx
            jacobian[jacobian_index++] = PJACOBIAN[0][9]; // Translation: Ty
            jacobian[jacobian_index++] = PJACOBIAN[0][10]; // Translation: Tz
            
            jacobian_index = j + number_of_parameters;
            jacobian[jacobian_index++] = PJACOBIAN[1][0]; // Fx
            jacobian[jacobian_index++] = PJACOBIAN[1][1]; // Fy
            jacobian[jacobian_index++] = PJACOBIAN[1][3]; // Cx
            jacobian[jacobian_index++] = PJACOBIAN[1][4]; // Cy
            if ((info->calibration_options & 0x01) == 0x01) jacobian[jacobian_index++] = PJACOBIAN[1][2]; // S
            if (((info->calibration_options & 0x04) == 0x04) || ((info->calibration_options & 0x02) == 0x02))
            {
                jacobian[jacobian_index++] = PJACOBIAN[1][11]; // K0
                jacobian[jacobian_index++] = PJACOBIAN[1][12]; // K1
            }
            if ((info->calibration_options & 0x08) == 0x08)
            {
                jacobian[jacobian_index++] = PJACOBIAN[1][13]; // K2
                jacobian[jacobian_index++] = PJACOBIAN[1][14]; // K3
            }
            if ((info->calibration_options & 0x04) == 0x04)
                jacobian[jacobian_index++] = PJACOBIAN[1][15]; // K4
            
            jacobian_index = j + number_of_parameters + number_of_camera_parameters + rottrans_index;
            jacobian[jacobian_index++] = PJACOBIAN[1][5]; // Rotation: Rx
            jacobian[jacobian_index++] = PJACOBIAN[1][6]; // Rotation: Ry
            jacobian[jacobian_index++] = PJACOBIAN[1][7]; // Rotation: Rz
            jacobian[jacobian_index++] = PJACOBIAN[1][8]; // Translation: Tx
            jacobian[jacobian_index++] = PJACOBIAN[1][9]; // Translation: Ty
            jacobian[jacobian_index++] = PJACOBIAN[1][10]; // Translation: Tz
        }
    }
    
    unsigned int CameraParameters::recoverParameters(const double * parameters, int calibration_options, double &fx, double &fy, double &cx, double &cy, double &s, double k[5])
    {
        unsigned int parameters_index;
        
        // Initialize the camera parameters that have to be estimated.
        fx = parameters[0];
        fy = parameters[1];
        cx = parameters[2];
        cy = parameters[3];
        parameters_index = 4;
        // Has skew coefficient?
        if ((calibration_options & 0x01) == 0x01)
        {
            s = parameters[parameters_index];
            ++parameters_index;
        }
        else s = 0;
        // Two first radial distortion coefficients?
        if (((calibration_options & 0x04) == 0x04) || ((calibration_options & 0x02) == 0x02))
        {
            k[0] = parameters[parameters_index];
            ++parameters_index;
            k[1] = parameters[parameters_index];
            ++parameters_index;
        }
        else k[0] = k[1] = 0.0;
        if ((calibration_options & 0x08) == 0x08)
        {
            k[2] = parameters[parameters_index];
            ++parameters_index;
            k[3] = parameters[parameters_index];
            ++parameters_index;
        }
        else k[2] = k[3] = 0.0;
        if ((calibration_options & 0x04) == 0x04)
        {
            k[4] = parameters[parameters_index];
            ++parameters_index;
        }
        else k[4] = 0;
        
        return parameters_index;
    }
    
    unsigned int CameraParameters::createParametersArray(double * &parameters, int calibration_options, double fx, double fy, double cx, double cy, double s, const double k[5], const double * rotation, const double * translation, unsigned int number_of_images)
    {
        unsigned int parameters_index;
        
        // Count the number of parameters available
        parameters_index = 4;
        if ((calibration_options & 0x01) == 0x01) ++parameters_index;
        if (((calibration_options & 0x04) == 0x04) || ((calibration_options & 0x02) == 0x02)) parameters_index += 2;
        if ((calibration_options & 0x08) == 0x08) parameters_index += 2;
        if ((calibration_options & 0x04) == 0x04) ++parameters_index;
        
        // Create the parameters array.
        parameters = new double[parameters_index + number_of_images * 6];
        parameters[0] = fx;
        parameters[1] = fy;
        parameters[2] = cx;
        parameters[3] = cy;
        parameters_index = 4;
        // Has skew coefficient?
        if ((calibration_options & 0x01) == 0x01)
        {
            parameters[parameters_index] = s;
            ++parameters_index;
        }
        // Two first radial distortion coefficients?
        if (((calibration_options & 0x04) == 0x04) || ((calibration_options & 0x02) == 0x02))
        {
            parameters[parameters_index] = k[0];
            ++parameters_index;
            parameters[parameters_index] = k[1];
            ++parameters_index;
        }
        if ((calibration_options & 0x08) == 0x08)
        {
            parameters[parameters_index] = k[2];
            ++parameters_index;
            parameters[parameters_index] = k[3];
            ++parameters_index;
        }
        if ((calibration_options & 0x04) == 0x04)
        {
            parameters[parameters_index] = k[4];
            ++parameters_index;
        }
        
        for (unsigned int i = 0; i < number_of_images; ++i)
        {
            parameters[parameters_index] = rotation[3 * i];
            parameters[parameters_index + 1] = rotation[3 * i + 1];
            parameters[parameters_index + 2] = rotation[3 * i + 2];
            parameters[parameters_index + 3] = translation[3 * i];
            parameters[parameters_index + 4] = translation[3 * i + 1];
            parameters[parameters_index + 5] = translation[3 * i + 2];
            parameters_index += 6;
        }
        return parameters_index;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

