
#include "srv_calibration_pattern.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CHESSBOARD DETECTION FUNCTION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void directionalNeighbor(const VectorDense<ChessboardCorner> &corners, unsigned int index, double vx, double vy, Matrix<int> &selected_corners, double &minimum_distance, int &selected)
    {
        const unsigned int number_of_corners = selected_corners.getNumberOfRows() * selected_corners.getNumberOfColumns();
        const int * corners_vector = selected_corners();
        VectorDense<bool> unused(corners.size(), true);
        
        for (unsigned int i = 0; i < number_of_corners; ++i)
            if (corners_vector[i] != -1) unused[corners_vector[i]] = false;
        minimum_distance = std::numeric_limits<double>::infinity();
        selected = 0;
        for (unsigned int i = 0; i < corners.size(); ++i)
        {
            if (unused[i])
            {
                double dx, dy, current_distance, ex, ey, distance_edge;
                
                dx = corners[i].getX() - corners[index].getX();
                dy = corners[i].getY() - corners[index].getY();
                
                current_distance = dx * vx + dy * vy;
                ex = dx - current_distance * vx;
                ey = dy - current_distance * vy;
                distance_edge = sqrt(ex * ex + ey * ey);
                if (current_distance < 0) current_distance = std::numeric_limits<double>::infinity();
                current_distance = current_distance + 5 * distance_edge;
                
                if (current_distance < minimum_distance)
                {
                    minimum_distance = current_distance;
                    selected = i;
                }
            }
        }
    }
    
    void initChessboard(const VectorDense<ChessboardCorner> &corners, unsigned int index, Matrix<int> &selected_corners)
    {
        if (corners.size() >= 9)
        {
            double v1x, v1y, v2x, v2y;
            double dist1[2], dist2[6];
            
            // Index and orientation of the central element.
            selected_corners.set(3, 3, -1);
            selected_corners(1, 1) = index;
            v1x = corners[index].getFirstEdgeX();
            v1y = corners[index].getFirstEdgeY();
            v2x = corners[index].getSecondEdgeX();
            v2y = corners[index].getSecondEdgeY();
            
            directionalNeighbor(corners, index,  v1x,  v1y, selected_corners, dist1[0], selected_corners(1, 2));
            directionalNeighbor(corners, index, -v1x, -v1y, selected_corners, dist1[1], selected_corners(1, 0));
            directionalNeighbor(corners, index,  v2x,  v2y, selected_corners, dist2[0], selected_corners(2, 1));
            directionalNeighbor(corners, index, -v2x, -v2y, selected_corners, dist2[1], selected_corners(0, 1));
            
            directionalNeighbor(corners, selected_corners(1, 0), -v2x, -v2y, selected_corners, dist2[2], selected_corners(0, 0));
            directionalNeighbor(corners, selected_corners(1, 0),  v2x,  v2y, selected_corners, dist2[3], selected_corners(2, 0));
            directionalNeighbor(corners, selected_corners(1, 2), -v2x, -v2y, selected_corners, dist2[4], selected_corners(0, 2));
            directionalNeighbor(corners, selected_corners(1, 2),  v2x,  v2y, selected_corners, dist2[5], selected_corners(2, 2));
            
            if (std::isinf(dist1[0]) || std::isinf(dist1[1]) || std::isinf(dist2[0]) || std::isinf(dist2[1]) || std::isinf(dist2[2]) || std::isinf(dist2[3]) || std::isinf(dist2[4]) || std::isinf(dist2[5]))
                selected_corners.set(0, 0);
            else
            {
                double mean_dist1, std_dist1, mean_dist2, std_dist2;
                
                mean_dist1 = (dist1[0] + dist1[1]) / 2.0;
                std_dist1 = sqrt((dist1[0] - mean_dist1) * (dist1[0] - mean_dist1) + (dist1[1] - mean_dist1) * (dist1[1] - mean_dist1));
                mean_dist2 = (dist2[0] + dist2[1] + dist2[2] + dist2[3] + dist2[4] + dist2[5]) / 6.0;
                std_dist2 = 0.0;
                for (unsigned int i = 0; i < 6; ++i)
                    std_dist2 = (dist2[i] - mean_dist2) * (dist2[i] - mean_dist2);
                std_dist2 = sqrt(std_dist2 / 5.0);
                
                if ((std_dist1 / mean_dist1 > 0.3) || (std_dist2 / mean_dist2 > 0.3))
                    selected_corners.set(0, 0);
            }
        }
        else selected_corners.set(0, 0);
    }
    
    double chessboardEnergy(const VectorDense<ChessboardCorner> &corners, Matrix<int> &selected_corners)
    {
        double energy;
        
        energy = 0.0;
        for (int row = 0; row < selected_corners.getNumberOfRows(); ++row)
        {
            for (int column = 0; column < selected_corners.getNumberOfColumns() - 2; ++column)
            {
                double x0, x1, x2, y0, y1, y2, dx0, dy0, dx1, dy1, current_energy;
                
                x0 = corners[selected_corners(row, column)].getX();
                y0 = corners[selected_corners(row, column)].getY();
                x1 = corners[selected_corners(row, column + 1)].getX();
                y1 = corners[selected_corners(row, column + 1)].getY();
                x2 = corners[selected_corners(row, column + 2)].getX();
                y2 = corners[selected_corners(row, column + 2)].getY();
                dx0 = x0 + x2 - 2 * x1;
                dy0 = y0 + y2 - 2 * y1;
                dx1 = x0 - x2;
                dy1 = y0 - y2;
                current_energy = sqrt(dx1 * dx1 + dy1 * dy1);
                if (current_energy == 0) current_energy = std::numeric_limits<double>::infinity();
                else current_energy = sqrt(dx0 * dx0 + dy0 * dy0) / current_energy;
                
                if (current_energy > energy) energy = current_energy;
            }
        }
        for (int column = 0; column < selected_corners.getNumberOfColumns(); ++column)
        {
            for (int row = 0; row < selected_corners.getNumberOfRows() - 2; ++row)
            {
                double x0, x1, x2, y0, y1, y2, dx0, dy0, dx1, dy1, current_energy;
                
                x0 = corners[selected_corners(row, column)].getX();
                y0 = corners[selected_corners(row, column)].getY();
                x1 = corners[selected_corners(row + 1, column)].getX();
                y1 = corners[selected_corners(row + 1, column)].getY();
                x2 = corners[selected_corners(row + 2, column)].getX();
                y2 = corners[selected_corners(row + 2, column)].getY();
                dx0 = x0 + x2 - 2 * x1;
                dy0 = y0 + y2 - 2 * y1;
                dx1 = x0 - x2;
                dy1 = y0 - y2;
                current_energy = sqrt(dx1 * dx1 + dy1 * dy1);
                if (current_energy == 0) current_energy = std::numeric_limits<double>::infinity();
                else current_energy = sqrt(dx0 * dx0 + dy0 * dy0) / current_energy;
                
                if (current_energy > energy) energy = current_energy;
            }
        }
        return (double)(selected_corners.getNumberOfColumns() * selected_corners.getNumberOfRows()) * (energy - 1.0);
    }
    
    void predictCorners(const VectorDense<ChessboardCorner> &corners, const VectorDense<unsigned int> &p1, const VectorDense<unsigned int> &p2, const VectorDense<unsigned int> &p3, VectorDense<Tuple<double, double> > &predicted_corners)
    {
        predicted_corners.set(p1.size());
        for (unsigned int i = 0; i < p1.size(); ++i)
        {
            double v1x, v2x, v1y, v2y, a1, a2, a3, s1, s2, s3;
            
            v1x = corners[p2[i]].getX() - corners[p1[i]].getX();
            v1y = corners[p2[i]].getY() - corners[p1[i]].getY();
            v2x = corners[p3[i]].getX() - corners[p2[i]].getX();
            v2y = corners[p3[i]].getY() - corners[p2[i]].getY();
            a1 = atan2(v1y, v1x);
            a2 = atan2(v2y, v2x);
            a3 = 2 * a2 - a1;
            s1 = sqrt(v1x * v1x + v1y * v1y);
            s2 = sqrt(v2x * v2x + v2y * v2y);
            s3 = 2 * s2 - s1;
            predicted_corners[i].setData(corners[p3[i]].getX() + 0.75 * s3 * cos(a3), corners[p3[i]].getY() + 0.75 * s3 * sin(a3));
        }
    }
    void assignClosestCorner(const VectorDense<ChessboardCorner> &corners, const VectorDense<unsigned int> &candidates, const VectorDense<Tuple<double, double> > &predicted_corners, VectorDense<unsigned int> &selected_indexes)
    {
        if (candidates.size() < predicted_corners.size()) // There are not enough candidate corners to assign to the predicted corners.
        {
            selected_indexes.set(0);
            return;
        }
        else
        {
            Matrix<double> distances(predicted_corners.size(), candidates.size());
            
            selected_indexes.set(predicted_corners.size());
            // Create a matrix with the distances between the predicted corners and all candidates.
            for (unsigned int row = 0; row < predicted_corners.size(); ++row)
            {
                for (unsigned int column = 0; column < candidates.size(); ++column)
                {
                    double dx, dy;
                    
                    dx = corners[candidates[column]].getX() - predicted_corners[row].getFirst();
                    dy = corners[candidates[column]].getY() - predicted_corners[row].getSecond();
                    distances(row, column) = sqrt(dx * dx + dy * dy);
                }
            }
            // Search for the closest assignment for each predicted point.
            for (unsigned int i = 0; i < predicted_corners.size(); ++i)
            {
                unsigned int selected_column, selected_row;
                double minimum_distance;
                
                minimum_distance = distances(0, 0);
                selected_row = selected_column = 0;
                
                for (unsigned int row = 0; row < predicted_corners.size(); ++row)
                {
                    for (unsigned int column = 0; column < candidates.size(); ++column)
                    {
                        if (distances(row, column) < minimum_distance)
                        {
                            minimum_distance = distances(row, column);
                            selected_column = column;
                            selected_row = row;
                        }
                    }
                }
                selected_indexes[selected_row] = candidates[selected_column];
                for (unsigned int row = 0; row < predicted_corners.size(); ++row)
                    distances(row, selected_column) = std::numeric_limits<double>::infinity();
                for (unsigned int column = 0; column < candidates.size(); ++column)
                    distances(selected_row, column) = std::numeric_limits<double>::infinity();
            }
        }
    }
    
    void growChessboard(const VectorDense<ChessboardCorner> &corners, Matrix<int> &original_chessboard, unsigned int border_type, Matrix<int> &chessboard)
    {
        const unsigned int number_of_corners = original_chessboard.getNumberOfRows() * original_chessboard.getNumberOfColumns();
        const int * corners_vector = original_chessboard();
        unsigned int number_of_candidates;
        VectorDense<bool> unused(corners.size(), true);
        VectorDense<unsigned int> candidates;
        
        if (original_chessboard.getNumberOfColumns() == 0) return;
        number_of_candidates = corners.size();
        for (unsigned int i = 0; i < number_of_corners; ++i)
            if (corners_vector[i] != -1) { unused[corners_vector[i]] = false; --number_of_candidates; }
        chessboard = original_chessboard;
        if (number_of_candidates > 0)
        {
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            //////// //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            //////// Figure debug("Debug Grow");
            //////// Draw::Pencil<unsigned char> debug_red_pencil, debug_marker_pencil, debug_cb_pencil;
            //////// Draw::Font debug_font(Draw::SMALL_FONT_7x14);
            //////// debug_red_pencil.setBorderSize(1);
            //////// debug_red_pencil.setBorderColor(255, 0, 0, 255);
            //////// debug_red_pencil.setBackgroundColor(0, 0, 0, 0);
            //////// debug_red_pencil.setAntialiasing(false);
            //////// debug_cb_pencil.setBorderSize(2);
            //////// debug_cb_pencil.setBorderColor(0, 255, 0, 255);
            //////// debug_cb_pencil.setBackgroundColor(0, 0, 0, 0);
            //////// debug_cb_pencil.setAntialiasing(true);
            //////// debug_marker_pencil.setBorderSize(1);
            //////// debug_marker_pencil.setBorderColor(255, 255, 0, 255);
            //////// debug_marker_pencil.setBackgroundColor(0, 0, 0, 0);
            //////// debug_marker_pencil.setAntialiasing(true);
            //////// double min_x, max_x, min_y, max_y;
            //////// int * debug_vector, debug_size;
            //////// min_x = max_x = corners[0].getX();
            //////// min_y = max_y = corners[0].getY();
            //////// for (unsigned int kk = 1; kk < corners.size(); ++kk)
            //////// {
            ////////     if (corners[kk].getX() < min_x) min_x = corners[kk].getX();
            ////////     if (corners[kk].getX() > max_x) max_x = corners[kk].getX();
            ////////     if (corners[kk].getY() < min_y) min_y = corners[kk].getY();
            ////////     if (corners[kk].getY() > max_y) max_y = corners[kk].getY();
            //////// }
            //////// Image<unsigned char> debug_image((unsigned int)(max_x - min_x + 1), (unsigned int)(max_y - min_y + 1), 3);
            //////// Image<unsigned char> debug_show_image((unsigned int)(max_x - min_x + 1), (unsigned int)(max_y - min_y + 1), 3);
            //////// debug_image.setValue(0.0);
            //////// debug_size = original_chessboard.getNumberOfColumns() * original_chessboard.getNumberOfRows();
            //////// debug_vector = original_chessboard();
            //////// for (unsigned int kk = 0; kk < corners.size(); ++kk)
            //////// {
            ////////     int kkx, kky;
            ////////     kkx = (unsigned int)round(corners[kk].getX() - min_x);
            ////////     kky = (unsigned int)round(corners[kk].getY() - min_y);
            ////////     Draw::Rectangle(debug_image, kkx - 3, kky - 3, kkx + 3, kky + 3, debug_red_pencil);
            //////// }
            //////// for (int kk = 0; kk < debug_size; ++kk)
            //////// {
            ////////     int kkx, kky;
            ////////     kkx = (unsigned int)round(corners[debug_vector[kk]].getX() - min_x);
            ////////     kky = (unsigned int)round(corners[debug_vector[kk]].getY() - min_y);
            ////////     Draw::Rectangle(debug_image, kkx - 3, kky - 3, kkx + 3, kky + 3, debug_cb_pencil);
            //////// }
            //////// //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            //////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            VectorDense<unsigned int> selected_indexes, p1, p2, p3;
            VectorDense<Tuple<double, double> > predicted_corners;
            
            candidates.set(number_of_candidates);
            number_of_candidates = 0;
            for (unsigned int i = 0; i < corners.size(); ++i)
            {
                if (unused[i])
                {
                    candidates[number_of_candidates] = i;
                    ++number_of_candidates;
                }
            }
            
            switch (border_type)
            {
            case 0: // Add a new column at the RIGHT of the current chessboard pattern.
                p1.set(original_chessboard.getNumberOfRows());
                p2.set(original_chessboard.getNumberOfRows());
                p3.set(original_chessboard.getNumberOfRows());
                for (int row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                {
                    p1[row] = original_chessboard(row, original_chessboard.getNumberOfColumns() - 3);
                    p2[row] = original_chessboard(row, original_chessboard.getNumberOfColumns() - 2);
                    p3[row] = original_chessboard(row, original_chessboard.getNumberOfColumns() - 1);
                }
                predictCorners(corners, p1, p2, p3, predicted_corners);
                assignClosestCorner(corners, candidates, predicted_corners, selected_indexes);
                if (selected_indexes.size() > 0)
                {
                    chessboard.set(original_chessboard.getNumberOfRows(), original_chessboard.getNumberOfColumns() + 1);
                    for (int row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                    {
                        int column;
                        for (column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                            chessboard(row, column) = original_chessboard(row, column);
                        chessboard(row, column) = selected_indexes[row];
                    }
                }
                break;
            case 1: // Add a new row at the BOTTOM of the current chessboard pattern.
                p1.set(original_chessboard.getNumberOfColumns());
                p2.set(original_chessboard.getNumberOfColumns());
                p3.set(original_chessboard.getNumberOfColumns());
                for (int column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                {
                    p1[column] = original_chessboard(original_chessboard.getNumberOfRows() - 3, column);
                    p2[column] = original_chessboard(original_chessboard.getNumberOfRows() - 2, column);
                    p3[column] = original_chessboard(original_chessboard.getNumberOfRows() - 1, column);
                }
                predictCorners(corners, p1, p2, p3, predicted_corners);
                assignClosestCorner(corners, candidates, predicted_corners, selected_indexes);
                if (selected_indexes.size() > 0)
                {
                    chessboard.set(original_chessboard.getNumberOfRows() + 1, original_chessboard.getNumberOfColumns());
                    for (int column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                    {
                        int row;
                        for (row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                            chessboard(row, column) = original_chessboard(row, column);
                        chessboard(row, column) = selected_indexes[column];
                    }
                }
                break;
            case 2: // Add a new column at the LEFT of the current chessboard pattern.
                p1.set(original_chessboard.getNumberOfRows());
                p2.set(original_chessboard.getNumberOfRows());
                p3.set(original_chessboard.getNumberOfRows());
                for (int row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                {
                    p1[row] = original_chessboard(row, 2);
                    p2[row] = original_chessboard(row, 1);
                    p3[row] = original_chessboard(row, 0);
                }
                predictCorners(corners, p1, p2, p3, predicted_corners);
                assignClosestCorner(corners, candidates, predicted_corners, selected_indexes);
                if (selected_indexes.size() > 0)
                {
                    chessboard.set(original_chessboard.getNumberOfRows(), original_chessboard.getNumberOfColumns() + 1);
                    for (int row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                    {
                        chessboard(row, 0) = selected_indexes[row];
                        for (int column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                            chessboard(row, column + 1) = original_chessboard(row, column);
                    }
                }
                break;
            case 3: // Add a new row at the TOP of the current chessboard pattern.
                p1.set(original_chessboard.getNumberOfColumns());
                p2.set(original_chessboard.getNumberOfColumns());
                p3.set(original_chessboard.getNumberOfColumns());
                for (int column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                {
                    p1[column] = original_chessboard(2, column);
                    p2[column] = original_chessboard(1, column);
                    p3[column] = original_chessboard(0, column);
                }
                predictCorners(corners, p1, p2, p3, predicted_corners);
                assignClosestCorner(corners, candidates, predicted_corners, selected_indexes);
                if (selected_indexes.size() > 0)
                {
                    chessboard.set(original_chessboard.getNumberOfRows() + 1, original_chessboard.getNumberOfColumns());
                    for (int column = 0; column < original_chessboard.getNumberOfColumns(); ++column)
                    {
                        chessboard(0, column) = selected_indexes[column];
                        for (int row = 0; row < original_chessboard.getNumberOfRows(); ++row)
                            chessboard(row + 1, column) = original_chessboard(row, column);
                    }
                }
                break;
            default:
                throw Exception("Incorrect border type with index '%d'.", border_type);
            }
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            /////// //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            /////// debug_show_image = debug_image;
            /////// for (unsigned int kk = 0; kk < selected_indexes.size(); ++kk)
            /////// {
            ///////     int kkx, kky;
            ///////     kkx = (unsigned int)round(corners[selected_indexes[kk]].getX() - min_x);
            ///////     kky = (unsigned int)round(corners[selected_indexes[kk]].getY() - min_y);
            ///////     Draw::Rectangle(debug_show_image, kkx - 4, kky - 4, kkx + 4, kky + 4, debug_marker_pencil);
            ///////     kkx = (unsigned int)(predicted_corners[kk].getFirst() - min_x);
            ///////     kky = (unsigned int)(predicted_corners[kk].getSecond() - min_y);
            ///////     Draw::Circle(debug_show_image, kkx, kky, 4, debug_marker_pencil);
            ///////     Draw::Text(debug_show_image, kkx - 5, kky - 5, debug_cb_pencil, debug_font, "+[%d]", kk);
            ///////     ////debug(debug_show_image);
            ///////     ////Figure::wait();
            /////// }
            /////// for (int row = 0; row < chessboard.getNumberOfRows(); ++row)
            /////// {
            ///////     for (int column = 0; column < chessboard.getNumberOfColumns(); ++column)
            ///////     {
            ///////         int kkx, kky;
            ///////         kkx = (unsigned int)round(corners[chessboard(row, column)].getX() - min_x);
            ///////         kky = (unsigned int)round(corners[chessboard(row, column)].getY() - min_y);
            ///////         Draw::Text(debug_show_image, kkx, kky, debug_marker_pencil, debug_font, "+ (%d,%d)", row, column);
            ///////     }
            /////// }
            /////// debug(debug_show_image);
            /////// std::cout << "Original energy: " << chessboardEnergy(corners, original_chessboard) << "; New chessboard energy: " << chessboardEnergy(corners, chessboard) << std::endl;
            /////// Figure::wait(10);
            /////// //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
            /////// //// ** DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE - DEBUG CODE **
        }
    }
    
    void detectChessboard(const VectorDense<ChessboardCorner> &corners, VectorDense<ChessboardInformation> &chessboards)
    {
        // ---| Magical constants |------------------------------------------------------------------------------------
        const double minimum_chessboard_energy_threshold = -10.0;
        // ------------------------------------------------------------------------------------------------------------
        std::list<Tuple<Matrix<int> *, double> > chessboard_list;
        unsigned int index;
        
        for (unsigned int i = 0; i < corners.size(); ++i)
        {
            Matrix<int> current_chessboard;
            double chessboard_energy;
            
            initChessboard(corners, i, current_chessboard);
            if ((current_chessboard.getNumberOfRows() == 0) || (chessboardEnergy(corners, current_chessboard) > 0.0))
                continue;
            
            while (true)
            {
                Matrix<int> current_proposal, selected_proposal;
                double energy, proposal_energy, minimum_energy;
                
                energy = chessboardEnergy(corners, current_chessboard);
                growChessboard(corners, current_chessboard, 0, selected_proposal);
                minimum_energy = chessboardEnergy(corners, selected_proposal);
                for (unsigned int g = 1; g < 4; ++g)
                {
                    growChessboard(corners, current_chessboard, g, current_proposal);
                    proposal_energy = chessboardEnergy(corners, current_proposal);
                    
                    if (proposal_energy < minimum_energy)
                    {
                        minimum_energy = proposal_energy;
                        selected_proposal = current_proposal;
                    }
                }
                
                if (minimum_energy < energy)
                    current_chessboard = selected_proposal;
                else break; // When no best model is found, exit the loop.
            }
            
            if ((chessboard_energy = chessboardEnergy(corners, current_chessboard)) < minimum_chessboard_energy_threshold)
            {
                VectorDense<bool> used_corners(corners.size(), false);
                VectorDense<bool> overlap((unsigned int)chessboard_list.size(), false);
                int number_of_elements, * list_vector;
                bool no_overlap, chessboard_is_better;
                
                number_of_elements = current_chessboard.getNumberOfRows() * current_chessboard.getNumberOfColumns();
                list_vector = current_chessboard();
                for (int k = 0; k < number_of_elements; ++k)
                    used_corners[list_vector[k]] = true;
                
                index = 0;
                no_overlap = true;
                chessboard_is_better = false;
                for (std::list<Tuple<Matrix<int> *, double> >::iterator begin = chessboard_list.begin(), end = chessboard_list.end(); begin != end; ++begin, ++index)
                {
                    number_of_elements = begin->getFirst()->getNumberOfRows() * begin->getFirst()->getNumberOfColumns();
                    list_vector = (*(begin->getFirst()))();
                    for (int k = 0; k < number_of_elements; ++k)
                    {
                        if (used_corners[list_vector[k]])
                        {
                            no_overlap = false;
                            chessboard_is_better = chessboard_is_better || (chessboard_energy < begin->getSecond());
                            overlap[index] = true;
                            break;
                        }
                    }
                }
                
                if (no_overlap || chessboard_is_better)
                {
                    if (chessboard_is_better)
                    {
                        index = 0;
                        for (std::list<Tuple<Matrix<int> *, double> >::iterator begin = chessboard_list.begin(), end = chessboard_list.end(); begin != end; ++index)
                        {
                            if (overlap[index])
                            {
                                delete begin->getFirst();
                                begin = chessboard_list.erase(begin);
                            }
                            else ++begin;
                        }
                    }
                    chessboard_list.push_back(Tuple<Matrix<int> *, double>(new Matrix<int>(current_chessboard), chessboard_energy));
                }
            }
        }
        
        chessboards.set((unsigned int)chessboard_list.size());
        index = 0;
        for (std::list<Tuple<Matrix<int> *, double> >::iterator begin = chessboard_list.begin(), end = chessboard_list.end(); begin != end; ++begin, ++index)
        {
            const int number_of_elements = begin->getFirst()->getNumberOfRows() * begin->getFirst()->getNumberOfColumns();
            const int * vector_matrix = (*(begin->getFirst()))();
            VectorDense<ChessboardCorner> current_corners(number_of_elements);
            for (int k = 0; k < number_of_elements; ++k)
                current_corners[k] = corners[vector_matrix[k]];
            chessboards[index].setCorners(current_corners, begin->getFirst()->getNumberOfRows(), begin->getFirst()->getNumberOfColumns());
            delete begin->getFirst();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

