// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CALIBRATION_PATTERN_HEADER_FILE__
#define __SRV_CALIBRATION_PATTERN_HEADER_FILE__

#include "../srv_image.hpp"
#include "../srv_matrix.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CHESSBOARD CORNER INFORMATION CLASS  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which stores the information of the location of the chessboard corners.
    class ChessboardCorner
    {
    public:
        /// Default constructor.
        ChessboardCorner(void) : m_x(-1.0), m_y(-1.0), m_first_edge_x(0.0), m_first_edge_y(0.0), m_second_edge_x(0.0), m_second_edge_y(0.0) {}
        /** Constructor which initializes the parameters of the chessboard corner.
         *  \param[in] x X-coordinate of the chessboard corner.
         *  \param[in] y Y-coordinate of the chessboard corner.
         *  \param[in] first_edge_x X-coordinate of the first edge vector of the corner.
         *  \param[in] first_edge_y Y-coordinate of the first edge vector of the corner.
         *  \param[in] second_edge_x X-coordinate of the second edge vector of the corner.
         *  \param[in] second_edge_y Y-coordinate of the second edge vector of the corner.
         */
        ChessboardCorner(double x, double y, double first_edge_x, double first_edge_y, double second_edge_x, double second_edge_y) :
            m_x(x), m_y(y), m_first_edge_x(first_edge_x), m_first_edge_y(first_edge_y), m_second_edge_x(second_edge_x), m_second_edge_y(second_edge_y) {}
        /** Function which sets the parameters of the chessboard corner.
         *  \param[in] x X-coordinate of the chessboard corner.
         *  \param[in] y Y-coordinate of the chessboard corner.
         *  \param[in] first_edge_x X-coordinate of the first edge vector of the corner.
         *  \param[in] first_edge_y Y-coordinate of the first edge vector of the corner.
         *  \param[in] second_edge_x X-coordinate of the second edge vector of the corner.
         *  \param[in] second_edge_y Y-coordinate of the second edge vector of the corner.
         */
        inline void set(double x, double y, double first_edge_x, double first_edge_y, double second_edge_x, double second_edge_y)
        {
            m_x = x; m_y = y;
            m_first_edge_x = first_edge_x; m_first_edge_y = first_edge_y;
            m_second_edge_x = second_edge_x; m_second_edge_y = second_edge_y;
        }
        /** Function which sets coordinates of the chessboard corner.
         *  \param[in] x X-coordinate of the chessboard corner.
         *  \param[in] y Y-coordinate of the chessboard corner.
         */
        inline void setCoordinates(double x, double y) { m_x = x; m_y = y; }
        /** Function which sets the first edge vector of the chessboard corner.
         *  \param[in] first_edge_x X-coordinate of the first edge vector of the corner.
         *  \param[in] first_edge_y Y-coordinate of the first edge vector of the corner.
         */
        inline void setFirstEdge(double first_edge_x, double first_edge_y) { m_first_edge_x = first_edge_x; m_first_edge_y = first_edge_y; }
        /** Function which sets the second edge vector of the chessboard corner.
         *  \param[in] second_edge_x X-coordinate of the second edge vector of the corner.
         *  \param[in] second_edge_y Y-coordinate of the second edge vector of the corner.
         */
        inline void setSecondEdge(double second_edge_x, double second_edge_y) { m_second_edge_x = second_edge_x; m_second_edge_y = second_edge_y; }
        
        /// Returns the X-coordinate of the chessboard corner.
        inline double getX(void) const { return m_x; }
        /// Sets the X-coordinate of the chessboard corner.
        inline void setX(double x) { m_x = x; }
        /// Returns the Y-coordinate of the chessboard corner.
        inline double getY(void) const { return m_y; }
        /// Sets the Y-coordinate of the chessboard corner.
        inline void setY(double y) { m_y = y; }
        /// Returns the X-coordinate of the first edge vector of the corner.
        inline double getFirstEdgeX(void) const { return m_first_edge_x; }
        /// Sets the X-coordinate of the first edge vector of the corner.
        inline void setFirstEdgeX(double first_edge_x) { m_first_edge_x = first_edge_x; }
        /// Returns the Y-coordinate of the first edge vector of the corner.
        inline double getFirstEdgeY(void) const { return m_first_edge_y; }
        /// Sets the Y-coordinate of the first edge vector of the corner.
        inline void setFirstEdgeY(double first_edge_y) { m_first_edge_y = first_edge_y; }
        /// Returns the X-coordinate of the second edge vector of the corner.
        inline double getSecondEdgeX(void) const { return m_second_edge_x; }
        /// Sets the X-coordinate of the second edge vector of the corner.
        inline void setSecondEdgeX(double second_edge_x) { m_second_edge_x = second_edge_x; }
        /// Returns the Y-coordinate of the second edge vector of the corner.
        inline double getSecondEdgeY(void) const { return m_second_edge_y; }
        /// Sets the Y-coordinate of the second edge vector of the corner.
        inline void setSecondEdgeY(double second_edge_y) { m_second_edge_y = second_edge_y; }
        
    protected:
        /// X-coordinate of the chessboard corner.
        double m_x;
        /// Y-coordinate of the chessboard corner.
        double m_y;
        /// X-coordinate of the first edge vector of the corner.
        double m_first_edge_x;
        /// Y-coordinate of the first edge vector of the corner.
        double m_first_edge_y;
        /// X-coordinate of the second edge vector of the corner.
        double m_second_edge_x;
        /// Y-coordinate of the second edge vector of the corner.
        double m_second_edge_y;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CHESSBOARD INFORMATION CLASS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which aggregates the corners which belong to the same chessboard pattern.
    class ChessboardInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ChessboardInformation(void) : m_number_of_horizontal_divisions(0), m_number_of_vertical_divisions(0), m_rectangle_width(0), m_rectangle_height(0) {}
        /** Constructor which initializes the information of the chessboard pattern.
         *  \param[in] corners vector with the corners which belong to the chessboard pattern. The corners are expected to be stored from the top-left corner to the bottom-right corner of the pattern.
         *  \param[in] number_of_horizontal_divisions number of horizontal divisions of the chessboard pattern.
         *  \param[in] number_of_vertical_divisions number of vertical divisions of the chessboard pattern.
         *  \param[in] rectangle_width width in real-world units of the chessboard rectangles.
         *  \param[in] rectangle_height height in real-world units of the chessboard rectangles.
         */
        ChessboardInformation(const VectorDense<ChessboardCorner> &corners, unsigned int number_of_horizontal_divisions, unsigned int number_of_vertical_divisions, double rectangle_width, double rectangle_height):
            m_corners(corners),
            m_number_of_horizontal_divisions(number_of_horizontal_divisions),
            m_number_of_vertical_divisions(number_of_vertical_divisions),
            m_rectangle_width(rectangle_width),
            m_rectangle_height(rectangle_height) {}
        /** Constructor which only sets the information of the chessboard pattern corners in the image.
         *  \param[in] corners vector with the corners which belong to the chessboard pattern. The corners are expected to be stored from the top-left corner to the bottom-right corner of the pattern.
         *  \param[in] number_of_horizontal_divisions number of horizontal divisions of the chessboard pattern.
         *  \param[in] number_of_vertical_divisions number of vertical divisions of the chessboard pattern.
         */
        ChessboardInformation(const VectorDense<ChessboardCorner> &corners, unsigned int number_of_horizontal_divisions, unsigned int number_of_vertical_divisions) :
            m_corners(corners),
            m_number_of_horizontal_divisions(number_of_horizontal_divisions),
            m_number_of_vertical_divisions(number_of_vertical_divisions),
            m_rectangle_width(1.0),
            m_rectangle_height(1.0) {}
        
        // -[ Set functions ]------------------------------------------------------------------------------------------------------------------------
        /** Function which sets the information of the chessboard pattern.
         *  \param[in] corners vector with the corners which belong to the chessboard pattern. The corners are expected to be stored from the top-left corner to the bottom-right corner of the pattern.
         *  \param[in] number_of_horizontal_divisions number of horizontal divisions of the chessboard pattern.
         *  \param[in] number_of_vertical_divisions number of vertical divisions of the chessboard pattern.
         *  \param[in] rectangle_width width in real-world units of the chessboard rectangles.
         *  \param[in] rectangle_height height in real-world units of the chessboard rectangles.
         */
        inline void set(const VectorDense<ChessboardCorner> &corners, unsigned int number_of_horizontal_divisions, unsigned int number_of_vertical_divisions, double rectangle_width, double rectangle_height)
        {
            m_corners = corners;
            m_number_of_horizontal_divisions = number_of_horizontal_divisions;
            m_number_of_vertical_divisions = number_of_vertical_divisions;
            m_rectangle_width = rectangle_width;
            m_rectangle_height = rectangle_height;
        }
        /** Function which sets the information of the corners of the chessboard pattern in the image.
         *  \param[in] corners vector with the corners which belong to the chessboard pattern. The corners are expected to be stored from the top-left corner to the bottom-right corner of the pattern.
         *  \param[in] number_of_horizontal_divisions number of horizontal divisions of the chessboard pattern.
         *  \param[in] number_of_vertical_divisions number of vertical divisions of the chessboard pattern.
         */
        inline void setCorners(const VectorDense<ChessboardCorner> &corners, unsigned int number_of_horizontal_divisions, unsigned int number_of_vertical_divisions)
        {
            m_corners = corners;
            m_number_of_horizontal_divisions = number_of_horizontal_divisions;
            m_number_of_vertical_divisions = number_of_vertical_divisions;
            m_rectangle_width = 1.0;
            m_rectangle_height = 1.0;
        }
        /** Function which sets the geometry of the chessboard rectangles.
         *  \param[in] rectangle_width width in real-world units of the chessboard rectangles.
         *  \param[in] rectangle_height height in real-world units of the chessboard rectangles.
         */
        inline void setPatternGeometry(double rectangle_width, double rectangle_height)
        {
            m_rectangle_width = rectangle_width;
            m_rectangle_height = rectangle_height;
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant reference to the vector with the chessboard pattern corners information.
        inline const VectorDense<ChessboardCorner >& getCorners(void) const { return m_corners; }
        /// Returns the number of horizontal division of the chessboard pattern.
        inline unsigned int getNumberOfHorizontalDivisions(void) const { return m_number_of_horizontal_divisions; }
        /// Returns the number of vertical divisions of the chessboard pattern.
        inline unsigned int getNumberOfVerticalDivisions(void) const { return m_number_of_vertical_divisions; }
        /// Returns the width of the chessboard pattern rectangles in real-world units.
        inline double getRectangleWidth(void) const { return m_rectangle_width; }
        /// Sets the width of the chessboard pattern rectangles in real-world units.
        inline void setRectangleWidth(double rectangle_width) { m_rectangle_width = rectangle_width; }
        /// Returns the height of the chessboard pattern rectangles in real-world units.
        inline double getRectangleHeight(void) const { return m_rectangle_height; }
        /// Sets the height of the chessboard pattern rectangles in real-world units.
        inline void setRectangleHeight(double rectangle_height) { m_rectangle_height = rectangle_height; }
        
        // -[ Draw the chessboard pattern in the image ]---------------------------------------------------------------------------------------------
        template <template <class> class IMAGE, class T>
        void draw(IMAGE<T> &image) const;
        
    protected:
        /// Corners of the chessboard pattern stored from top-left corner to the right-bottom corner.
        VectorDense<ChessboardCorner > m_corners;
        /// Number of horizontal division of the chessboard pattern.
        unsigned int m_number_of_horizontal_divisions;
        /// Number of vertical divisions of the chessboard pattern.
        unsigned int m_number_of_vertical_divisions;
        /// Chessboard rectangle width in real-world units.
        double m_rectangle_width;
        /// Chessboard rectangle height in real-world units.
        double m_rectangle_height;
    };
    
    template <template <class> class IMAGE, class T>
    void ChessboardInformation::draw(IMAGE<T> &image) const
    {
        const unsigned int number_of_elements = m_number_of_vertical_divisions * m_number_of_horizontal_divisions;
        Draw::Pencil<T> pencil;
        
        pencil.setBorderSize(2);
        pencil.setAntialiasing(true);
        pencil.setBackgroundColor(0, 0, 0, 0);
        for (unsigned int i = 0, k = 0; i < m_number_of_vertical_divisions; ++i)
        {
            for (unsigned int j = 0; j < m_number_of_horizontal_divisions; ++j, ++k)
            {
                double hue, huep, R, G, B;
                int x, y;
                
                x = (int)round(m_corners[k].getX());
                y = (int)round(m_corners[k].getY());
                hue = (double)k / (double)number_of_elements;;
                hue = (1.0 - hue) * 2.0 / 3.0; // Set the hue at 240 degree at max (from red to blue).
                
                huep = hue * 3.0; // divided by 2: 3 instead of 6.
                huep = (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                if (hue < 1.0 / 6.0) { R = 1.0; G = huep; B = 0.0; }
                else if (hue < 2.0 / 6.0) { R = huep; G = 1.0; B = 0.0; }
                else if (hue < 3.0 / 6.0) { R = 0.0; G = 1.0; B = huep; }
                else if (hue < 4.0 / 6.0) { R = 0.0; G = huep; B = 1.0; }
                else if (hue < 5.0 / 6.0) { R = huep; G = 0.0; B = 1.0; }
                else if (hue < 6.0 / 6.0) { R = 1.0; G = 0.0; B = huep; }
                else { R = 0.0; G = 0.0; B = 0.0; }
                pencil.setBorderColor((unsigned char)(255.0 * R), (unsigned char)(255.0 * G), (unsigned char)(255.0 * B), 255);
                
                if (j > 0)
                {
                    int xp, yp;
                    
                    xp = (int)round(m_corners[k - 1].getX());
                    yp = (int)round(m_corners[k - 1].getY());
                    Draw::Line(image, x, y, xp, yp, pencil);
                }
                if (i > 0)
                {
                    int xp, yp;
                    
                    xp = (int)round(m_corners[k - m_number_of_horizontal_divisions].getX());
                    yp = (int)round(m_corners[k - m_number_of_horizontal_divisions].getY());
                    Draw::Line(image, x, y, xp, yp, pencil);
                }
            }
        }
        for (unsigned int i = 0, k = 0; i < m_number_of_vertical_divisions; ++i)
        {
            for (unsigned int j = 0; j < m_number_of_horizontal_divisions; ++j, ++k)
            {
                double hue, huep, R, G, B;
                int x, y;
                
                x = (int)round(m_corners[k].getX());
                y = (int)round(m_corners[k].getY());
                hue = (double)k / (double)number_of_elements;;
                hue = (1.0 - hue) * 2.0 / 3.0; // Set the hue at 240 degree at max (from red to blue).
                
                huep = hue * 3.0; // divided by 2: 3 instead of 6.
                huep = (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                if (hue < 1.0 / 6.0) { R = 1.0; G = huep; B = 0.0; }
                else if (hue < 2.0 / 6.0) { R = huep; G = 1.0; B = 0.0; }
                else if (hue < 3.0 / 6.0) { R = 0.0; G = 1.0; B = huep; }
                else if (hue < 4.0 / 6.0) { R = 0.0; G = huep; B = 1.0; }
                else if (hue < 5.0 / 6.0) { R = huep; G = 0.0; B = 1.0; }
                else if (hue < 6.0 / 6.0) { R = 1.0; G = 0.0; B = huep; }
                else { R = 0.0; G = 0.0; B = 0.0; }
                pencil.setBorderColor((unsigned char)(255.0 * R), (unsigned char)(255.0 * G), (unsigned char)(255.0 * B), 255);
                
                Draw::Rectangle(image, x - 4, y - 4, x + 4, y + 4, pencil);
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CORNER PATTERN GENERATION FUNCTION   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    void createCorrelationPatch(double angle_x, double angle_y, unsigned int radius, bool flipped, Image<T> &convolution_patterns0, Image<T> &convolution_patterns1, Image<T> &convolution_patterns2, Image<T> &convolution_patterns3)
    {
        const double pi = 3.14159265358979;
        double pattern_sum0, pattern_sum1, pattern_sum2, pattern_sum3, half_radius2, pdf_norm;
        unsigned int window_size;
        
        half_radius2 = (double)(radius * radius) / 4.0;
        pdf_norm = ((double)radius / 2.0) * sqrt(2 * pi);
        window_size = (unsigned int)radius * 2 + 1;
        convolution_patterns0.setGeometry(window_size, window_size, 1);
        convolution_patterns1.setGeometry(window_size, window_size, 1);
        convolution_patterns2.setGeometry(window_size, window_size, 1);
        convolution_patterns3.setGeometry(window_size, window_size, 1);
        pattern_sum0 = pattern_sum1 = pattern_sum2 = pattern_sum3 = 0.0;
        for (unsigned int y = 0; y < window_size; ++y)
        {
            double * __restrict__ c0_ptr = convolution_patterns0.get(y, 0);
            double * __restrict__ c1_ptr = convolution_patterns1.get(y, 0);
            double * __restrict__ c2_ptr = convolution_patterns2.get(y, 0);
            double * __restrict__ c3_ptr = convolution_patterns3.get(y, 0);
            
            // Modification: Build the kernel already mirrored.
            for (unsigned int x = 0; x < window_size; ++x)
            {
                double rx, ry, distance, s1, s2, pdf_value;
                
                rx = (double)x - radius;
                ry = (double)y - radius;
                distance = sqrt(rx * rx + ry * ry);
                s1 = -rx * sin(angle_x) + ry * cos(angle_x);
                s2 = -rx * sin(angle_y) + ry * cos(angle_y);
                
                pdf_value = exp(-0.5 * distance * distance / half_radius2) / pdf_norm;
                if (flipped)
                {
                    if ((s1 >= -0.1) && (s2 >= -0.1))
                    {
                        c0_ptr[x] = pdf_value;
                        c1_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 <= 0.1) && (s2 <= 0.1))
                    {
                        c1_ptr[x] = pdf_value;
                        c0_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 >= -0.1) && (s2 <= 0.1))
                    {
                        c2_ptr[x] = pdf_value;
                        c0_ptr[x] = c1_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 <= 0.1) && (s2 >= -0.1))
                    {
                        c3_ptr[x] = pdf_value;
                        c0_ptr[x] = c1_ptr[x] = c2_ptr[x] = 0.0;
                    }
                    else
                    {
                        c0_ptr[x] = c1_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                }
                else
                {
                    if ((s1 <= -0.1) && (s2 <= -0.1)) //// Original: (s1 >= -0.1) && (s2 >= -0.1)
                    {
                        c0_ptr[x] = pdf_value;
                        c1_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 >= 0.1) && (s2 >= 0.1)) //// Original: (s1 <= 0.1) && (s2 <= 0.1)
                    {
                        c1_ptr[x] = pdf_value;
                        c0_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 <= -0.1) && (s2 >= 0.1)) //// Original: (s1 >= -0.1) && (s2 <= 0.1)
                    {
                        c2_ptr[x] = pdf_value;
                        c0_ptr[x] = c1_ptr[x] = c3_ptr[x] = 0.0;
                    }
                    else if ((s1 >= 0.1) && (s2 <= -0.1)) //// Original: (s1 <= 0.1) && (s2 >= -0.1)
                    {
                        c3_ptr[x] = pdf_value;
                        c0_ptr[x] = c1_ptr[x] = c2_ptr[x] = 0.0;
                    }
                    else
                    {
                        c0_ptr[x] = c1_ptr[x] = c2_ptr[x] = c3_ptr[x] = 0.0;
                    }
                }
                pattern_sum0 += c0_ptr[x];
                pattern_sum1 += c1_ptr[x];
                pattern_sum2 += c2_ptr[x];
                pattern_sum3 += c3_ptr[x];
            }
        }
        convolution_patterns0 /= pattern_sum0;
        convolution_patterns1 /= pattern_sum1;
        convolution_patterns2 /= pattern_sum2;
        convolution_patterns3 /= pattern_sum3;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CHESSBOARD CORNER DETECTION FUNCTION |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function detects the chessboard pattern corners present in the image.
     *  \param[in] source image where the putative chessboard corner points are located.
     *  \param[in] tau minimum score of the corners.
     *  \param[out] corners resulting vector with the corners information.
     *  \param[in] number_of_threads number of threads used to concurrently detect the corners in the image.
     *  \note This function implements the chessboard pattern detection algorithm implemented in the paper: A. Geiger, F. Moosmann, O. Car, B. Schuster, "Automatic Camera and Range Sensor Calibration using a Single Shot", ICRA 2012
     */
    template <template <class> class IMAGE, class T>
    void findCorners(const IMAGE<T> &source, double tau, VectorDense<ChessboardCorner> &corners, unsigned int number_of_threads)
    {
        // ----| Magical constant |--------------------------------------------------------------------------------------------------------
        const double pi = 3.14159265358979;
        const int region_size = 10;
        const double corner_threshold = 0.025;
        const unsigned int non_maxima_supression_size = 5;
        const unsigned int number_of_bins = 32;
        const double angular_step = pi / (double)number_of_bins;
        const double angular_smooth_sigma = 1.0;
        const double angular_histogram_threshold = 1e-5;
        const double minimum_corner_angle = 0.3;
        const double orientation_norm_threshold = 0.1;
        const double position_update_threshold = 4.0;
        // --------------------------------------------------------------------------------------------------------------------------------
        double minimum_value, maximum_value;
        Image<T> source_gray(source.getWidth(), source.getHeight(), 1);
        Image<double> work_image, convolution_patterns[4], image_corners_conv[4], image_corners_mean;
        Image<double> image_corners_a, image_corners_b, image_corners_1, image_corners_2, image_corners;
        // ----| FFT Convolution variables |-----------------------------------------------------------------------------------------------
        ImageFFT kernel_fft(source.getWidth(), source.getHeight(), 1);
        ImageFFT source_fft(source.getWidth(), source.getHeight(), 1);
        ImageFFT result_fft(source.getWidth(), source.getHeight(), 1);
        // --------------------------------------------------------------------------------------------------------------------------------
        ///// Chronometer chrono; // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        
        // ==| 1st part: Create a corners location image |===========================================================================================
        // 1) Process the image .....................................................................................................................
        ///// chrono.start(); // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        if (source.getNumberOfChannels() == 3) ImageRGB2Gray(source, source_gray, number_of_threads);
        else if (source.getNumberOfChannels() == 4) ImageRGB2Gray(Image<T>(source).subimage(0, 2), source_gray, number_of_threads);
        else if (source.getNumberOfChannels() == 1) source_gray = source;
        else throw Exception("The input image has an incorrect number of channels (%d).", source.getNumberOfChannels());
        work_image = source_gray;
        maximum_value = minimum_value = *work_image.get(0, 0, 0);
        for (unsigned int y = 0; y < work_image.getHeight(); ++y)
        {
            const double * ptr = work_image.get(y, 0);
            
            for (unsigned int x = 0; x < work_image.getWidth(); ++x, ++ptr)
            {
                if (*ptr > maximum_value) maximum_value = *ptr;
                if (*ptr < minimum_value) minimum_value = *ptr;
            }
        }
        work_image = (work_image - minimum_value) / (maximum_value - minimum_value);
        
        // 2) Search the corner 'pattern' ...........................................................................................................
        image_corners_conv[0].setGeometry(work_image);
        image_corners_conv[1].setGeometry(work_image);
        image_corners_conv[2].setGeometry(work_image);
        image_corners_conv[3].setGeometry(work_image);
        image_corners_mean.setGeometry(work_image);
        image_corners_a.setGeometry(work_image);
        image_corners_b.setGeometry(work_image);
        image_corners_1.setGeometry(work_image);
        image_corners_2.setGeometry(work_image);
        image_corners.setGeometry(work_image);
        image_corners.setValue(0.0);
        source_fft.setImage(work_image, number_of_threads); //< Calculate the frequency image of the 'work image' a single time.
        for (unsigned int i = 0; i < 6; ++i)
        {
            double radius, angle_x, angle_y;
            
            if (i % 2 == 0) { angle_x = 0.0; angle_y = pi / 2.0; }
            else { angle_x = pi / 4.0; angle_y = -pi / 4.0; }
            radius = (double)(i / 2 + 1) * 4;
            
            // 2.1) Generate the convolution patterns .................................................................
            createCorrelationPatch(angle_x, angle_y, (unsigned int)radius, true, convolution_patterns[0], convolution_patterns[1], convolution_patterns[2], convolution_patterns[3]);
            
            // 2.2) Filter the image using the generated patterns .....................................................
            for (unsigned int k = 0; k < 4; ++k) //< Do the convolution directly without calling the Convolution function.
            {
                kernel_fft.setImage(convolution_patterns[k], number_of_threads);
                result_fft.multiplication(source_fft, kernel_fft);
                result_fft.getImage(image_corners_conv[k], number_of_threads);
            }
            ////ImageConvolution(work_image, convolution_patterns[0], image_corners_conv[0], number_of_threads, true);
            ////ImageConvolution(work_image, convolution_patterns[1], image_corners_conv[1], number_of_threads, true);
            ////ImageConvolution(work_image, convolution_patterns[2], image_corners_conv[2], number_of_threads, true);
            ////ImageConvolution(work_image, convolution_patterns[3], image_corners_conv[3], number_of_threads, true);
            
            image_corners_mean.setValue(0.0);
            image_corners_mean += image_corners_conv[0];
            image_corners_mean += image_corners_conv[1];
            image_corners_mean += image_corners_conv[2];
            image_corners_mean += image_corners_conv[3];
            image_corners_mean /= 4.0;
            
            ImageMinimum(image_corners_conv[0] - image_corners_mean, image_corners_conv[1] - image_corners_mean, image_corners_a);
            ImageMinimum(image_corners_mean - image_corners_conv[2], image_corners_mean - image_corners_conv[3], image_corners_b);
            ImageMinimum(image_corners_a, image_corners_b, image_corners_1);
            
            ImageMinimum(image_corners_mean - image_corners_conv[0], image_corners_mean - image_corners_conv[1], image_corners_a);
            ImageMinimum(image_corners_conv[2] - image_corners_mean, image_corners_conv[3] - image_corners_mean, image_corners_b);
            ImageMinimum(image_corners_a, image_corners_b, image_corners_2);
            
            ImageMaximum(image_corners, image_corners_1, image_corners);
            ImageMaximum(image_corners, image_corners_2, image_corners);
        }
        // ----| Flip the convolved image |----------------------------------------------------------------------------
        ImageFFT::shift(image_corners);
        // ------------------------------------------------------------------------------------------------------------
        
        // ==| 2on Part: Search and refine corner locations |========================================================================================
        VectorDense<Triplet<unsigned int, unsigned int, unsigned int> > local_maximum;
        VectorDense<Tuple<double, double> > v1, v2, corner_location;
        Gaussian2DFilterRecursiveKernel kernel_dx(VLIET_4TH, 3.0, 1, 0, false);
        Gaussian2DFilterRecursiveKernel kernel_dy(VLIET_4TH, 3.0, 0, 1, false);
        Gaussian2DFilterKernel kernel_angular_histogram(angular_smooth_sigma, 0, 0, false);
        Image<double> work_dx(source.getWidth(), source.getHeight(), 1);
        Image<double> work_dy(source.getWidth(), source.getHeight(), 1);
        Image<double> work_angle(source.getWidth(), source.getHeight(), 1);
        Image<double> work_weight(source.getWidth(), source.getHeight(), 1);
        VectorDense<double> angular_histogram(number_of_bins), smoothed_angular_histogram;
        Matrix<double> A1(2, 2), A2(2, 2), G(2, 2), A(2, 2), U(2, 2), V(2, 2), b(2, 1), eigenvectors;
        VectorDense<double> eigenvalues, s_values;
        unsigned int number_of_corners;
        
        // Search for the local maxima in the corners image.
        ImageFindLocalExtrema(image_corners, non_maxima_supression_size, corner_threshold, local_maximum, true);
        number_of_corners = local_maximum.size();
        v1.set(local_maximum.size());
        v2.set(local_maximum.size());
        corner_location.set(local_maximum.size());
        for (unsigned int i = 0; i < local_maximum.size(); ++i)
            corner_location[i].setData((double)local_maximum[i].getFirst(), (double)local_maximum[i].getSecond());
        
        // Refine the corners location and filter out wrong results.
        GaussianFilter(source_gray, kernel_dx, work_dx, number_of_threads);
        GaussianFilter(source_gray, kernel_dy, work_dy, number_of_threads);
        for (unsigned int y = 0; y < work_image.getHeight(); ++y)
        {
            const double * __restrict__ ptr_dx = work_dx.get(y, 0);
            const double * __restrict__ ptr_dy = work_dy.get(y, 0);
            double * __restrict__ ptr_angle = work_angle.get(y, 0);
            double * __restrict__ ptr_weight = work_weight.get(y, 0);
            
            for (unsigned int x = 0; x < work_image.getWidth(); ++x)
            {
                double current_angle;
                
                current_angle = atan2(ptr_dy[x], ptr_dx[x]);
                if (current_angle < 0.0) current_angle += pi;
                if (current_angle > pi) current_angle -= pi;
                ptr_angle[x] = current_angle;
                ptr_weight[x] = sqrt(ptr_dx[x] * ptr_dx[x] + ptr_dy[x] * ptr_dy[x]);
            }
        }
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        ///////// //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
        ///////// Figure debug("debug");
        ///////// Draw::Font debug_font(Draw::SMALL_FONT_7x14);
        ///////// Draw::Pencil<unsigned char> red_pencil, green_pencil, text_pencil;
        ///////// Image<unsigned char> debug_image(ImageGray2HueColormap(ImageMaxcon(image_corners)));
        ///////// red_pencil.setBorderSize(1);
        ///////// red_pencil.setBorderColor(255, 0, 0, 255);
        ///////// red_pencil.setBackgroundColor(0, 0, 0, 0);
        ///////// red_pencil.setAntialiasing(false);
        ///////// green_pencil.setBorderSize(2);
        ///////// green_pencil.setBorderColor(0, 255, 0, 255);
        ///////// green_pencil.setBackgroundColor(0, 0, 0, 0);
        ///////// green_pencil.setAntialiasing(false);
        ///////// text_pencil.setBorderSize(1);
        ///////// text_pencil.setBorderColor(255, 255, 0, 255);
        ///////// text_pencil.setBackgroundColor(0, 0, 0, 0);
        ///////// text_pencil.setAntialiasing(false);
        ///////// for (unsigned int i = 0; i < number_of_corners; ++i)
        ///////// {
        /////////     int cx, cy;
        /////////     cx = (int)corner_location[i].getFirst();
        /////////     cy = (int)corner_location[i].getSecond();
        /////////     Draw::Rectangle(debug_image, cx - 4, cy - 4, cx + 4, cy + 4, red_pencil);
        ///////// }
        ///////// //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        ///////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        
        for (unsigned int i = 0; i < number_of_corners; ++i)
        {
            SubImage<double> angle_patch, weight_patch, dx_patch, dy_patch, image_patch;
            int cx, cy, region_x, region_y, region_width, region_height, patch_cx, patch_cy;
            VectorDense<Tuple<double, unsigned int> > modes_values;
            std::set<unsigned int> modes_bins;
            unsigned int valid_bins, index;
            double angle1, angle2, delta_angle/*, ratio0, ratio1*/;
            
            // Create a histogram with the gradient orientation and weights around the corner point.
            cx = (int)corner_location[i].getFirst();
            cy = (int)corner_location[i].getSecond();
            region_x = srvMax<int>(0, cx - region_size);
            region_y = srvMax<int>(0, cy - region_size);
            patch_cx = cx - region_x;
            patch_cy = cy - region_y;
            region_width = region_size * 2 + 1;
            region_height = region_size * 2 + 1;
            if (region_width + region_x > (int)work_angle.getWidth())
                region_width = work_angle.getWidth() - region_x;
            if (region_height + region_y > (int)work_angle.getHeight())
                region_height = work_angle.getHeight() - region_y;
            angle_patch = work_angle.subimage(region_x, region_y, region_width, region_height);
            weight_patch = work_weight.subimage(region_x, region_y, region_width, region_height);
            v1[i].setData(0, 0);
            v2[i].setData(0, 0);
            angular_histogram.setValue(0.0);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ angle_ptr = angle_patch.get(patch_y, 0);
                const double * __restrict__ weight_ptr = weight_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++angle_ptr, ++weight_ptr)
                {
                    double current_angle = *angle_ptr + pi / 2.0;
                    if (current_angle > pi) current_angle -= pi;
                    angular_histogram[(unsigned int)srvMax<int>(srvMin<int>((int)(current_angle / angular_step), (int)number_of_bins - 1), 0)] += *weight_ptr;
                }
            }
            // Search the two dominant orientations in the histogram of angles.
            kernel_angular_histogram.filterHorizontal(angular_histogram, smoothed_angular_histogram);
            valid_bins = 0;
            for (unsigned int j = 0; j < number_of_bins; ++j)
                if (smoothed_angular_histogram[j] > angular_histogram_threshold)
                    ++valid_bins;
            if (valid_bins == 0) continue;
            //////// {
            ////////     Draw::Text(debug_image, cx - 3, cy - 3, text_pencil, debug_font, "+C1"); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
            ////////     continue; // If all angular histogram bins are zero, the corner is not a valid corner.
            //////// }
            
            for (unsigned int k = 0; k < number_of_bins; ++k)
            {
                unsigned int j = k;
                while (true)
                {
                    unsigned int j1, j2;
                    double h0, h1, h2;
                    h0 = smoothed_angular_histogram[j];
                    j1 = (j + 1) % number_of_bins;
                    if (j > 0) j2 = j - 1;
                    else j2 = number_of_bins - 1;
                    h1 = smoothed_angular_histogram[j1];
                    h2 = smoothed_angular_histogram[j2];
                    if ((h1 >= h0) && (h1 >= h2)) j = j1;
                    else if ((h2 > h0) && (h2 > h1)) j = j2;
                    else break;
                }
                modes_bins.insert(j);
            }
            if (modes_bins.size() <= 1) continue;
            ////////{
            ////////    Draw::Text(debug_image, cx - 3, cy - 3, text_pencil, debug_font, "+C2"); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
            ////////    continue; // When a single mode or no mode is found then, the corner is not valid.
            ////////}
            modes_values.set((unsigned int)modes_bins.size());
            index = 0;
            for (std::set<unsigned int>::iterator begin = modes_bins.begin(), end = modes_bins.end(); begin != end; ++begin, ++index)
                modes_values[index].setData(smoothed_angular_histogram[*begin], *begin);
            sort(&modes_values[0], &modes_values[modes_values.size()], std::greater<Tuple<double, unsigned int> >());
            angle1 = ((double)modes_values[0].getSecond() * pi) / (double)number_of_bins;
            angle2 = ((double)modes_values[1].getSecond() * pi) / (double)number_of_bins;
            if (angle1 > angle2) srvSwap(angle1, angle2);
            delta_angle = srvMin(angle2 - angle1, angle1 + pi - angle2);
            if (delta_angle <= minimum_corner_angle) continue;
            ////////{
            ////////    Draw::Text(debug_image, cx - 3, cy - 3, text_pencil, debug_font, "+C3"); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
            ////////    continue; // If the angle between dominant orientation is too small, the corner is not valid.
            ////////}
            
            v1[i].setData(cos(angle1), sin(angle1));
            v2[i].setData(cos(angle2), sin(angle2));
            
            // Corner orientation refinement ..........................................................................
            A1.set(0.0);
            A2.set(0.0);
            dx_patch = work_dx.subimage(region_x, region_y, region_width, region_height);
            dy_patch = work_dy.subimage(region_x, region_y, region_width, region_height);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ dx_ptr = dx_patch.get(patch_y, 0);
                const double * __restrict__ dy_ptr = dy_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++dx_ptr, ++dy_ptr)
                {
                    double ox, oy, norm_o;
                    
                    ox = *dx_ptr;
                    oy = *dy_ptr;
                    norm_o = sqrt(ox * ox + oy * oy);
                    if (norm_o < orientation_norm_threshold)
                        continue;
                    ox /= norm_o;
                    oy /= norm_o;
                    
                    if (srvAbs(ox * v1[i].getFirst() + oy * v1[i].getSecond()) < 0.25)
                    {
                        A1(0, 0) += *dx_ptr * *dx_ptr;
                        A1(0, 1) += *dx_ptr * *dy_ptr;
                        A1(1, 0) += *dy_ptr * *dx_ptr;
                        A1(1, 1) += *dy_ptr * *dy_ptr;
                    }
                    if (srvAbs(ox * v2[i].getFirst() + oy * v2[i].getSecond()) < 0.25)
                    {
                        A2(0, 0) += *dx_ptr * *dx_ptr;
                        A2(0, 1) += *dx_ptr * *dy_ptr;
                        A2(1, 0) += *dy_ptr * *dx_ptr;
                        A2(1, 1) += *dy_ptr * *dy_ptr;
                    }
                }
            }
            MatrixEigen(A1, false, eigenvectors, eigenvalues);
            v1[i].setData(eigenvectors(0, 0), eigenvectors(1, 0));
            MatrixEigen(A2, false, eigenvectors, eigenvalues);
            v2[i].setData(eigenvectors(0, 0), eigenvectors(1, 0));
            
            // Corner location refinement .............................................................................
            G.set(0.0);
            b.set(0.0);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ dx_ptr = dx_patch.get(patch_y, 0);
                const double * __restrict__ dy_ptr = dy_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++dx_ptr, ++dy_ptr)
                {
                    double ox, oy, norm_o;
                    
                    ox = *dx_ptr;
                    oy = *dy_ptr;
                    norm_o = sqrt(ox * ox + oy * oy);
                    if (norm_o < orientation_norm_threshold)
                        continue;
                    ox /= norm_o;
                    oy /= norm_o;
                    
                    if ((patch_x != patch_cx) || (patch_y != patch_cy))
                    {
                        double wx, wy, dwx, dwy, d1, d2, f;
                        
                        wx = (double)patch_x - (double)patch_cx;
                        wy = (double)patch_y - (double)patch_cy;
                        f = wx * v1[i].getFirst() + wy * v1[i].getSecond();
                        dwx = wx - f * v1[i].getFirst();
                        dwy = wy - f * v1[i].getSecond();
                        d1 = sqrt(dwx * dwx + dwy * dwy);
                        f = wx * v2[i].getFirst() + wy * v2[i].getSecond();
                        dwx = wx - f * v2[i].getFirst();
                        dwy = wy - f * v2[i].getSecond();
                        d2 = sqrt(dwx * dwx + dwy * dwy);
                        
                        if (((d1 < 3.0) && (srvAbs(ox * v1[i].getFirst() + oy * v1[i].getSecond()) < 0.25)) ||
                            ((d2 < 3.0) && (srvAbs(ox * v2[i].getFirst() + oy * v2[i].getSecond()) < 0.25)))
                        {
                            double h00, h01, h10, h11;
                            
                            h00 = *dx_ptr * *dx_ptr;
                            h01 = *dx_ptr * *dy_ptr;
                            h10 = *dy_ptr * *dx_ptr;
                            h11 = *dy_ptr * *dy_ptr;
                            
                            G(0, 0) += h00;
                            G(0, 1) += h01;
                            G(1, 0) += h10;
                            G(1, 1) += h11;
                            b(0, 0) += h00 * (double)patch_x + h01 * (double)patch_y;
                            b(1, 0) += h10 * (double)patch_x + h11 * (double)patch_y;
                        }
                    }
                }
            }
            
            // If matrix G has full rank...
            ///// if (srvAbs(G(0, 1)) < 1e-6) ratio0 = 0.0;
            ///// else ratio0 = G(0, 0) / G(0, 1);
            ///// if (srvAbs(G(1, 1)) < 1e-6) ratio1 = 0.0;
            ///// else ratio1 = G(1, 0) / G(1, 1);
            ///// if (srvAbs(ratio0 - ratio1) > 1e-2)
            A = G; MatrixSVD(A, U, s_values, V);
            if ((s_values[0] > 1e-14) && (s_values[1] > 1e-14))
            {
                Tuple<double, double> corner_pos_old, corner_pos_new;
                Matrix<double> sol;
                double vx, vy;
                
                corner_pos_old = corner_location[i];
                MatrixSolve(G, b, sol);
                corner_pos_new.setData(sol(0, 0) + region_x, sol(1, 0) + region_y);
                corner_location[i] = corner_pos_new;
                
                vx = corner_pos_new.getFirst()  - corner_pos_old.getFirst();
                vy = corner_pos_new.getSecond() - corner_pos_old.getSecond();
                if (sqrt(vx * vx + vy * vy) >= position_update_threshold)
                {
                    ///////std::cout << "Case 4: " << v1[i] << " --- " << v2[i] << std::endl;
                    v1[i].setData(0, 0);
                    v2[i].setData(0, 0);
                    ///////Draw::Text(debug_image, cx - 3, cy - 3, text_pencil, debug_font, "+C4"); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
                    ///////debug(debug_image);
                    ///////Figure::wait();
                }
                /////// else Draw::Rectangle(debug_image, cx - 4, cy - 4, cx + 4, cy + 4, green_pencil); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
            }
            else // Set the corners as not valid.
            {
                ///////std::cout << "Case 5: " << v1[i] << " --- " << v2[i] << std::endl;
                v1[i].setData(0, 0);
                v2[i].setData(0, 0);
                ///////Draw::Text(debug_image, cx - 3, cy - 3, text_pencil, debug_font, "+C5"); // < DEBUG DEBUG DEBUG DEBUG DEBUG *********
                ///////debug(debug_image);
                ///////Figure::wait();
            }
        }
        
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        //////// //// vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv ////
        //////// debug(debug_image);
        //////// Figure::wait();
        //////// //// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ////
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        //////// //// DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG ////
        
        // ==| 3rd Part: Remove corners without edges and filter low scoring corners |===============================================================
        VectorDense<Tuple<double, unsigned int> > corner_scores;
        unsigned int number_of_valid_corners;
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < number_of_corners; ++i)
            if (!((v1[i].getFirst() == 0) && (v1[i].getSecond() == 0) && (v2[i].getFirst() == 0) && (v2[i].getFirst() == 0)))
                ++number_of_valid_corners;
        corner_scores.set(number_of_valid_corners);
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < number_of_corners; ++i)
        {
            if (!((v1[i].getFirst() == 0) && (v1[i].getSecond() == 0) && (v2[i].getFirst() == 0) && (v2[i].getFirst() == 0)))
            {
                double corner_score;
                int rx, ry;
                
                rx = (int)round(corner_location[i].getFirst());
                ry = (int)round(corner_location[i].getSecond());
                corner_score = 0.0;
                for (unsigned int j = 0; j < 3; ++j)
                {
                    const int radius = (j + 1) * 4; // Radius: 4, 8, 12.
                    const int score_size = 2 * radius + 1;
                    const int vector_size = score_size * score_size;
                    
                    if ((rx >= radius) && (rx < (int)work_image.getWidth() - radius) && (ry >= radius) && (ry < (int)work_image.getHeight() - radius))
                    {
                        double current_score, mean_weight, std_weight, mean_filter, std_filter, score_gradient, a1, a2, b1, b2, mu;
                        double * weight_vector, * filter_vector, score_a, score_b, score_1, score_2, score_intensity;
                        Image<double> image_filter(score_size, score_size, 1);
                        Image<double> weight_patch;
                        SubImage<double> image_patch;
                        
                        // Make a copy of the values, not only a pointer to the sub-image, since this values are modified for each
                        // radius of the correlation score.
                        weight_patch = work_weight.subimage(rx - radius, ry - radius, score_size, score_size);
                        image_patch = work_image.subimage(rx - radius, ry - radius, score_size, score_size);
                        weight_vector = weight_patch.get(0);
                        filter_vector = image_filter.get(0);
                        
                        current_score = 0.0;
                        for (int y = 0; y < score_size; ++y)
                        {
                            double * __restrict__ filter_ptr = image_filter.get(y, 0);
                            
                            for (int x = 0; x < score_size; ++x, ++filter_ptr)
                            {
                                double p1x, p1y, p2x, p2y, p3x, p3y, f, dp12x, dp12y, dp13x, dp13y;
                                
                                p1x = (double)(x - radius);
                                p1y = (double)(y - radius);
                                f = p1x * v1[i].getFirst() + p1y * v1[i].getSecond();
                                p2x = f * v1[i].getFirst();
                                p2y = f * v1[i].getSecond();
                                f = p1x * v2[i].getFirst() + p1y * v2[i].getSecond();
                                p3x = f * v2[i].getFirst();
                                p3y = f * v2[i].getSecond();
                                dp12x = p1x - p2x;
                                dp12y = p1y - p2y;
                                dp13x = p1x - p3x;
                                dp13y = p1y - p3y;
                                if ((sqrt(dp12x * dp12x + dp12y * dp12y) <= 1.5) || (sqrt(dp13x * dp13x + dp13y * dp13y) <= 1.5))
                                    *filter_ptr = 1.0;
                                else *filter_ptr = -1.0;
                            }
                        }
                        
                        mean_weight = 0.0;
                        mean_filter = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                        {
                            mean_weight += weight_vector[k];
                            mean_filter += filter_vector[k];
                        }
                        mean_weight /= (double)vector_size;
                        mean_filter /= (double)vector_size;
                        std_weight = 0.0;
                        std_filter = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                        {
                            std_weight += (weight_vector[k] - mean_weight) * (weight_vector[k] - mean_weight);
                            std_filter += (filter_vector[k] - mean_filter) * (filter_vector[k] - mean_filter);
                        }
                        std_weight = sqrt(std_weight / (double)(vector_size - 1));
                        std_filter = sqrt(std_filter / (double)(vector_size - 1));
                        
                        for (int k = 0; k < vector_size; ++k)
                        {
                            if (std_weight > 0) weight_vector[k] = (weight_vector[k] - mean_weight) / std_weight;
                            else weight_vector[k] = 0;
                            if (std_filter > 0) filter_vector[k] = (filter_vector[k] - mean_filter) / std_filter;
                            else filter_vector[k] = 0;
                        }
                        score_gradient = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                            score_gradient += weight_vector[k] * filter_vector[k];
                        if (score_gradient > 0.0) score_gradient /= (double)(vector_size - 1);
                        else score_gradient = 0.0;
                        createCorrelationPatch(atan2(v1[i].getSecond(), v1[i].getFirst()), atan2(v2[i].getSecond(), v2[i].getFirst()), (unsigned int)radius, false, convolution_patterns[0], convolution_patterns[1], convolution_patterns[2], convolution_patterns[3]);
                        
                        a1 = a2 = b1 = b2 = 0.0;
                        for (int y = 0; y < score_size; ++y)
                        {
                            const double * __restrict__ a1_ptr = convolution_patterns[0].get(y, 0);
                            const double * __restrict__ a2_ptr = convolution_patterns[1].get(y, 0);
                            const double * __restrict__ b1_ptr = convolution_patterns[2].get(y, 0);
                            const double * __restrict__ b2_ptr = convolution_patterns[3].get(y, 0);
                            const double * __restrict__ image_ptr = image_patch.get(y, 0);
                            
                            for (int x = 0; x < score_size; ++x, ++a1_ptr, ++a2_ptr, ++b1_ptr, ++b2_ptr, ++image_ptr)
                            {
                                a1 += *a1_ptr * *image_ptr;
                                a2 += *a2_ptr * *image_ptr;
                                b1 += *b1_ptr * *image_ptr;
                                b2 += *b2_ptr * *image_ptr;
                            }
                        }
                        mu = (a1 + a2 + b1 + b2) / 4.0;
                        score_a = srvMin(a1 - mu, a2 - mu);
                        score_b = srvMin(mu - b1, mu - b2);
                        score_1 = srvMin(score_a, score_b);
                        score_a = srvMin(mu - a1, mu - a2);
                        score_b = srvMin(b1 - mu, b2 - mu);
                        score_2 = srvMin(score_a, score_b);
                        score_intensity = srvMax(srvMax(score_1, score_2), 0.0);
                        current_score = score_gradient * score_intensity;
                        if (current_score > corner_score) corner_score = current_score;
                    }
                }
                corner_scores[number_of_valid_corners].setData(corner_score, i);
                ++number_of_valid_corners;
            }
        }
        // ............................................................................
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < corner_scores.size(); ++i)
            if (corner_scores[i].getFirst() >= tau)
                ++number_of_valid_corners;
        
        corners.set(number_of_valid_corners);
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < corner_scores.size(); ++i)
        {
            if (corner_scores[i].getFirst() >= tau)
            {
                unsigned int index = corner_scores[i].getSecond();
                Tuple<double, double> corner_n1;
                double flip;
                
                if (v1[index].getFirst() + v1[index].getSecond() < 0.0)
                    v1[index].setData(-v1[index].getFirst(), -v1[index].getSecond());
                // Make all coordinate systems right-handed (reduces matching ambiguities from 8 to 4).
                corner_n1.setData(v1[index].getSecond(), -v1[index].getFirst());
                if (corner_n1.getFirst() * v2[index].getFirst() + corner_n1.getSecond() * v2[index].getSecond() < 0.0) flip = 1.0;
                else flip = -1.0;
                
                corners[number_of_valid_corners].setCoordinates(corner_location[index].getFirst(), corner_location[index].getSecond());
                corners[number_of_valid_corners].setFirstEdge(v1[index].getFirst(), v1[index].getSecond());
                corners[number_of_valid_corners].setSecondEdge(flip * v2[index].getFirst(), flip * v2[index].getSecond());
                ++number_of_valid_corners;
            }
        }
        
        
        
        /////// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        /////// //    DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
        /////// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        /////// Figure debug("Debug");
        /////// std::cout << "Elapsed time: " << chrono.getElapsedTime() / 1000.0 << " seconds." << std::endl;
        /////// 
        /////// Draw::Pencil<unsigned char> red_pencil, yellow_pencil;
        /////// red_pencil.setBorderColor(255, 0, 0, 255);
        /////// red_pencil.setBackgroundColor(0, 0, 0, 0);
        /////// red_pencil.setBorderSize(1);
        /////// red_pencil.setAntialiasing(false);
        /////// yellow_pencil.setBorderColor(255, 255, 0, 255);
        /////// yellow_pencil.setBackgroundColor(0, 0, 0, 0);
        /////// yellow_pencil.setBorderSize(1);
        /////// yellow_pencil.setAntialiasing(false);
        /////// Image<unsigned char> debug_image(ImageGray2RGB(ImageMaxcon(image_corners)));
        /////// for (unsigned int i = 0; i < number_of_corners; ++i)
        /////// {
        ///////     int x = (int)round(corner_location[i].getFirst());
        ///////     int y = (int)round(corner_location[i].getSecond());
        ///////     if ((v1[i].getFirst() == 0) && (v1[i].getSecond() == 0) && (v2[i].getFirst() == 0) && (v2[i].getFirst() == 0))
        ///////         srv::Draw::Rectangle(debug_image, x - (int)non_maxima_supression_size, y - (int)non_maxima_supression_size, x + (int)non_maxima_supression_size, y + (int)non_maxima_supression_size, yellow_pencil);
        ///////     else srv::Draw::Rectangle(debug_image, x - (int)non_maxima_supression_size, y - (int)non_maxima_supression_size, x + (int)non_maxima_supression_size, y + (int)non_maxima_supression_size, red_pencil);
        /////// }
        /////// 
        /////// debug(debug_image);
        /////// for (char key = 0; key != 27; key = Figure::wait());
        /////// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
        /////// //    DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
        /////// // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
    }
    
    /** This function detects the chessboard pattern corners present in the image.
     *  \param[in] source image where the putative chessboard corner points are located.
     *  \param[in] tau minimum score of the corners.
     *  \param[out] corners resulting vector with the corners information.
     *  \param[in] number_of_threads number of threads used to concurrently detect the corners in the image.
     *  \note This function implements the chessboard pattern detection algorithm implemented in the paper: A. Geiger, F. Moosmann, O. Car, B. Schuster, "Automatic Camera and Range Sensor Calibration using a Single Shot", ICRA 2012
     */
    template <template <class> class IMAGE, class T>
    void findCornersGaussian(const IMAGE<T> &source, double tau, VectorDense<ChessboardCorner> &corners, unsigned int number_of_threads)
    {
        // ----| Magical constant |--------------------------------------------------------------------------------------------------------
        const double pi = 3.14159265358979;
        const int region_size = 10;
        const double corner_threshold = 0.025;
        const unsigned int non_maxima_supression_size = 5;
        const unsigned int number_of_bins = 32;
        const double angular_step = pi / (double)number_of_bins;
        const double angular_smooth_sigma = 1.0;
        const double angular_histogram_threshold = 1e-5;
        const double minimum_corner_angle = 0.3;
        const double orientation_norm_threshold = 0.1;
        const double position_update_threshold = 4.0;
        // --------------------------------------------------------------------------------------------------------------------------------
        double minimum_value, maximum_value;
        Image<T> source_gray(source.getWidth(), source.getHeight(), 1);
        Image<double> work_image, convolution_patterns[4], image_pattern_00, image_pattern_45, image_corners;
        
        // ==| 1st part: Create a corners location image |===========================================================================================
        // 1) Process the image .....................................................................................................................
        if (source.getNumberOfChannels() == 3) ImageRGB2Gray(source, source_gray, number_of_threads);
        else if (source.getNumberOfChannels() == 4) ImageRGB2Gray(Image<T>(source).subimage(0, 2), source_gray, number_of_threads);
        else if (source.getNumberOfChannels() == 1) source_gray = source;
        else throw Exception("The input image has an incorrect number of channels (%d).", source.getNumberOfChannels());
        work_image = source_gray;
        maximum_value = minimum_value = *work_image.get(0, 0, 0);
        for (unsigned int y = 0; y < work_image.getHeight(); ++y)
        {
            const double * ptr = work_image.get(y, 0);
            
            for (unsigned int x = 0; x < work_image.getWidth(); ++x, ++ptr)
            {
                if (*ptr > maximum_value) maximum_value = *ptr;
                if (*ptr < minimum_value) minimum_value = *ptr;
            }
        }
        work_image = (work_image - minimum_value) / (maximum_value - minimum_value);
        image_pattern_00.setGeometry(work_image);
        image_pattern_45.setGeometry(work_image);
        image_corners.setGeometry(work_image);
        image_corners.setValue(0.0);
        for (unsigned int i = 0; i < 3; ++i)
        {
            const double sigma = (double)(i + 1) * 3.0;
            Gaussian2DFilterRecursiveKernel kernel_chessboard_00(srv::VLIET_3RD, sigma, 1, 1, false);
            Gaussian2DFilterRecursiveKernel kernel_chessboard_45(srv::VLIET_3RD, sigma, 1, 1, true);
            
            GaussianFilter(work_image, kernel_chessboard_00, image_pattern_00, number_of_threads);
            GaussianFilter(work_image, kernel_chessboard_45, image_pattern_45, number_of_threads);
            ImageAbs(image_pattern_00, image_pattern_00);
            ImageAbs(image_pattern_45, image_pattern_45);
            ImageMaximum(image_corners, image_pattern_00, image_corners);
            ImageMaximum(image_corners, image_pattern_45, image_corners);
        }
        
        // ==| 2on Part: Search and refine corner locations |========================================================================================
        VectorDense<Triplet<unsigned int, unsigned int, unsigned int> > local_maximum;
        VectorDense<Tuple<double, double> > v1, v2, corner_location;
        Gaussian2DFilterRecursiveKernel kernel_dx(VLIET_3RD, 3.0, 1, 0, false);
        Gaussian2DFilterRecursiveKernel kernel_dy(VLIET_3RD, 3.0, 0, 1, false);
        Gaussian2DFilterKernel kernel_angular_histogram(angular_smooth_sigma, 0, 0, false);
        Image<double> work_dx(source.getWidth(), source.getHeight(), 1);
        Image<double> work_dy(source.getWidth(), source.getHeight(), 1);
        Image<double> work_angle(source.getWidth(), source.getHeight(), 1);
        Image<double> work_weight(source.getWidth(), source.getHeight(), 1);
        VectorDense<double> angular_histogram(number_of_bins), smoothed_angular_histogram;
        Matrix<double> A1(2, 2), A2(2, 2), G(2, 2), A(2, 2), U(2, 2), b(2, 1), V(2, 2), eigenvectors;
        VectorDense<double> eigenvalues, s_values;
        unsigned int number_of_corners;
        
        // Search for the local maxima in the corners image.
        ImageFindLocalExtrema(image_corners, non_maxima_supression_size, corner_threshold, local_maximum, true);
        number_of_corners = local_maximum.size();
        v1.set(local_maximum.size());
        v2.set(local_maximum.size());
        corner_location.set(local_maximum.size());
        for (unsigned int i = 0; i < local_maximum.size(); ++i)
            corner_location[i].setData((double)local_maximum[i].getFirst(), (double)local_maximum[i].getSecond());
        
        // Refine the corners location and filter out wrong results.
        GaussianFilter(source_gray, kernel_dx, work_dx, number_of_threads);
        GaussianFilter(source_gray, kernel_dy, work_dy, number_of_threads);
        for (unsigned int y = 0; y < work_image.getHeight(); ++y)
        {
            const double * __restrict__ ptr_dx = work_dx.get(y, 0);
            const double * __restrict__ ptr_dy = work_dy.get(y, 0);
            double * __restrict__ ptr_angle = work_angle.get(y, 0);
            double * __restrict__ ptr_weight = work_weight.get(y, 0);
            
            for (unsigned int x = 0; x < work_image.getWidth(); ++x)
            {
                double current_angle;
                
                current_angle = atan2(ptr_dy[x], ptr_dx[x]);
                if (current_angle < 0.0) current_angle += pi;
                if (current_angle > pi) current_angle -= pi;
                ptr_angle[x] = current_angle;
                ptr_weight[x] = sqrt(ptr_dx[x] * ptr_dx[x] + ptr_dy[x] * ptr_dy[x]);
            }
        }
        
        for (unsigned int i = 0; i < number_of_corners; ++i)
        {
            SubImage<double> angle_patch, weight_patch, dx_patch, dy_patch, image_patch;
            int cx, cy, region_x, region_y, region_width, region_height, patch_cx, patch_cy;
            VectorDense<Tuple<double, unsigned int> > modes_values;
            std::set<unsigned int> modes_bins;
            unsigned int valid_bins, index;
            double angle1, angle2, delta_angle/*, ratio0, ratio1*/;
            
            // Create a histogram with the gradient orientation and weights around the corner point.
            cx = (int)corner_location[i].getFirst();
            cy = (int)corner_location[i].getSecond();
            region_x = srvMax<int>(0, cx - region_size);
            region_y = srvMax<int>(0, cy - region_size);
            patch_cx = cx - region_x;
            patch_cy = cy - region_y;
            region_width = region_size * 2 + 1;
            region_height = region_size * 2 + 1;
            if (region_width + region_x > (int)work_angle.getWidth())
                region_width = work_angle.getWidth() - region_x;
            if (region_height + region_y > (int)work_angle.getHeight())
                region_height = work_angle.getHeight() - region_y;
            angle_patch = work_angle.subimage(region_x, region_y, region_width, region_height);
            weight_patch = work_weight.subimage(region_x, region_y, region_width, region_height);
            v1[i].setData(0, 0);
            v2[i].setData(0, 0);
            angular_histogram.setValue(0.0);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ angle_ptr = angle_patch.get(patch_y, 0);
                const double * __restrict__ weight_ptr = weight_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++angle_ptr, ++weight_ptr)
                {
                    double current_angle = *angle_ptr + pi / 2.0;
                    if (current_angle > pi) current_angle -= pi;
                    angular_histogram[(unsigned int)srvMax<int>(srvMin<int>((int)(current_angle / angular_step), (int)number_of_bins - 1), 0)] += *weight_ptr;
                }
            }
            // Search the two dominant orientations in the histogram of angles.
            kernel_angular_histogram.filterHorizontal(angular_histogram, smoothed_angular_histogram);
            valid_bins = 0;
            for (unsigned int j = 0; j < number_of_bins; ++j)
                if (smoothed_angular_histogram[j] > angular_histogram_threshold)
                    ++valid_bins;
            if (valid_bins == 0) continue;
            
            for (unsigned int k = 0; k < number_of_bins; ++k)
            {
                unsigned int j = k;
                while (true)
                {
                    unsigned int j1, j2;
                    double h0, h1, h2;
                    h0 = smoothed_angular_histogram[j];
                    j1 = (j + 1) % number_of_bins;
                    if (j > 0) j2 = j - 1;
                    else j2 = number_of_bins - 1;
                    h1 = smoothed_angular_histogram[j1];
                    h2 = smoothed_angular_histogram[j2];
                    if ((h1 >= h0) && (h1 >= h2)) j = j1;
                    else if ((h2 > h0) && (h2 > h1)) j = j2;
                    else break;
                }
                modes_bins.insert(j);
            }
            if (modes_bins.size() <= 1) continue;
            modes_values.set((unsigned int)modes_bins.size());
            index = 0;
            for (std::set<unsigned int>::iterator begin = modes_bins.begin(), end = modes_bins.end(); begin != end; ++begin, ++index)
                modes_values[index].setData(smoothed_angular_histogram[*begin], *begin);
            sort(&modes_values[0], &modes_values[modes_values.size()], std::greater<Tuple<double, unsigned int> >());
            angle1 = ((double)modes_values[0].getSecond() * pi) / (double)number_of_bins;
            angle2 = ((double)modes_values[1].getSecond() * pi) / (double)number_of_bins;
            if (angle1 > angle2) srvSwap(angle1, angle2);
            delta_angle = srvMin(angle2 - angle1, angle1 + pi - angle2);
            if (delta_angle <= minimum_corner_angle) continue;
            
            v1[i].setData(cos(angle1), sin(angle1));
            v2[i].setData(cos(angle2), sin(angle2));
            
            // Corner orientation refinement ..........................................................................
            A1.set(0.0);
            A2.set(0.0);
            dx_patch = work_dx.subimage(region_x, region_y, region_width, region_height);
            dy_patch = work_dy.subimage(region_x, region_y, region_width, region_height);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ dx_ptr = dx_patch.get(patch_y, 0);
                const double * __restrict__ dy_ptr = dy_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++dx_ptr, ++dy_ptr)
                {
                    double ox, oy, norm_o;
                    
                    ox = *dx_ptr;
                    oy = *dy_ptr;
                    norm_o = sqrt(ox * ox + oy * oy);
                    if (norm_o < orientation_norm_threshold)
                        continue;
                    ox /= norm_o;
                    oy /= norm_o;
                    
                    if (srvAbs(ox * v1[i].getFirst() + oy * v1[i].getSecond()) < 0.25)
                    {
                        A1(0, 0) += *dx_ptr * *dx_ptr;
                        A1(0, 1) += *dx_ptr * *dy_ptr;
                        A1(1, 0) += *dy_ptr * *dx_ptr;
                        A1(1, 1) += *dy_ptr * *dy_ptr;
                    }
                    if (srvAbs(ox * v2[i].getFirst() + oy * v2[i].getSecond()) < 0.25)
                    {
                        A2(0, 0) += *dx_ptr * *dx_ptr;
                        A2(0, 1) += *dx_ptr * *dy_ptr;
                        A2(1, 0) += *dy_ptr * *dx_ptr;
                        A2(1, 1) += *dy_ptr * *dy_ptr;
                    }
                }
            }
            MatrixEigen(A1, false, eigenvectors, eigenvalues);
            v1[i].setData(eigenvectors(0, 0), eigenvectors(1, 0));
            MatrixEigen(A2, false, eigenvectors, eigenvalues);
            v2[i].setData(eigenvectors(0, 0), eigenvectors(1, 0));
            
            // Corner location refinement .............................................................................
            G.set(0.0);
            b.set(0.0);
            for (int patch_y = 0; patch_y < region_height; ++patch_y)
            {
                const double * __restrict__ dx_ptr = dx_patch.get(patch_y, 0);
                const double * __restrict__ dy_ptr = dy_patch.get(patch_y, 0);
                
                for (int patch_x = 0; patch_x < region_width; ++patch_x, ++dx_ptr, ++dy_ptr)
                {
                    double ox, oy, norm_o;
                    
                    ox = *dx_ptr;
                    oy = *dy_ptr;
                    norm_o = sqrt(ox * ox + oy * oy);
                    if (norm_o < orientation_norm_threshold)
                        continue;
                    ox /= norm_o;
                    oy /= norm_o;
                    
                    if ((patch_x != patch_cx) || (patch_y != patch_cy))
                    {
                        double wx, wy, dwx, dwy, d1, d2, f;
                        
                        wx = (double)patch_x - (double)patch_cx;
                        wy = (double)patch_y - (double)patch_cy;
                        f = wx * v1[i].getFirst() + wy * v1[i].getSecond();
                        dwx = wx - f * v1[i].getFirst();
                        dwy = wy - f * v1[i].getSecond();
                        d1 = sqrt(dwx * dwx + dwy * dwy);
                        f = wx * v2[i].getFirst() + wy * v2[i].getSecond();
                        dwx = wx - f * v2[i].getFirst();
                        dwy = wy - f * v2[i].getSecond();
                        d2 = sqrt(dwx * dwx + dwy * dwy);
                        
                        if (((d1 < 3.0) && (srvAbs(ox * v1[i].getFirst() + oy * v1[i].getSecond()) < 0.25)) ||
                            ((d2 < 3.0) && (srvAbs(ox * v2[i].getFirst() + oy * v2[i].getSecond()) < 0.25)))
                        {
                            double h00, h01, h10, h11;
                            
                            h00 = *dx_ptr * *dx_ptr;
                            h01 = *dx_ptr * *dy_ptr;
                            h10 = *dy_ptr * *dx_ptr;
                            h11 = *dy_ptr * *dy_ptr;
                            
                            G(0, 0) += h00;
                            G(0, 1) += h01;
                            G(1, 0) += h10;
                            G(1, 1) += h11;
                            b(0, 0) += h00 * (double)patch_x + h01 * (double)patch_y;
                            b(1, 0) += h10 * (double)patch_x + h11 * (double)patch_y;
                        }
                    }
                }
            }
            
            // If matrix G has full rank...
            A = G; MatrixSVD(A, U, s_values, V);
            if ((s_values[0] > 1e-14) && (s_values[1] > 1e-14))
            {
                Tuple<double, double> corner_pos_old, corner_pos_new;
                Matrix<double> sol;
                double vx, vy;
                
                corner_pos_old = corner_location[i];
                MatrixSolve(G, b, sol);
                corner_pos_new.setData(sol(0, 0) + region_x, sol(1, 0) + region_y);
                corner_location[i] = corner_pos_new;
                
                vx = corner_pos_new.getFirst()  - corner_pos_old.getFirst();
                vy = corner_pos_new.getSecond() - corner_pos_old.getSecond();
                if (sqrt(vx * vx + vy * vy) >= position_update_threshold)
                {
                    v1[i].setData(0, 0);
                    v2[i].setData(0, 0);
                }
            }
            else // Set the corners as not valid.
            {
                v1[i].setData(0, 0);
                v2[i].setData(0, 0);
            }
        }
        
        // ==| 3rd Part: Remove corners without edges and filter low scoring corners |===============================================================
        VectorDense<Tuple<double, unsigned int> > corner_scores;
        unsigned int number_of_valid_corners;
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < number_of_corners; ++i)
            if (!((v1[i].getFirst() == 0) && (v1[i].getSecond() == 0) && (v2[i].getFirst() == 0) && (v2[i].getFirst() == 0)))
                ++number_of_valid_corners;
        corner_scores.set(number_of_valid_corners);
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < number_of_corners; ++i)
        {
            if (!((v1[i].getFirst() == 0) && (v1[i].getSecond() == 0) && (v2[i].getFirst() == 0) && (v2[i].getFirst() == 0)))
            {
                double corner_score;
                int rx, ry;
                
                rx = (int)round(corner_location[i].getFirst());
                ry = (int)round(corner_location[i].getSecond());
                corner_score = 0.0;
                for (unsigned int j = 0; j < 3; ++j)
                {
                    const int radius = (j + 1) * 4; // Radius: 4, 8, 12.
                    const int score_size = 2 * radius + 1;
                    const int vector_size = score_size * score_size;
                    
                    if ((rx >= radius) && (rx < (int)work_image.getWidth() - radius) && (ry >= radius) && (ry < (int)work_image.getHeight() - radius))
                    {
                        double current_score, mean_weight, std_weight, mean_filter, std_filter, score_gradient, a1, a2, b1, b2, mu;
                        double * weight_vector, * filter_vector, score_a, score_b, score_1, score_2, score_intensity;
                        Image<double> image_filter(score_size, score_size, 1);
                        Image<double> weight_patch;
                        SubImage<double> image_patch;
                        
                        // Make a copy of the values, not only a pointer to the sub-image, since this values are modified for each
                        // radius of the correlation score.
                        weight_patch = work_weight.subimage(rx - radius, ry - radius, score_size, score_size);
                        image_patch = work_image.subimage(rx - radius, ry - radius, score_size, score_size);
                        weight_vector = weight_patch.get(0);
                        filter_vector = image_filter.get(0);
                        
                        current_score = 0.0;
                        for (int y = 0; y < score_size; ++y)
                        {
                            double * __restrict__ filter_ptr = image_filter.get(y, 0);
                            
                            for (int x = 0; x < score_size; ++x, ++filter_ptr)
                            {
                                double p1x, p1y, p2x, p2y, p3x, p3y, f, dp12x, dp12y, dp13x, dp13y;
                                
                                p1x = (double)(x - radius);
                                p1y = (double)(y - radius);
                                f = p1x * v1[i].getFirst() + p1y * v1[i].getSecond();
                                p2x = f * v1[i].getFirst();
                                p2y = f * v1[i].getSecond();
                                f = p1x * v2[i].getFirst() + p1y * v2[i].getSecond();
                                p3x = f * v2[i].getFirst();
                                p3y = f * v2[i].getSecond();
                                dp12x = p1x - p2x;
                                dp12y = p1y - p2y;
                                dp13x = p1x - p3x;
                                dp13y = p1y - p3y;
                                if ((sqrt(dp12x * dp12x + dp12y * dp12y) <= 1.5) || (sqrt(dp13x * dp13x + dp13y * dp13y) <= 1.5))
                                    *filter_ptr = 1.0;
                                else *filter_ptr = -1.0;
                            }
                        }
                        
                        mean_weight = 0.0;
                        mean_filter = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                        {
                            mean_weight += weight_vector[k];
                            mean_filter += filter_vector[k];
                        }
                        mean_weight /= (double)vector_size;
                        mean_filter /= (double)vector_size;
                        std_weight = 0.0;
                        std_filter = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                        {
                            std_weight += (weight_vector[k] - mean_weight) * (weight_vector[k] - mean_weight);
                            std_filter += (filter_vector[k] - mean_filter) * (filter_vector[k] - mean_filter);
                        }
                        std_weight = sqrt(std_weight / (double)(vector_size - 1));
                        std_filter = sqrt(std_filter / (double)(vector_size - 1));
                        
                        for (int k = 0; k < vector_size; ++k)
                        {
                            if (std_weight > 0) weight_vector[k] = (weight_vector[k] - mean_weight) / std_weight;
                            else weight_vector[k] = 0;
                            if (std_filter > 0) filter_vector[k] = (filter_vector[k] - mean_filter) / std_filter;
                            else filter_vector[k] = 0;
                        }
                        score_gradient = 0.0;
                        for (int k = 0; k < vector_size; ++k)
                            score_gradient += weight_vector[k] * filter_vector[k];
                        if (score_gradient > 0.0) score_gradient /= (double)(vector_size - 1);
                        else score_gradient = 0.0;
                        createCorrelationPatch(atan2(v1[i].getSecond(), v1[i].getFirst()), atan2(v2[i].getSecond(), v2[i].getFirst()), (unsigned int)radius, false, convolution_patterns[0], convolution_patterns[1], convolution_patterns[2], convolution_patterns[3]);
                        
                        a1 = a2 = b1 = b2 = 0.0;
                        for (int y = 0; y < score_size; ++y)
                        {
                            const double * __restrict__ a1_ptr = convolution_patterns[0].get(y, 0);
                            const double * __restrict__ a2_ptr = convolution_patterns[1].get(y, 0);
                            const double * __restrict__ b1_ptr = convolution_patterns[2].get(y, 0);
                            const double * __restrict__ b2_ptr = convolution_patterns[3].get(y, 0);
                            const double * __restrict__ image_ptr = image_patch.get(y, 0);
                            
                            for (int x = 0; x < score_size; ++x, ++a1_ptr, ++a2_ptr, ++b1_ptr, ++b2_ptr, ++image_ptr)
                            {
                                a1 += *a1_ptr * *image_ptr;
                                a2 += *a2_ptr * *image_ptr;
                                b1 += *b1_ptr * *image_ptr;
                                b2 += *b2_ptr * *image_ptr;
                            }
                        }
                        mu = (a1 + a2 + b1 + b2) / 4.0;
                        score_a = srvMin(a1 - mu, a2 - mu);
                        score_b = srvMin(mu - b1, mu - b2);
                        score_1 = srvMin(score_a, score_b);
                        score_a = srvMin(mu - a1, mu - a2);
                        score_b = srvMin(b1 - mu, b2 - mu);
                        score_2 = srvMin(score_a, score_b);
                        score_intensity = srvMax(srvMax(score_1, score_2), 0.0);
                        current_score = score_gradient * score_intensity;
                        if (current_score > corner_score) corner_score = current_score;
                    }
                }
                corner_scores[number_of_valid_corners].setData(corner_score, i);
                ++number_of_valid_corners;
            }
        }
        // ............................................................................
        
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < corner_scores.size(); ++i)
            if (corner_scores[i].getFirst() >= tau)
                ++number_of_valid_corners;
        
        corners.set(number_of_valid_corners);
        number_of_valid_corners = 0;
        for (unsigned int i = 0; i < corner_scores.size(); ++i)
        {
            if (corner_scores[i].getFirst() >= tau)
            {
                unsigned int index = corner_scores[i].getSecond();
                Tuple<double, double> corner_n1;
                double flip;
                
                if (v1[index].getFirst() + v1[index].getSecond() < 0.0)
                    v1[index].setData(-v1[index].getFirst(), -v1[index].getSecond());
                // Make all coordinate systems right-handed (reduces matching ambiguities from 8 to 4).
                corner_n1.setData(v1[index].getSecond(), -v1[index].getFirst());
                if (corner_n1.getFirst() * v2[index].getFirst() + corner_n1.getSecond() * v2[index].getSecond() < 0.0) flip = 1.0;
                else flip = -1.0;
                
                corners[number_of_valid_corners].setCoordinates(corner_location[index].getFirst(), corner_location[index].getSecond());
                corners[number_of_valid_corners].setFirstEdge(v1[index].getFirst(), v1[index].getSecond());
                corners[number_of_valid_corners].setSecondEdge(flip * v2[index].getFirst(), flip * v2[index].getSecond());
                ++number_of_valid_corners;
            }
        }
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CHESSBOARD DETECTION FUNCTION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function search congruent aggregations of chessboard corners and finds the chessboard patterns in the image.
     *  \param[in] corners vector with the putative chessboard patterns of the image.
     *  \param[out] chessboard vector with the resulting chessboard patterns.
     *  \note This function implements the chessboard pattern detection algorithm implemented in the paper: A. Geiger, F. Moosmann, O. Car, B. Schuster, "Automatic Camera and Range Sensor Calibration using a Single Shot", ICRA 2012
     */
    void detectChessboard(const VectorDense<ChessboardCorner> &corners, VectorDense<ChessboardInformation> &chessboards);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

