// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_RANDOM_SEEDER_HPP_HEADER_FILE__
#define __SRV_RANDOM_SEEDER_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <cstdio>
#include <cstdlib>
#include <limits>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | RANDOM SEEDER CLASS DECLARATION      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class SeederRandom : public SeederBase<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SeederRandom(void);
        /** Parameters descriptor which sets the distance parameters.
         *  \param[in] distance_object object used to calculate the distance between the feature samples used to create the seeds.
         *  \param[in] minimum_distance minimum distance between selected seeds.
         *  \param[in] ratio ratio of samples used to create the sub-set of samples where the samples are sampled from.
         */
        SeederRandom(const VectorDistance &distance_object, double minimum_distance, double ratio);
        /// Virtual destructor.
        virtual ~SeederRandom(void) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant reference to the object which measures the distance between the selected seeds.
        inline const VectorDistance& getDistanceObject(void) const { return m_distance_object; }
        /// Returns the minimum distance between selected seeds.
        inline double getMinimumDistance(void) const { return m_minimum_distance; }
        /// Returns the ratio of samples used to create the sub-set of samples where the seeds are extracted from.
        inline double getRatio(void) const { return m_ratio; }
        /// Sets the ratio of samples used to create the sub-set of samples where the seeds are extracted from.
        inline void setRatio(double ratio) { m_ratio = ratio; }
        /** Sets the parameters of the seeder.
         *  \param[in] distance_object object used to calculate the distance between the feature samples used to create the seeds.
         *  \param[in] minimum_distance minimum distance between selected seeds.
         *  \param[in] ratio ratio of samples used to create the sub-set of samples where the samples are sampled from.
         */
        inline void set(const VectorDistance &distance_object, double minimum_distance, double ratio)
        {
            m_distance_object = distance_object;
            m_minimum_distance = minimum_distance;
            m_ratio = ratio;
        }
        
        // -[ Virtual functions ]--------------------------------------------------------------------------------------------------------------------
        using SeederBase<T, TMODE, TDISTANCE, N>::sow;
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant dense sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds. The vectors of this arrays are expected to be properly initialized (i.e. to have the correct dimensionality).
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void sow(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const { inner_sow(data, number_of_elements, seeds, number_of_seeds, number_of_threads, logger); }
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant sparse sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds. The vectors of this arrays are expected to be properly initialized (i.e. to have the correct dimensionality).
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void sow(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const { inner_sow(data, number_of_elements, seeds, number_of_seeds, number_of_threads, logger); }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the seeder object (virtual copy constructor).
        inline SeederBase<T, TMODE, TDISTANCE, N> * duplicate(void) const { return (SeederBase<T, TMODE, TDISTANCE, N> *)new SeederRandom<T, TMODE, TDISTANCE, N>(*this); }
        /// Returns the class identifier for the current seeder method.
        inline static SEEDER_IDENTIFIER getClassIdentifier(void) { return SEEDER_UNIFORM_RANDOM_SELECTION; }
        /// Generates a new empty instance of the seeder class.
        inline static SeederBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (SeederBase<T, TMODE, TDISTANCE, N>*)new SeederRandom(); }
        /// Returns a flag which states if the seeder class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the seeder type identifier of the current object.
        inline SEEDER_IDENTIFIER getIdentifier(void) const { return SEEDER_UNIFORM_RANDOM_SELECTION; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the seeder information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the seeder information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /** Private template function which generates the set of seeds for the given data set both for sparse and dense vectors.
         *  \param[in] data array of pointers to the constant sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void inner_sow(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger) const;
        
        /// Measures the distance between the selected seeds.
        VectorDistance m_distance_object;
         /// Minimum distance between selected seeds.
        double m_minimum_distance;
        /// Ratio of samples used to calculate the maximum number of vectors used to create the PCA model.
        double m_ratio;
        // Integer flag which is greater than zero when the seeder object has been initialized in a seeder factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RANDOM SEEDER CLASS IMPLEMENTATION   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    SeederRandom<T, TMODE, TDISTANCE, N>::SeederRandom(void) :
        m_distance_object(EUCLIDEAN_DISTANCE),
        m_minimum_distance(0),
        m_ratio(1.0)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    SeederRandom<T, TMODE, TDISTANCE, N>::SeederRandom(const VectorDistance &distance_object, double minimum_distance, double ratio) :
        m_distance_object(distance_object),
        m_minimum_distance(minimum_distance),
        m_ratio(ratio)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void SeederRandom<T, TMODE, TDISTANCE, N>::inner_sow(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int /* number_of_threads */, BaseLogger * logger) const
    {
        VectorDense<unsigned int> data_indexes(number_of_elements);
        unsigned int available_elements;
        
        if (logger != 0)
            logger->log("Selecting %d seeds from %d features.", number_of_seeds, number_of_elements);
        for (unsigned int index = 0; index < number_of_elements; ++index)
            data_indexes[index] = index;
        for (unsigned int index = 0; index < number_of_elements; ++index)
            srvSwap(data_indexes[index], data_indexes[index + rand() % (number_of_elements - index)]);
        available_elements = srvMin<unsigned int>(number_of_elements, srvMax<unsigned int>(number_of_seeds, (unsigned int)((double)number_of_elements * m_ratio)));
        
        for (unsigned int current_seed = 0; current_seed < number_of_seeds; ++current_seed)
        {
            unsigned int selected_seed_index;
            bool no_valid_seed;
            
            no_valid_seed = true;
            selected_seed_index = 0;
            for (unsigned int i = 0; i < available_elements; ++i)
            {
                double minimum_distance, current_distance;
                
                minimum_distance = std::numeric_limits<double>::max();
                for (unsigned int j = 0; j < current_seed; ++j)
                {
                    m_distance_object.distance(*data[data_indexes[i]], seeds[j], current_distance);
                    if (current_distance < minimum_distance)
                        minimum_distance = current_distance;
                }
                if (minimum_distance > m_minimum_distance)
                {
                    no_valid_seed = false;
                    selected_seed_index = i;
                    break;
                }
            }
            
            if (no_valid_seed)
                throw Exception("No seed can be found at a distance greater than the minimum distance threshold '%f'.", m_minimum_distance);
            
            seeds[current_seed].copy(*data[data_indexes[selected_seed_index]]);
            --available_elements;
            srvSwap(data_indexes[selected_seed_index], data_indexes[available_elements]);
            
            if ((logger != 0) && (current_seed % 10 == 9))
                logger->log("Selected %d of %d seeds.", current_seed + 1, number_of_elements);
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void SeederRandom<T, TMODE, TDISTANCE, N>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Seeder");
        parser.setAttribute("Identifier", this->getClassIdentifier());
        parser.setAttribute("Minimum_Distance", m_minimum_distance);
        parser.setAttribute("Ratio", m_ratio);
        parser.addChildren();
        m_distance_object.convertToXML(parser);
        parser.closeTag();
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void SeederRandom<T, TMODE, TDISTANCE, N>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Seeder") && ((SEEDER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getClassIdentifier()))
        {
            m_minimum_distance = parser.getAttribute("Minimum_Distance");
            m_ratio = parser.getAttribute("Ratio");
            
            while (!(parser.isTagIdentifier("Seeder") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Vector_Distance"))
                    m_distance_object.convertFromXML(parser);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

