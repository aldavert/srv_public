// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_CENTER_BASED_BASE_HPP_HEADER_FILE__
#define __SRV_CLUSTER_CENTER_BASED_BASE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
#include <algorithm>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "srv_cluster_base.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | BASE CLASS FOR THE CENTER-BASED      |
    //                   | CLUSTERING ALGORITHMS                |
    //                   | ** DECLARATION **                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    class CenterBasedClusterInformationTrace;
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class ClusterCenterBasedBase : public ClusterBase<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClusterCenterBasedBase(void);
        /** Constructor which initializes all the parameters of the base cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the space partitions.
         */
        ClusterCenterBasedBase(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method);
        /// Copy constructor.
        ClusterCenterBasedBase(const ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> &other);
        /// Destructor.
        virtual ~ClusterCenterBasedBase(void);
        /// Assignation operator.
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>& operator=(const ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> &other);
        
        // -[ Redefinition of base access functions ]------------------------------------------------------------------------------------------------
        // These functions are redefined to here to facilitate the access to their values.
        /// Returns the number of clusters.
        inline unsigned int getNumberOfClusters(void) const { return this->m_number_of_clusters; }
        /// Returns the number of dimensions of the clustered feature vectors.
        inline unsigned int getNumberOfDimensions(void) const { return this->m_number_of_dimensions; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Sets the number of clusters of the clustering algorithm.
        void setNumberOfCluster(unsigned int number_of_clusters);
        
        /// Returns a constant reference to the centroid of the index-th cluster.
        inline const VectorDense<TMODE, N>& getClusterCentroid(unsigned int index) const { return m_cluster_centers[index]; }
        /// Returns a constant pointer to the array of cluster centroids.
        inline const VectorDense<TMODE, N> * getClusterCentroids(void) const { return m_cluster_centers; }
        /** This function sets the centroids of the clusters.
         *  \param[in] cluster_centers array with the centroids of each cluster.
         *  \param[in] number_of_clusters number of clusters in the array.
         *  \param[in] number_of_threads number of threads used to initialize concurrently the centroids of the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void setClusterCentroids(const ConstantSubVectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** This function sets the centroids of the clusters.
         *  \param[in] cluster_centers array with the centroids of each cluster.
         *  \param[in] number_of_clusters number of clusters in the array.
         *  \param[in] number_of_threads number of threads used to initialize concurrently the centroids of the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual inline void setClusterCentroids(const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            ConstantSubVectorDense<TMODE, N> * constant_ptr = new ConstantSubVectorDense<TMODE, N>[number_of_clusters];
            for (unsigned int i = 0; i < number_of_clusters; ++i)
                constant_ptr[i] = cluster_centers[i];
            setClusterCentroids(constant_ptr, number_of_clusters, number_of_threads, logger);
            delete [] constant_ptr;
        }
        
        /// Returns a constant pointer to the seeder object.
        inline const SeederBase<T, TMODE, TDISTANCE, N> * getSeeder(void) const { return m_seeder; }
        /// Sets the seeder object used to create the initial set of samples of the clustering algorithm.
        inline void setSeeder(const SeederBase<T, TMODE, TDISTANCE, N> * seeder)
        {
            if (m_seeder != 0) delete m_seeder;
            m_seeder = (seeder != 0)?seeder->duplicate():0;
        }
        
        /// Returns the metric used to calculate the distance between the data vectors and the centroids of the clusters.
        inline const VectorDistance& getDistanceObject(void) const { return m_distance_object; }
        /// Sets the metric used to calculate the distance between the data vectors and the centroids of the clusters.
        inline void setDistanceObject(const VectorDistance &distance_object) { m_distance_object = distance_object; }
        /// Returns the distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
        inline TDISTANCE getCutoff(void) const { return m_cutoff; }
        /// Sets the distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
        inline void setCutoff(TDISTANCE cutoff) { m_cutoff = cutoff; }
        /// Returns the method used to deal with empty clusters while partitioning the feature space.
        inline CLUSTER_EMPTY_METHOD getClusterEmptyMethod(void) const { return m_cluster_empty_method; }
        /// Sets the method used to deal with empty clusters while partitioning the feature space.
        inline void setClusterEmptyMethod(CLUSTER_EMPTY_METHOD cluster_empty_method) { m_cluster_empty_method = cluster_empty_method; }
        /// Returns the maximum number of iterations of the algorithm.
        inline unsigned int getMaximumNumberOfIterations(void) const { return m_maximum_number_of_iterations; }
        /// Sets the maximum number of iterations of the algorithm.
        inline void setMaximumNumberOfIterations(unsigned int maximum_number_of_iterations) { m_maximum_number_of_iterations = maximum_number_of_iterations; }
        
        // -[ Clustering functions ]-----------------------------------------------------------------------------------------------------------------
        /** Creates the clusters from the set of dense data vectors.
         *  \param[in] data array of pointers to the constant dense sub-vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void create(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            if ((number_of_elements >= this->m_number_of_clusters) && (this->m_number_of_clusters > 0) && (this->m_seeder != 0))
            {
                this->m_seeder->sow(data, number_of_elements, this->m_cluster_centers, this->m_number_of_clusters, number_of_threads, logger);
                this->train(data, number_of_elements, this->m_cluster_centers, this->m_number_of_clusters, number_of_threads, logger);
            }
        }
        /** Creates the clusters from the set of sparse data vectors.
         *  \param[in] data array of pointers to the constant dense sub-vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void create(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            if ((number_of_elements >= this->m_number_of_clusters) && (this->m_number_of_clusters > 0) && (this->m_seeder != 0))
            {
                this->m_seeder->sow(data, number_of_elements, this->m_cluster_centers, this->m_number_of_clusters, number_of_threads, logger);
                this->train(data, number_of_elements, this->m_cluster_centers, this->m_number_of_clusters, number_of_threads, logger);
            }
        }
        
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant dense sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void cluster(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            inner_cluster(data, data_clusters, number_of_elements, number_of_threads, logger);
        }
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void cluster(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            inner_cluster(data, data_clusters, number_of_elements, number_of_threads, logger);
        }
        
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the data constant dense sub-vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void update(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            inner_update(data, number_of_elements, update_information, global_clustering, number_of_threads, logger);
        }
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void update(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            inner_update(data, number_of_elements, update_information, global_clustering, number_of_threads, logger);
        }
        
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query constant dense sub-vectors.
         *  \param[in] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void nearest(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<TDISTANCE, unsigned int> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            inner_nearest(data, distances, number_of_elements, number_of_neighbors, number_of_threads, logger);
        }
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query constant sparse sub-vectors.
         *  \param[in] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void nearest(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<TDISTANCE, unsigned int> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            inner_nearest(data, distances, number_of_elements, number_of_neighbors, number_of_threads, logger);
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of the cluster object into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the information of the cluster object from a XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Protected functions ]------------------------------------------------------------------------------------------------------------------
        /** Initializes the parameters of the base cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the partitions of the feature space.
         */
        void setCenterBasedBase(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method);
        /// Adds the current cluster centers to the trace list information.
        void addTraceInformation(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters);
        
        // -[ Protected template functions ]---------------------------------------------------------------------------------------------------------
        /** Template function which assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information.
         */
        template <template <class, class> class VECTOR>
        void inner_cluster(VECTOR<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger) const;
        /** Template function which updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the data constant sub-vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information.
         */
        template <template <class, class> class VECTOR>
        void inner_update(VECTOR<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger);
        /** Template function which searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query constant sub-vectors.
         *  \param[in] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information.
         */
        template <template <class, class> class VECTOR>
        void inner_nearest(VECTOR<T, N> const * const * data, VectorSparse<TDISTANCE, unsigned int> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger) const;
        
        // -[ Virtual protected functions ]----------------------------------------------------------------------------------------------------------
        /** Pure virtual function which implements the specific center-based clustering algorithm.
         *  \param[in] data array of pointers to the constant dense sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void train(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /** Pure virtual function which implements the specific center-based clustering algorithm.
         *  \param[in] data array of pointers to the constant sparse sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void train(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /// Virtual function which stores the attributes of the derived class.
        virtual void convertToAttributesXML(XmlParser &parser) const = 0;
        /// Virtual function which restores the attributes of the derived class.
        virtual void convertFromAttributesXML(XmlParser &parser) = 0;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Centroid of each cluster.
        VectorDense<TMODE, N> * m_cluster_centers;
        /// Object used to create the seeds of the clustering algorithm.
        SeederBase<T, TMODE, TDISTANCE, N> * m_seeder;
        /// Metric used to calculate the distance between the data vectors and the centroids of the clusters.
        VectorDistance m_distance_object;
        /// Maximum number of iterations of the algorithm.
        unsigned int m_maximum_number_of_iterations;
        /// Distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
        TDISTANCE m_cutoff;
        /// Method used to deal with empty clusters while partitioning the feature space.
        CLUSTER_EMPTY_METHOD m_cluster_empty_method;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BASE CLASS FOR THE CENTER-BASED      |
    //                   | CLUSTERING ALGORITHMS                |
    //                   | ** IMPLEMENTATION **                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::ClusterCenterBasedBase(void) :
        ClusterBase<T, TMODE, TDISTANCE, N>(),
        m_cluster_centers(0),
        m_seeder(0),
        m_distance_object(EUCLIDEAN_DISTANCE),
        m_maximum_number_of_iterations(1000),
        m_cutoff(0),
        m_cluster_empty_method(CLUSTER_EMPTY_EXCEPTION)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::ClusterCenterBasedBase(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method) :
        ClusterBase<T, TMODE, TDISTANCE, N>(number_of_clusters, number_of_dimensions),
        m_cluster_centers((number_of_clusters != 0)?new VectorDense<TMODE, N>[number_of_clusters]:0),
        m_seeder((seeder != 0)?seeder->duplicate():0),
        m_distance_object(distance_object),
        m_maximum_number_of_iterations(maximum_number_of_iterations),
        m_cutoff(cutoff),
        m_cluster_empty_method(cluster_empty_method)
    {
        for (unsigned int i = 0; i < number_of_clusters; ++i)
            m_cluster_centers[i].set((N)number_of_dimensions, 0);
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::ClusterCenterBasedBase(const ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> &other) :
        ClusterBase<T, TMODE, TDISTANCE, N>(other),
        m_cluster_centers((other.m_number_of_clusters != 0)?new VectorDense<TMODE, N>[other.m_number_of_clusters]:0),
        m_seeder((other.m_seeder != 0)?other.m_seeder->duplicate():0),
        m_distance_object(other.m_distance_object),
        m_maximum_number_of_iterations(other.m_maximum_number_of_iterations),
        m_cutoff(other.m_cutoff),
        m_cluster_empty_method(other.m_cluster_empty_method)
    {
        for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
            m_cluster_centers[i] = other.m_cluster_centers[i];
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::~ClusterCenterBasedBase(void)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        if (m_seeder != 0) delete m_seeder;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>& ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::operator=(const ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> &other)
    {
        if (this != &other)
        {
            ClusterBase<T, TMODE, TDISTANCE, N>::operator=(other);
            
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_cluster_centers != 0) delete [] m_cluster_centers;
            if (m_seeder != 0) delete m_seeder;
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            if (other.m_number_of_clusters > 0)
            {
                m_cluster_centers = new VectorDense<TMODE, N>[other.m_number_of_clusters];
                for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
                    m_cluster_centers[i] = other.m_cluster_centers[i];
            }
            else m_cluster_centers = 0;
            if (other.m_seeder != 0) m_seeder = other.m_seeder->duplicate();
            else m_seeder = 0;
            
            m_distance_object = other.m_distance_object;
            m_maximum_number_of_iterations = other.m_maximum_number_of_iterations;
            m_cutoff = other.m_cutoff;
            m_cluster_empty_method = other.m_cluster_empty_method;
        }
        
        return *this;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::setNumberOfCluster(unsigned int number_of_clusters)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        this->m_number_of_clusters = number_of_clusters;
        if (number_of_clusters > 0)
        {
            m_cluster_centers = new VectorDense<TMODE, N>[number_of_clusters];
            for (unsigned int i = 0; i < number_of_clusters; ++i)
                m_cluster_centers[i].set(this->m_number_of_dimensions, 0);
        }
        else m_cluster_centers = 0;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::setClusterCentroids(const ConstantSubVectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        if (logger != 0)
            logger->log("Setting the centroids of %d clusters.", number_of_clusters);
        this->m_number_of_clusters = number_of_clusters;
        if (number_of_clusters > 0)
        {
            m_cluster_centers = new VectorDense<TMODE, N>[number_of_clusters];
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_clusters; i += number_of_threads)
                {
                    m_cluster_centers[i].set((N)this->m_number_of_dimensions, 0);
                    m_cluster_centers[i].copy(cluster_centers[i]);
                }
            }
        }
        else m_cluster_centers = 0;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::setCenterBasedBase(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        if (m_seeder != 0) delete m_seeder;
        
        // -[ Set ]----------------------------------------------------------------------------------------------------------------------------------
        this->setBase(number_of_clusters, number_of_dimensions);
        
        if (number_of_clusters > 0)
        {
            m_cluster_centers = new VectorDense<TMODE, N>[number_of_clusters];
            for (unsigned int i = 0; i < number_of_clusters; ++i)
                m_cluster_centers[i].set(number_of_dimensions, 0);
        }
        else m_cluster_centers = 0;
        
        m_seeder = (seeder != 0)?seeder->duplicate():0;
        m_distance_object = distance_object;
        m_maximum_number_of_iterations = maximum_number_of_iterations;
        m_cutoff = cutoff;
        m_cluster_empty_method = cluster_empty_method;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::inner_cluster(VECTOR<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            TDISTANCE minimum_distance, current_distance;
            unsigned int selected_cluster;
            
            for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
            {
                m_distance_object.distance(*data[i], this->m_cluster_centers[0], minimum_distance);
                selected_cluster = 0;
                for (unsigned int j = 1; j < this->m_number_of_clusters; ++j)
                {
                    m_distance_object.distance(*data[i], this->m_cluster_centers[j], current_distance);
                    if (current_distance < minimum_distance)
                    {
                        minimum_distance = current_distance;
                        selected_cluster = j;
                    }
                }
                data_clusters[i].set(1);
                data_clusters[i][0].setData(1.0f, selected_cluster);
                if ((logger != 0) && (thread_id == 0) && ((i / number_of_threads) % 10000 == 0))
                    logger->log("Clustered sample %d of %d.", i + 1, number_of_elements);
            }
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::inner_update(VECTOR<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger)
    {
        if ((update_information.size() > 0) && (this->m_number_of_clusters > 0))
        {
            VectorDense<unsigned int> data_cluster_index(number_of_elements), cluster_histogram(this->m_number_of_clusters, 0);
            unsigned int number_of_removed_clusters, number_of_added_clusters, new_number_of_clusters;
            unsigned int index_current_center, index_new_center;
            VectorDense<TMODE, N> * new_cluster_centers;
            VectorSparse<unsigned int, unsigned int> sorted_information(update_information.size());
            
            
            // 1) Calculate the number of clusters added and removed.
            number_of_removed_clusters = 0;
            number_of_added_clusters = 0;
            for (unsigned int i = 0; i < update_information.size(); ++i)
            {
                if (update_information[i].getFirst() == 0) ++number_of_removed_clusters;
                else if (update_information[i].getFirst() > 1) number_of_added_clusters += update_information[i].getFirst() - 1;
            }
            
            // 2) Create the new clusters structures.
            if ((number_of_removed_clusters == 0) && (number_of_added_clusters == 0)) return;
            new_number_of_clusters = this->m_number_of_clusters - number_of_removed_clusters + number_of_added_clusters;
            new_cluster_centers = new VectorDense<TMODE, N>[new_number_of_clusters];
            for (unsigned int i = 0; i  < new_number_of_clusters; ++i)
                new_cluster_centers[i].set((N)this->m_number_of_dimensions);
            
            if (logger != 0) logger->log("Updating clusters: adding %d clusters and removing %d clusters. Calculating the cluster of each given data vector...", number_of_added_clusters, number_of_removed_clusters);
            // 3) Calculate to which clusters the data points belong to.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                TDISTANCE minimum_distance, current_distance;
                unsigned int selected_cluster;
                
                for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
                {
                    m_distance_object.distance(*data[i], this->m_cluster_centers[0], minimum_distance);
                    selected_cluster = 0;
                    for (unsigned int j = 1; j < this->m_number_of_clusters; ++j)
                    {
                        m_distance_object.distance(*data[i], this->m_cluster_centers[j], current_distance);
                        if (current_distance < minimum_distance)
                        {
                            minimum_distance = current_distance;
                            selected_cluster = j;
                        }
                    }
                    data_cluster_index[i] = selected_cluster;
                    if ((logger != 0) && (thread_id == 0) && ((i / number_of_threads) % 10000 == 0))
                        logger->log("Clustered sample %d of %d.", i + 1, number_of_elements);
                }
            }
            for (unsigned int i = 0; i < number_of_elements; ++i)
                ++cluster_histogram[data_cluster_index[i]];
            
            // 4) Update the number of changes for each cluster.
            for (unsigned int i = 0; i < update_information.size(); ++i)
                sorted_information[i].setData(update_information[i].getSecond(), update_information[i].getFirst());
            std::sort(&sorted_information[0], &sorted_information[sorted_information.size()]);
            if (logger != 0)
                logger->log("Updating the cluster centers...");
            index_current_center = index_new_center = 0;
            for (unsigned int i = 0; i < sorted_information.size(); ++i, ++index_current_center)
            {
                const unsigned int current_index = sorted_information[i].getFirst();
                const unsigned int partitions = sorted_information[i].getSecond();
                for (; index_current_center < current_index; ++index_current_center, ++index_new_center)
                    new_cluster_centers[index_new_center].copy(this->m_cluster_centers[index_current_center]);
                if (partitions == 1)
                {
                    new_cluster_centers[index_new_center].copy(this->m_cluster_centers[index_current_center]);
                    ++index_new_center;
                }
                else if (partitions > 1)
                {
                    VectorDense<const typename ConstantVectorType<VECTOR, T, N>::Type *> sub_data(cluster_histogram[current_index]);
                    unsigned int n;
                    
                    n = 0;
                    for (unsigned int m = 0; m < number_of_elements; ++m)
                    {
                        if (data_cluster_index[m] == current_index)
                        {
                            sub_data[n] = data[m];
                            ++n;
                        }
                    }
                    this->m_seeder->sow(sub_data.getData(), sub_data.size(), &new_cluster_centers[index_new_center], partitions, number_of_threads, logger);
                    this->train(sub_data.getData(), sub_data.size(), &new_cluster_centers[index_new_center], partitions, number_of_threads, logger);
                    index_new_center += partitions;
                }
            }
            for (; index_current_center < this->m_number_of_clusters; ++index_current_center, ++index_new_center)
                new_cluster_centers[index_new_center].copy(this->m_cluster_centers[index_current_center]);
            
            // 5) Call the clustering training algorithm with the modified cluster centers.
            if (logger != 0)
                logger->log("Calling the training function with the new clusters.");
            delete [] this->m_cluster_centers;
            this->m_cluster_centers = new_cluster_centers;
            this->m_number_of_clusters = new_number_of_clusters;
            if (global_clustering)
                this->train(data, number_of_elements, this->m_cluster_centers, this->m_number_of_clusters, number_of_threads, logger);
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::inner_nearest(VECTOR<T, N> const * const * data, VectorSparse<TDISTANCE, unsigned int> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            Heap<Tuple<TDISTANCE, unsigned int>, SrvCompareMax<Tuple<TDISTANCE, unsigned int> > > distance_heap(number_of_neighbors);
            TDISTANCE current_distance;
            
            for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
            {
                if (data[i]->size() > 0)
                {
                    distance_heap.clear();
                    for (unsigned int j = 0; j < this->m_number_of_clusters; ++j)
                    {
                        m_distance_object.distance(*data[i], this->m_cluster_centers[j], current_distance);
                        if (distance_heap.isFull())
                        {
                            if (current_distance < distance_heap.peek().getFirst())
                            {
                                distance_heap.remove();
                                distance_heap.insert(Tuple<TDISTANCE, unsigned int>(current_distance, j));
                            }
                        }
                        else distance_heap.insert(Tuple<TDISTANCE, unsigned int>(current_distance, j));
                    }
                    
                    distances[i].set(distance_heap.size());
                    for (unsigned int j = distance_heap.size() - 1; !distance_heap.isEmpty(); --j)
                    {
                        distances[i][j] = distance_heap.peek();
                        distance_heap.remove();
                    }
                }
                else distances[i].set(0);
                
                if ((logger != 0) && (thread_id == 0) && ((i / number_of_threads) % 10000 == 0))
                    logger->log("Calculated the k-Nearest neighbors of the sample %d of %d.", i + 1, number_of_elements);
            }
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Cluster_Object");
        parser.setAttribute("Identifier", (int)this->getIdentifier());
        parser.setAttribute("Number_Of_Clusters", this->m_number_of_clusters);
        parser.setAttribute("Number_Of_Dimensions", this->m_number_of_dimensions);
        parser.setAttribute("Cutoff", m_cutoff);
        parser.setAttribute("cluster_empty_method", (int)m_cluster_empty_method);
        parser.setAttribute("Number_Of_Iterations", m_maximum_number_of_iterations);
        convertToAttributesXML(parser);
        parser.addChildren();
        if (this->m_seeder != 0)
            this->m_seeder->convertToXML(parser);
        m_distance_object.convertToXML(parser);
        for (unsigned int i = 0; i < this->m_number_of_clusters; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            saveVector(parser, "Cluster_Center", this->m_cluster_centers[i]);
        }
        parser.closeTag();
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Cluster_Object") && ((CLUSTER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getIdentifier()))
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (this->m_seeder != 0) { delete this->m_seeder; this->m_seeder = 0; }
            if (this->m_cluster_centers != 0) { delete [] this->m_cluster_centers; this->m_cluster_centers = 0; }
            
            // -[ Retrieve ]-------------------------------------------------------------------------------------------------------------------------
            this->m_number_of_clusters = parser.getAttribute("Number_Of_Clusters");
            this->m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            this->m_cluster_information_trace_enabled = false;
            m_cutoff = parser.getAttribute("Cutoff");
            m_cluster_empty_method = (CLUSTER_EMPTY_METHOD)((int)parser.getAttribute("cluster_empty_method"));
            m_maximum_number_of_iterations = parser.getAttribute("Number_Of_Iterations");
            convertFromAttributesXML(parser);
            
            if (this->m_number_of_clusters > 0)
                this->m_cluster_centers = new VectorDense<TMODE, N>[this->m_number_of_clusters];
            
            while (!(parser.isTagIdentifier("Cluster_Object") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Cluster_Center"))
                {
                    unsigned int index;
                    
                    index = parser.getAttribute("ID");
                    loadVector(parser, "Cluster_Center", this->m_cluster_centers[index]);
                }
                else if (parser.isTagIdentifier("Seeder"))
                {
                    this->m_seeder = SeederFactory<T, TMODE, TDISTANCE, N>::Type::getInstantiator((SEEDER_IDENTIFIER)((int)parser.getAttribute("Identifier")));
                    this->m_seeder->convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("Vector_Distance"))
                    m_distance_object.convertFromXML(parser);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::addTraceInformation(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters)
    {
        CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N> * information;
        
        information = new CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>(iteration, cluster_centers, number_of_clusters, this->m_distance_object);
        this->m_cluster_information_trace_list = new ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * >(information, this->m_cluster_information_trace_list);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CENTER-BASED CLUSTER INFORMATION     |
    //                   | TRACE CLASS                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Container class which stores the centers of the clusters at a certain iteration of the clustering algorithm.
     *  This class is used to store information about the cluster centers calculated at different iterations
     *  of a center based clustering algorithm.
     */
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class CenterBasedClusterInformationTrace : public ClusterInformationTrace<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CenterBasedClusterInformationTrace(void);
        /** Constructor which initializes the clustering information at the current iteration of the algorithm.
         *  \param[in] iteration iteration of the clustering algorithm.
         *  \param[in] cluster_centers array with the centroids of each cluster.
         *  \param[in] number_of_clusters number of elements in the clusters array.
         */
        CenterBasedClusterInformationTrace(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, const VectorDistance &distance_object);
        /// Copy constructor.
        CenterBasedClusterInformationTrace(const CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N> &other);
        /// Destructor.
        virtual ~CenterBasedClusterInformationTrace(void);
        /// Assignation operator.
        CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>& operator=(const CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N> &other);
        /// Returns a copy of the cluster information trace object (virtual copy constructor).
        ClusterInformationTrace<T, TMODE, TDISTANCE, N> * duplicate(void) const { return (ClusterInformationTrace<T, TMODE, TDISTANCE, N> *)new CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>(*this); }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Sets the information of the clustering algorithm at the given iteration.
         *  \param[in] iteration iteration of the clustering algorithm.
         *  \param[in] cluster_centers array with the centroids of each cluster.
         *  \param[in] number_of_clusters number of elements in the clusters array.
         */
        void set(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_centers, const VectorDistance &distance_object);
        
        /// Sets the information about the cluster centroids at the current iteration.
        void setClusters(const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters);
        /// Returns a constant pointer to the array with the centroids of the clusters.
        inline const VectorDense<TMODE, N> * getClusters(void) const { return m_cluster_centers; }
        /// Returns a constant reference to the index-th cluster centroid.
        inline const VectorDense<TMODE, N>& getCluster(unsigned int index) const { return m_cluster_centers[index]; }
        /// Returns the number of clusters.
        inline unsigned int getNumberOfClusters(void) const { return m_number_of_clusters; }
        
        /// Returns the metric used to calculate the distance between the data vectors and the centroids of the clusters.
        inline const VectorDistance& getDistanceObject(void) const { return m_distance_object; }
        /// Sets the metric used to calculate the distance between the data vectors and the centroids of the clusters.
        inline void setDistanceObject(const VectorDistance &distance_object) { m_distance_object = distance_object; }
        
        // -[ Clustering functions ]-----------------------------------------------------------------------------------------------------------------
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant dense sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        void cluster(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        void cluster(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the cluster creation information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the cluster creation information from a XML object.
        void convertFromXML(XmlParser &parser);
        
    private:
        /// Array with the centroids of the clusters.
        VectorDense<TMODE, N> * m_cluster_centers;
        /// Number of clusters.
        unsigned int m_number_of_clusters;
        /// Metric used to calculate the distance between the data vectors and the centroids of the clusters.
        VectorDistance m_distance_object;
    };
    
    template <class T, class TMODE, class TDISTANCE, class N>
    CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::CenterBasedClusterInformationTrace(void) :
        ClusterInformationTrace<T, TMODE, TDISTANCE, N>(),
        m_cluster_centers(0),
        m_number_of_clusters(0),
        m_distance_object(EUCLIDEAN_DISTANCE)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::CenterBasedClusterInformationTrace(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, const VectorDistance &distance_object) :
        ClusterInformationTrace<T, TMODE, TDISTANCE, N>(iteration),
        m_cluster_centers((number_of_clusters > 0)?new VectorDense<TMODE, N>[number_of_clusters]:0),
        m_number_of_clusters(number_of_clusters),
        m_distance_object(distance_object)
    {
        for (unsigned int i = 0; i < number_of_clusters; ++i)
            m_cluster_centers[i] = cluster_centers[i];
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::CenterBasedClusterInformationTrace(const CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N> &other) :
        ClusterInformationTrace<T, TMODE, TDISTANCE, N>(other),
        m_cluster_centers((other.m_number_of_clusters > 0)?new VectorDense<TMODE, N>[other.m_number_of_clusters]:0),
        m_number_of_clusters(other.m_number_of_clusters),
        m_distance_object(other.m_distance_object)
    {
        for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
            m_cluster_centers[i] = other.m_cluster_centers[i];
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::~CenterBasedClusterInformationTrace(void)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>& CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::operator=(const CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N> &other)
    {
        if (this != &other)
        {
            ClusterInformationTrace<T, TMODE, TDISTANCE, N>::operator=(other);
            
            if (m_cluster_centers != 0) delete [] m_cluster_centers;
            
            if (other.m_number_of_clusters > 0)
            {
                m_cluster_centers = new VectorDense<TMODE, N>[other.m_number_of_clusters];
                m_number_of_clusters = other.m_number_of_clusters;
                for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
                    m_cluster_centers[i] = other.m_cluster_centers[i];
            }
            else
            {
                m_cluster_centers = 0;
                m_number_of_clusters = 0;
            }
            m_distance_object = other.m_distance_object;
        }
        
        return *this;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::set(unsigned int iteration, const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, const VectorDistance &distance_object)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        
        this->m_iteration = iteration;
        if (number_of_clusters > 0)
        {
            m_cluster_centers = new VectorDense<TMODE, N>[number_of_clusters];
            m_number_of_clusters = number_of_clusters;
            for (unsigned int i = 0; i < number_of_clusters; ++i)
                m_cluster_centers[i] = cluster_centers[i];
        }
        else
        {
            m_cluster_centers = 0;
            m_number_of_clusters = 0;
        }
        m_distance_object = distance_object;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::setClusters(const VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters)
    {
        if (m_cluster_centers != 0) delete [] m_cluster_centers;
        if (number_of_clusters > 0)
        {
            m_cluster_centers = new VectorDense<TMODE, N>[number_of_clusters];
            m_number_of_clusters = number_of_clusters;
            for (unsigned int i = 0; i < number_of_clusters; ++i)
                m_cluster_centers[i] = cluster_centers[i];
        }
        else
        {
            m_cluster_centers = 0;
            m_number_of_clusters = 0;
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::cluster(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            TDISTANCE minimum_distance, current_distance;
            unsigned int selected_cluster;
            
            for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
            {
                m_distance_object.distance(*data[i], this->m_cluster_centers[0], minimum_distance);
                selected_cluster = 0;
                for (unsigned int j = 1; j < this->m_number_of_clusters; ++j)
                {
                    m_distance_object.distance(*data[i], this->m_cluster_centers[j], current_distance);
                    if (current_distance < minimum_distance)
                    {
                        minimum_distance = current_distance;
                        selected_cluster = j;
                    }
                }
                data_clusters[i].set(1);
                data_clusters[i][0].setData(1.0f, selected_cluster);
                if ((logger != 0) && (thread_id == 0) && ((i / number_of_threads) % 10000 == 0))
                    logger->log("Clustered sample %d of %d.", i + 1, number_of_elements);
            }
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::cluster(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger) const
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_id = omp_get_thread_num();
            TDISTANCE minimum_distance, current_distance;
            unsigned int selected_cluster;
            
            for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
            {
                m_distance_object.distance(*data[i], this->m_cluster_centers[0], minimum_distance);
                selected_cluster = 0;
                for (unsigned int j = 1; j < this->m_number_of_clusters; ++j)
                {
                    m_distance_object.distance(*data[i], this->m_cluster_centers[j], current_distance);
                    if (current_distance < minimum_distance)
                    {
                        minimum_distance = current_distance;
                        selected_cluster = j;
                    }
                }
                data_clusters[i].set(1);
                data_clusters[i][0].setData(1.0f, selected_cluster);
                if ((logger != 0) && (thread_id == 0) && ((i / number_of_threads) % 10000 == 0))
                    logger->log("Clustered sample %d of %d.", i + 1, number_of_elements);
            }
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Cluster_Trace_Information");
        parser.setAttribute("Iteration", this->m_iteration);
        parser.setAttribute("Number_Of_Clusters", m_number_of_clusters);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            saveVector(parser, "Centroid", m_cluster_centers[i]);
        }
        parser.closeTag();
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void CenterBasedClusterInformationTrace<T, TMODE, TDISTANCE, N>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Cluster_Trace_Information"))
        {
            if (m_cluster_centers != 0) delete [] m_cluster_centers;
            
            this->m_iteration = parser.getAttribute("Iteration");
            m_number_of_clusters = parser.getAttribute("Number_Of_Clusters");
            if (m_number_of_clusters > 0) m_cluster_centers = new VectorDense<TMODE, N>[m_number_of_clusters];
            else m_cluster_centers = 0;
            while (!(parser.isTagIdentifier("Cluster_Trace_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Centroid"))
                {
                    unsigned int index;
                    
                    index = parser.getAttribute("ID");
                    loadVector(parser, "Centroid", m_cluster_centers[index]);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

