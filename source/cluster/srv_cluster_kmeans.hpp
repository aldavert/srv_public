// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_KMEANS_HPP_HEADER_FILE__
#define __SRV_CLUSTER_KMEANS_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
#include <algorithm>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "srv_cluster_center_based_base.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS DECLARATION |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class ClusterKMeans : public ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClusterKMeans(void);
        /** Constructor which initializes all the parameters of the k-means cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the partitions of the feature space.
         */
        ClusterKMeans(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method);
        /// Copy constructor.
        ClusterKMeans(const ClusterKMeans<T, TMODE, TDISTANCE, N> &other);
        /// Destructor.
        virtual ~ClusterKMeans(void);
        /// Assignation operator.
        ClusterKMeans<T, TMODE, TDISTANCE, N>& operator=(const ClusterKMeans<T, TMODE, TDISTANCE, N> &other);
        /** Initializes the parameters of the k-means cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the partitions of the feature space.
         */
        inline void set(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method) { this->setCenterBasedBase(number_of_clusters, number_of_dimensions, seeder, distance_object, maximum_number_of_iterations, cutoff, cluster_empty_method); }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the cluster object (virtual copy constructor).
        inline ClusterBase<T, TMODE, TDISTANCE, N> * duplicate(void) const { return (ClusterBase<T, TMODE, TDISTANCE, N> *)new ClusterKMeans<T, TMODE, TDISTANCE, N>(*this); }
        /// Returns the class identifier for the current cluster method.
        inline static CLUSTER_IDENTIFIER getClassIdentifier(void) { return KMEANS_CLUSTER; }
        /// Generates a new empty instance of the cluster class.
        inline static ClusterBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (ClusterBase<T, TMODE, TDISTANCE, N> *)new ClusterKMeans(); }
        /// Returns a flag which states if the cluster class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the cluster type identifier of the current object.
        inline CLUSTER_IDENTIFIER getIdentifier(void) const { return KMEANS_CLUSTER; }
        
    protected:
        /** Function which implements the k-means clustering algorithm.
         *  \param[in] data array of pointers to the constant dense sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void train(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            inner_train(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
        }
        /** Function which implements the k-means clustering algorithm.
         *  \param[in] data array of pointers to the constant sparse sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void train(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            inner_train(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
        }
        
        /** Template function which implements the clustering algorithm for both the sparse and the dense vectors.
         *  \param[in] data array of pointers to the constant sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void inner_train(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger);
        
        /// Function which stores the attributes of the derived class.
        inline void convertToAttributesXML(XmlParser &/* parser */) const { }
        /// Function which restores the attributes of the derived class.
        inline void convertFromAttributesXML(XmlParser &/* parser */) { }
        
        // Integer flag which is greater than zero when the cluster object has been initialized in a cluster factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS             |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeans<T, TMODE, TDISTANCE, N>::ClusterKMeans(void) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>()
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeans<T, TMODE, TDISTANCE, N>::ClusterKMeans(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>(number_of_clusters, number_of_dimensions, seeder, distance_object, maximum_number_of_iterations, cutoff, cluster_empty_method)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeans<T, TMODE, TDISTANCE, N>::ClusterKMeans(const ClusterKMeans<T, TMODE, TDISTANCE, N> &other) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>(other)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeans<T, TMODE, TDISTANCE, N>::~ClusterKMeans(void)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeans<T, TMODE, TDISTANCE, N>& ClusterKMeans<T, TMODE, TDISTANCE, N>::operator=(const ClusterKMeans<T, TMODE, TDISTANCE, N> &other)
    {
        if (this != &other)
        {
            ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::operator=(other);
        }
        
        return *this;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterKMeans<T, TMODE, TDISTANCE, N>::inner_train(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger)
    {
        if ((number_of_elements >= number_of_clusters) && (number_of_clusters > 0) && (this->m_seeder != 0))
        {
            VectorDense<TMODE, N> * * update_centers;
            VectorDense<unsigned int> * cluster_histogram;
            TDISTANCE maximum_cluster_distance, current_cluster_distance;
            unsigned int number_of_resample_clusters, * cluster_selected;
            TDISTANCE * cluster_distance;
            
            // Create structures ....................................................................................................................
            if (logger != 0)
                logger->log("Creating the structures for standard k-Means algorithm.");
            update_centers = new VectorDense<TMODE, N> * [number_of_threads];
            cluster_histogram = new VectorDense<unsigned int>[number_of_threads];
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
            {
                cluster_histogram[thread].set(number_of_clusters);
                update_centers[thread] = new VectorDense<TMODE, N>[number_of_clusters];
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    update_centers[thread][i].set(this->m_number_of_dimensions);
            }
            if (this->m_cluster_information_trace_enabled)
                this->addTraceInformation(0, cluster_centers, number_of_clusters);
            cluster_selected = new unsigned int[number_of_elements];
            cluster_distance = new TDISTANCE[number_of_elements];
            
            for (unsigned int iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    // 1) Re-initialize the pooling structures to zero ..............................................................................
                    cluster_histogram[thread_id].setValue(0);
                    for (unsigned int i = 0; i < number_of_clusters; ++i)
                        update_centers[thread_id][i].setValue(0);
                    
                    // 2) Assign each data vector to the closest cluster centroid and accumulate it .................................................
                    for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
                    {
                        TDISTANCE minimum_distance, current_distance;
                        unsigned int selected_cluster;
                        
                        this->m_distance_object.distance(*data[i], cluster_centers[0], minimum_distance);
                        selected_cluster = 0;
                        for (unsigned int j = 1; j < number_of_clusters; ++j)
                        {
                            this->m_distance_object.distance(*data[i], cluster_centers[j], current_distance);
                            
                            if (current_distance < minimum_distance)
                            {
                                minimum_distance = current_distance;
                                selected_cluster = j;
                            }
                        }
                        
                        cluster_selected[i] = selected_cluster;
                        cluster_distance[i] = minimum_distance;
                        update_centers[thread_id][selected_cluster] += *data[i];
                        ++cluster_histogram[thread_id][selected_cluster];
                    }
                }
                
                // 3) Merge the different threads ...................................................................................................
                for (unsigned int thread = 1; thread < number_of_threads; ++thread)
                {
                    cluster_histogram[0] += cluster_histogram[thread];
                    for (unsigned int i = 0; i < number_of_clusters; ++i)
                        update_centers[0][i] += update_centers[thread][i];
                }
                
                // 4) Calculate the maximum distance between the new and old clusters ...............................................................
                maximum_cluster_distance = 0;
                number_of_resample_clusters = 0;
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                {
                    if (cluster_histogram[0][i] == 0)
                    {
                        if (this->m_cluster_empty_method == CLUSTER_EMPTY_EXCEPTION)
                            throw Exception("During the iterative k-means algorithm a cluster has no vector assigned.");
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_IGNORE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty - Ignoring.", i);
                            update_centers[0][i].copy(cluster_centers[i]);
                        }
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_RESAMPLE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty.", i);
                            maximum_cluster_distance = srvMax<TDISTANCE>(this->m_cutoff * 2 + 1, maximum_cluster_distance);
                            ++number_of_resample_clusters;
                        }
                    }
                    else
                    {
                        update_centers[0][i] /= (TMODE)cluster_histogram[0][i];
                        this->m_distance_object.distance(cluster_centers[i], update_centers[0][i], current_cluster_distance);
                        if (current_cluster_distance > maximum_cluster_distance)
                            maximum_cluster_distance = current_cluster_distance;
                    }
                }
                if (number_of_resample_clusters > 0)
                {
                    VectorDense<Tuple<TDISTANCE, unsigned int> > resample_indexes;
                    VectorDense<unsigned int> resample_histogram(number_of_clusters, 0), clusters_to_resample(number_of_resample_clusters);
                    unsigned int resample_selected, resample_size;
                    TDISTANCE current_distance;
                    
                    if (logger != 0) logger->log("[WARNING] Re-sampling %d clusters.", number_of_resample_clusters);
                    // Calculate the actual distances between the clusters and the centroids.
                    for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        this->m_distance_object.distance(*data[rk], update_centers[0][cluster_selected[rk]], cluster_distance[rk]);
                    number_of_resample_clusters = 0;
                    for (unsigned int rk = 0; rk < number_of_clusters; ++rk)
                    {
                        if (cluster_histogram[0][rk] == 0)
                        {
                            clusters_to_resample[number_of_resample_clusters] = rk;
                            ++number_of_resample_clusters;
                        }
                    }
                    
                    for (unsigned int m = 0; m < number_of_resample_clusters; ++m)
                    {
                        // Calculate the histogram of each cluster.
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                            ++resample_histogram[cluster_selected[rk]];
                        // Select the cluster with more samples.
                        resample_selected = 0;
                        resample_size = resample_histogram[0];
                        for (unsigned int rk = 1; rk < number_of_clusters; ++rk)
                        {
                            if (resample_histogram[rk] > resample_size)
                            {
                                resample_selected = rk;
                                resample_size = resample_histogram[rk];
                            }
                        }
                        // Get the indexes and the distance of the samples to the selected cluster.
                        resample_indexes.set(resample_size);
                        resample_size = 0;
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            if (cluster_selected[rk] == resample_selected)
                            {
                                resample_indexes[resample_size].setData(cluster_distance[rk], rk);
                                ++resample_size;
                            }
                        }
                        std::sort(&resample_indexes[0], &resample_indexes[resample_size]);
                        
                        update_centers[0][clusters_to_resample[m]].copy(*data[resample_indexes[resample_size / 2 + rand() % (resample_size / 2)].getSecond()]);
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            this->m_distance_object.distance(*data[rk], update_centers[0][clusters_to_resample[m]], current_distance);
                            if (current_distance < cluster_distance[rk])
                            {
                                cluster_distance[rk] = current_distance;
                                cluster_selected[rk] = clusters_to_resample[m];
                            }
                        }
                    }
                }
                
                if (logger != 0)
                    logger->log("Iteration %06d of %06d of the k-means algorithm. Maximum update distance is %f.", iteration + 1, this->m_maximum_number_of_iterations, (double)maximum_cluster_distance);
                
                // 5) Check the cutoff threshold ....................................................................................................
                if (maximum_cluster_distance <= this->m_cutoff)
                    break;
                
                // 6) Set the new cluster centers as the current cluster centers ....................................................................
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    cluster_centers[i].copy(update_centers[0][i]);
                
                if (this->m_cluster_information_trace_enabled)
                    this->addTraceInformation(iteration + 1, cluster_centers, number_of_clusters);
            }
            
            // Free allocated memory ................................................................................................................
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
                delete [] update_centers[thread];
            delete [] update_centers;
            delete [] cluster_histogram;
            delete [] cluster_selected;
            delete [] cluster_distance;
        }
        else if (logger != 0)
        {
            if (number_of_elements < number_of_clusters) logger->log("Not enough samples (%d) to create %d clusters.", number_of_elements, number_of_clusters);
            if (number_of_clusters == 0) logger->log("The number of clusters is set to zero.");
            if (this->m_seeder == 0) logger->log("The k-means algorithms requires a seeder in order to create the initial set of centers.");
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

