// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_KMEANS_FAST_HPP_HEADER_FILE__
#define __SRV_CLUSTER_KMEANS_FAST_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
#include <algorithm>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "srv_cluster_center_based_base.hpp"
#include "srv_triangle_inequality.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS DECLARATION |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Implements the k-means algorithm proposed by Charles Elkan which uses the Triangle Inequality to
     *  accelerate the original Floyd's k-Means algorithm.
     *  
     *  C. Elkan, "Using the Triangle Inequality to Accelerate k-Means", ICML 2003.
     */
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class ClusterKMeansFast : public ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClusterKMeansFast(void);
        /** Constructor which initializes all the parameters of the k-means cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the partitions of the feature space.
         *  \param[in] save_memory boolean flag which enables or disables the memory economic version of the algorithm which trades memory for computational cost.
         *  \param[in] number_of_cluster_neighbors number of nearest neighbors calculated for each cluster in the economic version of the algorithm.
         */
        ClusterKMeansFast(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method, bool save_memory, unsigned int number_of_cluster_neighbors);
        /// Copy constructor.
        ClusterKMeansFast(const ClusterKMeansFast<T, TMODE, TDISTANCE, N> &other);
        /// Destructor.
        virtual ~ClusterKMeansFast(void);
        /// Assignation operator.
        ClusterKMeansFast<T, TMODE, TDISTANCE, N>& operator=(const ClusterKMeansFast<T, TMODE, TDISTANCE, N> &other);
        /** Initializes the parameters of the k-means cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         *  \param[in] seeder object used to create the initial points of the clustering algorithm.
         *  \param[in] distance_object object used to calculate the distance between the samples and the cluster centers.
         *  \param[in] maximum_number_of_iterations maximum number of iterations of the algorithm.
         *  \param[in] cutoff distance threshold of the maximum cluster update used to stop the iterative clustering algorithm.
         *  \param[in] cluster_empty_method method used to deal with empty clusters while creating the partitions of the feature space.
         *  \param[in] save_memory boolean flag which enables or disables the memory economic version of the algorithm which trades memory for computational cost.
         *  \param[in] number_of_cluster_neighbors number of nearest neighbors calculated for each cluster in the economic version of the algorithm.
         */
        inline void set(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method, bool save_memory, unsigned int number_of_cluster_neighbors)
        {
            this->setCenterBasedBase(number_of_clusters, number_of_dimensions, seeder, distance_object, maximum_number_of_iterations, cutoff, cluster_empty_method);
            m_save_memory = save_memory;
            m_number_of_cluster_neighbors = number_of_cluster_neighbors;
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which enables or disables the memory economic version of the algorithm.
        inline bool getSaveMemory(void) const { return m_save_memory; }
        /// Sets the boolean flag which enables or disables the memory economic version of the algorithm.
        inline void setSaveMemory(bool save_memory) { m_save_memory = save_memory; }
        /// Returns the number of nearest neighbors calculated for each cluster in the economic version of the algorithm.
        inline unsigned int getNumberOfClusterNeighbors(void) const { return m_number_of_cluster_neighbors; }
        /// Sets the number of nearest neighbors calculated for each cluster in the economic version of the algorithm.
        inline void setNumberOfClusterNeighbors(unsigned int number_of_cluster_neighbors) { m_number_of_cluster_neighbors = number_of_cluster_neighbors; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the cluster object (virtual copy constructor).
        inline ClusterBase<T, TMODE, TDISTANCE, N> * duplicate(void) const { return (ClusterBase<T, TMODE, TDISTANCE, N> *)new ClusterKMeansFast<T, TMODE, TDISTANCE, N>(*this); }
        /// Returns the class identifier for the current cluster method.
        inline static CLUSTER_IDENTIFIER getClassIdentifier(void) { return KMEANS_FAST_CLUSTER; }
        /// Generates a new empty instance of the cluster class.
        inline static ClusterBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (ClusterBase<T, TMODE, TDISTANCE, N> *)new ClusterKMeansFast(); }
        /// Returns a flag which states if the cluster class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the cluster type identifier of the current object.
        inline CLUSTER_IDENTIFIER getIdentifier(void) const { return KMEANS_FAST_CLUSTER; }
        
    protected:
        /** Function which implements the k-means clustering algorithm.
         *  \param[in] data array of pointers to the constant dense sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        void train(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            if (m_save_memory) protectedTrainMemory(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
            else protectedTrain(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
        }
        /** Function which implements the k-means clustering algorithm.
         *  \param[in] data array of pointers to the constant sparse sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        void train(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            if (m_save_memory) protectedTrainMemory(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
            else protectedTrain(data, number_of_elements, cluster_centers, number_of_clusters, number_of_threads, logger);
        }
        
        /** Template function which implements the clustering algorithm for both the sparse and the dense vectors.
         *  \param[in] data array of pointers to the constant sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void protectedTrain(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger);
        
        /** Template function which implements the memory economic version of the clustering algorithm for both the sparse and the dense vectors.
         *  \param[in] data array of pointers to the constant sub-vectors used by the clustering algorithm.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in,out] cluster_centers array with the centroids of the resulting clusters. Initially, it contains the seeds of the clusters.
         *  \param[in] number_of_clusters number of clusters calculated.
         *  \param[in] number_of_threads number of threads used to concurrently cluster the data vectors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void protectedTrainMemory(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger);
        
        /// Function which stores the attributes of the derived class.
        inline void convertToAttributesXML(XmlParser &parser) const
        {
            parser.setAttribute("Save_Memory", m_save_memory);
            parser.setAttribute("Number_Of_Cluster_Neighbors", m_number_of_cluster_neighbors);
        }
        /// Function which restores the attributes of the derived class.
        inline void convertFromAttributesXML(XmlParser &parser)
        {
            m_save_memory = parser.getAttribute("Save_Memory");
            m_number_of_cluster_neighbors = parser.getAttribute("Number_Of_Cluster_Neighbors");
        }
        
        /// Boolean flag which enables or disables the memory economic version of the algorithm.
        bool m_save_memory;
        /// Number of nearest neighbors calculated for each cluster in the economic version of the algorithm.
        unsigned int m_number_of_cluster_neighbors;
        
        /// Integer flag which is greater than zero when the cluster object has been initialized in a cluster factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS             |
    //                   | CONSTRUCTORS, DESTRUCTOR AND         |
    //                   | ASSIGNATION OPERATOR                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeansFast<T, TMODE, TDISTANCE, N>::ClusterKMeansFast(void) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>(),
        m_save_memory(false),
        m_number_of_cluster_neighbors(0) {}
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeansFast<T, TMODE, TDISTANCE, N>::ClusterKMeansFast(unsigned int number_of_clusters, unsigned int number_of_dimensions, const SeederBase<T, TMODE, TDISTANCE, N> * seeder, const VectorDistance &distance_object, unsigned int maximum_number_of_iterations, const TDISTANCE &cutoff, CLUSTER_EMPTY_METHOD cluster_empty_method, bool save_memory, unsigned int number_of_cluster_neighbors) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>(number_of_clusters, number_of_dimensions, seeder, distance_object, maximum_number_of_iterations, cutoff, cluster_empty_method),
        m_save_memory(save_memory),
        m_number_of_cluster_neighbors(number_of_cluster_neighbors) {}
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeansFast<T, TMODE, TDISTANCE, N>::ClusterKMeansFast(const ClusterKMeansFast<T, TMODE, TDISTANCE, N> &other) :
        ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>(other),
        m_save_memory(other.m_save_memory),
        m_number_of_cluster_neighbors(other.m_number_of_cluster_neighbors) {}
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeansFast<T, TMODE, TDISTANCE, N>::~ClusterKMeansFast(void) {}
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterKMeansFast<T, TMODE, TDISTANCE, N>& ClusterKMeansFast<T, TMODE, TDISTANCE, N>::operator=(const ClusterKMeansFast<T, TMODE, TDISTANCE, N> &other)
    {
        if (this != &other)
        {
            ClusterCenterBasedBase<T, TMODE, TDISTANCE, N>::operator=(other);
            m_save_memory = other.m_save_memory;
            m_number_of_cluster_neighbors = other.m_number_of_cluster_neighbors;
        }
        return *this;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS             |
    //                   | FULL EFFICIENT IMPLEMENTATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterKMeansFast<T, TMODE, TDISTANCE, N>::protectedTrain(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger)
    {
        if ((number_of_elements >= number_of_clusters) && (number_of_clusters > 0) && (this->m_seeder != 0))
        {
            VectorDense<TMODE, N> * * update_centers;
            VectorDense<unsigned int> * cluster_histogram;
            TDISTANCE maximum_cluster_distance, * center_to_center_distances;
            TDISTANCE * data_to_center_distances, * center_threshold, * update_distance;
            unsigned int * selected_cluster, number_of_resample_clusters;
            
            // Create structures.
            if (logger != 0)
                logger->log("Creating the structures for the Fast k-Means algorithm.");
            update_centers = new VectorDense<TMODE, N> * [number_of_threads];
            cluster_histogram = new VectorDense<unsigned int>[number_of_threads];
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
            {
                cluster_histogram[thread].set(number_of_clusters);
                update_centers[thread] = new VectorDense<TMODE, N>[number_of_clusters];
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    update_centers[thread][i].set((N)this->m_number_of_dimensions);
            }
            center_to_center_distances = new TDISTANCE[number_of_clusters * number_of_clusters];
            data_to_center_distances = new TDISTANCE[number_of_clusters * number_of_elements];
            center_threshold = new TDISTANCE[number_of_clusters];
            selected_cluster = new unsigned int[number_of_elements];
            update_distance = new TDISTANCE[number_of_clusters];
            
            if (this->m_cluster_information_trace_enabled)
                this->addTraceInformation(0, cluster_centers, number_of_clusters);
            
            // NOTATION INFORMATION:
            //      * $u(x)$ approximate distance between the point $x$ and its centroid.
            //      * $c(x)$ centroid of $x$, i.e. the centroid closer to $x$.
            //      * $l(x, c)$ estimation of the distance between point $x$ and the centroid $c$. It can be the estimated distance or the real distance.
            //      * $r(x)$ is a flag which indicates that the point can be updated (?).
            
            for (unsigned int iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
            {
                // 1.A) For all centers $c$ and $c'$, compute $d(c, c')$.
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_clusters; i += number_of_threads)
                    {
                        center_to_center_distances[i * number_of_clusters + i] = 0;
                        for (unsigned int j = i + 1; j < number_of_clusters; ++j)
                        {
                            TDISTANCE current_distance;
                            
                            this->m_distance_object.distance(cluster_centers[i], cluster_centers[j], current_distance);
                            center_to_center_distances[i * number_of_clusters + j] = current_distance / (TDISTANCE)2;
                            center_to_center_distances[j * number_of_clusters + i] = current_distance / (TDISTANCE)2;
                        }
                    }
                }
                // 1.B) For all centers $c$, compute $s(c) = \frac{1}{2} min_(c'\neq c}d(c, c')$
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_clusters; i += number_of_threads)
                    {
                        TDISTANCE * current_centers = &center_to_center_distances[i * number_of_clusters];
                        center_threshold[i] = std::numeric_limits<TDISTANCE>::max();
                        for (unsigned int j = 0; j < number_of_clusters; ++j)
                            if ((i != j) && (current_centers[j] < center_threshold[i]))
                                center_threshold[i] = current_centers[j];
                    }
                }
                
                // In the first iteration, we calculate the actual distance between the cluster and the data points.
                if (iteration == 0)
                {
                    if (logger != 0)
                        logger->log("First iteration: Calculating the actual distance between data and initial seeds.");
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE * current_distances = &data_to_center_distances[i * number_of_clusters];
                            
                            this->m_distance_object.distance(cluster_centers[0], *data[i], current_distances[0]);
                            selected_cluster[i] = 0;
                            
                            for (unsigned int j = 1; j < number_of_clusters; ++j)
                            {
                                this->m_distance_object.distance(cluster_centers[j], *data[i], current_distances[j]);
                                if (current_distances[j] < current_distances[selected_cluster[i]])
                                    selected_cluster[i] = j;
                            }
                        }
                    }
                }
                else
                {
                    // 2) Identify all points $x$ such that $u(x) \leq s(c(x))$, i.e. search all the points
                    //    which distance to the cluster is lower than the centroids threshold (and disable them).
                    
                    // 3) For all remaining points $x$ and centers $c$ such that
                    //      (i) $c \neq c(x)$ and              // For all centroids which are not the current centroid of $x$,
                    //     (ii) $u(x) > l(x, c)$ and           // which are closer to the point $x$ than its current centroid
                    //    (iii) $u(x) > \frac{1}{2}d(c(x), c)$ // and hold the distance constrain.
                    //    3.1) If $r(x)$, then compute $d(x, c(x))$ and assign $r(x) = false$. Otherwise, $d(x, c(x)) = u(x)$
                    //    3.2) If $d(x, c(x)) > l(x, c)$ or $d(x, c(x)) > \frac{1}{2}d(c(x), c)$, then
                    //         compute $d(x, c)$ and if $d(x, c) < d(x, c(x))$ then assign $c(x) = c$
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE * current_distances = &data_to_center_distances[i * number_of_clusters];
                            TDISTANCE * current_centers = &center_to_center_distances[selected_cluster[i] * number_of_clusters];
                            
                            if (current_distances[selected_cluster[i]] > center_threshold[selected_cluster[i]])
                            {
                                unsigned int previously_selected = selected_cluster[i];
                                unsigned int sc = selected_cluster[i];
                                bool up_to_date = false;
                                
                                for (unsigned int c = 0; c < number_of_clusters; ++c)
                                {
                                    // Pre-conditions of point 3 of the algorithm.
                                    if (c == previously_selected) continue;
                                    if (current_distances[previously_selected] <= current_distances[c]) continue;
                                    if (current_distances[previously_selected] <= current_centers[c]) continue;
                                    
                                    // Update once the distance between the centroid and the current cluster.
                                    if (!up_to_date)
                                    {
                                        this->m_distance_object.distance(cluster_centers[sc], *data[i], current_distances[sc]);
                                        up_to_date = true;
                                    }
                                    
                                    // Check if another cluster is available.
                                    if ((current_distances[sc] > current_distances[c]) ||
                                        (current_distances[sc] > center_to_center_distances[sc * number_of_clusters + c]))
                                    {
                                        this->m_distance_object.distance(cluster_centers[c], *data[i], current_distances[c]);
                                        // -------------------------------------------------------------------------------------------
                                        // NOTE: The second comparison is added to generate the same result as the original k-means
                                        // algorithm. Points which are equidistant are assigned to the cluster with the lower index.
                                        // It would be probably better to not use the points which are equidistant or assign them
                                        // to more than one cluster.
                                        // -------------------------------------------------------------------------------------------
                                        if ((current_distances[c] < current_distances[sc]) || ((current_distances[c] == current_distances[sc]) && (c < sc)))
                                            sc = c;
                                    }
                                }
                                selected_cluster[i] = sc;
                            }
                        }
                    }
                }
                
                // 4) For each cluster center, let $m(c)$ be the mean of the points assigned to $c$.
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    // 4.1) Re-initialize the pooling structures to zero.
                    cluster_histogram[thread_id].setValue(0);
                    for (unsigned int i = 0; i < number_of_clusters; ++i)
                        update_centers[thread_id][i].setValue(0);
                    
                    // 4.2) Assign each data vector to the closest cluster centroid and accumulate it.
                    for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
                    {
                        update_centers[thread_id][selected_cluster[i]] += *data[i];
                        ++cluster_histogram[thread_id][selected_cluster[i]];
                    }
                }
                // 4.3) Calculate the maximum distance between the new and old clusters.
                maximum_cluster_distance = 0;
                number_of_resample_clusters = 0;
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                {
                    // 4.3.1) Merge the different threads.
                    for (unsigned int thread = 1; thread < number_of_threads; ++thread)
                    {
                        cluster_histogram[0][i] += cluster_histogram[thread][i];
                        update_centers[0][i] += update_centers[thread][i];
                    }
                    // 4.3.2) Check if any centroid does not have any vector associated.
                    if (cluster_histogram[0][i] == 0)
                    {
                        if (this->m_cluster_empty_method == CLUSTER_EMPTY_EXCEPTION)
                            throw Exception("During the iterative k-means algorithm a cluster has no vector assigned.");
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_IGNORE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty - Ignoring.", i);
                            update_centers[0][i].copy(cluster_centers[i]);
                        }
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_RESAMPLE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty.", i);
                            maximum_cluster_distance = srvMax<TDISTANCE>(this->m_cutoff * 2 + 1, maximum_cluster_distance);
                            ++number_of_resample_clusters;
                        }
                        update_distance[i] = 0;
                    }
                    else
                    {
                        update_centers[0][i] /= (TMODE)cluster_histogram[0][i];
                        // 4.3.3) Calculate the distance between the previous and new centroids.
                        this->m_distance_object.distance(cluster_centers[i], update_centers[0][i], update_distance[i]);
                        if (update_distance[i] > maximum_cluster_distance)
                            maximum_cluster_distance = update_distance[i];
                    }
                }
                
                if (number_of_resample_clusters > 0)
                {
                    VectorDense<Tuple<TDISTANCE, unsigned int> > resample_indexes;
                    VectorDense<unsigned int> resample_histogram(number_of_clusters, 0), clusters_to_resample(number_of_resample_clusters);
                    unsigned int resample_selected, resample_size;
                    
                    if (logger != 0) logger->log("[WARNING] Re-sampling %d clusters.", number_of_resample_clusters);
                    number_of_resample_clusters = 0;
                    for (unsigned int rk = 0; rk < number_of_clusters; ++rk)
                    {
                        if (cluster_histogram[0][rk] == 0)
                        {
                            clusters_to_resample[number_of_resample_clusters] = rk;
                            ++number_of_resample_clusters;
                        }
                    }
                    
                    for (unsigned int m = 0; m < number_of_resample_clusters; ++m)
                    {
                        // Calculate the histogram of each cluster.
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                            ++resample_histogram[selected_cluster[rk]];
                        // Select the cluster with more samples.
                        resample_selected = 0;
                        resample_size = resample_histogram[0];
                        for (unsigned int rk = 1; rk < number_of_clusters; ++rk)
                        {
                            if (resample_histogram[rk] > resample_size)
                            {
                                resample_selected = rk;
                                resample_size = resample_histogram[rk];
                            }
                        }
                        // Get the indexes and the distance of the samples to the selected cluster.
                        resample_indexes.set(resample_size);
                        resample_size = 0;
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            if (selected_cluster[rk] == resample_selected)
                            {
                                TDISTANCE * current_distances = &data_to_center_distances[rk * number_of_clusters];
                                resample_indexes[resample_size].setData(current_distances[resample_selected], rk);
                                ++resample_size;
                            }
                        }
                        std::sort(&resample_indexes[0], &resample_indexes[resample_size]);
                        
                        update_centers[0][clusters_to_resample[m]].copy(*data[resample_indexes[resample_size / 2 + rand() % (resample_size / 2)].getSecond()]);
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            TDISTANCE * current_distances = &data_to_center_distances[rk * number_of_clusters];
                            this->m_distance_object.distance(*data[rk], update_centers[0][clusters_to_resample[m]], current_distances[clusters_to_resample[m]]);
                            if (current_distances[clusters_to_resample[m]] < current_distances[selected_cluster[rk]])
                                selected_cluster[rk] = clusters_to_resample[m];
                        }
                    }
                }
                
                // 5) For each point $x$ and center $c$, assign: $l(x, c) = max{ l(x, c) - d(c, m(c)), 0 }$
                // 6) For each point $x$, assign
                //    $u(x) = u(x) + d(m(c(x)), c(x))$
                //    $r(x) = true$
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_elements; i += number_of_threads)
                    {
                        TDISTANCE * current_distances = &data_to_center_distances[i * number_of_clusters];
                        
                        for (unsigned int c = 0; c < number_of_clusters; ++c)
                        {
                            if (c == selected_cluster[i])
                                current_distances[c] += update_distance[c];
                            else if (current_distances[c] > update_distance[c])
                                current_distances[c] -= update_distance[c];
                            else current_distances[c] = 0;
                        }
                    }
                }
                
                
                if (logger != 0)
                    logger->log("Iteration %06d of %06d of the k-means algorithm. Maximum update distance is %f.", iteration + 1, this->m_maximum_number_of_iterations, (double)maximum_cluster_distance);
                
                // 7) Replace each center $c$ by $m(c)$.
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    cluster_centers[i].copy(update_centers[0][i]);
                
                if (this->m_cluster_information_trace_enabled)
                    this->addTraceInformation(iteration + 1, cluster_centers, number_of_clusters);
                
                // 8) Check the cutoff threshold.
                if (maximum_cluster_distance <= this->m_cutoff)
                    break;
            }
            
            // Free allocated memory.
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
                delete [] update_centers[thread];
            delete [] update_centers;
            delete [] cluster_histogram;
            delete [] center_to_center_distances;
            delete [] data_to_center_distances;
            delete [] center_threshold;
            delete [] selected_cluster;
            delete [] update_distance;
        }
        else if (logger != 0)
        {
            if (number_of_elements < number_of_clusters) logger->log("Not enough samples (%d) to create %d clusters.", number_of_elements, number_of_clusters);
            if (number_of_clusters == 0) logger->log("The number of clusters is set to zero.");
            if (this->m_seeder == 0) logger->log("The k-means algorithms requires a seeder in order to create the initial set of centers.");
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | K-MEANS CLUSTERING CLASS             |
    //                   | MEMORY EFFICIENT IMPLEMENTATION      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void ClusterKMeansFast<T, TMODE, TDISTANCE, N>::protectedTrainMemory(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * cluster_centers, unsigned int number_of_clusters, unsigned int number_of_threads, BaseLogger * logger)
    {
        if ((number_of_elements >= number_of_clusters) && (number_of_clusters > 0) && (this->m_seeder != 0))
        {
            TriangleInequalityTable<TDISTANCE> distance_table(number_of_clusters, srvMin<unsigned int>(number_of_clusters - 1, m_number_of_cluster_neighbors));
            TDISTANCE * cluster_distance, maximum_cluster_distance, * current_cluster_distance;
            VectorDense<unsigned int> * cluster_histogram;
            VectorDense<TMODE, N> * * update_centers;
            unsigned int * cluster_selected, number_of_resample_clusters;
            bool * * unvisited_cluster;
            
            // Create structures ....................................................................................................................
            if (logger != 0)
                logger->log("Creating the structures for the memory-restricted Fast k-Means algorithm.");
            update_centers = new VectorDense<TMODE, N> * [number_of_threads];
            cluster_histogram = new VectorDense<unsigned int>[number_of_threads];
            cluster_selected = new unsigned int[number_of_elements];
            cluster_distance = new TDISTANCE[number_of_elements];
            unvisited_cluster = new bool * [number_of_threads];
            current_cluster_distance = new TDISTANCE[number_of_clusters];
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
            {
                cluster_histogram[thread].set(number_of_clusters);
                update_centers[thread] = new VectorDense<TMODE, N>[number_of_clusters];
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    update_centers[thread][i].set((N)this->m_number_of_dimensions);
                unvisited_cluster[thread] = new bool[number_of_clusters];
            }
            if (this->m_cluster_information_trace_enabled)
                this->addTraceInformation(0, cluster_centers, number_of_clusters);
            
            for (unsigned int iteration = 0; iteration < this->m_maximum_number_of_iterations; ++iteration)
            {
                distance_table.initialize(cluster_centers, this->m_distance_object);
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    // 1) Re-initialize the pooling structures to zero ..............................................................................
                    cluster_histogram[thread_id].setValue(0);
                    for (unsigned int i = 0; i < number_of_clusters; ++i)
                        update_centers[thread_id][i].setValue(0);
                    
                    // 2) Assign each data vector to the closest cluster centroid and accumulate it .................................................
                    if (iteration > 0)
                    {
                        for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE minimum_distance;
                            unsigned int selected_cluster;
                            
                            // Use the distance table to update the distance.
                            distance_table.nearestNeighbor(*data[i], cluster_centers, this->m_distance_object, cluster_distance[i], cluster_selected[i], unvisited_cluster[thread_id], minimum_distance, selected_cluster);
                            // Aggregate the current feature to its cluster.
                            cluster_selected[i] = selected_cluster;
                            cluster_distance[i] = minimum_distance;
                            update_centers[thread_id][cluster_selected[i]] += *data[i];
                            ++cluster_histogram[thread_id][cluster_selected[i]];
                        }
                    }
                    else
                    {
                        if (logger != 0)
                            logger->log("First iteration: Calculating the actual distance between data and initial seeds.");
                        for (unsigned int i = thread_id; i < number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE minimum_distance, current_distance;
                            unsigned int selected_cluster;
                            
                            this->m_distance_object.distance(*data[i], cluster_centers[0], minimum_distance);
                            selected_cluster = 0;
                            for (unsigned int j = 1; j < number_of_clusters; ++j)
                            {
                                this->m_distance_object.distance(*data[i], cluster_centers[j], current_distance);
                                
                                if (current_distance < minimum_distance)
                                {
                                    minimum_distance = current_distance;
                                    selected_cluster = j;
                                }
                            }
                            
                            cluster_selected[i] = selected_cluster;
                            cluster_distance[i] = minimum_distance;
                            update_centers[thread_id][selected_cluster] += *data[i];
                            ++cluster_histogram[thread_id][selected_cluster];
                        }
                    }
                }
                
                // 3) Merge the different threads ...................................................................................................
                for (unsigned int thread = 1; thread < number_of_threads; ++thread)
                {
                    cluster_histogram[0] += cluster_histogram[thread];
                    for (unsigned int i = 0; i < number_of_clusters; ++i)
                        update_centers[0][i] += update_centers[thread][i];
                }
                
                // 4) Calculate the maximum distance between the new and old clusters ...............................................................
                number_of_resample_clusters = 0;
                maximum_cluster_distance = 0;
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                {
                    if (cluster_histogram[0][i] == 0)
                    {
                        if (this->m_cluster_empty_method == CLUSTER_EMPTY_EXCEPTION)
                            throw Exception("During the iterative k-means algorithm a cluster has no vector assigned.");
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_IGNORE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty - Ignoring.", i);
                            update_centers[0][i].copy(cluster_centers[i]);
                        }
                        else if (this->m_cluster_empty_method == CLUSTER_EMPTY_RESAMPLE)
                        {
                            if (logger != 0) logger->log("[WARNING] Cluster %d is empty.", i);
                            maximum_cluster_distance = srvMax<TDISTANCE>(this->m_cutoff * 2 + 1, maximum_cluster_distance);
                            ++number_of_resample_clusters;
                        }
                    }
                    else
                    {
                        update_centers[0][i] /= (TMODE)cluster_histogram[0][i];
                        this->m_distance_object.distance(cluster_centers[i], update_centers[0][i], current_cluster_distance[i]);
                        if (current_cluster_distance[i] > maximum_cluster_distance)
                            maximum_cluster_distance = current_cluster_distance[i];
                    }
                }
                if (number_of_resample_clusters > 0)
                {
                    VectorDense<Tuple<TDISTANCE, unsigned int> > resample_indexes;
                    VectorDense<unsigned int> resample_histogram(number_of_clusters, 0), clusters_to_resample(number_of_resample_clusters);
                    unsigned int resample_selected, resample_size;
                    TDISTANCE current_distance;
                    
                    if (logger != 0) logger->log("[WARNING] Re-sampling %d clusters.", number_of_resample_clusters);
                    // Calculate the actual distances between the clusters and the centroids.
                    for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        this->m_distance_object.distance(*data[rk], update_centers[0][cluster_selected[rk]], cluster_distance[rk]);
                    number_of_resample_clusters = 0;
                    for (unsigned int rk = 0; rk < number_of_clusters; ++rk)
                    {
                        if (cluster_histogram[0][rk] == 0)
                        {
                            clusters_to_resample[number_of_resample_clusters] = rk;
                            ++number_of_resample_clusters;
                        }
                    }
                    
                    for (unsigned int m = 0; m < number_of_resample_clusters; ++m)
                    {
                        // Calculate the histogram of each cluster.
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                            ++resample_histogram[cluster_selected[rk]];
                        // Select the cluster with more samples.
                        resample_selected = 0;
                        resample_size = resample_histogram[0];
                        for (unsigned int rk = 1; rk < number_of_clusters; ++rk)
                        {
                            if (resample_histogram[rk] > resample_size)
                            {
                                resample_selected = rk;
                                resample_size = resample_histogram[rk];
                            }
                        }
                        // Get the indexes and the distance of the samples to the selected cluster.
                        resample_indexes.set(resample_size);
                        resample_size = 0;
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            if (cluster_selected[rk] == resample_selected)
                            {
                                resample_indexes[resample_size].setData(cluster_distance[rk], rk);
                                ++resample_size;
                            }
                        }
                        std::sort(&resample_indexes[0], &resample_indexes[resample_size]);
                        
                        update_centers[0][clusters_to_resample[m]].copy(*data[resample_indexes[resample_size / 2 + rand() % (resample_size / 2)].getSecond()]);
                        for (unsigned int rk = 0; rk < number_of_elements; ++rk)
                        {
                            this->m_distance_object.distance(*data[rk], update_centers[0][clusters_to_resample[m]], current_distance);
                            if (current_distance < cluster_distance[rk])
                            {
                                cluster_distance[rk] = current_distance;
                                cluster_selected[rk] = clusters_to_resample[m];
                            }
                        }
                    }
                }
                else
                {
                    for (unsigned int i = 0; i < number_of_elements; ++i)
                        cluster_distance[i] += current_cluster_distance[cluster_selected[i]];
                }
                
                if (logger != 0)
                    logger->log("Iteration %06d of %06d of the k-means algorithm. Maximum update distance is %f.", iteration + 1, this->m_maximum_number_of_iterations, (double)maximum_cluster_distance);
                
                // 5) Check the cutoff threshold ....................................................................................................
                if (maximum_cluster_distance <= this->m_cutoff)
                    break;
                
                // 6) Set the new cluster centers as the current cluster centers ....................................................................
                for (unsigned int i = 0; i < number_of_clusters; ++i)
                    cluster_centers[i].copy(update_centers[0][i]);
                
                if (this->m_cluster_information_trace_enabled)
                    this->addTraceInformation(iteration + 1, cluster_centers, number_of_clusters);
            }
            
            // Free allocated memory ................................................................................................................
            for (unsigned int thread = 0; thread < number_of_threads; ++thread)
            {
                delete [] update_centers[thread];
                delete [] unvisited_cluster[thread];
            }
            delete [] unvisited_cluster;
            delete [] update_centers;
            delete [] cluster_histogram;
            delete [] cluster_selected;
            delete [] cluster_distance;
            delete [] current_cluster_distance;
        }
        else if (logger != 0)
        {
            if (number_of_elements < number_of_clusters) logger->log("Not enough samples (%d) to create %d clusters.", number_of_elements, number_of_clusters);
            if (number_of_clusters == 0) logger->log("The number of clusters is set to zero.");
            if (this->m_seeder == 0) logger->log("The k-means algorithms requires a seeder in order to create the initial set of centers.");
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

