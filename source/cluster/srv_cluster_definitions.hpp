// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_DEFINITIONS_HPP_HEADER_FILE__
#define __SRV_CLUSTER_DEFINITIONS_HPP_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CLUSTER DEFINITIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    enum CLUSTER_IDENTIFIER { KMEANS_CLUSTER = 7101,           ///< Identifier of the k-means clustering algorithm.
                              KMEDIANS_CLUSTER,                ///< Identifier of the k-medians clustering algorithm.
                              KMEANS_FAST_CLUSTER,             ///< Identifier of the k-means clustering algorithm which uses the triangle inequality to accelerate the data-to-cluster assignations.
                              KMEDIANS_FAST_CLUSTER            ///< Identifier of the k-medians clustering algorithm which uses the triangle inequality to accelerate the data-to-cluster assignations.
    };
    
    /// Options to deal with empty clusters while creating the partitions of the feature space.
    enum CLUSTER_EMPTY_METHOD { CLUSTER_EMPTY_EXCEPTION = 7201, ///< Generates an exception and stops the clustering algorithm.
                                CLUSTER_EMPTY_IGNORE,           ///< Ignores the empty cluster and continues with the algorithm.
                                CLUSTER_EMPTY_RESAMPLE          ///< Move the empty cluster to the most populated cluster.
    };
    
    enum SEEDER_IDENTIFIER { SEEDER_UNIFORM_RANDOM_SELECTION = 7001, ///< Creates the seeds selecting a random set of the training samples.
                             SEEDER_KMEANS_PLUS_PLUS                 ///< Uses the k-means++ algorithm.
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

