// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA


#ifndef __SRV_SEEDER_BASE_HPP_HEADER_FILE__
#define __SRV_SEEDER_BASE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "srv_cluster_definitions.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PURE VIRTUAL SEEDER CLASS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class SeederBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SeederBase(void) {}
        /// Virtual destructor.
        virtual ~SeederBase(void) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        
        // -[ Virtual functions ]--------------------------------------------------------------------------------------------------------------------
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant dense sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void sow(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant sparse sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void sow(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void sow(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            sow(data_constant_ptrs.getData(), number_of_elements, seeds, number_of_seeds, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array with the vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void sow(const VECTOR<T, N> * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            sow(data_constant_ptrs.getData(), number_of_elements, seeds, number_of_seeds, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the seeder object (virtual copy constructor).
        virtual SeederBase<T, TMODE, TDISTANCE, N> * duplicate(void) const = 0;
        ///// /// Returns the class identifier for the current seeder method.
        ///// inline static SEEDER_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the seeder class.
        ///// inline static SeederBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (SeederBase<T, TMODE, TDISTANCE, N>*)new Seeder(); }
        ///// /// Returns a flag which states if the seeder class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the seeder type identifier of the current object.
        virtual SEEDER_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the seeder information into an XML object.
        virtual void convertToXML(XmlParser &parser) const = 0;
        /// Retrieves the seeder information from an XML object.
        virtual void convertFromXML(XmlParser &parser) = 0;
        
    protected:
        //// // Integer flag which is greater than zero when the seeder object has been initialized in a seeder factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SEEDER FACTORY CLASS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    struct SeederFactory { typedef Factory<SeederBase<T, TMODE, TDISTANCE, N> , SEEDER_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

