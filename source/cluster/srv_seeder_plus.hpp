// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_PLUS_SEEDER_HPP_HEADER_FILE__
#define __SRV_PLUS_SEEDER_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <cstdio>
#include <cstdlib>
#include <limits>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PLUS SEEDER CLASS DECLARATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class SeederPlus : public SeederBase<T, TMODE, TDISTANCE, N>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SeederPlus(void);
        /** Parameters descriptor which sets the metric used to calculate the distance between the features.
         *  \param[in] distance_object object used to calculate the distance between the feature samples used to create the seeds.
         *  \param[in] normal_distribution boolean flag which enables the use of a normal distribution instead of a uniform distribution when true.
         *  \param[in] ratio ratio of samples used to create the sub-set of samples where the samples are sampled from.
         */
        SeederPlus(const VectorDistance &distance_object, bool normal_distribution, double ratio);
        /// Virtual destructor.
        virtual ~SeederPlus(void) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant reference to the object which measures the distance between the selected seeds.
        inline const VectorDistance& getDistanceObject(void) const { return m_distance_object; }
        /** Sets the distance parameters
         *  \param[in] distance_object object used to calculate the distance between the feature samples used to create the seeds.
         */
        inline void setDistanceObject(const VectorDistance &distance_object) { m_distance_object = distance_object; }
        /// Returns the boolean flag which enables the use of a normal distribution instead of a uniform distribution when true.
        inline bool getNormalDistribution(void) const { return m_normal_distribution; }
        /// Sets the boolean flag which enables the use of a normal distribution instead of a uniform distribution when true.
        inline void setNormalDistribution(bool normal_distribution) { m_normal_distribution = normal_distribution; }
        /// Returns the ratio of samples used to create the sub-set of samples where the seeds are extracted from.
        inline double getRatio(void) const { return m_ratio; }
        /// Sets the ratio of samples used to create the sub-set of samples where the seeds are extracted from.
        inline void setRatio(double ratio) { m_ratio = ratio; }
        /** Sets the parameters of the seeder.
         *  \param[in] distance_object object used to calculate the distance between the feature samples used to create the seeds.
         *  \param[in] normal_distribution boolean flag which enables the use of a normal distribution instead of a uniform distribution when true.
         *  \param[in] ratio ratio of samples used to create the sub-set of samples where the samples are sampled from.
         */
        inline void set(const VectorDistance &distance_object, bool normal_distribution, double ratio)
        {
            m_distance_object = distance_object;
            m_normal_distribution = normal_distribution;
            m_ratio = ratio;
        }
        
        // -[ Virtual functions ]--------------------------------------------------------------------------------------------------------------------
        using SeederBase<T, TMODE, TDISTANCE, N>::sow;
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant dense sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds. The vectors of this arrays are expected to be properly initialized (i.e. to have the correct dimensionality).
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void sow(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const { inner_sow(data, number_of_elements, seeds, number_of_seeds, number_of_threads, logger); }
        /** This function generates a set of seeds for the given data set.
         *  \param[in] data array of pointers to the constant sparse sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds. The vectors of this arrays are expected to be properly initialized (i.e. to have the correct dimensionality).
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        inline void sow(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger = 0) const { inner_sow(data, number_of_elements, seeds, number_of_seeds, number_of_threads, logger); }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the seeder object (virtual copy constructor).
        inline SeederBase<T, TMODE, TDISTANCE, N> * duplicate(void) const { return (SeederBase<T, TMODE, TDISTANCE, N> *)new SeederPlus<T, TMODE, TDISTANCE, N>(*this); }
        /// Returns the class identifier for the current seeder method.
        inline static SEEDER_IDENTIFIER getClassIdentifier(void) { return SEEDER_KMEANS_PLUS_PLUS; }
        /// Generates a new empty instance of the seeder class.
        inline static SeederBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (SeederBase<T, TMODE, TDISTANCE, N> *)new SeederPlus(); }
        /// Returns a flag which states if the seeder class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the seeder type identifier of the current object.
        inline SEEDER_IDENTIFIER getIdentifier(void) const { return SEEDER_KMEANS_PLUS_PLUS; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the seeder information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the seeder information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /** Private template function which generates the set of seeds for the given data set both for sparse and dense vectors.
         *  \param[in] data array of pointers to the constant sub-vectors which are used to generate the seeds.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[out] seeds an array with the selected seeds.
         *  \param[in] number_of_seeds number of generated seeds.
         *  \param[in] number_of_threads number of threads used to concurrently extract the seeds from the data.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void inner_sow(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger) const;
        /// Measures the distance between the selected seeds.
        VectorDistance m_distance_object;
        /// Boolean flag which enables or disables the use of a normal distribution instead of a uniform distribution.
        bool m_normal_distribution;
        /// Ratio of samples used to calculate the maximum number of vectors used to create the PCA model.
        double m_ratio;
        // Integer flag which is greater than zero when the seeder object has been initialized in a seeder factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PLUS SEEDER CLASS IMPLEMENTATION     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    SeederPlus<T, TMODE, TDISTANCE, N>::SeederPlus(void) :
        m_distance_object(EUCLIDEAN_DISTANCE),
        m_normal_distribution(false),
        m_ratio(1.0)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    SeederPlus<T, TMODE, TDISTANCE, N>::SeederPlus(const VectorDistance &distance_object, bool normal_distribution, double ratio) :
        m_distance_object(distance_object),
        m_normal_distribution(normal_distribution),
        m_ratio(ratio)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    template <template <class, class> class VECTOR>
    void SeederPlus<T, TMODE, TDISTANCE, N>::inner_sow(VECTOR<T, N> const * const * data, unsigned int number_of_elements, VectorDense<TMODE, N> * seeds, unsigned int number_of_seeds, unsigned int number_of_threads, BaseLogger * logger) const
    {
        unsigned int random_number_of_elements;
        unsigned int * indexes;
        
        if (number_of_seeds > number_of_elements)
            throw Exception("The number of seeds (%d) exceeds the number available elements (%d)", number_of_seeds, number_of_elements);
        indexes = new unsigned int[number_of_elements];
        for (unsigned int i = 0; i < number_of_elements; ++i) indexes[i] = i;
        for (unsigned int i = 0; i < number_of_elements; ++i) srvSwap(indexes[i], indexes[(rand() % (number_of_elements - i)) + i]);
        random_number_of_elements = srvMin<unsigned int>(number_of_elements, srvMax<unsigned int>(number_of_seeds, (unsigned int)((double)number_of_elements * m_ratio)));
        
        if (m_normal_distribution)
        {
            VectorDense<TDISTANCE> distances(random_number_of_elements);
            VectorDense<Tuple<TDISTANCE, unsigned int> > sorted_distances(random_number_of_elements);
            
            seeds[0].copy(*data[indexes[rand() % random_number_of_elements]]);
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                    m_distance_object.distance(*data[indexes[i]], seeds[0], distances[i]);
            }
            if (logger != 0)
                logger->log("Selected seed 1 of %d. This seed is randomly selected.", number_of_seeds);
            
            for (unsigned int current_seed = 1; current_seed < number_of_seeds; ++current_seed)
            {
                const double uniform_random = (double)rand() / (double)RAND_MAX;
                const double sigma = 0.2;
                const double sigma2 = sigma * sigma;
                unsigned int selected_element, number_of_zeros;
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                        sorted_distances[i].setData(distances[i], i);
                }
                std::sort(sorted_distances.getData(), sorted_distances.getData() + random_number_of_elements);
                number_of_zeros = 0;
                ///////for (unsigned int i = 0; (i < random_number_of_elements) && (sorted_distances[i].getFirst() <= std::numeric_limits<TDISTANCE>::epsilon()); ++i, ++number_of_zeros);
                for (unsigned int i = 0; (i < random_number_of_elements) && (sorted_distances[i].getFirst() <= 0); ++i, ++number_of_zeros);
                if (number_of_zeros == random_number_of_elements)
                {
                    delete [] indexes;
                    throw Exception("Cannot select %d seeds since the given set of vectors only has %d different vectors.", number_of_seeds, current_seed);
                }
                selected_element = (random_number_of_elements - number_of_zeros - 1);
                selected_element = number_of_zeros + (unsigned int)((double)selected_element * (1.0 - sqrt(-2 * sigma2 * log(uniform_random))));
                selected_element = sorted_distances[std::max((unsigned int)0, std::min(selected_element, random_number_of_elements - 1))].getSecond();
                
                seeds[current_seed].copy(*data[indexes[selected_element]]);
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                    {
                        TDISTANCE current_distance;
                        
                        m_distance_object.distance(*data[indexes[i]], seeds[current_seed], current_distance);
                        if (current_distance < distances[i])
                            distances[i] = current_distance;
                    }
                }
                if ((logger != 0) && (current_seed % 10 == 0))
                    logger->log("Selected seed %d of %d.", current_seed + 1, number_of_seeds);
            }
        }
        else
        {
            VectorDense<TDISTANCE> distances(random_number_of_elements);
            
            seeds[0].copy(*data[indexes[rand() % random_number_of_elements]]);
            if ((m_distance_object.getIdentifier() == COSINE_DISTANCE) || (m_distance_object.getIdentifier() == BHATTACHARYYA_DISTANCE) ||
                (m_distance_object.getIdentifier() == INTERSECTION_DISTANCE) || (m_distance_object.getIdentifier() == GEODESIC_DISTANCE))
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                        m_distance_object.distance(*data[indexes[i]], seeds[0], distances[i]);
                }
            }
            else
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                        m_distance_object.distancePartial(*data[indexes[i]], seeds[0], distances[i]);
                }
            }
            if (logger != 0)
                logger->log("Selected seed 1 of %d. This seed is randomly selected.", number_of_seeds);
            
            for (unsigned int current_seed = 1; current_seed < number_of_seeds; ++current_seed)
            {
                const double threshold_probability = (double)rand() / (double)RAND_MAX;
                double accumulated_distance, accumulated_probability;
                unsigned int selected_element;
                
                accumulated_distance = 0;
                for (unsigned int i = 0; i < random_number_of_elements; ++i)
                    accumulated_distance += (double)distances[i] * (double)distances[i];
                selected_element = random_number_of_elements;
                accumulated_probability = 0;
                for (unsigned int i = 0; i < random_number_of_elements; ++i)
                {
                    const double probability = (double)distances[i] * (double)distances[i] / (double)accumulated_distance;
                    accumulated_probability += probability;
                    if ((accumulated_probability >= threshold_probability) && (distances[i] != 0))
                    {
                        selected_element = i;
                        break;
                    }
                }
                
                if (selected_element == random_number_of_elements)
                {
                    delete [] indexes;
                    throw Exception("Cannot select %d seeds since the given set of vectors only has %d different vectors.", number_of_seeds, current_seed);
                }
                seeds[current_seed].copy(*data[indexes[selected_element]]);
                if ((m_distance_object.getIdentifier() == COSINE_DISTANCE) || (m_distance_object.getIdentifier() == BHATTACHARYYA_DISTANCE) ||
                    (m_distance_object.getIdentifier() == INTERSECTION_DISTANCE) || (m_distance_object.getIdentifier() == GEODESIC_DISTANCE))
                {
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE current_distance;
                            
                            m_distance_object.distance(*data[indexes[i]], seeds[current_seed], current_distance);
                            if (current_distance < distances[i])
                                distances[i] = current_distance;
                        }
                    }
                }
                else
                {
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int i = omp_get_thread_num(); i < random_number_of_elements; i += number_of_threads)
                        {
                            TDISTANCE current_distance;
                            
                            m_distance_object.distancePartial(*data[indexes[i]], seeds[current_seed], current_distance);
                            if (current_distance < distances[i])
                                distances[i] = current_distance;
                        }
                    }
                }
                if ((logger != 0) && (current_seed % 10 == 0))
                    logger->log("Selected seed %d of %d.", current_seed + 1, number_of_seeds);
            }
        }
        
        delete [] indexes;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void SeederPlus<T, TMODE, TDISTANCE, N>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Seeder");
        parser.setAttribute("Identifier", this->getClassIdentifier());
        parser.setAttribute("Normal_Distribution", m_normal_distribution);
        parser.setAttribute("Ratio", m_ratio);
        parser.addChildren();
        m_distance_object.convertToXML(parser);
        parser.closeTag();
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void SeederPlus<T, TMODE, TDISTANCE, N>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Seeder") && ((SEEDER_IDENTIFIER)((int)parser.getAttribute("Identifier")) == this->getClassIdentifier()))
        {
            m_normal_distribution = parser.getAttribute("Normal_Distribution");
            m_ratio = parser.getAttribute("Ratio");
            
            while (!(parser.isTagIdentifier("Seeder") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Vector_Distance"))
                    m_distance_object.convertFromXML(parser);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

