// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_BASE_HPP_HEADER_FILE__
#define __SRV_CLUSTER_BASE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
#include "srv_cluster_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | PURE VIRTUAL CLUSTER CLASS           |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // Forward class declaration.
    template <class T, class TMODE, class TDISTANCE, class N> class ClusterInformationTrace;
    
    /** Pure virtual class of the clustering objects.
     *  
     *  \tparam T type of the samples data.
     *  \tparam TMODE type of the clusters data.
     *  \tparam TDISTANCE type of the data used to calculate the distance to the clusters.
     *  \tparam N type of the samples index.
     */
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class ClusterBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClusterBase(void);
        /** Constructor which initializes all the parameters of the base cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         */
        ClusterBase(unsigned int number_of_clusters, unsigned int number_of_dimensions);
        /// Copy constructor.
        ClusterBase(const ClusterBase<T, TMODE, TDISTANCE, N> &other);
        /// Destructor.
        virtual ~ClusterBase(void);
        /// Assignation operator.
        ClusterBase<T, TMODE, TDISTANCE, N>& operator=(const ClusterBase<T, TMODE, TDISTANCE, N> &other);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the number of clusters.
        inline unsigned int getNumberOfClusters(void) const { return m_number_of_clusters; }
        /// Returns the number of dimensions of the clustered feature vectors.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Sets the number of dimensions of the clustered feature vectors.
        inline void setNumberOfDimensions(unsigned int number_of_dimensions) { m_number_of_dimensions = number_of_dimensions; }
        
        // -[ Clustering functions ]-----------------------------------------------------------------------------------------------------------------
        /** Creates the clusters from the set of dense data vectors.
         *  \param[in] data array of pointers to the constant dense sub-vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void create(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /** Creates the clusters from the set of sparse data vectors.
         *  \param[in] data array of pointers to the constant dense sub-vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void create(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        
        /** Creates the clusters from the set of data vectors.
         *  \param[in] data array of pointers to the vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void create(VECTOR<T, N> const * const * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            create(data_constant_ptrs.getData(), number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** Creates the clusters from the set of data vectors.
         *  \param[in] data array of vectors used to create the clusters.
         *  \param[in] number_of_elements number of vectors in the array.
         *  \param[in] number_of_threads number of threads used to create the clusters concurrently.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void create(const VECTOR<T, N> * data, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            create(data_constant_ptrs.getData(), number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant dense sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void cluster(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void cluster(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data vectors which will be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void cluster(VECTOR<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            cluster(data_constant_ptrs.getData(), data_clusters, number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array with the data vectors that will be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void cluster(const VECTOR<T, N> * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            cluster(data_constant_ptrs.getData(), data_clusters, number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the data constant dense sub-vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void update(ConstantSubVectorDense<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void update(ConstantSubVectorSparse<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array of pointers to the vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void update(VECTOR<T, N> const * const * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            update(data_constant_ptrs.getData(), number_of_elements, update_information, global_clustering, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** Updates the clusters by splitting or removing the selected clusters.
         *  \param[in] data array with the vectors used to update the clusters.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] update_information sparse vector with the information of the clusters that must be updated. The value of the of the sparse vector corresponds to the number of partitions of the cluster (set to zero to remove the cluster) while the index is the index of the updated cluster.
         *  \param[in] global_clustering boolean flag which when true it applies the clustering algorithm using all data after all clusters have been updated.
         *  \param[in] number_of_threads number of threads used to concurrently update the cluster.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void update(const VECTOR<T, N> * data, unsigned int number_of_elements, const VectorSparse<unsigned int, unsigned int> &update_information, bool global_clustering, unsigned int number_of_threads, BaseLogger * logger = 0)
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            update(data_constant_ptrs.getData(), number_of_elements, update_information, global_clustering, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query constant dense sub-vectors.
         *  \param[out] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void nearest(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<TDISTANCE> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query constant sparse sub-vectors.
         *  \param[out] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void nearest(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<TDISTANCE> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array of pointers to the query vectors.
         *  \param[in] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void nearest(VECTOR<T, N> const * const * data, VectorSparse<TDISTANCE> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            nearest(data_constant_ptrs.getData(), distances, number_of_elements, number_of_neighbors, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** Searches the K-nearest neighbors clusters for each given data vector.
         *  \param[in] data array with the query vectors.
         *  \param[in] distances array with the cluster indices and distances to the K nearest neighbors. This array is expected to be allocated (i.e.\ the function does not allocate it) and to have as many elements as the data array.
         *  \param[in] number_of_elements number of elements of the arrays.
         *  \param[in] number_of_neighbors number of nearest neighbors returned for each data vector.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distance to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void nearest(const VECTOR<T, N> * data, VectorSparse<TDISTANCE> * distances, unsigned int number_of_elements, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            nearest(data_constant_ptrs.getData(), distances, number_of_elements, number_of_neighbors, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        // -[ Cluster creation information ]---------------------------------------------------------------------------------------------------------
        /// Returns the boolean flag which indicates if the cluster centers generated at each iteration of the training algorithm are stored or not.
        inline bool isClusterInformationTrace(void) const { return m_cluster_information_trace_enabled; }
        /// Returns a vector with the centroids of the clusters calculated at each iteration of the iterative training algorithm.
        void getClusterInformationTrace(VectorDense<const ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > &cluster_information) const;
        /// Resets the list of cluster centers.
        void resetClusterInformationTrace(void);
        /// Enables or disables to store all cluster centers generated at each iteration of the training algorithm.
        void enableClusterInformationTrace(bool cluster_trace);
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates the cluster object (virtual copy constructor).
        virtual ClusterBase<T, TMODE, TDISTANCE, N> * duplicate(void) const = 0;
        ///// /// Returns the class identifier for the current cluster method.
        ///// inline static CLUSTER_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the cluster class.
        ///// inline static ClusterBase<T, TMODE, TDISTANCE, N> * generateObject(void) { return (ClusterBase<T, TMODE, TDISTANCE, N> *)new Cluster(); }
        ///// /// Returns a flag which states if the cluster class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the cluster type identifier of the current object.
        virtual CLUSTER_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the information of the cluster object into a XML object.
        virtual void convertToXML(XmlParser &parser) const = 0;
        /// Retrieves the information of the cluster object from a XML object.
        virtual void convertFromXML(XmlParser &parser) = 0;
        
    protected:
        /** Initializes the parameters of the base cluster class.
         *  \param[in] number_of_clusters number of cluster created by the clustering algorithm.
         *  \param[in] number_of_dimensions number of dimensions of the clustered feature vectors.
         */
        void setBase(unsigned int number_of_clusters, unsigned int number_of_dimensions);
        
        /// Number of clusters generated by the clustering algorithm.
        unsigned int m_number_of_clusters;
        /// Number of dimensions of the clustered feature vectors.
        unsigned int m_number_of_dimensions;
        
        /// Boolean flag which is true when the cluster centers obtained at each step of the iterative training algorithm is stored.
        bool m_cluster_information_trace_enabled;
        /// List with the different cluster centers generated by the iterative clustering algorithm.
        ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * m_cluster_information_trace_list;
        
        //// // Integer flag which is greater than zero when the cluster object has been initialized in a cluster factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | PURE VIRTUAL CLUSTER CLASS           |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterBase<T, TMODE, TDISTANCE, N>::ClusterBase(void) :
        m_number_of_clusters(0),
        m_number_of_dimensions(0),
        m_cluster_information_trace_enabled(false),
        m_cluster_information_trace_list(0)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterBase<T, TMODE, TDISTANCE, N>::ClusterBase(unsigned int number_of_clusters, unsigned int number_of_dimensions) :
        m_number_of_clusters(number_of_clusters),
        m_number_of_dimensions(number_of_dimensions),
        m_cluster_information_trace_enabled(false),
        m_cluster_information_trace_list(0)
    {
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterBase<T, TMODE, TDISTANCE, N>::ClusterBase(const ClusterBase<T, TMODE, TDISTANCE, N> &other) :
        m_number_of_clusters(other.m_number_of_clusters),
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_cluster_information_trace_enabled(other.m_cluster_information_trace_enabled),
        m_cluster_information_trace_list(0)
    {
        if (other.m_cluster_information_trace_list != 0)
        {
            const ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr;
            ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * current_ptr;
            
            list_ptr = other.m_cluster_information_trace_list;
            current_ptr = m_cluster_information_trace_list = new ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * >(list_ptr->getData()->duplicate());
            list_ptr = list_ptr->getNext();
            while (list_ptr != 0)
            {
                current_ptr->addNext(list_ptr->getData()->duplicate());
                list_ptr = list_ptr->getNext();
                current_ptr = current_ptr->getNext();
            }
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterBase<T, TMODE, TDISTANCE, N>::~ClusterBase(void)
    {
        if (m_cluster_information_trace_list != 0)
        {
            ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr = m_cluster_information_trace_list;
            for (; list_ptr != 0; list_ptr = list_ptr->getNext())
                delete list_ptr->getData();
            delete m_cluster_information_trace_list;
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    ClusterBase<T, TMODE, TDISTANCE, N>& ClusterBase<T, TMODE, TDISTANCE, N>::operator=(const ClusterBase<T, TMODE, TDISTANCE, N> &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_cluster_information_trace_list != 0)
            {
                for (ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr = m_cluster_information_trace_list; list_ptr != 0; list_ptr = list_ptr->getNext())
                    delete list_ptr->getData();
                delete m_cluster_information_trace_list;
            }
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            m_number_of_dimensions = other.m_number_of_dimensions;
            m_number_of_clusters = other.m_number_of_clusters;
            
            m_cluster_information_trace_enabled = other.m_cluster_information_trace_enabled;
            if (other.m_cluster_information_trace_list != 0)
            {
                const ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr;
                ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * current_ptr;
                
                list_ptr = other.m_cluster_information_list;
                current_ptr = m_cluster_information_trace_list = new ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * >(list_ptr->getData()->duplicate());
                list_ptr = list_ptr->getNext();
                while (list_ptr != 0)
                {
                    current_ptr->addNext(list_ptr->getData()->duplicate());
                    current_ptr = current_ptr->getNext();
                    list_ptr = list_ptr->getNext();
                }
            }
            else m_cluster_information_trace_list = 0;
        }
        
        return *this;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterBase<T, TMODE, TDISTANCE, N>::setBase(unsigned int number_of_clusters, unsigned int number_of_dimensions)
    {
        // -[ Free ]---------------------------------------------------------------------------------------------------------------------------------
        if (m_cluster_information_trace_list != 0)
        {
            ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr = m_cluster_information_trace_list;
            for (; list_ptr != 0; list_ptr = list_ptr->getNext())
                delete list_ptr->getData();
            delete m_cluster_information_trace_list;
        }
        
        // -[ Set ]----------------------------------------------------------------------------------------------------------------------------------
        m_number_of_clusters = number_of_clusters;
        m_number_of_dimensions = number_of_dimensions;
        m_cluster_information_trace_enabled = false;
        m_cluster_information_trace_list = 0;
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterBase<T, TMODE, TDISTANCE, N>::getClusterInformationTrace(VectorDense<const ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > &cluster_information) const
    {
        const ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * current_ptr;
        unsigned int number_of_values;
        
        current_ptr = m_cluster_information_trace_list;
        number_of_values = 0;
        while (current_ptr != 0)
        {
            ++number_of_values;
            current_ptr = current_ptr->getNext();
        }
        cluster_information.set(number_of_values);
        
        current_ptr = m_cluster_information_trace_list;
        number_of_values = 0;
        while (current_ptr != 0)
        {
            cluster_information[cluster_information.size() - number_of_values - 1] = current_ptr->getData();
            current_ptr = current_ptr->getNext();
            ++number_of_values;
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterBase<T, TMODE, TDISTANCE, N>::resetClusterInformationTrace(void)
    {
        if (m_cluster_information_trace_list != 0)
        {
            ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr;
            
            list_ptr = m_cluster_information_trace_list;
            while (list_ptr != 0)
            {
                delete list_ptr->getData();
                list_ptr = list_ptr->getNext();
            }
            
            delete m_cluster_information_trace_list;
            m_cluster_information_trace_list = 0;
        }
    }
    
    template <class T, class TMODE, class TDISTANCE, class N>
    void ClusterBase<T, TMODE, TDISTANCE, N>::enableClusterInformationTrace(bool cluster_trace)
    {
        if (m_cluster_information_trace_list != 0)
        {
            ChainedList<ClusterInformationTrace<T, TMODE, TDISTANCE, N> * > * list_ptr;
            
            list_ptr = m_cluster_information_trace_list;
            while (list_ptr != 0)
            {
                delete list_ptr->getData();
                list_ptr = list_ptr->getNext();
            }
            
            delete m_cluster_information_trace_list;
            m_cluster_information_trace_list = 0;
        }
        m_cluster_information_trace_enabled = cluster_trace;
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CLUSTER FACTORY                      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    struct ClusterFactory { typedef Factory<ClusterBase<T, TMODE, TDISTANCE, N> , CLUSTER_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CLUSTER INFORMATION TRACE CLASS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which stores the clustering information at the different stages of the clustering algorithm.
    template <class T, class TMODE, class TDISTANCE = TMODE, class N = unsigned int>
    class ClusterInformationTrace
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ClusterInformationTrace(void) : m_iteration(0) {}
        /// Constructor which sets the iteration at which the current cluster state has been generated.
        ClusterInformationTrace(unsigned int iteration) : m_iteration(iteration) {}
        /// Copy constructor.
        ClusterInformationTrace(const ClusterInformationTrace<T, TMODE, TDISTANCE, N> &other) : m_iteration(other.m_iteration) {}
        /// Destructor.
        virtual ~ClusterInformationTrace(void) {}
        /// Assignation operator.
        ClusterInformationTrace<T, TMODE, TDISTANCE, N>& operator=(const ClusterInformationTrace<T, TMODE, TDISTANCE, N> &other)
        {
            if (this != &other) m_iteration = other.m_iteration;
            return *this;
        }
        /// Returns a copy of the cluster information trace object (virtual copy constructor).
        virtual ClusterInformationTrace<T, TMODE, TDISTANCE, N> * duplicate(void) const = 0;
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the iteration of the clustering algorithm.
        inline unsigned int getIteration(void) const { return m_iteration; }
        /// Sets the iteration of the clustering algorithm.
        inline void setIteration(unsigned int iteration) { m_iteration = iteration; }
        
        // -[ Clustering functions ]-----------------------------------------------------------------------------------------------------------------
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant dense sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void cluster(ConstantSubVectorDense<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data constant sparse sub-vectors which are going to be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void cluster(ConstantSubVectorSparse<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array of pointers to the data vectors which will be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void cluster(VECTOR<T, N> const * const * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i]->getData(), data[i]->size());
            cluster(data_constant_ptrs.getData(), data_clusters, number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        /** Assigns to each data vector the weight of belonging to a cluster.
         *  \param[in] data array with the data vectors that will be clustered.
         *  \param[out] data_clusters array of sparse vectors with the to belong to a certain cluster. This array is not allocated by the function and it is expected to have the same number of elements than the data vectors array.
         *  \param[in] number_of_elements number of elements in the array.
         *  \param[in] number_of_threads number of threads used to concurrently assign the data vectors to the clusters.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        inline void cluster(const VECTOR<T, N> * data, VectorSparse<float> * data_clusters, unsigned int number_of_elements, unsigned int number_of_threads, BaseLogger * logger = 0) const
        {
            VectorDense<typename ConstantVectorType<VECTOR, T, N>::Type * > data_constant_ptrs(number_of_elements);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                data_constant_ptrs[i] = new typename ConstantVectorType<VECTOR, T, N>::Type(data[i].getData(), data[i].size());
            cluster(data_constant_ptrs.getData(), data_clusters, number_of_elements, number_of_threads, logger);
            for (unsigned int i = 0; i < number_of_elements; ++i)
                delete data_constant_ptrs[i];
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the cluster creation information into a XML object.
        virtual void convertToXML(XmlParser &parser) const = 0;
        /// Retrieves the cluster creation information from a XML object.
        virtual void convertFromXML(XmlParser &parser) = 0;
        
    protected:
        /// Iteration of the clustering algorithm.
        unsigned int m_iteration;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY CLUSTERING FUNCTIONS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Resorts a given set of sample vectors so that the samples of the same cluster are
     *  put together in the resulting array. Sample vectors are resorted in ascending
     *  order with respect to their cluster label and they are assigned to the highest
     *  likelihood cluster.
     *  \warning {When distances are used as likelihood measures, they must be negated or inverted
     *  or the elements assigned to the cluster will be the furthest away elements}
     *  \param[in,out] samples array of pointers to the sample vectors to be re-sorted.
     *  \param[in] cluster_likelihood array with the cluster belonging likelihood information of each sample vector.
     *  \param[in] number_of_samples number of elements in the <b>samples</b> and the <b>cluster_likelihood</b> arrays.
     *  \param[out] modes_histogram number of samples vector present at each cluster.
     *  \param[in] number_of_modes total number of clusters.
     *  \param[out] auxiliary an auxiliary array which is re-sorted as the samples array. This is an auxiliary array which is not used when set to 0 (default value).
     *  \note cluster labels are expected to be between 0 and N - 1, where N is the number of modes.
     */
    template <template <class, class> class VECTOR, class T, class N, class Q, class TLIKE, class NLIKE>
    void resortCluster(VECTOR<T, N> * * samples, const VectorSparse<TLIKE, NLIKE> * cluster_likelihood, unsigned int number_of_samples, unsigned int * modes_histogram, unsigned int number_of_modes, Q * auxiliary)
    {
        VectorDense<unsigned int> offsets(number_of_modes);
        VectorDense<unsigned int> selected_cluster(number_of_samples);
        
        // 0) Select the cluster with the higher likelihood.
        for (unsigned int i = 0; i < number_of_samples; ++i)
        {
            TLIKE maximum_likelihood;
            unsigned int selected;
            
            maximum_likelihood = cluster_likelihood[i][0].getFirst();
            selected = 0;
            for (unsigned int j = 1; j < (unsigned int)cluster_likelihood[i].size(); ++j)
            {
                if (cluster_likelihood[i][j].getFirst() > maximum_likelihood)
                {
                    maximum_likelihood = cluster_likelihood[i][j].getFirst();
                    selected = j;
                }
            }
            selected_cluster[i] = (unsigned int)cluster_likelihood[i][selected].getSecond();
        }
        // 1) Set the frequency histogram of each cluster to zero.
        for (unsigned int i = 0; i < number_of_modes; ++i) modes_histogram[i] = 0;
        // 2) Calculate the frequency of samples assigned to each cluster (i.e. the number of samples that belong to each cluster).
        for (unsigned int i = 0; i < number_of_samples; ++i)
            ++modes_histogram[selected_cluster[i]];
        // 3) Calculate accumulated the offset of each sample.
        offsets[0] = modes_histogram[0];
        for (unsigned int i = 1; i < number_of_modes; ++i)
            offsets[i] = offsets[i - 1] + modes_histogram[i];
        // 4) Resort the samples vector.
        for (unsigned int current = 0, cluster = 0; current < number_of_samples;)
        {
            --offsets[selected_cluster[current]];
            if (current >= offsets[selected_cluster[current]])
            {
                while ((current < number_of_samples) && (current >= offsets[selected_cluster[current]]))
                {
                    current += modes_histogram[cluster];
                    ++cluster;
                }
            }
            else
            {
                unsigned int destination = offsets[selected_cluster[current]];
                srvSwap(samples[current], samples[destination]);
                srvSwap(selected_cluster[current], selected_cluster[destination]);
                if (auxiliary != 0)
                    srvSwap(auxiliary[current], auxiliary[destination]);
            }
        }
    }
    
    /** Resorts a given set of sample vectors so that the samples of the same cluster are
     *  put together in the resulting array. Sample vectors are resorted in ascending
     *  order with respect to their cluster label and they are assigned to the cluster
     *  with the higher likelihood to belong to.
     *  \param[in,out] samples array of pointers to the sample vectors to be re-sorted.
     *  \param[in,out] cluster_likelihood array with the cluster belonging likelihood information of each sample vector.
     *  \param[in] number_of_samples number of elements in the <b>samples</b> and the <b>cluster_likelihood</b> arrays.
     *  \param[out] modes_histogram number of samples vector present at each cluster.
     *  \param[in] number_of_modes total number of clusters.
     *  \param[out] auxiliary an auxiliary array which is re-sorted as the samples array. This is an auxiliary array which is not used when set to 0 (default value).
     *  \note cluster labels are expected to be between 0 and N - 1, where N is the number of modes.
     */
    template <template <class, class> class VECTOR, class T, class N, class Q, class TLIKE, class NLIKE>
    void resortCluster(const VECTOR<T, N> * * samples, VectorSparse<TLIKE, NLIKE> * cluster_likelihood, unsigned int number_of_samples, unsigned int * modes_histogram, unsigned int number_of_modes, Q * auxiliary)
    {
        VectorDense<unsigned int> offsets(number_of_modes);
        VectorDense<unsigned int> selected_cluster(number_of_samples);
        
        // 0) Select the cluster with the higher likelihood.
        for (unsigned int i = 0; i < number_of_samples; ++i)
        {
            TLIKE maximum_likelihood;
            unsigned int selected;
            
            maximum_likelihood = cluster_likelihood[i][0].getFirst();
            selected = 0;
            for (unsigned int j = 1; j < (unsigned int)cluster_likelihood[i].size(); ++j)
            {
                if (cluster_likelihood[i][j].getFirst() > maximum_likelihood)
                {
                    maximum_likelihood = cluster_likelihood[i][j].getFirst();
                    selected = j;
                }
            }
            selected_cluster[i] = (unsigned int)cluster_likelihood[i][selected].getSecond();
        }
        // 1) Set the frequency histogram of each cluster to zero.
        for (unsigned int i = 0; i < number_of_modes; ++i) modes_histogram[i] = 0;
        // 2) Calculate the frequency of samples assigned to each cluster (i.e. the number of samples that belong to each cluster).
        for (unsigned int i = 0; i < number_of_samples; ++i)
            ++modes_histogram[selected_cluster[i]];
        // 3) Calculate accumulated the offset of each sample.
        offsets[0] = modes_histogram[0];
        for (unsigned int i = 1; i < number_of_modes; ++i)
            offsets[i] = offsets[i - 1] + modes_histogram[i];
        // 4) Resort the samples vector.
        for (unsigned int current = 0, cluster = 0, accumulated = 0; current < number_of_samples;)
        {
            --offsets[selected_cluster[current]];
            if (current >= offsets[selected_cluster[current]])
            {
                while ((cluster < number_of_modes) && (current >= offsets[selected_cluster[current]]))
                {
                    accumulated += modes_histogram[cluster];
                    ++cluster;
                    current = accumulated;
                }
            }
            else
            {
                unsigned int destination = offsets[selected_cluster[current]];
                srvSwap(samples[current], samples[destination]);
                srvSwap(selected_cluster[current], selected_cluster[destination]);
                if (auxiliary != 0)
                    srvSwap(auxiliary[current], auxiliary[destination]);
            }
        }
    }
    
    /** Resorts a given set of sample vectors so that the samples of the same cluster are
     *  put together in the resulting array. Sample vectors are resorted in ascending
     *  order with respect to their cluster label and they are assigned to the cluster
     *  with the higher likelihood to belong to.
     *  \param[in,out] samples array of pointers to the sample vectors to be re-sorted.
     *  \param[in,out] cluster_likelihood array with the cluster belonging likelihood information of each sample vector.
     *  \param[in] number_of_samples number of elements in the <b>samples</b> and the <b>cluster_likelihood</b> arrays.
     *  \param[out] modes_histogram number of samples vector present at each cluster.
     *  \param[in] number_of_modes total number of clusters.
     *  \note cluster labels are expected to be between 0 and N - 1, where N is the number of modes.
     */
    template <template <class, class> class VECTOR, class T, class N, class TLIKE, class NLIKE>
    inline void resortCluster(VECTOR<T, N> * * samples, VectorSparse<TLIKE, NLIKE> * cluster_likelihood, unsigned int number_of_samples, unsigned int * modes_histogram, unsigned int number_of_modes)
    {
        resortCluster(samples, cluster_likelihood, number_of_samples, modes_histogram, number_of_modes, (char *)0);
    }
    
    /** Resorts a given set of sample vectors so that the samples of the same cluster are
     *  put together in the resulting array. Sample vectors are resorted in ascending
     *  order with respect to their cluster label and they are assigned to the cluster
     *  with the higher likelihood to belong to.
     *  \param[in,out] samples array of pointers to the sample vectors to be re-sorted.
     *  \param[in,out] cluster_likelihood array with the cluster belonging likelihood information of each sample vector.
     *  \param[in] number_of_samples number of elements in the <b>samples</b> and the <b>cluster_likelihood</b> arrays.
     *  \param[out] modes_histogram number of samples vector present at each cluster.
     *  \param[in] number_of_modes total number of clusters.
     *  \note cluster labels are expected to be between 0 and N - 1, where N is the number of modes.
     */
    template <template <class, class> class VECTOR, class T, class N, class TLIKE, class NLIKE>
    inline void resortCluster(const VECTOR<T, N> * * samples, VectorSparse<TLIKE, NLIKE> * cluster_likelihood, unsigned int number_of_samples, unsigned int * modes_histogram, unsigned int number_of_modes)
    {
        resortCluster(samples, cluster_likelihood, number_of_samples, modes_histogram, number_of_modes, (char *)0);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

