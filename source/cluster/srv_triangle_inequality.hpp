// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_TRIANGLE_INEQUALITY_TABLE_HPP_HEADER_FILE__
#define __SRV_TRIANGLE_INEQUALITY_TABLE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
#include <algorithm>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_vector.hpp"
#include "../srv_xml.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | TRIANGLE INEQUALITY CLASS            |
    //                   | ** DECLARATION **                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This class stores the distance relationships between clusters and can be used to reduce the computational cost
     *  of calculating the nearest cluster centroid by means of the triangle inequality property which says that
     *  if d(a, b) >= 2 * d(b, c) then 'a' is closer to 'b' than 'c'.
     */
    template <class TDISTANCE>
    class TriangleInequalityTable
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        TriangleInequalityTable(void);
        /** Constructor which sets the size of the distances table.
         *  \param[in] number_of_clusters number of clusters in the table.
         *  \param[in] number_of_nearest_centroids number of nearest neighbors stored for each cluster.
         */
        TriangleInequalityTable(unsigned int number_of_clusters, unsigned int number_of_nearest_centroids);
        /// Copy constructor.
        TriangleInequalityTable(const TriangleInequalityTable<TDISTANCE> &other);
        /// Destructor.
        ~TriangleInequalityTable(void);
        /// Assignation operator.
        TriangleInequalityTable<TDISTANCE>& operator=(const TriangleInequalityTable<TDISTANCE> &other);
        /** This function sets the size of the distance table.
         *  \param[in] number_of_clusters number of clusters in the table.
         *  \param[in] number_of_nearest_centroids number of nearest neighbors stored for each cluster.
         */
        void set(unsigned int number_of_clusters, unsigned int number_of_nearest_centroids); 
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the number of nearest neighbors of each centroids stored to calculate the cluster of the samples.
        inline unsigned int getNumberOfNearestCentroids(void) const { return m_number_of_nearest_centroids; }
        /// Sets the number of nearest neighbors of each centroids stored to calculate the cluster of the samples.
        inline void setNumberOfNearestCentroids(unsigned int number_of_nearest_centroids);
        /// Returns a constant pointer to the array of heaps used to sort the distances of the centroids.
        inline const typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type * getDistanceHeaps(void) const { return m_distance_heap; }
        /// Returns a constant reference to the distance heap of the index-th cluster.
        inline const typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type& getDistanceHeap(unsigned int index) const { return m_distance_heap[index]; }
        /// Returns a constant pointer to the array of constant sub-vectors with the distances of the nearest neighbors of each cluster.
        inline const ConstantSubVectorSparse<TDISTANCE, unsigned int> * getCentroidDistance(void) const { return m_centroid_distance; }
        /// Returns a constant reference to the constant sub-vectors with the distances of the nearest neighbors of the index-th cluster.
        inline const ConstantSubVectorSparse<TDISTANCE, unsigned int>& getCentroidDistance(unsigned int index) const { return m_centroid_distance[index]; }
        
        // -[ Table functions ]----------------------------------------------------------------------------------------------------------------------
        /** This function initializes the distance table.
         *  \param[in] cluster_centers cluster centers used to create the distance.
         *  \param[in] distance_object distance object used to calculate the distance between to cluster centroids.
         */
        template <template <class, class> class VECTOR, class TMODE, class N>
        void initialize(const VECTOR<TMODE, N> * cluster_centers, const VectorDistance &distance_object);
        
        /** This function calculate which is the closest centroid to the given data.
         *  \param[in] data query data vector.
         *  \param[in] cluster_centers cluster centers which are queried.
         *  \param[in] distance_object distance object used to calculate the distance between the query data and cluster centroids.
         *  \param[in] current_cluster_distance current distance estimate between the centroid and the query. Set to maximum value when no cluster is selected.
         *  \param[in] current_cluster_selected current centroid assigned to the query data. Set to 0 when no cluster is preferred.
         *  \param[out] unvisited_cluster array of boolean flags which set to true the clusters that have been visited while searching the closest centroid.
         *  \param[out] closest_distance distance between the query and the cluster centroid closer to it.
         *  \param[out] closest_selected index of the selected closest cluster centroid.
         */
        template <template <class, class> class VECTOR, template <class, class> class VECTOR_CLUSTER, class T, class N, class TMODE>
        void nearestNeighbor(const VECTOR<T, N> &data, const VECTOR_CLUSTER<TMODE, N> * cluster_centers, const VectorDistance &distance_object, TDISTANCE current_cluster_distance, unsigned int current_cluster_selected, bool * unvisited_cluster, TDISTANCE &closest_distance, unsigned int &closest_selected) const;
        
        /** This function calculate which is the closest centroid to the given data.
         *  \param[in] data query data vector.
         *  \param[in] cluster_centers cluster centers which are queried.
         *  \param[in] distance_object distance object used to calculate the distance between the query data and cluster centroids.
         *  \param[out] unvisited_cluster array of boolean flags which set to true the clusters that have been visited while searching the closest centroid.
         *  \param[out] closest_distance distance between the query and the cluster centroid closer to it.
         *  \param[out] closest_selected index of the selected closest cluster centroid.
         */
        template <template <class, class> class VECTOR, template <class, class> class VECTOR_CLUSTER, class T, class N, class TMODE>
        inline void nearestNeighbor(const VECTOR<T, N> &data, const VECTOR_CLUSTER<TMODE, N> * cluster_centers, const VectorDistance &distance_object, bool * unvisited_cluster, TDISTANCE &closest_distance, unsigned int &closest_selected) const
        {
            nearestNeighbor(data, cluster_centers, distance_object, unvisited_cluster, std::numeric_limits<TDISTANCE>::max(), 0, closest_distance, closest_selected);
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// This function stores the table information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// This function retrieves from an XML object the information of the table.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Number of clusters of the table.
        unsigned int m_number_of_clusters;
        /// Number of nearest neighbors of each centroids stored to calculate the cluster of the samples.
        unsigned int m_number_of_nearest_centroids;
        /// Heaps used to sort the distances of the centroids.
        typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type * m_distance_heap;
        /// Constant sub-vectors to the centroid distances.
        ConstantSubVectorSparse<TDISTANCE, unsigned int> * m_centroid_distance;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TRIANGLE INEQUALITY CLASS            |
    //                   | ** IMPLEMENTATION **                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TDISTANCE>
    TriangleInequalityTable<TDISTANCE>::TriangleInequalityTable(void) :
        m_number_of_clusters(0),
        m_number_of_nearest_centroids(0),
        m_distance_heap(0),
        m_centroid_distance(0)
    {
    }
    
    template <class TDISTANCE>
    TriangleInequalityTable<TDISTANCE>::TriangleInequalityTable(unsigned int number_of_clusters, unsigned int number_of_nearest_centroids) :
        m_number_of_clusters(number_of_clusters),
        m_number_of_nearest_centroids(srvMin(number_of_clusters - 1, number_of_nearest_centroids)),
        m_distance_heap((number_of_clusters > 0)?new typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type[number_of_clusters]:0),
        m_centroid_distance((number_of_clusters > 0)?new ConstantSubVectorSparse<TDISTANCE, unsigned int>[number_of_clusters]:0)
    {
        for (unsigned int i = 0; i < number_of_clusters; ++i)
        {
            m_distance_heap[i].resize(number_of_nearest_centroids);
            m_centroid_distance[i].set(m_distance_heap[i].getData(), number_of_nearest_centroids);
        }
    }
    
    template <class TDISTANCE>
    TriangleInequalityTable<TDISTANCE>::TriangleInequalityTable(const TriangleInequalityTable<TDISTANCE> &other) :
        m_number_of_clusters(other.m_number_of_clusters),
        m_number_of_nearest_centroids(other.m_number_of_nearest_centroids),
        m_distance_heap((other.m_number_of_clusters > 0)?new typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type[other.m_number_clusters]:0),
        m_centroid_distance((other.m_number_of_clusters > 0)?new ConstantSubVectorSparse<TDISTANCE, unsigned int>[other.m_number_of_clusters]:0)
    {
        for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
        {
            m_distance_heap[i] = other.m_distance_heap[i];
            m_centroid_distance[i].set(m_distance_heap[i].getData(), other.m_number_of_nearest_centroids);
        }
    }
    
    template <class TDISTANCE>
    TriangleInequalityTable<TDISTANCE>::~TriangleInequalityTable(void)
    {
        if (m_distance_heap != 0) delete [] m_distance_heap;
        if (m_centroid_distance != 0) delete [] m_centroid_distance;
    }
    
    template <class TDISTANCE>
    TriangleInequalityTable<TDISTANCE>& TriangleInequalityTable<TDISTANCE>::operator=(const TriangleInequalityTable<TDISTANCE> &other)
    {
        if (this != &other)
        {
            if (m_distance_heap != 0) delete [] m_distance_heap;
            if (m_centroid_distance != 0) delete [] m_centroid_distance;
            
            m_number_of_clusters = other.m_number_of_clusters;
            m_number_of_nearest_centroids = other.m_number_of_nearest_centroids;
            if (other.m_number_of_clusters > 0)
            {
                m_distance_heap = new typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type[other.m_number_of_clusters];
                m_centroid_distance = new ConstantSubVectorSparse<TDISTANCE, unsigned int>[other.m_number_of_clusters];
                for (unsigned int i = 0; i < other.m_number_of_clusters; ++i)
                {
                    m_distance_heap[i] = other.m_distance_heap[i];
                    m_centroid_distance[i].set(m_distance_heap[i].getData(), other.m_number_of_nearest_centroids);
                }
            }
            else
            {
                m_distance_heap = 0;
                m_centroid_distance = 0;
            }
        }
        
        return *this;
    }
    
    template <class TDISTANCE>
    void TriangleInequalityTable<TDISTANCE>::set(unsigned int number_of_clusters, unsigned int number_of_nearest_centroids)
    {
        if (m_distance_heap != 0) delete [] m_distance_heap;
        if (m_centroid_distance != 0) delete [] m_centroid_distance;
        
        m_number_of_clusters = number_of_clusters;
        m_number_of_nearest_centroids = srvMin(number_of_clusters - 1, number_of_nearest_centroids);
        if (number_of_clusters > 0)
        {
            m_distance_heap = new typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type[number_of_clusters];
            m_centroid_distance = new ConstantSubVectorSparse<TDISTANCE, unsigned int>[number_of_clusters];
            for (unsigned int i = 0; i < number_of_clusters; ++i)
            {
                m_distance_heap[i].resize(number_of_nearest_centroids);
                m_centroid_distance[i].set(m_distance_heap[i].getData(), number_of_nearest_centroids);
            }
        }
        else
        {
            m_distance_heap = 0;
            m_centroid_distance = 0;
        }
    }
    
    template <class TDISTANCE>
    template <template <class, class> class VECTOR, class TMODE, class N>
    void TriangleInequalityTable<TDISTANCE>::initialize(const VECTOR<TMODE, N> * cluster_centers, const VectorDistance &distance_object)
    {
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
            m_distance_heap[i].clear();
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
        {
            for (unsigned int j = i + 1; j < m_number_of_clusters; ++j)
            {
                TDISTANCE current_distance;
                distance_object.distance(cluster_centers[i], cluster_centers[j], current_distance);
                
                if (!m_distance_heap[i].isFull()) m_distance_heap[i].insert(Tuple<TDISTANCE, unsigned int>(current_distance, j));
                else if (m_distance_heap[i].peek().getFirst() > current_distance)
                {
                    m_distance_heap[i].remove();
                    m_distance_heap[i].insert(Tuple<TDISTANCE, unsigned int>(current_distance, j));
                }
                
                if (!m_distance_heap[j].isFull()) m_distance_heap[j].insert(Tuple<TDISTANCE, unsigned int>(current_distance, i));
                else if (m_distance_heap[j].peek().getFirst() > current_distance)
                {
                    m_distance_heap[j].remove();
                    m_distance_heap[j].insert(Tuple<TDISTANCE, unsigned int>(current_distance, i));
                }
            }
        }
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
            m_distance_heap[i].inPlaceSort();
    }
    
    template <class TDISTANCE>
    template <template <class, class> class VECTOR, template <class, class> class VECTOR_CLUSTER, class T, class N, class TMODE>
    void TriangleInequalityTable<TDISTANCE>::nearestNeighbor(const VECTOR<T, N> &data, const VECTOR_CLUSTER<TMODE, N> * cluster_centers, const VectorDistance &distance_object, TDISTANCE current_cluster_distance, unsigned int current_cluster_selected, bool * unvisited_cluster, TDISTANCE &closest_distance, unsigned int &closest_selected) const
    {
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
            unvisited_cluster[i] = true;
        if (2 * current_cluster_distance < m_centroid_distance[current_cluster_selected][0].getFirst())
        {
            closest_distance = current_cluster_distance;
            closest_selected = current_cluster_selected;
        }
        else
        {
            bool keep_searching;
            
            // Calculate the actual distance between the current cluster and the data vector ........................................................
            distance_object.distance(data, cluster_centers[current_cluster_selected], closest_distance);
            closest_selected = current_cluster_selected;
            // Initialize the search structures .....................................................................................................
            unvisited_cluster[current_cluster_selected] = false;
            /////// //
            /////// // =============================================================================================================
            /////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
            /////// VectorDense<Tuple<unsigned int, TDISTANCE> > debug_trace(m_number_of_nearest_centroids + 1);
            /////// unsigned int debug_trace_index = 1;
            /////// debug_trace[0].setData(closest_selected, closest_distance);
            /////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
            /////// // =============================================================================================================
            /////// //
            // Search for the closest centroid while the triangle inequality is hold and the current cluster has more neighbors .....................
            keep_searching = true;
            for (unsigned int index = 0; (index < m_number_of_nearest_centroids) && (keep_searching = (2 * closest_distance >= m_centroid_distance[closest_selected][index].getFirst()));)
            {
                const unsigned int current_selected = m_centroid_distance[closest_selected][index].getSecond();
                
                if (unvisited_cluster[current_selected]) // Only check for clusters whose distance has not been already calculated.
                {
                    TDISTANCE current_distance;
                    
                    distance_object.distance(data, cluster_centers[current_selected], current_distance);
                    // Change the selected cluster when a neighbor is closer than the current cluster.
                    // -------------------------------------------------------------------------------------------
                    // NOTE: The second comparison is added to generate the same result as the original k-means
                    // algorithm. Points which are equidistant are assigned to the cluster with the lower index.
                    // It would be probably better to not use the points which are equidistant or assign them
                    // to more than one cluster.
                    // -------------------------------------------------------------------------------------------
                    if ((current_distance < closest_distance) || ((current_distance == closest_distance) && (current_selected < closest_selected)))
                    {
                        closest_distance = current_distance;
                        closest_selected = current_selected;
                        /////// //
                        /////// // =============================================================================================================
                        /////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
                        /////// debug_trace[debug_trace_index].setData(closest_selected, closest_distance);
                        /////// ++debug_trace_index;
                        /////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
                        /////// // =============================================================================================================
                        /////// //
                        // Restart the search (index = 0) for the next selected cluster.
                        index = 0;
                    }
                    else ++index;
                    unvisited_cluster[current_selected] = false; // Set the current selected as a visited cluster.
                }
                else ++index;
            }
            if (keep_searching)
            {
                // When we are not sure that the closest centroid has been found, check the remaining cluster centroids.
                for (unsigned int index = 0; index < m_number_of_clusters; ++index)
                {
                    if (unvisited_cluster[index])
                    {
                        TDISTANCE current_distance;
                        
                        distance_object.distance(data, cluster_centers[index], current_distance);
                        if ((current_distance < closest_distance) || ((current_distance == closest_distance) && (index < closest_selected)))
                        {
                            closest_distance = current_distance;
                            closest_selected = index;
                        }
                        unvisited_cluster[index] = false;       // Can be useful to check how many clusters centroids are evaluated per iteration.
                    }
                }
            }
            ///////// //
            ///////// // =============================================================================================================
            ///////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
            ///////// // DEBUG: Check that the selected centroid is actually the closest centroid:
            ///////// {
            /////////     TDISTANCE minimum_distance;
            /////////     unsigned int selected;
            /////////     
            /////////     distance_object.distance(data, cluster_centers[0], minimum_distance);
            /////////     selected = 0;
            /////////     for (unsigned int i = 1; i < m_number_of_clusters; ++i)
            /////////     {
            /////////         TDISTANCE current_distance;
            /////////         distance_object.distance(data, cluster_centers[i], current_distance);
            /////////         if (current_distance < minimum_distance)
            /////////         {
            /////////             minimum_distance = current_distance;
            /////////             selected = i;
            /////////         }
            /////////     }
            /////////     if (selected != closest_selected)
            /////////     {
            /////////         std::set<unsigned int> cluster_traces;
            /////////         std::cout << std::endl;
            /////////         std::cout << "=========================================================================" << std::endl;
            /////////         std::cout << "WARNING: A different cluster has been assigned to the data vector" << std::endl;
            /////////         std::cout << "Distance between the clusters of the each path: " << std::endl;
            /////////         for (unsigned int p = 0; p < debug_trace_index; ++p)
            /////////             cluster_traces.insert(debug_trace[p].getFirst());
            /////////         for (std::set<unsigned int>::iterator begin = cluster_traces.begin(), end = cluster_traces.end(); begin != end; ++begin)
            /////////         {
            /////////             std::cout << "Cluster trace " << *begin << ":";
            /////////             for (unsigned int k = 0; k < m_number_of_nearest_centroids; ++k)
            /////////                 std::cout << " " << m_centroid_distance[*begin][k].getSecond() << "@" << m_centroid_distance[*begin][k].getFirst();
            /////////             std::cout << std::endl;
            /////////         }
            /////////         std::cout << std::endl;
            /////////         std::cout << "Search path:";
            /////////         for (unsigned int k = 0; k < debug_trace_index; ++k)
            /////////             std::cout << " " << debug_trace[k].getFirst() << "@" << debug_trace[k].getSecond();
            /////////         std::cout << std::endl;
            /////////         std::cout << "Exhaustive search: " << selected << " at " << minimum_distance << " distance units." << std::endl;
            /////////         std::cout << "Correcting the wrong choice!" << std::endl;
            /////////         closest_selected = selected;
            /////////         std::cout << "=========================================================================" << std::endl;
            /////////         std::cout << std::endl;
            /////////         std::cout << std::endl;
            /////////     }
            ///////// }
            ///////// // DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG - DEBUG
            ///////// // =============================================================================================================
            ///////// //
        }
    }
    
    template <class TDISTANCE>
    void TriangleInequalityTable<TDISTANCE>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Triangle_Inequality_Table");
        parser.setAttribute("Number_Of_Clusters", m_number_of_clusters);
        parser.setAttribute("Number_Of_Nearest_Centroids", m_number_of_nearest_centroids);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_clusters; ++i)
        {
            parser.openTag("Distance_Information");
            parser.setAttribute("ID", i);
            parser.setAttribute("Heap_Size", m_distance_heap[i].size());
            parser.addChildren();
            saveVector(parser, "Heap", m_centroid_distance[i]);
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    template <class TDISTANCE>
    void TriangleInequalityTable<TDISTANCE>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Triangle_Inequality_Table"))
        {
            if (m_distance_heap != 0) delete [] m_distance_heap;
            if (m_centroid_distance != 0) delete [] m_centroid_distance;
            m_number_of_clusters = parser.getAttribute("Number_Of_Clusters");
            m_number_of_nearest_centroids = parser.getAttribute("Number_Of_Nearest_Centroids");
            
            if (m_number_of_clusters > 0)
            {
                m_distance_heap = new typename MaxHeap<Tuple<TDISTANCE, unsigned int> >::Type[m_number_of_clusters];
                m_centroid_distance = new ConstantSubVectorSparse<TDISTANCE, unsigned int>[m_number_of_clusters];
                for (unsigned int i = 0; i < m_number_of_clusters; ++i)
                {
                    m_distance_heap.resize(m_number_of_nearest_centroids);
                    m_centroid_distance.set(m_distance_heap.getData(), m_number_of_nearest_centroids);
                }
            }
            while (!(parser.isTagIdentifier("Triangle_Inequality_Table") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Distance_Information"))
                {
                    VectorDense<Tuple<TDISTANCE, unsigned int> > xml_data;
                    unsigned int id, size;
                    
                    id = parser.getAttribute("ID");
                    size = parser.getAttribute("Heap_Size");
                    while (!(parser.isTagIdentifier("Distance_Information") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Heap"))
                            load(parser, "Heap", xml_data);
                        else parser.getNext();
                    }
                    parser.getNext();
                    
                    if (xml_data.size() != m_number_of_nearest_centroids)
                        throw Exception("Incorrect heap size.");
                    m_distance_heap[id].set(xml_data.getData(), m_number_of_nearest_centroids, size);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

