// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_UTILITIES_HPP_HEADER_FILE__
#define __SRV_UTILITIES_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <sys/time.h>
#include <exception>
#include <iostream>
#include <iomanip>
#include <cstdarg>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <cmath>
#include <set>
#include <queue>
#include <list>
#include <functional>
#include <vector>
#include <cstring>
#include <algorithm>
#include <limits>
#include <map>

namespace srv
{
    // =[ RETURN TYPE PRIORY TEMPLATES - META-PROGRAMMING ]==========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    // Template if.
    template <bool Condition, typename TrueResult, typename FalseResult> class META_IF;
    template <typename TrueResult, typename FalseResult> struct META_IF< true, TrueResult, FalseResult> { typedef  TrueResult RESULT; };
    template <typename TrueResult, typename FalseResult> struct META_IF<false, TrueResult, FalseResult> { typedef FalseResult RESULT; };
    
    // Template meta-programming function which selects the most general type of the function operator.
    template <typename FIRST, typename SECOND> struct META_MGT { typedef typename META_IF< (sizeof(FIRST) > sizeof(SECOND)), FIRST, SECOND >::RESULT TYPE; };
    template <> struct META_MGT<float, int> { typedef float TYPE; };
    template <> struct META_MGT<float, unsigned int> { typedef float TYPE; };
    template <> struct META_MGT<float, long> { typedef float TYPE; };
    template <> struct META_MGT<float, unsigned long> { typedef float TYPE; };
    template <> struct META_MGT<double, long> { typedef double TYPE; };
    template <> struct META_MGT<double, unsigned long> { typedef double TYPE; };
    template <> struct META_MGT<int, float> { typedef float TYPE; };
    template <> struct META_MGT<unsigned int, float> { typedef float TYPE; };
    template <> struct META_MGT<long, float> { typedef float TYPE; };
    template <> struct META_MGT<unsigned long, float> { typedef float TYPE; };
    template <> struct META_MGT<long, double> { typedef double TYPE; };
    template <> struct META_MGT<unsigned long, double> { typedef double TYPE; };
    
    template <typename FIRST, typename SECOND> struct META_LGT { typedef typename META_IF< (sizeof(FIRST) < sizeof(SECOND)), FIRST, SECOND >::RESULT TYPE; };
    template <> struct META_LGT<float, int> { typedef int TYPE; };
    template <> struct META_LGT<float, unsigned int> { typedef unsigned int TYPE; };
    template <> struct META_LGT<float, long> { typedef long TYPE; };
    template <> struct META_LGT<float, unsigned long> { typedef unsigned long TYPE; };
    template <> struct META_LGT<double, long> { typedef long TYPE; };
    template <> struct META_LGT<double, unsigned long> { typedef unsigned long TYPE; };
    template <> struct META_LGT<int, float> { typedef int TYPE; };
    template <> struct META_LGT<unsigned int, float> { typedef unsigned int TYPE; };
    template <> struct META_LGT<long, float> { typedef long TYPE; };
    template <> struct META_LGT<unsigned long, float> { typedef unsigned long TYPE; };
    template <> struct META_LGT<long, double> { typedef long TYPE; };
    template <> struct META_LGT<unsigned long, double> { typedef unsigned long TYPE; };
    
    template <typename T, bool CONSTANT = false> struct META_CONSTANT_TYPE { typedef typename META_IF< CONSTANT, const T, T >::RESULT TYPE; };
    
    template <typename ORIGINAL_TYPE> struct META_GENERAL_TYPE { typedef long TYPE; };
    template <> struct META_GENERAL_TYPE<float> { typedef double TYPE; };
    template <> struct META_GENERAL_TYPE<double> { typedef double TYPE; };
    
    // Meta-programming code which sets the type of the modes of the clustering algorithm.
    template <typename T> struct MODE_TYPE { typedef double TYPE; };
    template <> struct MODE_TYPE<char> { typedef int TYPE; };
    template <> struct MODE_TYPE<unsigned char> { typedef int TYPE; };
    template <> struct MODE_TYPE<short> { typedef int TYPE; };
    template <> struct MODE_TYPE<unsigned short> { typedef int TYPE; };
    template <> struct MODE_TYPE<int> { typedef long TYPE; };
    template <> struct MODE_TYPE<unsigned int> { typedef long TYPE; };
    template <> struct MODE_TYPE<long> { typedef long TYPE; };
    template <> struct MODE_TYPE<unsigned long> { typedef long TYPE; };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    //                   +--------------------------------------+
    //                   | AUXILIARY TEMPLATE FUNCTIONS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Template structure to avoid performing type deduction in function arguments (making an argument non-deductible).
    template <typename T> struct Identity { typedef T type; };
    /// Template structure which compares that the first values is higher than the second.
    template <class T> struct SrvCompareMax { inline bool operator() (const T &first, const T &second) const { return first > second; } };
    /// Template structure which compares that the first values is lower than the second.
    template <class T> struct SrvCompareMin { inline bool operator() (const T &first, const T &second) const { return first < second; } };
    /// Exchanges the values between the given parameters.
    template <class T> inline void srvSwap(T &a, T &b) { T c = a; a = b; b = c; }
    /// Returns the absolute value.
    template <class U> inline U srvAbs(const U &value) { return (value < 0)?(U)-value:value; }
    /// Returns the maximum value.
    template <class U> inline U srvMax(const U &first, const U &second) { return (first > second)?first:second; }
    /// Returns the minimum value.
    template <class U> inline U srvMin(const U &first, const U &second) { return (first < second)?first:second; }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY CLASSES AND FUNCTIONS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class Exception : public std::exception
    {
    public:
        Exception(void) : std::exception() {}
        Exception(const char *format, ...)
        {
            va_list args;
            va_start(args, format);
            vsprintf(m_message, format, args);
            va_end(args);
        }   
        virtual const char* what(void) const throw() {return m_message;}
    protected:
        char m_message[1024];
    };
    
    class ExceptionSingularity : public Exception
    {
    public:
        ExceptionSingularity(void) : Exception() {}
        ExceptionSingularity(const char *format, ...)
        {
            va_list args;
            va_start(args, format);
            vsprintf(m_message, format, args);
            va_end(args);
        }   
    };
    class ExceptionArgument : public Exception
    {
    public:
        ExceptionArgument(void) : Exception() {}
        ExceptionArgument(const char *format, ...)
        {
            va_list args;
            va_start(args, format);
            vsprintf(m_message, format, args);
            va_end(args);
        }   
    };
    class ExceptionAttribute : public Exception
    {
    public:
        ExceptionAttribute(void) : Exception() {}
        ExceptionAttribute(const char *format, ...)
        {
            va_list args;
            va_start(args, format);
            vsprintf(m_message, format, args);
            va_end(args);
        }   
    };
    
    class Chronometer
    {
    public:
        Chronometer(void) {m_start_seconds = m_start_useconds = 0;}
        ~Chronometer(void) {};
        inline void start(void)
        {
            struct timeval tempo;
            gettimeofday(&tempo, 0);
            m_start_useconds = tempo.tv_usec;
            m_start_seconds = tempo.tv_sec;
        }
        inline double getElapsedTime(void) const
        {
            struct timeval tempo;
            gettimeofday(&tempo, 0);
            return 1000.0 * (double)(tempo.tv_sec - m_start_seconds) + (double)(tempo.tv_usec - m_start_useconds) / 1000.0;
        }
        
        inline long getElapsedMilliseconds(void) const
        {
            struct timeval tempo;
            gettimeofday(&tempo, 0);
            return 1000000 * (tempo.tv_sec - m_start_seconds) + (tempo.tv_usec - m_start_useconds);
        }
    private:
        long m_start_useconds;
        long m_start_seconds;
    };
    
    /// Simple chained list class.
    template <class T>
    class ChainedList
    {
    public:
        /// Default constructor.
        ChainedList(void) : m_next(0) {}
        /// Constructor which creates a new node pointing to the specified node of the list.
        ChainedList(ChainedList<T> * next) : m_next(next) {}
        /** Parameters constructor
         *  \param[in] value value added in the node.
         *  \param[in] next pointer to the next node of the chained list.
         */
        ChainedList(const T &value, ChainedList<T> *next = 0) : m_value(value), m_next(next) {}
        /// Copy constructor.
        ChainedList(const ChainedList<T> &cl)
        {
            m_value = cl.m_value;
            if (cl.m_next != 0) m_next = new ChainedList<T>(*cl.m_next);
            else m_next = 0;
        }
        /// Destructor.
        ~ChainedList(void)
        {
            // Simple recursive destructor, this destructor can have heap problems for too long lists (too many recursion levels).
            if (m_next != 0) delete m_next;
        }
        /// Assignation operator.
        ChainedList<T>* operator=(const ChainedList<T> &cl)
        {
            if (this != &cl)
            {
                if (m_next != 0) delete m_next;
                m_value = cl.m_value;
                m_next = (cl.m_next != 0)?new ChainedList<T>(*cl.m_next):0;
            }
            return *this;
        }
        /** Add a new node to the chained list.
         *  \param[in] current head node of the chained list.
         *  \param[in] value value to be added in the chained list.
         */
        friend inline void add(ChainedList<T> *&current, const T &value)
        {
            current = new ChainedList<T>(value, current);
        }
        /// Returns a reference to the data value of the chained list node.
        inline T& getData(void) { return m_value; }
        /// Returns a constant reference to the data value of the chained list node.
        inline const T& getData(void) const {return m_value;}
        /// Returns a pointer to the next node of the chained list.
        inline ChainedList<T>* getNext(void) {return m_next;}
        /// Returns a constant pointer to the next node of the chained list.
        inline const ChainedList<T>* getNext(void) const {return m_next;}
        /// Creates a new node at the current location of the list.
        inline void addNext(void) { m_next = new ChainedList<T>(m_next); }
        /// Creates a new node at the current location of the list.
        inline void addNext(const T &value) { m_next = new ChainedList<T>(value, m_next); }
        /// Sets the value of the chained list node.
        inline void setData(const T &value) { m_value = value; }
        /** Sets the pointer to the next node of the chained list node.
         *  \param[in] next pointer to the next chained list node.
         */
        inline void setNext(ChainedList<T> *next) { m_next = next; }
    private:
        /// Value stored in the node.
        T m_value;
        /// Pointer to the next chained list node.
        ChainedList<T> *m_next;
    };
    
    /// Iterative free function for chained lists.
    template <class T>
    void freeChainedList(ChainedList<T> *current)
    {
        ChainedList<T> *next = current;
        while (next != 0)
        {
            next = current->getNext();
            current->setNext(0);
            delete current;
            current = next;
        }
    }
    
    void verboseMessage(std::ostream *output, const char *format, ...);
    
    /** This function converts to string the given value and add the thousand separation comas.
     *  \param[in] value input integer value.
     *  \param[out] output char array with the formated integer.
     */
    void thousandSeparator(long value, char * output);
    /** This function converts to string the given value and add the thousand separation comas.
     *  \param[in] value input integer value.
     *  \returns a standard string with the formated integer.
     */
    inline std::string thousandSeparator(long value) { char output[512]; thousandSeparator(value, output); return output; }
    
    /// Two elements tuple container class.
    template <class T1, class T2>
    class Tuple
    {
    public:
        /// Default constructor.
        Tuple(void) {}
        /// Parameter constructor where the two elements are given as parameters.
        Tuple(const T1 &first, const T2 &second) :
            m_first(first),
            m_second(second) {}
        /// Copy constructors.
        Tuple(const Tuple<T1, T2> &other) :
            m_first(other.m_first),
            m_second(other.m_second) {}
        /// Copy constructor for elements of a different data-type.
        template <class DT1, class DT2>
        Tuple(const Tuple<DT1, DT2> &other) :
            m_first((T1)other.getFirst()),
            m_second((T2)other.getSecond()) {}
        /// Assignation operator.
        Tuple<T1, T2>& operator=(const Tuple<T1, T2> &other)
        {
            if (this != &other)
            {
                m_first = other.m_first;
                m_second = other.m_second;
            }
            return *this;
        }
        /// Assignation operator for elements of a different data type.
        template <class DT1, class DT2>
        Tuple<T1, T2>& operator=(const Tuple<DT1, DT2> &other)
        {
            m_first = (T1)other.getFirst();
            m_second = (T2)other.getSecond();
            return *this;
        }
        /// Returns a constant reference to the first element of the tuple.
        inline const T1& getFirst(void) const { return m_first; }
        /// Returns a reference to the first element of the tuple
        inline T1& getFirst(void) { return m_first; }
        /// Returns a constant reference to the second element of the tuple.
        inline const T2& getSecond(void) const { return m_second; }
        /// Returns a reference to the second element of the tuple.
        inline T2& getSecond(void) { return m_second; }
        /// Sets the first element of the tuple.
        inline void setFirst(const T1 &first) { m_first = first; }
        /// Sets the second element of the tuple.
        inline void setSecond(const T2 &second) { m_second = second; }
        /// Sets both elements of the tuple.
        inline void setData(const T1 &first, const T2 &second)
        {
            m_first = first;
            m_second = second;
        }
        /// Equal operator between elements of the same type.
        inline bool operator==(const Tuple<T1, T2> &other) const {return (m_first == other.m_first) && (m_second == other.m_second);}
        /// Equal operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator==(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first == (RT1)other.getFirst()) && ((RT2)m_second == (RT2)other.getSecond());
        }
        /// Different operator between elements of the same type.
        inline bool operator!=(const Tuple<T1, T2> &other) const {return (m_first != other.m_first) || (m_second != other.m_second);}
        /// Different operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator!=(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first != (RT1)other.getFirst()) || ((RT2)m_second != (RT2)other.getSecond());
        }
        /// Lesser operator between elements of the same type.
        inline bool operator<(const Tuple<T1, T2> &other)  const {return (m_first < other.m_first) || ((m_first == other.m_first) && (m_second < other.m_second));}
        /// Lesser operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator<(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first < (RT1)other.getFirst()) || (((RT1)m_first == (RT1)other.getFirst()) && ((RT2)m_second < (RT2)other.getSecond()));
        }
        /// Lesser or equal operator between elements of the same type.
        inline bool operator<=(const Tuple<T1, T2> &other) const {return (m_first < other.m_first) || ((m_first == other.m_first) && (m_second <= other.m_second));}
        /// Lesser or equal operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator<=(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first < (RT1)other.getFirst()) || (((RT1)m_first == (RT1)other.getFirst()) && ((RT2)m_second <= (RT2)other.getSecond()));
        }
        /// Greater operator between elements of the same type.
        inline bool operator>(const Tuple<T1, T2> &other)  const {return (m_first > other.m_first) || ((m_first == other.m_first) && (m_second > other.m_second));}
        /// Greater operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator>(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first > (RT1)other.getFirst()) || (((RT1)m_first == (RT1)other.getFirst()) && ((RT2)m_second > (RT2)other.getSecond()));
        }
        /// Greater or equal or equal operator between elements of the same type.
        inline bool operator>=(const Tuple<T1, T2> &other) const {return (m_first > other.m_first) || ((m_first == other.m_first) && (m_second >= other.m_second));}
        /// Greater or equal operator between elements of different data types (compare the content of the tuples).
        template <class DT1, class DT2>
        inline bool operator>=(const Tuple<DT1, DT2> &other) const
        {
            // 1) Get the most general data type using meta-programming.
            typedef typename META_MGT<T1, DT1>::TYPE RT1;
            typedef typename META_MGT<T2, DT2>::TYPE RT2;
            // 2) Return the operator result.
            return ((RT1)m_first > (RT1)other.getFirst()) || (((RT1)m_first == (RT1)other.getFirst()) && ((RT2)m_second >= (RT2)other.getSecond()));
        }
        /// Sets the first and the second elements of the tuple to zero.
        inline void setToZero(void) // This function is set to avoid problems with padding and Valgrind (conditional jump or move depends...).
        {
            const unsigned int size = sizeof(*this);
            char * padding_ptr = (char *)this;
            for (unsigned int i = 0; i < size; ++i) padding_ptr[i] = 0;
        }
        
    private:
        /// First element of the tuple.
        T1 m_first;
        /// Second element of the tuple.
        T2 m_second;
    };
    /// Overloads the <b><<</b> operator to put the values of the tuple into an output stream.
    template <class T1, class T2>
    inline std::ostream& operator<<(std::ostream &out, const Tuple<T1, T2> &tuple)
    {
        out << tuple.getFirst() << ":" << tuple.getSecond();
        return out;
    }
    /// Overloads the <b>>></b> operator to get the values of the tuple from an input stream.
    template <class T1, class T2>
    inline std::istream& operator>>(std::istream &in, Tuple<T1, T2> &tuple)
    {
        char aux;
        in >> tuple.getFirst() >> aux >> tuple.getSecond();
        return in;
    }
    
    template <class T1, class T2, class T3>
    class Triplet
    {
    public:
        Triplet(void) {}
        Triplet(const T1 &first, const T2 &second, const T3 &third) :
            m_first(first),
            m_second(second),
            m_third(third) {}
        inline const T1& getFirst(void) const {return m_first;}
        inline T1& getFirst(void) {return m_first;}
        inline const T2& getSecond(void) const {return m_second;}
        inline T2& getSecond(void) {return m_second;}
        inline const T3& getThird(void) const {return m_third;}
        inline T3& getThird(void) {return m_third;}
        inline void setFirst(const T1 &first) {m_first = first;}
        inline void setSecond(const T2 &second) {m_second = second;}
        inline void setThird(const T3 &third) {m_third = third;}
        inline void setData(const T1 &first, const T2 &second, const T3 &third)
        {
            m_first = first;
            m_second = second;
            m_third = third;
        }
        inline bool operator==(const Triplet<T1, T2, T3> &triplet) const {return (m_first == triplet.m_first) && (m_second == triplet.m_second) && (m_third == triplet.m_third);}
        inline bool operator!=(const Triplet<T1, T2, T3> &triplet) const {return !(*this == triplet);}
        inline bool operator<(const Triplet<T1, T2, T3> &triplet)  const {return (m_first < triplet.m_first) || ((m_first == triplet.m_first) && (m_second < triplet.m_second)) || ((m_first == triplet.m_first) && (m_second == triplet.m_second) && (m_third < triplet.m_third));}
        inline bool operator<=(const Triplet<T1, T2, T3> &triplet) const {return (*this == triplet) || (*this < triplet);}
        inline bool operator>(const Triplet<T1, T2, T3> &triplet)  const {return !(*this <= triplet);}
        inline bool operator>=(const Triplet<T1, T2, T3> &triplet) const {return !(*this < triplet);}
        
        friend std::ostream& operator<<(std::ostream &out, const Triplet<T1, T2, T3> &triplet)
        {
            out << triplet.m_first << " " << triplet.m_second << " " << triplet.m_third;
            return out;
        }
        friend std::istream& operator>>(std::istream &in, Triplet<T1, T2, T3> &triplet)
        {
            in >> triplet.m_first >> triplet.m_second >> triplet.m_third;
            return in;
        }
    private:
        T1 m_first;
        T2 m_second;
        T3 m_third;
    };
    
    /// Converts a long decimal number to a base 62 number stored in a char array.
    inline void convertLongBase62(char *buffer, long value, unsigned int length)
    {
        const char characters[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        bool negative(value < 0);
        value = (value < 0)?-value:value;
        for (unsigned int i = 0; i < length - 1; ++i)
        {
            buffer[length - i - 1] = characters[value % 62];
            value /= 62;
        }
        if (negative) buffer[0] = characters[value % 62 + 31];
        else buffer[0] = characters[value % 62];
    }
    
    /** Converts a float number to a base 62 number stored in an char array. The length
     *  parameter set the amount of significant bytes which will be used in the base
     *  62 conversion.
     *  \param[in] buffer char array where the resulting base 62 number is stored.
     *  \param[in] value value to be converted to base 62.
     *  \param[in] length size of the buffer array.
     */
    inline void convertDoubleBase62(char *buffer, double value, unsigned int length)
    {
        if (value != 0)
        {
            long exponent, significand;
            // Float bits: 1 bit (sign) + 8 bits (signed exponent) + 23 bits (significand)
            // Base62: 2 bytes (exponent) + length - 2 bytes (significand).
            exponent = (long)floor(log10(srvAbs(value)));
            significand = (long)(value / pow(10.0, (double)exponent - (double)length + 3.0));
            convertLongBase62(buffer, exponent, 2);
            convertLongBase62(&buffer[2], significand, length - 2);
        }
        else for (unsigned int i = 0; i < length; ++i) buffer[i] = '_';
    }
    
    /// Converts a char array which stores a 62 base number to a long decimal number.
    inline long convertBase62Long(const char *buffer, unsigned int length)
    {
        char c;
        long value;
        bool negative;
        value = 0;
        c = (char)(buffer[0] - 48);
        if (c > 10) c = (char)(c - 7);
        if (c > 36) c = (char)(c - 6);
        if (c >= 31)
        {
            negative = true;
            c = (char)(c - 31);
        }
        else negative = false;
        value = (long)c;
        for (unsigned int i = 1; i < length; ++i)
        {
            c = (char)(buffer[i] - 48);
            if (c > 10) c = (char)(c - 7);
            if (c > 36) c = (char)(c - 6);
            value = 62 * value + (long)c;
        }
        return (negative)?-value:value;
    }
    
    /// Converts a char array which double value in base 62 to its original floating value.
    inline double convertBase62Double(const char *buffer, unsigned int length)
    {
        long exponent, significand;
        if (buffer[0] == '_') return 0.0;
        exponent = convertBase62Long(buffer, 2);
        significand = convertBase62Long(&buffer[2], length - 2);
        return (double)significand * pow(10.0, (double)exponent - (double)length + 3.0);
    }
    
    inline int strLeftCut(const char *origin, char *destination, char character)
    {
        int index;
        for (index = 0; origin[index] != '\0'; ++index)
            destination[index] = origin[index];
        for (; index > 0; --index)
            if (destination[index - 1] == character) break;
        destination[index] = '\0';
        return index;
    }
    
    inline int strRightCut(const char *origin, char *destination, char character)
    {
        int index, destination_index;
        for (index = 0; origin[index] != '\0'; ++index); // Go to the end of the string.
        for (; (index >= 0) && (origin[index] != character); --index);
        ++index;
        for (destination_index = 0; origin[index] != '\0'; ++index, ++destination_index)
            destination[destination_index] = origin[index];
        destination[destination_index] = '\0';
        return index - destination_index;
    }
    
    inline int getFilenameRoute(const char *filename, char *route) { return strLeftCut(filename, route, '/'); }
    inline int getFilenameExtension(const char *filename, char *extension) { return strRightCut(filename, extension, '.'); }
    
    inline unsigned int copy_str(const char *origin, char *destination, unsigned int max_size, unsigned int destination_index = 0)
    {
        for (unsigned int index = 0; (destination_index < max_size - 1) && (origin[index] != '\0'); ++index, ++destination_index) destination[destination_index] = origin[index];
        destination[destination_index] = '\0';
        return destination_index;
    }
    
    inline unsigned char inverted_exp(double value)
    {
        const unsigned int lut_size = 555;
        unsigned char exp_lut[lut_size] = {255, 252, 249, 247, 245, 242, 240, 237, 235, 233, 230, 228, 226, 223, 221, 219, 217, 215, 212, 210, 208, 206, 204,
                          202, 200, 198, 196, 194, 192, 190, 188, 187, 185, 183, 181, 179, 177, 176, 174, 172, 170, 169, 167, 165, 164, 162,
                          160, 159, 157, 156, 154, 153, 151, 150, 148, 147, 145, 144, 142, 141, 139, 138, 137, 135, 134, 133, 131, 130, 129,
                          127, 126, 125, 124, 122, 121, 120, 119, 118, 116, 115, 114, 113, 112, 111, 110, 108, 107, 106, 105, 104, 103, 102,
                          101, 100, 99, 98, 97, 96, 95, 94, 93, 92, 91, 91, 90, 89, 88, 87, 86, 85, 84, 84, 83, 82, 81, 80, 79, 79, 78, 77,
                          76, 76, 75, 74, 73, 73, 72, 71, 70, 70, 69, 68, 68, 67, 66, 66, 65, 64, 64, 63, 62, 62, 61, 61, 60, 59, 59, 58, 58,
                          57, 56, 56, 55, 55, 54, 54, 53, 53, 52, 52, 51, 50, 50, 49, 49, 48, 48, 48, 47, 47, 46, 46, 45, 45, 44, 44, 43, 43,
                          43, 42, 42, 41, 41, 40, 40, 40, 39, 39, 38, 38, 38, 37, 37, 37, 36, 36, 35, 35, 35, 34, 34, 34, 33, 33, 33, 32, 32,
                          32, 31, 31, 31, 30, 30, 30, 30, 29, 29, 29, 28, 28, 28, 27, 27, 27, 27, 26, 26, 26, 26, 25, 25, 25, 25, 24, 24, 24,
                          24, 23, 23, 23, 23, 22, 22, 22, 22, 22, 21, 21, 21, 21, 20, 20, 20, 20, 20, 19, 19, 19, 19, 19, 18, 18, 18, 18, 18,
                          18, 17, 17, 17, 17, 17, 16, 16, 16, 16, 16, 16, 15, 15, 15, 15, 15, 15, 15, 14, 14, 14, 14, 14, 14, 14, 13, 13, 13,
                          13, 13, 13, 13, 12, 12, 12, 12, 12, 12, 12, 12, 11, 11, 11, 11, 11, 11, 11, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10,
                          10, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 9, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 6,
                          6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 4, 4, 4, 4, 4, 4,
                          4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
                          3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                          2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                          1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
        return (value <= 5.55)?exp_lut[(int)(value / 100.0)]:(unsigned char)0;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CIRCULAR QUEUE                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    class NodeSingle
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        NodeSingle(void) : m_next(0) {}
        NodeSingle(const T &data) : m_data(data), m_next(0) {}
        NodeSingle(const T &data, NodeSingle *next) : m_data(data), m_next(next) {}
        NodeSingle(const NodeSingle &copy) : m_data(copy.m_data), m_next((copy.m_next != 0)?new NodeSingle(*copy.m_next):0) {}
        ~NodeSingle(void) { if (m_next != 0) delete m_next; }
        NodeSingle& operator=(const NodeSingle &copy)
        {
            if (this != &copy)
            {
                if (m_next != 0) delete m_next;
                m_data = copy.m_data;
                m_next = (copy.m_next != 0)?new NodeSingle(*copy.m_next):0;
            }
            return *this;
        }
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        inline const T& getData(void) const { return m_data; }
        inline T& getData(void) { return m_data; }
        inline const NodeSingle* getNext(void) const { return m_next; }
        inline NodeSingle* getNext(void) { return m_next; }
        inline void setNext(NodeSingle *next) { m_next = next; }
    private:
        T m_data;
        NodeSingle *m_next;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | QUEUE                                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    class Queue
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        Queue(void) : m_first(0), m_last(0), m_size(0) {}
        Queue(const Queue &copy) :
            m_first((copy.m_first != 0)?new NodeSingle<T>(*copy.m_first):0),
            m_last(0),
            m_size(copy.m_size) { if (m_first != 0) for (m_last = m_first; m_last->getNext() != 0; m_last = m_last->getNext()); }
        ~Queue(void) { if (m_first != 0) delete m_first; }
        Queue& operator=(const Queue &copy)
        {
            if (this != &copy)
            {
                if (m_first != 0) delete m_first;
                m_size = copy.m_size;
                if (copy.m_first != 0)
                {
                    m_first = m_last = new NodeSingle<T>(*copy.m_first);
                    for (; m_last->getNext() != 0; m_last = m_last->getNext());
                }
                else m_first = m_last = 0;
            }
            return *this;
        }
        
        // -[ Allocation ]---------------------------------------------------------------------------------------------------------------------------
        inline bool isEmpty(void) const { return m_first == 0; }
        inline unsigned int size(void) const { return m_size; }
        
        // -[ Operators ]----------------------------------------------------------------------------------------------------------------------------
        void insert(const T &element)
        {
            if (m_first != 0) { m_last->setNext(new NodeSingle<T>(element)); m_last = m_last->getNext(); }
            else m_last = m_first = new NodeSingle<T>(element);
            ++m_size;
        }
        inline void remove(void)
        {
            if (m_last != m_first)
            {
                NodeSingle<T> *aux = m_first;
                m_first = m_first->getNext();
                aux->setNext(0);
                delete aux;
                --m_size;
            }
            else if (m_first != 0) { delete m_first; m_first = m_last = 0; m_size = 0; }
        }
        const T& peek(void) const { return m_first->getData(); }
        T& peek(void) { return m_first->getData(); }
    private:
        NodeSingle<T> *m_first;
        NodeSingle<T> *m_last;
        unsigned int m_size;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | STACK                                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    class Stack
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        Stack(void) : m_top(0), m_size(0) {}
        Stack(const Stack &copy) :
            m_top((copy.m_top != 0)?new NodeSingle<T>(*copy.m_top):0),
            m_size(copy.m_size) {}
        ~Stack(void) { if (m_top != 0) delete m_top; }
        Stack& operator=(const Stack &copy)
        {
            if (this != &copy)
            {
                if (m_top != 0) delete m_top;
                m_size = copy.m_size;
                if (copy.m_first != 0) m_top = new NodeSingle<T>(*copy.m_top);
                else m_top = 0;
            }
            return *this;
        }
        
        // -[ Allocation ]---------------------------------------------------------------------------------------------------------------------------
        inline bool isEmpty(void) const { return m_top == 0; }
        inline unsigned int size(void) const { return m_size; }
        
        // -[ Operators ]----------------------------------------------------------------------------------------------------------------------------
        void insert(const T &element)
        {
            m_top = new NodeSingle<T>(element, m_top);
            ++m_size;
        }
        inline void remove(void)
        {
            if (m_top != 0)
            {
                NodeSingle<T> *aux = m_top;
                m_top = m_top->getNext();
                aux->setNext(0);
                delete aux;
                --m_size;
            }
        }
        const T& peek(void) const { return m_top->getData(); }
        T& peek(void) { return m_top->getData(); }
    private:
        NodeSingle<T> *m_top;
        unsigned int m_size;
    };
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CIRCULAR QUEUE                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Circular queue template class implementation.
    template <class T>
    class CircularQueue
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CircularQueue(void) : m_data(0), m_capacity(0), m_first(0), m_last(0) {}
        /// Constructor where the number of elements of the circular queue is given.
        CircularQueue(unsigned int queue_capacity) : m_data(new T[queue_capacity + 1]), m_capacity(queue_capacity + 1), m_first(0), m_last(0) {}
        /// Copy constructor.
        CircularQueue(const CircularQueue<T> &copy) :
            m_data((copy.m_data != 0)?new T[copy.m_capacity]:0),
            m_capacity(copy.m_capacity),
            m_first(copy.m_first),
            m_last(copy.m_last) { for (unsigned int i = 0; i < copy.m_capacity; ++i) m_data[i] = copy.m_data[i]; }
        /// Destructor.
        ~CircularQueue(void) { if (m_data != 0) delete [] m_data; }
        /// Assignation operator.
        CircularQueue<T>& operator=(const CircularQueue<T> &copy)
        {
            if (&copy != this)
            {
                // Free .............................................................................................................................
                if (m_data != 0) delete [] m_data;
                // Copy .............................................................................................................................
                if (copy.m_data != 0)
                {
                    m_data = new T[copy.m_capacity];
                    m_capacity = copy.m_capacity;
                    m_first = copy.m_first;
                    m_last = copy.m_last;
                    for (unsigned int i = 0; i < copy.m_capacity; ++i) m_data[i] = copy.m_data[i];
                }
                else
                {
                    m_data = 0;
                    m_capacity = m_first = m_last = 0;
                }
            }
            return *this;
        }
        
        // -[ Allocation ]---------------------------------------------------------------------------------------------------------------------------
        /// Returns a boolean which is set to \b true when the queue is empty.
        inline bool isEmpty(void) const { return m_first == m_last; }
        /// Returns a boolean which is set to \b true when the queue is full.
        inline bool isFull(void) const { return (m_last + 1) % m_capacity == m_first; }
        /// Returns the number of elements in the circular queue.
        inline unsigned int size(void) const { return (m_first > m_last)?m_capacity - m_first + m_last:m_last - m_first; }
        /// Returns the total capacity of the circular queue.
        inline unsigned int capacity(void) const { return m_capacity - 1; }
        inline void clear(void) { m_first = m_last = 0; }
        /// Resizes the capacity of the circular queue.
        void resize(unsigned int queue_capacity)
        {
            if (m_data != 0) delete [] m_data;
            m_data = new T[queue_capacity + 1];
            m_capacity = queue_capacity + 1;
            m_first = m_last = 0;
        }
        
        // -[ Operators ]----------------------------------------------------------------------------------------------------------------------------
        /// Inserts an element to the circular queue. When the queue is full, the oldest element is overwritten.
        void insert(const T &element)
        {
            m_data[m_last] = element;
            m_last = (m_last + 1) % m_capacity;
            if (m_first == m_last) m_first = (m_first + 1) % m_capacity;
        }
        /// Removes the oldest element of the circular queue.
        inline void remove(void) { if (m_first != m_last) m_first = (m_first + 1) % m_capacity; }
        /// Returns a constant reference to the oldest element of the circular queue.
        const T& peek(void) const { return m_data[m_first]; }
        /// Returns a reference to the oldest element of the circular queue.
        T& peek(void) { return m_data[m_first]; }
    private:
        /// Array which stores the circular queue data.
        T *m_data;
        /// Total capacity of the circular queue.
        unsigned int m_capacity;
        /// Location of the oldest location of the circular queue.
        unsigned int m_first;
        /// Location where the newest element will be inserted in the circular queue.
        unsigned int m_last;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | STATIC STACK                         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Circular stack template class implementation.
    template <class T>
    class CircularStack
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CircularStack(void) : m_data(0), m_capacity(0), m_first(0), m_last(0) {}
        /// Parameters constructor where the capacity of the circular stack is specified.
        CircularStack(unsigned int stack_capacity) : m_data(new T[stack_capacity + 1]), m_capacity(stack_capacity + 1), m_first(0), m_last(0) {}
        /// Copy constructor.
        CircularStack(const CircularStack &copy) :
            m_data((copy.m_data != 0)?new T[copy.m_capacity]:0),
            m_capacity(copy.m_capacity),
            m_first(copy.m_first),
            m_last(copy.m_last) { for (unsigned int i = 0; i < copy.m_capacity; ++i) m_data[i] = copy.m_data[i]; }
        /// Destructor.
        ~CircularStack(void) { if (m_data != 0) delete [] m_data; }
        /// Assignation operator.
        CircularStack& operator=(const CircularStack &copy)
        {
            if (&copy != this)
            {
                // Free .............................................................................................................................
                if (m_data != 0) delete [] m_data;
                // Copy .............................................................................................................................
                if (copy.m_data != 0)
                {
                    m_data = new T[copy.m_capacity];
                    m_capacity = copy.m_capacity;
                    m_first = copy.m_first;
                    m_last = copy.m_last;
                    for (unsigned int i = 0; i < copy.m_capacity; ++i) m_data[i] = copy.m_data[i];
                }
                else
                {
                    m_data = 0;
                    m_capacity = m_first = m_last = 0;
                }
            }
            return *this;
        }
        
        // -[ Allocation ]---------------------------------------------------------------------------------------------------------------------------
        /// Returns a boolean which is set to \b true when the stack is empty.
        inline bool isEmpty(void) const { return m_first == m_last; }
        /// Returns a boolean which is set to \b true when the stack is full.
        inline bool isFull(void) const { return (m_last + 1) % m_capacity == m_first; }
        /// Returns the number of elements in the circular stack.
        inline unsigned int size(void) const { return (m_first > m_last)?m_capacity - m_first + m_last:m_last - m_first; }
        /// Returns the total capacity of the circular stack.
        inline unsigned int capacity(void) const { return m_capacity - 1; }
        /// Resizes the capacity of the circular stack.
        void resize(unsigned int stack_capacity)
        {
            if (m_data != 0) delete [] m_data;
            m_data = new T[stack_capacity + 1];
            m_capacity = stack_capacity + 1;
            m_first = m_last = 0;
        }
        
        // -[ Operators ]----------------------------------------------------------------------------------------------------------------------------
        /// Inserts an element to the circular stack. When the stack is full, the oldest element is overwritten.
        void insert(const T &element)
        {
            m_data[m_last] = element;
            m_last = (m_last + 1) % m_capacity;
            if (m_first == m_last) m_first = (m_first + 1) % m_capacity;
        }
        /// Removes the newest element of the circular stack.
        inline void remove(void) { if (m_first != m_last) m_last = (m_last == 0)?m_capacity - 1:m_last - 1; }
        /// Returns a constant reference to the newest element of the circular stack.
        const T& peek(void) const { return (m_last == 0)?m_data[m_capacity - 1]:m_data[m_last - 1]; }
        /// Returns a reference to the newest element of the circular stack.
        T& peek(void) { return (m_last == 0)?m_data[m_capacity - 1]:m_data[m_last - 1]; }
    private:
        /// Array which stores the circular stack data.
        T *m_data;
        /// Total capacity of the circular stack.
        unsigned int m_capacity;
        /// Location of the oldest element of the circular stack.
        unsigned int m_first;
        /// Location where a new element of the circular stack will be stored.
        unsigned int m_last;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | HEAPS                                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Static heap class (i.e. heap implemented over an static array so that it has a maximum capacity).
    template <class T, class COMPARE = SrvCompareMin<T> >
    class Heap
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        Heap(void) : m_data(0), m_number_of_elements(0), m_last(1) {}
        /// Constructor which specifies the maximum capacity of the heap.
        Heap(unsigned int number_of_elements) :
            m_data((number_of_elements > 0)?new T[number_of_elements + 1]:0),
            m_number_of_elements(number_of_elements + 1),
            m_last(1) {}
        /// Copy constructor.
        Heap(const Heap &copy) :
            m_data((copy.m_number_of_elements > 0)?new T[copy.m_number_of_elements]:0),
            m_number_of_elements(copy.m_number_of_elements),
            m_last(copy.m_last) { for (unsigned int i = 1; i < copy.m_number_of_elements; ++i) m_data[i] = copy.m_data[i]; }
        /// Destructor.
        ~Heap(void) { if (m_data != 0) delete [] m_data; }
        /// Assignation operator.
        Heap& operator=(const Heap &copy)
        {
            if (this != &copy)
            {
                if (m_data != 0) delete [] m_data;
                if (copy.m_number_of_elements != 0)
                {
                    m_data = new T[copy.m_number_of_elements];
                    m_number_of_elements = copy.m_number_of_elements;
                    m_last = copy.m_last;
                    for (unsigned int i = 1; i < copy.m_number_of_elements; ++i) m_data[i] = copy.m_data[i];
                }
                else { m_data = 0; m_number_of_elements = 0; m_last = 1; }
            }
            return *this;
        }
        // -[ Heap allocation ]----------------------------------------------------------------------------------------------------------------------
        /// Returns the capacity of the heap.
        inline unsigned int capacity(void) const { return m_number_of_elements - 1; }
        /// Returns the number of the elements stored in the heap.
        inline unsigned int size(void) const { return m_last - 1; }
        /// Returns a boolean flag which is true when the heap is empty.
        inline bool isEmpty(void) const { return m_last == 1; }
        /// Returns a boolean flag which is true when the heap is full.
        inline bool isFull(void) const { return m_last == m_number_of_elements; }
        /// Removes all the elements of the heap.
        inline void clear(void) { m_last = 1; }
        /// Changes the capacity of the heap.
        inline void resize(unsigned int number_of_elements)
        {
            if (m_data != 0) delete [] m_data;
            m_last = 1;
            if (number_of_elements != 0)
            {
                m_data = new T[number_of_elements + 1];
                m_number_of_elements = number_of_elements + 1;
            }
            else { m_data = 0; m_number_of_elements = 0; }
        }
        // -[ Heap operators ]-----------------------------------------------------------------------------------------------------------------------
        /// Inserts a new element into the heap.
        void insert(const T &element)
        {
            unsigned int current;
            COMPARE _compare;
            // 1) Insert the element in the last position.
            current = m_last;
            if (m_last >= m_number_of_elements) throw srv::Exception("ERROR"); //std::cout << "Mierda pa tiiiii:" << m_last << std::endl;
            m_data[m_last] = element;
            ++m_last;
            // 2) Compare the added element with its parent; if they are in the correct order, stop.
            while ((current / 2 > 0) && _compare(m_data[current], m_data[current / 2]))
            {
                srvSwap(m_data[current], m_data[current / 2]);
                current /= 2;
            }
        }
        /// Removes the peek of the heap.
        void remove(void)
        {
            unsigned int current;
            COMPARE _compare;
            
            // 1) Replace the root of the heap with the last element of the last level
            --m_last;
            m_data[1] = m_data[m_last];
            current = 1;
            // 2) Compare the new root with its children; if they are in the correct order stop.
            while (current < m_last)
            {
                unsigned int left, right, selected;
                
                left = 2 * current;
                right = 2 * current + 1;
                selected = current;
                if ((left < m_last) && (!_compare(m_data[selected], m_data[left]))) selected = left;
                if ((right < m_last) && (!_compare(m_data[selected], m_data[right]))) selected = right;
                if (current == selected) break; // They are in the correct order.
                srvSwap(m_data[current], m_data[selected]);
                current = selected;
            }
        }
        /** Removes all the elements of the heap by performing and in-place sort operation.
         *  To access to the sorted elements, we can use the <b>getData()</b> function.
         */
        void inPlaceSort(void)
        {
            unsigned int current;
            COMPARE _compare;
            
            while (m_last != 1) // While the heap is not empty.
            {
                // 1) Replace the root of the heap with the last element of the last level
                --m_last;
                srvSwap(m_data[1], m_data[m_last]);
                current = 1;
                // 2) Compare the new root with its children; if they are in the correct order stop.
                while (current < m_last)
                {
                    unsigned int left, right, selected;
                    
                    left = 2 * current;
                    right = 2 * current + 1;
                    selected = current;
                    if ((left < m_last) && (!_compare(m_data[selected], m_data[left]))) selected = left;
                    if ((right < m_last) && (!_compare(m_data[selected], m_data[right]))) selected = right;
                    if (current == selected) break; // They are in the correct order.
                    srvSwap(m_data[current], m_data[selected]);
                    current = selected;
                }
            }
        }
        /** Returns a sorted array of the elements stored in the heap.
         *  \param[out] data array with the sorted heap elements. This array is expected to have at least as many elements as the size of the heap.
         */
        void sort(T * data)
        {
            unsigned int current, last;
            COMPARE _compare;
            
            for (unsigned int i = 1; i < m_last; ++i)
                data[i - 1] = m_data[i];
            last = m_last;
            while (last != 1) // While the heap is not empty.
            {
                // 1) Replace the root of the heap with the last element of the last level
                --last;
                srvSwap(data[0], data[last - 1]);
                current = 1;
                // 2) Compare the new root with its children; if they are in the correct order stop.
                while (current < last)
                {
                    unsigned int left, right, selected;
                    
                    left = 2 * current;
                    right = 2 * current + 1;
                    selected = current;
                    if ((left < last) && (!_compare(data[selected - 1], data[left - 1]))) selected = left;
                    if ((right < last) && (!_compare(data[selected - 1], data[right - 1]))) selected = right;
                    if (current == selected) break; // They are in the correct order.
                    srvSwap(data[current - 1], data[selected - 1]);
                    current = selected;
                }
            }
        }
        /// Returns a constant reference to the peek of the heap.
        inline const T& peek(void) const { return m_data[1]; }
        /// Returns a constant array to the elements of the heap.
        inline const T * getData(void) const { return &m_data[1]; }
        /** Sets the information stored in the heap.
         *  \param[in] data array with the raw elements of the heap.
         *  \param[in] number_of_elements number of elements of the array (and of the heap).
         *  \param[in] last_size size of the original heap which sets which is the last element of the heap.
         */
        void setData(const T * data, unsigned int number_of_elements, unsigned int last_size)
        {
            if (m_data != 0) delete [] m_data;
            if (number_of_elements > 0)
            {
                m_data = new T[number_of_elements + 1];
                m_number_of_elements = number_of_elements + 1;
                m_last = last_size + 1;
                for (unsigned int i = 0; i < number_of_elements; ++i)
                    m_data[i + 1] = data[i];
            }
            else
            {
                m_data = 0;
                m_number_of_elements = m_last = 0;
            }
        }
    private:
        /// Array containing the heap.
        T *m_data;
        /// Number of elements of the heap.
        unsigned int m_number_of_elements;
        /// Index of the last element inserted to the heap.
        unsigned int m_last;
    };
    /// Meta-class which generates a min-heap.
    template <class T> struct MinHeap { typedef Heap<T, SrvCompareMin<T> > Type; };
    /// Meta class which generates a max-heap.
    template <class T> struct MaxHeap { typedef Heap<T, SrvCompareMax<T> > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | MEDIAN CLASS                         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    class MedianObject
    {
    public:
        /** Constructor which initializes the number of threads which call the median object. */
        MedianObject(unsigned int /* number_of_threads */) {}
        
        /** Calculates the median of the given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the array.
         *  \param[in] thread_id identifier of the current thread.
         *  \returns the median of the given vector.
         */
        inline T calculate(T * values, unsigned int number_of_values, unsigned int /* thread_id */, unsigned int /* cluster_size */)
        {
            if (number_of_values == 1) return values[0];
            if (number_of_values == 2) return (T)(((double)values[0] + (double)values[1]) / 2.0);
            else
            {
                std::sort(values, &values[number_of_values]);
                if (number_of_values % 2 == 1) return values[number_of_values / 2];
                else return (T)(((double)values[number_of_values / 2] + (double)values[number_of_values / 2 - 1]) / 2.0);
            }
        }
        
        /** Calculates the median of a given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the values array.
         *  \return the median of the array of values.
         */
        inline T calculate(T * values, unsigned int number_of_values) { return calculate(values, number_of_values, 1, number_of_values); }
    };
    
    template <>
    class MedianObject<unsigned char>
    {
    public:
        /** Constructor which initializes the number of threads which call the median object. */
        MedianObject(unsigned int number_of_threads) :
            m_median_histogram((number_of_threads > 0)?new unsigned int * [number_of_threads]:0),
            m_number_of_threads(number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < number_of_threads; ++thread_index)
                m_median_histogram[thread_index] = new unsigned int[256];
        }
        
        // Copy constructor.
        MedianObject(const MedianObject<unsigned char> &other) :
            m_median_histogram((other.m_number_of_threads > 0)?new unsigned int * [other.m_number_of_threads]:0),
            m_number_of_threads(other.m_number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < other.m_number_of_threads; ++thread_index)
            {
                m_median_histogram[thread_index] = new unsigned int[256];
                for (unsigned int k = 0; k < 256; ++k)
                    m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
            }
        }
        
        /// Destructor.
        ~MedianObject(void)
        {
            if (m_median_histogram != 0)
            {
                for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                delete [] m_median_histogram;
            }
        }
        
        /// Assignation operator.
        MedianObject<unsigned char>& operator=(const MedianObject<unsigned char> &other)
        {
            if (this != &other)
            {
                if (m_median_histogram != 0)
                {
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                        if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                    delete [] m_median_histogram;
                }
                if (other.m_number_of_threads > 0)
                {
                    m_median_histogram = new unsigned int * [other.m_number_of_threads];
                    m_number_of_threads = other.m_number_of_threads;
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    {
                        m_median_histogram[thread_index] = new unsigned int[256];
                        for (unsigned int k = 0; k < 256; ++k)
                            m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
                    }
                }
                else
                {
                    m_median_histogram = 0;
                    m_number_of_threads = 0;
                }
            }
            return *this;
        }
        
        /** Calculates the median of the given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the array.
         *  \param[in] thread_id identifier of the current thread.
         *  \returns the median of the given vector.
         */
        inline unsigned char calculate(unsigned char * values, unsigned int number_of_values, unsigned int thread_id, unsigned int cluster_size)
        {
            if (number_of_values == 1) return values[0];
            if (number_of_values == 2) return (unsigned char)(((unsigned short)values[0] + (unsigned short)values[1]) / 2);
            else
            {
                for (unsigned int i = 0; i < 256; ++i) m_median_histogram[thread_id][i] = 0;
                for (unsigned int i = 0; i < number_of_values; ++i) ++m_median_histogram[thread_id][values[i]];
                for (unsigned int i = 0, accum = m_median_histogram[thread_id][i]; i < 255; ++i)
                {
                    if (accum > cluster_size / 2)
                        return (unsigned char)i;
                    accum += m_median_histogram[thread_id][i + 1];
                }
                return std::numeric_limits<unsigned char>::max();
            }
        }
        
        /** Calculates the median of a given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the values array.
         *  \return the median of the array of values.
         */
        inline unsigned char calculate(unsigned char * values, unsigned int number_of_values) { return calculate(values, number_of_values, 0, number_of_values); }
        
    private:
        /// Histogram vectors used to calculate the median for the small integer types.
        unsigned int * * m_median_histogram;
        /// Number of threads used to calculate the median.
        unsigned int m_number_of_threads;
    };
    
    template <>
    class MedianObject<char>
    {
    public:
        /** Constructor which initializes the number of threads which call the median object. */
        MedianObject(unsigned int number_of_threads) :
            m_median_histogram((number_of_threads > 0)?new unsigned int * [number_of_threads]:0),
            m_number_of_threads(number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < number_of_threads; ++thread_index)
                m_median_histogram[thread_index] = new unsigned int[256];
        }
        
        // Copy constructor.
        MedianObject(const MedianObject<char> &other) :
            m_median_histogram((other.m_number_of_threads > 0)?new unsigned int * [other.m_number_of_threads]:0),
            m_number_of_threads(other.m_number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < other.m_number_of_threads; ++thread_index)
            {
                m_median_histogram[thread_index] = new unsigned int[256];
                for (unsigned int k = 0; k < 256; ++k)
                    m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
            }
        }
        
        /// Destructor.
        ~MedianObject(void)
        {
            if (m_median_histogram != 0)
            {
                for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                delete [] m_median_histogram;
            }
        }
        
        /// Assignation operator.
        MedianObject<char>& operator=(const MedianObject<char> &other)
        {
            if (this != &other)
            {
                if (m_median_histogram != 0)
                {
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                        if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                    delete [] m_median_histogram;
                }
                if (other.m_number_of_threads > 0)
                {
                    m_median_histogram = new unsigned int * [other.m_number_of_threads];
                    m_number_of_threads = other.m_number_of_threads;
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    {
                        m_median_histogram[thread_index] = new unsigned int[256];
                        for (unsigned int k = 0; k < 256; ++k)
                            m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
                    }
                }
                else
                {
                    m_median_histogram = 0;
                    m_number_of_threads = 0;
                }
            }
            return *this;
        }
        
        /** Private function which calculates the median of the given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the array.
         *  \param[in] thread_id identifier of the current thread.
         *  \returns the median of the given vector.
         */
        inline char calculate(char * values, unsigned int number_of_values, unsigned int thread_id, unsigned int cluster_size)
        {
            if (number_of_values == 1) return values[0];
            if (number_of_values == 2) return (char)(((short)values[0] + (short)values[1]) / 2);
            else
            {
                const int minimum_value = (int)std::numeric_limits<char>::min();
                for (unsigned int i = 0; i < 256; ++i) m_median_histogram[thread_id][i] = 0;
                for (unsigned int i = 0; i < number_of_values; ++i) ++m_median_histogram[thread_id][(int)values[i] - minimum_value];
                for (unsigned int i = 0, accum = m_median_histogram[thread_id][i]; i < 255; ++i)
                {
                    if (accum > cluster_size / 2)
                        return (char)((int)i + minimum_value);
                    accum += m_median_histogram[thread_id][i + 1];
                }
                return std::numeric_limits<char>::max();
            }
        }
        
        /** Calculates the median of a given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the values array.
         *  \return the median of the array of values.
         */
        inline char calculate(char * values, unsigned int number_of_values) { return calculate(values, number_of_values, 0, number_of_values); }
        
    private:
        /// Histogram vectors used to calculate the median for the small integer types.
        unsigned int * * m_median_histogram;
        /// Number of threads used to calculate the median.
        unsigned int m_number_of_threads;
    };
    
    template <>
    class MedianObject<unsigned short>
    {
    public:
        /** Constructor which initializes the number of threads which call the median object. */
        MedianObject(unsigned int number_of_threads) :
            m_median_histogram((number_of_threads > 0)?new unsigned int * [number_of_threads]:0),
            m_number_of_threads(number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < number_of_threads; ++thread_index)
                m_median_histogram[thread_index] = new unsigned int[256];
        }
        
        // Copy constructor.
        MedianObject(const MedianObject<unsigned short> &other) :
            m_median_histogram((other.m_number_of_threads > 0)?new unsigned int * [other.m_number_of_threads]:0),
            m_number_of_threads(other.m_number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < other.m_number_of_threads; ++thread_index)
            {
                m_median_histogram[thread_index] = new unsigned int[256];
                for (unsigned int k = 0; k < 256; ++k)
                    m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
            }
        }
        
        /// Destructor.
        ~MedianObject(void)
        {
            if (m_median_histogram != 0)
            {
                for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                delete [] m_median_histogram;
            }
        }
        
        /// Assignation operator.
        MedianObject<unsigned short>& operator=(const MedianObject<unsigned short> &other)
        {
            if (this != &other)
            {
                if (m_median_histogram != 0)
                {
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                        if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                    delete [] m_median_histogram;
                }
                if (other.m_number_of_threads > 0)
                {
                    m_median_histogram = new unsigned int * [other.m_number_of_threads];
                    m_number_of_threads = other.m_number_of_threads;
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    {
                        m_median_histogram[thread_index] = new unsigned int[65536];
                        for (unsigned int k = 0; k < 65536; ++k)
                            m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
                    }
                }
                else
                {
                    m_median_histogram = 0;
                    m_number_of_threads = 0;
                }
            }
            return *this;
        }
        
        /** Private function which calculates the median of the given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the array.
         *  \param[in] thread_id identifier of the current thread.
         *  \returns the median of the given vector.
         */
        inline unsigned short calculate(unsigned short * values, unsigned int number_of_values, unsigned int thread_id, unsigned int cluster_size)
        {
            if (number_of_values == 1) return values[0];
            if (number_of_values == 2) return (unsigned short)(((unsigned int)values[0] + (unsigned int)values[1]) / 2);
            else
            {
                const int minimum_value = (int)std::numeric_limits<unsigned short>::min();
                for (unsigned int i = 0; i < 65536; ++i) m_median_histogram[thread_id][i] = 0;
                for (unsigned int i = 0; i < number_of_values; ++i) ++m_median_histogram[thread_id][(int)values[i] - minimum_value];
                for (unsigned int i = 0, accum = m_median_histogram[thread_id][i]; i < 65535; ++i)
                {
                    if (accum > cluster_size / 2)
                        return (unsigned short)((int)i + minimum_value);
                    accum += m_median_histogram[thread_id][i + 1];
                }
                return std::numeric_limits<unsigned short>::max();
            }
        }
        
        /** Calculates the median of a given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the values array.
         *  \return the median of the array of values.
         */
        inline unsigned short calculate(unsigned short * values, unsigned int number_of_values) { return calculate(values, number_of_values, 0, number_of_values); }
        
    private:
        /// Histogram vectors used to calculate the median for the small integer types.
        unsigned int * * m_median_histogram;
        /// Number of threads used to calculate the median.
        unsigned int m_number_of_threads;
    };
    
    template <>
    class MedianObject<short>
    {
    public:
        /** Constructor which initializes the number of threads which call the median object. */
        MedianObject(unsigned int number_of_threads) :
            m_median_histogram((number_of_threads > 0)?new unsigned int * [number_of_threads]:0),
            m_number_of_threads(number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < number_of_threads; ++thread_index)
                m_median_histogram[thread_index] = new unsigned int[65536];
        }
        
        // Copy constructor.
        MedianObject(const MedianObject<short> &other) :
            m_median_histogram((other.m_number_of_threads > 0)?new unsigned int * [other.m_number_of_threads]:0),
            m_number_of_threads(other.m_number_of_threads)
        {
            for (unsigned int thread_index = 0; thread_index < other.m_number_of_threads; ++thread_index)
            {
                m_median_histogram[thread_index] = new unsigned int[65536];
                for (unsigned int k = 0; k < 65536; ++k)
                    m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
            }
        }
        
        /// Destructor.
        ~MedianObject(void)
        {
            if (m_median_histogram != 0)
            {
                for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                delete [] m_median_histogram;
            }
        }
        
        /// Assignation operator.
        MedianObject<short>& operator=(const MedianObject<short> &other)
        {
            if (this != &other)
            {
                if (m_median_histogram != 0)
                {
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                        if (m_median_histogram[thread_index] != 0) delete [] m_median_histogram[thread_index];
                    delete [] m_median_histogram;
                }
                if (other.m_number_of_threads > 0)
                {
                    m_median_histogram = new unsigned int * [other.m_number_of_threads];
                    m_number_of_threads = other.m_number_of_threads;
                    for (unsigned int thread_index = 0; thread_index < m_number_of_threads; ++thread_index)
                    {
                        m_median_histogram[thread_index] = new unsigned int[65536];
                        for (unsigned int k = 0; k < 65536; ++k)
                            m_median_histogram[thread_index][k] = other.m_median_histogram[thread_index][k];
                    }
                }
                else
                {
                    m_median_histogram = 0;
                    m_number_of_threads = 0;
                }
            }
            return *this;
        }
        
        /** Private function which calculates the median of the given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the array.
         *  \param[in] thread_id identifier of the current thread.
         *  \returns the median of the given vector.
         */
        inline short calculate(short * values, unsigned int number_of_values, unsigned int thread_id, unsigned int cluster_size)
        {
            if (number_of_values == 1) return values[0];
            if (number_of_values == 2) return (short)(((int)values[0] + (int)values[1]) / 2.0);
            else
            {
                const int minimum_value = (int)std::numeric_limits<short>::min();
                for (unsigned int i = 0; i < 65536; ++i) m_median_histogram[thread_id][i] = 0;
                for (unsigned int i = 0; i < number_of_values; ++i) ++m_median_histogram[thread_id][(int)values[i] - minimum_value];
                for (unsigned int i = 0, accum = m_median_histogram[thread_id][i]; i < 65535; ++i)
                {
                    if (accum > cluster_size / 2)
                        return (short)((int)i + minimum_value);
                    accum += m_median_histogram[thread_id][i + 1];
                }
                return std::numeric_limits<short>::max();
            }
        }
        
        /** Calculates the median of a given set of values.
         *  \param[in] values array of values.
         *  \param[in] number_of_values number of elements in the values array.
         *  \return the median of the array of values.
         */
        inline short calculate(short * values, unsigned int number_of_values) { return calculate(values, number_of_values, 0, number_of_values); }
        
    private:
        /// Histogram vectors used to calculate the median for the small integer types.
        unsigned int * * m_median_histogram;
        /// Number of threads used to calculate the median.
        unsigned int m_number_of_threads;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY TEMPLATE CLASS TO MANAGE   |
    //                   | THE PARAMETER VALUES                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which manages the parameters of the classes configuration objects.
    template <class T>
    class ParameterValue
    {
    public:
        /// Default constructor.
        ParameterValue(void) : m_update(false) {}
        /// Constructor which initializes the value of the parameter container.
        ParameterValue(const T &value) : m_value(value), m_update(false) {}
        /// Constructor which initializes both the value of the parameter and the update flag.
        ParameterValue(const T &value, bool update_value) : m_value(value), m_update(update_value) {}
        /// Sets the value of the parameter and resets the update flag.
        inline void setValue(const T &value) { m_value = value; m_update = false; }
        /// Returns a constant reference to the value.
        inline const T& getValue(void) const { return m_value; }
        /// Returns a reference to the value.
        inline T& getValue(void) { return m_value; }
        /// Returns a constant reference to the value.
        inline const T& operator*(void) const { return m_value; }
        /// Returns a reference to the value.
        inline T& operator*(void) { return m_value; }
        /// Returns the boolean which indicates if the update flag has been set.
        inline bool isUpdate(void) const { return m_update; }
        /// Returns a constant pointer to the update boolean flag.
        inline const bool * getUpdatePtr(void) const { return &m_update; }
        /// Returns a pointer to the update boolean flag.
        inline bool * getUpdatePtr(void) { return &m_update; }
        /// Updates the value only when the update flag of the other parameter value is true.
        void update(const ParameterValue &other) { if (other.m_update && (m_value != other.m_value)) { m_value = other.m_value; m_update = true; } }
        /// Resets the update flag.
        void reset(void) { m_update = false; }
        /// Shows the information of the parameters value at the given output stream.
        inline std::ostream& show(std::ostream &out, const char * description) const { out << (m_update?("+"):("·")) << description << m_value; return out; }
    private:
        /// Value of the parameter.
        T m_value;
        /// Update flag set by the parser parameters class.
        bool m_update;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | USER PARAMETERS PARSER CLASSES       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class ValueParameter
    {
    public:
        ValueParameter(const char *value_name, const char *value_description, bool has_min_value, bool has_max_value);
        ValueParameter(const char *parameter_name, const char *value_name, const char * value_description, bool has_min_value, bool has_max_value, bool * update);
        virtual ~ValueParameter(void) {}
        virtual void parseValue(const char *value) = 0;
        inline bool isParameter(void) const {return m_parameter_name[0] != '\0';}
        inline bool hasValue(void) const {return m_has_value;}
        inline bool isBlockSeparator(void) const {return m_block_separator;}
        inline bool isParameterName(char *name) {return ((!m_block_separator) && (strcasecmp(m_parameter_name, name) == 0));}
        inline const char* getValueName(void) const {return m_value_name;}
        inline const char* getParameterName(void) const {return m_parameter_name;}
        inline const char* getDescription(void) const {return m_description;}
        inline const char* getRangeInformation(void) const {return m_range_information;}
        inline void setUpdate(void) { if (m_update != 0) *m_update = true; }
    protected:
        void OutOfRange(const char *value);
        bool m_value_range_max;
        bool m_value_range_min;
        bool m_has_value;
        bool m_block_separator;
        char m_value_name[64];
        char m_parameter_name[64];
        char m_description[1024];
        char m_range_information[1024];
        bool * m_update;
    };
    
    class BlockSeparator : public ValueParameter
    {
    public:
        BlockSeparator(const char * description) :
            ValueParameter("BLOCK_INFORMATION", "", description, false, false, 0) { m_block_separator = true; }
        inline void parseValue(const char * /* value */) { }
    private:
    };
    
    class ParameterFloat : public ValueParameter
    {
    public:
        ParameterFloat(const char *value_name, const char *value_description, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable);
        ParameterFloat(const char *parameter_name, const char *value_name, const char *value_description, float default_value, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable);
        ParameterFloat(const char *parameter_name, const char *value_name, const char *value_description, float default_value, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        float m_min_value;
        float m_max_value;
        float *m_value;
    };
    
    class ParameterDouble : public ValueParameter
    {
    public:
        ParameterDouble(const char *value_name, const char *value_description, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable);
        ParameterDouble(const char *parameter_name, const char *value_name, const char *value_description, double default_value, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable);
        ParameterDouble(const char *parameter_name, const char *value_name, const char *value_description, double default_value, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        double m_min_value;
        double m_max_value;
        double *m_value;
    };
    
    class ParameterUChar : public ValueParameter
    {
    public:
        ParameterUChar(const char *value_name, const char *value_description, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable);
        ParameterUChar(const char *parameter_name, const char *value_name, const char *value_description, unsigned char default_value, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable);
        ParameterUChar(const char *parameter_name, const char *value_name, const char *value_description, unsigned char default_value, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        unsigned char m_min_value;
        unsigned char m_max_value;
        unsigned char *m_value;
    };
    
    class ParameterChar : public ValueParameter
    {
    public:
        ParameterChar(const char *value_name, const char *value_description, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable);
        ParameterChar(const char *parameter_name, const char *value_name, const char *value_description, char default_value, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable);
        ParameterChar(const char *parameter_name, const char *value_name, const char *value_description, char default_value, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        char m_min_value;
        char m_max_value;
        char *m_value;
    };
    
    class ParameterCharArray : public ValueParameter
    {
    public:
        ParameterCharArray(const char *value_name, const char *value_description, char *destination_variable);
        ParameterCharArray(const char *parameter_name, const char *value_name, const char *value_description, char *destination_variable);
        ParameterCharArray(const char *parameter_name, const char *value_name, const char *value_description, char *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        char *m_value;
    };
    
    class ParameterUShort : public ValueParameter
    {
    public:
        ParameterUShort(const char *value_name, const char *value_description, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable);
        ParameterUShort(const char *parameter_name, const char *value_name, const char *value_description, unsigned short default_value, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable);
        ParameterUShort(const char *parameter_name, const char *value_name, const char *value_description, unsigned short default_value, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        unsigned short m_min_value;
        unsigned short m_max_value;
        unsigned short *m_value;
    };
    
    class ParameterShort : public ValueParameter
    {
    public:
        ParameterShort(const char *value_name, const char *value_description, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable);
        ParameterShort(const char *parameter_name, const char *value_name, const char *value_description, short default_value, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable);
        ParameterShort(const char *parameter_name, const char *value_name, const char *value_description, short default_value, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        short m_min_value;
        short m_max_value;
        short *m_value;
    };
    
    class ParameterUInt : public ValueParameter
    {
    public:
        ParameterUInt(const char *value_name, const char *value_description, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable);
        ParameterUInt(const char *parameter_name, const char *value_name, const char *value_description, unsigned int default_value, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable);
        ParameterUInt(const char *parameter_name, const char *value_name, const char *value_description, unsigned int default_value, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        unsigned int m_min_value;
        unsigned int m_max_value;
        unsigned int *m_value;
    };
    
    class ParameterInt : public ValueParameter
    {
    public:
        ParameterInt(const char *value_name, const char *value_description, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable);
        ParameterInt(const char *parameter_name, const char *value_name, const char *value_description, int default_value, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable);
        ParameterInt(const char *parameter_name, const char *value_name, const char *value_description, int default_value, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        int m_min_value;
        int m_max_value;
        int *m_value;
    };
    
    class ParameterULong : public ValueParameter
    {
    public:
        ParameterULong(const char *value_name, const char *value_description, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable);
        ParameterULong(const char *parameter_name, const char *value_name, const char *value_description, unsigned long default_value, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable);
        ParameterULong(const char *parameter_name, const char *value_name, const char *value_description, unsigned long default_value, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        unsigned long m_min_value;
        unsigned long m_max_value;
        unsigned long *m_value;
    };
    
    class ParameterLong : public ValueParameter
    {
    public:
        ParameterLong(const char *value_name, const char *value_description, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable);
        ParameterLong(const char *parameter_name, const char *value_name, const char *value_description, long default_value, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable);
        ParameterLong(const char *parameter_name, const char *value_name, const char *value_description, long default_value, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        long m_min_value;
        long m_max_value;
        long *m_value;
    };
    
    class ParameterBoolean : public ValueParameter
    {
    public:
        ParameterBoolean(const char *value_name, const char *value_description, bool *destination_variable);
        ParameterBoolean(const char *parameter_name, const char *value_name, const char *value_description, bool *destination_variable);
        ParameterBoolean(const char *parameter_name, const char *value_name, const char *value_description, bool *destination_variable, bool *update);
        void parseValue(const char *value);
    private:
        bool *m_value;
    };
    
    class ParameterParser
    {
    public:
        ParameterParser(const char *application_name, const char *initial_message = 0, const char *final_message = 0);
        ~ParameterParser(void);
        inline void addParameter(ValueParameter *parameter)
        {
            if (parameter->isParameter()) m_parameters.push_back(parameter);
            else m_values.push_back(parameter);
        }
        void parse(char **values, int number_of_arguments);
        void synopsisMessage(std::ostream &out) const;
    private:
        std::vector<ValueParameter *> m_values;
        std::vector<ValueParameter *> m_parameters;
        char m_initial_message[4098];
        char m_final_message[4098];
        char m_application_name[256];
    };
    
    /** Auxiliary function which converts a colon-separated text list (of numbers) into a std library list.
     *  \param[in] text text array with the list.
     *  \param[out] elements resulting elements list.
     */
    template <class T>
    void text2list(char * __restrict__ text, const T &min_value, const T &max_value, std::list<T> &elements)
    {
        unsigned int index_begin, index_end;
        
        for (index_begin = index_end = 0; text[index_end] != '\0'; ++index_end)
        {
            if (text[index_end] == ':')
            {
                if (index_begin != index_end)
                {
                    T current;
                    
                    text[index_end] = '\0';
                    current = (T)std::atof(&text[index_begin]);
                    if ((current >= min_value) && (current <= max_value))
                        elements.push_back(current);
                }
                index_begin = index_end = index_end + 1;
            }
        }
        if (index_begin != index_end)
        {
            T current;
            
            text[index_end] = '\0';
            current = (T)std::atof(&text[index_begin]);
            if ((current >= min_value) && (current <= max_value))
                elements.push_back(current);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY SCALING FUNCTIONS          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This template function calculate the offset and scale factor needed to store data which ranges between -1.0 and 1.0
     *  into a data type different than double or float.
     *  \param[in] has_negative_values boolean flag which indicates that the data has negative when true.
     *  \param[out] offset offset of the data.
     *  \param[out] scale_factor scale factor of the data.
     *  \tparam type of the destination data structure.
     */
    template <class T_SAMPLE>
    void rescaleFactor(bool has_negative_values, double &offset, double &scale_factor)
    {
        if (has_negative_values && !std::numeric_limits<T_SAMPLE>::is_signed)
        {
            offset = 1.0;
            if (sizeof(T_SAMPLE) == 1)
                scale_factor = (double)std::numeric_limits<T_SAMPLE>::max() / 2.0;
            else if (sizeof(T_SAMPLE) == 2)
                scale_factor = 500.0;
            else
            {
                if (std::numeric_limits<T_SAMPLE>::is_integer) scale_factor = 5000.0;
                else scale_factor = 1.0;
            }
        }
        else
        {
            offset = 0;
            if (sizeof(T_SAMPLE) == 1)
            {
                if (std::numeric_limits<T_SAMPLE>::is_signed) scale_factor = srvMin<double>(-(double)std::numeric_limits<T_SAMPLE>::min(), (double)std::numeric_limits<T_SAMPLE>::max());
                else scale_factor = (double)std::numeric_limits<T_SAMPLE>::max();
            }
            else if (sizeof(T_SAMPLE) == 2)
                scale_factor = 1000.0;
            else
            {
                if (std::numeric_limits<T_SAMPLE>::is_integer) scale_factor = 10000.0;
                else scale_factor = 1.0;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TAB CLASS                            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// This class manages the tabs on an standard output stream.
    class CharacterTab
    {
    public:
        /// Constructor sets the character and the amount of repetitions used at each tab.
        CharacterTab(char tab_character, unsigned int repetition);
        /// Function which increases the tab index for a given output stream.
        void increase(std::ostream &out);
        /// Function which decreases the tab index for a given output stream.
        void decrease(std::ostream &out);
        /// Friend function which redirects the tab into an output stream.
        friend std::ostream& operator<<(std::ostream &out, const CharacterTab &tab_object);
    private:
        typedef std::map<std::ostream *, unsigned int> TabTable;
        /// Table which stores the tab index for each output stream.
        static TabTable& tabTable(void);
        /// Character used to create the tabs.
        char m_tab_character;
        /// Number of characters added at each tab.
        unsigned int m_tab_repetitions;
    };
    
    std::ostream& operator<<(std::ostream &out, const CharacterTab &tab_object);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TEMPLATE FACTORY CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Generic template class to create object factories.
     */
    template <class T, typename IDENTIFIER>
    class Factory
    {
        /// Type which defines a pointer to the function which creates the object as a new type.
        typedef T * (*Instantiator)();
        /// Type which defines the map which translates the object identifier to the function which creates a new instance.
        typedef std::map<IDENTIFIER, Instantiator> InstantiatorMap;
    public:
        /// Default constructor.
        Factory(void);
        /// Destructor.
        ~Factory(void) {}
        /** Returns a new instance of the object.
         *  \param[in] id identifier of the object.
         *  \returns a new instance of the object.
         */
        static T * getInstantiator(IDENTIFIER id) { return instantiatorMap()[id](); }
        /// Returns a reference to the vector with the identifiers of the objects which have been initialized in the factory.
        inline std::vector<IDENTIFIER>& getInstantiatorIds(void) { return m_list_of_ids; }
        /// Returns a constant reference to the vector with the identifiers of the objects which have been initialized in the factory.
        inline const std::vector<IDENTIFIER>& getInstantiatorIds(void) const { return m_list_of_ids; }
        /** Registers a new object into the factor.
         *  \param[in] id identifier of the object.
         *  \param[in] function function which creates a new instance of the object.
         *  \returns a integer which is 1 when the object has been successfully added to the factory and 0 when the object has not been added to the factory.
         */
        static int registerInstantiator(IDENTIFIER id, Instantiator function);
    private:
        /** Function which stores the static map of the factory. The map is stored in a function in order to avoid
         *  the "static initialization order fiasco", i.e. a function which calls the map is called before than the
         *  static map is created and a linker error is generated.
         */
        static InstantiatorMap& instantiatorMap(void);
        /// Vector which contains the identifiers that have been added to the factory.
        std::vector<IDENTIFIER> m_list_of_ids;
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ IMPLEMENTATION OF THE FACTORY CLASS ]======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T, typename IDENTIFIER>
    Factory<T, IDENTIFIER>::Factory(void) : m_list_of_ids(instantiatorMap().size())
    {
        int index = 0;
        for (typename InstantiatorMap::iterator begin = instantiatorMap().begin(), end = instantiatorMap().end(); begin != end; ++begin)
        {
            m_list_of_ids[index] = begin->first;
            ++index;
        }
    }
    
    template <class T, typename IDENTIFIER>
    typename Factory<T, IDENTIFIER>::InstantiatorMap& Factory<T, IDENTIFIER>::instantiatorMap(void)
    {
        static InstantiatorMap static_variable = InstantiatorMap();
        return static_variable;
    }
    
    template <class T, typename IDENTIFIER>
    int Factory<T, IDENTIFIER>::registerInstantiator(IDENTIFIER id, Factory<T, IDENTIFIER>::Instantiator function)
    {
        instantiatorMap().insert(std::make_pair(id, function));
        //instantiatorMap().insert(std::pair<IDENTIFIER, Factory<T, IDENTIFIER>::Instantiator>(id, function));
        return 1;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MACROS FOR TEMPLATE FACTORIES ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    #define TEMPLATE_1P(CLASS_NAME, A) \
        CLASS_NAME<A>
    #define TEMPLATE_2P(CLASS_NAME, A, B) \
        CLASS_NAME<A, B>
    #define TEMPLATE_3P(CLASS_NAME, A, B, C) \
        CLASS_NAME<A, B, C>
    #define TEMPLATE_4P(CLASS_NAME, A, B, C, D) \
        CLASS_NAME<A, B, C, D>
    #define TEMPLATE_5P(CLASS_NAME, A, B, C, D, E) \
        CLASS_NAME<A, B, C, D, E>
    #define TEMPLATE_6P(CLASS_NAME, A, B, C, D, E, F) \
        CLASS_NAME<A, B, C, D, E, F>
    #define TEMPLATE_7P(CLASS_NAME, A, B, C, D, E, F, G) \
        CLASS_NAME<A, B, C, D, E, F, G>
    #define TEMPLATE_8P(CLASS_NAME, A, B, C, D, E, F, G, H) \
        CLASS_NAME<A, B, C, D, E, F, G, H>
    #define TEMPLATE_9P(CLASS_NAME, A, B, C, D, E, F, G, H, I) \
        CLASS_NAME<A, B, C, D, E, F, G, H, I>
    
    /*
    #define ADD_FACTORY(FACTORY_NAME, CLASS_NAME, T, TDISTANCE, N) \
        template <> int CLASS_NAME<T, TDISTANCE, N>::m_is_initialized = FACTORY_NAME<T, TDISTANCE, N>::Type::registerInstantiator(CLASS_NAME<T, TDISTANCE, N>::getClassIdentifier(), &CLASS_NAME<T, TDISTANCE, N>::generateObject);
    */
    #define ADD_FACTORY(FACTORY_NAME, CLASS_NAME) \
        template <> int CLASS_NAME::m_is_initialized = FACTORY_NAME::Type::registerInstantiator(CLASS_NAME::getClassIdentifier(), &CLASS_NAME::generateObject);
    // Non-template factories.
    #define ADD_FACTORY_NT(FACTORY_NAME, CLASS_NAME) \
        int CLASS_NAME::m_is_initialized = FACTORY_NAME::Type::registerInstantiator(CLASS_NAME::getClassIdentifier(), &CLASS_NAME::generateObject);
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    
    template <class TVALUE>
    unsigned int otsuThreshold(const TVALUE * histogram, unsigned int number_of_elements)
    {
        unsigned int total, sum;
        TVALUE sum_b, w_b;
        unsigned int selected;
        double maximum;
        
        sum = total = 0;
        for (unsigned int i = 0; i < number_of_elements; ++i)
        {
            total += histogram[i];
            sum += i * histogram[i];
        }
        selected = 0;
        maximum = 0;
        w_b = sum_b = 0;
        for (unsigned int i = 0; i < number_of_elements; ++i)
        {
            double m_b, m_f, between;
            TVALUE w_f;
            
            w_b += histogram[i];
            if (w_b == 0) continue;
            w_f = total - w_b;
            if (w_f == 0) break;
            sum_b += (TVALUE)i * histogram[i];
            
            m_b = (double)sum_b / (double)w_b;
            m_f = (double)(sum - sum_b) / (double)w_f;
            between = (double)w_b * (double)w_f * (m_b - m_f) * (m_b - m_f);
            if (between >= maximum)
            {
                selected = i;
                maximum = between;
            }
        }
        return selected;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif
