// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_RANDOM_HPP_HEADER_FILE__
#define __SRV_RANDOM_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <cmath>
#include <cstdlib>
#include <omp.h>

namespace srv
{
    /// This function returns a random uniformly distributed value in range [0, 1).
    inline double randomUniform(void) { return (double)rand() / (double)RAND_MAX; }
    /// This function returns a random uniformly distributed value in range (0, 1) (i.e. it excludes both 0.0 and 1.0).
    inline double randomUniformNonZero(void) { return (double)(rand() + 1) / ((double)RAND_MAX + 1.0); }
    
    /** This function randomly generates values which follow a Gaussian distribution. The function implements the Box-Muller transform.
     *  \param[in] mean mean of the Gaussian random distribution.
     *  \param[in] variance variance of the Gaussian random distribution.
     *  \returns a random value sampled from the selected Gaussian distribution.
     */
    inline double randomGaussian(double mean, double variance)
    {
        const double pi = 2.0 * 3.141592654;
        double rand0, rand1;
        
        // Box-Muller transform.
        rand0 = std::sqrt(-2.0 * variance * std::log(randomUniformNonZero()));
        rand1 = std::cos(pi * randomUniform());
        return rand0 * rand1 + mean;
    }
    
    /** This function initializes the values of the vector randomly following a Gaussian distribution.
     *  \param[out] data array where the resulting random values are stored.
     *  \param[in] size number of elements of the array.
     *  \param[in] mean mean of the Gaussian random distribution.
     *  \param[in] variance variance of the Gaussian random distribution.
     *  \param[in] factor scale factor applied to the random values before assigning them to the vector (needed to rescale for values for data types different than double or float).
     *  \param[in] number_of_threads number of threads used to concurrently create the random values.
     *  \returns a random number following the selected Gaussian distribution.
     */
    template <typename T>
    inline void randomGaussian(T * data, unsigned int size, double mean, double variance, double factor = 1.0, unsigned int number_of_threads = 1)
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
                data[i] = (T)(factor * randomGaussian(mean, variance));
        }
    }
    
    template <template <class, class> class VECTOR, class T, class N>
    inline void randomGaussian(VECTOR<T, N> &data, double mean, double variance, double factor = 1.0, unsigned int number_of_threads = 1) { randomGaussian(data.getData(), data.size(), mean, variance, factor, number_of_threads); }
    
    /** This function randomly generated values which follow a Gamma distribution.
     *  \param[in] alpha sets the shape of the Gamma distribution.
     *  \param[in] beta  sets the scale of the Gamma distribution.
     *  \returns a random value sampled from the selected Gamma distribution.
     */
    double randomGamma(double alpha, double beta);
    
    /** This function initializes the values of the vector randomly following a Gamma distribution.
     *  \param[out] data array where the resulting random values are stored.
     *  \param[in] size number of elements of the array.
     *  \param[in] alpha sets the shape of the Gamma distribution.
     *  \param[in] beta  sets the scale of the Gamma distribution.
     *  \param[in] factor scale factor applied to the random values before assigning them to the vector (needed to rescale for values for data types different than double or float).
     *  \param[in] number_of_threads number of threads used to concurrently create the random values.
     *  \returns a random number following the selected Gaussian distribution.
     */
    template <typename T>
    inline void randomGamma(T * data, unsigned int size, double alpha, double beta, double factor = 1.0, unsigned int number_of_threads = 1)
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < size; i += number_of_threads)
                data[i] = (T)(factor * randomGamma(alpha, beta));
        }
    }
}   

#endif

