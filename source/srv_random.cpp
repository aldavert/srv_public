// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_random.hpp"

namespace srv
{
    
    double randomGamma(double alpha, double beta)
    {
        if (alpha < 1.0) return randomGamma(1.0 + alpha, beta) * std::pow(randomUniformNonZero(), 1.0 / alpha);
        else
        {
            double d, c, v, x, u;
            
            d = alpha - 1.0 / 3.0;
            c = (1.0 / 3.0) / std::sqrt(d);
            while (true)
            {
                do
                {
                    x = randomGaussian(0.0, 1.0);
                    v = 1.0 + c * x;
                } while (v <= 0);
                v = v * v * v;
                u = randomUniformNonZero();
                if (u < 1.0 - 0.0331 * x * x * x * x) break;
                if (std::log(u) < 0.5 * x * x + d * (1.0 - v + std::log(v))) break;
            }
            
            return beta * d * v;
        }
    }
    
}

