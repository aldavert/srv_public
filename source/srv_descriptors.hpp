// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_MAIN_LOCAL_DESCRIPTORS_HEADER_FILE__
#define __SRV_MAIN_LOCAL_DESCRIPTORS_HEADER_FILE__

#include "descriptors/srv_dense_descriptors_definitions.hpp"
#include "descriptors/srv_dense_descriptors.hpp"
#include "descriptors/upright/srv_upright_descriptors.hpp"
#include "descriptors/approximate/srv_integral_hog_descriptor.hpp"
#include "srv_file_compression.hpp"
#include <iostream>

#define INITIALIZE_DENSE_DESCRIPTOR(TFILTER, TFEATURE)\
    ADD_IMAGE_FEATURE_FACTORY(srv::GradientModuleImageFeature, TFILTER, TFEATURE) \
    ADD_IMAGE_FEATURE_FACTORY(srv::GradientOrientationImageFeature, TFILTER, TFEATURE) \
    ADD_IMAGE_FEATURE_FACTORY(srv::GradientImageFeature, TFILTER, TFEATURE) \
    ADD_IMAGE_FEATURE_FACTORY(srv::DerivativeImageFeature, TFILTER, TFEATURE) \
    ADD_IMAGE_FEATURE_FACTORY(srv::RotatedDerivativeImageFeature, TFILTER, TFEATURE) \
    ADD_IMAGE_FEATURE_FACTORY(srv::LineImageFeature, TFILTER, TFEATURE) \
    ADD_DENSE_DESCRIPTOR_FACTORY(srv::UprightDescriptor, TFILTER, TFEATURE) \
    ADD_DENSE_DESCRIPTOR_FACTORY(srv::IntegralHOG, TFILTER, TFEATURE)

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS FOR THE DENSE    |
    //                   | DESCRIPTORS CLASS                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Shows the information of the dense descriptors object in an standard output stream.
     *  \param[out] out standard library output stream.
     *  \param[in] object dense descriptor object.
     *  \returns the input standard library output stream.
     */
    template <class TFILTER, class TFEATURE>
    std::ostream& operator<<(std::ostream &out, const DenseDescriptor<TFILTER, TFEATURE> * object)
    {
        const DDESCRIPTOR_IDENTIFIER descriptor_identifier = object->getIdentifier();
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Dense descriptor ]" << std::endl;
        tab.increase(out);
        
        // Algorithm information ..................................................................
        out << tab << " · Algorithm type: ";
        if      (descriptor_identifier == DD_UPRIGHT_DESCRIPTOR) out << "Upright algorithm.";
        else if (descriptor_identifier == DD_INTEGRAL_HOG) out << "Integral HOG algorithm.";
        else out << ">>UNKNOWN ALGORITHM<<";
        out << std::endl;
        
        // Region sampling information ............................................................
        out << tab << " · Minimum region size: " << object->getMinimumRegionSize() << std::endl;
        out << tab << " · Maximum region size: " << object->getMaximumRegionSize() << std::endl;
        out << tab << " · Growth factor: " << object->getGrowthFactor() << std::endl;
        if (object->getRelativeStep()) out << tab << " · Relative step factor: " << object->getStep() << std::endl;
        else out << tab << " · Step: " << object->getStep() << " pixels." << std::endl;
        out << tab << " · Gaussian kernel sigma ratio: " << object->getSigmaRatio() << std::endl;
        out << tab << " · Number of scales: " << object->getNumberOfScales() << std::endl;
        
        // Region information at each scale .......................................................
        for (unsigned int i = 0; i < object->getNumberOfScales(); ++i)
        {
            const typename DenseDescriptor<TFILTER, TFEATURE>::ScaleInformation &current = object->getScalesInformation(i);
            
            out << tab << " [ Scale " << i << " ]" << std::endl;
            tab.increase(out);
            out << tab << " · Region size: " << current.getRegionSize() << " pixels." << std::endl;
            out << tab << " · Region step: " << current.getRegionStep() << " pixels." << std::endl;
            out << tab << " · Gaussian kernel sigma: " << current.getScaleSigma() << std::endl;
            tab.decrease(out);
        }
        
        // Image derivatives information ..........................................................
        out << tab << " · Color method: ";
        if      (object->getColorMethod() == DD_GRAYSCALE)                 out << "Convert the image to gray-scale.";
        else if (object->getColorMethod() == DD_LUMINANCE)                 out << "Convert the image to luminance.";
        else if (object->getColorMethod() == DD_MAXIMUM_CHANNEL)           out << "Process the RGB channels separately and select the channel with the strongest response.";
        else if (object->getColorMethod() == DD_COLOR_RGB)                 out << "Process the RGB channels separately.";
        else if (object->getColorMethod() == DD_COLOR_OPPONENT)            out << "Convert the images to the opponent color space and process the channels separately.";
        else if (object->getColorMethod() == DD_COLOR_OPPONENT_NORMALIZED) out << "Convert the images to the normalized opponent color space and process the channels separately.";
        else out << ">>UNKNOWN COLOR METHOD<<";
        out << std::endl;
        
        // General descriptor information .........................................................
        out << tab << " · Number of spatial bins: " << object->getNumberOfSpatialBins() << std::endl;
        out << tab << " · Number of image features per spatial bin: " << object->getNumberOfFeatureBins() << std::endl;
        out << tab << " · Resulting descriptor has negative values: " << (object->hasDescriptorNegativeValues()?("[True]"):("[False]")) << std::endl;
        out << tab << " · Descriptor dimensionality: " << object->getNumberOfBins(3) << std::endl;
        
        // Descriptor specific information ........................................................
        if (descriptor_identifier == DD_UPRIGHT_DESCRIPTOR)
        {
            const UprightDescriptor<TFILTER, TFEATURE> * object_ptr = (UprightDescriptor<TFILTER, TFEATURE> *)object;
            
            out << tab << " · Gaussian kernel algorithm: ";
            if      (object_ptr->getGaussianMethod() == DD_STANDARD_GAUSSIAN)   out << "Normal Gaussian kernel.";
            else if (object_ptr->getGaussianMethod() == DD_DERICHE_RECURSIVE)   out << "Deriche's recursive Gaussian kernel.";
            else if (object_ptr->getGaussianMethod() == DD_VLIET_3RD_RECURSIVE) out << "3rd order Vliet's recursive Gaussian kernel.";
            else if (object_ptr->getGaussianMethod() == DD_VLIET_4TH_RECURSIVE) out << "4th order Vliet's recursive Gaussian kernel.";
            else if (object_ptr->getGaussianMethod() == DD_VLIET_5TH_RECURSIVE) out << "5th order Vliet's recursive Gaussian kernel.";
            else out << ">>UNKNOWN GAUSSIAN ALGORITHM<<";
            out << std::endl;
            
            // Descriptor spatial information .........................................................
            out << tab << " · Descriptor spatial information:" << std::endl;
            tab.increase(out);
            out << tab << " · Number of levels of the spatial pyramid: " << object_ptr->getNumberOfSpatialLevels() << std::endl;
            out << tab << " · Initial number of partitions in the X direction: " << object_ptr->getSpatialInitialX() << std::endl;
            out << tab << " · Initial number of partitions in the Y direction: " << object_ptr->getSpatialInitialY() << std::endl;
            out << tab << " · Pyramid's degree in the X direction: " << object_ptr->getSpatialDegreeX() << std::endl;
            out << tab << " · Pyramid's degree in the Y direction: " << object_ptr->getSpatialDegreeY() << std::endl;
            out << tab << " · Spatial bins per level:";
            for (unsigned int i = 0; i < object_ptr->getNumberOfSpatialLevels(); ++i)
                out << " " << object_ptr->getNumberOfSpatialBins(i);
            out << std::endl;
            
            out << tab << " · Spatial grid type: ";
            if (object_ptr->getSpatialMethod() == DD_GRID_CARTESIAN) out << "Cartesian grid.";
            else if (object_ptr->getSpatialMethod() == DD_GRID_POLAR) out << "Polar grid.";
            else out << ">>UNKNOWN GRID REPRESENTATION<<";
            out << std::endl;
            out << tab << " · Spatial bins interpolation: " << (object_ptr->getSpatialInterpolation()?("[Enabled]"):("[Disabled]")) << std::endl;
            out << tab << " · Gaussian spatial weighting function: " << (object_ptr->getSpatialGaussian()?("[Enabled]"):("[Disabled]")) << std::endl;
            if (object_ptr->getSpatialGaussian())
                out << tab << " · Sigma of the Gaussian weighting function: " << object_ptr->getSpatialGaussianSigma() << std::endl;
            tab.decrease(out);
            
            // Normalization information ..............................................................
            out << tab << " · Spatial normalization:" << std::endl;
            out << object_ptr->getSpatialNorm();
            out << tab << " · Descriptor normalization:" << std::endl;
            out << object_ptr->getNorm();
            out << tab << " · Descriptor cutoff threshold: " << object_ptr->getCutoff() << std::endl;
            out << tab << " · Descriptor power factor re-normalization: " << object_ptr->getPowerFactor() << std::endl;
            
            out << tab << " · Number of image features extracted: " << object_ptr->getNumberOfFeatureObjects() << std::endl;
            for (unsigned int i = 0; i < object_ptr->getNumberOfFeatureObjects(); ++i)
            {
                const ImageFeatureBase<TFILTER, TFEATURE> * current = object_ptr->getFeatureObject(i);
                DDESCRIPTOR_UPRIGHT_IMAGE_FEATURE identifier = current->getIdentifier();
                
                out << tab << " · Image feature " << i + 1 << ":" << std::endl;
                tab.increase(out);
                if (identifier == DD_UPRIGHT_GRADIENT_MODULE)
                {
                    const GradientModuleImageFeature<TFILTER, TFEATURE> * current_ptr = (const GradientModuleImageFeature<TFILTER, TFEATURE> *)current;
                    
                    out << tab << " · Type: Gradient module." << std::endl;
                    out << tab << " · Features generated per pixel: " << current_ptr->getNumberOfFeaturesPerPixel() << std::endl;
                    out << tab << " · Total number of features generated: " << current_ptr->getNumberOfFeatures() << std::endl;
                }
                else if (identifier == DD_UPRIGHT_GRADIENT_ORIENTATION)
                {
                    const GradientOrientationImageFeature<TFILTER, TFEATURE> * current_ptr = (const GradientOrientationImageFeature<TFILTER, TFEATURE> *)current;
                    
                    out << tab << " · Type: Gradient orientation." << std::endl;
                    out << tab << " · Interpolation between orientation bins: " << (current_ptr->getInterpolation()?("[Enabled]"):("[Disabled]")) << std::endl;
                    out << tab << " · Features generated per pixel: " << current_ptr->getNumberOfFeaturesPerPixel() << std::endl;
                    out << tab << " · Total number of features generated: " << current_ptr->getNumberOfFeatures() << std::endl;
                }
                else if (identifier == DD_UPRIGHT_GRADIENT)
                {
                    const GradientImageFeature<TFILTER, TFEATURE> * current_ptr = (const GradientImageFeature<TFILTER, TFEATURE> *)current;
                    
                    out << tab << " · Type: Gradient." << std::endl;
                    out << tab << " · Features generated per pixel: " << current_ptr->getNumberOfFeaturesPerPixel() << std::endl;
                    out << tab << " · Total number of features generated: " << current_ptr->getNumberOfFeatures() << std::endl;
                }
                else if (identifier == DD_UPRIGHT_DERIVATIVES)
                {
                    const DerivativeImageFeature<TFILTER, TFEATURE> * current_ptr = (const DerivativeImageFeature<TFILTER, TFEATURE> *)current;
                    
                    out << tab << " · Type: Derivatives." << std::endl;
                    out << tab << " · Number of enabled derivatives: " << current_ptr->getNumberOfEnabledDerivatives() << std::endl;
                    out << tab << " · Enabled derivatives:";
                    if (current_ptr->getDxEnabled()) out << " dX";
                    if (current_ptr->getDyEnabled()) out << " dY";
                    if (current_ptr->getDxDyEnabled()) out << " dXdY";
                    if (current_ptr->getDx2Enabled()) out << " dX2";
                    if (current_ptr->getDy2Enabled()) out << " dY2";
                    if (current_ptr->getDx2DyEnabled()) out << " dX2dY";
                    if (current_ptr->getDxDy2Enabled()) out << " dXdY";
                    if (current_ptr->getDx2Dy2Enabled()) out << " dX2dY2";
                    out << std::endl;
                    out << tab << " · Features generated per pixel: " << current_ptr->getNumberOfFeaturesPerPixel() << std::endl;
                    out << tab << " · Total number of features generated: " << current_ptr->getNumberOfFeatures() << std::endl;
                }
                else if (identifier == DD_UPRIGHT_ROTATED_DERIVATIVES)
                {
                    const RotatedDerivativeImageFeature<TFILTER, TFEATURE> * current_ptr = (const RotatedDerivativeImageFeature<TFILTER, TFEATURE> *)current;
                    
                    out << tab << " · Type: Rotated derivatives." << std::endl;
                    out << tab << " · Number of enabled derivatives: " << current_ptr->getNumberOfEnabledDerivatives() << std::endl;
                    out << tab << " · Enabled derivatives:";
                    if (current_ptr->getDxEnabled()) out << " dX";
                    if (current_ptr->getDyEnabled()) out << " dY";
                    if (current_ptr->getDxDyEnabled()) out << " dXdY";
                    if (current_ptr->getDx2Enabled()) out << " dX2";
                    if (current_ptr->getDy2Enabled()) out << " dY2";
                    if (current_ptr->getDx2DyEnabled()) out << " dX2dY";
                    if (current_ptr->getDxDy2Enabled()) out << " dXdY";
                    if (current_ptr->getDx2Dy2Enabled()) out << " dX2dY2";
                    out << std::endl;
                    out << tab << " · Features generated per pixel: " << current_ptr->getNumberOfFeaturesPerPixel() << std::endl;
                    out << tab << " · Total number of features generated: " << current_ptr->getNumberOfFeatures() << std::endl;
                }
                else out << tab << " · Type: >>UNKNOWN UPRIGHT FEATURE TYPE<<" << std::endl;
                tab.decrease(out);
            }
        }
        else if (descriptor_identifier == DD_INTEGRAL_HOG)
        {
            const IntegralHOG<TFILTER, TFEATURE> * object_ptr = (IntegralHOG<TFILTER, TFEATURE> *)object;
            const double pi2deg = 180.0 / 3.141592653589793;
            
            out << tab << " · Number of Orientation Bins: " << object_ptr->getNumberOfOrientationBins() << std::endl;
            out << tab << " · Orientation bins at (degrees):";
            for (unsigned int i = 0; i < object_ptr->getNumberOfOrientationBins(); ++i)
            {
                char buffer[32];
                sprintf(buffer, " %5.1f", object_ptr->getAngle(i) * pi2deg);
                out << buffer;
            }
            out << std::endl;
            out << tab << " · Spatial partitions in X: " << object_ptr->getPartitionsX() << std::endl;
            out << tab << " · Spatial partitions in Y: " << object_ptr->getPartitionsY() << std::endl;
        }
        
        tab.decrease(out);
        return out;
    }
    
    /** Loads the dense descriptor object from an XML file.
     *  \param[in] filename file where the dense descriptor object is stored.
     *  \param[out] object dense descriptor object.
     */
    template <class TFILTER, class TFEATURE>
    void loadObject(const char * filename, DenseDescriptor<TFILTER, TFEATURE> * &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        if (!parser.isTagIdentifier("Dense_Descriptor_Information"))
            throw srv::Exception("File '%s' does not contain a dense descriptor object.");
        object = DenseDescriptorFactory<TFILTER, TFEATURE>::Type::getInstantiator((srv::DDESCRIPTOR_IDENTIFIER)((int)parser.getAttribute("Identifier")));
        object->convertFromXML(parser);
        delete file;
    }
    
    /** Saves the dense descriptor object into an XML file.
     *  \param[in] filename file where the vector dense descriptor is going to be stored.
     *  \param[in] object dense descriptor object.
     */
    template <class TFILTER, class TFEATURE>
    void saveObject(const char * filename, const DenseDescriptor<TFILTER, TFEATURE> * object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    /** Generates a vector with the size of the dense descriptor regions at each scale.
     *  \param[in] object dense descriptors object.
     *  \param[out] region_sizes output vector with the size of the regions at each scale.
     */
    template <class TFILTER, class TFEATURE>
    inline void regionSizes(const DenseDescriptor<TFILTER, TFEATURE> * object, VectorDense<unsigned int> &region_sizes)
    {
        region_sizes.set(object->getNumberOfScales());
        for (unsigned int i = 0; i < object->getNumberOfScales(); ++i)
            region_sizes[i] = object->getScalesInformation(i).getRegionSize();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

