// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_MATRIX_HPP_HEADER_FILE__
#define __SRV_MATRIX_HPP_HEADER_FILE__

#include <iostream>
#include <limits>
#include "srv_utilities.hpp"
#include "vector/srv_vector.hpp"
#include "vector/srv_vector_operators.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | SIMPLE MATRIX CLASS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Simple Matrix class.
     *
     *  Simple template matrix class where the number of columns and rows are specified using a template.
     *  Since the size of the matrices must be specified directly (must be known during compilation time),
     *  the matrices checking is performed directly by the compiler. This class has also efficient implementations
     *  for specific matrix sizes (2x2, 3x3, ...).
     */
    template <class T, unsigned int NUMBER_OF_ROWS, unsigned int NUMBER_OF_COLUMNS = NUMBER_OF_ROWS>
    class MatrixStatic
    {
    public:
        /** \brief Constant access to the value of a matrix cell.
         *  
         *  Returns a constant reference to the (i, j) matrix cell. The function does not check
         *  if the specified cell is valid.
         *
         *  \param[in] row row of the selected cell.
         *  \param[in] col column of the selected cell.
         *  \return A constant reference to the matrix cell.
         */
        inline const T& operator()(unsigned int row, unsigned int column) const {return m_matrix[column + row * NUMBER_OF_COLUMNS];}
        /** \brief Access to the value of a matrix cell.
         *
         *  Returns a reference to the (i, j) matrix cell. The function does not check if the
         *  specified cell is valid.
         *  
         *  \param[in] row row of the selected cell.
         *  \param[in] col column of the selected cell.
         *  \return A reference to the matrix cell.
         */
        inline T& operator()(unsigned int row, unsigned int column) {return m_matrix[column + row * NUMBER_OF_COLUMNS];}
        /** \brief Access to a row of the matrix.
         *  
         *  Returns a constant pointer to the specified row of the matrix. The function does not
         *  check if the specified row is valid.
         *
         *  \param[in] row row of the matrix.
         *  \return A constant pointer to the values of the matrix row.
         */
        inline const T* operator()(unsigned int row) const {return &m_matrix[row * NUMBER_OF_COLUMNS];}
        /** \brief Access to a row of the matrix.
         *  
         *  Returns a pointer to the specified row of the matrix. The function does not
         *  check if the specified row is valid.
         *
         *  \param[in] row row of the matrix.
         *  \return A pointer to the values of the matrix row.
         */
        inline T* operator()(unsigned int row) {return &m_matrix[row * NUMBER_OF_COLUMNS];}
        /** \brief Access to the matrix values array.
         *
         *  Returns a constant pointer to the values of the matrix array.
         *  
         *  \return A constant pointer to the values of the matrix.
         */
        inline const T* operator()(void) const {return m_matrix;}
        /** \brief Access to the matrix values array.
         *
         *  Returns a pointer to the values of the matrix array.
         *  
         *  \return A pointer to the values of the matrix.
         */
        inline T* operator()(void) {return m_matrix;}
        /** \brief Constant access to the value of a matrix cell.
         *  
         *  Returns a constant reference to the (i, j) matrix cell. The function does not check
         *  if the specified cell is valid.
         *
         *  \param[in] row row of the selected cell.
         *  \param[in] col column of the selected cell.
         *  \return A constant reference to the matrix cell.
         */
        inline const T& at(unsigned int row, unsigned int column) const {return m_matrix[column + row * NUMBER_OF_COLUMNS];}
        /** \brief Access to the value of a matrix cell.
         *
         *  Returns a reference to the (i, j) matrix cell. The function does not check if the
         *  specified cell is valid.
         */
        inline T& at(unsigned int row, unsigned int column) {return m_matrix[column + row * NUMBER_OF_COLUMNS];}
        /** \brief Access to a row of the matrix.
         *  
         *  Returns a constant pointer to the specified row of the matrix. The function does not
         *  check if the specified row is valid.
         *
         *  \param[in] row row of the matrix.
         *  \return A constant pointer to the values of the matrix row.
         */
        inline const T* at(unsigned int row) const {return &m_matrix[row * NUMBER_OF_COLUMNS];}
        /** \brief Access to a row of the matrix.
         *  
         *  Returns a pointer to the specified row of the matrix. The function does not
         *  check if the specified row is valid.
         *
         *  \param[in] row row of the matrix.
         *  \return A pointer to the values of the matrix row.
         */
        inline T* at(unsigned int row) {return &m_matrix[row * NUMBER_OF_COLUMNS];}
        /** \brief Access to the matrix values array.
         *
         *  Returns a constant pointer to the values of the matrix array.
         *  
         *  \return A constant pointer to the values of the matrix.
         */
        inline const T* at(void) const {return m_matrix;}
        /** \brief Access to the matrix values array.
         *
         *  Returns a pointer to the values of the matrix array.
         *  
         *  \return A pointer to the values of the matrix.
         */
        inline T* at(void) {return m_matrix;}
        /** \brief Initializes the matrix.
         *
         *  Initializes all the matrix cells to the specified value.
         *
         *  \param[in] value Initialization value.
         */
        inline void set(const T &value) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] = value;}
        /// Returns the number of columns of the matrix.
        inline unsigned int getNumberOfColumns(void) const {return NUMBER_OF_COLUMNS;}
        /// Returns the number of rows of the matrix.
        inline unsigned int getNumberOfRows(void) const {return NUMBER_OF_ROWS;}
        /** \brief Adds to the current matrix another simple matrix object.
         *
         *  \param[in] other Matrix object added.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator+=(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &other) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] += other.m_matrix[i]; return *this;}
        /** \brief Subtracts to the current matrix another simple matrix object.
         *
         *  \param[in] other Matrix object subtracted.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator-=(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &other) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] -= other.m_matrix[i]; return *this;}
        /** \brief Calculates the addition of another matrix to the current simple matrix object.
         *  \param[in] other Simple matrix object added to the current matrix.
         *  \return New matrix storing the result of the operation.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator+(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &other) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] + other.m_matrix[i]; return result;}
        /** \brief Calculates the subtraction of another matrix to the current simple matrix object.
         *  \param[in] other Simple matrix object subtracted to the current matrix.
         *  \return New matrix storing the result of the operation.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator-(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &other) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] - other.m_matrix[i]; return result;}
        /** \brief Returns the matrix resulting of the multiplication of the current matrix with another simple matrix object.
         *
         *  \param[in] other Simple matrix object multiplied to the current matrix.
         *  \return New matrix storing the result of the operation.
         */
        template <unsigned int NUMBER_OF_COLUMNS_other>
        MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS_other> operator*(const MatrixStatic<T, NUMBER_OF_COLUMNS, NUMBER_OF_COLUMNS_other> &other) const
        {
            MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS_other> result;
            for (unsigned int i = 0; i < NUMBER_OF_ROWS; ++i)
            {
                for (unsigned int j = 0; j < NUMBER_OF_COLUMNS_other; ++j)
                {
                    result(i, j) = this->at(i, 0) * other(0, i);
                    for (unsigned int k = 1; k < NUMBER_OF_COLUMNS; ++k)
                        result(i, j) += this->at(i, k) * other(k, i);
                }
            }
            return result;
        }
        
        MatrixStatic<T, 2, 2> operator*(const MatrixStatic<T, 2, 2> &other) const
        {
            MatrixStatic<T, 2, 2> result;
            result(0, 0) = this->at(0, 0) * other(0, 0) + this->at(0, 1) * other(1, 0);
            result(0, 1) = this->at(0, 0) * other(0, 1) + this->at(0, 1) * other(1, 1);
            result(1, 0) = this->at(1, 0) * other(0, 0) + this->at(1, 1) * other(1, 0);
            result(1, 1) = this->at(1, 0) * other(0, 1) + this->at(1, 1) * other(1, 1);
            return result;
        }
        MatrixStatic<T, 3, 3> operator*(const MatrixStatic<T, 3, 3> &other) const
        {
            MatrixStatic<T, 3, 3> result;
            result(0, 0) = this->at(0, 0) * other(0, 0) + this->at(0, 1) * other(1, 0) + this->at(0, 2) * other(2, 0);
            result(0, 1) = this->at(0, 0) * other(0, 1) + this->at(0, 1) * other(1, 1) + this->at(0, 2) * other(2, 1);
            result(0, 2) = this->at(0, 0) * other(0, 2) + this->at(0, 1) * other(1, 2) + this->at(0, 2) * other(2, 2);
            result(1, 0) = this->at(1, 0) * other(0, 0) + this->at(1, 1) * other(1, 0) + this->at(1, 2) * other(2, 0);
            result(1, 1) = this->at(1, 0) * other(0, 1) + this->at(1, 1) * other(1, 1) + this->at(1, 2) * other(2, 1);
            result(1, 2) = this->at(1, 0) * other(0, 2) + this->at(1, 1) * other(1, 2) + this->at(1, 2) * other(2, 2);
            result(2, 0) = this->at(2, 0) * other(0, 0) + this->at(2, 1) * other(1, 0) + this->at(2, 2) * other(2, 0);
            result(2, 1) = this->at(2, 0) * other(0, 1) + this->at(2, 1) * other(1, 1) + this->at(2, 2) * other(2, 1);
            result(2, 2) = this->at(2, 0) * other(0, 2) + this->at(2, 1) * other(1, 2) + this->at(2, 2) * other(2, 2);
            return result;
        }
        /** \brief Returns a new matrix with the contents of the current matrix multiplied by a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return Resulting matrix.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator*(const T &value) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] * value; return result;}
        /** \brief Returns a new matrix with the contents of the current matrix divided by a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return Resulting matrix.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator/(const T &value) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] / value; return result;}
        /** \brief Returns a new matrix with the contents of the current matrix adding a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return Resulting matrix.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator+(const T &value) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] + value; return result;}
        /** \brief Returns a new matrix with the contents of the current matrix subtracting a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return Resulting matrix.
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> operator-(const T &value) const {MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> result; for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) result.m_matrix[i] = m_matrix[i] - value; return result;}
        /** \brief Multiplies the matrix cell values by a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator*=(const T &value) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] *= value; return *this;}
        /** \brief Divides the matrix cell values by a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator/=(const T &value) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] /= value; return *this;}
        /** \brief Add to the matrix cell values a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator+=(const T &value) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] += value; return *this;}
        /** \brief Subtract to the matrix cell values a scalar.
         *
         *  \param[in] value Scalar value.
         *  \return A reference to the simple matrix object (a reference to \b *this).
         */
        inline MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS>& operator-=(const T &value) {for (unsigned int i = 0; i < NUMBER_OF_ROWS * NUMBER_OF_COLUMNS; ++i) m_matrix[i] -= value; return *this;}
    private:
        /// Array storing the matrix values.
        T m_matrix[NUMBER_OF_ROWS * NUMBER_OF_COLUMNS];
    };
    
    template <class T, unsigned int NUMBER_OF_ROWS, unsigned int NUMBER_OF_COLUMNS, template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    void MatrixVectorMultiplication(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &matrix, const VECTORA<TA, NA> &product_vector, VECTORB<TB, NB> &result_vector)
    {
        const T *ptr;
        
        if (product_vector.size() != (NA)NUMBER_OF_COLUMNS) throw Exception("Incorrect vector size. Product vector must have the same dimensionality as the number of columns of the matrix.");
        if (result_vector.size() != (NB)NUMBER_OF_ROWS) throw Exception("Incorrect vector size. Result vector must have the same dimensionality as the number of rows of the matrix.");
        ptr = matrix();
        for (unsigned int i = 0; i < NUMBER_OF_ROWS; ++i)
        {
            result_vector[i] = 0;
            for (unsigned int j = 0; j < NUMBER_OF_COLUMNS; ++j, ++ptr)
                result_vector[i] += *ptr * product_vector[j];
        }
    }
    template <class T, unsigned int NUMBER_OF_ROWS>
    void MatrixTranspose(MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_ROWS> &matrix)
    {
        for (unsigned int i = 0; i < NUMBER_OF_ROWS; ++i)
            for (unsigned int j = i + 1; j < NUMBER_OF_ROWS; ++j)
                srvSwap(matrix.at(i, j), matrix.at(j, i));
    }
    template <class T>
    void MatrixTranspose(MatrixStatic<T, 2, 2> &matrix)
    {
        srvSwap(matrix.at(0, 1), matrix.at(1, 0));
    }
    template <class T>
    void MatrixTranspose(MatrixStatic<T, 3, 3> &matrix)
    {
        srvSwap(matrix.at(0, 1), matrix.at(1, 0));
        srvSwap(matrix.at(0, 2), matrix.at(2, 0));
        srvSwap(matrix.at(1, 2), matrix.at(2, 1));
    }
    template <class T, unsigned int NUMBER_OF_ROWS, unsigned int NUMBER_OF_COLUMNS>
    void MatrixTranspose(const MatrixStatic<T, NUMBER_OF_ROWS, NUMBER_OF_COLUMNS> &origin_matrix, MatrixStatic<T, NUMBER_OF_COLUMNS, NUMBER_OF_ROWS> &destination_matrix)
    {
        for (unsigned int i = 0; i < NUMBER_OF_ROWS; ++i)
            for (unsigned int j = 0; j < NUMBER_OF_COLUMNS; ++j)
                destination_matrix(j, i) = origin_matrix(i, j);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GENERAL MATRIX CLASS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** \brief Matrix class
     *  
     *  Basic matrix class which uses dynamic memory to allocate the matrix memory values. The matrix uses the ATLAS library to calculate the different
     *  linear algebra algorithms.
     */
    template <class T>
    class Matrix
    {
    public:
        // -[ Constructors, destructor and assignation operator ]----------------------------------------------------------------------------------------
        /// Default constructor.
        Matrix(void);
        /** Constructor which sets the geometry of the matrix.
         *  \param[in] number_of_rows number of rows of the matrix.
         *  \param[in] number_of_columns number of columns of the matrix.
         */
        Matrix(int number_of_rows, int number_of_columns);
        /** Constructor which sets the geometry of the matrix and its initial value.
         *  \param[in] number_of_rows number of rows of the matrix.
         *  \param[in] number_of_columns number of columns of the matrix.
         *  \param[in] initial_value value used to initialize the matrix.
         */
        Matrix(int number_of_rows, int number_of_columns, const T &initial_value);
        /// Copy constructor.
        Matrix(const Matrix &copy);
        /// Destructor.
        ~Matrix(void);
        /// Assignation operator.
        Matrix& operator=(const Matrix &copy);
        /// Assignation operator for matrices of different data types.
        template <class R>
        Matrix& operator=(const Matrix<R> &copy);
        
        // -[ Functions to change the resize/reset the matrix ]------------------------------------------------------------------------------------------
        void set(const T &value);
        void set(int number_of_rows, int number_of_columns);
        void set(int number_of_rows, int number_of_columns, const T &value);
        void eye(int size);
        void reshape(int number_of_rows, int number_of_columns);
        void reshape(Matrix<T> &destination) const;
        void submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<T> &destination) const;
        void submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<T> &destination, int destination_row, int destination_column) const;
        Matrix<T> submatrix(int initial_row, int initial_column, int final_row, int final_column) const;
        template <class R>
        void submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<R> &destination) const;
        template <class R>
        void submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<R> &destination, int destination_row, int destination_column) const;
        template <class R>
        Matrix<R> submatrix(int initial_row, int initial_column, int final_row, int final_column) const;
        inline void copyColumn(int destination_column, const T *column);
        template <class R>
        inline void copyColumn(int destination_column, const R *column);
        
        // -[ Functions to access to the matrix fields ]-------------------------------------------------------------------------------------------------
        /// Returns the number of rows of the matrix.
        inline int getNumberOfRows(void) const {return m_number_of_rows;}
        /// Returns the number of columns of the matrix.
        inline int getNumberOfColumns(void) const {return m_number_of_columns;}
        /// Returns a constant pointer to the matrix values array. The matrix values are stored following a column-major order.
        inline const T* operator()(void) const {return m_matrix_values;}
        /// Returns a pointer to the matrix values array. The matrix values are stored following a column-major order.
        inline T* operator()(void) {return m_matrix_values;}
        /** Returns a constant pointer to a column of the matrix.
         *  \param column column of the matrix.
         *  \return a constant pointer to the array containing the column matrix values.
         */
        inline const T* operator()(int column) const { return m_matrix_values + m_number_of_rows * column; }
        /** Returns a pointer to a column of the matrix.
         *  \param column column of the matrix.
         *  \return a pointer to the array containing the column matrix values.
         */
        inline T* operator()(int column) { return m_matrix_values + m_number_of_rows * column; }
        /** Returns a constant reference an element of the matrix.
         *  \param row row of the element.
         *  \param column column of the element.
         *  \return constant reference to the (row, column) element of the matrix.
         */
        inline const T& operator()(int row, int column) const { return *(m_matrix_values + m_number_of_rows * column + row); }
        /** Returns a reference an element of the matrix.
         *  \param row row of the element.
         *  \param column column of the element.
         *  \return reference to the (row, column) element of the matrix.
         */
        inline T& operator()(int row, int column) { return *(m_matrix_values + m_number_of_rows * column + row); }
        
        // -[ XML conversion functions ]-----------------------------------------------------------------------------------------------------------------
        /// Stores the matrix information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the matrix information form a XML object.
        void convertFromXML(XmlParser &parser);
        
        // -[ Matrix functions ]-------------------------------------------------------------------------------------------------------------------------
        /** This function accumulates the values of matrix columns.
         *  \param[out] result a vector with the accumulated sum of each column.
         *  \param[in] number_of_threads number of threads used to concurrently process the matrix.
         */
        inline void sumColumns(VectorDense<T> &result, unsigned int number_of_threads = 1) const
        {
            if (result.size() != m_number_of_columns) result.set(m_number_of_columns);
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < m_number_of_columns; j += number_of_threads)
                {
                    T accum = 0;
                    for (unsigned int i = 0, k = j * m_number_of_rows; i < m_number_of_rows; ++i, ++k)
                        accum += m_matrix_values[k];
                    result[j] = accum;
                }
            }
        }
        /** This function accumulates the values of matrix rows.
         *  \param[out] result a vector with the accumulated sum of each row.
         *  \param[in] number_of_threads number of threads used to concurrently process the matrix.
         */
        inline void sumRows(VectorDense<T> &result, unsigned int number_of_threads = 1) const
        {
            if ((int)result.size() != m_number_of_rows) result.set((unsigned int)m_number_of_rows, 0);
            else result.setValue(0);
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (int j = 0, k = 0; j < m_number_of_columns; ++j, k += m_number_of_rows)
                    for (int i = omp_get_thread_num(); i < m_number_of_rows; i += number_of_threads)
                        result[i] += m_matrix_values[k + i];
            }
        }
        
        // -[ Matrix operators ]-------------------------------------------------------------------------------------------------------------------------
        /// Transposes the matrix.
        void transpose(void);
        
        /// This function overloads the addition assignment operator for a value.
        inline Matrix<T>& operator+=(const T &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] += value; return *this; }
        /// This function overloads the addition assignment operator for a value of a different data type than the matrix.
        template <class R>
        inline Matrix<T>& operator+=(const R &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] += (T)value; return *this; }
        
        /// This function overloads the subtraction assignment operator for a value.
        inline Matrix<T>& operator-=(const T &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] -= value; return *this; }
        /// This function overloads the subtraction assignment operator for a value of a different data type than the matrix..
        template <class R>
        inline Matrix<T>& operator-=(const R &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] -= (T)value; return *this; }
        
        /// This function overloads the multiplication assignment operator for a value.
        inline Matrix<T>& operator*=(const T &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] *= value; return *this; }
        /// This function overloads the multiplication assignment operator for a value of a different data type than the matrix..
        template <class R>
        inline Matrix<T>& operator*=(const R &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] *= (T)value; return *this; }
        
        /// This function overloads the division assignment operator for a value.
        inline Matrix<T>& operator/=(const T &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] /= value; return *this; }
        /// This function overloads the division assignment operator for a value of a different data type than the matrix..
        template <class R>
        inline Matrix<T>& operator/=(const R &value) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] /= (T)value; return *this; }
        
        /** This function overloads the addition assignment operator to add the values of another matrix to the current matrix.
         *  \param[in] other matrix added to the current matrix.
         *  \returns a reference to the current matrix.
         *  \note this function does not check that both matrices have the same geometry.
         */
        inline Matrix<T>& operator+=(const Matrix<T> &other) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] += other.m_matrix_values[i]; return *this; }
        /** This function overloads the subtraction assignment operator to subtracts the values of another matrix to the current matrix.
         *  \param[in] other matrix subtracted to the current matrix.
         *  \returns a reference to the current matrix.
         *  \note this function does not check that both matrices have the same geometry.
         */
        inline Matrix<T>& operator-=(const Matrix<T> &other) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] -= other.m_matrix_values[i]; return *this; }
        /** This function overloads the multiplication assignment operator to multiplies the values of another matrix to the current matrix.
         *  \param[in] other matrix which multiplies the element-wise the values of the current matrix.
         *  \returns a reference to the current matrix.
         *  \note this function does not check that both matrices have the same geometry.
         */
        inline Matrix<T>& operator*=(const Matrix<T> &other) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] *= other.m_matrix_values[i]; return *this; }
        /** This function overloads the division assignment operator to divide the values of another matrix to the current matrix.
         *  \param[in] other matrix which divides the element-wise the values of the current matrix.
         *  \returns a reference to the current matrix.
         *  \note this function does not check that both matrices have the same geometry.
         */
        inline Matrix<T>& operator/=(const Matrix<T> &other) { for (int i = 0; i < m_number_of_rows * m_number_of_columns; ++i) m_matrix_values[i] /= other.m_matrix_values[i]; return *this; }
        
        template <class R>
        friend std::ostream& operator<<(std::ostream &out, const Matrix<R> &matrix);
        
    private:
        /// Array containing the values of the matrix stored following a column major order.
        T * m_matrix_values;
        /// Number of rows of the matrix.
        int m_number_of_rows;
        /// Number of columns of the matrix.
        int m_number_of_columns;
    };
    
    /// Default constructor.
    template <class T>
    Matrix<T>::Matrix(void) :
        m_matrix_values(0),
        m_number_of_rows(0),
        m_number_of_columns(0) {}
    
    /** Parameters constructor where the matrix geometry is specified in the function parameters.
     *  \param number_of_rows number of rows of the matrix.
     *  \param number_of_columns number of columns of the matrix.
     */
    template <class T>
    Matrix<T>::Matrix(int number_of_rows, int number_of_columns) :
        m_matrix_values(((number_of_rows > 0) && (number_of_columns > 0))?new T[number_of_rows * number_of_columns]:0),
        m_number_of_rows(number_of_rows),
        m_number_of_columns(number_of_columns) {}
    
    /** Parameters constructor where the matrix geometry and the initial value of the matrix elements is given as parameters.
     *  \param number_of_rows number of rows of the matrix.
     *  \param number_of_columns number of columns of the matrix.
     *  \param initial_value initialization value.
     */
    template <class T>
    Matrix<T>::Matrix(int number_of_rows, int number_of_columns, const T &initial_value) :
        m_matrix_values(((number_of_rows > 0) && (number_of_columns > 0))?new T[number_of_rows * number_of_columns]:0),
        m_number_of_rows(number_of_rows),
        m_number_of_columns(number_of_columns)
    {
        T *values_ptr = m_matrix_values;
        for (int column = 0; column < number_of_columns; ++column)
            for (int row = 0; row < number_of_rows; ++row, ++values_ptr)
                *values_ptr = initial_value;
    }
    
    /// Copy constructor.
    template <class T>
    Matrix<T>::Matrix(const Matrix &copy) :
        m_matrix_values((copy.m_matrix_values != 0)?new T[copy.m_number_of_rows * copy.m_number_of_columns]:0),
        m_number_of_rows(copy.m_number_of_rows),
        m_number_of_columns(copy.m_number_of_columns)
    {
        const T *values_copy_ptr = copy.m_matrix_values;
        T *values_ptr = m_matrix_values;
        for (int column = 0; column < copy.m_number_of_columns; ++column)
            for (int row = 0; row < copy.m_number_of_rows; ++row, ++values_ptr, ++values_copy_ptr)
                *values_ptr = *values_copy_ptr;
    }
    
    /// Destructor.
    template <class T>
    Matrix<T>::~Matrix(void)
    {
        if (m_matrix_values != 0) delete [] m_matrix_values;
    }
    
    /// Assignation operator
    template <class T>
    Matrix<T>& Matrix<T>::operator=(const Matrix<T> &copy)
    {
        if (this != &copy)
        {
            if (m_matrix_values != 0) delete [] m_matrix_values;
            if (copy.m_matrix_values != 0)
            {
                const T *values_copy_ptr;
                T *values_ptr;
                
                values_copy_ptr = copy.m_matrix_values;
                values_ptr = m_matrix_values = new T[copy.m_number_of_rows * copy.m_number_of_columns];
                m_number_of_rows = copy.m_number_of_rows;
                m_number_of_columns = copy.m_number_of_columns;
                for (int column = 0; column < copy.m_number_of_columns; ++column)
                    for (int row = 0; row < copy.m_number_of_rows; ++row, ++values_ptr, ++values_copy_ptr)
                        *values_ptr = *values_copy_ptr;
            }
            else
            {
                m_matrix_values = 0;
                m_number_of_rows = m_number_of_columns = 0;
            }
        }
        
        return *this;
    }
    
    /// Assignation operator
    template <class T>
    template <class R>
    Matrix<T>& Matrix<T>::operator=(const Matrix<R> &copy)
    {
        if ((void*)this != (void*)&copy)
        {
            if (m_matrix_values != 0) delete [] m_matrix_values;
            if (copy() != 0)
            {
                const R *values_copy_ptr;
                T *values_ptr;
                
                values_copy_ptr = copy();
                values_ptr = m_matrix_values = new T[copy.getNumberOfRows() * copy.getNumberOfColumns()];
                m_number_of_rows = copy.getNumberOfRows();
                m_number_of_columns = copy.getNumberOfColumns();
                for (int column = 0; column < copy.getNumberOfColumns(); ++column)
                    for (int row = 0; row < copy.getNumberOfRows(); ++row, ++values_ptr, ++values_copy_ptr)
                        *values_ptr = (T)*values_copy_ptr;
            }
            else
            {
                m_matrix_values = 0;
                m_number_of_rows = m_number_of_columns = 0;
            }
        }
        
        return *this;
    }
    
    /** Sets all values of the matrix to the specified value.
     *  \param[in] value initialization value.
     */
    template <class T>
    void Matrix<T>::set(const T &value)
    {
        T *matrix_ptr = m_matrix_values;
        for (int column = 0; column < m_number_of_columns; ++column)
            for (int row = 0; row < m_number_of_rows; ++row, ++matrix_ptr)
                *matrix_ptr = value;
    }
    
    /** Sets the geometry of the matrix.
     *  \param[in] number_of_rows new number of rows of the matrix.
     *  \param[in] number_of_columns new number of columns of the matrix.
     */
    template <class T>
    void Matrix<T>::set(int number_of_rows, int number_of_columns)
    {
        if ((number_of_rows != m_number_of_rows) || (number_of_columns != m_number_of_columns))
        {
            delete [] m_matrix_values;
            if ((number_of_rows > 0) && (number_of_columns > 0))
            {
                m_matrix_values = new T[number_of_rows * number_of_columns];
                m_number_of_rows = number_of_rows;
                m_number_of_columns = number_of_columns;
            }
            else
            {
                m_matrix_values = 0;
                m_number_of_rows = 0;
                m_number_of_columns = 0;
            }
        }
    }
    
    /** Sets the geometry and initializes the matrix.
     *  \param[in] number_of_rows number of rows of the matrix.
     *  \param[in] number_of_columns number of columns of the matrix.
     *  \param[in] value initialization value.
     */
    template <class T>
    void Matrix<T>::set(int number_of_rows, int number_of_columns, const T &value)
    {
        T *matrix_ptr;
        
        if ((number_of_rows != m_number_of_rows) || (number_of_columns != m_number_of_columns))
        {
            delete [] m_matrix_values;
            if ((number_of_rows > 0) && (number_of_columns > 0))
            {
                m_matrix_values = new T[number_of_rows * number_of_columns];
                m_number_of_rows = number_of_rows;
                m_number_of_columns = number_of_columns;
            }
            else
            {
                m_matrix_values = 0;
                m_number_of_rows = 0;
                m_number_of_columns = 0;
            }
        }
        matrix_ptr = m_matrix_values;
        for (int column = 0; column < number_of_columns; ++column)
            for (int row = 0; row < number_of_rows; ++row, ++matrix_ptr)
                *matrix_ptr = value;
    }
    
    /** Creates an identity matrix of the specified size.
     *  \param[in] size size of the squared identity matrix.
     */
    template <class T>
    void Matrix<T>::eye(int size)
    {
        if ((m_number_of_rows != size) || (m_number_of_columns != size))
        {
            delete [] m_matrix_values;
            m_number_of_rows = size;
            m_number_of_columns = size;
            m_matrix_values = new T[m_number_of_rows * m_number_of_columns];
        }
        
        for (int i = 0, k = 0; i < size; ++i)
        {
            for (int j = 0; j < size; ++j, ++k)
            {
                if (i == j) m_matrix_values[k] = 1;
                else m_matrix_values[k] = 0;
            }
        }
    }
    
    /** This function changes the geometry of the matrix object.
     *  \param[in] number_of_rows new number of rows of the matrix.
     *  \param[in] number_of_columns new number of columns of the matrix.
     */
    template <class T>
    void Matrix<T>::reshape(int number_of_rows, int number_of_columns)
    {
        if ((m_number_of_rows != number_of_rows) || (m_number_of_columns != number_of_columns))
        {
            T *matrix_values, *current_matrix_values, *new_matrix_values;
            if (m_number_of_rows * m_number_of_columns != number_of_rows * number_of_columns)
                throw Exception("Matrix reshape error, the new matrix geometry results in a different amount of values.");
            m_number_of_columns = number_of_columns;
            m_number_of_rows = number_of_rows;
        }
    }
    
    /** This function copies the matrix to another matrix with a different geometry. The destination matrix keeps its geometry, so that the values are reshape
     *  to fit the geometry of the destination matrix.
     *  \param[in] number_of_rows new number of rows of the matrix.
     *  \param[in] number_of_columns new number of columns of the matrix.
     */
    template <class T>
    void Matrix<T>::reshape(Matrix<T> &destination) const
    {
        if ((m_number_of_rows != destination.getNumberOfRows()) || (m_number_of_columns != destination.getNumberOfColumns()))
        {
            const T *current_matrix_values;
            T *destination_matrix_values;
            if (m_number_of_rows * m_number_of_columns != destination.getNumberOfRows() * destination.getNumberOfColumns())
                throw Exception("Matrix reshape error, the geometry of the destination matrix results in a different amount of values than the current matrix.");
            
            current_matrix_values = m_matrix_values;
            destination_matrix_values = destination.m_matrix_values;
            for (int i = 0; i < m_number_of_columns * m_number_of_rows; ++i, ++current_matrix_values, ++destination_matrix_values)
                *destination_matrix_values = *current_matrix_values;
        }
        else destination = *this;
    }
    
    /** Copies a submatrix of the current matrix to a destination matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \param[out] destination destination matrix where the submatrix is copied to.
     */
    template <class T>
    void Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<T> &destination) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        
        destination.set(final_row - initial_row + 1, final_column - initial_column + 1);
        T *result_matrix_ptr = destination.m_matrix_values;
        for (int column = initial_column; column <= final_column; ++column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
    }
    
    template <>
    void Matrix<double>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<double> &destination) const;
    
    /** Copies a submatrix of the current matrix to a destination matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \param[out] destination destination matrix where the submatrix is copied to.
     */
    template <class T>
    template <class R>
    void Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<R> &destination) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        
        destination.set(final_row - initial_row + 1, final_column - initial_column + 1);
        R *result_matrix_ptr = destination.m_matrix_values;
        for (int column = initial_column; column <= final_column; ++column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
    }
    
    /** Copies a submatrix of the current matrix to the specified "coordinates" (initial row and column) of a destination matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \param[out] destination destination matrix where the submatrix is copied to.
     *  \param[in] destination_row initial row of the destination matrix where the submatrix is copied.
     *  \param[in] destination_column initial column of the destination matrix where the submatrix is copied.
     */
    template <class T>
    void Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<T> &destination, int destination_row, int destination_column) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        if ((destination_row < 0) || (destination_column < 0))
            throw Exception("Error, invalid submatrix destination row/column.");
        if ((final_row - initial_row + 1 > destination.m_number_of_rows - destination_row) || (final_column - initial_column + 1 > destination.m_number_of_columns - destination_column))
            throw Exception("Error, the destination submatrix is smaller than the original submatrix.");
        
        for (int column = initial_column, result_column = destination_column; column <= final_column; ++column, ++result_column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            T *result_matrix_ptr = destination.m_matrix_values + result_column * destination.m_number_of_rows + destination_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
    }
    
    template <>
    void Matrix<double>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<double> &destination, int destination_row, int destination_column) const;
    
    /** Copies a submatrix of the current matrix to the specified "coordinates" (initial row and column) of a destination matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \param[out] destination destination matrix where the submatrix is copied to.
     *  \param[in] destination_row initial row of the destination matrix where the submatrix is copied.
     *  \param[in] destination_column initial column of the destination matrix where the submatrix is copied.
     */
    template <class T>
    template <class R>
    void Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column, Matrix<R> &destination, int destination_row, int destination_column) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        if ((destination_row < 0) || (destination_column < 0))
            throw Exception("Error, invalid submatrix destination row/column.");
        if ((final_row - initial_row + 1 > destination.m_number_of_rows - destination_row) || (final_column - initial_column + 1 > destination.m_number_of_columns - destination_column))
            throw Exception("Error, the destination submatrix is smaller than the original submatrix.");
        
        for (int column = initial_column, result_column = destination_column; column <= final_column; ++column, ++result_column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            R *result_matrix_ptr = destination.m_matrix_values + result_column * destination.m_number_of_rows + destination_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
    }
    
    /** Returns a submatrix of the current matrix. The submatrix is a different matrix object, so that, changes in the returned submatrix
     *  does not affect the original matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \return resulting submatrix.
     */
    template <class T>
    Matrix<T> Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        Matrix<T> result(final_row - initial_row + 1, final_column - initial_column + 1);
        T *result_matrix_ptr = result.m_matrix_values;
        for (int column = initial_column; column <= final_column; ++column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
        return result;
    }
    
    /** Returns a submatrix of the current matrix. The submatrix is a different matrix object, so that, changes in the returned submatrix
     *  does not affect the original matrix.
     *  \param[in] initial_row first row of the submatrix.
     *  \param[in] initial_column first column of the submatrix.
     *  \param[in] final_row last row (final_row value included) of the submatrix.
     *  \param[in] final_column last column (final_column value included) of the submatrix.
     *  \return resulting submatrix.
     */
    template <class T>
    template <class R>
    Matrix<R> Matrix<T>::submatrix(int initial_row, int initial_column, int final_row, int final_column) const
    {
        if ((initial_row < 0) || (initial_row >= m_number_of_rows) || (final_row < 0) || (final_row >= m_number_of_rows) ||
                (initial_column < 0) || (initial_column >= m_number_of_columns) || (final_column < 0) || (final_column >= m_number_of_columns))
            throw Exception("Error, incorrect submatrix geometry.");
        if ((initial_row > final_row) || (initial_column > final_column))
            throw Exception("Error, the initial row/column of the submatrix is greater than the final row/column");
        Matrix<R> result(final_row - initial_row + 1, final_column - initial_column + 1);
        R *result_matrix_ptr = result.m_matrix_values;
        for (int column = initial_column; column <= final_column; ++column)
        {
            const T *submatrix_ptr = m_matrix_values + column * m_number_of_rows + initial_row;
            for (int row = initial_row; row <= final_row; ++row, ++result_matrix_ptr, ++submatrix_ptr)
                *result_matrix_ptr = *submatrix_ptr;
        }
        return result;
    }
    
    /** This functions copies a vector to the specified column of the matrix.
     *  \param[in] destination_column column of the current matrix where the vector is going to be copied to.
     *  \param[in] column constant array which is going to be copied to the specified column.
     */
    template <class T>
    inline void Matrix<T>::copyColumn(int destination_column, const T *column)
    {
        T *destination_ptr = m_matrix_values + destination_column * m_number_of_rows;
        for (int i = 0; i < m_number_of_rows; ++i)
            destination_ptr[i] = column[i];
    }
    
    template <>
    void Matrix<double>::copyColumn(int destination_column, const double *column);
    template <>
    void Matrix<float>::copyColumn(int destination_column, const float *column);
    
    /** This functions copies a vector of a different type to the specified column of the matrix.
     *  \param[in] destination_column column of the current matrix where the vector is going to be copied to.
     *  \param[in] column constant array which is going to be copied to the specified column.
     */
    template <class T>
    template <class R>
    inline void Matrix<T>::copyColumn(int destination_column, const R *column)
    {
        T *destination_ptr = m_matrix_values + destination_column * m_number_of_rows;
        for (int i = 0; i < m_number_of_rows; ++i)
            destination_ptr[i] = (T)column[i];
    }
    
    /** Convert the matrix into an XML object.
     *  \param parser an XML object parser.
     */
    template <class T>
    void Matrix<T>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Matrix");
        parser.setAttribute("NumberOfRows", m_number_of_rows);
        parser.setAttribute("NumberOfColumns", m_number_of_columns);
        parser.addChildren();
        if (m_matrix_values != 0)
        {
            parser.openTag("MatrixValues");
            parser.addChildren();
            writeToBase64Array<T>(parser, m_matrix_values, (unsigned long)(m_number_of_rows * m_number_of_columns));
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    /** Retrieves the matrix values from an XML object.
     *  \param parser an XML object parser.
     */
    template <class T>
    void Matrix<T>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Matrix"))
        {
            if (m_matrix_values != 0) delete [] m_matrix_values;
            m_matrix_values = 0;
            m_number_of_rows = parser.getAttribute("NumberOfRows");
            m_number_of_columns = parser.getAttribute("NumberOfColumns");
            
            while (!(parser.isTagIdentifier("Matrix") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("MatrixValues"))
                {
                    m_matrix_values = new T[m_number_of_rows * m_number_of_columns];
                    readFromBase64Array(parser, "MatrixValues", m_matrix_values, (unsigned long)(m_number_of_rows * m_number_of_columns));
                    while (!(parser.isTagIdentifier("MatrixValues") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    template <class T>
    void Matrix<T>::transpose(void)
    {
        if (m_matrix_values != 0)
        {
            T * new_values;
            
            new_values = new T[m_number_of_rows * m_number_of_columns];
            for (int j = 0; j < m_number_of_columns; ++j)
            {
                const T * __restrict__ src_ptr = m_matrix_values + (j * m_number_of_rows);
                T * __restrict__ dst_ptr = new_values + j;
                for (int i = 0; i < m_number_of_rows; ++i)
                {
                    *dst_ptr = src_ptr[i];
                    dst_ptr += m_number_of_columns;
                }
            }
            
            srvSwap(m_number_of_rows, m_number_of_columns);
            delete [] m_matrix_values;
            m_matrix_values = new_values;
        }
    }
    
    template <class T>
    std::ostream& operator<<(std::ostream &out, const Matrix<T> &matrix)
    {
        if ((matrix.getNumberOfColumns() > 0) && (matrix.getNumberOfRows() > 0))
        {
            int number_of_digits;
            double exponent;
            T largest;
            largest = srvAbs<T>(matrix()[0]);
            for (int index = 1; index < matrix.getNumberOfRows() * matrix.getNumberOfColumns(); ++index)
                largest = srvMax<T>(srvAbs<T>(matrix()[index]), largest);
            if (largest != 0)
                number_of_digits = (int)floor(log(largest) / log(10));
            else number_of_digits = 0;
            exponent = pow(10.0, (double)number_of_digits);
            if (number_of_digits != 0)
                out << "1e" << number_of_digits << " * " << std::endl;
            for (int row = 0; row < matrix.getNumberOfRows(); ++row)
            {
                for (int column = 0; column < matrix.getNumberOfColumns(); ++column)
                {
                    char buffer[64];
                    sprintf(buffer, " %9.6f", matrix(row, column) / exponent);
                    out << buffer;
                }
                out << std::endl;
            }
        }
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALLS TO ATLAS FUNCTIONS FOR DOUBLE  |
    //                   | PRECISION FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    template <class T> bool MatrixIsSymmetric(const Matrix<T> &X);
    
    /** Double-precision Generalized Matrix Multiplication algorithm. This algorithm calculates the matrix multiplication C = alpha * op(\b A ) * op(\b B ) + beta * C, where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] alpha is the factor which scales the matrix multiplication.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[in] beta is the factor which scales the result matrix \b R before adding the multiplication result.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixGEMM(double alpha, const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, double beta, Matrix<double> &R);
    /** Double-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = alpha * op(\b A ) * op(\b B ), where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] alpha is the factor which scales the matrix multiplication.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(double alpha, const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, Matrix<double> &R);
    /** Double-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = op(\b A ) * op(\b B ), where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(const Matrix<double> &A, bool transposeA, const Matrix<double> &B, bool transposeB, Matrix<double> &R);
    /** Double-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C =\b A *\b B.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(const Matrix<double> &A, const Matrix<double> &B, Matrix<double> &R);
    /** Double-precision partial matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = [\b A *\b B(:, 1:K), zeros(:, K + 1:N)], where
     *  \b K is the number of columns of\b B which are used to calculate the matrix multiplication and\b N is the number of columns of\b C. Therefore, the resulting matrix\b C has
     *  in the first\b K columns the result of the multiplication of\b A *\b B and the rest are set to zero.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] final_column the final column of the matrix B which is used in the multiplication.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixPartialMultiplication(const Matrix<double> &A, const Matrix<double> &B, int final_column, Matrix<double> &R);
    /** Double-precision matrix QR factorization of the matrix \b Q. The functions calculates the\b Q matrix in place.
     *  \param[in,out] Q a reference to the original rectangular matrix which the functions is going to transform to the \b Q matrix.
     *  \param[in] R a reference to the\b R triangular matrix.
     *  \param[in] number_of_threads number of threads used to calculate the\b QR decomposition.
     */
    void MatrixQRDecomposition(Matrix<double> &Q, Matrix<double> &R, unsigned int number_of_threads = 1);
    /** Double-precision singular value decomposition algorithm which decomposes A = U * D * V^T
     *  \param[in,out] A matrix to be decomposed (the matrix can be modified).
     *  \param[out] U resulting U matrix.
     *  \param[out] s vector storing the trace of the D matrix.
     *  \param[out] V resulting transposed V matrix.
     *  \param[in] economic calculates the economic SVD.
     */
    void MatrixSVD(Matrix<double> &A, Matrix<double> &U, VectorDense<double> &s, Matrix<double> &V, bool economic = false);
    /////// /** Double-precision Reduced Singular Value Decomposition which uses the QR decomposition to reduce the dimensionality of the A matrix and efficiently calculate the SVD matrix decomposition.
    ///////  *  \param[in,out] A matrix to be decomposed (the matrix can be modified).
    ///////  *  \param[out] U resulting U matrix.
    ///////  *  \param[out] s vector storing the trace of the D matrix.
    ///////  *  \param[out] V resulting V matrix.
    ///////  *  \param[in] number_of_threads number of threads used to calculate the QR decomposition.
    ///////  */
    /////// void MatrixRSVD(Matrix<double> &A, Matrix<double> &U, VectorDense<double> &s, Matrix<double> &V, unsigned int number_of_threads = 1);
    /** Double-precision matrix inversion function.
     *  \param[in] original original matrix.
     *  \param[out] inverse resulting inverted matrix.
     */
    void MatrixInverse(const Matrix<double> &original, Matrix<double> &inverse);
    /** Double-precision in-place matrix inversion function.
     *  \param[in,out] inverse matrix to be inverted.
     */
    void MatrixInverse(Matrix<double> &inverse);
    /** Double-precision function which computes the solution to a real system of linear equations
     *  A * X = B, where A is an N-by-N matrix and X and B are N-byNRHS matrices. The LU
     *  decomposition with partial pivoting and row interchanges is used to factor A as
     *      A = P * L * U,
     *  where P is a permutation matrix, L is a unit lower triangular, and U is upper triangular.
     *  The factored for of A is used then to solve the system of equations A * X = B;
     *  
     *  \param[in] A The N-by-N coefficient matrix A.
     *  \param[in] B The N-by-NRHS matrix of right hand side matrix B.
     *  \param[out] solution The N-by-NRHS solution matrix X.
     *  \throw ExceptionSingularity when the matrix is singular.
     *  \throw ExceptionArguments when the parameters are incorrect.
     */
    void MatrixSolve(const Matrix<double> &A, const Matrix<double> &B, Matrix<double> &solution);
    /** Double-precision function which computes the solution to a real system of linear equations
     *  A * X = B, where A is an N-by-N matrix and X and B are N-byNRHS matrices. The LU
     *  decomposition with partial pivoting and row interchanges is used to factor A as
     *      A = P * L * U,
     *  where P is a permutation matrix, L is a unit lower triangular, and U is upper triangular.
     *  The factored for of A is used then to solve the system of equations A * X = B;
     *  
     *  \param[in,out] A The N-by-N coefficient matrix A.
     *  \param[in,out] B The N-by-NRHS matrix of right hand side matrix B.
     *  \param[out] IPIV The N pivot indices that define the permutation matrix P; Row i of the matrix was interchanged with row IPIV(i)
     *  \throw ExceptionSingularity when the matrix is singular.
     *  \throw ExceptionArguments when the parameters are incorrect.
     */
    void MatrixSolve(Matrix<double> &A, Matrix<double> &B, VectorDense<int> &IPIV);
    /** Double-precision function which calculates the eigenvalues and eigenvectors of a symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenSymmetric(const Matrix<double> &A, Matrix<double> &V, VectorDense<double> &D);
    /** Double-precision function which calculates the eigenvalues of a symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenSymmetric(const Matrix<double> &A, VectorDense<double> &D);
    /** Double-precision function which calculates the eigenvalues and eigenvectors of a non-symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenNonSymmetric(const Matrix<double> &A, bool no_balance, Matrix<double> &V, VectorDense<double> &D);
    /** Double-precision function which calculates the eigenvalues of a non-symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenNonSymmetric(const Matrix<double> &A, bool no_balance, VectorDense<double> &D);
    /** Double-precision function which returns the eigenvalues and eigenvectors of the given matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    inline void MatrixEigen(const Matrix<double> &A, bool no_balance, Matrix<double> &V, VectorDense<double> &D)
    {
        if (MatrixIsSymmetric(A)) MatrixEigenSymmetric(A, V, D);
        else MatrixEigenNonSymmetric(A, no_balance, V, D);
    }
    /** Double-precision function which returns the eigenvalues of the given matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    inline void MatrixEigen(const Matrix<double> &A, bool no_balance, VectorDense<double> &D)
    {
        if (MatrixIsSymmetric(A)) MatrixEigenSymmetric(A, D);
        else MatrixEigenNonSymmetric(A, no_balance, D);
    }
    /** Double-precision function which calculates the logarithm of the given matrix.
     *  \param[in] A source matrix.
     *  \param[out] B resulting matrix.
     *  \param[in] number_of_threads number of threads used to calculate the matrix logarithm.
     */
    void MatrixLogarithm(const Matrix<double> &A, Matrix<double> &B, unsigned int number_of_threads = 1);
    /** Double-precision function which calculates the exponential of the given matrix.
     *  \param[in] A source matrix.
     *  \param[out] B resulting matrix.
     *  \param[in] number_of_threads number of threads used to calculate the matrix exponential.
     */
    void MatrixExponential(const Matrix<double> &A, Matrix<double> &B, unsigned int number_of_threads = 1);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALLS TO ATLAS FUNCTIONS FOR SINGLE  |
    //                   | PRECISION FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Single-precision Generalized Matrix Multiplication algorithm. This algorithm calculates the matrix multiplication C = alpha * op(\b A ) * op(\b B ) + beta * C, where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] alpha is the factor which scales the matrix multiplication.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[in] beta is the factor which scales the result matrix \b R before adding the multiplication result.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixGEMM(float alpha, const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, float beta, Matrix<float> &R);
    /** Single-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = alpha * op(\b A ) * op(\b B ), where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] alpha is the factor which scales the matrix multiplication.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(float alpha, const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, Matrix<float> &R);
    /** Single-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = op(\b A ) * op(\b B ), where op(\b A )
     *  and op(\b B ) is op(\b X ) =\b X or op(\b X ) =\b X**T,
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] transposeA a boolean which indicates if the matrix\b A must be transposed.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] transposeB a boolean which indicates if the matrix\b B must be transposed.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(const Matrix<float> &A, bool transposeA, const Matrix<float> &B, bool transposeB, Matrix<float> &R);
    /** Single-precision matrix multiplication algorithm. This algorithm calculates the matrix multiplication C =\b A *\b B.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixMultiplication(const Matrix<float> &A, const Matrix<float> &B, Matrix<float> &R);
    /** Single-precision partial matrix multiplication algorithm. This algorithm calculates the matrix multiplication C = [\b A *\b B(:, 1:K), zeros(:, K + 1:N)], where
     *  \b K is the number of columns of\b B which are used to calculate the matrix multiplication and\b N is the number of columns of\b C. Therefore, the resulting matrix\b C has
     *  in the first\b K columns the result of the multiplication of\b A *\b B and the rest are set to zero.
     *  \param[in] A a constant reference to the left matrix of the multiplication.
     *  \param[in] B a constant reference to the right matrix of the multiplication.
     *  \param[in] final_column the final column of the matrix B which is used in the multiplication.
     *  \param[out] R a reference to the matrix where the result multiplication is stored.
     */
    void MatrixPartialMultiplication(const Matrix<float> &A, const Matrix<float> &B, int final_column, Matrix<float> &R);
    /** Single-precision matrix QR factorization of the matrix \b Q. The functions calculates the\b Q matrix in place.
     *  \param[in,out] Q a reference to the original rectangular matrix which the functions is going to transform to the \b Q matrix.
     *  \param[in] R a reference to the\b R triangular matrix.
     *  \param[in] number_of_threads number of threads used to calculate the\b QR decomposition.
     */
    void MatrixQRDecomposition(Matrix<float> &Q, Matrix<float> &R, unsigned int number_of_threads = 1);
    /** Single-precision singular value decomposition algorithm which decomposes A = U * D * V^T
     *  \param[in,out] A matrix to be decomposed (the matrix can be modified).
     *  \param[out] U resulting U matrix.
     *  \param[out] s vector storing the trace of the D matrix.
     *  \param[out] V resulting V matrix. This matrix is transposed.
     *  \param[in] economic calculates the economic SVD.
     */
    void MatrixSVD(Matrix<float> &A, Matrix<float> &U, VectorDense<float> &s, Matrix<float> &V, bool economic = false);
    /////// /** Single-precision Reduced Singular Value Decomposition which uses the QR decomposition to reduce the dimensionality of the A matrix and efficiently calculate the SVD matrix decomposition.
    ///////  *  \param[in,out] A matrix to be decomposed (the matrix can be modified).
    ///////  *  \param[out] U resulting U matrix.
    ///////  *  \param[out] s vector storing the trace of the D matrix.
    ///////  *  \param[out] V resulting V matrix.
    ///////  *  \param[in] number_of_threads number of threads used to calculate the QR decomposition.
    ///////  */
    /////// void MatrixRSVD(Matrix<float> &A, Matrix<float> &U, VectorDense<float> &s, Matrix<float> &V, unsigned int number_of_threads = 1);
    /** Single-precision matrix inversion function.
     *  \param[in] original original matrix.
     *  \param[out] inverse resulting inverted matrix.
     */
    void MatrixInverse(const Matrix<float> &original, Matrix<float> &inverse);
    /** Single-precision in-place matrix inversion function.
     *  \param[in,out] inverse matrix to be inverted.
     */
    void MatrixInverse(Matrix<float> &inverse);
    /** Single-precision function which computes the solution to a real system of linear equations
     *  A * X = B, where A is an N-by-N matrix and X and B are N-byNRHS matrices. The LU
     *  decomposition with partial pivoting and row interchanges is used to factor A as
     *      A = P * L * U,
     *  where P is a permutation matrix, L is a unit lower triangular, and U is upper triangular.
     *  The factored for of A is used then to solve the system of equations A * X = B;
     *  
     *  \param[in] A The N-by-N coefficient matrix A.
     *  \param[in] B The N-by-NRHS matrix of right hand side matrix B.
     *  \param[out] solution The N-by-NRHS solution matrix X.
     *  \throw ExceptionSingularity when the matrix is singular.
     *  \throw ExceptionArguments when the parameters are incorrect.
     */
    void MatrixSolve(const Matrix<float> &A, const Matrix<float> &B, Matrix<float> &solution);
    /** Single-precision function which computes the solution to a real system of linear equations
     *  A * X = B, where A is an N-by-N matrix and X and B are N-byNRHS matrices. The LU
     *  decomposition with partial pivoting and row interchanges is used to factor A as
     *      A = P * L * U,
     *  where P is a permutation matrix, L is a unit lower triangular, and U is upper triangular.
     *  The factored for of A is used then to solve the system of equations A * X = B;
     *  
     *  \param[in,out] A The N-by-N coefficient matrix A.
     *  \param[in,out] B The N-by-NRHS matrix of right hand side matrix B.
     *  \param[out] IPIV The N pivot indices that define the permutation matrix P; Row i of the matrix was interchanged with row IPIV(i)
     *  \throw ExceptionSingularity when the matrix is singular.
     *  \throw ExceptionArguments when the parameters are incorrect.
     */
    void MatrixSolve(Matrix<float> &A, Matrix<float> &B, VectorDense<int> &IPIV);
    /** Single-precision function which calculates the eigenvalues and eigenvectors of a symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenSymmetric(const Matrix<float> &A, Matrix<float> &V, VectorDense<float> &D);
    /** Single-precision function which calculates the eigenvalues of a symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenSymmetric(const Matrix<float> &A, VectorDense<float> &D);
    /** Single-precision function which calculates the eigenvalues and eigenvectors of a non-symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenNonSymmetric(const Matrix<float> &A, bool no_balance, Matrix<float> &V, VectorDense<float> &D);
    /** Single-precision function which calculates the eigenvalues of a non-symmetric matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    void MatrixEigenNonSymmetric(const Matrix<float> &A, bool no_balance, VectorDense<float> &D);
    /** Single-precision function which returns the eigenvalues and eigenvectors of the given matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] V Matrix storing the eigenvectors of A.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    inline void MatrixEigen(const Matrix<float> &A, bool no_balance, Matrix<float> &V, VectorDense<float> &D)
    {
        if (MatrixIsSymmetric(A)) MatrixEigenSymmetric(A, V, D);
        else MatrixEigenNonSymmetric(A, no_balance, V, D);
    }
    /** Single-precision function which returns the eigenvalues of the given matrix.
     *  \param[in] A Original matrix.
     *  \param[in] no_balance finds the eigenvalues without a preliminary balancing step.
     *  \param[out] D Vector storing the eigenvalues of A.
     */
    inline void MatrixEigen(const Matrix<float> &A, bool no_balance, VectorDense<float> &D)
    {
        if (MatrixIsSymmetric(A)) MatrixEigenSymmetric(A, D);
        else MatrixEigenNonSymmetric(A, no_balance, D);
    }
    /** Single-precision function which calculates the logarithm of the given matrix.
     *  \param[in] A source matrix.
     *  \param[out] B resulting matrix.
     *  \param[in] number_of_threads number of threads used to calculate the matrix logarithm.
     */
    void MatrixLogarithm(const Matrix<float> &A, Matrix<float> &B, unsigned int number_of_threads = 1);
    /** Single-precision function which calculates the exponential of the given matrix.
     *  \param[in] A source matrix.
     *  \param[out] B resulting matrix.
     *  \param[in] number_of_threads number of threads used to calculate the matrix exponential.
     */
    void MatrixExponential(const Matrix<float> &A, Matrix<float> &B, unsigned int number_of_threads = 1);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | OTHER MATRIX OPERATORS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Returns the trace of the matrix.
    template <class T>
    inline T MatrixTrace(const Matrix<T> &matrix)
    {
        T trace = 0;
        for (int i = 0; i < srvMin<int>(matrix.getNumberOfRows(), matrix.getNumberOfColumns()); ++i)
            trace += matrix(i, i);
        return trace;
    }
    
    /// Returns the transpose of a matrix.
    template <class T>
    inline void MatrixTranspose(const Matrix<T> &original_matrix, Matrix<T> &transposed_matrix)
    {
        const T * __restrict__ original_ptr;
        if ((transposed_matrix.getNumberOfRows() != original_matrix.getNumberOfColumns()) || (transposed_matrix.getNumberOfColumns() != original_matrix.getNumberOfRows()))
            transposed_matrix.set(original_matrix.getNumberOfColumns(), original_matrix.getNumberOfRows());
        
        original_ptr = original_matrix();
        for (int column = 0; column < original_matrix.getNumberOfColumns(); ++column)
            for (int row = 0; row < original_matrix.getNumberOfRows(); ++row, ++original_ptr)
                transposed_matrix(column, row) = *original_ptr; //original_matrix(row, column);
    }
    
    template <class T>
    inline void MatrixJoinColumns(const Matrix<T> &first_matrix, const Matrix<T> &second_matrix, Matrix<T> &combined_matrix)
    {
        const T *first_ptr, *second_ptr;
        T *combined_ptr;
        if (first_matrix.getNumberOfRows() != second_matrix.getNumberOfRows()) throw Exception("The two matrices have a different number of rows. They cannot be combined.");
        
        combined_matrix.set(first_matrix.getNumberOfRows(), first_matrix.getNumberOfColumns() + second_matrix.getNumberOfColumns());
        first_ptr = first_matrix();
        second_ptr = second_matrix();
        combined_ptr = combined_matrix();
        for (int i = 0; i < first_matrix.getNumberOfColumns(); ++i)
            for (int k = 0; k < first_matrix.getNumberOfRows(); ++k, ++combined_ptr, ++first_ptr)
                *combined_ptr = *first_ptr;
        for (int i = 0; i < second_matrix.getNumberOfColumns(); ++i)
            for (int k = 0; k < second_matrix.getNumberOfRows(); ++k, ++combined_ptr, ++second_ptr)
                *combined_ptr = *second_ptr;
    }
    
    template <class T>
    inline void MatrixJoinRows(const Matrix<T> &first_matrix, const Matrix<T> &second_matrix, Matrix<T> &combined_matrix)
    {
        const T *first_ptr, *second_ptr;
        T *combined_ptr;
        if (first_matrix.getNumberOfColumns() != second_matrix.getNumberOfColumns()) throw Exception("The two matrices have a different number of rows. They cannot be combined.");
        
        combined_matrix.set(first_matrix.getNumberOfRows() + second_matrix.getNumberOfRows(), first_matrix.getNumberOfColumns());
        first_ptr = first_matrix();
        second_ptr = second_matrix();
        combined_ptr = combined_matrix();
        
        for (int i = 0; i < first_matrix.getNumberOfColumns(); ++i)
        {
            for (int k = 0; k < first_matrix.getNumberOfRows(); ++k, ++combined_ptr, ++first_ptr)
                *combined_ptr = *first_ptr;
            for (int k = 0; k < second_matrix.getNumberOfRows(); ++k, ++combined_ptr, ++second_ptr)
                *combined_ptr = *second_ptr;
        }
    }
    
    template <class T, template <class, class> class V1, class T1, class N1, template <class, class> class V2, class T2, class N2>
    inline void MatrixVectorMultiplication(const Matrix<T> &matrix, const V1<T1, N1> &vec, V2<T2, N2> &destination)
    {
        for (int row = 0; row < matrix.getNumberOfRows(); ++row)
        {
            T value = 0;
            for (int column = 0; column < matrix.getNumberOfColumns(); ++column)
                value += matrix(row, column) * (T)vec[column];
            destination[row] = (T2)value;
        }
    }
    template <class T, template <class, class> class V1, class T1, class N1, template <class, class> class V2, class T2, class N2>
    inline void MatrixSparseVectorMultiplication(const Matrix<T> &matrix, const V1<T1, N1> &vec, V2<T2, N2> &destination)
    {
        for (int row = 0; row < matrix.getNumberOfRows(); ++row)
        {
            T value = 0;
            for (int column = 0; column < (int)vec.size(); ++column)
                value += matrix(row, vec.getIndex(column)) * (T)vec.getValue(column);
            destination[row] = (T2)value;
        }
    }
    template<class T>
    inline void MatrixVectorMultiplication(const Matrix<T> &matrix, const T *vec, T *destination)
    {
        for (int row = 0; row < matrix.getNumberOfRows(); ++row)
        {
            destination[row] = 0;
            for (int column = 0; column < matrix.getNumberOfColumns(); ++column)
                destination[row] += matrix(row, column) * vec[column];
        }
    }
    
    /// Calculates the determinant of 2x2 matrix
    template <class T>
    inline T determinant2x2Matrix(const Matrix<T> &matrix) {return matrix(0, 0) * matrix(1, 1) - matrix(0, 1) * matrix(1, 0);}
    
    /// Calculates the determinant of 3x3 matrix
    template <class T>
    inline T determinant3x3Matrix(const Matrix<T> &matrix)
    {
        T pos, neg;
        pos = matrix(0, 0) * matrix(1, 1) * matrix(2, 2);
        pos += matrix(0, 1) * matrix(1, 2) * matrix(2, 0);
        pos += matrix(0, 2) * matrix(1, 0) * matrix(2, 1);
        neg = matrix(0, 2) * matrix(1, 1) * matrix(2, 0);
        neg += matrix(0, 1) * matrix(1, 0) * matrix(2, 2);
        neg += matrix(0, 0) * matrix(1, 2) * matrix(2, 1);
        return pos - neg;
    }
    
    template <class T>
    inline void MatrixDiagonal(const VectorDense<T> &vect, Matrix<T> &matrix)
    {
        matrix.set(vect.size(), vect.size(), 0);
        for (unsigned int i = 0; i < vect.size(); ++i) matrix(i, i) = vect[i];
    }
    
    template <class T>
    inline void MatrixDiagonal(const Matrix<T> &matrix, VectorDense<T> &vect)
    {
        vect.resize(srvMin<int>(matrix.getNumberOfColumns(), matrix.getNumberOfRows()));
        for (int i = 0; i < srvMin<int>(matrix.getNumberOfColumns(), matrix.getNumberOfRows()); ++i)
            vect[i] = matrix(i, i);
    }
    
    template <class T>
    inline void MatrixLMS(const Matrix<T> &X, const Matrix<T> &y, Matrix<T> &solution)
    {
        Matrix<T> pseudo_inverse, aux;
        if (X.getNumberOfRows() != y.getNumberOfRows())
            throw Exception("LMS Error: both matrices has a different amount of rows (different amount of samples)");
        
        MatrixMultiplication(X, true, X, false, pseudo_inverse);
        MatrixInverse(pseudo_inverse);
        MatrixMultiplication(pseudo_inverse, false, X, true, aux);
        MatrixMultiplication(aux, y, solution);
    }
    
    /// Checks if the given matrix is a symmetric matrix.
    template <class T>
    inline bool MatrixIsSymmetric(const Matrix<T> &X)
    {
        if (X.getNumberOfRows() != X.getNumberOfColumns())
            throw ExceptionArgument("A symmetric matrix must be square.");
        const T eps = (T)10 * (T)X.getNumberOfRows() * (T)X.getNumberOfColumns() * (T)1.1102230246251565e-16;
        for (int row = 0; row < X.getNumberOfRows(); ++row)
        {
            for (int col = row + 1; col < X.getNumberOfColumns(); ++col)
            {
                T mij = X(row, col);
                T mji = X(col, row);
                if (srvAbs<T>(mij - mji) > (srvMax<T>(srvAbs<T>(mij), srvAbs<T>(mji)) * eps)) return false;
            }
        }
        return true;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

