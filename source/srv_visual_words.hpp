// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_MAIN_VISUAL_WORDS_HEADER_FILE__
#define __SRV_MAIN_VISUAL_WORDS_HEADER_FILE__

#include "srv_descriptors.hpp"
#include "srv_codebook.hpp"
#include "visual_words/srv_dense_visual_words.hpp"
#include "visual_words/srv_visual_words_histogram.hpp"
#include "srv_file_compression.hpp"

#define INITIALIZE_DENSE_VISUAL_WORDS(TFILTER, TFEATURE, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    INITIALIZE_CODEBOOK_FACTORIES(TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    INITIALIZE_DENSE_DESCRIPTOR(TFILTER, TFEATURE)

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS FOR THE DENSE    |
    //                   | VISUAL WORDS                         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Shows the information of the dense visual words object in an standard output stream.
     *  \param[out] out standard library output stream.
     *  \param[in] object dense visual words object.
     *  \returns the input standard library output stream.
     */
    template <class TFILTER, class TFEATURE, class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    std::ostream& operator<<(std::ostream &out, const DenseVisualWords<TFILTER, TFEATURE, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> &object)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Dense visual words ]" << std::endl;
        tab.increase(out);
        out << tab << " · Descriptor spatial normal threshold: " << object.getDescriptorThreshold() << std::endl;
        out << object.getDenseDescriptors();
        out << object.getCodebook();
        tab.decrease(out);
        
        return out;
    }
    
    /** Loads the dense visual words object from an XML file.
     *  \param[in] filename file where the dense visual words object is stored.
     *  \param[out] object dense visual words object.
     */
    template <class TFILTER, class TFEATURE, class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    void loadObject(const char * filename, DenseVisualWords<TFILTER, TFEATURE, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object.convertFromXML(parser);
        delete file;
    }
    
    /** Saves the dense visual words object into an XML file.
     *  \param[in] filename file where the dense visual words is going to be stored.
     *  \param[in] object dense visual words object.
     */
    template <class TFILTER, class TFEATURE, class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    void saveObject(const char * filename, const DenseVisualWords<TFILTER, TFEATURE, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> &object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object.convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    /** Generates a vector with the size of the visual words regions at each scale.
     *  \param[in] object dense visual words object.
     *  \param[out] region_sizes output vector with the size of the regions at each scale.
     */
    template <class TFILTER, class TFEATURE, class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    inline void regionSizes(const DenseVisualWords<TFILTER, TFEATURE, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> &object, VectorDense<unsigned int> &region_sizes)
    {
        regionSizes(object.getDenseDescriptors(), region_sizes);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DECLARATION OF THE AUXILIARY         |
    //                   | FUNCTIONS FOR THE HISTOGRAM OF       |
    //                   | VISUAL WORDS PARAMETERS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Shows the information of the histogram of visual words parameters in an standard output stream.
     *  \param[out] out standard library output stream.
     *  \param[in] object histogram of visual words parameters object.
     *  \returns the input standard library output stream.
     */
    std::ostream& operator<<(std::ostream &out, const VisualWordsHistogramParameters &object);
    
    /** Loads the histogram of visual words parameters object from an XML file.
     *  \param[in] filename file where the parameters of the visual words histogram object is stored.
     *  \param[out] object visual words histogram parameters object.
     *  \note this function does not deallocates the object pointed by the object pointer.
     */
    void loadObject(const char * filename, VisualWordsHistogramParameters &object);
    
    /** Saves the histogram of visual words parameters object into an XML file.
     *  \param[in] filename file where the histogram of visual words parameters is going to be stored.
     *  \param[in] object histogram of visual words parameters object.
     */
    void saveObject(const char * filename, const VisualWordsHistogramParameters &object);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif
