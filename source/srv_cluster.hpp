// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CLUSTER_METHODS_MAIN_ROOT_HPP_HEADER_FILE__
#define __SRV_CLUSTER_METHODS_MAIN_ROOT_HPP_HEADER_FILE__

#include "srv_vector.hpp"
#include "cluster/srv_seeder_base.hpp"
#include "cluster/srv_seeder_random.hpp"
#include "cluster/srv_seeder_plus.hpp"
#include "cluster/srv_cluster_base.hpp"
#include "cluster/srv_cluster_center_based_base.hpp"
#include "cluster/srv_cluster_kmeans.hpp"
#include "cluster/srv_cluster_kmedians.hpp"
#include "cluster/srv_cluster_kmeans_fast.hpp"
#include "cluster/srv_cluster_kmedians_fast.hpp"
#include <iostream>
#include "srv_file_compression.hpp"

//                   +--------------------------------------+
//                   | CLUSTER AND SEEDER MACROS            |
//                   +--------------------------------------+
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                ##############
//                                  ##########
//                                    ######
//                                      ##

#define ADD_SEEDER_FACTORY(CLASS_NAME, T, TMODE, TDISTANCE, N) \
    ADD_FACTORY(TEMPLATE_4P(SeederFactory, T, TMODE, TDISTANCE, N), TEMPLATE_4P(CLASS_NAME, T, TMODE, TDISTANCE, N))

#define INITIALIZE_SEEDER_FACTORIES(T, TMODE, TDISTANCE, N) \
    ADD_SEEDER_FACTORY(srv::SeederRandom, T, TMODE, TDISTANCE, N) \
    ADD_SEEDER_FACTORY(srv::SeederPlus, T, TMODE, TDISTANCE, N)

//                                      ##
//                                    ######
//                                  ##########
//                                ##############
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                   +--------------------------------------+
//                   | CLUSTER MACROS                       |
//                   +--------------------------------------+
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                ##############
//                                  ##########
//                                    ######
//                                      ##

#define ADD_CLUSTER_FACTORY(CLASS_NAME, T, TMODE, TDISTANCE, N) \
    ADD_FACTORY(TEMPLATE_4P(ClusterFactory, T, TMODE, TDISTANCE, N), TEMPLATE_4P(CLASS_NAME, T, TMODE, TDISTANCE, N))

#define INITIALIZE_CLUSTER_FACTORIES(T, TMODE, TDISTANCE, N) \
    ADD_CLUSTER_FACTORY(srv::ClusterKMeans, T, TMODE, TDISTANCE, N) \
    ADD_CLUSTER_FACTORY(srv::ClusterKMedians, T, TMODE, TDISTANCE, N) \
    ADD_CLUSTER_FACTORY(srv::ClusterKMeansFast, T, TMODE, TDISTANCE, N) \
    ADD_CLUSTER_FACTORY(srv::ClusterKMediansFast, T, TMODE, TDISTANCE, N) \
    INITIALIZE_SEEDER_FACTORIES(T, TMODE, TDISTANCE, N)

namespace srv
{
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SEEDER AUXILIARY FUNCTIONS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Shows the information of a seeder object.
     *  \param[out] out standard library output stream.
     *  \param[in] object seeder object.
     *  \returns the input standard library output stream.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    std::ostream& operator<<(std::ostream &out, const SeederBase<T, TMODE, TDISTANCE, N> * object)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Seeder ]" << std::endl;
        tab.increase(out);
        out << tab << " · Type: ";
        if (object->getIdentifier() == SEEDER_UNIFORM_RANDOM_SELECTION) out << "Random";
        else if (object->getIdentifier() == SEEDER_KMEANS_PLUS_PLUS) out << "k-Means++";
        else out << "[UNKNOW OR NOT YET IMPLEMENTED]";
        out << " seeder." << std::endl;
        
        if (object->getIdentifier() == SEEDER_UNIFORM_RANDOM_SELECTION)
        {
            const SeederRandom<T, TMODE, TDISTANCE, N> * object_ptr = (SeederRandom<T, TMODE, TDISTANCE, N> *)object;
            
            out << tab << " · Ratio: " << object_ptr->getRatio() << std::endl;
            out << tab << " · Minimum distance: " << object_ptr->getMinimumDistance() << std::endl;
            out << object_ptr->getDistanceObject();
        }
        else if (object->getIdentifier() == SEEDER_KMEANS_PLUS_PLUS)
        {
            const SeederPlus<T, TMODE, TDISTANCE, N> * object_ptr = (SeederPlus<T, TMODE, TDISTANCE, N> *)object;
            
            out << tab << " · Ratio: " << object_ptr->getRatio() << std::endl;
            out << tab << " · Normal distribution: " << ((object_ptr->getNormalDistribution())?("[TRUE]"):("[FALSE]")) << std::endl;
            out << object_ptr->getDistanceObject();
        }
        tab.decrease(out);
        
        return out;
    }
    
    /** Loads the seeder object from an XML file.
     *  \param[in] filename file where the seeder object is stored.
     *  \param[out] object seeder object.
     *  \note this function does not deallocates the object pointed by the object pointer.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    void loadObject(const char * filename, SeederBase<T, TMODE, TDISTANCE, N> * &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object = SeederFactory<T, TMODE, TDISTANCE, N>::Type::getInstantiator((SEEDER_IDENTIFIER)((int)parser.getAttribute("Identifier")));
        object->convertFromXML(parser);
        delete file;
    }
    
    /** Saves the seeder object into an XML file.
     *  \param[in] filename file where the vector seeder is going to be stored.
     *  \param[in] object seeder object.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    void saveObject(const char * filename, const SeederBase<T, TMODE, TDISTANCE, N> * object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CLUSTER AUXILIARY FUNCTIONS          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Shows the information of a cluster object.
     *  \param[out] out standard library output stream.
     *  \param[in] object cluster object.
     *  \returns the input standard library output stream.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    std::ostream& operator<<(std::ostream &out, const ClusterBase<T, TMODE, TDISTANCE, N> * object)
    {
        const CLUSTER_IDENTIFIER identifier = object->getIdentifier();
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Cluster ]" << std::endl;
        tab.increase(out);
        out << tab << " · Number of clusters: " << object->getNumberOfClusters() << std::endl;
        out << tab << " · Number of dimensions: " << object->getNumberOfDimensions() << std::endl;
        out << tab << " · Cluster trace: " << (object->isClusterInformationTrace()?("[ENABLED]"):("[DISABLED]")) << std::endl;
        if ((identifier == KMEANS_CLUSTER) || (identifier == KMEDIANS_CLUSTER) || (identifier == KMEANS_FAST_CLUSTER) || (identifier == KMEDIANS_FAST_CLUSTER))
        {
            const ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> * object_ptr = (ClusterCenterBasedBase<T, TMODE, TDISTANCE, N> *)object;
            
            out << tab << " · Cluster algorithm: ";
            if      (identifier == KMEANS_CLUSTER       ) out << "standard k-Means";
            else if (identifier == KMEDIANS_CLUSTER     ) out << "standard k-Medians";
            else if (identifier == KMEANS_FAST_CLUSTER  ) out << "triangle inequality k-Means";
            else if (identifier == KMEDIANS_FAST_CLUSTER) out << "triangle inequality k-Medians";
            out << " algorithm." << std::endl;
            out << object_ptr->getSeeder();
            out << object_ptr->getDistanceObject();
            out << tab << " · Cutoff: " << object_ptr->getCutoff() << std::endl;
            out << tab << " · Empty cluster method: ";
            switch (object_ptr->getClusterEmptyMethod())
            {
            case CLUSTER_EMPTY_EXCEPTION:
                out << "Generate an exception.";
                break;
            case CLUSTER_EMPTY_IGNORE:
                out << "Ignore the empty clusters.";
                break;
            case CLUSTER_EMPTY_RESAMPLE  :
                out << "Re-sample empty cluster.";
                break;
            default:
                out << "[UNKNOWN]";
            }
            out << std::endl;
            out << tab << " · Maximum number of iterations: " << object_ptr->getMaximumNumberOfIterations() << std::endl;
            if (identifier == KMEANS_FAST_CLUSTER)
            {
                const ClusterKMeansFast<T, TMODE, TDISTANCE, N> * current_ptr = (ClusterKMeansFast<T, TMODE, TDISTANCE, N> *)object;
                
                out << tab << " · Memory method: ";
                if (current_ptr->getSaveMemory() == 0) out << " full standard algorithm." << std::endl;
                else if (current_ptr->getSaveMemory() == 1)
                {
                    out << " memory restricted algorithm." << std::endl;
                    out << tab << " · Number of nearest clusters: " << current_ptr->getNumberOfClusterNeighbors() << std::endl;
                }
            }
            else if (identifier == KMEDIANS_FAST_CLUSTER)
            {
                const ClusterKMediansFast<T, TMODE, TDISTANCE, N> * current_ptr = (ClusterKMediansFast<T, TMODE, TDISTANCE, N> *)object;
                
                out << tab << " · Memory method: ";
                if (current_ptr->getSaveMemory() == 0) out << " full standard algorithm." << std::endl;
                else if (current_ptr->getSaveMemory() == 1)
                {
                    out << " memory restricted algorithm." << std::endl;
                    out << tab << " · Number of nearest clusters: " << current_ptr->getNumberOfClusterNeighbors() << std::endl;
                }
            }
        }
        else out << tab << " · Cluster algorithm: [UNKNOWN]" << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    /** Loads the cluster object from an XML file.
     *  \param[in] filename file where the cluster object is stored.
     *  \param[out] object cluster object.
     *  \note this function does not deallocates the object pointed by the object pointer.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    void loadObject(const char * filename, ClusterBase<T, TMODE, TDISTANCE, N> * &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object = ClusterFactory<T, TMODE, TDISTANCE, N>::Type::getInstantiator((CLUSTER_IDENTIFIER)((int)parser.getAttribute("Identifier")));
        object->convertFromXML(parser);
        delete file;
    }
    
    /** Saves the cluster object into an XML file.
     *  \param[in] filename file where the vector cluster is going to be stored.
     *  \param[in] object cluster object.
     */
    template <class T, class TMODE, class TDISTANCE, class N>
    void saveObject(const char * filename, const ClusterBase<T, TMODE, TDISTANCE, N> * object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

