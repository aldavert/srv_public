// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_visual_words_histogram.hpp"

#include <set>

namespace srv
{
    //                   +--------------------------------------+
    //                   | VISUAL WORDS HISTOGRAM INFORMATION   |
    //                   | CLASS - IMPLEMENTATION               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS DESTRUCTOR AND ASSIGNATION OPERATORS ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(void) :
        m_number_of_spatial_bins(1),
        m_number_of_visual_words(0),
        m_number_of_words_per_descriptor(0),
        m_number_of_bins(0),
        m_number_of_bins_spatial(0),
        m_number_of_pyramid_levels(1),
        m_initial_degree_x(1),
        m_initial_degree_y(1),
        m_growth_x(1),
        m_growth_y(1),
        m_level_spatial_bins(new unsigned int[1]),
        m_level_partitions_x(new unsigned int[1]),
        m_level_partitions_y(new unsigned int[1]),
        m_pooling_method(BOVW_POOLING_SUM),
        m_use_polarity(false),
        m_use_interpolation(false),
        m_use_unreliable(false),
        m_use_separated_scales(false),
        m_number_of_scales(0),
        m_scales_margin(false),
        m_use_spatial_normalization(false),
        m_spatial_area(false),
        m_spatial_normalization(MANHATTAN_NORM),
        m_normalization(EUCLIDEAN_NORM),
        m_power_factor(1.0),
        m_cutoff_value(1.0)
    {
        m_level_spatial_bins[0] = 1;
        m_level_partitions_x[0] = 1;
        m_level_partitions_y[0] = 1;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor) :
        m_number_of_spatial_bins(1),
        m_number_of_visual_words(number_of_visual_words),
        m_number_of_words_per_descriptor(number_of_words_per_descriptor),
        m_number_of_bins(number_of_visual_words),
        m_number_of_bins_spatial(number_of_visual_words),
        m_number_of_pyramid_levels(1),
        m_initial_degree_x(1),
        m_initial_degree_y(1),
        m_growth_x(1),
        m_growth_y(1),
        m_level_spatial_bins(new unsigned int[1]),
        m_level_partitions_x(new unsigned int[1]),
        m_level_partitions_y(new unsigned int[1]),
        m_pooling_method(BOVW_POOLING_SUM),
        m_use_polarity(false),
        m_use_interpolation(false),
        m_use_unreliable(false),
        m_use_separated_scales(false),
        m_number_of_scales(0),
        m_scales_margin(false),
        m_use_spatial_normalization(false),
        m_spatial_area(false),
        m_spatial_normalization(MANHATTAN_NORM),
        m_normalization(EUCLIDEAN_NORM),
        m_power_factor(1.0),
        m_cutoff_value(1.0)
    {
        m_level_spatial_bins[0] = 1;
        m_level_partitions_x[0] = 1;
        m_level_partitions_y[0] = 1;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor) :
        m_number_of_spatial_bins(0),
        m_number_of_visual_words(number_of_visual_words),
        m_number_of_words_per_descriptor(number_of_words_per_descriptor),
        m_number_of_bins(0),
        m_number_of_bins_spatial(number_of_visual_words),
        m_number_of_pyramid_levels(number_of_pyramid_levels),
        m_initial_degree_x(initial_x),
        m_initial_degree_y(initial_y),
        m_growth_x(growth_x),
        m_growth_y(growth_y),
        m_level_spatial_bins((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_level_partitions_x((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_level_partitions_y((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_pooling_method(BOVW_POOLING_SUM),
        m_use_polarity(false),
        m_use_interpolation(false),
        m_use_unreliable(false),
        m_use_separated_scales(false),
        m_number_of_scales(0),
        m_scales_margin(false),
        m_use_spatial_normalization(true),
        m_spatial_area(false),
        m_spatial_normalization(MANHATTAN_NORM),
        m_normalization(EUCLIDEAN_NORM),
        m_power_factor(1.0),
        m_cutoff_value(1.0)
    {
        for (unsigned int level = 0, bins_x = initial_x, bins_y = initial_y; level < number_of_pyramid_levels; ++level, bins_x *= growth_x, bins_y *= growth_y)
        {
            m_number_of_spatial_bins += bins_x * bins_y;
            m_level_spatial_bins[level] = bins_x * bins_y;
            m_level_partitions_x[level] = bins_x;
            m_level_partitions_y[level] = bins_y;
        }
        m_number_of_bins = m_number_of_spatial_bins * number_of_visual_words;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor, BOVW_POOLING_OPERATOR pooling_method, bool polarity, bool interpolation, bool unreliable, bool use_separated_scales, unsigned int number_of_scales, bool scales_margin, bool use_spatial_norm, bool use_spatial_area, const VectorNorm &spatial_norm, const VectorNorm &global_norm, double power_factor, double cutoff) :
        m_number_of_spatial_bins(0),
        m_number_of_visual_words(number_of_visual_words),
        m_number_of_words_per_descriptor(number_of_words_per_descriptor),
        m_number_of_bins(0),
        m_number_of_bins_spatial(0),
        m_number_of_pyramid_levels(number_of_pyramid_levels),
        m_initial_degree_x(initial_x),
        m_initial_degree_y(initial_y),
        m_growth_x(growth_x),
        m_growth_y(growth_y),
        m_level_spatial_bins((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_level_partitions_x((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_level_partitions_y((number_of_pyramid_levels > 0)?new unsigned int[number_of_pyramid_levels]:0),
        m_pooling_method(pooling_method),
        m_use_polarity(polarity),
        m_use_interpolation(interpolation),
        m_use_unreliable(unreliable),
        m_use_separated_scales(use_separated_scales),
        m_number_of_scales(number_of_scales),
        m_scales_margin(scales_margin),
        m_use_spatial_normalization(use_spatial_norm),
        m_spatial_area(use_spatial_area),
        m_spatial_normalization(spatial_norm),
        m_normalization(global_norm),
        m_power_factor(power_factor),
        m_cutoff_value(cutoff)
    {
        for (unsigned int level = 0, bins_x = initial_x, bins_y = initial_y; level < number_of_pyramid_levels; ++level, bins_x *= growth_x, bins_y *= growth_y)
        {
            m_number_of_spatial_bins += bins_x * bins_y;
            m_level_spatial_bins[level] = bins_x * bins_y;
            m_level_partitions_x[level] = bins_x;
            m_level_partitions_y[level] = bins_y;
        }
        
        if (unreliable) m_number_of_bins = m_number_of_spatial_bins * (number_of_visual_words + number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * number_of_visual_words;
        if (polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor) :
        m_number_of_spatial_bins(0),
        m_number_of_visual_words(number_of_visual_words),
        m_number_of_words_per_descriptor(number_of_words_per_descriptor),
        m_number_of_bins(0),
        m_number_of_bins_spatial(number_of_visual_words),
        m_number_of_pyramid_levels(pyramid_information.size()),
        m_initial_degree_x(0),
        m_initial_degree_y(0),
        m_growth_x(1),
        m_growth_y(1),
        m_level_spatial_bins((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_level_partitions_x((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_level_partitions_y((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_pooling_method(BOVW_POOLING_SUM),
        m_use_polarity(false),
        m_use_interpolation(false),
        m_use_unreliable(false),
        m_use_separated_scales(false),
        m_number_of_scales(0),
        m_scales_margin(false),
        m_use_spatial_normalization(true),
        m_spatial_area(false),
        m_spatial_normalization(MANHATTAN_NORM),
        m_normalization(EUCLIDEAN_NORM),
        m_power_factor(1.0),
        m_cutoff_value(1.0)
    {
        if (pyramid_information.size() > 0)
        {
            m_initial_degree_x = pyramid_information[0].getFirst();
            m_initial_degree_y = pyramid_information[0].getSecond();
            for (unsigned int level = 0; level < pyramid_information.size(); ++level)
            {
                m_number_of_spatial_bins += pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_spatial_bins[level] = pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_partitions_x[level] = pyramid_information[level].getFirst();
                m_level_partitions_y[level] = pyramid_information[level].getSecond();
            }
        }
        m_number_of_bins = m_number_of_spatial_bins * number_of_visual_words;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor, BOVW_POOLING_OPERATOR pooling_method, bool polarity, bool interpolation, bool unreliable, bool use_separated_scales, unsigned int number_of_scales, bool scales_margin, bool use_spatial_norm, bool use_spatial_area, const VectorNorm &spatial_norm, const VectorNorm &global_norm, double power_factor, double cutoff) :
        m_number_of_spatial_bins(0),
        m_number_of_visual_words(number_of_visual_words),
        m_number_of_words_per_descriptor(number_of_words_per_descriptor),
        m_number_of_bins(0),
        m_number_of_bins_spatial(0),
        m_number_of_pyramid_levels(pyramid_information.size()),
        m_initial_degree_x(0),
        m_initial_degree_y(0),
        m_growth_x(1),
        m_growth_y(1),
        m_level_spatial_bins((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_level_partitions_x((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_level_partitions_y((pyramid_information.size() > 0)?new unsigned int[pyramid_information.size()]:0),
        m_pooling_method(pooling_method),
        m_use_polarity(polarity),
        m_use_interpolation(interpolation),
        m_use_unreliable(unreliable),
        m_use_separated_scales(use_separated_scales),
        m_number_of_scales(number_of_scales),
        m_scales_margin(scales_margin),
        m_use_spatial_normalization(use_spatial_norm),
        m_spatial_area(use_spatial_area),
        m_spatial_normalization(spatial_norm),
        m_normalization(global_norm),
        m_power_factor(power_factor),
        m_cutoff_value(cutoff)
    {
        if (pyramid_information.size() > 0)
        {
            m_initial_degree_x = pyramid_information[0].getFirst();
            m_initial_degree_y = pyramid_information[0].getSecond();
            for (unsigned int level = 0; level < pyramid_information.size(); ++level)
            {
                m_number_of_spatial_bins += pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_spatial_bins[level] = pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_partitions_x[level] = pyramid_information[level].getFirst();
                m_level_partitions_y[level] = pyramid_information[level].getSecond();
            }
        }
        
        if (unreliable) m_number_of_bins = m_number_of_spatial_bins * (number_of_visual_words + number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * number_of_visual_words;
        if (polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    VisualWordsHistogramParameters::VisualWordsHistogramParameters(const VisualWordsHistogramParameters &other) :
        m_number_of_spatial_bins(other.m_number_of_spatial_bins),
        m_number_of_visual_words(other.m_number_of_visual_words),
        m_number_of_words_per_descriptor(other.m_number_of_words_per_descriptor),
        m_number_of_bins(other.m_number_of_bins),
        m_number_of_bins_spatial(other.m_number_of_bins_spatial),
        m_number_of_pyramid_levels(other.m_number_of_pyramid_levels),
        m_initial_degree_x(other.m_initial_degree_x),
        m_initial_degree_y(other.m_initial_degree_y),
        m_growth_x(other.m_growth_x),
        m_growth_y(other.m_growth_y),
        m_level_spatial_bins((other.m_level_spatial_bins != 0)?new unsigned int[other.m_number_of_pyramid_levels]:0),
        m_level_partitions_x((other.m_level_partitions_x != 0)?new unsigned int[other.m_number_of_pyramid_levels]:0),
        m_level_partitions_y((other.m_level_partitions_y != 0)?new unsigned int[other.m_number_of_pyramid_levels]:0),
        m_pooling_method(other.m_pooling_method),
        m_use_polarity(other.m_use_polarity),
        m_use_interpolation(other.m_use_interpolation),
        m_use_unreliable(other.m_use_unreliable),
        m_use_separated_scales(other.m_use_separated_scales),
        m_number_of_scales(other.m_number_of_scales),
        m_scales_margin(other.m_scales_margin),
        m_use_spatial_normalization(other.m_use_spatial_normalization),
        m_spatial_normalization(other.m_spatial_normalization),
        m_normalization(other.m_normalization),
        m_power_factor(other.m_power_factor),
        m_cutoff_value(other.m_cutoff_value)
    {
        for (unsigned int i = 0; i < other.m_number_of_pyramid_levels; ++i)
        {
            m_level_spatial_bins[i] = other.m_level_spatial_bins[i];
            m_level_partitions_x[i] = other.m_level_partitions_x[i];
            m_level_partitions_y[i] = other.m_level_partitions_y[i];
        }
    }
    
    VisualWordsHistogramParameters::~VisualWordsHistogramParameters(void)
    {
        if (m_level_spatial_bins != 0) delete [] m_level_spatial_bins;
        if (m_level_partitions_x != 0) delete [] m_level_partitions_x;
        if (m_level_partitions_y != 0) delete [] m_level_partitions_y;
    }
    
    VisualWordsHistogramParameters& VisualWordsHistogramParameters::operator=(const VisualWordsHistogramParameters &other)
    {
        if (&other != this)
        {
            if (m_level_spatial_bins != 0) delete [] m_level_spatial_bins;
            if (m_level_partitions_x != 0) delete [] m_level_partitions_x;
            if (m_level_partitions_y != 0) delete [] m_level_partitions_y;
            
            m_number_of_spatial_bins = other.m_number_of_spatial_bins;
            m_number_of_visual_words = other.m_number_of_visual_words;
            m_number_of_words_per_descriptor = other.m_number_of_words_per_descriptor;
            m_number_of_bins = other.m_number_of_bins;
            m_number_of_bins_spatial = other.m_number_of_bins_spatial;
            m_number_of_pyramid_levels = other.m_number_of_pyramid_levels;
            m_initial_degree_x = other.m_initial_degree_x;
            m_initial_degree_y = other.m_initial_degree_y;
            m_growth_x = other.m_growth_x;
            m_growth_y = other.m_growth_y;
            if (other.m_level_spatial_bins != 0)
            {
                m_level_spatial_bins = new unsigned int[other.m_number_of_pyramid_levels];
                m_level_partitions_x = new unsigned int[other.m_number_of_pyramid_levels];
                m_level_partitions_y = new unsigned int[other.m_number_of_pyramid_levels];
                for (unsigned int i = 0; i < other.m_number_of_pyramid_levels; ++i)
                {
                    m_level_spatial_bins[i] = other.m_level_spatial_bins[i];
                    m_level_partitions_x[i] = other.m_level_partitions_x[i];
                    m_level_partitions_y[i] = other.m_level_partitions_y[i];
                }
            }
            else
            {
                m_level_spatial_bins = 0;
                m_level_partitions_x = 0;
                m_level_partitions_y = 0;
            }
            m_pooling_method = other.m_pooling_method;
            m_use_polarity = other.m_use_polarity;
            m_use_interpolation = other.m_use_interpolation;
            m_use_unreliable = other.m_use_unreliable;
            m_use_separated_scales = other.m_use_separated_scales;
            m_number_of_scales = other.m_number_of_scales;
            m_scales_margin = other.m_scales_margin;
            m_use_spatial_normalization = other.m_use_spatial_normalization;
            m_spatial_normalization = other.m_spatial_normalization;
            m_normalization = other.m_normalization;
            m_power_factor = other.m_power_factor;
            m_cutoff_value = other.m_cutoff_value;
        }
        
        return *this;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ ACCESS FUNCTIONS ]=========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    void VisualWordsHistogramParameters::setNumberOfVisualWords(unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor)
    {
        m_number_of_visual_words = number_of_visual_words;
        m_number_of_words_per_descriptor = number_of_words_per_descriptor;
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (number_of_visual_words + number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setPyramidParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y)
    {
        if (m_level_spatial_bins != 0) delete [] m_level_spatial_bins;
        if (m_level_partitions_x != 0) delete [] m_level_partitions_x;
        if (m_level_partitions_y != 0) delete [] m_level_partitions_y;
        
        if (number_of_pyramid_levels > 0)
        {
            m_level_spatial_bins = new unsigned int[number_of_pyramid_levels];
            m_level_partitions_x = new unsigned int[number_of_pyramid_levels];
            m_level_partitions_y = new unsigned int[number_of_pyramid_levels];
        }
        else
        {
            m_level_spatial_bins = 0;
            m_level_partitions_x = 0;
            m_level_partitions_y = 0;
        }
        
        m_number_of_pyramid_levels = number_of_pyramid_levels;
        m_initial_degree_x = initial_x;
        m_initial_degree_y = initial_y;
        m_growth_x = growth_x;
        m_growth_y = growth_y;
        m_number_of_spatial_bins = 0;
        for (unsigned int level = 0, bins_x = initial_x, bins_y = initial_y; level < number_of_pyramid_levels; ++level, bins_x *= growth_x, bins_y *= growth_y)
        {
            m_number_of_spatial_bins += bins_x * bins_y;
            m_level_spatial_bins[level] = bins_x * bins_y;
            m_level_partitions_x[level] = bins_x;
            m_level_partitions_y[level] = bins_y;
        }
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setPyramidParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information)
    {
        if (m_level_spatial_bins != 0) delete [] m_level_spatial_bins;
        if (m_level_partitions_x != 0) delete [] m_level_partitions_x;
        if (m_level_partitions_y != 0) delete [] m_level_partitions_y;
        
        if (pyramid_information.size() > 0)
        {
            m_level_spatial_bins = new unsigned int[pyramid_information.size()];
            m_level_partitions_x = new unsigned int[pyramid_information.size()];
            m_level_partitions_y = new unsigned int[pyramid_information.size()];
        }
        else
        {
            m_level_spatial_bins = 0;
            m_level_partitions_x = 0;
            m_level_partitions_y = 0;
        }
        
        m_number_of_pyramid_levels = pyramid_information.size();
        m_growth_x = 0;
        m_growth_y = 0;
        if (pyramid_information.size() > 0)
        {
            m_initial_degree_x = pyramid_information[0].getFirst();
            m_initial_degree_y = pyramid_information[0].getSecond();
            m_number_of_spatial_bins = 0;
            for (unsigned int level = 0; level < pyramid_information.size(); ++level)
            {
                m_number_of_spatial_bins += pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_spatial_bins[level] = pyramid_information[level].getFirst() * pyramid_information[level].getSecond();
                m_level_partitions_x[level] = pyramid_information[level].getFirst();
                m_level_partitions_y[level] = pyramid_information[level].getSecond();
            }
        }
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setPolarity(bool polarity)
    {
        m_use_polarity = polarity;
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setUnreliable(bool unreliable)
    {
        m_use_unreliable = unreliable;
        
        if (unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setDifferentiateScales(bool use_separated_scales)
    {
        if (use_separated_scales && (m_number_of_scales == 0)) m_number_of_scales = 1;
        m_use_separated_scales = use_separated_scales;
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    void VisualWordsHistogramParameters::setNumberOfScales(unsigned int number_of_scales)
    {
        if (m_use_separated_scales && (number_of_scales == 0)) m_number_of_scales = 1;
        else m_number_of_scales = number_of_scales;
        
        if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
        else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
        if (m_use_polarity) m_number_of_bins *= 2;
        if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
        m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    void VisualWordsHistogramParameters::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Visual_Words_Histogram_Parameters");
        parser.setAttribute("Number_Of_Visual_Words", m_number_of_visual_words);
        parser.setAttribute("Number_Of_Words_Descriptor", m_number_of_words_per_descriptor);
        parser.setAttribute("Number_Of_Pyramid_Levels", m_number_of_pyramid_levels);
        parser.setAttribute("Initial_Degree_X", m_initial_degree_x);
        parser.setAttribute("Initial_Degree_Y", m_initial_degree_y);
        parser.setAttribute("Growth_X", m_growth_x);
        parser.setAttribute("Growth_Y", m_growth_y);
        parser.setAttribute("Pooling_Method", (int)m_pooling_method);
        parser.setAttribute("Polarity", m_use_polarity);
        parser.setAttribute("Interpolation", m_use_interpolation);
        parser.setAttribute("Unreliable", m_use_unreliable);
        parser.setAttribute("Separated_Scales", m_use_separated_scales);
        parser.setAttribute("Number_Of_Scales", m_number_of_scales);
        parser.setAttribute("Scales_Margin", m_scales_margin);
        parser.setAttribute("Use_Spatial_Normalization", m_use_spatial_normalization);
        parser.setAttribute("Spatial_Area", m_spatial_area);
        parser.setAttribute("Power_Factor", m_power_factor);
        parser.setAttribute("Cutoff_Value", m_cutoff_value);
        parser.addChildren();
        // ..........................................................................................................................................
        for (unsigned int i = 0; i < m_number_of_pyramid_levels; ++i)
        {
            parser.openTag("Spatial_Information");
            parser.setAttribute("ID", i);
            parser.setAttribute("Partitions_X", m_level_partitions_x[i]);
            parser.setAttribute("Partitions_Y", m_level_partitions_y[i]);
            parser.closeTag();
        }
        // ..........................................................................................................................................
        parser.openTag("Spatial_Normalization");
        parser.addChildren();
        m_spatial_normalization.convertToXML(parser);
        parser.closeTag();
        // ..........................................................................................................................................
        parser.openTag("Global_Normalization");
        parser.addChildren();
        m_normalization.convertToXML(parser);
        parser.closeTag();
        // ..........................................................................................................................................
        parser.closeTag();
    }
    
    void VisualWordsHistogramParameters::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Visual_Words_Histogram_Parameters"))
        {
            if (m_level_spatial_bins != 0) delete [] m_level_spatial_bins;
            if (m_level_partitions_x != 0) delete [] m_level_partitions_x;
            if (m_level_partitions_y != 0) delete [] m_level_partitions_y;
            
            m_number_of_visual_words = parser.getAttribute("Number_Of_Visual_Words");
            m_number_of_words_per_descriptor = parser.getAttribute("Number_Of_Words_Descriptor");
            m_number_of_pyramid_levels = parser.getAttribute("Number_Of_Pyramid_Levels");
            m_initial_degree_x = parser.getAttribute("Initial_Degree_X");
            m_initial_degree_y = parser.getAttribute("Initial_Degree_Y");
            m_growth_x = parser.getAttribute("Growth_X");
            m_growth_y = parser.getAttribute("Growth_Y");
            m_pooling_method = (BOVW_POOLING_OPERATOR)((int)parser.getAttribute("Pooling_Method"));
            m_use_polarity = parser.getAttribute("Polarity");
            m_use_interpolation = parser.getAttribute("Interpolation");
            m_use_unreliable = parser.getAttribute("Unreliable");
            m_use_separated_scales = parser.getAttribute("Separated_Scales");
            m_number_of_scales = parser.getAttribute("Number_Of_Scales");
            m_scales_margin = parser.getAttribute("Scales_Margin");
            m_use_spatial_normalization = parser.getAttribute("Use_Spatial_Normalization");
            m_spatial_area = parser.getAttribute("Spatial_Area");
            m_power_factor = parser.getAttribute("Power_Factor");
            m_cutoff_value = parser.getAttribute("Cutoff_Value");
            
            if (m_number_of_pyramid_levels > 0)
            {
                m_level_spatial_bins = new unsigned int[m_number_of_pyramid_levels];
                m_level_partitions_x = new unsigned int[m_number_of_pyramid_levels];
                m_level_partitions_y = new unsigned int[m_number_of_pyramid_levels];
                for (unsigned int l = 0; l < m_number_of_pyramid_levels; ++l)
                {
                    m_level_spatial_bins[l] = 0;
                    m_level_partitions_x[l] = 0;
                    m_level_partitions_y[l] = 0;
                }
            }
            else
            {
                m_level_spatial_bins = 0;
                m_level_partitions_x = 0;
                m_level_partitions_y = 0;
            }
            
            while (!(parser.isTagIdentifier("Visual_Words_Histogram_Parameters") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Spatial_Normalization"))
                {
                    while (!(parser.isTagIdentifier("Spatial_Normalization") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Vector_Norm")) m_spatial_normalization.convertFromXML(parser);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Global_Normalization"))
                {
                    while (!(parser.isTagIdentifier("Global_Normalization") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Vector_Norm")) m_spatial_normalization.convertFromXML(parser);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Spatial_Information"))
                {
                    unsigned int ID;
                    
                    ID = parser.getAttribute("ID");
                    m_level_partitions_x[ID] = parser.getAttribute("Partitions_X");
                    m_level_partitions_y[ID] = parser.getAttribute("Partitions_Y");
                    m_level_spatial_bins[ID] = m_level_partitions_x[ID] * m_level_partitions_y[ID];
                    while (!(parser.isTagIdentifier("Spatial_Information") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
            
            m_number_of_spatial_bins = 0;
            for (unsigned int level = 0; level < m_number_of_pyramid_levels; ++level)
                m_number_of_spatial_bins += m_level_spatial_bins[level];
            
            if (m_use_unreliable) m_number_of_bins = m_number_of_spatial_bins * (m_number_of_visual_words + m_number_of_words_per_descriptor);
            else m_number_of_bins = m_number_of_spatial_bins * m_number_of_visual_words;
            if (m_use_polarity) m_number_of_bins *= 2;
            if (m_use_separated_scales && (m_number_of_scales > 1)) m_number_of_bins *= m_number_of_scales;
            m_number_of_bins_spatial = m_number_of_bins / m_number_of_spatial_bins;
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | HISTOGRAM OF VISUAL WORDS CLASS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    std::istream& operator>>(std::istream &in, VisualWordsHistogram &histogram)
    {
        unsigned int number_of_dimensions;
        
        in >> histogram.m_x >> histogram.m_y >> histogram.m_width >> histogram.m_height >> histogram.m_initial_scale >> number_of_dimensions;
        histogram.m_histogram.set(number_of_dimensions);
        for (unsigned int i = 0; i < number_of_dimensions; ++i)
        {
            unsigned int current_index;
            double current_weight;
            
            in >> current_weight >> current_index;
            histogram.m_histogram[i].setData(current_weight, current_index);
        }
        return in;
    }
    
    std::ostream& operator<<(std::ostream &out, const VisualWordsHistogram &histogram)
    {
        out << histogram.m_x << " " << histogram.m_y << " " << histogram.m_width << " " << histogram.m_height << " " << histogram.m_initial_scale << " " << histogram.m_histogram.size();
        for (unsigned int i = 0; i < histogram.m_histogram.size(); ++i)
            out << " " << histogram.m_histogram[i].getFirst() << " " << histogram.m_histogram[i].getSecond();
        return out;
    }
    
    void VisualWordsHistogram::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Visual_Words_Histogram");
        parser.setAttribute("x", m_x);
        parser.setAttribute("y", m_y);
        parser.setAttribute("width", m_width);
        parser.setAttribute("height", m_height);
        parser.setAttribute("initial_scale", m_initial_scale);
        parser.addChildren();
        saveVector(parser, "Histogram", m_histogram);
        parser.closeTag();
    }
    
    void VisualWordsHistogram::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Visual_Words_Histogram"))
        {
            m_x = parser.getAttribute("x");
            m_y = parser.getAttribute("y");
            m_width = parser.getAttribute("width");
            m_height = parser.getAttribute("height");
            m_initial_scale = parser.getAttribute("initial_scale");
            
            while (!(parser.isTagIdentifier("Visual_Words_Histogram") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Histogram")) loadVector(parser, "Histogram", m_histogram);
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

