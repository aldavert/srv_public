// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_VISUAL_WORDS_HISTOGRAM_HEADER_FILE__
#define __SRV_VISUAL_WORDS_HISTOGRAM_HEADER_FILE__

#include <map>
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../descriptors/srv_dense_descriptors.hpp"
#include "srv_visual_words_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | VISUAL WORDS HISTOGRAM INFORMATION   |
    //                   | CLASS - DECLARATION                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which stores the histogram of visual words parameters.
    class VisualWordsHistogramParameters
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        VisualWordsHistogramParameters(void);
        /** Constructor where only the size of the codebook is specified.
         *  \param[in] number_of_visual_words number of visual words in the codebook.
         *  \param[in] number_of_words_per_descriptor number of visual words assigned to a descriptor.
         */
        VisualWordsHistogramParameters(unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor);
        /** Constructor where the size of the codebook and the geometry of the spatial pyramid are given as parameters.
         *  \param[in] number_of_pyramid_levels number of levels of the spatial pyramid.
         *  \param[in] initial_x initial number of partitions in the X-direction in the spatial pyramid.
         *  \param[in] initial_y initial number of partitions in the Y-direction in the spatial pyramid.
         *  \param[in] growth_x growth factor of the spatial partitions in the X-direction of the spatial pyramid for each successive level.
         *  \param[in] growth_y growth factor of the spatial partitions in the Y-direction of the spatial pyramid for each successive level.
         *  \param[in] number_of_visual_words number of visual words of the codebook.
         *  \param[in] number_of_words_per_descriptor number of visual words assigned to a descriptor.
         */
        VisualWordsHistogramParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor);
        /** Constructor which the parameters of the histogram of visual words.
         *  \param[in] number_of_pyramid_levels number of levels of the spatial pyramid.
         *  \param[in] initial_x initial number of partitions in the X-direction of the spatial pyramid.
         *  \param[in] initial_y initial number of partitions in the Y-direction of the spatial pyramid.
         *  \param[in] growth_x growth factor of the spatial partitions in the X-direction of the spatial pyramid for each successive level.
         *  \param[in] growth_y growth factor of the spatial partitions in the Y-direction of the spatial pyramid for each successive level.
         *  \param[in] number_of_visual_words number of visual words of the codebook.
         *  \param[in] number_of_words_per_descriptor number of visual words assigned to a descriptor.
         *  \param[in] pooling_method method used to pool the visual words into the histogram.
         *  \param[in] polarity enables or disables the use of polarity in the histogram of visual words. When polarity is enabled, the sign of the weight of each visual word is taken into account by accumulating positive and negative contributions separately (this doubles the size of the histogram of visual words).
         *  \param[in] interpolation enables or disables the use of bi-linear interpolation to weight the contribution of visual words between spatial levels at the same level of the pyramid.
         *  \param[in] unreliable enables or disables the aggregation of unreliable visual words instead that simply rejecting them (unreliable visual words will be aggregated on a differentiated set of bins of the histogram).
         *  \param[in] use_separated_scales enables or disables the use of the scale of the local features to differentiate the contribution of the visual words in the histogram.
         *  \param[in] number_of_scales sets the number of scales of the visual words which are accumulated independently for histograms where only the visual words of a certain scales are accumulated. Set to zero to accumulate visual words independently of their scale.
         *  \param[in] scales_margin enables the extraction of histograms at the lower and upper scales where visual words are not available for all scale bins when the contribution of visual words is discriminated between scales.
         *  \param[in] use_spatial_norm enables or disables the normalization of the histogram at each scale of the spatial pyramid.
         *  \param[in] use_spatial_area enables or disables the use of the area of the spatial bins to normalize the histogram at each spatial level.
         *  \param[in] spatial_norm normalization object used to normalize the histogram of visual words at each level of the spatial pyramid.
         *  \param[in] global_norm normalization object used to normalize the histogram of visual words.
         *  \param[in] power_factor power factor used to increase the contribution of visual words with a low support.
         *  \param[in] cutoff cut-off threshold used to lessen the contribution of too frequent visual words.
         */
        VisualWordsHistogramParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor, BOVW_POOLING_OPERATOR pooling_method, bool polarity, bool interpolation, bool unreliable, bool use_separated_scales, unsigned int number_of_scales, bool scales_margin, bool use_spatial_norm, bool use_spatial_area, const VectorNorm &spatial_norm, const VectorNorm &global_norm, double power_factor, double cutoff);
        /** Constructor where the size of the codebook and the geometry of the spatial pyramid are given as parameters.
         *  \param[in] pyramid_information vector of tuples where the first elements are the partitions in the X-direction and the second elements are the partitions in the Y-direction.
         *  \param[in] number_of_visual_words number of visual words of the codebook.
         *  \param[in] number_of_words_per_descriptor number of visual words assigned to a descriptor.
         */
        VisualWordsHistogramParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor);
        /** Constructor which the parameters of the histogram of visual words.
         *  \param[in] pyramid_information vector of tuples where the first elements are the partitions in the X-direction and the second elements are the partitions in the Y-direction.
         *  \param[in] number_of_visual_words number of visual words of the codebook.
         *  \param[in] number_of_words_per_descriptor number of visual words assigned to a descriptor.
         *  \param[in] pooling_method method used to pool the visual words into the histogram.
         *  \param[in] polarity enables or disables the use of polarity in the histogram of visual words. When polarity is enabled, the sign of the weight of each visual word is taken into account by accumulating positive and negative contributions separately (this doubles the size of the histogram of visual words).
         *  \param[in] interpolation enables or disables the use of bi-linear interpolation to weight the contribution of visual words between spatial levels at the same level of the pyramid.
         *  \param[in] unreliable enables or disables the aggregation of unreliable visual words instead that simply rejecting them (unreliable visual words will be aggregated on a differentiated set of bins of the histogram).
         *  \param[in] use_separated_scales enables or disables the use of the scale of the local features to differentiate the contribution of the visual words in the histogram.
         *  \param[in] number_of_scales sets the number of scales of the visual words which are accumulated independently for histograms where only the visual words of a certain scales are accumulated. Set to zero to accumulate visual words independently of their scale.
         *  \param[in] scales_margin enables the extraction of histograms at the lower and upper scales where visual words are not available for all scale bins when the contribution of visual words is discriminated between scales.
         *  \param[in] use_spatial_norm enables or disables the normalization of the histogram at each scale of the spatial pyramid.
         *  \param[in] use_spatial_area enables or disables the use of the area of the spatial bins to normalize the histogram at each spatial level.
         *  \param[in] spatial_norm normalization object used to normalize the histogram of visual words at each level of the spatial pyramid.
         *  \param[in] global_norm normalization object used to normalize the histogram of visual words.
         *  \param[in] power_factor power factor used to increase the contribution of visual words with a low support.
         *  \param[in] cutoff cut-off threshold used to lessen the contribution of too frequent visual words.
         */
        VisualWordsHistogramParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information, unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor, BOVW_POOLING_OPERATOR pooling_method, bool polarity, bool interpolation, bool unreliable, bool use_separated_scales, unsigned int number_of_scales, bool scales_margin, bool use_spatial_norm, bool use_spatial_area, const VectorNorm &spatial_norm, const VectorNorm &global_norm, double power_factor, double cutoff);
        /// Copy constructor.
        VisualWordsHistogramParameters(const VisualWordsHistogramParameters &other);
        /// Destructor.
        ~VisualWordsHistogramParameters(void);
        /// Assignation operator
        VisualWordsHistogramParameters& operator=(const VisualWordsHistogramParameters &other);
        
        // -[ Access functions to the codebook parameters ]------------------------------------------------------------------------------------------
        /** Set the number of visual words of the codebook used to generate the visual words.
         *  \param[in] number_of_visual_words number of visual words of the codebook.
         *  \param[in] number_of_words_per_descriptor number of words returned for each encoded descriptor.
         */
        void setNumberOfVisualWords(unsigned int number_of_visual_words, unsigned int number_of_words_per_descriptor);
        /// Returns the number of visual words of the codebook used to generate the visual words.
        inline unsigned int getNumberOfVisualWords(void) const { return m_number_of_visual_words; }
        /// Returns the number of visual words assigned to a descriptor.
        inline unsigned int getNumberOfWordsDescriptor(void) const { return m_number_of_words_per_descriptor; }
        
        // -[ Access functions to the spatial pyramid options ]--------------------------------------------------------------------------------------
        /** Sets the parameters of the spatial pyramid.
         *  \param[in] number_of_pyramid_levels number of levels of the spatial pyramid.
         *  \param[in] initial_x initial number of partitions in the X-direction of the spatial pyramid.
         *  \param[in] initial_y initial number of partitions in the Y-direction of the spatial pyramid.
         *  \param[in] growth_x growth factor of the spatial partitions in the X-direction of the spatial pyramid for each successive level.
         *  \param[in] growth_y growth factor of the spatial partitions in the Y-direction of the spatial pyramid for each successive level.
         */
        void setPyramidParameters(unsigned int number_of_pyramid_levels, unsigned int initial_x, unsigned int initial_y, unsigned int growth_x, unsigned int growth_y);
        /** Sets the parameters of the spatial pyramid.
         *  \param[in] pyramid_information vector of tuples where the first elements are the partitions in the X-direction and the second elements are the partitions in the Y-direction.
         */
        void setPyramidParameters(const VectorDense<Tuple<unsigned int, unsigned int> > &pyramid_information);
        /// Returns the number of spatial bins of the histogram of visual words.
        inline unsigned int getNumberOfSpatialBins(void) const { return m_number_of_spatial_bins; }
        /// Returns the total number of bins of the histogram of visual words.
        inline unsigned int getNumberOfBins(void) const { return m_number_of_bins; }
        /// Returns the total number of bins at each spatial bin of the visual words histogram.
        inline unsigned int getNumberOfBinsSpatial(void) const { return m_number_of_bins_spatial; }
        /// Returns the number of levels of the spatial pyramid.
        inline unsigned int getNumberOfPyramidLevels(void) const { return m_number_of_pyramid_levels; }
        /// Returns the initial number of partitions in the X-direction of the spatial pyramid.
        inline unsigned int getInitialXDegree(void) const { return m_initial_degree_x; }
        /// Returns the initial number of partitions in the Y-direction of the spatial pyramid.
        inline unsigned int getInitialYDegree(void) const { return m_initial_degree_y; }
        /// Returns the growth factor of the spatial partitions in the X-direction of the spatial pyramid for each successive level.
        inline unsigned int getPyramidGrowthX(void) const { return m_growth_x; }
        /// Returns the growth factor of the spatial partitions in the Y-direction of the spatial pyramid for each successive level.
        inline unsigned int getPyramidGrowthY(void) const { return m_growth_y; }
        /// Returns the number of spatial bins at the index-th level of the pyramid.
        inline unsigned int getNumberOfSpatialBins(unsigned int index) const { return m_level_spatial_bins[index]; }
        /// Returns the number of divisions in the X-direction at the index-th level of the pyramid.
        inline unsigned int getNumberOfDivisionsX(unsigned int index) const { return m_level_partitions_x[index]; }
        /// Returns the number of divisions in the Y-direction at the index-th level of the pyramid.
        inline unsigned int getNumberOfDivisionsY(unsigned int index) const { return m_level_partitions_y[index]; }
        
        // -[ Visual words pooling options ]---------------------------------------------------------------------------------------------------------
        /// Returns a boolean which indicates if the max-pooling accumulation method for the spatial pyramid is enabled or disabled.
        inline BOVW_POOLING_OPERATOR getPoolingMethod(void) const { return m_pooling_method; }
        /// Enables or disables the use of the max-pooling method to accumulate the contributions of the different levels of the spatial pyramid.
        inline void setPoolingMethod(BOVW_POOLING_OPERATOR pooling_method) { m_pooling_method = pooling_method; }
        /// Returns a boolean which indicates if the polarity accumulation method is enabled or disabled.
        inline bool getPolarity(void) const { return m_use_polarity; }
        /// Enables or disables the use of the polarity accumulation method.
        void setPolarity(bool polarity);
        /// Returns a boolean which indicates if bi-linear interpolation is used to weight the contribution of visual words between adjacent spatial bins is enabled or disabled.
        inline bool getInterpolation(void) const { return m_use_interpolation; }
        /// Enables or disables the use of bi-linear interpolation to weight the contribution of visual words between adjacent spatial bins.
        inline void setInterpolation(bool interpolation) { m_use_interpolation = interpolation; }
        /// Returns a boolean flag which indicates if unreliable visual words are aggregated on the histogram or are simply rejected.
        inline bool getUnreliable(void) const { return m_use_unreliable; }
        /// Enables or disables the aggregation of unreliable visual words instead that simply rejecting them (unreliable visual words will be aggregated on a differentiated set of bins of the histogram).
        void setUnreliable(bool unreliable);
        /// Returns a boolean which indicates if local features scale is used to differentiate the contribution of visual words in the histogram.
        inline bool getDifferentiateScales(void) const { return m_use_separated_scales; }
        /// Enables or disables the use of the scale of the local features to differentiate the contribution of visual words in the histogram.
        void setDifferentiateScales(bool use_separated_scales);
        /// Returns the number of scales which are accumulated independently.
        inline unsigned int getNumberOfScales(void) const { return m_number_of_scales; }
        /// Sets the number of scales which are accumulated independently. Set to zero to pool visual words without taking into account their scale.
        void setNumberOfScales(unsigned int number_of_scales);
        /// Returns a boolean which indicates if the histogram of visual words can be extracted at the lower and upper scales when the contribution of visual words is discriminated between scales.
        inline bool getScalesMargin(void) const { return m_scales_margin; }
        /// Enables or disables the extraction of histograms at the lower and upper scales where visual words are not available for all scale bins when the contribution of visual words is discriminated between scales.
        void setScalesMargin(bool scales_margin) { m_scales_margin = scales_margin; }
        
        // -[ Histogram normalization options ]------------------------------------------------------------------------------------------------------
        /// Returns a boolean which indicates if the normalization of spatial bins is enabled or disabled.
        inline bool getSpatialNormalization(void) const { return m_use_spatial_normalization; }
        /// Enables or disables the use of histogram normalization at the different levels of the spatial pyramid.
        inline void setSpatialNormalization(bool spatial_normalization) { m_use_spatial_normalization = spatial_normalization; }
        /// Returns a boolean which indicates if the different levels of the spatial pyramid are normalized by the relative are of the spatial bin when true. The p-norm of the vector is used when false.
        inline bool getAreaWeighting(void) const { return m_spatial_area; }
        /// Sets the boolean flag which enables the use of the area of the spatial bins to normalize the histogram at each level of the spatial pyramid.
        inline void setAreaWeighting(bool spatial_area) { m_spatial_area = spatial_area; }
        /// Returns the vector normalization object used to normalize each level of the spatial pyramid.
        inline const VectorNorm& getSpatialNorm(void) const { return m_spatial_normalization; }
        /// Sets the normalization object used to normalize the histogram at each level of the pyramid.
        inline void setSpatialNorm(const VectorNorm &norm) { m_spatial_normalization = norm; }
        /// Returns the normalization object used to normalize the histogram of visual words.
        inline const VectorNorm& getHistogramNorm(void) const { return m_normalization; }
        /// Sets the order of the normalization object used to normalize the histogram of visual words.
        inline void setHistogramNorm(const VectorNorm &norm) { m_normalization = norm; }
        /// Returns the power factor applied to the bins of the histogram of visual words in order to lessen the effects of to frequent visual words and increase the contribution of less frequent visual words.
        inline double getPowerFactor(void) const { return m_power_factor; }
        /// Sets the power factor applied to the bins of the histogram of visual words in order to lessen the effects of to frequent visual words and increase the contribution of less frequent visual words. The function generates an exception if the power factor is not between 0 and 1.
        inline void setPowerFactor(double power_factor) { if ((power_factor < 0.0) || (power_factor > 1.0)) throw Exception("Incorrect histogram of visual words power factor (%f)", power_factor); m_power_factor = power_factor; }
        /// Returns the cut-off threshold applied to the normalized bins of the visual words histogram in order to reduce the contribution of too frequent visual words.
        inline double getCutoffValue(void) const { return m_cutoff_value; }
        /// Sets the cut-off threshold applied to the normalized bins of the visual words histogram in order to reduce the contribution of too frequent visual words. The function generates an exception if the cutoff value is not between 0 and 1.
        inline void setCutoffValue(double cutoff_value) { if ((cutoff_value < 0.0) || (cutoff_value > 1.0)) throw Exception("Incorrect cut-off threshold (%f) for the histogram of visual words.", cutoff_value); m_cutoff_value = cutoff_value; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the visual words histogram information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the visual words histogram information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        // -[ Histogram dimensionality parameters ]------------------------------------------------
        /// Total number of spatial bins in the histogram.
        unsigned int m_number_of_spatial_bins;
        /// Number of visual words in the codebook.
        unsigned int m_number_of_visual_words;
        /// Number of visual words assigned to a descriptor.
        unsigned int m_number_of_words_per_descriptor;
        /// Total number of bins of the bag of visual words histogram.
        unsigned int m_number_of_bins;
        /// Number of bins of the bag of visual words histogram at each spatial bin of the histogram.
        unsigned int m_number_of_bins_spatial;
        // -[ Spatial pyramids parameters ]--------------------------------------------------------
        /// Number of levels of the pyramid of spatial bins.
        unsigned int m_number_of_pyramid_levels;
        /// Initial number of spatial partitions in the X direction.
        unsigned int m_initial_degree_x;
        /// Initial number of spatial partitions in the Y direction.
        unsigned int m_initial_degree_y;
        /// Growth factor in the spatial partitions in the X direction.
        unsigned int m_growth_x;
        /// Growth factor in the spatial partitions in the Y direction.
        unsigned int m_growth_y;
        // -[ Spatial pyramids information ]-------------------------------------------------------
        /// Array with the spatial bins at each level of the pyramid.
        unsigned int * m_level_spatial_bins;
        /// Array with the partitions in the X-direction at each level of the pyramid.
        unsigned int * m_level_partitions_x;
        /// Array with the partitions in the Y-direction at each level of the pyramid.
        unsigned int * m_level_partitions_y;
        // -[ Pooling parameters ]-----------------------------------------------------------------
        /// Flag which enables or disables the use of max-pooling the contribution of spatial histogram bins.
        BOVW_POOLING_OPERATOR m_pooling_method;
        /// Flag which enables or disables the use of polarity (i.e.\ accumulate the positive and negative contributions of visual words separately) in visual words histograms.
        bool m_use_polarity;
        /// Flag which enables or disables the use spatial interpolation to weight the contribution of visual words among the spatial bins.
        bool m_use_interpolation;
        /// Flag which enables or disables the aggregation of unreliable visual words instead that simply rejecting them (unreliable visual words will be aggregated on a differentiated set of bins of the histogram).
        bool m_use_unreliable;
        /// Flag which enables or disables the use of local feature scale to differentiate the contribution of the visual words in histogram.
        bool m_use_separated_scales;
        /// Number of different visual words scales. Set to zero to accumulate visual words independently of their scale.
        unsigned int m_number_of_scales;
        /// Flag which enables the extraction of histograms at the lower and upper scales where visual words are not available for all scale bins when the contribution of visual words is discriminated between scales.
        bool m_scales_margin;
        // -[ Normalization parameters ]-----------------------------------------------------------
        /// Flag which enables or disables the normalization of histogram bins at the different spatial pyramid levels.
        bool m_use_spatial_normalization;
        /// Enable the use of the area instead of an Lp-norm to normalize the spatial bins of the histogram.
        bool m_spatial_area;
        /// Normalization applied to each spatial level of visual words.
        VectorNorm m_spatial_normalization;
        /// Normalization applied to the histogram of visual words.
        VectorNorm m_normalization;
        /// Power factor used to lessen the effect of visual words which are too frequent.
        double m_power_factor;
        /// Cut-off threshold used to cut the contribution of the visual words.
        double m_cutoff_value;
        
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | HISTOGRAM OF VISUAL WORDS CLASS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which stores the information about a visual words histogram that are accumulated in image rectangular regions.
    class VisualWordsHistogram
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        VisualWordsHistogram(void) : m_x(0), m_y(0), m_width(0), m_height(0), m_initial_scale(0), m_histogram(0) {}
        /** Constructor which initializes the location of the rectangular region in the image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangular region.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangular region.
         *  \param[in] width width of the rectangular region.
         *  \param[in] height height of the rectangular region.
         */
        VisualWordsHistogram(int x, int y, int width, int height) : m_x(x), m_y(y), m_width(width), m_height(height), m_initial_scale(0), m_histogram(0) {}
        /** Constructor which initializes the location of the rectangular region in the image and the initial scale of the visual words accumulated in the histogram.
         *  \param[in] x X-coordinate of the top-left corner of the rectangular region.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangular region.
         *  \param[in] width width of the rectangular region.
         *  \param[in] height height of the rectangular region.
         *  \param[in] initial_scale initial scale of the visual words accumulated in the histograms.
         */
        VisualWordsHistogram(int x, int y, int width, int height, int initial_scale) : m_x(x), m_y(y), m_width(width), m_height(height), m_initial_scale(initial_scale), m_histogram(0) {}
        /** Constructor which initializes the location of the rectangular region in the image and its histogram of visual words.
         *  \param[in] x X-coordinate of the top-left corner of the rectangular region.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangular region.
         *  \param[in] width width of the rectangular region.
         *  \param[in] height height of the rectangular region.
         *  \param[in] histogram histogram of visual words of the region.
         */
        VisualWordsHistogram(int x, int y, int width, int height, const VectorSparse<double> &histogram) :
            m_x(x), m_y(y), m_width(width), m_height(height), m_initial_scale(0), m_histogram(histogram) {}
        /** Constructor which initializes the parameters of the rectangular region.
         *  \param[in] x X-coordinate of the top-left corner of the rectangular region.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangular region.
         *  \param[in] width width of the rectangular region.
         *  \param[in] height height of the rectangular region.
         *  \param[in] initial_scale initial scale of the visual words accumulated in the histograms.
         *  \param[in] histogram histogram of visual words of the region.
         */
        VisualWordsHistogram(int x, int y, int width, int height, int initial_scale, const VectorSparse<double> &histogram) :
            m_x(x), m_y(y), m_width(width), m_height(height), m_initial_scale(initial_scale), m_histogram(histogram) {}
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the X-coordinate of the top-left corner of the rectangular region.
        inline int getX(void) const { return m_x; }
        /// Sets the X-coordinate of the top-left corner of the rectangular region.
        inline void setX(int x) { m_x = x; }
        /// Returns the Y-coordinate of the top-left corner of the rectangular region.
        inline int getY(void) const { return m_y; }
        /// Sets the Y-coordinate of the top-left corner of the rectangular region.
        inline void setY(int y) { m_y = y; }
        /// Sets the X and Y coordinates of the top-left corner of the rectangular region.
        inline void setLocation(int x, int y) { m_x = x; m_y = y; }
        /// Returns the width of the rectangular region.
        inline int getWidth(void) const { return m_width; }
        /// Sets the width of the rectangular region.
        inline void setWidth(int width) { m_width = width; }
        /// Returns the height of the rectangular region.
        inline int getHeight(void) const { return m_height; }
        /// Sets the height of the rectangular region.
        inline void setHeight(int height) { m_height = height; }
        /// Sets the top-left corner coordinates and the geometry of the rectangular region.
        inline void setGeometry(int x, int y, int width, int height) { m_x = x; m_y = y; m_width = width; m_height = height; }
        /// Sets the geometry of the rectangular region.
        inline void setGeometry(int width, int height) { m_width = width; m_height = height; }
        /// Returns the initial scale of the visual words accumulated by the histogram.
        inline int getInitialScale(void) const { return m_initial_scale; }
        /// Sets the initial scale of the visual words accumulated by the histogram.
        inline void setInitialScale(int initial_scale) { m_initial_scale = initial_scale; }
        /// Returns a constant reference to the sparse vector where the histogram is stored.
        inline const VectorSparse<double>& getHistogram(void) const { return m_histogram; }
        /// Returns a reference to the sparse vector where the histogram is stored.
        inline VectorSparse<double>& getHistogram(void) { return m_histogram; }
        /// Sets the histogram of the rectangular region.
        template <template <class, class> class VECTOR, class TWEIGHT_OTHER, class TINDEX_OTHER>
        inline void setHistogram(const VECTOR<TWEIGHT_OTHER, TINDEX_OTHER> &other) { m_histogram = other; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the histogram of visual words information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the histogram of visual words information from an XML object.
        void convertFromXML(XmlParser &parser);
        
        // -[ Stream redirection functions ]---------------------------------------------------------------------------------------------------------
        /// Retrieves the histogram information from an input stream.
        friend std::istream& operator>>(std::istream &in, VisualWordsHistogram &histogram);
        /// Stores the histogram information into an output stream.
        friend std::ostream& operator<<(std::ostream &out, const VisualWordsHistogram &histogram);
    private:
        /// X-coordinate of the top-left corner of the rectangular region where the histogram have been calculated.
        int m_x;
        /// Y-coordinate of the top-left corner of the rectangular region where the histogram have been calculated.
        int m_y;
        /// Width of the rectangular region.
        int m_width;
        /// Height of the rectangular region.
        int m_height;
        /// Initial scale for histogram of visual words which take into account the scale of the accumulated visual words.
        int m_initial_scale;
        /// Sparse vector with the histogram values.
        VectorSparse<double> m_histogram;
    };
    
    /// Retrieves the histogram information from an input stream.
    std::istream& operator>>(std::istream &in, VisualWordsHistogram &histogram);
    
    /// Stores the histogram information into an output stream.
    std::ostream& operator<<(std::ostream &out, const VisualWordsHistogram &histogram);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VISUAL WORDS IMAGE CLASS             |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which accumulates the visual words into an image-like structure, so that, visual words spatially accumulated more efficiently.
    template <class TWEIGHT, class TINDEX>
    class VisualWordsImage
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        VisualWordsImage(void);
        /** Constructor which generates an empty image of visual words with the specified geometry.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] scales_information array with of region size-index tuples which defines the information of the scales of the visual words image.
         *  \param[in] number_of_scales number of scales of the visual words image
         */
        VisualWordsImage(unsigned int width, unsigned int height, const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales);
        /** Constructor which creates a visual words image generated from the visual words extracted from densely sampled regions.
         *  \param[in] local_regions array with the location of the dense regions of the visual words.
         *  \param[in] visual_words array with the visual words associated to each location.
         *  \param[in] number_of_regions number of elements in both regions and visual words arrays.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] region_size size of the dense regions at each scale.
         *  \param[in] number_of_scales number of scales of the local regions.
         */
        VisualWordsImage(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, unsigned int width, unsigned int height, const unsigned int * region_size, unsigned int number_of_scales);
        /// Copy constructor.
        VisualWordsImage(const VisualWordsImage<TWEIGHT, TINDEX> &other);
        /// Destructor.
        ~VisualWordsImage(void);
        /// Assignation operator.
        VisualWordsImage<TWEIGHT, TINDEX>& operator=(const VisualWordsImage<TWEIGHT, TINDEX> &other);
        
        // -[ Set the image information ]------------------------------------------------------------------------------------------------------------
        /** This function sets the dimensionality and the number of scales of the visual words image.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] scales_information array with of region size-index tuples which defines the information of the scales of the visual words image.
         *  \param[in] number_of_scales number of scales of the visual words image
         */
        void set(unsigned int width, unsigned int height, const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales);
        /** Function which creates a visual words image generated from the visual words extracted from densely sampled regions.
         *  \param[in] local_regions array with the location of the dense regions of the visual words.
         *  \param[in] visual_words array with the visual words associated to each location.
         *  \param[in] number_of_regions number of elements in both regions and visual words arrays.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] region_size size of the dense regions at each scale.
         *  \param[in] number_of_scales number of scales of the local regions.
         */
        void set(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, unsigned int width, unsigned int height, const unsigned int * region_size, unsigned int number_of_scales);
        /** Function which creates a visual words image generated from the visual words extracted from densely sampled regions.
         *  \param[in] local_regions array with the location of the dense regions of the visual words.
         *  \param[in] visual_words array with the visual words associated to each location.
         *  \param[in] number_of_regions number of elements in both regions and visual words arrays.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] region_size vector with the size of the dense regions at each scale.
         */
        inline void set(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, unsigned int width, unsigned int height, const VectorDense<unsigned int> &region_size) { set(local_regions, visual_words, number_of_regions, width, height, region_size.getData(), region_size.size()); }
        /// Removes the visual words stored in the image but keeps the scales information structures.
        void clear(void);
        /** This function adds to the visual words image the given visual words extracted from local regions.
         *  \param[in] local_regions array with the location of the dense regions of the visual words.
         *  \param[in] visual_words array with the visual words associated to each location.
         *  \param[in] number_of_regions number of elements in both regions and visual words arrays.
         */
        void append(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, const unsigned int * region_size, unsigned int number_of_scales);
        
        // -[ Access information ]-------------------------------------------------------------------------------------------------------------------
        /// Returns the width of the visual words image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the visual words image.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of scales in the visual words image.
        inline unsigned int getNumberOfScales(void) const { return m_number_of_scales; }
        /// Returns the local region geometry of the index-th scale of the visual words image.
        inline const Tuple<unsigned int, unsigned int>& getScaleInformation(unsigned int index) const { return m_scales_information[index]; }
        
        // -[ Access the visual words information ]--------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the visual words image to the index-th layer of the visual words image (i.e.\ to the visual words image of the index-th scale).
        inline const VectorSparse<TWEIGHT, TINDEX>* get(unsigned int index) const { return m_visual_words[index]; }
        /// Returns a pointer to the visual words image to the index-th layer of the visual words image (i.e.\ to the visual words image of the index-th scale).
        inline VectorSparse<TWEIGHT, TINDEX>* get(unsigned int index) { return m_visual_words[index]; }
        /** Returns a constant reference to the visual words at a certain location of the image.
         *  \param[in] x x-coordinate in the visual words image.
         *  \param[in] y y-coordinate in the visual words image.
         *  \param[in] index local regions scale of the visual words image.
         */
        inline const VectorDense<TWEIGHT, TINDEX>& get(unsigned int x, unsigned int y, unsigned int index) const { return m_visual_words[index][x + y * m_width]; }
        /** Returns a reference to the visual words at a certain location of the image.
         *  \param[in] x x-coordinate in the visual words image.
         *  \param[in] y y-coordinate in the visual words image.
         *  \param[in] index local regions scale of the visual words image.
         */
        inline VectorDense<TWEIGHT, TINDEX>& get(unsigned int x, unsigned int y, unsigned int index) { return m_visual_words[index][x + y * m_width]; }
        
        // -[ Histogram of visual words extraction functions ]---------------------------------------------------------------------------------------
        /** Calculate the histogram of visual words at the given location and returns the unnormalized histogram.
         *  \param[in,out] histogram visual words histogram which stores the resulting histogram and it contains the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in] histogram_parameters parameters of the visual words histogram.
         *  \returns the accumulated spatial l1-norm of each level of the spatial pyramid.
         */
        void extractRaw(VisualWordsHistogram &histogram, const VisualWordsHistogramParameters &histogram_parameters) const;
        /** Calculate the histogram of visual words at the given location.
         *  \param[in,out] histogram visual words histogram which stores the resulting histogram and it contains the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in] histogram_parameters parameters of the visual words histogram.
         *  \returns the accumulated spatial l1-norm of each level of the spatial pyramid.
         */
        double extract(VisualWordsHistogram &histogram, const VisualWordsHistogramParameters &histogram_parameters) const;
        
        /** Calculate a set of histograms of visual words using the same parameters.
         *  \param[in,out] histogram array with the resulting histograms of visual words. The array must be allocated and the histograms must be initialized to the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in,out] normals a dense vector which stores the accumulated spatial l1-norm for each histogram of visual words. The size of the vector specifies the number of elements in the histogram array.
         *  \param[in] histogram_parameters parameters of the visual words histogram used to extract all the histograms.
         *  \param[in] number_of_threads number of threads used to extract the visual words histograms concurrently.
         */
        inline void extract(VisualWordsHistogram * histogram, VectorDense<double> &normals, const VisualWordsHistogramParameters &histogram_parameters, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int index = omp_get_thread_num(); index < normals.size(); index += number_of_threads)
                    normals[index] = extract(histogram[index], histogram_parameters);
            }
        }
        
        /** Calculate a set of histograms of visual words using the same parameters.
         *  \param[in,out] histogram array of pointers to the resulting histograms of visual words. The array must be allocated and the histograms must be initialized to the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in,out] normals a dense vector which stores the accumulated spatial l1-norm for each histogram of visual words. The size of the vector specifies the number of elements in the histogram array.
         *  \param[in] histogram_parameters parameters of the visual words histogram used to extract all the histograms.
         *  \param[in] number_of_threads number of threads used to extract the visual words histograms concurrently.
         */
        inline void extract(VisualWordsHistogram * * histogram, VectorDense<double> &normals, const VisualWordsHistogramParameters &histogram_parameters, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int index = omp_get_thread_num(); index < normals.size(); index += number_of_threads)
                    normals[index] = extract(*histogram[index], histogram_parameters);
            }
        }
        
        /** Calculate a set of histograms of visual words using different parameters.
         *  \param[in,out] histogram array with the resulting histograms of visual words. The array must be allocated and the histograms must be initialized to the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in,out] normals a dense vector which stores the accumulated spatial l1-norm for each histogram of visual words. The size of the vector specifies the number of elements in the histogram array.
         *  \param[in] histogram_parameters array with the parameters for each histogram.
         *  \param[in] number_of_threads number of threads used to extract the visual words histograms concurrently.
         */
        inline void extract(VisualWordsHistogram * histogram, VectorDense<double> &normals, const VisualWordsHistogramParameters * histogram_parameters, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int index = omp_get_thread_num(); index < normals.size(); index += number_of_threads)
                    normals[index] = extract(histogram[index], histogram_parameters[index]);
            }
        }
        
        /** Calculate a set of histograms of visual words using different parameters.
         *  \param[in,out] histogram array of pointers to the resulting histograms of visual words. The array must be allocated and the histograms must be initialized to the coordinates of the rectangular region where the histogram is accumulated from.
         *  \param[in,out] normals a dense vector which stores the accumulated spatial l1-norm for each histogram of visual words. The size of the vector specifies the number of elements in the histogram array.
         *  \param[in] histogram_parameters array of pointers to the parameters for each histogram.
         *  \param[in] number_of_threads number of threads used to extract the visual words histograms concurrently.
         */
        inline void extract(VisualWordsHistogram * * histogram, VectorDense<double> &normals, VisualWordsHistogramParameters const * const * histogram_parameters, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int index = omp_get_thread_num(); index < normals.size(); index += number_of_threads)
                    normals[index] = extract(*histogram[index], *histogram_parameters[index]);
            }
        }
        /** Calculates the total amount of correct and valid visual words for the given image region.
         *  \param[in] histogram_region information about the region where the visual words histogram is accumulated from.
         *  \param[in] histogram_parameters parameters of the visual words histogram.
         *  \param[out] valid_words number of reliable visual words in the selected region.
         *  \param[out] total_words number of visual words in the selected region.
         */
        void visualWordsCounter(const VisualWordsHistogram &histogram_region, const VisualWordsHistogramParameters &histogram_parameters, unsigned int &valid_words, unsigned int &total_words) const;
    private:
        // -[ Private functions to calculate the local histogram of visual words ]-------------------------------------------------------------------
        void extractSingleLevel(const VisualWordsHistogram &histogram_region, const VisualWordsHistogramParameters &histogram_parameters, unsigned int current_pyramid_level, double * histogram) const;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Image with the visual words at each scale.
        VectorSparse<TWEIGHT, TINDEX> * * m_visual_words;
        /// Width of the original image.
        unsigned int m_width;
        /// Height of the original image.
        unsigned int m_height;
        /// Array with the geometry of the regions at the different scales.
        Tuple<unsigned int, unsigned int> * m_scales_information;
        /// Number of scales of the local regions.
        unsigned int m_number_of_scales;
        /// Look-up table which maps the geometry of the local regions with an scale of the visual words image.
        std::map<Tuple<unsigned int, unsigned int>, unsigned int> m_scales_lut;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VISUAL WORDS IMAGE CLASS             |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>::VisualWordsImage(void) :
        m_visual_words(0),
        m_width(0),
        m_height(0),
        m_scales_information(0),
        m_number_of_scales(0) { }
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>::VisualWordsImage(unsigned int width, unsigned int height, const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales) :
        m_visual_words((number_of_scales > 0)?new VectorSparse<TWEIGHT, TINDEX>*[number_of_scales]:0),
        m_width(width),
        m_height(height),
        m_scales_information((number_of_scales > 0)?new Tuple<unsigned int, unsigned int>[number_of_scales]:0),
        m_number_of_scales(number_of_scales)
    {
        for (unsigned int i = 0; i < number_of_scales; ++i)
            m_scales_lut[scales_information[i]] = i;
        if ((unsigned int)m_scales_lut.size() != number_of_scales)
            throw Exception("There non-unique scales in the provided scales information.");
        
        for (unsigned int i = 0; i < number_of_scales; ++i)
        {
            m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[width * height];
            for (unsigned int j = 0; j < width * height; ++j)
                m_visual_words[i][j].set(0);
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>::VisualWordsImage(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, unsigned int width, unsigned int height, const unsigned int * region_size, unsigned int number_of_scales) :
        m_visual_words((number_of_scales > 0)?new VectorSparse<TWEIGHT, TINDEX> * [number_of_scales]:0),
        m_width(width),
        m_height(height),
        m_scales_information((number_of_scales > 0)?new Tuple<unsigned int, unsigned int>[number_of_scales]:0),
        m_number_of_scales(number_of_scales)
    {
        if (number_of_scales > 0)
        {
            for (unsigned int i = 0; i < number_of_scales; ++i)
            {
                m_scales_information[i].setData(region_size[i], region_size[i]);
                m_scales_lut[m_scales_information[i]] = i;
                m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[width * height];
                
                for (unsigned int j = 0; j < width * height; ++j)
                    m_visual_words[i][j].set(0);
            }
            if ((unsigned int)m_scales_lut.size() != number_of_scales)
                throw Exception("Failed to create a unique set of region geometries.");
            
            for (unsigned int i = 0; i < number_of_regions; ++i)
            {
                int x, y;
                
                x = (int)(local_regions[i].getX() + region_size[local_regions[i].getScale()] / 2);
                y = (int)(local_regions[i].getY() + region_size[local_regions[i].getScale()] / 2);
                if ((x >= 0) && (x < (int)m_width) && (y >= 0) && (y < (int)m_height))
                    m_visual_words[local_regions[i].getScale()][x + y * m_width] += visual_words[i];
            }
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>::VisualWordsImage(const VisualWordsImage<TWEIGHT, TINDEX> &other) :
        m_visual_words((other.m_visual_words != 0)?new VectorSparse<TWEIGHT, TINDEX> * [other.m_number_of_scales]:0),
        m_width(other.m_width),
        m_height(other.m_height),
        m_scales_information((other.m_scales_information != 0)?new Tuple<unsigned int, unsigned int>[other.m_number_of_scales]:0),
        m_number_of_scales(other.m_number_of_scales),
        m_scales_lut(other.m_scales_lut)
    {
        for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
        {
            m_scales_information[i] = other.m_scales_information[i];
            m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[other.m_width * other.m_height];
            for (unsigned int j = 0; j < other.m_width * other.m_height; ++j)
                m_visual_words[i][j] = other.m_visual_words[i][j];
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>::~VisualWordsImage(void)
    {
        if (m_visual_words != 0)
        {
            for (unsigned int i = 0; i < m_number_of_scales; ++i)
                delete [] m_visual_words[i];
            delete [] m_visual_words;
        }
        if (m_scales_information != 0) delete [] m_scales_information;
    }
    
    template <class TWEIGHT, class TINDEX>
    VisualWordsImage<TWEIGHT, TINDEX>& VisualWordsImage<TWEIGHT, TINDEX>::operator=(const VisualWordsImage<TWEIGHT, TINDEX> &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_visual_words != 0)
            {
                for (unsigned int i = 0; i < m_number_of_scales; ++i)
                    delete [] m_visual_words[i];
                delete [] m_visual_words;
            }
            if (m_scales_information != 0) delete [] m_scales_information;
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            m_width = other.m_width;
            m_height = other.m_height;
            m_number_of_scales = other.m_number_of_scales;
            m_scales_lut = other.m_scales_lut;
            
            if (other.m_scales_information != 0)
            {
                m_scales_information = new Tuple<unsigned int, unsigned int>[other.m_number_of_scales];
                for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
                    m_scales_information[i] = other.m_scales_information[i];
            }
            else m_scales_information = 0;
            if (other.m_visual_words != 0)
            {
                m_visual_words = new VectorSparse<TWEIGHT, TINDEX>*[other.m_number_of_scales];
                for (unsigned int i = 0; i < other.m_number_of_scales; ++i)
                {
                    m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[other.m_width * other.m_height];
                    for (unsigned int j = 0; j < other.m_width * other.m_height; ++j)
                        m_visual_words[i][j] = other.m_visual_words[i][j];
                }
            }
            else m_visual_words = 0;
        }
        
        return *this;
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::set(unsigned int width, unsigned int height, const Tuple<unsigned int, unsigned int> * scales_information, unsigned int number_of_scales)
    {
        if (m_visual_words != 0)
        {
            for (unsigned int i = 0; i < m_number_of_scales; ++i)
                delete [] m_visual_words[i];
            delete [] m_visual_words;
        }
        if (m_scales_information != 0) delete [] m_scales_information;
        
        m_scales_lut.clear();
        m_width = width;
        m_height = height;
        if (number_of_scales > 0)
        {
            m_visual_words = new VectorSparse<TWEIGHT, TINDEX> * [number_of_scales];
            m_scales_information = new Tuple<unsigned int, unsigned int>[number_of_scales];
            m_number_of_scales = number_of_scales;
            
            for (unsigned int i = 0; i < number_of_scales; ++i)
            {
                m_scales_information[i] = scales_information[i];
                m_scales_lut[scales_information[i]] = i;
                m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[width * height];
                for (unsigned int j = 0; j < width * height; ++j)
                    m_visual_words[i][j].set(0);
            }
            
            if ((unsigned int)m_scales_lut.size() != number_of_scales)
                throw Exception("There non-unique scales in the provided scales information.");
        }
        else
        {
            m_visual_words = 0;
            m_scales_information = 0;
            m_number_of_scales = 0;
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::set(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, unsigned int width, unsigned int height, const unsigned int * region_size, unsigned int number_of_scales)
    {
        if (m_visual_words != 0)
        {
            for (unsigned int i = 0; i < m_number_of_scales; ++i)
                delete [] m_visual_words[i];
            delete [] m_visual_words;
        }
        if (m_scales_information != 0) { delete [] m_scales_information; m_scales_information = 0; }
        
        m_visual_words = 0;
        m_width = width;
        m_height = height;
        m_scales_lut.clear();
        m_number_of_scales = number_of_scales;
        
        if (number_of_scales > 0)
        {
            std::map<Tuple<unsigned int, unsigned int>, unsigned int>::iterator search_iterator;
            
            m_scales_information = new Tuple<unsigned int, unsigned int>[number_of_scales];
            m_visual_words = new VectorSparse<TWEIGHT, TINDEX> * [number_of_scales];
            for (unsigned int i = 0; i < number_of_scales; ++i)
            {
                m_scales_information[i].setData(region_size[i], region_size[i]);
                m_scales_lut[m_scales_information[i]] = i;
                m_visual_words[i] = new VectorSparse<TWEIGHT, TINDEX>[width * height];
                for (unsigned int j = 0; j < width * height; ++j)
                    m_visual_words[i][j].set(0);
            }
            if ((unsigned int)m_scales_lut.size() != number_of_scales)
                throw Exception("Failed to create a unique set of region geometries.");
            
            for (unsigned int i = 0; i < number_of_regions; ++i)
            {
                int x, y;
                
                x = (int)(local_regions[i].getX() + region_size[local_regions[i].getScale()] / 2);
                y = (int)(local_regions[i].getY() + region_size[local_regions[i].getScale()] / 2);
                if ((x >= 0) && (x < (int)m_width) && (y >= 0) && (y < (int)m_height))
                    m_visual_words[local_regions[i].getScale()][x + y * m_width] += visual_words[i];
            }
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::clear(void)
    {
        for (unsigned int i = 0; i < m_number_of_scales; ++i)
            for (unsigned int j = 0; j < m_width * m_height; ++j)
                m_visual_words[i][j].set(0);
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::append(const DescriptorLocation * local_regions, const VectorSparse<TWEIGHT, TINDEX> * visual_words, unsigned int number_of_regions, const unsigned int * region_size, unsigned int number_of_scales)
    {
        if (m_number_of_scales != number_of_scales)
            throw Exception("Different number of scales of the visual words image and the given set of visual words.");
        for (unsigned int i = 0; i < number_of_regions; ++i)
        {
            int x, y;
            
            x = (int)(local_regions[i].getX() + region_size[local_regions[i].getScale()] / 2);
            y = (int)(local_regions[i].getY() + region_size[local_regions[i].getScale()] / 2);
            if ((x >= 0) && (x < (int)m_width) && (y >= 0) && (y < (int)m_height))
                m_visual_words[local_regions[i].getScale()][x + y * m_width] += visual_words[i];
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::extractRaw(VisualWordsHistogram &histogram, const VisualWordsHistogramParameters &histogram_parameters) const
    {
        VectorDense<double> visual_words_histogram(histogram_parameters.getNumberOfBins(), 0);
        
        // 1) Accumulate the visual words.
        for (unsigned int level = 0, offset = 0; level < histogram_parameters.getNumberOfPyramidLevels(); ++level)
        {
            const unsigned int partitions_x = histogram_parameters.getNumberOfDivisionsX(level);
            const unsigned int partitions_y = histogram_parameters.getNumberOfDivisionsY(level);
            double * visual_words_histogram_ptr = visual_words_histogram.getData() + histogram_parameters.getNumberOfBinsSpatial() * offset;
            
            // 1.A) Extract the histogram at the current level of the spatial pyramid.
            extractSingleLevel(histogram, histogram_parameters, level, visual_words_histogram_ptr);
            
            // 1.B) Normalize the current spatial level if needed.
            SubVectorDense<double> current_level(visual_words_histogram_ptr, histogram_parameters.getNumberOfBinsSpatial() * partitions_x * partitions_y);
            
            if (histogram_parameters.getSpatialNormalization())
            {
                if (histogram_parameters.getAreaWeighting()) current_level *= (double)(partitions_x * partitions_y);
                else
                {
                    double histogram_norm = histogram_parameters.getSpatialNorm().norm(current_level);
                    if (histogram_norm > 0.0) current_level /= histogram_norm;
                }
            }
            
            offset += partitions_x * partitions_y;
        }
        
        // 2) Save the resulting histogram in the local region sparse vector.
        histogram.getHistogram().set(visual_words_histogram);
    }
    
    template <class TWEIGHT, class TINDEX>
    double VisualWordsImage<TWEIGHT, TINDEX>::extract(VisualWordsHistogram &histogram, const VisualWordsHistogramParameters &histogram_parameters) const
    {
        VectorDense<double> visual_words_histogram(histogram_parameters.getNumberOfBins(), 0);
        double histogram_norm, histogram_accumulated_norm;
        VectorNorm l1_norm(MANHATTAN_NORM);
        
        // 1) Accumulate the visual words.
        histogram_accumulated_norm = 0;
        for (unsigned int level = 0, offset = 0; level < histogram_parameters.getNumberOfPyramidLevels(); ++level)
        {
            const unsigned int partitions_x = histogram_parameters.getNumberOfDivisionsX(level);
            const unsigned int partitions_y = histogram_parameters.getNumberOfDivisionsY(level);
            double *visual_words_histogram_ptr = visual_words_histogram.getData() + histogram_parameters.getNumberOfBinsSpatial() * offset;
            
            // 1.A) Extract the histogram at the current level of the spatial pyramid.
            extractSingleLevel(histogram, histogram_parameters, level, visual_words_histogram_ptr);
            
            // 1.B) Normalize the current spatial level if needed.
            SubVectorDense<double> current_level(visual_words_histogram_ptr, histogram_parameters.getNumberOfBinsSpatial() * partitions_x * partitions_y);
            histogram_accumulated_norm += l1_norm.norm(current_level);
            
            if (histogram_parameters.getSpatialNormalization())
            {
                if (histogram_parameters.getAreaWeighting()) current_level *= (double)(partitions_x * partitions_y);
                else
                {
                    histogram_norm = histogram_parameters.getSpatialNorm().norm(current_level);
                    if (histogram_norm > 0.0) current_level /= histogram_norm;
                }
            }
            
            offset += partitions_x * partitions_y;
        }
        
        // 2) Normalize the histogram (global normalization and post processing).
        histogram_norm = histogram_parameters.getHistogramNorm().norm(visual_words_histogram);
        if (histogram_norm > 0)
        {
            visual_words_histogram /= histogram_norm;
            
            if (histogram_parameters.getPowerFactor() < 1.0)
            {
                for (unsigned int i = 0; i < visual_words_histogram.size(); ++i)
                {
                    if (visual_words_histogram[i] >= 0.0) visual_words_histogram[i] = pow(visual_words_histogram[i], histogram_parameters.getPowerFactor());
                    else visual_words_histogram[i] = -pow(-visual_words_histogram[i], histogram_parameters.getPowerFactor());
                }
                
                histogram_norm = histogram_parameters.getHistogramNorm().norm(visual_words_histogram);
                visual_words_histogram /= histogram_norm;
            }
            
            if (histogram_parameters.getCutoffValue() < 1.0)
            {
                for (unsigned int i = 0; i < visual_words_histogram.size(); ++i)
                {
                    if (visual_words_histogram[i] >= 0.0) visual_words_histogram[i] = srvMin<double>(histogram_parameters.getCutoffValue(), visual_words_histogram[i]);
                    else visual_words_histogram[i] = -srvMin<double>(histogram_parameters.getCutoffValue(), -visual_words_histogram[i]);
                }
                
                histogram_norm = histogram_parameters.getHistogramNorm().norm(visual_words_histogram);
                visual_words_histogram /= histogram_norm;
            }
        }
        
        // 3) Save the resulting histogram in the local region sparse vector.
        histogram.getHistogram().set(visual_words_histogram);
        
        return histogram_accumulated_norm;
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::extractSingleLevel(const VisualWordsHistogram &histogram_region, const VisualWordsHistogramParameters &histogram_parameters, unsigned int current_pyramid_level, double * histogram) const
    {
        VectorDense<const VectorSparse<TWEIGHT, TINDEX> * > visual_words_ptrs(m_number_of_scales); // Pointers to the visual words images.
        const unsigned int codebook_step = histogram_parameters.getNumberOfVisualWords() * (histogram_parameters.getDifferentiateScales()?srvMax<unsigned int>(1, histogram_parameters.getNumberOfScales()):1);
        const double step_x = (double)histogram_region.getWidth() / (double)histogram_parameters.getNumberOfDivisionsX(current_pyramid_level);
        const double step_y = (double)histogram_region.getHeight() / (double)histogram_parameters.getNumberOfDivisionsY(current_pyramid_level);
        unsigned int initial_scale, final_scale;
        
        if (histogram_region.getInitialScale() >= 0) initial_scale = (unsigned int)histogram_region.getInitialScale();
        else initial_scale = 0;
        if (histogram_parameters.getNumberOfScales() > 0) final_scale = srvMin<unsigned int>((unsigned int)srvMax<int>(0, histogram_region.getInitialScale() + (int)histogram_parameters.getNumberOfScales()), m_number_of_scales);
        else final_scale = m_number_of_scales;
        
        for (int y = histogram_region.getY(), yindex = 0; yindex < histogram_region.getHeight(); ++yindex, ++y)
        {
            // Check if the current (x, y)-coordinate is still within the image boundaries.
            if (y < 0) continue;
            if ((y < (int)m_height) && (histogram_region.getX() < (int)m_width))
            {
                // Initialize the pointers to the visual words image of each scale.
                for (unsigned int scale = 0; scale < m_number_of_scales; ++scale)
                    visual_words_ptrs[scale] = &m_visual_words[scale][y * m_width + (unsigned int)srvMax<int>(0, histogram_region.getX())]; // Initialize the pointers to the visual words images.
                
                for (int x = histogram_region.getX(), xindex = 0; xindex < histogram_region.getWidth(); ++xindex, ++x)
                {
                    // Check if the current x-coordinate is still within the image boundaries.
                    if (x < 0) continue;
                    if (x < (int)m_width)
                    {
                        // UPDATE THE HISTOGRAM VALUES FOR THE CURRENT NUMBER OF PARTITIONS ---------------------------------------------------------
                        if (histogram_parameters.getInterpolation())
                        {
                            unsigned int spatial_offset_x0, spatial_offset_x1, spatial_offset_y0, spatial_offset_y1;
                            double spatial_weight_x0, spatial_weight_x1, spatial_weight_y0, spatial_weight_y1;
                            
                            const double real_x = (double)xindex / step_x - 0.5;
                            const double real_y = (double)yindex / step_y - 0.5;
                            
                            if (real_x <= 0.0)
                            {
                                spatial_offset_x0 = 0;
                                spatial_weight_x0 = 1.0;
                                spatial_offset_x1 = 0;
                                spatial_weight_x1 = 0.0;
                            }
                            else if (real_x >= (double)histogram_parameters.getNumberOfDivisionsX(current_pyramid_level) - 1.0)
                            {
                                spatial_offset_x0 = histogram_parameters.getNumberOfDivisionsX(current_pyramid_level) - 1;
                                spatial_weight_x0 = 1.0;
                                spatial_offset_x1 = histogram_parameters.getNumberOfDivisionsX(current_pyramid_level) - 1;
                                spatial_weight_x1 = 0.0;
                            }
                            else
                            {
                                spatial_offset_x0 = (unsigned int)real_x;
                                spatial_weight_x0 = 1.0 - (real_x - floor(real_x));
                                spatial_offset_x1 = (unsigned int)real_x + 1;
                                spatial_weight_x1 = 1.0 - spatial_weight_x0;
                            }
                            
                            if (real_y <= 0.0)
                            {
                                spatial_offset_y0 = 0;
                                spatial_weight_y0 = 1.0;
                                spatial_offset_y1 = 0;
                                spatial_weight_y1 = 0.0;
                            }
                            else if (real_y >= (double)histogram_parameters.getNumberOfDivisionsY(current_pyramid_level) - 1.0)
                            {
                                spatial_offset_y0 = histogram_parameters.getNumberOfDivisionsY(current_pyramid_level) - 1;
                                spatial_weight_y0 = 1.0;
                                spatial_offset_y1 = histogram_parameters.getNumberOfDivisionsY(current_pyramid_level) - 1;
                                spatial_weight_y1 = 0.0;
                            }
                            else
                            {
                                spatial_offset_y0 = (unsigned int)real_y;
                                spatial_weight_y0 = 1.0 - (real_y - floor(real_y));
                                spatial_offset_y1 = (unsigned int)real_y + 1;
                                spatial_weight_y1 = 1.0 - spatial_weight_y0;
                            }
                            
                            const unsigned int spatial_offset00 = (spatial_offset_x0 + spatial_offset_y0 * histogram_parameters.getNumberOfDivisionsX(current_pyramid_level)) * codebook_step;
                            const unsigned int spatial_offset10 = (spatial_offset_x0 + spatial_offset_y1 * histogram_parameters.getNumberOfDivisionsX(current_pyramid_level)) * codebook_step;
                            const unsigned int spatial_offset01 = (spatial_offset_x1 + spatial_offset_y0 * histogram_parameters.getNumberOfDivisionsX(current_pyramid_level)) * codebook_step;
                            const unsigned int spatial_offset11 = (spatial_offset_x1 + spatial_offset_y1 * histogram_parameters.getNumberOfDivisionsX(current_pyramid_level)) * codebook_step;
                            const double spatial_weight00 = spatial_weight_x0 * spatial_weight_y0;
                            const double spatial_weight10 = spatial_weight_x0 * spatial_weight_y1;
                            const double spatial_weight01 = spatial_weight_x1 * spatial_weight_y0;
                            const double spatial_weight11 = spatial_weight_x1 * spatial_weight_y1;
                            
                            if (histogram_parameters.getDifferentiateScales())
                            {
                                unsigned int scale_index;
                                if (histogram_region.getInitialScale() < 0) scale_index = -histogram_region.getInitialScale();
                                else scale_index = 0;
                                for (unsigned int scale = initial_scale; scale < final_scale; ++scale, ++scale_index)
                                {
                                    const unsigned int scale_offset00 = spatial_offset00 + scale_index * histogram_parameters.getNumberOfVisualWords();
                                    const unsigned int scale_offset10 = spatial_offset10 + scale_index * histogram_parameters.getNumberOfVisualWords();
                                    const unsigned int scale_offset01 = spatial_offset01 + scale_index * histogram_parameters.getNumberOfVisualWords();
                                    const unsigned int scale_offset11 = spatial_offset11 + scale_index * histogram_parameters.getNumberOfVisualWords();
                                    
                                    for (unsigned int index = 0; index < visual_words_ptrs[scale]->size(); ++index)
                                    {
                                        // Checks if unreliable words are accumulated or the index of the word is lower than the greatest codeword of the codebook (i.e.\ is a reliable codeword).
                                        if (histogram_parameters.getUnreliable() || (visual_words_ptrs[scale]->getIndex(index) < histogram_parameters.getNumberOfVisualWords()))
                                        {
                                            double bin_weight00, bin_weight01, bin_weight10, bin_weight11;
                                            TINDEX bin_index00, bin_index01, bin_index10, bin_index11;
                                            
                                            bin_index00 = visual_words_ptrs[scale]->getIndex(index) + scale_offset00;
                                            bin_index10 = visual_words_ptrs[scale]->getIndex(index) + scale_offset10;
                                            bin_index01 = visual_words_ptrs[scale]->getIndex(index) + scale_offset01;
                                            bin_index11 = visual_words_ptrs[scale]->getIndex(index) + scale_offset11;
                                            bin_weight00 = spatial_weight00 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight10 = spatial_weight10 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight01 = spatial_weight01 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight11 = spatial_weight11 * (double)visual_words_ptrs[scale]->getValue(index);
                                            if (histogram_parameters.getPolarity())
                                            {
                                                bin_index00 *= 2; // Separate positive from negative contributions:
                                                bin_index10 *= 2; // the number of visual words of the histogram is
                                                bin_index01 *= 2; // doubled.
                                                bin_index11 *= 2;
                                                if (visual_words_ptrs[scale]->getValue(index) >= 0)
                                                {
                                                    bin_index00 += 1; // Positively weighted visual words are displaced
                                                    bin_index10 += 1; // a position.
                                                    bin_index01 += 1;
                                                    bin_index11 += 1;
                                                }
                                                else
                                                {
                                                    bin_weight00 = -bin_weight00; // The sign of the negatively weighted
                                                    bin_weight10 = -bin_weight10; // visual words is changed back to
                                                    bin_weight01 = -bin_weight01; // positive.
                                                    bin_weight11 = -bin_weight11;
                                                }
                                            }
                                            
                                            if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_SUM)
                                            {
                                                histogram[bin_index00] += bin_weight00;
                                                histogram[bin_index10] += bin_weight10;
                                                histogram[bin_index01] += bin_weight01;
                                                histogram[bin_index11] += bin_weight11;
                                            }
                                            else if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_MAX)
                                            {
                                                histogram[bin_index00] = srvMax<double>(histogram[bin_index00], bin_weight00);
                                                histogram[bin_index10] = srvMax<double>(histogram[bin_index10], bin_weight10);
                                                histogram[bin_index01] = srvMax<double>(histogram[bin_index01], bin_weight01);
                                                histogram[bin_index11] = srvMax<double>(histogram[bin_index11], bin_weight11);
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (unsigned int scale = initial_scale; scale < final_scale; ++scale)
                                {
                                    for (unsigned int index = 0; index < visual_words_ptrs[scale]->size(); ++index)
                                    {
                                        // Checks if unreliable words are accumulated or the index of the word is lower than the greatest codeword of the codebook (i.e.\ is a reliable codeword).
                                        if (histogram_parameters.getUnreliable() || (visual_words_ptrs[scale]->getIndex(index) < histogram_parameters.getNumberOfVisualWords()))
                                        {
                                            double bin_weight00, bin_weight01, bin_weight10, bin_weight11;
                                            TINDEX bin_index00, bin_index01, bin_index10, bin_index11;
                                            
                                            bin_index00 = visual_words_ptrs[scale]->getIndex(index) + spatial_offset00;
                                            bin_index10 = visual_words_ptrs[scale]->getIndex(index) + spatial_offset10;
                                            bin_index01 = visual_words_ptrs[scale]->getIndex(index) + spatial_offset01;
                                            bin_index11 = visual_words_ptrs[scale]->getIndex(index) + spatial_offset11;
                                            bin_weight00 = spatial_weight00 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight10 = spatial_weight10 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight01 = spatial_weight01 * (double)visual_words_ptrs[scale]->getValue(index);
                                            bin_weight11 = spatial_weight11 * (double)visual_words_ptrs[scale]->getValue(index);
                                            if (histogram_parameters.getPolarity())
                                            {
                                                bin_index00 *= 2; // Separate positive from negative contributions:
                                                bin_index10 *= 2; // the number of visual words of the histogram is
                                                bin_index01 *= 2; // doubled.
                                                bin_index11 *= 2;
                                                if (visual_words_ptrs[scale]->getValue(index) >= 0)
                                                {
                                                    bin_index00 += 1; // Positively weighted visual words are displaced
                                                    bin_index10 += 1; // a position.
                                                    bin_index01 += 1;
                                                    bin_index11 += 1;
                                                }
                                                else
                                                {
                                                    bin_weight00 = -bin_weight00; // The sign of the negatively weighted
                                                    bin_weight10 = -bin_weight10; // visual words is changed back to
                                                    bin_weight01 = -bin_weight01; // positive.
                                                    bin_weight11 = -bin_weight11;
                                                }
                                            }
                                            
                                            if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_SUM)
                                            {
                                                histogram[bin_index00] += bin_weight00;
                                                histogram[bin_index10] += bin_weight10;
                                                histogram[bin_index01] += bin_weight01;
                                                histogram[bin_index11] += bin_weight11;
                                            }
                                            else if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_MAX)
                                            {
                                                histogram[bin_index00] = srvMax<double>(histogram[bin_index00], bin_weight00);
                                                histogram[bin_index10] = srvMax<double>(histogram[bin_index10], bin_weight10);
                                                histogram[bin_index01] = srvMax<double>(histogram[bin_index01], bin_weight01);
                                                histogram[bin_index11] = srvMax<double>(histogram[bin_index11], bin_weight11);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            const unsigned int spatial_offset = ((unsigned int)((double)xindex / step_x) + (unsigned int)((double)yindex / step_y) * histogram_parameters.getNumberOfDivisionsX(current_pyramid_level)) * codebook_step;
                            
                            if (histogram_parameters.getDifferentiateScales())
                            {
                                unsigned int scale_index;
                                
                                if (histogram_region.getInitialScale() < 0) scale_index = -histogram_region.getInitialScale();
                                else scale_index = 0;
                                for (unsigned int scale = initial_scale; scale < final_scale; ++scale, ++scale_index)
                                {
                                    const unsigned int scale_offset = spatial_offset + scale_index * histogram_parameters.getNumberOfVisualWords();
                                    for (unsigned int index = 0; index < visual_words_ptrs[scale]->size(); ++index)
                                    {
                                        // Checks if unreliable words are accumulated or the index of the word is lower than the greatest codeword of the codebook (i.e.\ is a reliable codeword).
                                        if (histogram_parameters.getUnreliable() || (visual_words_ptrs[scale]->getIndex(index) < histogram_parameters.getNumberOfVisualWords()))
                                        {
                                            TINDEX bin_index = visual_words_ptrs[scale]->getIndex(index) + scale_offset;
                                            double bin_weight = visual_words_ptrs[scale]->getValue(index);
                                            
                                            if (histogram_parameters.getPolarity())
                                            {
                                                bin_index *= 2;
                                                if (visual_words_ptrs[scale]->getValue(index) >= 0) ++bin_index;
                                                else bin_weight = -bin_weight;
                                            }
                                            
                                            if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_SUM)
                                                histogram[bin_index] += bin_weight;
                                            else if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_MAX)
                                                histogram[bin_index] = srvMax<double>(histogram[bin_index], bin_weight);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                for (unsigned int scale = initial_scale; scale < final_scale; ++scale)
                                {
                                    for (unsigned int index = 0; index < visual_words_ptrs[scale]->size(); ++index)
                                    {
                                        // Checks if unreliable words are accumulated or the index of the word is lower than the greatest codeword of the codebook (i.e.\ is a reliable codeword).
                                        if (histogram_parameters.getUnreliable() || (visual_words_ptrs[scale]->getIndex(index) < histogram_parameters.getNumberOfVisualWords()))
                                        {
                                            TINDEX bin_index = visual_words_ptrs[scale]->getIndex(index) + spatial_offset;
                                            double bin_weight = visual_words_ptrs[scale]->getValue(index);
                                            
                                            if (histogram_parameters.getPolarity())
                                            {
                                                bin_index *= 2;
                                                if (visual_words_ptrs[scale]->getValue(index) >= 0) ++bin_index;
                                                else bin_weight = -bin_weight;
                                            }
                                            
                                            if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_SUM)
                                                histogram[bin_index] += bin_weight;
                                            else if (histogram_parameters.getPoolingMethod() == BOVW_POOLING_MAX)
                                                histogram[bin_index] = srvMax<double>(histogram[bin_index], bin_weight);
                                        }
                                    }
                                }
                            }
                        }
                        
                        
                        // --------------------------------------------------------------------------------------------------------------------------
                        
                        for (unsigned int scale = 0; scale < m_number_of_scales; ++scale) // Move to the next location of the visual words image.
                            ++visual_words_ptrs[scale];
                    }
                    else break;
                }
            }
            else break;
        }
    }
    
    template <class TWEIGHT, class TINDEX>
    void VisualWordsImage<TWEIGHT, TINDEX>::visualWordsCounter(const VisualWordsHistogram &histogram_region, const VisualWordsHistogramParameters &histogram_parameters, unsigned int &valid_words, unsigned int &total_words) const
    {
        VectorDense<const VectorSparse<TWEIGHT, TINDEX> * > visual_words_ptrs(m_number_of_scales); // Pointers to the visual words images.
        unsigned int initial_scale, final_scale;
        
        if (histogram_region.getInitialScale() >= 0) initial_scale = (unsigned int)histogram_region.getInitialScale();
        else initial_scale = 0;
        if (histogram_parameters.getNumberOfScales() > 0) final_scale = srvMin<unsigned int>((unsigned int)srvMax<int>(0, histogram_region.getInitialScale() + (int)histogram_parameters.getNumberOfScales()), m_number_of_scales);
        else final_scale = m_number_of_scales;
        
        valid_words = 0;
        total_words = 0;
        for (int y = histogram_region.getY(), yindex = 0; yindex < histogram_region.getHeight(); ++yindex, ++y)
        {
            // Check if the current (x, y)-coordinate is still within the image boundaries.
            if (y < 0) continue;
            if ((y < (int)m_height) && (histogram_region.getX() < (int)m_width))
            {
                // Initialize the pointers to the visual words image of each scale.
                for (unsigned int scale = 0; scale < m_number_of_scales; ++scale)
                    visual_words_ptrs[scale] = &m_visual_words[scale][y * m_width + (unsigned int)srvMax<int>(0, histogram_region.getX())]; // Initialize the pointers to the visual words images.
                
                for (int x = histogram_region.getX(), xindex = 0; xindex < histogram_region.getWidth(); ++xindex, ++x)
                {
                    // Check if the current x-coordinate is still within the image boundaries.
                    if (x < 0) continue;
                    if (x < (int)m_width)
                    {
                        for (unsigned int scale = initial_scale; scale < final_scale; ++scale)
                        {
                            total_words += visual_words_ptrs[scale]->size();
                            for (unsigned int index = 0; index < visual_words_ptrs[scale]->size(); ++index)
                            {
                                if (visual_words_ptrs[scale]->getIndex(index) < histogram_parameters.getNumberOfVisualWords())
                                    ++valid_words;
                            }
                        }
                        
                        for (unsigned int scale = 0; scale < m_number_of_scales; ++scale) // Move to the next location of the visual words image.
                            ++visual_words_ptrs[scale];
                    }
                    else break;
                }
            }
            else break;
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

