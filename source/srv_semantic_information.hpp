// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA


#ifndef __SRV_SEMANTIC_INFORMATION_HPP_HEADER_FILE__
#define __SRV_SEMANTIC_INFORMATION_HPP_HEADER_FILE__

// -[ SRV header files ]-------------------------------
#include "srv_utilities.hpp"
#include "srv_xml.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PART INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Structure which contains information corresponding to a part object/semantic category.
    class PartInformation
    {
    public:
        // Constructors, destructor and assignation operator ..........................................
        /// Default constructor.
        PartInformation(void);
        /// Parameter constructor which the identifier and color code of the category part are given as parameters.
        PartInformation(const char *identifier, unsigned char red, unsigned char green, unsigned char blue);
        /// Copy constructor.
        PartInformation(const PartInformation &copy);
        /// Destructor.
        ~PartInformation(void);
        /// Assignation operator.
        PartInformation& operator=(const PartInformation &copy);
        
        // Get/set part color code ....................................................................
        /// Returns the red color code of the category part.
        inline unsigned char getRed(void) const {return m_red;}
        /// Returns the green color code of the category part.
        inline unsigned char getGreen(void) const {return m_green;}
        /// Returns the blue color code of the category part.
        inline unsigned char getBlue(void) const {return m_blue;}
        /// Sets the red color code of the category part.
        inline void setRed(unsigned char red) {m_red = red;}
        /// Sets the green color code of the category part.
        inline void setGreen(unsigned char green) {m_green = green;}
        /// Sets the blue color code of the category part.
        inline void setBlue(unsigned char blue) {m_blue = blue;}
        /// Sets the (red, green, blue) color code of the category part.
        inline void setColorCode(unsigned char red, unsigned char green, unsigned char blue) {m_red = red; m_green = green; m_blue = blue;}
        /// Returns true if the given (red, green, blue) color code corresponds to the color code of the category part; returns false otherwise.
        inline bool isSameRGB(unsigned char red, unsigned char green, unsigned char blue) const {return (m_red == red) && (m_blue == blue) && (m_green == green);}
        /// Returns true if the given (blue, green, red) color code corresponds to the color code of the category part; returns false otherwise.
        inline bool isSameBGR(unsigned char blue, unsigned char green, unsigned char red) const {return (m_red == red) && (m_blue == blue) && (m_green == green);}
        
        // Get/set the category part identifier .......................................................
        /// Returns a constant array to the identifier of the category part.
        inline const char* getIdentifier(void) const {return m_identifier;}
        /// Sets the array string which stores the identifier of the category part.
        inline void setIdentifier(const char *identifier)
        {
            unsigned int index;
            for (index = 0; index < 63 && identifier[index] != '\0'; ++index)
                m_identifier[index] = identifier[index];
            m_identifier[index] = '\0';
        }
        
        // XML methods ................................................................................
        /// Converts the part information object to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the part information object from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Text identifier of the semantic part.
        char m_identifier[64];
        /// Red color of the semantic part color code.
        unsigned char m_red;
        /// Green color of the semantic part color code.
        unsigned char m_green;
        /// Blue color of the semantic part color code.
        unsigned char m_blue;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CATEGORY INFORMATION CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Structure which stores the information corresponding to an object/semantic category: class identifier, class color code and an array with the different parts which define the catogory.
    class CategoryInformation
    {
    public:
        // Constructors, destructor and copy operator .................................................
        /// Default constructor.
        CategoryInformation(void);
        /// Parameters constructor where the identifier, color and parts which form the category are specified.
        CategoryInformation(const char *identifier, unsigned char red, unsigned char green, unsigned char blue, unsigned int number_of_parts, const PartInformation *parts);
        /// Copy constructor.
        CategoryInformation(const CategoryInformation &copy);
        /// Destructor.
        ~CategoryInformation(void);
        /// Assignation operator.
        CategoryInformation& operator=(const CategoryInformation &copy);
        
        // Set/get the color code of the category .....................................................
        /// Returns the red color code of the category.
        inline unsigned char getRed(void) const {return m_red;}
        /// Returns the green color code of the category.
        inline unsigned char getGreen(void) const {return m_green;}
        /// Returns the blue color code of the category.
        inline unsigned char getBlue(void) const {return m_blue;}
        /// Sets the red color code of the category.
        inline void setRed(unsigned char red) {m_red = red;}
        /// Sets the green color code of the category.
        inline void setGreen(unsigned char green) {m_green = green;}
        /// Sets the blue color code of the category.
        inline void setBlue(unsigned char blue) {m_blue = blue;}
        /// Sets the (red, green, blue) color code of the category.
        inline void setColorCode(unsigned char red, unsigned char green, unsigned char blue) {m_red = red; m_green = green; m_blue = blue;}
        /// Returns true if the given (red, green, blue) color code corresponds to the class identifier; false otherwise.
        inline bool isSameRGB(unsigned char red, unsigned char green, unsigned char blue) const {return (m_red == red) && (m_blue == blue) && (m_green == green);}
        /// Returns true if the given (blue, green, red) color code corresponds to the class identifier; false otherwise.
        inline bool isSameBGR(unsigned char blue, unsigned char green, unsigned char red) const {return (m_red == red) && (m_blue == blue) && (m_green == green);}
        
        // Set/get the category identifier.
        /// Returns a constant pointer to the array containing the string of the category identifier.
        inline const char* getIdentifier(void) const {return m_identifier;}
        /// Sets the identifier of the category.
        inline void setIdentifier(const char *identifier)
        {
            unsigned int index;
            for (index = 0; (index < 63) && (identifier[index] != '\0'); ++index) m_identifier[index] = identifier[index];
            m_identifier[index] = '\0';
        }
        
        // Set/get category parts information .........................................................
        /// Returns the number of parts that form the category.
        inline unsigned int getNumberOfParts(void) const {return m_number_of_parts;}
        /// Returns a constant reference to the 'index'-th part of the category.
        inline const PartInformation& getParts(unsigned int index) const {return m_parts[index];}
        /// Returns a reference to the 'index'-th part of the category.
        inline PartInformation& getParts(unsigned int index) {return m_parts[index];}
        /// Returns a constant pointer to the parts of the category.
        inline const PartInformation* getParts(void) const {return m_parts;}
        /// Returns a pointer to the parts of the category.
        inline PartInformation* getParts(void) {return m_parts;}
        /// Sets the number of parts of the category definition.
        void setNumberOfParts(unsigned int number_of_parts);
        
        // XML methods ................................................................................
        /// Converts the category information object to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the category information object from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Array to the different parts which form the category definition.
        PartInformation *m_parts;
        /// Number of parts which forms the category definition.
        unsigned int m_number_of_parts;
        /// Char string containing the identifier of the category.
        char m_identifier[64];
        /// Red color code corresponding to the category.
        unsigned char m_red;
        /// Green color code corresponding to the category.
        unsigned char m_green;
        /// Blue color code corresponding to the category.
        unsigned char m_blue;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CATEGORIES INFORMATION CLASS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container class which stores the object information corresponding to a set of (database) categories.
    class CategoriesInformation
    {
    public:
        // Constructors, destructor and copy operator .................................................
        /// Default constructor.
        CategoriesInformation(void);
        /// Parameters constructor where background and border color codes and the categories information is passed as parameters.
        CategoriesInformation(unsigned char background_red, unsigned char background_green, unsigned char background_blue, unsigned char border_red, unsigned char border_green, unsigned char border_blue, unsigned int number_of_categories, const CategoryInformation *categories);
        /// Copy constructor.
        CategoriesInformation(const CategoriesInformation &copy);
        /// Destructor.
        ~CategoriesInformation(void);
        /// Assignation operator.
        CategoriesInformation& operator=(const CategoriesInformation &copy);
        
        // Set/get background and border color codes ......................................................
        /// Returns the background red color code.
        inline unsigned char getBackgroundRed(void) const {return m_background_red;}
        /// Returns the background green color code.
        inline unsigned char getBackgroundGreen(void) const {return m_background_green;}
        /// Returns the background blue color code.
        inline unsigned char getBackgroundBlue(void) const {return m_background_blue;}
        /// Returns the border red color code.
        inline unsigned char getBorderRed(void) const {return m_border_red;}
        /// Returns the border green color code.
        inline unsigned char getBorderGreen(void) const {return m_border_green;}
        /// Returns the border blue color code.
        inline unsigned char getBorderBlue(void) const {return m_border_blue;}
        /// Sets the background red color code.
        inline void setBackgroundRed(unsigned char background_red) {m_background_red = background_red;}
        /// Sets the background green color code.
        inline void setBackgroundGreen(unsigned char background_green) {m_background_green = background_green;}
        /// Sets the background blue color code.
        inline void setBackgroundBlue(unsigned char background_blue) {m_background_blue = background_blue;}
        /// Sets the background (red, green, blue) color code.
        inline void setBackgroundColor(unsigned char background_red, unsigned char background_green, unsigned char background_blue) {m_background_red = background_red; m_background_green = background_green, m_background_blue = background_blue;}
        /// Sets the border red color code.
        inline void setBorderRed(unsigned char border_red) {m_border_red = border_red;}
        /// Sets the border green color code.
        inline void setBorderGreen(unsigned char border_green) {m_border_green = border_green;}
        /// Sets the border blue color code.
        inline void setBorderBlue(unsigned char border_blue) {m_border_blue = border_blue;}
        /// Sets the border (red, green, blue) color code.
        inline void setBorderColor(unsigned char border_red, unsigned char border_green, unsigned char border_blue) {m_border_red = border_red; m_border_green = border_green; m_border_blue = border_blue;}
        
        // Retrieve categories information ............................................................
        /// Returns the number of categories
        inline unsigned int getNumberOfCategories(void) const {return m_number_of_categories;}
        /// Returns the total number of subparts contained by all categories.
        inline unsigned int getNumberOfSubcategories(void) const {return m_number_of_subcategories;}
        /// Returns a constant pointer to the categories array.
        inline const CategoryInformation* getCategory(void) const {return m_categories;}
        /// Returns a constant reference to a certain category.
        inline const CategoryInformation& getCategory(unsigned int index) const {return m_categories[index];}
        /// Returns a reference to a certain category.
        inline CategoryInformation& getCategory(unsigned int index) {return m_categories[index];}
        /// Returns a pointers to the parts of a certain category.
        inline const PartInformation* getSubcategory(unsigned int index) const {return m_part_access[index];}
        /// Returns the part information with the given subcategory index
        inline const CategoryInformation* getSubcategoryCategory(unsigned int index) const {return m_categories_access[index];}
        /// Returns the subcategory index given the category identifier and the sub-category identifier.
        int getSubcategoryIndex(const char *category_identifier, const char *subcategory_identifier) const;
        /// Returns the subcategory index given its corresponding (red, green, blue) code.
        int getSubcategoryIndexRGB(unsigned char red, unsigned char green, unsigned char blue) const;
        /// Returns the subcategory index given its corresponding (blue, green, red) code.
        int getSubcategoryIndexBGR(unsigned char blue, unsigned char green, unsigned char red) const;
        /// Returns the subcategory index given a pointer to its corresponding (red, green, blue) code.
        int getSubcategoryIndexRGB(unsigned char *ptr) const;
        /// Returns the subcategory index given a pointer to its corresponding (blue, green, red) code.
        int getSubcategoryIndexBGR(unsigned char *ptr) const;
        /// Adds to the current CategoriesInformation object, the new categories and part definitions of a different categories information object.
        bool merge(const CategoriesInformation &other);
        /** Sets the categories information of the object.
         *  \param[in] categories array with the new categories information.
         *  \param[in] number_of_categories number of elements in the array.
         */
        void setCategoriesInformation(const CategoryInformation * categories, unsigned int number_of_categories);
        
        // XML methods ................................................................................
        /// Converts the categories information to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the categories information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Array storing the information of the different categories.
        CategoryInformation *m_categories;
        /// Number of categories.
        unsigned int m_number_of_categories;
        /// Total number of subcategories stored in all categories.
        unsigned int m_number_of_subcategories;
        /// Array of pointers to the categories information structures (where the index corresponds to the subcategory index).
        CategoryInformation **m_categories_access;
        /// Array of pointers to all the subcategories information structures (where the index corresponds to the subcategory index).
        PartInformation **m_part_access;
        /// Background red color code.
        unsigned char m_background_red;
        /// Background green color code.
        unsigned char m_background_green;
        /// Background blue color code.
        unsigned char m_background_blue;
        /// Border red color code.
        unsigned char m_border_red;
        /// Border green color code.
        unsigned char m_border_green;
        /// Border blue color code.
        unsigned char m_border_blue;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BOUNDING BOX CLASS                   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// This class stores the information of a bounding box which marks the location of an object within an image. It stores the location of the bounding box, its category identifier and object detection parameters (pose, difficult and truncated).
    class BoundingBox
    {
    public:
        // Constructors, destructor and assignation operator ..........................................
        /// Default constructor.
        BoundingBox(void);
        /// Parameters constructor where the bounding box information is given as parameters (coordinates, category information and attributes).
        BoundingBox(short xmin, short ymin, short xmax, short ymax, const char *pose, const char *category, const char *part, bool difficult, bool truncated, const CategoriesInformation *categories_information);
        /// Copy constructor.
        BoundingBox(const BoundingBox &copy);
        /// Destructor.
        ~BoundingBox(void) {}
        /// Assignation operator.
        BoundingBox& operator=(const BoundingBox &copy);
        
        // Get/set the bounding box coordinates .......................................................
        /// Returns the minimum X coordinate of the bounding box.
        inline int getXMin(void) const {return m_xmin;}
        /// Returns the minimum Y coordinate of the bounding box.
        inline int getYMin(void) const {return m_ymin;}
        /// Returns the maximum X coordinate of the bounding box.
        inline int getXMax(void) const {return m_xmax;}
        /// Returns the maximum Y coordinate of the bounding box.
        inline int getYMax(void) const {return m_ymax;}
        /// Sets the minimum X coordinate of the bounding box.
        inline void setXMin(short xmin) {m_xmin = xmin;}
        /// Sets the minimum Y coordinate of the bounding box.
        inline void setYMin(short ymin) {m_ymin = ymin;}
        /// Sets the maximum X coordinate of the bounding box.
        inline void setXMax(short xmax) {m_xmax = xmax;}
        /// Sets the maximum Y coordinate of the bounding box.
        inline void setYMax(short ymax) {m_ymax = ymax;}
        /// Returns true if the given coordinates are inside the bounding box, false otherwise.
        inline bool isInside(int x, int y) const {return (x >= m_xmin) && (y >= m_ymin) && (x <= m_xmax) && (y <= m_ymax);}
        
        // Get/set object category and label ..........................................................
        /// Returns the category identifier of the bounding box.
        inline const char* getCategory(void) const {return m_category;}
        /// Returns the category part identifier of the bounding box.
        inline const char* getPart(void) const {return m_part;}
        /// Sets the category and part identifier of the bounding box.
        void setCategoryPart(const char *category, const char *part)
        {
            unsigned int index;
            for (index = 0; (index < 63) && (category[index] != '\0'); ++index)
                m_category[index] = category[index];
            m_category[index] = '\0';
            for (index = 0; (index < 63) && (part[index] != '\0'); ++index)
                m_part[index] = part[index];
            m_part[index] = '\0';
        }
        /// Returns the category part index (the bounding box label) of the bounding box corresponding to its associated categories information data (CategoriesInformation object).
        inline int getLabel(void) const {return m_label;}
        /// Updates the label index corresponding to the category-part identifier of the bounding box corresponding to the given categories information object (CategoriesInformation object).
        inline void updateLabel(const CategoriesInformation *categories_information) {m_label = categories_information->getSubcategoryIndex(m_category, m_part);}
        
        // Get/set object attributes ..................................................................
        /// Returns a boolean which indicates if the bounding box object has been marked as difficult or not.
        inline bool isDifficult(void) const {return m_difficult;}
        /// Returns a boolean which indicates if the bounding box object has been marked as truncated (not completely seen, occluded) or not.
        inline bool isTruncated(void) const {return m_truncated;}
        /// Sets the value of the difficult flag of the bounding box.
        inline void setDifficult(bool value) {m_difficult = value;}
        /// Sets the value of the truncated flag of the bounding box.
        inline void setTruncated(bool value) {m_truncated = value;}
        /// Returns a char string which contains the pose identifier given to the bounding box object.
        inline const char* getPose(void) const {return m_pose;}
        /// Sets the pose char string identifier of the bounding box.
        inline void setPose(const char *pose)
        {
            unsigned int index;
            for (index = 0; (index < 63) && (pose[index] != '\0'); ++index) m_pose[index] = pose[index];
            m_pose[index] = '\0';
        }
        
        // XML methods ................................................................................
        /// Converts the bounding box object to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the bounding box object from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// X minimum coordinate of the bounding box.
        short m_xmin;
        /// Y minimum coordinate of the bounding box.
        short m_ymin;
        /// X maximum coordinate of the bounding box.
        short m_xmax;
        /// Y maximum coordinate of the bounding box.
        short m_ymax;
        /// Index of the bounding box category-part identifier in the categories information object (CategoriesInformation object) associated with the bounding box.
        int m_label;
        /// Char string array containing the pose information of the object (front view, lateral view, top view, ...).
        char m_pose[64];
        /// Char string array identifier of the category of the bounding box.
        char m_category[64];
        /// Char string array identifier of the category's part of the bounding box.
        char m_part[64];
        /// Boolean flag indicating if the object is difficult to detect/classify or not.
        bool m_difficult;
        /// Boolean flag indicating if the object is truncated (occluded) or not.
        bool m_truncated;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GLOBAL CATEGORY CLASS                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Semantic information for global categories image information.
    class GlobalCategory
    {
    public:
        // Constructors, destructor and assignation operator ..........................................
        /// Default constructor.
        GlobalCategory(void);
        /// Parameters constructor where the category and part identifiers are given as parameters together with an (optional) categories information object used to calculate the label index of the assigned category-part information to the global category object.
        GlobalCategory(const char * category, const char * part, bool difficult, const CategoriesInformation * categories_information = 0);
        /// Copy constructor.
        GlobalCategory(const GlobalCategory &copy);
        /// Destructor.
        ~GlobalCategory(void);
        /// Assignation operator.
        GlobalCategory& operator=(const GlobalCategory &copy);
        
        // Get/set category and label .................................................................
        /// Returns the category identifier of the global category object.
        inline const char* getCategory(void) const {return m_category;}
        /// Returns the category part identifier of the global category object.
        inline const char* getPart(void) const {return m_part;}
        /// Sets the category and part identifier of the global category object.
        inline void setCategoryPart(const char *category, const char *part)
        {
            unsigned int index;
            for (index = 0; (index < 63) && (category[index] != '\0'); ++index)
                m_category[index] = category[index];
            m_category[index] = '\0';
            for (index = 0; (index < 63) && (part[index] != '\0'); ++index)
                m_part[index] = part[index];
            m_part[index] = '\0';
        }
        /// Returns the category part index (the global category object label) of the global category object corresponding to its associated categories information data (CategoriesInformation object).
        inline int getLabel(void) const {return m_label;}
        /// Updates the label index corresponding to the category-part identifier of the global category object corresponding to the given categories information object (CategoriesInformation object).
        inline void updateLabel(const CategoriesInformation *categories_information) {m_label = categories_information->getSubcategoryIndex(m_category, m_part);}
        /// Returns a boolean flag which indicates when the global category is difficult to detect.
        inline bool getDifficult(void) const { return m_difficult; }
        /// Sets the boolean flag which indicates if the global category is difficult to detect.
        inline void setDifficult(bool difficult) { m_difficult = difficult; }
        
        // XML methods ................................................................................
        /// Converts the global information object to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the global information object from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Char string array identifier of the global category.
        char m_category[64];
        /// Char string array identifier of the global category part.
        char m_part[64];
        /// Index of the global category-part identifier in the categories information object (CategoriesInformation object) associated with the bounding box.
        int m_label;
        /// Bolean flag which indicates that the global category is difficult to detect.
        bool m_difficult;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

