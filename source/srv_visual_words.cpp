// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_visual_words.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | DECLARATION OF THE AUXILIARY         |
    //                   | FUNCTIONS FOR THE HISTOGRAM OF       |
    //                   | VISUAL WORDS PARAMETERS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    std::ostream& operator<<(std::ostream &out, const VisualWordsHistogramParameters &object)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Parameters of a Histogram of Visual Words ]" << std::endl;
        tab.increase(out);
        out << tab << " · Number of visual words: " << object.getNumberOfVisualWords() << std::endl;
        out << tab << " · Number of visual words generated per descriptor: " << object.getNumberOfWordsDescriptor() << std::endl;
        out << tab << " · Number of spatial bins of the histogram: " << object.getNumberOfSpatialBins() << std::endl;
        out << tab << " · Total number of bins of the histogram (dimensionality): " << object.getNumberOfBins() << std::endl;
        out << tab << " · Number of spatial bins: " << object.getNumberOfBinsSpatial() << std::endl;
        out << tab << " · Number of pyramid levels: " << object.getNumberOfPyramidLevels() << std::endl;
        out << tab << " · Initial number of partitions in the X direction: " << object.getInitialXDegree() << std::endl;
        out << tab << " · Initial number of partitions in the Y direction: " << object.getInitialYDegree() << std::endl;
        out << tab << " · Growth factor of the pyramid in the X direction: " << object.getPyramidGrowthX() << std::endl;
        out << tab << " · Growth factor of the pyramid in the Y direction: " << object.getPyramidGrowthY() << std::endl;
        out << tab << " · Pyramid information: " << std::endl;
        tab.increase(out);
        for (unsigned int i = 0; i < object.getNumberOfPyramidLevels(); ++i)
            out << tab << " - Level " << i + 1 << ": " << object.getNumberOfSpatialBins(i) << " spatial bins (" << object.getNumberOfDivisionsX(i) << " by " << object.getNumberOfDivisionsY(i) << ")." << std::endl;
        tab.decrease(out);
        out << tab << " · Pooling method: ";
        if (object.getPoolingMethod() == BOVW_POOLING_SUM) out << "Sum operator.";
        else if (object.getPoolingMethod() == BOVW_POOLING_MAX) out << "Max operator.";
        else out << "UNKNOWN";
        out << std::endl;
        out << tab << " · Polarity: " << (object.getPolarity()?("[Enabled]"):("[Disabled]")) << std::endl;
        out << tab << " · Interpolation: " << (object.getInterpolation()?("[Enabled]"):("[Disabled]")) << std::endl;
        out << tab << " · Unreliable: " << (object.getUnreliable()?("[Enabled]"):("[Disabled]")) << std::endl;
        out << tab << " · Separate scales: " << (object.getDifferentiateScales()?("[Enabled]"):("[Disabled]")) << std::endl;
        out << tab << " · Number of scales: " << object.getNumberOfScales() << std::endl;
        out << tab << " · Scales margin: " << (object.getScalesMargin()?("[Enabled]"):("[Disabled]")) << std::endl;
        out << tab << " · Spatial normalization: " << (object.getSpatialNormalization()?("[Enabled]"):("[Disabled]")) << std::endl;
        if (object.getAreaWeighting()) out << tab << " · Spatial normalization information: Area based normalization." << std::endl;
        else
        {
            out << tab << " · Spatial normalization information:" << std::endl;
            out << object.getSpatialNorm();
        }
        out << tab << " · Histogram normalization information:" << std::endl;
        out << object.getHistogramNorm();
        out << tab << " · Normalization power factor: " << object.getPowerFactor() << std::endl;
        out << tab << " · Normalization cutoff threshold: " << object.getCutoffValue() << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    void loadObject(const char * filename, VisualWordsHistogramParameters &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object.convertFromXML(parser);
        delete file;
    }
    
    void saveObject(const char * filename, const VisualWordsHistogramParameters &object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object.convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

