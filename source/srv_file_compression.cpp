// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_file_compression.hpp"
#include <cstring>
#include <fstream>
#include <sstream>
#include <zlib.h>
#include <bzlib.h>
#include "srv_utilities.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | LIBRARY COMPRESSION WRAPPERS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class StreamBufferZLib
    {
    public:
        inline void open(const char * filename, const char * fmode) { m_file = gzopen(filename, fmode); }
        inline bool is_open(void) const { return !(m_file == 0); }
        inline bool close(void) { return gzclose(m_file) == Z_OK; }
        inline int read(char * buffer, int buffer_size) { return gzread(m_file, buffer, buffer_size); }
        inline int write(char * pbase, int w) { return gzwrite(m_file, pbase, w); }
    private:
        gzFile m_file;
    };
    
    class StreamBufferBZip
    {
    public:
        inline void open(const char * filename, const char * fmode) { m_file = BZ2_bzopen(filename, fmode); }
        inline bool is_open(void) const { return !(m_file == 0); }
        inline bool close(void)
        {
            int errnum;
            
            BZ2_bzclose(m_file);
            BZ2_bzerror(m_file, &errnum);
            return BZ_OK == errnum;
        }
        inline int read(char * buffer, int buffer_size) { return BZ2_bzread(m_file, buffer, buffer_size); }
        inline int write(char * pbase, int w) { return BZ2_bzwrite(m_file, pbase, w); }
    private:
        BZFILE * m_file;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TEMPLATE COMPRESSION CLASSES         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class STREAMBUFFER>
    class CompressedStreamBuffer : public std::streambuf
    {
    public:
        CompressedStreamBuffer(): m_opened(0), m_zlevel(-1)
        {
            setp(m_buffer, m_buffer + (m_buffer_size - 1));
            setg(m_buffer + 4, m_buffer + 4, m_buffer + 4);
        }
        ~CompressedStreamBuffer(void) { close(); }
        
        inline int is_open(void) { return m_opened; }
        CompressedStreamBuffer * open(const char * filename, int mode_open);
        CompressedStreamBuffer * close(void);
        
        virtual int overflow(int c = EOF);
        virtual int underflow(void);
        virtual int sync(void);
        inline void zlevel(int z) { this->m_zlevel = std::max(-1, std::min(9, z)); }
        inline int zlevel(void) const  { return this->m_zlevel; }
    private:
        int flush_buffer(void);
        
        static const int m_buffer_size = 47 + 256;
        STREAMBUFFER m_compression_object;
        char m_buffer[m_buffer_size];
        char m_opened;
        int m_mode;
        int m_zlevel;
    };
    
    template <class STREAMBUFFER>
    CompressedStreamBuffer<STREAMBUFFER> * CompressedStreamBuffer<STREAMBUFFER>::open(const char * filename, int mode_open)
    {
        unsigned int idx;
        char fmode[10];
        
        // File already opened.
        if (is_open()) return (CompressedStreamBuffer<STREAMBUFFER> *)0;
        m_mode = mode_open;
        // No append nor read/write mode.
        if ((m_mode & std::ios::ate) || (m_mode & std::ios::app) || ((m_mode & std::ios::in) && (m_mode & std::ios::out)))
            return (CompressedStreamBuffer<STREAMBUFFER> *)0;
        for (unsigned int i = 0; i < sizeof(fmode); ++i) fmode[i] = '\0';
        idx = 0;
        if (m_mode & std::ios::in) fmode[idx++] = 'r';
        else if (m_mode & std::ios::out)
        {
            fmode[idx++] = 'w';
            if ((this->zlevel() >= 0) && (this->zlevel() <= 9))
                fmode[idx++] = (char)(48 + this->zlevel());
        }
        fmode[idx++] = 'b';
        fmode[idx] = '\0';
        
        m_compression_object.open(filename, fmode);
        // File failed to open.
        if (!m_compression_object.is_open())
            return (CompressedStreamBuffer<STREAMBUFFER> *)0;
        m_opened = 1;
        return this;
    }
    
    template <class STREAMBUFFER>
    CompressedStreamBuffer<STREAMBUFFER> * CompressedStreamBuffer<STREAMBUFFER>::close()
    {
        if (is_open())
        {
            sync();
            m_opened = 0;
            if (m_compression_object.close()) return this;
        }
        return (CompressedStreamBuffer *)0;
    }
    
    template <class STREAMBUFFER>
    int CompressedStreamBuffer<STREAMBUFFER>::underflow(void)
    {
        int n_putback, num;
        
        // Used for input buffer only
        if (gptr() && (gptr() < egptr()))
            return *reinterpret_cast<unsigned char *>(gptr());
        
        if (!(m_mode & std::ios::in) || !m_opened)
            return EOF;
        // Josuttis' implementation of inbuf
        n_putback = std::min(4, (int)(gptr() - eback()));
        std::memcpy(m_buffer + (4 - n_putback), gptr() - n_putback, n_putback);
        
        num = m_compression_object.read(m_buffer + 4, m_buffer_size - 4);
        if (num <= 0) // It is an error or end-of-file.
            return EOF;
        setg(m_buffer + (4 - n_putback), m_buffer + 4, m_buffer + 4 + num);
        
        return *reinterpret_cast<unsigned char *>(gptr());
    }
    
    template <class STREAMBUFFER>
    int CompressedStreamBuffer<STREAMBUFFER>::flush_buffer(void)
    {
        // Separate the writing of the buffer from overflow() and sync() operation.
        int w;
        
        w = (int)(pptr() - pbase());
        if (m_compression_object.write(pbase(), w) != w)
            return EOF;
        pbump(-w);
        return w;
    }
    
    template <class STREAMBUFFER>
    int CompressedStreamBuffer<STREAMBUFFER>::overflow(int c)
    {
        // Used for output buffer only
        if (!(m_mode & std::ios::out) || !m_opened) return EOF;
        if (c != EOF) { *pptr() = (char)c; pbump(1); }
        if (flush_buffer() == EOF) return EOF;
        return c;
    }
    
    template <class STREAMBUFFER>
    int CompressedStreamBuffer<STREAMBUFFER>::sync(void)
    {
        if ((pptr() && (pptr() > pbase())) && (flush_buffer() == EOF))
            return -1;
        return 0;
    }
    
    template <class STREAMBUFFER>
    class CompressedStreamBase : virtual public std::ios
    {
    public:
        CompressedStreamBase(void) { init(&m_buffer); }
        CompressedStreamBase(const char * filename, int mode_open) { init(&m_buffer); open(filename, mode_open); }
        ~CompressedStreamBase(void) { m_buffer.close(); }
        void open(const char * filename, int mode_open) { if (!m_buffer.open(filename, mode_open)) clear(rdstate() | std::ios::badbit); }
        inline void close(void) { if (m_buffer.is_open() && (!m_buffer.close())) clear(rdstate() | std::ios::badbit); }
        inline void zlevel(int z) { m_buffer.zlevel(z); }
        inline int zlevel(void) const  { return m_buffer.zlevel(); }
        inline CompressedStreamBuffer<STREAMBUFFER> * rdbuf(void) { return &m_buffer; }
    private:
        int m_zlevel;
    protected:
        CompressedStreamBuffer<STREAMBUFFER> m_buffer;
    };
    
    template <class STREAMBUFFER>
    class CompressedStreamInput: public CompressedStreamBase<STREAMBUFFER>, public std::istream
    {
    public:
        CompressedStreamInput(void) : std::istream(&this->m_buffer) {}
        CompressedStreamInput(const char * filename, int mode_open = std::ios::in ) : CompressedStreamBase<STREAMBUFFER>(filename, mode_open), std::istream(&this->m_buffer) {}
        inline CompressedStreamBuffer<STREAMBUFFER> * rdbuf(void) { return CompressedStreamBase<STREAMBUFFER>::rdbuf(); }
        inline void open(const char * filename, int mode_open = std::ios::in) { CompressedStreamBase<STREAMBUFFER>::open(filename, mode_open); }
    };
    
    template <class STREAMBUFFER>
    class CompressedStreamOutput : public CompressedStreamBase<STREAMBUFFER>, public std::ostream
    {
    public:
        CompressedStreamOutput(void) : std::ostream(&this->m_buffer) {}
        CompressedStreamOutput(const char * filename, int mode = std::ios::out) : CompressedStreamBase<STREAMBUFFER>(filename, mode), std::ostream(&this->m_buffer) {}
        inline CompressedStreamBuffer<STREAMBUFFER> * rdbuf(void) { return CompressedStreamBase<STREAMBUFFER>::rdbuf(); }
        inline void open(const char * filename, int mode_open = std::ios::out) { CompressedStreamBase<STREAMBUFFER>::open(filename, mode_open); }
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | WRAPPER CLASSES                      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    static CompressionPolicy static_compression_policy = srv::ZLibCompression;
    void compressionPolicy(CompressionPolicy c) { compressionPolicy() = c; }
    CompressionPolicy& compressionPolicy(void) { return static_compression_policy; }
    
    
    std::istream * iZipStream(const std::string &source, bool as_file)
    {
        const char * filename = source.c_str();
        unsigned char buffer[4];
        FILE * file;
        
        if (!as_file)
            return new std::istringstream(source);
        // Local environment to destroy check once used.
        {
            std::ifstream check(filename);
            if (!check.good()) return 0;
        }
        
        file = fopen(filename, "rb");
        if (!file)
            throw srv::Exception("Cannot open file '%s'", source.c_str());
        fread(buffer, sizeof(buffer), 1, file);
        fclose(file);
        
        /////std::cout << "[DEBUG] " << (int)buffer[0] << " " << (int)buffer[1] << " " << (int)buffer[2] << " " << (int)buffer[3] << std::endl;
        // BZIP ID=5a42 3968 (dec: 66 90 104 57) ascii: ZB9h
        if ((buffer[0] == 'B') && (buffer[1] == 'Z'))
            return new CompressedStreamInput<StreamBufferBZip>(filename);
        // ZLIB ID=8b1f 0808 (dec: 31 139 8 0)
        if ((buffer[0] == 0x1f) && (buffer[1] == 0x8b))
            return new CompressedStreamInput<StreamBufferZLib>(filename);
        
        return new std::ifstream(filename);
    }
    
    std::ostream * oZipStream(const std::string &filename)
    {
        switch (compressionPolicy())
        {
        case ZLibCompression:
            return new CompressedStreamOutput<StreamBufferZLib>(filename.c_str());
        case BZipCompression:
            return new CompressedStreamOutput<StreamBufferBZip>(filename.c_str());
        case NoCompression:
        default:
            return new std::ofstream(filename.c_str());
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

