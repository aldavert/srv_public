// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_classifier.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | SCORES INFORMATION CLASS             |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ScoresInformation::ScoresInformation(void) :
        m_scores(0),
        m_labels(0),
        m_number_of_samples(0),
        m_number_of_categories(0),
        m_number_of_dimensions(0)
    {
    }
    
    ScoresInformation::ScoresInformation(const char * filename) :
        m_scores(0),
        m_labels(0),
        m_number_of_samples(0),
        m_number_of_categories(0),
        m_number_of_dimensions(0)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("Scores file '%s' cannot be loaded.", filename);
        
        *file >> m_number_of_samples >> m_number_of_dimensions >> m_number_of_categories;
        if ((m_number_of_samples > 0) && (m_number_of_categories > 0))
        {
            m_scores = new VectorDense<double>[m_number_of_samples];
            m_labels = new VectorDense<unsigned int>[m_number_of_samples];
            
            for (unsigned int i = 0; i < m_number_of_samples; ++i)
            {
                unsigned int current_number_of_labels;
                
                m_scores[i].set(m_number_of_categories);
                *file >> current_number_of_labels;
                m_labels[i].set(current_number_of_labels);
                for (unsigned int j = 0; j < current_number_of_labels; ++j)
                    *file >> m_labels[i][j];
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    *file >> m_scores[i][j];
            }
        }
        
        delete file;
    }
    
    ScoresInformation::ScoresInformation(const VectorDense<double> * scores, const VectorDense<unsigned int> * labels, unsigned int number_of_samples, unsigned int number_of_categories, unsigned int number_of_dimensions) :
        m_scores((number_of_samples > 0)?new VectorDense<double>[number_of_samples]:0),
        m_labels((number_of_samples > 0)?new VectorDense<unsigned int>[number_of_samples]:0),
        m_number_of_samples(number_of_samples),
        m_number_of_categories(number_of_categories),
        m_number_of_dimensions(number_of_dimensions)
    {
        for (unsigned int i = 0; i < number_of_samples; ++i)
        {
            m_scores[i] = scores[i];
            m_labels[i] = labels[i];
        }
    }
    
    ScoresInformation::ScoresInformation(const ScoresInformation &other) :
        m_scores((other.m_number_of_samples > 0)?new VectorDense<double>[other.m_number_of_samples]:0),
        m_labels((other.m_number_of_samples > 0)?new VectorDense<unsigned int>[other.m_number_of_samples]:0),
        m_number_of_samples(other.m_number_of_samples),
        m_number_of_categories(other.m_number_of_categories),
        m_number_of_dimensions(other.m_number_of_dimensions)
    {
        for (unsigned int i = 0; i < other.m_number_of_samples; ++i)
        {
            m_scores[i] = other.m_scores[i];
            m_labels[i] = other.m_labels[i];
        }
    }
    
    ScoresInformation::~ScoresInformation(void)
    {
        if (m_scores != 0) delete [] m_scores;
        if (m_labels != 0) delete [] m_labels;
    }
    
    ScoresInformation& ScoresInformation::operator=(const ScoresInformation &other)
    {
        if (this != &other)
        {
            // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
            if (m_scores != 0) delete [] m_scores;
            if (m_labels != 0) delete [] m_labels;
            
            // -[ Copy ]-----------------------------------------------------------------------------------------------------------------------------
            if (other.m_number_of_samples)
            {
                m_scores = new VectorDense<double>[other.m_number_of_samples];
                m_labels = new VectorDense<unsigned int>[other.m_number_of_samples];
                m_number_of_samples = other.m_number_of_samples;
                for (unsigned int i = 0; i < other.m_number_of_samples; ++i)
                {
                    m_scores[i] = other.m_scores[i];
                    m_labels[i] = other.m_labels[i];
                }
            }
            else
            {
                m_scores = 0;
                m_labels = 0;
                m_number_of_samples = 0;
            }
            m_number_of_categories = other.m_number_of_categories;
            m_number_of_dimensions = other.m_number_of_dimensions;
        }
        
        return *this;
    }
    
    void ScoresInformation::set(const VectorDense<double> * scores, const VectorDense<unsigned int> * labels, unsigned int number_of_samples, unsigned int number_of_categories, unsigned int number_of_dimensions)
    {
        // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
        if (m_scores != 0) delete [] m_scores;
        if (m_labels != 0) delete [] m_labels;
        
        // -[ Set information ]------------------------------------------------------------------------------------------------------------------
        if (number_of_samples > 0)
        {
            m_scores = new VectorDense<double>[number_of_samples];
            m_labels = new VectorDense<unsigned int>[number_of_samples];
            m_number_of_samples = number_of_samples;
            
            for (unsigned int i = 0; i < number_of_samples; ++i)
            {
                m_scores[i] = scores[i];
                m_labels[i] = labels[i];
            }
        }
        else
        {
            m_scores = 0;
            m_labels = 0;
            m_number_of_samples = 0;
        }
        m_number_of_categories = number_of_categories;
        m_number_of_dimensions = number_of_dimensions;
    }
    
    void ScoresInformation::load(const char * filename)
    {
        // -[ Free ]-----------------------------------------------------------------------------------------------------------------------------
        if (m_scores != 0) delete [] m_scores;
        if (m_labels != 0) delete [] m_labels;
        m_scores = 0;
        m_labels = 0;
        m_number_of_samples = 0;
        m_number_of_categories = 0;
        m_number_of_dimensions = 0;
        
        // -[ Load information form disk ]-------------------------------------------------------------------------------------------------------
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("Scores file '%s' cannot be loaded.", filename);
        
        *file >> m_number_of_samples >> m_number_of_dimensions >> m_number_of_categories;
        if ((m_number_of_samples > 0) && (m_number_of_categories > 0))
        {
            m_scores = new VectorDense<double>[m_number_of_samples];
            m_labels = new VectorDense<unsigned int>[m_number_of_samples];
            
            for (unsigned int i = 0; i < m_number_of_samples; ++i)
            {
                unsigned int current_number_of_labels;
                
                m_scores[i].set(m_number_of_categories);
                *file >> current_number_of_labels;
                m_labels[i].set(current_number_of_labels);
                for (unsigned int j = 0; j < current_number_of_labels; ++j)
                    *file >> m_labels[i][j];
                for (unsigned int j = 0; j < m_number_of_categories; ++j)
                    *file >> m_scores[i][j];
            }
        }
        
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

