// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_utilities.hpp"
#include <sstream>

namespace srv
{
    //                   +--------------------------------------+
    //                   | AUXILIARY CLASSES AND FUNCTIONS      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void verboseMessage(std::ostream *output, const char *format, ...)
    {
        char message[4096];
        va_list args;
        time_t rawtime;
        struct tm  *timeinfo;
        
        va_start(args, format);
        vsprintf(message, format, args);
        va_end(args);
        time(&rawtime);
        timeinfo = localtime(&rawtime);
        *output << "[" << timeinfo->tm_mday << "/" << timeinfo->tm_mon + 1 << "/" << timeinfo->tm_year + 1900 << " " << timeinfo->tm_hour << ":" << timeinfo->tm_min << ":" << timeinfo->tm_sec << "] " << message;
    }
    
    void thousandSeparator(long value, char * output)
    {
        int len, number_of_dots;
        char original[512];
        
        sprintf(original, "%ld", value);
        for (len = (value < 0)?1:0; original[len] != '\0'; ++len);
        number_of_dots = (len % 3 == 0)?len / 3 - 1:len / 3;
        if (value >= 0) --len;
        
        for (int i = len, j = len + number_of_dots, k = 0; i >= 0; --i, --j, ++k)
        {
            output[j] = original[i];
            if ((j > 0) && ((k + 1) % 3 == 0))
            {
                --j;
                output[j] = ',';
            }
        }
        output[len + number_of_dots + 1] = '\0';
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE AUXILIARY FUNCTIONS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
///////    IplImage* mergeImages(const IplImage *first, const IplImage *second, bool horizontal)
///////    {
///////        IplImage *merged_image;
///////        CvMat subimage;
///////        
///////        if (first->depth != second->depth)
///////            throw Exception("The images have a different depth.");
///////        if (first->nChannels != second->nChannels)
///////            throw Exception("The images have a different number of channels.");
///////        
///////        if (horizontal)
///////        {
///////            merged_image = cvCreateImage(cvSize(first->width + second->width, srvMax<int>(first->height, second->height)), first->depth, first->nChannels);
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, 0, first->width, first->height));
///////            cvCopy(first, &subimage);
///////            cvGetSubRect(merged_image, &subimage, cvRect(first->width, 0, second->width, second->height));
///////            cvCopy(second, &subimage);
///////        }
///////        else
///////        {
///////            merged_image = cvCreateImage(cvSize(srvMax<int>(first->width, second->width), first->height + second->height), first->depth, first->nChannels);
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, 0, first->width, first->height));
///////            cvCopy(first, &subimage);
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, first->height, second->width, second->height));
///////            cvCopy(second, &subimage);
///////        }
///////        
///////        return merged_image;
///////    }
///////    
///////    void mergeImage(const IplImage *first, const IplImage *second, IplImage *merged_image, bool horizontal)
///////    {
///////        CvMat subimage;
///////        
///////        if ((first->depth != second->depth) || (first->depth != merged_image->depth))
///////            throw Exception("The images have a different depth.");
///////        if ((first->nChannels != second->nChannels) || (first->nChannels != merged_image->nChannels))
///////            throw Exception("The images have a different number of channels.");
///////        
///////        if (horizontal)
///////        {
///////            if ((merged_image->width < first->width + second->width) || (merged_image->height < srvMax<int>(first->height, second->height)))
///////                throw Exception("The merged image is to small to house the two given images");
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, 0, first->width, first->height));
///////            cvCopy(first, &subimage);
///////            cvGetSubRect(merged_image, &subimage, cvRect(first->width, 0, second->width, second->height));
///////            cvCopy(second, &subimage);
///////        }
///////        else
///////        {
///////            if ((merged_image->height < first->height + second->height) || (merged_image->width < srvMax<int>(first->width, second->width)))
///////                throw Exception("The merged image is to small to house the two given images");
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, 0, first->width, first->height));
///////            cvCopy(first, &subimage);
///////            cvGetSubRect(merged_image, &subimage, cvRect(0, first->height, second->width, second->height));
///////            cvCopy(second, &subimage);
///////        }
///////    }
///////    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | USER PARAMETERS PARSER CLASSES       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ValueParameter::ValueParameter(const char *value_name, const char *value_description, bool has_min_value, bool has_max_value) :
        m_value_range_max(has_max_value),
        m_value_range_min(has_min_value),
        m_has_value(false),
        m_block_separator(false),
        m_update(0)
    {
        unsigned int index;
        for (index = 0; (value_name[index] != '\0') && (index < 63); ++index)
            m_value_name[index] = value_name[index];
        m_value_name[index] = '\0';
        m_parameter_name[0] = '\0';
        for (index = 0; (value_description[index] != '\0') && (index < 1023); ++index)
            m_description[index] = value_description[index];
        m_description[index] = '\0';
        m_range_information[0] = '\0';
    }
    
    ValueParameter::ValueParameter(const char *parameter_name, const char *value_name, const char *value_description, bool has_min_value, bool has_max_value, bool *update) :
        m_value_range_max(has_max_value),
        m_value_range_min(has_min_value),
        m_has_value(false),
        m_block_separator(false),
        m_update(update)
    {
        unsigned int index;
        for (index = 0; (value_name[index] != '\0') && (index < 63); ++index)
            m_value_name[index] = value_name[index];
        m_value_name[index] = '\0';
        for (index = 0; (parameter_name[index] != '\0') && (index < 63); ++index)
            m_parameter_name[index] = parameter_name[index];
        m_parameter_name[index] = '\0';
        for (index = 0; (value_description[index] != '\0') && (index < 1023); ++index)
            m_description[index] = value_description[index];
        m_description[index] = '\0';
        m_range_information[0] = '\0';
    }
    void ValueParameter::OutOfRange(const char *value)
    {
        if (m_parameter_name[0] != '\0') throw Exception("In parameter '%s <%s>', the given value '%s' is out of range.", m_parameter_name, m_value_name, value);
        else throw Exception("Value '%s' is out of range for parameter '<%s>'.", value, m_value_name);
    }
    
    ParameterFloat::ParameterFloat(const char *value_name, const char *value_description, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f)", (double)min_value, m_value_name ,(double)max_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>)", (double)min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f)", m_value_name, (double)max_value);
        m_has_value = true;
    }
    
    ParameterFloat::ParameterFloat(const char *parameter_name, const char *value_name, const char *value_description, float default_value, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f) [Default=%f]", (double)min_value, m_value_name, (double)max_value, (double)default_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>) [Default=%f]", (double)min_value, m_value_name, (double)default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f) [Default=%f]", m_value_name, (double)max_value, (double)default_value);
        else sprintf(m_range_information, "[Default=%f]", (double)default_value);
        m_has_value = true;
    }
    
    ParameterFloat::ParameterFloat(const char *parameter_name, const char *value_name, const char *value_description, float default_value, bool has_min_value, float min_value, bool has_max_value, float max_value, float *destination_variable, bool *update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f) [Default=%f]", (double)min_value, m_value_name, (double)max_value, (double)default_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>) [Default=%f]", (double)min_value, m_value_name, (double)default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f) [Default=%f]", m_value_name, (double)max_value, (double)default_value);
        else sprintf(m_range_information, "[Default=%f]", (double)default_value);
        m_has_value = true;
    }
    
    void ParameterFloat::parseValue(const char *value)
    {
        std::istringstream istr(value);
        istr.imbue(std::locale("C"));
        istr >> *m_value;
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterDouble::ParameterDouble(const char *value_name, const char *value_description, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterDouble::ParameterDouble(const char *parameter_name, const char *value_name, const char *value_description, double default_value, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f) [Default=%f]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>) [Default=%f]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f) [Default=%f]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%f]", default_value);
        m_has_value = true;
    }
    
    ParameterDouble::ParameterDouble(const char *parameter_name, const char *value_name, const char *value_description, double default_value, bool has_min_value, double min_value, bool has_max_value, double max_value, double *destination_variable, bool *update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%f <= <%s> <= %f) [Default=%f]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%f <= <%s>) [Default=%f]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %f) [Default=%f]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%f]", default_value);
        m_has_value = true;
    }
    
    void ParameterDouble::parseValue(const char *value)
    {
        std::istringstream istr(value);
        istr.imbue(std::locale("C"));
        istr >> *m_value;
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterUChar::ParameterUChar(const char *value_name, const char *value_description, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterUChar::ParameterUChar(const char *parameter_name, const char *value_name, const char *value_description, unsigned char default_value, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterUChar::ParameterUChar(const char *parameter_name, const char *value_name, const char *value_description, unsigned char default_value, bool has_min_value, unsigned char min_value, bool has_max_value, unsigned char max_value, unsigned char *destination_variable, bool *update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterUChar::parseValue(const char *value)
    {
        *m_value = (unsigned char)atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterChar::ParameterChar(const char *value_name, const char *value_description, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterChar::ParameterChar(const char *parameter_name, const char *value_name, const char *value_description, char default_value, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterChar::ParameterChar(const char *parameter_name, const char *value_name, const char *value_description, char default_value, bool has_min_value, char min_value, bool has_max_value, char max_value, char *destination_variable, bool *update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterChar::parseValue(const char *value)
    {
        *m_value = (char)atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterCharArray::ParameterCharArray(const char *value_name, const char *value_description, char *destination_variable) :
        ValueParameter(value_name, value_description, false, false),
        m_value(destination_variable)
    {
        m_has_value = true;
    }
    
    ParameterCharArray::ParameterCharArray(const char *parameter_name, const char *value_name, const char *value_description, char *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, false, false, 0),
        m_value(destination_variable)
    {
        m_has_value = true;
    }
    
    ParameterCharArray::ParameterCharArray(const char *parameter_name, const char *value_name, const char *value_description, char *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, false, false, update),
        m_value(destination_variable)
    {
        m_has_value = true;
    }
    
    void ParameterCharArray::parseValue(const char *value)
    {
        strcpy(m_value, value);
    }
    
    ParameterUShort::ParameterUShort(const char *value_name, const char *value_description, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterUShort::ParameterUShort(const char *parameter_name, const char *value_name, const char *value_description, unsigned short default_value, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterUShort::ParameterUShort(const char *parameter_name, const char *value_name, const char *value_description, unsigned short default_value, bool has_min_value, unsigned short min_value, bool has_max_value, unsigned short max_value, unsigned short *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterUShort::parseValue(const char *value)
    {
        *m_value = (unsigned short)atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterShort::ParameterShort(const char *value_name, const char *value_description, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterShort::ParameterShort(const char *parameter_name, const char *value_name, const char *value_description, short default_value, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterShort::ParameterShort(const char *parameter_name, const char *value_name, const char *value_description, short default_value, bool has_min_value, short min_value, bool has_max_value, short max_value, short *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterShort::parseValue(const char *value)
    {
        *m_value = (short)atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterUInt::ParameterUInt(const char *value_name, const char *value_description, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterUInt::ParameterUInt(const char *parameter_name, const char *value_name, const char *value_description, unsigned int default_value, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterUInt::ParameterUInt(const char *parameter_name, const char *value_name, const char *value_description, unsigned int default_value, bool has_min_value, unsigned int min_value, bool has_max_value, unsigned int max_value, unsigned int *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterUInt::parseValue(const char *value)
    {
        *m_value = (unsigned int)atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterInt::ParameterInt(const char *value_name, const char *value_description, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterInt::ParameterInt(const char *parameter_name, const char *value_name, const char *value_description, int default_value, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    ParameterInt::ParameterInt(const char *parameter_name, const char *value_name, const char *value_description, int default_value, bool has_min_value, int min_value, bool has_max_value, int max_value, int *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%d <= <%s> <= %d) [Default=%d]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%d <= <%s>) [Default=%d]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %d) [Default=%d]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%d]", default_value);
        m_has_value = true;
    }
    
    void ParameterInt::parseValue(const char *value)
    {
        *m_value = atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterULong::ParameterULong(const char *value_name, const char *value_description, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterULong::ParameterULong(const char *parameter_name, const char *value_name, const char *value_description, unsigned long default_value, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld) [Default=%ld]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>) [Default=%ld]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld) [Default=%ld]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%ld]", default_value);
        m_has_value = true;
    }
    
    ParameterULong::ParameterULong(const char *parameter_name, const char *value_name, const char *value_description, unsigned long default_value, bool has_min_value, unsigned long min_value, bool has_max_value, unsigned long max_value, unsigned long *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld) [Default=%ld]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>) [Default=%ld]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld) [Default=%ld]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%ld]", default_value);
        m_has_value = true;
    }
    
    void ParameterULong::parseValue(const char *value)
    {
        *m_value = (unsigned long)atol(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterLong::ParameterLong(const char *value_name, const char *value_description, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable) :
        ValueParameter(value_name, value_description, has_min_value, has_max_value),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld)", min_value, m_value_name ,max_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>)", min_value, m_value_name);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld)", m_value_name, max_value);
        m_has_value = true;
    }
    
    ParameterLong::ParameterLong(const char *parameter_name, const char *value_name, const char *value_description, long default_value, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, 0),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld) [Default=%ld]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>) [Default=%ld]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld) [Default=%ld]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%ld]", default_value);
        m_has_value = true;
    }
    
    ParameterLong::ParameterLong(const char *parameter_name, const char *value_name, const char *value_description, long default_value, bool has_min_value, long min_value, bool has_max_value, long max_value, long *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, has_min_value, has_max_value, update),
        m_min_value(min_value),
        m_max_value(max_value),
        m_value(destination_variable)
    {
        *m_value = default_value;
        if (has_min_value && has_max_value) sprintf(m_range_information, "(%ld <= <%s> <= %ld) [Default=%ld]", min_value, m_value_name, max_value, default_value);
        else if (has_min_value) sprintf(m_range_information, "(%ld <= <%s>) [Default=%ld]", min_value, m_value_name, default_value);
        else if (has_max_value) sprintf(m_range_information, "(<%s> <= %ld) [Default=%ld]", m_value_name, max_value, default_value);
        else sprintf(m_range_information, "[Default=%ld]", default_value);
        m_has_value = true;
    }
    
    void ParameterLong::parseValue(const char *value)
    {
        *m_value = atoi(value);
        if (m_value_range_max && (*m_value > m_max_value)) OutOfRange(value);
        if (m_value_range_min && (*m_value < m_min_value)) OutOfRange(value);
    }
    
    ParameterBoolean::ParameterBoolean(const char *value_name, const char *value_description, bool *destination_variable) :
        ValueParameter(value_name, value_description, false, false),
        m_value(destination_variable)
    {
        *m_value = false;
        m_has_value = false;
    }
    
    ParameterBoolean::ParameterBoolean(const char *parameter_name, const char *value_name, const char *value_description, bool *destination_variable) :
        ValueParameter(parameter_name, value_name, value_description, false, false, 0),
        m_value(destination_variable)
    {
        *m_value = false;
        m_has_value = false;
    }
    
    ParameterBoolean::ParameterBoolean(const char *parameter_name, const char *value_name, const char *value_description, bool *destination_variable, bool * update) :
        ValueParameter(parameter_name, value_name, value_description, false, false, update),
        m_value(destination_variable)
    {
        *m_value = false;
        m_has_value = false;
    }
    
    void ParameterBoolean::parseValue(const char * /* value */)
    {
        *m_value = true;
    }
    
    ParameterParser::ParameterParser(const char *application_name, const char *initial_message, const char *final_message)
    {
        unsigned int index;
        for (index = 0; (application_name[index] != '\0') && (index < 255); ++index)
            m_application_name[index] = application_name[index];
        m_application_name[index] = '\0';
        if (initial_message != 0)
        {
            for (index = 0; (initial_message[index] != '\0') && (index < 4097); ++index)
                m_initial_message[index] = initial_message[index];
            m_initial_message[index] = '\0';
        }
        else m_initial_message[0] = '\0';
        if (final_message != 0)
        {
            for (index = 0; (final_message[index] != '\0') && (index < 4097); ++index)
                m_final_message[index] = final_message[index];
            m_final_message[index] = '\0';
        }
        else m_final_message[0] = '\0';
    }
    
    ParameterParser::~ParameterParser(void)
    {
        for (unsigned int i = 0; i < m_values.size(); ++i)
            delete m_values[i];
        for (unsigned int i = 0; i < m_parameters.size(); ++i)
            delete m_parameters[i];
    }
    
    void ParameterParser::parse(char **values, int number_of_arguments)
    {
        int index;
        if (number_of_arguments <= (int)m_values.size()) throw Exception("Incorrect number of parameters.");
        for (index = 1; index <= (int)m_values.size(); ++index)
            m_values[index - 1]->parseValue(values[index]);
        for (; index < number_of_arguments; ++index)
        {
            bool not_found;
            
            not_found = true;
            for (unsigned int k = 0; k < m_parameters.size(); ++k)
                if (m_parameters[k]->isParameterName(values[index]))
                {
                    not_found = false;
                    if (m_parameters[k]->hasValue())
                    {
                        ++index;
                        if (index >= number_of_arguments)
                            throw Exception("Expected value '<%s>' after parameter '%s'.", m_parameters[k]->getValueName(), m_parameters[k]->getParameterName());
                        m_parameters[k]->parseValue(values[index]);
                    }
                    else m_parameters[k]->parseValue(0);
                    m_parameters[k]->setUpdate();
                }
            if (not_found)
                throw Exception("Unknown parameter '%s'.", values[index]);
        }
    }
    
    void ParameterParser::synopsisMessage(std::ostream &out) const
    {
        unsigned int *parameters_width, *values_width, width_step;
        
        if (m_values.size() > 0) values_width = new unsigned int[m_values.size()];
        else values_width = 0;
        if (m_parameters.size() > 0) parameters_width = new unsigned int[m_parameters.size()];
        else parameters_width = 0;
        width_step = 0;
        for (unsigned int i = 0; i < m_values.size(); ++i)
        {
            for (values_width[i] = 0; m_values[i]->getValueName()[values_width[i]] != '\0'; ++values_width[i]);
            values_width[i] += 2;
            if (values_width[i] > width_step) width_step = values_width[i];
        }
        for (unsigned int i = 0; i < m_parameters.size(); ++i)
        {
            for (parameters_width[i] = 0; m_parameters[i]->getParameterName()[parameters_width[i]] != '\0'; ++parameters_width[i]);
            if (m_parameters[i]->hasValue())
            {
                parameters_width[i] += 3;
                for (unsigned int j = 0; m_parameters[i]->getValueName()[j] != '\0'; ++parameters_width[i], ++j);
            }
            if (parameters_width[i] > width_step) width_step = parameters_width[i];
        }
        width_step += 7;
        out << m_initial_message << std::endl << "Synopsis:" << std::endl << "$ " << m_application_name;
        
        for (unsigned int i = 0; i < m_values.size(); ++i)
            out << " <" << m_values[i]->getValueName() << ">";
        if (m_parameters.size() > 0)
        {
            out << " (";
            for (unsigned int i = 0, j = 0; i < m_parameters.size(); ++i)
            {
                if (!m_parameters[i]->isBlockSeparator())
                {
                    if (j > 0) out << " | ";
                    out << m_parameters[i]->getParameterName();
                    if (m_parameters[i]->hasValue()) out << " <" << m_parameters[i]->getValueName() << ">";
                    ++j;
                }
            }
            out << ")*";
        }
        
        out << std::endl << std::endl << "Where:" << std::endl;
        for (unsigned int i = 0; i < m_values.size(); ++i)
        {
            bool first_tab;
            for (unsigned int j = 0; j < width_step - values_width[i]; ++j) out << " ";
            out << "<" << m_values[i]->getValueName() << "> ";
            first_tab = true;
            for (unsigned int j = 0; m_values[i]->getDescription()[j] != '\0'; ++j)
            {
                if (m_values[i]->getDescription()[j] == '&')
                {
                    if (first_tab)
                    {
                        unsigned int subparameter_width;
                        const char *current_ptr;
                        
                        current_ptr = &(m_values[i]->getDescription()[j]);
                        subparameter_width = 0;
                        out << std::endl;
                        for (subparameter_width = 0; (current_ptr[subparameter_width] != '&') && (current_ptr[subparameter_width] != '\0'); ++subparameter_width);
                        if (subparameter_width < width_step)
                            for (unsigned int k = 0; k < width_step - subparameter_width - 1; ++k)
                                out << " ";
                        first_tab = false;
                    }
                    else
                    {
                        out << " ";
                        first_tab = true;
                    }
                }
                else out << m_values[i]->getDescription()[j];
            }
            out << std::endl;
        }
        
        for (unsigned int i = 0; i < m_parameters.size(); ++i)
        {
            bool initial_tab, first_tab;
            if (m_parameters[i]->isBlockSeparator())
            {
                for (unsigned int j = 0; j < width_step; ++j) out << " ";
                out << " " << m_parameters[i]->getDescription() << std::endl;
            }
            else
            {
                for (unsigned int j = 0; j < width_step - parameters_width[i]; ++j) out << " ";
                out << m_parameters[i]->getParameterName() << " ";
                if (m_parameters[i]->hasValue())
                    out << "<" << m_parameters[i]->getValueName() << "> ";
                first_tab = true;
                initial_tab = true;
                for (unsigned int j = 0; m_parameters[i]->getDescription()[j] != '\0'; ++j)
                {
                    if (m_parameters[i]->getDescription()[j] == '&')
                    {
                        if (initial_tab)
                        {
                            out << " " << m_parameters[i]->getRangeInformation();
                            initial_tab = false;
                        }
                        if (first_tab)
                        {
                            unsigned int subparameter_width;
                            const char *current_ptr;
                            
                            current_ptr = &(m_parameters[i]->getDescription()[j]);
                            subparameter_width = 0;
                            out << std::endl;
                            for (subparameter_width = 0; (current_ptr[subparameter_width] != '&') && (current_ptr[subparameter_width] != '\0'); ++subparameter_width);
                            if (subparameter_width < width_step)
                                for (unsigned int k = 0; k < width_step - subparameter_width - 1; ++k)
                                    out << " ";
                            first_tab = false;
                        }
                        else
                        {
                            out << " ";
                            first_tab = true;
                        }
                    }
                    else out << m_parameters[i]->getDescription()[j];
                }
                if (initial_tab) out << " " << m_parameters[i]->getRangeInformation();
                out << std::endl;
            }
        }
        
        out << std::endl << m_final_message << std::endl;
        
        if (values_width != 0) delete [] values_width;
        if (parameters_width != 0) delete [] parameters_width;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TAB CLASS                            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    CharacterTab::CharacterTab(char tab_character, unsigned int repetition) :
        m_tab_character(tab_character),
        m_tab_repetitions(repetition) {}
    
    void CharacterTab::increase(std::ostream &out)
    {
        TabTable::iterator search;
        
        search = tabTable().find(&out);
        if (search == tabTable().end()) tabTable()[&out] = 1;
        else ++search->second;
    }
    
    void CharacterTab::decrease(std::ostream &out)
    {
        TabTable::iterator search;
        
        search = tabTable().find(&out);
        if (search == tabTable().end()) tabTable()[&out] = 0;
        else if (search->second > 0) search->second = search->second - 1;
    }
    
    CharacterTab::TabTable& CharacterTab::tabTable(void)
    {
        static TabTable tab;
        
        return tab;
    }
    
    std::ostream& operator<<(std::ostream &out, const CharacterTab &tab_object)
    {
        const CharacterTab::TabTable &tabTable = tab_object.tabTable();
        CharacterTab::TabTable::const_iterator search;
        unsigned int size;
        
        size = tab_object.m_tab_repetitions;
        search = tabTable.find(&out);
        if (search != tabTable.end()) size *= search->second;
        else size = 0;
        for (unsigned int i = 0; i < size; ++i)
            out << tab_object.m_tab_character;
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

