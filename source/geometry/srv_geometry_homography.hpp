// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_HOMOGRAPHY_HPP_HEADER_FILE__
#define __SRV_HOMOGRAPHY_HPP_HEADER_FILE__

// -[ Other header files ]---------------------------------------------
#ifdef __ENABLE_LEVMAR__
    #include <levmar.h>
#endif
// -[ SRV header files ]-----------------------------------------------
#include "srv_geometry_definitions.hpp"
#include "srv_geometry_model.hpp"
#include "srv_geometry_fitting.hpp"
#include "../srv_vector.hpp"
#include "../srv_utilities.hpp"
#include "../srv_matrix.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | HOMOGRAPHY MODEL CLASS DECLARATION   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Class which stores the homography matrix which relates two images.
     *  \tparam data type used to represent the Homography matrix.
     */
    template <class T = double>
    class HomographyModel : public GeometricModel<T>
    {
    public:
        
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        HomographyModel(void);
        /** Parameters constructor which sets the method used to fit the Homography matrix.
         *  \param[in] fitting_algorithm algorithm used to fit the Homography parameters.
         *  \param[in] distance_threshold minimum distance between corresponding points to set them as valid.
         */
        HomographyModel(HOMOGRAPHY_ALGORITHMS fitting_algorithm, T distance_threshold = (T)1e-3);
        
        // -[ Fitting virtual functions ]------------------------------------------------------------------------------------------------------------
        /** This function estimates the Homography matrix parameters from the given matrix
         *  correspondences.
         *  \param[in] data 6xN matrix where the homogeneous coordinates of the corresponding points are stored in columns.
         *  \note The first image coordinates are stored in the first three rows, and the second image coordinates are stored in the last rows.
         */
        void fit(const Matrix<T> &data);
        /** This function calculates the number of corresponding points which belong
         *  to the current Homography matrix model.
         *  \param[in] data 6xN matrix where the homogeneous coordinates of the corresponding points are stored in columns.
         *  \return the number of inlier points in the given data matrix.
         */
        unsigned int evaluate(const Matrix<T> &data) const;
        /** This function calculates the number of corresponding points which belong to the current
         *  Homography matrix model.
         *  \param[in] data 6xN matrix where the homogeneous coordinates of the corresponding points are stored in columns.
         *  \param[out] inliers a N dimensional dense vector which stores which data points fits the current homography model.
         *  \return the number of inlier points in the given data matrix.
         */
        unsigned int evaluate(const Matrix<T> &data, VectorDense<bool> &inliers) const;
        /** Function which evaluates if a single data vector fits into the model.
         *  \param[in] data array with the evaluated vector.
         *  \returns a boolean flag which indicates if the data fits into the model.
         */
        bool evaluate(const T * data) const;
        /// Returns the minimum number of correspondences needed to estimate the Homography matrix.
        inline unsigned int getMinimumNumberOfSamples(void) const { return (m_fitting_algorithm == HOMOGRAPHY_LM_D2)?5:4; }
        /// Returns a constant reference to the geometric model matrix.
        inline const Matrix<T>& getModel(void) const { return m_homography; }
        /// Sets the geometric model matrix.
        inline void setModel(const Matrix<T> &model)
        {
            if ((model.getNumberOfRows() != 3) || (model.getNumberOfColumns() != 3))
                throw Exception("The Homography matrix must be a 3x3 matrix.");
            m_homography = model;
        }
        /** This function estimates the Homography matrix from correspondences in image coordinates.
         *  The function re-normalizes the point coordinates in order to improve the model estimation.
         *  \param[in] points_first array of tuples with the coordinates of the corresponding points in the first image.
         *  \param[in] points_second array of tuples with the coordinates of the corresponding points in the second image.
         *  \param[in] number_of_samples number of correspondences (i.e. number of elements in the arrays).
         *  \param[in] method method used to fit the model.
         *  \param[out] inliers vector with a boolean flag which indicates which point correspondences fit into the model.
         *  \returns the number of point correspondences which fit to the model.
         */
        unsigned int fit(const Tuple<T, T> * points_first, const Tuple<T, T> * points_second, unsigned int number_of_samples, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers);
        /** This function estimates the Homography matrix from correspondences in image coordinates.
         *  The function re-normalizes the point coordinates in order to improve the model estimation.
         *  \param[in] first matrix with the coordinates of the first image.
         *  \param[in] second matrix with the coordinates of the first image.
         *  \param[in] method method used to fit the model.
         *  \param[out] inliers vector with a boolean flag which indicates which point correspondences fit into the model.
         *  \returns the number of point correspondences which fit to the model.
         */
        unsigned int fit(const Matrix<T> &first, const Matrix<T> &second, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers);
        /// Copies the content of the given geometric model into the object.
        inline void copy(const GeometricModel<T> * other)
        {
            if (other->getIdentifier() != this->getIdentifier())
                throw Exception("Both objects must be of the same type.");
            *this = *((HomographyModel<T> *)other);
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the fitting method used to calculate the Homography matrix.
        inline HOMOGRAPHY_ALGORITHMS getFittingMethod(void) const {return m_fitting_algorithm;}
        /// Sets the fitting method used to calculate the Homography matrix.
        inline void setFittingMethod(HOMOGRAPHY_ALGORITHMS method) {m_fitting_algorithm = method;}
        /// Returns the distance threshold used to evaluate if a pair of corresponding points fits the current homography matrix model.
        inline T getDistanceThreshold(void) const { return m_distance_threshold; }
        /// Sets the distance threshold used to evaluate if a pair of corresponding points fits the current homography matrix model.
        inline void setDistanceThreshold(T distance_threshold) { m_distance_threshold = distance_threshold; }
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates a geometric model (virtual copy constructor).
        GeometricModel<T> * duplicate(void) const { return (GeometricModel<T> *)new HomographyModel<T>(*this); }
        /// Returns the identifier for the current geometric model class
        inline static GEOMETRIC_MODEL_IDENTIFIER getClassIdentifier(void) { return GM_HOMOGRAPHY; }
        /// Generates a new empty instance of the geometric model class.
        inline static GeometricModel<T> * generateObject(void) { return (GeometricModel<T> *)new HomographyModel<T>(); }
        /// Returns a flag which states if the geometric model class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the identifier for the current geometric model object.
        virtual GEOMETRIC_MODEL_IDENTIFIER getIdentifier(void) const { return GM_HOMOGRAPHY; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the Homography object parameters into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the Homography object parameters from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Auxiliary fitting functions ]----------------------------------------------------------------------------------------------------------
        /** This function calculates the Homography matrix using the least mean square algorithm.
         *  \param[in] data 6xN matrix where the homogeneous coordinates of the corresponding points are stored in columns.
         */
        void fitLMS(const Matrix<T> &data);
        
#ifdef __ENABLE_LEVMAR__
        // -[ Non-linear fitting functions ]---------------------------------------------------------------------------------------------------------
        /// Auxiliary function which calculates the Sampson error for an Homography matrix estimate.
        static void homographyD2Error(T * parameters, T * estimated_measurements, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function which calculates the jacobians of the Sampson error for an Homography matrix estimate.
        static void homographyD2Jacobian(T * parameters, T * jacobian, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function which calculates the Gold Standard error for an Homography matrix estimate.
        static void homographyGSError(T * parameters, T * estimated_measurements, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function which calculates the jacobians of the Gold Standard error for an Homography matrix estimate.
        static void homographyGSJacobian(T * parameters, T * jacobian, int number_of_parameters, int number_of_measurements, void * additional_data);
        /// Auxiliary function to call the LEVMAR library function to calculate the LM algorithm using the Sampson error.
        void levmarCallSampson(T * parameters, T * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, T * options, T * information, T * second_measurements);
        /// Auxiliary function to call the LEVMAR library function to calculate the LM algorithm using the Gold Standard error.
        void levmarCallGoldStandard(T * parameters, T * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, T * options, T * information, T * second_measurements);
#endif
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Homography matrix.
        Matrix<T> m_homography;
        /// Method used to fit the Homography matrix to the data.
        HOMOGRAPHY_ALGORITHMS m_fitting_algorithm;
        /// Distance threshold to the current Homography matrix model to a pair of corresponding points be considered an inlier.
        T m_distance_threshold;
        
        /// Static integer which indicates if the class has been initialized in the geometric model factory.
        static int m_is_initialized;
    };
    
    template <>
    inline void HomographyModel<double>::levmarCallSampson(double * parameters, double * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, double * options, double * information, double * second_measurements)
    {
        dlevmar_der(homographyD2Error, homographyD2Jacobian, parameters, first_measurements, number_of_parameters, number_of_measurements, 1000, options, information, 0, 0, second_measurements);
    }
    template <>
    inline void HomographyModel<float>::levmarCallSampson(float * parameters, float * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, float * options, float * information, float * second_measurements)
    {
        slevmar_der(homographyD2Error, homographyD2Jacobian, parameters, first_measurements, number_of_parameters, number_of_measurements, 1000, options, information, 0, 0, second_measurements);
    }
    template <class T>
    inline void HomographyModel<T>::levmarCallSampson(T * parameters, T * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, T * options, T * information, T * second_measurements)
    {
        throw Exception("Levenberg-Marquardt minimization can only be done with double or float Homography data types.");
    }
    template <>
    inline void HomographyModel<double>::levmarCallGoldStandard(double * parameters, double * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, double * options, double * information, double * second_measurements)
    {
        dlevmar_der(homographyGSError, homographyGSJacobian, parameters, first_measurements, number_of_parameters, number_of_measurements, 1000, options, information, 0, 0, second_measurements);
    }
    template <>
    inline void HomographyModel<float>::levmarCallGoldStandard(float * parameters, float * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, float * options, float * information, float * second_measurements)
    {
        slevmar_der(homographyGSError, homographyGSJacobian, parameters, first_measurements, number_of_parameters, number_of_measurements, 1000, options, information, 0, 0, second_measurements);
    }
    template <class T>
    inline void HomographyModel<T>::levmarCallGoldStandard(T * parameters, T * first_measurements, unsigned int number_of_parameters, unsigned int number_of_measurements, T * options, T * information, T * second_measurements)
    {
        throw Exception("Levenberg-Marquardt minimization can only be done with double or float Homography data types.");
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | HOMOGRAPHY MODEL CLASS               |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T>
    HomographyModel<T>::HomographyModel(void) :
        GeometricModel<T>(),
        m_homography(3, 3, 0),
        m_fitting_algorithm(HOMOGRAPHY_LMS),
        m_distance_threshold((T)1e-3) {}
    
    template <class T>
    HomographyModel<T>::HomographyModel(HOMOGRAPHY_ALGORITHMS fitting_algorithm, T distance_threshold) :
        GeometricModel<T>(),
        m_homography(3, 3, 0),
        m_fitting_algorithm(fitting_algorithm),
        m_distance_threshold(distance_threshold) {}
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ FITTING VIRTUAL FUNCTIONS ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T>
    void HomographyModel<T>::fit(const Matrix<T> &data)
    {
        if (m_fitting_algorithm == HOMOGRAPHY_LMS)
        {
            fitLMS(data);
        }
#ifdef __ENABLE_LEVMAR__
        else if (m_fitting_algorithm == HOMOGRAPHY_LM_D2)
        {
            const int number_of_parameters = 9;
            T * parameters, * first_measurements, * second_measurements, options[LM_OPTS_SZ], information[LM_INFO_SZ];
            int number_of_measurements;
            
            fitLMS(data);
            parameters = new T[9];
            parameters[0] = (T)m_homography(0, 0);
            parameters[1] = (T)m_homography(0, 1);
            parameters[2] = (T)m_homography(0, 2);
            parameters[3] = (T)m_homography(1, 0);
            parameters[4] = (T)m_homography(1, 1);
            parameters[5] = (T)m_homography(1, 2);
            parameters[6] = (T)m_homography(2, 0);
            parameters[7] = (T)m_homography(2, 1);
            parameters[8] = (T)m_homography(2, 2);
            
            options[0] = LM_INIT_MU;
            options[1] = (T)1e-12;
            options[2] = (T)1e-12;
            options[3] = (T)1e-15;
            options[4] = LM_DIFF_DELTA;
            
            number_of_measurements = data.getNumberOfColumns() * 2;
            first_measurements = new T[number_of_measurements];
            second_measurements = new T[number_of_measurements];
            for (int k = 0, j = 0; k < data.getNumberOfColumns(); ++k, j += 2)
            {
                first_measurements[j] = data(0, k);
                first_measurements[j + 1] = data(1, k);
                second_measurements[j] = data(3, k);
                second_measurements[j + 1] = data(4, k);
            }
            levmarCallSampson(parameters, first_measurements, number_of_parameters, number_of_measurements, options, information, second_measurements);
            
            m_homography(0, 0) = parameters[0];
            m_homography(0, 1) = parameters[1];
            m_homography(0, 2) = parameters[2];
            m_homography(1, 0) = parameters[3];
            m_homography(1, 1) = parameters[4];
            m_homography(1, 2) = parameters[5];
            m_homography(2, 0) = parameters[6];
            m_homography(2, 1) = parameters[7];
            m_homography(2, 2) = parameters[8];
            
            delete [] first_measurements;
            delete [] second_measurements;
            delete [] parameters;
        }
        else if (m_fitting_algorithm == HOMOGRAPHY_LM_GS)
        {
            const int number_of_parameters = 9;
            T * parameters, * first_measurements, * second_measurements, options[LM_OPTS_SZ], information[LM_INFO_SZ];
            int number_of_measurements;
            
            fitLMS(data);
            parameters = new T[9];
            parameters[0] = (T)m_homography(0, 0);
            parameters[1] = (T)m_homography(0, 1);
            parameters[2] = (T)m_homography(0, 2);
            parameters[3] = (T)m_homography(1, 0);
            parameters[4] = (T)m_homography(1, 1);
            parameters[5] = (T)m_homography(1, 2);
            parameters[6] = (T)m_homography(2, 0);
            parameters[7] = (T)m_homography(2, 1);
            parameters[8] = (T)m_homography(2, 2);
            
            options[0] = LM_INIT_MU;
            options[1] = (T)1e-12;
            options[2] = (T)1e-12;
            options[3] = (T)1e-15;
            options[4] = LM_DIFF_DELTA;
            
            number_of_measurements = data.getNumberOfColumns() * 4;
            first_measurements = new T[number_of_measurements];
            second_measurements = new T[number_of_measurements];
            for (int k = 0, j = 0; k < data.getNumberOfColumns(); ++k, j += 4)
            {
                first_measurements[j] = data(0, k);
                first_measurements[j + 1] = data(1, k);
                first_measurements[j + 2] = data(3, k);
                first_measurements[j + 3] = data(4, k);
                second_measurements[j] = data(0, k);
                second_measurements[j + 1] = data(1, k);
                second_measurements[j + 2] = data(3, k);
                second_measurements[j + 3] = data(4, k);
            }
            levmarCallGoldStandard(parameters, first_measurements, number_of_parameters, number_of_measurements, options, information, second_measurements);
            
            m_homography(0, 0) = parameters[0];
            m_homography(0, 1) = parameters[1];
            m_homography(0, 2) = parameters[2];
            m_homography(1, 0) = parameters[3];
            m_homography(1, 1) = parameters[4];
            m_homography(1, 2) = parameters[5];
            m_homography(2, 0) = parameters[6];
            m_homography(2, 1) = parameters[7];
            m_homography(2, 2) = parameters[8];
            
            delete [] first_measurements;
            delete [] second_measurements;
            delete [] parameters;
        }
#else
        else throw Exception("The selected method is not implemented or it is not available.");
#endif
    }
    
    template <class T>
    unsigned int HomographyModel<T>::evaluate(const Matrix<T> &data) const
    {
        T second_projected[3], cross[3], distance;
        unsigned int number_of_inliers;
        
        number_of_inliers = 0;
        for (int column = 0; column < data.getNumberOfColumns(); ++column)
        {
            MatrixVectorMultiplication(m_homography, &data(column)[3], second_projected);
            crossProduct3(data(column), second_projected, cross);
            distance = (T)sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);
            if (distance < m_distance_threshold) ++number_of_inliers;
        }
        
        return number_of_inliers;
    }
    
    template <class T>
    unsigned int HomographyModel<T>::evaluate(const Matrix<T> &data, VectorDense<bool> &inliers) const
    {
        T second_projected[3], cross[3], distance;
        unsigned int number_of_inliers;
        
        number_of_inliers = 0;
        for (int column = 0; column < data.getNumberOfColumns(); ++column)
        {
            MatrixVectorMultiplication(m_homography, &data(column)[3], second_projected);
            crossProduct3(data(column), second_projected, cross);
            distance = (T)sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);
            if (distance < m_distance_threshold)
            {
                inliers[column] = true;
                ++number_of_inliers;
            }
            else inliers[column] = false;
        }
        
        return number_of_inliers;
    }
    
    template <class T>
    bool HomographyModel<T>::evaluate(const T * data) const
    {
        T second_projected[3], cross[3], distance;
        
        MatrixVectorMultiplication(m_homography, &data[3], second_projected);
        crossProduct3(data, second_projected, cross);
        distance = (T)sqrt(cross[0] * cross[0] + cross[1] * cross[1] + cross[2] * cross[2]);
        return distance < m_distance_threshold;
    }
    
    template <class T>
    unsigned int HomographyModel<T>::fit(const Tuple<T, T> * points_first, const Tuple<T, T> * points_second, unsigned int number_of_samples, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers)
    {
        Matrix<T> data, first_transform, second_transform;
        unsigned int number_of_inliers;
        
        // 1) Normalize the data coordinates ........................................................................................................
        normalizeCoordinates(points_first, points_second, number_of_samples, data, first_transform, second_transform);
        
        // 2) Fit the model .........................................................................................................................
        number_of_inliers = GeometricFitting<T>::fit(data, method, inliers, this);
        
        // 3) Denormalize ...........................................................................................................................
        if (number_of_inliers >= this->getMinimumNumberOfSamples())
        {
            Matrix<T> aux;
            
            MatrixMultiplication(m_homography, second_transform, aux);
            MatrixInverse(first_transform);
            MatrixMultiplication(first_transform, aux, m_homography);
        }
        else m_homography.set(3, 3, 0);
        
        return number_of_inliers;
    }
    
    template <class T>
    unsigned int HomographyModel<T>::fit(const Matrix<T> &first, const Matrix<T> &second, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers)
    {
        Matrix<T> data, first_transform, second_transform;
        unsigned int number_of_inliers;
        
        // 1) Normalize the data coordinates ........................................................................................................
        normalizeCoordinates(first, second, this->getMinimumNumberOfSamples(), data, first_transform, second_transform);
        
        // 2) Fit the model .........................................................................................................................
        number_of_inliers = GeometricFitting<T>::fit(data, method, inliers, this);
        
        // 3) Denormalize ...........................................................................................................................
        if (number_of_inliers >= this->getMinimumNumberOfSamples())
        {
            Matrix<T> aux;
            
            MatrixMultiplication(m_homography, second_transform, aux);
            MatrixInverse(first_transform);
            MatrixMultiplication(first_transform, aux, m_homography);
        }
        else m_homography.set(3, 3, 0);
        
        return number_of_inliers;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ AUXILIARY FITTING FUNCTIONS ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T>
    void HomographyModel<T>::fitLMS(const Matrix<T> &data)
    {
        Matrix<T> A, u, v;
        VectorDense<T> s;
        
        A.set(data.getNumberOfColumns() * 2, 9);
        for (int i = 0; i < data.getNumberOfColumns(); ++i)
        {
            A(2 * i, 0) = 0.0;
            A(2 * i, 1) = 0.0;
            A(2 * i, 2) = 0.0;
            A(2 * i, 3) = -data(2, i) * data(3, i);
            A(2 * i, 4) = -data(2, i) * data(4, i);
            A(2 * i, 5) = -data(2, i) * data(5, i);
            A(2 * i, 6) = data(1, i) * data(3, i);
            A(2 * i, 7) = data(1, i) * data(4, i);
            A(2 * i, 8) = data(1, i) * data(5, i);
            A(2 * i + 1, 0) = data(2, i) * data(3, i);
            A(2 * i + 1, 1) = data(2, i) * data(4, i);
            A(2 * i + 1, 2) = data(2, i) * data(5, i);
            A(2 * i + 1, 3) = 0.0;
            A(2 * i + 1, 4) = 0.0;
            A(2 * i + 1, 5) = 0.0;
            A(2 * i + 1, 6) = -data(0, i) * data(3, i);
            A(2 * i + 1, 7) = -data(0, i) * data(4, i);
            A(2 * i + 1, 8) = -data(0, i) * data(5, i);
        }
        MatrixSVD(A, u, s, v);
        
        m_homography(0, 0) = v(0, 8);
        m_homography(0, 1) = v(1, 8);
        m_homography(0, 2) = v(2, 8);
        m_homography(1, 0) = v(3, 8);
        m_homography(1, 1) = v(4, 8);
        m_homography(1, 2) = v(5, 8);
        m_homography(2, 0) = v(6, 8);
        m_homography(2, 1) = v(7, 8);
        m_homography(2, 2) = v(8, 8);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ NON-LINEAR FITTING FUNCTIONS ]=============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
#ifdef __ENABLE_LEVMAR__
    
    template <class T>
    void HomographyModel<T>::homographyD2Error(T * parameters, T * estimated_measurements, int /*number_of_parameters*/, int number_of_measurements, void *additional_data)
    {
        T * second_measurements = (T *)additional_data;
        for (unsigned int i = 0; i < (unsigned int)number_of_measurements; i += 2)
        {
            T t2, x, y;
            
            x = second_measurements[i];
            y = second_measurements[i + 1];
            t2 = parameters[6] * x + parameters[7] * y + parameters[8];
            estimated_measurements[i] = (parameters[0] * x + parameters[1] * y + parameters[2]) / t2;
            estimated_measurements[i + 1] = (parameters[3] * x + parameters[4] * y + parameters[5]) / t2;
        }
    }
    
    template <class T>
    void HomographyModel<T>::homographyD2Jacobian(T * parameters, T * jacobian, int number_of_parameters, int number_of_measurements, void *additional_data)
    {
        T * second_measurements = (T *)additional_data;
        
        for (unsigned int i = 0, j = 0; i < (unsigned int)number_of_measurements; i += 2, j += number_of_parameters * 2)
        {
            T t0, t1, t2, x, y;
            
            x = second_measurements[i];
            y = second_measurements[i + 1];
            t0 = parameters[0] * x + parameters[1] * y + parameters[2];
            t1 = parameters[3] * x + parameters[4] * y + parameters[5];
            t2 = parameters[6] * x + parameters[7] * y + parameters[8];
            // Jx
            jacobian[j] = x / t2;
            jacobian[j + 1] = y / t2;
            jacobian[j + 2] = 1 / t2;
            jacobian[j + 3] = 0;
            jacobian[j + 4] = 0;
            jacobian[j + 5] = 0;
            jacobian[j + 6] = -(x * t0) / (t2 * t2);
            jacobian[j + 7] = -(y * t0) / (t2 * t2);
            jacobian[j + 8] = -t0 / (t2 * t2);
            // Jy
            jacobian[j + 9] = 0;
            jacobian[j + 10] = 0;
            jacobian[j + 11] = 0;
            jacobian[j + 12] = x / t2;
            jacobian[j + 13] = y / t2;
            jacobian[j + 14] = 1 / t2;
            jacobian[j + 15] = -(x * t1) / (t2 * t2);
            jacobian[j + 16] = -(y * t1) / (t2 * t2);
            jacobian[j + 17] = -t1 / (t2 * t2);
        }
    }
    
    template <class T>
    void HomographyModel<T>::homographyGSError(T * parameters, T * estimated_measurements, int /*number_of_parameters*/, int number_of_measurements, void *additional_data)
    {
        T * second_measurements = (T *)additional_data;
        T * h = parameters;
        T xAi, yAi, xBi, yBi, det, delta[4], epsilon[2];
        Matrix<T> J(2, 4), aux;
        
        for (unsigned int i = 0; i < (unsigned)number_of_measurements; i += 4)
        {
            xBi = second_measurements[i];
            yBi = second_measurements[i + 1];
            xAi = second_measurements[i + 2];
            yAi = second_measurements[i + 3];
            epsilon[0] = -h[5] + h[8] * yBi - h[3] * xAi - h[4] * yAi + h[6] * xAi * yBi + h[7] * yAi * yBi;
            epsilon[1] = h[2] + h[0] * xAi + h[1] * yAi - h[8] * xBi - h[6] * xAi * xBi - h[7] * xBi * yAi;
            J(0, 0) = -h[3] + h[6] * yBi;
            J(0, 1) = -h[4] + h[7] * yBi;
            J(0, 2) = 0;
            J(0, 3) = h[8] + h[6] * xAi + h[7] * yAi;
            J(1, 0) = h[0] - h[6] * xBi;
            J(1, 1) = h[1] - h[7] * xBi;
            J(1, 2) = -h[8] - h[6] * xAi - h[7] * yAi;
            J(1, 3) = 0;
            
            // ### delta = -trans(J) * inv(J * trans(J)) * epsilon; ###
            // Calculating: aux = J * trans(J)
            MatrixGEMM(1.0, J, false, J, true, 0.0, aux); // J * trans(J);
            // Calculating: aux = inv(aux)
            det = aux(0, 0) * aux(1, 1) - aux(0, 1) * aux(1, 0);// inv(J * trans(J))
            srvSwap(aux(0, 0), aux(1, 1)); 
            aux(0, 0) /= det;
            aux(1, 1) /= det;
            aux(0, 1) /= -det;
            aux(1, 0) /= -det;
            // Calculating: aux * epsilon
            det = aux(0, 0) * epsilon[0] + aux(0, 1) * epsilon[1];
            epsilon[1] = aux(1, 0) * epsilon[0] + aux(1, 1) * epsilon[1];
            epsilon[0] = det;
            delta[0] = J(0, 0) * epsilon[0] + J(1, 0) * epsilon[1];
            delta[1] = J(0, 1) * epsilon[0] + J(1, 1) * epsilon[1];
            delta[2] = J(0, 2) * epsilon[0] + J(1, 2) * epsilon[1];
            delta[3] = J(0, 3) * epsilon[0] + J(1, 3) * epsilon[1];
            
            estimated_measurements[i] = second_measurements[i] + delta[0];
            estimated_measurements[i + 1] = second_measurements[i + 1] + delta[1];
            estimated_measurements[i + 2] = second_measurements[i + 2] + delta[2];
            estimated_measurements[i + 3] = second_measurements[i + 3] + delta[3];
        }
    }
    
    template <class T>
    void HomographyModel<T>::homographyGSJacobian(T *parameters, T *jacobian, int number_of_parameters, int number_of_measurements, void *additional_data)
    {
        T *second_measurements = (T *)additional_data;
        T xAi, yAi, xBi, yBi, J[4][9];
        T x0, x1, x2, x3, x4, x5, x6, x7, x8, x9, x10, x11, x12, x13, x14, x15, x16, x17, x18, x19, x20, x21, x22, x23, x24, x25, x26;
        T x27, x28, x29, x30, x31, x32, x33, x34, x35, x36, x37, x38, x41, x42, x43, x44, x45, x46, x47, x48, x49, x50, x51;
        T x52, x53, x54, x55, x56, x57, x58, x59, x60, x61, x62, x63, x64, x65, x66, x67, x68, x69, x70, x71, x72, x73, x74, x75, x76;
        T x77, x78, x79, x80, x81, x82, x83, x84, x85, x86, x87, x88, x89, x90, x91, x92, x93, x94, x95, x96, x97, x98, x99, x100, x101;
        T x102, x103, x104, x105, x106, x107, x108, x109, x110, x111, x112, x113, x114, x115, x116, x117, x118, x119, x120, x121, x122;
        T x123, x124, x125, x126, x127, x128, x130, x131, x133, x134, x135, x136, x137, x138, x139, x140, x141, x142, x143;
        T x144, x145, x146, x147, x148, x149, x150, x151, x152, x153, x154, x155, x156, x157, x158, x159, x160, x161, x162, x163, x164;
        T x165, x166, x167, x168, x169, x172, x173, x174, x175, x176, x177, x178, x179, x180, x181, x183, x184, x185;
        T x186, x187, x188, x189, x190, x191, x194, x197, x198, x199, x200, x201, x202, x203, x204, x205, x206;
        T x207, x209;
        T h0, h1, h2, h3, h4, h5, h6, h7, h8;
        // We are using the equation system simplification directly obtained from python sympy function CSE. This functions generates an
        // overhead of replacement expressions.
        
        h0 = parameters[0];
        h1 = parameters[1];
        h2 = parameters[2];
        h3 = parameters[3];
        h4 = parameters[4];
        h5 = parameters[5];
        h6 = parameters[6];
        h7 = parameters[7];
        h8 = parameters[8];
        for (unsigned int i = 0, j = 0; i < (unsigned int)number_of_measurements; i += 4)
        {
            xBi = second_measurements[i];
            yBi = second_measurements[i + 1];
            xAi = second_measurements[i + 2];
            yAi = second_measurements[i + 3];
            
            x0 = h7 * yBi;
            x1 = x0 - h4;
            x2 = h6 * yBi;
            x3 = x2 - h3;
            x4 = h6 * xBi;
            x5 = h7 * xBi;
            x6 = h1 - x5;
            x7 = h7 * yAi;
            x8 = h6 * xAi;
            x9 = h8 + x7 + x8;
            x10 = h0 - x4;
            x11 = x3 * x3;
            x12 = x9 * x9;
            x13 = x1 * x1;
            x14 = x11 + x12 + x13;
            x15 = 1 / x14;
            x16 = x1 * x6;
            x17 = x10 * x3;
            x18 = x16 + x17;
            x19 = -2 * h0;
            x20 = 2 * x2;
            x21 = 2 * h3;
            x22 = x20 - x21;
            x23 = x15 * x18 * x22;
            x24 = 2 * x4;
            x25 = x19 + x23 + x24;
            x26 = x6 * x6;
            x27 = -x9;
            x28 = x27 * x27;
            x29 = x10 * x10;
            x30 = x26 + x28 + x29;
            x31 = x18 * x18;
            x32 = x15 * x31;
            x33 = x30 - x32;
            x34 = (T)1 / (x33 * x33);
            x35 = h3 - x2;
            x36 = (T)sqrt(x34);
            x37 = x15 * x15;
            x38 = x4 - h0;
            x41 = x18;
            x42 = x31;
            x43 = -x32;
            x44 = x30 + x43;
            x45 = 1 / x44;
            x46 = h8 * yBi;
            x47 = x2 * xAi;
            x48 = x0 * yAi;
            x49 = x46 + x47 + x48;
            x50 = h3 * xAi;
            x51 = h4 * yAi;
            x52 = h5 + x50 + x51;
            x53 = x49 - x52;
            x54 = 2 * x0;
            x55 = 2 * h4;
            x56 = x54 - x55;
            x57 = x45 * x45;
            x58 = -2 * h1;
            x59 = x15 * x41 * x56;
            x60 = 2 * x5;
            x61 = x58 + x59 + x60;
            x62 = h1 * yAi;
            x63 = h0 * xAi;
            x64 = h2 + x62 + x63;
            x65 = x4 * xAi;
            x66 = h8 * xBi;
            x67 = x5 * yAi;
            x68 = x65 + x66 + x67;
            x69 = x64 - x68;
            x70 = x36 * x38;
            x71 = 2 * h0;
            x72 = x24 - x71;
            x73 = x15 * x41 * x72;
            x74 = x15 * x23 * x41;
            x75 = x73 + x74;
            x76 = x15 * x32 * x45;
            x77 = x37 * x42 * x56;
            x78 = 2 * h1;
            x79 = x60 - x78;
            x80 = x15 * x41 * x79;
            x81 = x77 + x80;
            x82 = (T)pow(x37, 3.0 / 2.0);
            x83 = x15 + x76;
            x84 = x10 * x15 * x41 * x45;
            x85 = x35 * x83;
            x86 = x15 * x38 * x41 * x45;
            x87 = -2 * x9 * xAi;
            x88 = -2 * x3 * yBi;
            x89 = x87 + x88;
            x90 = 2 * x27 * xAi;
            x91 = x15 * x32 * x89;
            x92 = -2 * x3 * xBi;
            x93 = 2 * x10 * yBi;
            x94 = x92 + x93;
            x95 = x15 * x41 * x94;
            x96 = 2 * x10 * xBi;
            x97 = x90 + x91 + x95 + x96;
            x98 = x10 * yBi;
            x99 = -x3 * xBi;
            x100 = x98 + x99;
            x101 = x15 * x3 * x41 * x45;
            x102 = x38 * x45;
            x103 = x101 + x102;
            x104 = -2 * x9 * yAi;
            x105 = -2 * x1 * yBi;
            x106 = x104 + x105;
            x107 = 2 * x27 * yAi;
            x108 = 2 * x6 * xBi;
            x109 = 2 * x6 * yBi;
            x110 = -2 * x1 * xBi;
            x111 = x109 + x110;
            x112 = x111 * x15 * x41;
            x113 = x106 * x15 * x32;
            x114 = x107 + x108 + x112 + x113;
            x115 = x6 * yBi;
            x116 = -x1 * xBi;
            x117 = x115 + x116;
            x118 = x15 * x35 * x36 * x41;
            x119 = x70 - x118;
            x120 = 2 * h8;
            x121 = 2 * x8;
            x122 = 2 * x7;
            x123 = x120 + x121 + x122;
            x124 = -x123;
            x125 = x124 * x37 * x42;
            x126 = x124 + x125;
            x127 = x85 - x86;
            x128 = x5 - h1;
            x130 = x25;
            x131 = h4 - x0;
            x133 = x61;
            x134 = x15 * x36 * x41;
            x135 = x128 * x36;
            x136 = x37 * x42 * x57 * x75;
            x137 = x22 * x37;
            x138 = x37 * x41 * x45 * x72;
            x139 = 4 * x2;
            x140 = 4 * h3;
            x141 = x139 - x140;
            x142 = x141 * x42 * x45 * x82;
            x143 = x136 + x137 + x138 + x142;
            x144 = -2 * x0;
            x145 = x144 + x55;
            x146 = -x15 * x41 * x45;
            x147 = 4 * x0;
            x148 = 4 * h4;
            x149 = x147 - x148;
            x150 = x149 * x42 * x45 * x82;
            x151 = x37 * x41 * x45 * x79;
            x152 = x37 * x42 * x57 * x81;
            x153 = x37 * x56;
            x154 = x150 + x151 + x152 + x153;
            x155 = x15 * x41 * x45 * x6;
            x156 = x131 * x83;
            x157 = x128 * x15 * x41 * x45;
            x158 = -4 * x9 * xAi;
            x159 = -4 * x3 * yBi;
            x160 = x158 + x159;
            x161 = x160 * x32 * x37 * x45;
            x162 = x37 * x89;
            x163 = x15 * x32 * x57 * x97;
            x164 = x37 * x41 * x45 * x94;
            x165 = x161 + x162 + x163 + x164;
            x166 = x1 * x15 * x41 * x45;
            x167 = x128 * x45;
            x168 = x166 + x167;
            x169 = -x83 * yBi;
            x172 = x114;
            x173 = -4 * x9 * yAi;
            x174 = -4 * x1 * yBi;
            x175 = x173 + x174;
            x176 = x146 * xBi;
            x177 = x15 * x41 * x45 * yBi;
            x178 = x45 * xBi;
            x179 = x156 - x157;
            x180 = x36 * x37 * x42;
            x181 = x15 + x180;
            x183 = x126;
            x184 = 4 * x8;
            x185 = 4 * x7;
            x186 = 4 * h8;
            x187 = x184 + x185 + x186;
            x188 = -x187;
            x189 = x124 * x37;
            x190 = x131 * x134;
            x191 = x135 - x190;
            x194 = x97;
            x197 = x45;
            x198 = x57;
            x199 = x153 * x197 * x41;
            x200 = x133 * x198 * x37 * x42;
            x201 = x199 + x200;
            x202 = x15 * x197 * x41 * x9;
            x203 = x15 * x172 * x198 * x32;
            x204 = x111 * x197 * x37 * x41;
            x205 = x106 * x37;
            x206 = x175 * x197 * x32 * x37;
            x207 = x203 + x204 + x205 + x206;
            x209 = x83;
            
            J[0][0] = x53 * (x134 + x35 * (x137 * x36 * x41 + x130 * x34 * x37 * x42) + x10 * x15 * x3 * x36 + x10 * x130 * x15 * x34 * x41) + x69 * (-x36 + x130 * x34 * x38 - x15 * x3 * x35 * x36 - x130 * x15 * x34 * x35 * x41) + xAi * (x70 + x134 * x3);
            J[0][1] = x103 * yAi + x53 * (x201 * x35 + x1 * x10 * x15 * x197 + x10 * x133 * x15 * x198 * x41) + x69 * (x133 * x198 * x38 - x1 * x15 * x197 * x35 - x133 * x15 * x198 * x35 * x41);
            J[0][2] = x119;
            J[0][3] = x53 * (x209 + x143 * x35 - x102 * x137 * x41 - x102 * x15 * x38 - x15 * x198 * x38 * x41 * x75) + x69 * (x146 + x102 * x15 * x3 + x198 * x38 * x75 + x137 * x197 * x3 * x41 + x15 * x198 * x3 * x41 * x75) - xAi * (x84 + x85);
            J[0][4] = x53 * (x154 * x35 - x102 * x153 * x41 - x15 * x167 * x38 - x15 * x198 * x38 * x41 * x81) + x69 * (x198 * x38 * x81 + x15 * x197 * x35 * x6 + x145 * x197 * x35 * x37 * x41 - x15 * x198 * x35 * x41 * x81) - yAi * (x84 + x85);
            J[0][5] = x86 - x85;
            J[0][6] = x53 * (x169 + x176 + x165 * x35 + x10 * x100 * x15 * x197 + x10 * x162 * x197 * x41 + x10 * x15 * x194 * x198 * x41) + x69 * (x177 + x178 + x194 * x198 * x38 + x100 * x15 * x197 * x3 + x162 * x197 * x3 * x41 + x15 * x194 * x198 * x3 * x41) + x127 * xAi * yBi - x103 * xAi * xBi;
            J[0][7] = x53 * (x35 * (x205 + x15 * x175 * x180 + x111 * x36 * x37 * x41 + x172 * x34 * x37 * x42) + x10 * x117 * x15 * x36 + x10 * x205 * x36 * x41 + x10 * x15 * x172 * x34 * x41) + x69 * (x172 * x34 * x38 + x117 * x15 * x3 * x36 + x205 * x3 * x36 * x41 + x15 * x172 * x3 * x34 * x41) + yAi * yBi * (x10 * x134 + x181 * x35) - x119 * xBi * yAi;
            J[0][8] = x127 * yBi + x53 * (x35 * (x189 + x183 * x198 * x37 * x42 + x188 * x197 * x42 * x82) + x102 * x123 * x37 * x41 + x10 * x15 * x183 * x198 * x41) + x69 * (x183 * x198 * x38 + x123 * x197 * x35 * x37 * x41 + x15 * x183 * x198 * x3 * x41) - x103 * xBi;
            J[1][0] = x168 * xAi + x53 * (x131 * (x137 * x197 * x41 + x130 * x15 * x198 * x32) - x15 * x167 * x3 - x128 * x130 * x15 * x198 * x41) + x69 * (x128 * x130 * x198 + x1 * x15 * x197 * x3 + x1 * x130 * x15 * x198 * x41);
            J[1][1] = x53 * (x134 + x131 * (x153 * x36 * x41 + x133 * x34 * x37 * x42) + x1 * x15 * x36 * x6 + x133 * x15 * x34 * x41 * x6) + x69 * (-x36 + x128 * x133 * x34 - x1 * x131 * x15 * x36 - x131 * x133 * x15 * x34 * x41) + yAi * (x135 + x1 * x134);
            J[1][2] = x191;
            J[1][3] = x53 * (x131 * x143 + x10 * x15 * x167 + x137 * x197 * x41 * x6 + x15 * x198 * x41 * x6 * x75) + x69 * (x128 * x198 * x75 - x131 * x137 * x197 * x41 - x131 * x15 * x197 * x38 - x131 * x15 * x198 * x41 * x75) - xAi * (x155 + x156);
            J[1][4] = x53 * (x209 + x131 * x154 - x128 * x15 * x167 - x153 * x167 * x41 - x128 * x15 * x198 * x41 * x81) + x69 * (x146 + x128 * x198 * x81 + x131 * x15 * x197 * x6 + x131 * x145 * x197 * x37 * x41 - x131 * x15 * x198 * x41 * x81) - yAi * (x155 + x156);
            J[1][5] = x157 - x156;
            J[1][6] = x53 * (x131 * x165 + x100 * x15 * x197 * x6 + x162 * x197 * x41 * x6 + x15 * x194 * x198 * x41 * x6) + x69 * (x128 * x194 * x198 + x1 * x100 * x15 * x197 + x1 * x162 * x197 * x41 + x1 * x15 * x194 * x198 * x41) + x179 * xAi * yBi - x168 * xAi * xBi;
            J[1][7] = x53 * (x169 + x176 + x131 * x207 + x117 * x15 * x197 * x6 + x197 * x205 * x41 * x6 + x15 * x172 * x198 * x41 * x6) + x69 * (x177 + x178 + x128 * x172 * x198 + x1 * x117 * x15 * x197 + x1 * x197 * x205 * x41 + x1 * x15 * x172 * x198 * x41) + x179 * yAi * yBi - x168 * xBi * yAi;
            J[1][8] = x53 * (x131 * (x189 + x183 * x34 * x37 * x42 + x188 * x36 * x42 * x82) + x123 * x135 * x37 * x41 + x15 * x183 * x34 * x41 * x6) + x69 * (x128 * x183 * x34 + x1 * x15 * x183 * x34 * x41 + x123 * x131 * x36 * x37 * x41) + yBi * (x131 * x181 + x134 * x6) - x191 * xBi;
            J[2][0] = x36 * x9 * xAi + x130 * x34 * x69 * x9 - x15 * x3 * x36 * x53 * x9 - x130 * x15 * x34 * x41 * x53 * x9;
            J[2][1] = x197 * x9 * yAi + x133 * x198 * x69 * x9 - x1 * x15 * x197 * x53 * x9 - x133 * x15 * x198 * x41 * x53 * x9;
            J[2][2] = x36 * x9;
            J[2][3] = x202 * xAi + x198 * x69 * x75 * x9 - x102 * x15 * x53 * x9 - x137 * x197 * x41 * x53 * x9 - x15 * x198 * x41 * x53 * x75 * x9;
            J[2][4] = x202 * yAi - x199 * x53 * x9 + x198 * x69 * x81 * x9 - x15 * x167 * x53 * x9 - x15 * x198 * x41 * x53 * x81 * x9;
            J[2][5] = x202;
            J[2][6] = x36 * x69 * xAi - x134 * x53 * xAi + x194 * x34 * x69 * x9 - x134 * x9 * xAi * yBi - x36 * x9 * xAi * xBi - x100 * x15 * x36 * x53 * x9 - x162 * x36 * x41 * x53 * x9 - x15 * x194 * x34 * x41 * x53 * x9;
            J[2][7] = x197 * x69 * yAi - x177 * x9 * yAi - x178 * x9 * yAi + x172 * x198 * x69 * x9 - x117 * x15 * x197 * x53 * x9 - x15 * x197 * x41 * x53 * yAi - x197 * x205 * x41 * x53 * x9 - x15 * x172 * x198 * x41 * x53 * x9;
            J[2][8] = x197 * x69 - x177 * x9 - x178 * x9 + x183 * x198 * x69 * x9 - x15 * x197 * x41 * x53 - x189 * x197 * x41 * x53 * x9 - x15 * x183 * x198 * x41 * x53 * x9;
            J[3][0] = x27 * x53 * (x137 * x197 * x41 + x130 * x15 * x198 * x32) - x15 * x197 * x27 * x3 * x69 - x15 * x197 * x27 * x41 * xAi - x130 * x15 * x198 * x27 * x41 * x69;
            J[3][1] = x201 * x27 * x53 - x1 * x15 * x197 * x27 * x69 - x15 * x197 * x27 * x41 * yAi - x133 * x15 * x198 * x27 * x41 * x69;
            J[3][2] = x202;
            J[3][3] = x143 * x27 * x53 - x209 * x27 * xAi - x102 * x15 * x27 * x69 - x137 * x197 * x27 * x41 * x69 - x15 * x198 * x27 * x41 * x69 * x75;
            J[3][4] = x154 * x27 * x53 - x199 * x27 * x69 - x209 * x27 * yAi - x15 * x167 * x27 * x69 - x15 * x198 * x27 * x41 * x69 * x81;
            J[3][5] = x209 * x9;
            J[3][6] = x165 * x27 * x53 - x209 * x53 * xAi + x209 * x27 * xAi * yBi + x15 * x178 * x27 * x41 * xAi + x15 * x197 * x41 * x69 * xAi - x100 * x15 * x197 * x27 * x69 - x162 * x197 * x27 * x41 * x69 - x15 * x194 * x198 * x27 * x41 * x69;
            J[3][7] = x207 * x27 * x53 - x209 * x53 * yAi + x209 * x27 * yAi * yBi + x15 * x178 * x27 * x41 * yAi + x15 * x197 * x41 * x69 * yAi - x117 * x15 * x197 * x27 * x69 - x197 * x205 * x27 * x41 * x69 - x15 * x172 * x198 * x27 * x41 * x69;
            J[3][8] = -x209 * x53 + x209 * x27 * yBi + x27 * x53 * (x189 + x183 * x198 * x37 * x42 + x188 * x197 * x42 * x82) + x15 * x178 * x27 * x41 + x15 * x197 * x41 * x69 - x189 * x197 * x27 * x41 * x69 - x15 * x183 * x198 * x27 * x41 * x69;
            
            for (unsigned int k = 0; k < 4; ++k)
                for (unsigned int m = 0; m < (unsigned int)number_of_parameters; ++m, ++j)
                        jacobian[j] = J[k][m];
        }
    }
#endif
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <class T>
    void HomographyModel<T>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Geometric_Model");
        parser.setAttribute("Identifier", this->getIdentifier());
        parser.setAttribute("Fitting", (int)m_fitting_algorithm);
        parser.setAttribute("Distance", m_distance_threshold);
        parser.setAttribute("h00", m_homography(0, 0));
        parser.setAttribute("h01", m_homography(0, 1));
        parser.setAttribute("h02", m_homography(0, 2));
        parser.setAttribute("h10", m_homography(1, 0));
        parser.setAttribute("h11", m_homography(1, 1));
        parser.setAttribute("h12", m_homography(1, 2));
        parser.setAttribute("h20", m_homography(2, 0));
        parser.setAttribute("h21", m_homography(2, 1));
        parser.setAttribute("h22", m_homography(2, 2));
        parser.closeTag();
    }
    
    template <class T>
    void HomographyModel<T>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Geometric_Model"))
        {
            GEOMETRIC_MODEL_IDENTIFIER current_id = (GEOMETRIC_MODEL_IDENTIFIER)((int)parser.getAttribute("Identifier"));
            if (current_id != this->getIdentifier())
                throw Exception("The given XML object does not contains information about the expected geometric model. It was expected an object with the identifier %d but an object with the identifier %d found instead.", (int)this->getIdentifier(), (int)current_id);
            
            m_homography.set(3, 3);
            m_fitting_algorithm = (HOMOGRAPHY_ALGORITHMS)((int)parser.getAttribute("Fitting"));
            m_distance_threshold = parser.getAttribute("Distance");
            m_homography(0, 0) = parser.getAttribute("h00");
            m_homography(0, 1) = parser.getAttribute("h01");
            m_homography(0, 2) = parser.getAttribute("h02");
            m_homography(1, 0) = parser.getAttribute("h10");
            m_homography(1, 1) = parser.getAttribute("h11");
            m_homography(1, 2) = parser.getAttribute("h12");
            m_homography(2, 0) = parser.getAttribute("h20");
            m_homography(2, 1) = parser.getAttribute("h21");
            m_homography(2, 2) = parser.getAttribute("h22");
            
            while (!(parser.isTagIdentifier("Geometric_Model") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

