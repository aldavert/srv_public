// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_GEOMETRY_FITTING_HPP_HEADER_FILE__
#define __SRV_GEOMETRY_FITTING_HPP_HEADER_FILE__

#include "srv_geometry_definitions.hpp"
#include "srv_geometry_model.hpp"
#include "../srv_matrix.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | FITTING CLASS DECLARATION            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class with the different fitting methods used to fit a geometric model to the given data.
    template <class T>
    class GeometricFitting
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        GeometricFitting(void) : m_method(FITTING_DIRECT) {}
        /// Constructor which sets the identifier method used to fit the model to the data.
        GeometricFitting(FITTING_IDENTIFIERS method) : m_method(method) {}
        
        // -[ Fitting functions ]--------------------------------------------------------------------------------------------------------------------
        /** Generic function used to fit the model to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[in] method_identifier identifier of the method used to fit the model to the data.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        inline unsigned int fit(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model) const { return GeometricFitting<T>::fit(data, m_method, inliers, model); }
        /** Function which uses the model base function to fit it to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        inline unsigned int fitDirect(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model) const { return GeometricFitting<T>::direct(data, inliers, model); }
        /** Function which uses the RANSAC algorithm to robustly fit the model to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        inline unsigned int fitRansac(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model) const { return GeometricFitting<T>::ransac(data, inliers, model); }
        
        // -[ Static fitting functions ]-------------------------------------------------------------------------------------------------------------
        /** Generic static function used to fit the model to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[in] method_identifier identifier of the method used to fit the model to the data.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        static unsigned int fit(const Matrix<T> &data, FITTING_IDENTIFIERS &method_identifier, VectorDense<bool> &inliers, GeometricModel<T> * model);
        /** Function which uses the model base function to fit it to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        static unsigned int direct(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model);
        /** Function which uses the RANSAC algorithm to robustly fit the model to the data.
         *  \param[in] data matrix with the data used to estimate the model parameters. Elements expected to be stored in the matrix columns.
         *  \param[out] inliers vector with a boolean flag for each data sample which is true when the sample is an inlier.
         *  \param[out] model model fitted to the data.
         *  \returns the number of inlier samples.
         */
        static unsigned int ransac(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model);
        
    protected:
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Identifier of the method used to fit the model to the data.
        FITTING_IDENTIFIERS m_method;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | FITTING CLASS IMPLEMENTATION         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    unsigned int GeometricFitting<T>::fit(const Matrix<T> &data, FITTING_IDENTIFIERS &method_identifier, VectorDense<bool> &inliers, GeometricModel<T> * model)
    {
        unsigned int number_of_inliers;
        
        switch (method_identifier)
        {
        case FITTING_DIRECT:
            number_of_inliers = GeometricFitting<T>::direct(data, inliers, model);
            break;
        case FITTING_RANSAC:
            number_of_inliers = GeometricFitting<T>::ransac(data, inliers, model);
            break;
        default:
            throw Exception("The selected fitting method (%d) has not been yet implemented.", (int)method_identifier);
            break;
        }
        return number_of_inliers;
    }
    
    template <class T>
    unsigned int GeometricFitting<T>::direct(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model)
    {
        unsigned int number_of_inliers;
        
        model->fit(data);
        inliers.resize(data.getNumberOfColumns());
        number_of_inliers = model->evaluate(data, inliers);
        
        return number_of_inliers;
    }
    
    template <class T>
    unsigned int GeometricFitting<T>::ransac(const Matrix<T> &data, VectorDense<bool> &inliers, GeometricModel<T> * model)
    {
        // Constant definitions ...................................................................
        const unsigned int minimum_samples = model->getMinimumNumberOfSamples();
        const double p = 0.99;
        // Function variables .....................................................................
        unsigned int best_number_of_inliers, number_of_inliers, maximum_iteration;
        VectorDense<unsigned int> random_indexes;
        Matrix<T> data_points_subset;
        GeometricModel<T> * current_model;
        double epsilon;
        
        // Initialize structures ..................................................................
        current_model = model->duplicate();
        data_points_subset.set(data.getNumberOfRows(), minimum_samples);
        random_indexes.resize(data.getNumberOfColumns());
        for (int k = 0; k < data.getNumberOfColumns(); ++k) random_indexes[k] = k;
        epsilon = 0.5;
        best_number_of_inliers = 0;
        maximum_iteration = (unsigned int)(log(1 - p) / log(1 - pow(0.5, minimum_samples)));
        for (unsigned int iteration = 0, samples_index = data.getNumberOfColumns(); iteration < maximum_iteration; ++iteration)
        {
            if (samples_index >= data.getNumberOfColumns() - minimum_samples)
            {
                // Resort random indexes.
                for (int k = 0; k < data.getNumberOfColumns(); ++k)
                    srvSwap(random_indexes[k], random_indexes[k + rand() % (data.getNumberOfColumns() - k)]);
                samples_index = 0;
            }
            
            for (unsigned int k = 0; k < minimum_samples; ++k, ++samples_index)
                data_points_subset.copyColumn(k, data(random_indexes[samples_index]));
            current_model->fit(data_points_subset);
            number_of_inliers = current_model->evaluate(data);
            if (number_of_inliers > best_number_of_inliers)
            {
                double e;
                
                e = 1.0 - (double)number_of_inliers / (double)data.getNumberOfColumns();
                model->copy(current_model);
                best_number_of_inliers = number_of_inliers;
                if (e < epsilon)
                {
                    epsilon = e;
                    maximum_iteration = (unsigned int)(log(1 - 0.99) / log(1 - pow(1 - epsilon, minimum_samples)));
                }
            }
        }
        
        if (best_number_of_inliers >= minimum_samples)
        {
            // Iterative model fitting using the model fitted by the RANSAC algorithm
            // as an initial set.
            current_model->copy(model);
            for (unsigned int last_iteration = 0; last_iteration < 1000; ++last_iteration)
            {
                // Allocate memory for the inlier points
                data_points_subset.set(data.getNumberOfRows(), best_number_of_inliers);
                // Gather inlier samples
                for (int k = 0, j = 0; k < data.getNumberOfColumns(); ++k)
                {
                    if (current_model->evaluate(data(k)))
                    {
                        data_points_subset.copyColumn(j, data(k));
                        ++j;
                    }
                }
                // Fit the model with the inliers.
                current_model->fit(data_points_subset);
                
                // Calculate the number of inliers with the newly generated model.
                number_of_inliers = current_model->evaluate(data);
                // If the number of inliers increases, fit again...
                if (number_of_inliers > best_number_of_inliers)
                {
                    best_number_of_inliers = number_of_inliers;
                    model->copy(current_model);
                }
                else break; // Otherwise stop
            }
            
            inliers.resize(data.getNumberOfColumns());
            // Select the samples which belongs to the inliers set using the best fitted model.
            best_number_of_inliers = model->evaluate(data, inliers);
        }
        
        // Free allocated memory ..................................................................
        delete current_model;
        
        // Return the number of inliers ...........................................................
        return best_number_of_inliers;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

