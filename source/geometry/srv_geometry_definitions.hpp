// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_GEOMETRY_MODEL_DEFINITIONS_HPP_HEADER_FILE__
#define __SRV_GEOMETRY_MODEL_DEFINITIONS_HPP_HEADER_FILE__

namespace srv
{
    
    /// Identifier of the method used to fit the geometric model to the data.
    enum FITTING_IDENTIFIERS
    {
        FITTING_DIRECT = 17202,  ///< Directly using the fitting algorithm of the model without using any filter.
        FITTING_RANSAC           ///< Uses the RANSAC algorithm to robustly fit the model.
    };
    
    /// Identifier of the different geometric models.
    enum GEOMETRIC_MODEL_IDENTIFIER
    {
        GM_HOMOGRAPHY = 17001     /// Geometric model of the Homography matrix.
    };
    
    /// Algorithms to estimate the Homography matrix from a set of corresponding points.
    enum HOMOGRAPHY_ALGORITHMS
    {
        HOMOGRAPHY_LMS = 17101,   ///< Least Means Squares algorithm.
#ifdef __ENABLE_LEVMAR__
        HOMOGRAPHY_LM_D2,         ///< Levenberg-Marquardt minimization algorithm using Sampson error.
        HOMOGRAPHY_LM_GS          ///< Levenberg-Marquardt minimization algorithm using Gold Standard error.
#endif
    };
    
}

#endif

