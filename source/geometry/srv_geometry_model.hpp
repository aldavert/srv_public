// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_GEOMETRY_MODEL_HPP_HEADER_FILE__
#define __SRV_GEOMETRY_MODEL_HPP_HEADER_FILE__

#include "../srv_utilities.hpp"
#include "../srv_vector.hpp"
#include "../srv_xml.hpp"
#include "../srv_matrix.hpp"
#include "srv_geometry_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Builds an skew symmetric matrix from the given vector.
     *  \param[in] vec input vector.
     *  \param[out] matrix resulting skew symmetric matrix.
     */
    template <class T>
    inline void skewSymmetricMatrix(const VectorDense<T> &vec, Matrix<T> &matrix)
    {
        matrix.set(3, 3, 0);
        matrix(1, 2) = -vec[0];
        matrix(2, 1) = vec[0];
        matrix(0, 2) = vec[1];
        matrix(2, 0) = -vec[1];
        matrix(0, 1) = -vec[2];
        matrix(1, 0) = vec[2];
    }
    
    /** Converts the Rodrigues rotation formula form into a rotation matrix.
     *  \param[in] rotation_axis axis of the Rodrigues rotation.
     *  \param[in] angle Rodrigues rotation angle.
     *  \param[out] rotation_matrix rotation matrix.
     */
    template <class T>
    inline void rodrigues2RotationMatrix(const VectorDense<T> &rotation_axis, T angle, Matrix<T> &rotation_matrix)
    {
        T sa, ca;
        
        rotation_matrix.set(3, 3);
        sa = sin(angle);
        ca = 1 - cos(angle);
        rotation_matrix(0, 0) = (T)(1.0                          + ca * (rotation_axis[0] * rotation_axis[0] - 1.0));
        rotation_matrix(0, 1) = (T)(      sa * -rotation_axis[2] + ca * (rotation_axis[0] * rotation_axis[1]      ));
        rotation_matrix(0, 2) = (T)(      sa *  rotation_axis[1] + ca * (rotation_axis[0] * rotation_axis[2]      ));
        rotation_matrix(1, 0) = (T)(      sa *  rotation_axis[2] + ca * (rotation_axis[1] * rotation_axis[0]      ));
        rotation_matrix(1, 1) = (T)(1.0                          + ca * (rotation_axis[1] * rotation_axis[1] - 1.0));
        rotation_matrix(1, 2) = (T)(      sa * -rotation_axis[0] + ca * (rotation_axis[1] * rotation_axis[2]      ));
        rotation_matrix(2, 0) = (T)(      sa * -rotation_axis[1] + ca * (rotation_axis[2] * rotation_axis[0]      ));
        rotation_matrix(2, 1) = (T)(      sa *  rotation_axis[0] + ca * (rotation_axis[2] * rotation_axis[1]      ));
        rotation_matrix(2, 2) = (T)(1.0                          + ca * (rotation_axis[2] * rotation_axis[2] - 1.0));
    }
    
    /** Converts a rotation matrix into the Rodrigues rotation formula form.
     *  \param[in] rotation_matrix rotation matrix.
     *  \param[out] rotation_axis axis of the Rodrigues rotation.
     *  \param[out] angle Rodrigues rotation angle.
     */
    template <class T>
    inline void rotationMatrix2Rodrigues(const Matrix<T> &rotation_matrix, VectorDense<T> &rotation_axis, T &angle)
    {
        T factor;
        
        rotation_axis.resize(3);
        angle = (T)acos((MatrixTrace(rotation_matrix) - 1.0) / 2.0);
        factor = (T)(2.0 * sin(angle));
        rotation_axis[0] = (rotation_matrix(2, 1) - rotation_matrix(1, 2)) / factor;
        rotation_axis[1] = (rotation_matrix(0, 2) - rotation_matrix(2, 0)) / factor;
        rotation_axis[2] = (rotation_matrix(1, 0) - rotation_matrix(0, 1)) / factor;
    }
    
    /** Extract the rotation angles from a rotation matrix.
     *  \param[in] rotation_matrix rotation matrix.
     *  \param[out] x rotation in the X-axis.
     *  \param[out] y rotation in the Y-axis.
     *  \param[out] z rotation in the Z-axis.
     */
    template <class T>
    inline void rotation2angles(const Matrix<T> &rotation_matrix, T &x, T &y, T &z)
    {
        x = (T)atan2(rotation_matrix(2, 1), rotation_matrix(2, 2));
        z = (T)atan2(rotation_matrix(1, 0), rotation_matrix(0, 0));
        y = (T)atan2(-rotation_matrix(2, 0), rotation_matrix(2, 2) / cos(x));
    }
    
    /** Extract the rotation angles from a rotation matrix.
     *  \param[in] rotation_matrix rotation matrix.
     *  \param[out] x rotation in the X-axis.
     *  \param[out] y rotation in the Y-axis.
     *  \param[out] z rotation in the Z-axis.
     */
    template <class T>
    inline void rotation2angles(const MatrixStatic<T, 3> &rotation_matrix, T &x, T &y, T &z)
    {
        x = (T)atan2(rotation_matrix(2, 1), rotation_matrix(2, 2));
        z = (T)atan2(rotation_matrix(1, 0), rotation_matrix(0, 0));
        y = (T)atan2(-rotation_matrix(2, 0), rotation_matrix(2, 2) / cos(x));
    }
    
    /** Creates a rotation matrix from the given rotation angles.
     *  \param[in] x rotation in the X-axis.
     *  \param[in] y rotation in the Y-axis.
     *  \param[in] z rotation in the Z-axis.
     *  \param[out] rotation_matrix rotation matrix.
     */
    template <class T>
    inline void angles2rotation(double x, double y, double z, Matrix<T> &rotation_matrix)
    {
        Matrix<T> rotation_x, rotation_y, rotation_z, aux_matrix;
        VectorDense<T> axis(3, 0);
        
        axis[0] = 1.0;
        rodrigues2RotationMatrix(axis, x, rotation_x);
        axis[0] = 0.0; axis[1] = 1.0;
        rodrigues2RotationMatrix(axis, y, rotation_y);
        axis[1] = 0.0; axis[2] = 1.0;
        rodrigues2RotationMatrix(axis, z, rotation_z);
        
        MatrixMultiplication(rotation_z, rotation_y, aux_matrix);
        MatrixMultiplication(aux_matrix, rotation_x, rotation_matrix);
    }
    
    /// Calculates the cross product between two 3-dimensional vectors.
    template <class T>
    inline void crossProduct3(const T * __restrict__ first, const T * __restrict__ second, T * __restrict__ destination)
    {
        destination[0] = first[1] * second[2] - first[2] * second[1];
        destination[1] = first[2] * second[0] - first[0] * second[2];
        destination[2] = first[0] * second[1] - first[1] * second[0];
    }
    
    /** Calculates the cross product vector between two 3-dimensional vectors.
     *  \param[in] first first 3D vector.
     *  \param[in] second second 3D vector.
     *  \param[out] destination resulting 3D vector.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB, template <class, class> class VECTORC, class TC, class NC>
    inline void crossProduct3(const VECTORA<TA, NA> &first, const VECTORB<TB, NB> &second, VECTORC<TC, NC> &destination)
    {
        destination[0] = (TC)((double)first[1] * (double)second[2] - (double)first[2] * (double)second[1]);
        destination[1] = (TC)((double)first[2] * (double)second[0] - (double)first[0] * (double)second[2]);
        destination[2] = (TC)((double)first[0] * (double)second[1] - (double)first[1] * (double)second[0]);
    }
    
    /// Calculates the dot product between two 3-dimensional vectors.
    template <class T>
    inline T dotProduct3(const T * __restrict__ first, const T * __restrict__ second)
    {
        return first[0] * second[0] + first[1] * second[1] + first[2] * second[2];
    }
    
    /** Calculates the dot product vector between two 3-dimensional vectors.
     *  \param[in] first first 3D vector.
     *  \param[in] second second 3D vector.
     *  \returns the dot product between the two vectors.
     */
    template <template <class, class> class VECTORA, class TA, class NA, template <class, class> class VECTORB, class TB, class NB>
    inline typename META_MGT<TA, TB>::TYPE dotProduct3(const VECTORA<TA, NA> &first, const VECTORB<TB, NB> &second)
    {
        typedef typename META_MGT<TA, TB>::TYPE OUTPUT_TYPE;
        return (OUTPUT_TYPE)first[0] * (OUTPUT_TYPE)second[0] + (OUTPUT_TYPE)first[1] * (OUTPUT_TYPE)second[1] + (OUTPUT_TYPE)first[2] * (OUTPUT_TYPE)second[2];
    }
    
    /// Calculates the Rodrigues rotation formula to rotate a vector in the space.
    template <class T>
    inline void rodriguesVector3(const T * __restrict__ original_vector, const T  * __restrict__ axis, T angle, T * __restrict__ destination)
    {
        T cross_product[3], dp, ca, sa;
        crossProduct3(axis, original_vector, cross_product);
        dp = dotProduct3(axis, original_vector);
        ca = cos(angle);
        sa = sin(angle);
        destination[0] = original_vector[0] * cos(angle) + cross_product[0] * sa + axis[0] * dp * (1.0 - ca);
        destination[1] = original_vector[1] * cos(angle) + cross_product[1] * sa + axis[1] * dp * (1.0 - ca);
        destination[2] = original_vector[2] * cos(angle) + cross_product[2] * sa + axis[2] * dp * (1.0 - ca);
    }
    
    /// Calculates the Euclidean norm of 3D vector.
    template <template <class, class> class VECTOR, class T, class N>
    inline T norm3(const VECTOR<T, N> &v) { return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]); }
    
    /// Calculates the Euclidean norm of 3D vector.
    template <class T>
    inline T norm3(const T * __restrict__ v) { return sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]); }
    
    /** Calculate the normalization transform applied to image points coordinates to calculate
     *  image geometric models (homography matrix, fundamental matrix, ...) for
     *  uncalibrated camera images (the intrinsic camera parameters are unknown).
     *  \param[in] points array with the coordinates of the image points.
     *  \param[in] number_of_points number of elements in the array.
     *  \param[out] transform resulting transform matrix.
     */
    template <class T>
    void normalizationTransform(const Tuple<T, T> * points, unsigned int number_of_points, Matrix<T> &transform)
    {
        double cx, cy, s, sx, sy;
        
        cx = 0.0;
        cy = 0.0;
        for (int i = 0; i < (int)number_of_points; ++i)
        {
            cx += points[i].getFirst();
            cy += points[i].getSecond();
        }
        cx /= (double)number_of_points;
        cy /= (double)number_of_points;
        
        s = 0.0;
        for (int i = 0; i < (int)number_of_points; ++i)
        {
            sx = (points[i].getFirst() - cx);
            sy = (points[i].getSecond() - cy);
            s += sqrt(sx * sx + sy * sy);
        }
        s = sqrt(2.0) / (s / (double)number_of_points);
        
        transform.set(3, 3, 0);
        transform(0, 0) = s;
        transform(0, 2) = -s * cx;
        transform(1, 1) = s;
        transform(1, 2) = -s * cy;
        transform(2, 2) = 1.0;
    }
    
    /** Given a set of two corresponding points matrices, this function returns a single matrix with the normalized coordinates and the normalization matrices.
     *  \param[in] points_first array with the 2D coordinates of the first image.
     *  \param[in] points_second array with the 2D coordinates of the second image.
     *  \param[in] number_of_samples number of elements in the coordinates arrays.
     *  \param[out] data resulting 6xN matrix where the normalized coordinates are stored.
     *  \param[out] first_transform normalization transform for the first point coordinates.
     *  \param[out] second_transform normalization transform for the second point coordinates.
     */
    template <class T>
    unsigned int normalizeCoordinates(const Tuple<T, T> * points_first, const Tuple<T, T> * points_second, unsigned int number_of_samples, Matrix<T> &data, Matrix<T> &first_transform, Matrix<T> &second_transform)
    {
        Matrix<T> first_norm, second_norm;
        
        // 2) Calculate the normalization transform .................................................................................................
        normalizationTransform(points_first, number_of_samples, first_transform);
        normalizationTransform(points_second, number_of_samples, second_transform);
        
        // 3) Get the transformed coordinates matrix ................................................................................................
        data.set(6, number_of_samples);
        for (int i = 0; i < (int)number_of_samples; ++i)
        {
            data(0, i) =  first_transform(0, 0) *  points_first[i].getFirst() +  first_transform(0, 1) *  points_first[i].getSecond() +  first_transform(0, 2);
            data(1, i) =  first_transform(1, 0) *  points_first[i].getFirst() +  first_transform(1, 1) *  points_first[i].getSecond() +  first_transform(1, 2);
            data(2, i) =  first_transform(2, 0) *  points_first[i].getFirst() +  first_transform(2, 1) *  points_first[i].getSecond() +  first_transform(2, 2);
            data(3, i) = second_transform(0, 0) * points_second[i].getFirst() + second_transform(0, 1) * points_second[i].getSecond() + second_transform(0, 2);
            data(4, i) = second_transform(1, 0) * points_second[i].getFirst() + second_transform(1, 1) * points_second[i].getSecond() + second_transform(1, 2);
            data(5, i) = second_transform(2, 0) * points_second[i].getFirst() + second_transform(2, 1) * points_second[i].getSecond() + second_transform(2, 2);
        }
        
        return number_of_samples;
    }
    
    /** Calculate the normalization transform applied to image points coordinates to calculate image geometric models (homography matrix, fundamental matrix, ...) for
     *  uncalibrated camera images (the intrinsic camera parameters are unknown).
     *  \param[in] input matrix containing the input information.
     *  \param[out] transform resulting transform matrix.
     */
    template <class T>
    void normalizationTransform(const Matrix<T> &input, Matrix<T> &transform)
    {
        double cx, cy, s, sx, sy;
        
        s = 0.0;
        cx = 0.0;
        cy = 0.0;
        if (input.getNumberOfColumns() > input.getNumberOfRows()) // For point matrices where coordinates are stored in columns...
        {
            for (int i = 0; i < input.getNumberOfColumns(); ++i)
            {
                cx += input(0, i);
                cy += input(1, i);
            }
            cx /= (double)input.getNumberOfColumns();
            cy /= (double)input.getNumberOfColumns();
            
            for (int i = 0; i < input.getNumberOfColumns(); ++i)
            {
                sx = (input(0, i) - cx);
                sy = (input(1, i) - cy);
                s += sqrt(sx * sx + sy * sy);
            }
            s = sqrt(2.0) / (s / (double)input.getNumberOfColumns());
        }
        else // when coordinates are stored in rows...
        {
            for (int i = 0; i < input.getNumberOfColumns(); ++i)
            {
                cx += input(i, 0);
                cy += input(i, 1);
            }
            cx /= (double)input.getNumberOfColumns();
            cy /= (double)input.getNumberOfColumns();
            
            for (int i = 0; i < input.getNumberOfColumns(); ++i)
            {
                sx = (input(i, 0) - cx);
                sy = (input(i, 1) - cy);
                s += sqrt(sx * sx + sy * sy);
            }
            s = sqrt(2.0) / (s / (double)input.getNumberOfRows());
        }
        
        transform.set(3, 3, 0);
        transform(0, 0) = s;
        transform(0, 2) = -s * cx;
        transform(1, 1) = s;
        transform(1, 2) = -s * cy;
        transform(2, 2) = 1.0;
    }
    
    /** Given a set of two corresponding points matrices, this function returns a single matrix with the normalized coordinates and the normalization matrices.
     *  \param[in] first first matrix containing the points coordinates (matrix geometry is 2xN, 3xN, Nx3 or Nx2).
     *  \param[in] second second matrix containing the points coordinates (matrix geometry is 2xN, 3xN, Nx3 or Nx2).
     *  \param[in] min_size minimum number of points coordinates (if the number of point coordinates is lower than the given threshold an exception is generated).
     *  \param[out] data resulting 6xN matrix where the normalized coordinates are stored.
     *  \param[out] first_transform normalization transform for the first point coordinates.
     *  \param[out] second_transform normalization transform for the second point coordinates.
     */
    template <class T>
    unsigned int normalizeCoordinates(const Matrix<T> &first, const Matrix<T> &second, int min_size, Matrix<T> &data, Matrix<T> &first_transform, Matrix<T> &second_transform)
    {
        Matrix<T> first_norm, second_norm;
        int number_of_samples;
        
        // 1) Check the dimensionality of the matrices ..............................................................................................
        if (((first.getNumberOfRows() == 2) || (first.getNumberOfRows() == 3)) && (first.getNumberOfColumns() >= min_size)) number_of_samples = first.getNumberOfColumns();
        else if ((first.getNumberOfRows() > min_size) && ((first.getNumberOfColumns() == 2) && (first.getNumberOfColumns() == 3))) number_of_samples = first.getNumberOfRows();
        else throw Exception("First points matrix has an incorrect geometry. It must be a 3xN or a Nx3 matrix, with N >= %d.", min_size);
        if (((second.getNumberOfRows() == 2) || (second.getNumberOfRows() == 3)) && (second.getNumberOfColumns() >= min_size))
        {
            if (second.getNumberOfColumns() != number_of_samples)
                throw Exception("The geometries of the first and second point coordinates matrices does not match. Expected a matrix of 3x%d, but a matrix of 3x%d is given instead.", number_of_samples, second.getNumberOfColumns());
        }
        else if ((second.getNumberOfRows() >= min_size) && ((second.getNumberOfColumns() == 2) || (second.getNumberOfColumns() == 3)))
        {
            if (second.getNumberOfRows() != number_of_samples)
                throw Exception("The geometries of the first and second point coordinates matrices does not match. Expected a matrix of 3x%d, but a matrix of 3x%d is given instead.", number_of_samples, second.getNumberOfRows());
        }
        else throw Exception("Second points correspondences matrix has an incorrect geometry. It must by a 3xN or a Nx3 matrix, with N >= %d.", min_size);
        
        
        // 2) Calculate the normalization transform .................................................................................................
        normalizationTransform(first, first_transform);
        normalizationTransform(second, second_transform);
        
        // 3) Get the transformed coordinates matrix ................................................................................................
        data.set(6, number_of_samples);
        if ((first.getNumberOfRows() == 2) || (first.getNumberOfRows() == 3)) // Points-coordinates are stored in columns
        {
            for (int i = 0; i < number_of_samples; ++i)
            {
                data(0, i) = first_transform(0, 0) * first(0, i) + first_transform(0, 1) * first(1, i) + first_transform(0, 2);
                data(1, i) = first_transform(1, 0) * first(0, i) + first_transform(1, 1) * first(1, i) + first_transform(1, 2);
                data(2, i) = first_transform(2, 0) * first(0, i) + first_transform(2, 1) * first(1, i) + first_transform(2, 2);
            }
        }
        else // Point-coordinates are stored in rows
        {
            for (int i = 0; i < number_of_samples; ++i)
            {
                data(0, i) = first_transform(0, 0) * first(i, 0) + first_transform(0, 1) * first(i, 1) + first_transform(0, 2);
                data(1, i) = first_transform(1, 0) * first(i, 0) + first_transform(1, 1) * first(i, 1) + first_transform(1, 2);
                data(2, i) = first_transform(2, 0) * first(i, 0) + first_transform(2, 1) * first(i, 1) + first_transform(2, 2);
            }
        }
        
        if ((second.getNumberOfRows() == 2) || (second.getNumberOfRows() == 3)) // Points-coordinates are stored in columns
        {
            for (int i = 0; i < number_of_samples; ++i)
            {
                data(3, i) = second_transform(0, 0) * second(0, i) + second_transform(0, 1) * second(1, i) + second_transform(0, 2);
                data(4, i) = second_transform(1, 0) * second(0, i) + second_transform(1, 1) * second(1, i) + second_transform(1, 2);
                data(5, i) = second_transform(2, 0) * second(0, i) + second_transform(2, 1) * second(1, i) + second_transform(2, 2);
            }
        }
        else // Point-coordinates are stored in rows
        {
            for (int i = 0; i < number_of_samples; ++i)
            {
                data(3, i) = second_transform(0, 0) * second(i, 0) + second_transform(0, 1) * second(i, 1) + second_transform(0, 2);
                data(4, i) = second_transform(1, 0) * second(i, 0) + second_transform(1, 1) * second(i, 1) + second_transform(1, 2);
                data(5, i) = second_transform(2, 0) * second(i, 0) + second_transform(2, 1) * second(i, 1) + second_transform(2, 2);
            }
        }
        
        return number_of_samples;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GEOMETRIC MODEL VIRTUAL CLASS        |
    //                   | DEFINITION                           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which defines the functions needed by a geometry model.
    template <class T>
    class GeometricModel
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Virtual destructor.
        virtual ~GeometricModel(void) {}
        
        // -[ Pure virtual fitting functions ]-------------------------------------------------------------------------------------------------------
        /** This function estimates the Homography matrix parameters from the given matrix
         *  correspondences.
         *  \param[in] data matrix with the data used to create the model. Elements expected to be stored in the matrix columns.
         *  \note The first image coordinates are stored in the first three rows, and the second image coordinates are stored in the last rows.
         */
        virtual void fit(const Matrix<T> &data) = 0;
        /** Virtual function which evaluates how the data fits into the model.
         *  \param[in] data matrix with the evaluated data. Elements expected to be stored in the matrix columns.
         *  \return the number of inlier samples for the given samples.
         */
        virtual unsigned int evaluate(const Matrix<T> &data) const = 0;
        /** Virtual function which evaluates how the data fits into the model.
         *  \param[in] data matrix with the evaluated data. Elements expected to be stored in the matrix columns.
         *  \param[out] inliers vector with a boolean flag which indicates which samples fit to the model.
         *  \return the number of inlier samples for the given samples.
         */
        virtual unsigned int evaluate(const Matrix<T> &data, VectorDense<bool> &inliers) const = 0;
        /** Virtual function which evaluates if a single data vector fits into the model.
         *  \param[in] data array with the evaluated vector.
         *  \returns a boolean flag which indicates if the data fits into the model.
         */
        virtual bool evaluate(const T * data) const = 0;
        /// Returns the minimum number of samples needed to estimate the model.
        virtual unsigned int getMinimumNumberOfSamples(void) const = 0;
        /// Returns a constant reference to the geometric model matrix.
        virtual const Matrix<T>& getModel(void) const = 0;
        /// Sets the geometric model matrix.
        virtual void setModel(const Matrix<T> &model) = 0;
        /** This function estimates the model from correspondences in image coordinates.
         *  \param[in] points_first array of tuples with the coordinates of the corresponding points in the first image.
         *  \param[in] points_second array of tuples with the coordinates of the corresponding points in the second image.
         *  \param[in] number_of_samples number of correspondences (i.e. number of elements in the arrays).
         *  \param[in] method method used to fit the model.
         *  \param[out] inliers vector with a boolean flag which indicates which point correspondences fit into the model.
         *  \returns the number of point correspondences which fit to the model.
         */
        virtual unsigned int fit(const Tuple<T, T> * points_first, const Tuple<T, T> * points_second, unsigned int number_of_samples, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers) = 0;
        /** This function estimates the model from correspondences in image coordinates.
         *  \param[in] first matrix with the coordinates of the first image.
         *  \param[in] second matrix with the coordinates of the first image.
         *  \param[in] method method used to fit the model.
         *  \param[out] inliers vector with a boolean flag which indicates which point correspondences fit into the model.
         *  \returns the number of point correspondences which fit to the model.
         */
        virtual unsigned int fit(const Matrix<T> &first, const Matrix<T> &second, FITTING_IDENTIFIERS method, VectorDense<bool> &inliers) = 0;
        /// Copies the content of the given geometric model into the object.
        virtual void copy(const GeometricModel<T> * other) = 0;
        
        // -[ Factory functions ]--------------------------------------------------------------------------------------------------------------------
        /// Duplicates a geometric model (virtual copy constructor).
        virtual GeometricModel<T> * duplicate(void) const = 0;
        ///// /// Returns the identifier for the current geometric model class
        ///// inline static GEOMETRIC_MODEL_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the geometric model class.
        ///// inline static GemeotricModel<T> * generateObject(void) { return (GeometricModel<T> *)new (); }
        ///// /// Returns a flag which states if the geometric model class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the identifier for the current geometric model object.
        virtual GEOMETRIC_MODEL_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the geometric model information into an XML object.
        virtual void convertToXML(XmlParser &parser) const = 0;
        /// Retrieves the geometric model information from an XML object.
        virtual void convertFromXML(XmlParser &parser) = 0;
        
    protected:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        GeometricModel(void) {}
        
        //// /// Static integer which indicates if the class has been initialized in the geometric model factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEBOOK FACTORY                     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    struct GeometricModelFactory { typedef Factory<GeometricModel<T>, GEOMETRIC_MODEL_IDENTIFIER> Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

