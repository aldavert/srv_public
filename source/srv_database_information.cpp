// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_database_information.hpp"
#include <cstring>
#include <set>
#include <map>

namespace srv
{
    //                   +--------------------------------------+
    //                   | PIXEL SEMANTIC INFORMATION CLASS     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void PixelSemanticInformation::projectiveTransform(const double *projective, PixelSemanticInformation &transformed_annotation) const
    {
        const int border_color[1] = { -1000 };
        transformed_annotation.m_pixels_annotation.setGeometry(m_pixels_annotation);
        ImageTransform(m_pixels_annotation, projective, transformed_annotation.m_pixels_annotation, NEAREST, DefaultNumberOfThreads::ImageTransform(), border_color);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DATABASE IMAGE INFORMATION CLASS     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    DatabaseImageInformation::DatabaseImageInformation(void) :
        m_number_of_bounding_boxes(0),
        m_bounding_boxes(0),
        m_number_of_global_categories(0),
        m_global_categories(0),
        m_width(0),
        m_height(0)
    {
        m_image_filename[0] = '\0';
        m_mask_filename[0] = '\0';
        m_metadata_filename[0] = '\0';
        m_database_image_filename[0] = '\0';
        m_database_mask_filename[0] = '\0';
        m_database_metadata_filename[0] = '\0';
    }
    
    DatabaseImageInformation::DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, unsigned int width, unsigned int height) :
        m_number_of_bounding_boxes(0),
        m_bounding_boxes(0),
        m_number_of_global_categories(0),
        m_global_categories(0),
        m_width(width),
        m_height(height)
    {
        copy_str(image_filename, m_database_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_database_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
        copy_str(image_filename, m_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_metadata_filename, FILENAME_SIZE);
    }
    
    DatabaseImageInformation::DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, unsigned int number_of_bounding_boxes, const BoundingBox *bounding_boxes, unsigned int number_of_global_categories, const GlobalCategory *global_categories, unsigned int width, unsigned int height) :
        m_number_of_bounding_boxes(number_of_bounding_boxes),
        m_bounding_boxes((number_of_bounding_boxes > 0)?new BoundingBox[number_of_bounding_boxes]:0),
        m_number_of_global_categories(number_of_global_categories),
        m_global_categories((number_of_global_categories > 0)?new GlobalCategory[number_of_global_categories]:0),
        m_width(width),
        m_height(height)
    {
        copy_str(image_filename, m_database_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_database_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
        copy_str(image_filename, m_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_metadata_filename, FILENAME_SIZE);
        for (unsigned int i = 0; i < number_of_bounding_boxes; ++i) m_bounding_boxes[i] = bounding_boxes[i];
        for (unsigned int i = 0; i < number_of_global_categories; ++i) m_global_categories[i] = global_categories[i];
    }
    
    DatabaseImageInformation::DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, const char *route, unsigned int width, unsigned int height) :
        m_number_of_bounding_boxes(0),
        m_bounding_boxes(0),
        m_number_of_global_categories(0),
        m_global_categories(0),
        m_width(width),
        m_height(height)
    {
        unsigned int index;
        copy_str(image_filename, m_database_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_database_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
        index = copy_str(route, m_image_filename, ROUTE_SIZE);
        copy_str(image_filename, m_image_filename, ROUTE_SIZE, index);
        if (mask_filename[0] != '\0')
        {
            index = copy_str(route, m_mask_filename, ROUTE_SIZE);
            copy_str(mask_filename, m_mask_filename, ROUTE_SIZE, index);
        }
        else m_mask_filename[0] = '\0';
        if (metadata_filename[0] != '\0')
        {
            index = copy_str(route, m_metadata_filename, ROUTE_SIZE);
            copy_str(metadata_filename, m_metadata_filename, ROUTE_SIZE, index);
        }
        else m_metadata_filename[0] = '\0';
    }
    
    DatabaseImageInformation::DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, const char *route, unsigned int number_of_bounding_boxes, const BoundingBox *bounding_boxes, unsigned int number_of_global_categories, const GlobalCategory *global_categories, unsigned int width, unsigned int height) :
        m_number_of_bounding_boxes(number_of_bounding_boxes),
        m_bounding_boxes((number_of_bounding_boxes > 0)?new BoundingBox[number_of_bounding_boxes]:0),
        m_number_of_global_categories(number_of_global_categories),
        m_global_categories((number_of_global_categories > 0)?new GlobalCategory[number_of_global_categories]:0),
        m_width(width),
        m_height(height)
    {
        unsigned int index;
        copy_str(image_filename, m_database_image_filename, FILENAME_SIZE);
        copy_str(mask_filename, m_database_mask_filename, FILENAME_SIZE);
        copy_str(metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
        index = copy_str(route, m_image_filename, ROUTE_SIZE);
        copy_str(image_filename, m_image_filename, ROUTE_SIZE, index);
        if (mask_filename[0] != '\0')
        {
            index = copy_str(route, m_mask_filename, ROUTE_SIZE);
            copy_str(mask_filename, m_mask_filename, ROUTE_SIZE, index);
        }
        else m_mask_filename[0] = '\0';
        if (metadata_filename[0] != '\0')
        {
            index = copy_str(route, m_metadata_filename, ROUTE_SIZE);
            copy_str(metadata_filename, m_metadata_filename, ROUTE_SIZE, index);
        }
        else m_metadata_filename[0] = '\0';
        for (unsigned int i = 0; i < number_of_bounding_boxes; ++i) m_bounding_boxes[i] = bounding_boxes[i];
        for (unsigned int i = 0; i < number_of_global_categories; ++i) m_global_categories[i] = global_categories[i];
    }

    DatabaseImageInformation::DatabaseImageInformation(const DatabaseImageInformation &copy) :
        m_number_of_bounding_boxes(copy.m_number_of_bounding_boxes),
        m_bounding_boxes((copy.m_bounding_boxes != 0)?new BoundingBox[copy.m_number_of_bounding_boxes]:0),
        m_number_of_global_categories(copy.m_number_of_global_categories),
        m_global_categories((copy.m_number_of_global_categories > 0)?new GlobalCategory[copy.m_number_of_global_categories]:0),
        m_width(copy.m_width),
        m_height(copy.m_height)
    {
        copy_str(copy.m_image_filename, m_image_filename, ROUTE_SIZE);
        copy_str(copy.m_mask_filename, m_mask_filename, ROUTE_SIZE);
        copy_str(copy.m_metadata_filename, m_metadata_filename, ROUTE_SIZE);
        copy_str(copy.m_database_image_filename, m_database_image_filename, FILENAME_SIZE);
        copy_str(copy.m_database_mask_filename, m_database_mask_filename, FILENAME_SIZE);
        copy_str(copy.m_database_metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
        for (unsigned int i = 0; i < copy.m_number_of_bounding_boxes; ++i)
            m_bounding_boxes[i] = copy.m_bounding_boxes[i];
        for (unsigned int i = 0; i < copy.m_number_of_global_categories; ++i) m_global_categories[i] = copy.m_global_categories[i];
    }
    
    DatabaseImageInformation::~DatabaseImageInformation(void)
    {
        if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
        if (m_global_categories != 0) delete [] m_global_categories;
    }
    
    DatabaseImageInformation& DatabaseImageInformation::operator=(const DatabaseImageInformation &copy)
    {
        if (this != &copy)
        {
            // Free ...................................................................................
            if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
            if (m_global_categories != 0) delete [] m_global_categories;
            
            // Copy ...................................................................................
            copy_str(copy.m_image_filename, m_image_filename, ROUTE_SIZE);
            copy_str(copy.m_mask_filename, m_mask_filename, ROUTE_SIZE);
            copy_str(copy.m_metadata_filename, m_metadata_filename, ROUTE_SIZE);
            copy_str(copy.m_database_image_filename, m_database_image_filename, FILENAME_SIZE);
            copy_str(copy.m_database_mask_filename, m_database_mask_filename, FILENAME_SIZE);
            copy_str(copy.m_database_metadata_filename, m_database_metadata_filename, FILENAME_SIZE);
            m_number_of_bounding_boxes = copy.m_number_of_bounding_boxes;
            m_bounding_boxes = (copy.m_bounding_boxes != 0)?new BoundingBox[copy.m_number_of_bounding_boxes]:0;
            for (unsigned int i = 0; i < copy.m_number_of_bounding_boxes; ++i)
                m_bounding_boxes[i] = copy.m_bounding_boxes[i];
            m_number_of_global_categories = copy.m_number_of_global_categories;
            m_global_categories = (copy.m_number_of_global_categories > 0)?new GlobalCategory[copy.m_number_of_global_categories]:0;
            for (unsigned int i = 0; i < copy.m_number_of_global_categories; ++i) m_global_categories[i] = copy.m_global_categories[i];
            m_width = copy.m_width;
            m_height = copy.m_height;
        }
        return *this;
    }
    
    void DatabaseImageInformation::setNumberOfBoundingBoxes(unsigned int number_of_bounding_boxes)
    {
        if (number_of_bounding_boxes != 0)
        {
            BoundingBox *new_bounding_boxes;
            
            new_bounding_boxes = new BoundingBox[number_of_bounding_boxes];
            for (unsigned int i = 0; i < srvMin<unsigned int>(m_number_of_bounding_boxes, number_of_bounding_boxes); ++i)
                new_bounding_boxes[i] = m_bounding_boxes[i];
            
            if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
            m_bounding_boxes = new_bounding_boxes;
        }
        else
        {
            if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
            m_bounding_boxes = 0;
        }
        m_number_of_bounding_boxes = number_of_bounding_boxes;
    }
    
    void DatabaseImageInformation::setNumberOfGlobalCategories(unsigned int number_of_global_categories)
    {
        if (number_of_global_categories != 0)
        {
            GlobalCategory *new_global_categories;
            
            new_global_categories = new GlobalCategory[number_of_global_categories];
            for (unsigned int i = 0; i < srvMin<unsigned int>(m_number_of_global_categories, number_of_global_categories); ++i)
                new_global_categories[i] = m_global_categories[i];
            
            if (m_global_categories != 0) delete [] m_global_categories;
            m_global_categories = new_global_categories;
        }
        else
        {
            if (m_global_categories != 0) delete [] m_global_categories;
            m_global_categories = 0;
        }
        m_number_of_global_categories = number_of_global_categories;
    }
    
    void DatabaseImageInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Sample");
        parser.setAttribute("original", m_database_image_filename);
        parser.setAttribute("width", m_width);
        parser.setAttribute("height", m_height);
        if ((m_database_mask_filename[0] != '\0') || (m_database_metadata_filename[0] != '\0') || (m_number_of_bounding_boxes != 0))
        {
            parser.addChildren();
            if (m_database_mask_filename[0] != '\0')
            {
                parser.openTag("Mask");
                parser.setAttribute("filename", m_database_mask_filename);
                parser.closeTag();
            }
            if (m_database_metadata_filename[0] != '\0')
            {
                parser.openTag("Metadata");
                parser.setAttribute("filename", m_database_metadata_filename);
                parser.closeTag();
            }
            for (unsigned int i = 0; i < m_number_of_bounding_boxes; ++i)
                m_bounding_boxes[i].convertToXML(parser);
            for (unsigned int i = 0; i < m_number_of_global_categories; ++i)
                m_global_categories[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void DatabaseImageInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Sample"))
        {
            std::vector<BoundingBox*> bounding_boxes;
            std::vector<GlobalCategory* > global_categories;
            const char *filename;
            // Free -------------------------------------------------------
            m_image_filename[0] = '\0';
            m_mask_filename[0] = '\0';
            m_metadata_filename[0] = '\0';
            m_database_image_filename[0] = '\0';
            m_database_mask_filename[0] = '\0';
            m_database_metadata_filename[0] = '\0';
            if (m_bounding_boxes != 0)
            {
                delete [] m_bounding_boxes;
                m_bounding_boxes = 0;
            }
            if (m_global_categories != 0)
            {
                delete [] m_global_categories;
                m_global_categories = 0;
            }
            m_number_of_bounding_boxes = 0;
            m_number_of_global_categories = 0;
            // Restore from XML information -------------------------------
            filename = parser.getAttribute("original");
            copy_str(filename, m_database_image_filename, FILENAME_SIZE);
            copy_str(filename, m_image_filename, FILENAME_SIZE);
            m_width = parser.getAttribute("width");
            m_height = parser.getAttribute("height");
            while(!(parser.isTagIdentifier("Sample") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Mask"))
                {
                    filename = parser.getAttribute("filename");
                    copy_str(filename, m_database_mask_filename, FILENAME_SIZE);
                    copy_str(filename, m_mask_filename, FILENAME_SIZE);
                    while (!(parser.isTagIdentifier("Mask") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Metadata"))
                {
                    filename = parser.getAttribute("filename");
                    copy_str(filename, m_database_metadata_filename, FILENAME_SIZE);
                    copy_str(filename, m_metadata_filename, FILENAME_SIZE);
                    while (!(parser.isTagIdentifier("Metadata") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Bounding_Box"))
                {
                    BoundingBox *bb;
                    bb = new BoundingBox();
                    bb->convertFromXML(parser);
                    bounding_boxes.push_back(bb);
                }
                else if (parser.isTagIdentifier("Global_Category"))
                {
                    GlobalCategory *gc;
                    gc = new GlobalCategory();
                    gc->convertFromXML(parser);
                    global_categories.push_back(gc);
                }
                else parser.getNext();
            }
            parser.getNext();
            if (bounding_boxes.size() > 0)
            {
                m_number_of_bounding_boxes = (unsigned int)bounding_boxes.size();
                m_bounding_boxes = new BoundingBox[bounding_boxes.size()];
                for (unsigned int i = 0; i < bounding_boxes.size(); ++i)
                {
                    m_bounding_boxes[i] = *bounding_boxes[i];
                    delete bounding_boxes[i];
                }
            }
            if (global_categories.size() > 0)
            {
                m_number_of_global_categories = (unsigned int)global_categories.size();
                m_global_categories = new GlobalCategory[global_categories.size()];
                for (unsigned int i = 0; i < global_categories.size(); ++i)
                {
                    m_global_categories[i] = *global_categories[i];
                    delete global_categories[i];
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE INFORMATION CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ImageInformation::ImageInformation(void) :
        m_global_categories(0),
        m_number_of_global_categories(0),
        m_bounding_boxes(0),
        m_number_of_bounding_boxes(0),
        m_categories_information(0) {}
    
    ImageInformation::ImageInformation(const DatabaseImageInformation &image_information, const CategoriesInformation &categories_information) :
        m_image(image_information.getImageFilename()),
        m_global_categories((image_information.getNumberOfGlobalCategories() > 0)?new GlobalCategory[image_information.getNumberOfGlobalCategories()]:0),
        m_number_of_global_categories(image_information.getNumberOfGlobalCategories()),
        m_bounding_boxes((image_information.getNumberOfBoundingBoxes() > 0)?new BoundingBox[image_information.getNumberOfBoundingBoxes()]:0),
        m_number_of_bounding_boxes(image_information.getNumberOfBoundingBoxes()),
        m_categories_information(&categories_information)
    {
        const unsigned char background_color[3] = { categories_information.getBackgroundRed(), categories_information.getBackgroundGreen(), categories_information.getBackgroundBlue() };
        Image<unsigned char> segmentation_image;
        
        // Check if the database image have been loaded, otherwise throw an exception.
        if (m_image.getNumberOfChannels() == 1) m_image = ImageGray2RGB(m_image);
        else if (m_image.getNumberOfChannels() == 4) m_image = m_image.subimage(0, 2);
        else if (m_image.getNumberOfChannels() != 3)  throw Exception("Database image '%s' ha not been properly loaded.", image_information.getImageFilename());
        
        //  Copy the global and bounding boxes information.
        for (unsigned int i = 0; i < image_information.getNumberOfGlobalCategories(); ++i)
            m_global_categories[i] = image_information.getGlobalCategory(i);
        for (unsigned int i = 0; i < image_information.getNumberOfBoundingBoxes(); ++i)
            m_bounding_boxes[i] = image_information.getBoundingBox(i);
        
        // Load the segmentation mask.
        if (image_information.hasMask())
        {
            segmentation_image.load(image_information.getMaskFilename());
            if (segmentation_image.getNumberOfChannels() == 4) segmentation_image = segmentation_image.subimage(0, 2); // The image has an alpha channel, remove it.
            else if (segmentation_image.getNumberOfChannels() != 3) throw Exception("Database image mask '%s' cannot be loaded.", image_information.getMaskFilename());
        }
        else if (image_information.hasBoundingBoxes())
        {
            Draw::Pencil<unsigned char> bounding_box_pen;
            
            segmentation_image.setGeometry(image_information.getWidth(), image_information.getHeight(), 3);
            segmentation_image.setValues(background_color);
            bounding_box_pen.setBorderColor(0, 0, 0, 0);
            bounding_box_pen.setAntialiasing(false);
            bounding_box_pen.setBorderSize(1);
            
            for (unsigned int i = 0; i < image_information.getNumberOfBoundingBoxes(); ++i)
            {
                int x0, y0, x1, y1;
                int label;
                
                label = image_information.getBoundingBox(i).getLabel();
                x0 = (int)image_information.getBoundingBox(i).getXMin();
                y0 = (int)image_information.getBoundingBox(i).getYMin();
                x1 = (int)image_information.getBoundingBox(i).getXMax();
                y1 = (int)image_information.getBoundingBox(i).getYMax();
                if (label >= 0)
                {
                    const PartInformation *part = categories_information.getSubcategory(label);
                    bounding_box_pen.setBackgroundColor(part->getRed(), part->getGreen(), part->getBlue(), 255);
                    Draw::Rectangle(segmentation_image, x0, y0, x1, y1, bounding_box_pen);
                }
            }
        }
        else
        {
            segmentation_image.setGeometry(image_information.getWidth(), image_information.getHeight(), 3);
            unsigned int number_of_global_categories;
            
            number_of_global_categories = 0;
            for (unsigned int k = 0; k < image_information.getNumberOfGlobalCategories(); ++k)
                if (image_information.getGlobalCategory(k).getLabel() >= 0) ++number_of_global_categories;
            
            if (number_of_global_categories > 0)
            {
                unsigned char *semantic_colors[3];
                int label;
                
                semantic_colors[0] = new unsigned char[number_of_global_categories];
                semantic_colors[1] = new unsigned char[number_of_global_categories];
                semantic_colors[2] = new unsigned char[number_of_global_categories];
                
                number_of_global_categories = 0;
                for (unsigned int k = 0; k < image_information.getNumberOfGlobalCategories(); ++k)
                {
                    label = image_information.getGlobalCategory(k).getLabel();
                    
                    if (label >= 0)
                    {
                        const PartInformation *part = categories_information.getSubcategory(label);
                        
                        semantic_colors[0][number_of_global_categories] = part->getRed();
                        semantic_colors[1][number_of_global_categories] = part->getGreen();
                        semantic_colors[2][number_of_global_categories] = part->getBlue();
                        ++number_of_global_categories;
                    }
                }
                
                for (unsigned int y = 0, index = 0; y < segmentation_image.getHeight(); ++y)
                {
                    unsigned char * __restrict__ red_segmentation_image_ptr = segmentation_image.get(y, 0);
                    unsigned char * __restrict__ green_segmentation_image_ptr = segmentation_image.get(y, 1);
                    unsigned char * __restrict__ blue_segmentation_image_ptr = segmentation_image.get(y, 2);
                    
                    for (unsigned int x = 0; x < segmentation_image.getWidth(); ++x, ++index)
                    {
                        *red_segmentation_image_ptr = semantic_colors[0][index % number_of_global_categories];
                        *green_segmentation_image_ptr = semantic_colors[1][index % number_of_global_categories];
                        *blue_segmentation_image_ptr = semantic_colors[2][index % number_of_global_categories];
                    }
                }
                
                delete [] semantic_colors[0];
                delete [] semantic_colors[1];
                delete [] semantic_colors[2];
            }
            else segmentation_image.setValues(background_color);
        }
        m_pixel_semantic_information.set(segmentation_image, categories_information);
    }
    
    ImageInformation::ImageInformation(const ImageInformation &copy) :
        m_image(copy.m_image),
        m_pixel_semantic_information(copy.m_pixel_semantic_information),
        m_global_categories((copy.m_global_categories != 0)?new GlobalCategory[copy.m_number_of_global_categories]:0),
        m_number_of_global_categories(copy.m_number_of_global_categories),
        m_bounding_boxes((copy.m_bounding_boxes != 0)?new BoundingBox[copy.m_number_of_bounding_boxes]:0),
        m_number_of_bounding_boxes(copy.m_number_of_bounding_boxes),
        m_categories_information(copy.m_categories_information)
    {
        for (unsigned int i = 0; i < copy.m_number_of_global_categories; ++i)
            m_global_categories[i] = copy.m_global_categories[i];
        for (unsigned int i = 0; i < copy.m_number_of_bounding_boxes; ++i)
            m_bounding_boxes[i] = copy.m_bounding_boxes[i];
    }
    
    ImageInformation::~ImageInformation(void)
    {
        if (m_global_categories != 0) delete [] m_global_categories;
        if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
    }
    
    ImageInformation& ImageInformation::operator=(const ImageInformation &copy)
    {
        if (this != &copy)
        {
            // Free -----------------------------------------------------------------------------------
            if (m_global_categories != 0) delete [] m_global_categories;
            if (m_bounding_boxes != 0) delete [] m_bounding_boxes;
            
            // Copy -----------------------------------------------------------------------------------
            m_image = copy.m_image;
            m_pixel_semantic_information = copy.m_pixel_semantic_information;
            if (copy.m_global_categories != 0)
            {
                m_number_of_global_categories = copy.m_number_of_global_categories;
                m_global_categories = new GlobalCategory[copy.m_number_of_global_categories];
                for (unsigned int i = 0; i < copy.m_number_of_global_categories; ++i)
                    m_global_categories[i] = copy.m_global_categories[i];
            }
            else
            {
                m_global_categories = 0;
                m_number_of_global_categories = 0;
            }
            if (copy.m_bounding_boxes != 0)
            {
                m_number_of_bounding_boxes = copy.m_number_of_bounding_boxes;
                m_bounding_boxes = new BoundingBox[copy.m_number_of_bounding_boxes];
                for (unsigned int i = 0; i < copy.m_number_of_bounding_boxes; ++i)
                    m_bounding_boxes[i] = copy.m_bounding_boxes[i];
            }
            else
            {
                m_bounding_boxes = 0;
                m_number_of_bounding_boxes = 0;
            }
            m_categories_information = copy.m_categories_information;
        }
        
        return *this;
    }
    
    void ImageInformation::transformInformation(const double *transform, ImageInformation &image_information) const
    {
        // Free the image allocated in the result image information object ..........................................................................
        if (image_information.m_global_categories != 0)
        {
            delete [] image_information.m_global_categories;
            image_information.m_global_categories = 0;
        }
        if (image_information.m_bounding_boxes != 0)
        {
            delete [] image_information.m_bounding_boxes;
            image_information.m_bounding_boxes = 0;
            image_information.m_number_of_bounding_boxes = 0;
        }
        
        // Transform the images of the resulting information object .................................................................................
        image_information.m_image.setGeometry(m_image);
        ImageTransform(m_image, transform, image_information.m_image, BILINEAR);
        m_pixel_semantic_information.projectiveTransform(transform, image_information.m_pixel_semantic_information);
        
        image_information.m_number_of_global_categories = m_number_of_global_categories;
        if (m_global_categories != 0)
        {
            image_information.m_global_categories = new GlobalCategory[m_number_of_global_categories];
            for (unsigned int i = 0; i < m_number_of_global_categories; ++i)
                image_information.m_global_categories[i] = m_global_categories[i];
        }
        
        if (m_bounding_boxes != 0)
        {
            std::vector<BoundingBox> bounding_boxes;
            double normalization;
            
            normalization = srvMax<double>(m_image.getWidth(), m_image.getHeight());
            for (unsigned int i = 0; i < m_number_of_bounding_boxes; ++i)
            {
                double x0, y0, x1, y1, xaux, yaux, zaux, xr, yr;
                
                xr = (m_bounding_boxes[i].getXMin() - (double)(m_image.getWidth() / 2)) / normalization;
                yr = (m_bounding_boxes[i].getYMin() - (double)(m_image.getHeight() / 2)) / normalization;
                xaux = xr * transform[0] + yr * transform[1] + transform[2];
                yaux = xr * transform[3] + yr * transform[4] + transform[5];
                zaux = xr * transform[6] + yr * transform[7] + transform[8];
                x1 = x0 = (xaux / zaux) * normalization + (double)(m_image.getWidth() / 2);
                y1 = y0 = (xaux / zaux) * normalization + (double)(m_image.getHeight() / 2);
                
                xr = (m_bounding_boxes[i].getXMin() - (double)(m_image.getWidth() / 2)) / normalization;
                yr = (m_bounding_boxes[i].getYMax() - (double)(m_image.getHeight() / 2)) / normalization;
                xaux = xr * transform[0] + yr * transform[1] + transform[2];
                yaux = xr * transform[3] + yr * transform[4] + transform[5];
                zaux = xr * transform[6] + yr * transform[7] + transform[8];
                xr = (xaux / zaux) * normalization + (double)(m_image.getWidth() / 2);
                yr = (yaux / zaux) * normalization + (double)(m_image.getHeight() / 2);
                x0 = srvMin<double>(x0, xr);
                x1 = srvMax<double>(x1, xr);
                y0 = srvMin<double>(y0, yr);
                y1 = srvMax<double>(y1, yr);
                
                xr = (m_bounding_boxes[i].getXMax() - (double)(m_image.getWidth() / 2)) / normalization;
                yr = (m_bounding_boxes[i].getYMin() - (double)(m_image.getHeight() / 2)) / normalization;
                xaux = xr * transform[0] + yr * transform[1] + transform[2];
                yaux = xr * transform[3] + yr * transform[4] + transform[5];
                zaux = xr * transform[6] + yr * transform[7] + transform[8];
                xr = (xaux / zaux) * normalization + (double)(m_image.getWidth() / 2);
                yr = (yaux / zaux) * normalization + (double)(m_image.getHeight() / 2);
                x0 = srvMin<double>(x0, xr);
                x1 = srvMax<double>(x1, xr);
                y0 = srvMin<double>(y0, yr);
                y1 = srvMax<double>(y1, yr);
                
                xr = (m_bounding_boxes[i].getXMax() - (double)(m_image.getWidth() / 2)) / normalization;
                yr = (m_bounding_boxes[i].getYMax() - (double)(m_image.getHeight() / 2)) / normalization;
                xaux = xr * transform[0] + yr * transform[1] + transform[2];
                yaux = xr * transform[3] + yr * transform[4] + transform[5];
                zaux = xr * transform[6] + yr * transform[7] + transform[8];
                xr = (xaux / zaux) * normalization + (double)(m_image.getWidth() / 2);
                yr = (yaux / zaux) * normalization + (double)(m_image.getHeight() / 2);
                x0 = srvMin<double>(x0, xr);
                x1 = srvMax<double>(x1, xr);
                y0 = srvMin<double>(y0, yr);
                y1 = srvMax<double>(y1, yr);
                
                x0 = srvMax<double>(0, srvMin<double>((double)m_image.getWidth(), x0));
                x1 = srvMax<double>(0, srvMin<double>((double)m_image.getWidth(), x1));
                y0 = srvMax<double>(0, srvMin<double>((double)m_image.getHeight(), y0));
                y1 = srvMax<double>(0, srvMin<double>((double)m_image.getHeight(), y1));
                
                if ((x1 - x0) * (y1 - y0) > 20) // Minimum area of the transformed bounding boxes.
                {
                    BoundingBox current_box(m_bounding_boxes[i]);
                    current_box.setXMin((short)x0);
                    current_box.setXMax((short)x1);
                    current_box.setYMin((short)y0);
                    current_box.setYMax((short)y1);
                    bounding_boxes.push_back(current_box);
                }
            }
            
            if (bounding_boxes.size() > 0)
            {
                image_information.m_bounding_boxes = new BoundingBox[bounding_boxes.size()];
                image_information.m_number_of_bounding_boxes = (unsigned int)bounding_boxes.size();
                for (unsigned int i = 0; i < bounding_boxes.size(); ++i)
                    image_information.m_bounding_boxes[i] = bounding_boxes[i];
            }
        }
        image_information.m_categories_information = m_categories_information;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DATABASE INFORMATION CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    DatabaseInformation::DatabaseInformation(void) :
        m_current_categories_information(0),
        m_train_set(0),
        m_number_of_train_images(0),
        m_validation_set(0),
        m_number_of_validation_images(0),
        m_test_set(0),
        m_number_of_test_images(0) {}
    
    DatabaseInformation::DatabaseInformation(const CategoriesInformation &categories_information, const DatabaseImageInformation *train_set, unsigned int number_of_train_images, const DatabaseImageInformation *validation_set, unsigned int number_of_validation_images, const DatabaseImageInformation *test_set, unsigned int number_of_test_images) :
        m_current_categories_information(&m_categories_information),
        m_number_of_train_images(number_of_train_images),
        m_number_of_validation_images(number_of_validation_images),
        m_number_of_test_images(number_of_test_images)
    {
        m_categories_information = categories_information;
        if (number_of_train_images != 0)
        {
            m_train_set = new DatabaseImageInformation[number_of_train_images];
            for (unsigned int i = 0; i < number_of_train_images; ++i)
                m_train_set[i] = train_set[i];
        }
        else m_train_set = 0;
        if (number_of_validation_images != 0)
        {
            m_validation_set = new DatabaseImageInformation[number_of_validation_images];
            for (unsigned int i = 0; i < number_of_validation_images; ++i)
                m_validation_set[i] = validation_set[i];
        }
        else m_validation_set = 0;
        if (number_of_test_images != 0)
        {
            m_test_set = new DatabaseImageInformation[number_of_test_images];
            for (unsigned int i = 0; i < number_of_test_images; ++i)
                m_test_set[i] = test_set[i];
        }
    }
    
    DatabaseInformation::DatabaseInformation(const char *database_filename) :
        m_current_categories_information(&m_categories_information),
        m_train_set(0),
        m_number_of_train_images(0),
        m_validation_set(0),
        m_number_of_validation_images(0),
        m_test_set(0),
        m_number_of_test_images(0)
    {
        char route[512];
        int route_index;
        for (route_index = 0; database_filename[route_index] != '\0'; ++route_index)
            route[route_index] = database_filename[route_index];
        //++route_index;
        for (; route_index > 0; --route_index)
            if (route[route_index - 1] == '/') break;
        route[route_index] = '\0';
        std::ifstream database_file;
        database_file.open(database_filename);
        if (database_file.is_open())
        {
            XmlParser parser(&database_file);
            if (parser.isTagIdentifier("Database"))
            {
                while (!(parser.isTagIdentifier("Database") && parser.isCloseTag()))
                {
                    if (parser.isTagIdentifier("Categories_Information"))
                    {
                        m_categories_information.convertFromXML(parser);
                    }
                    else if (parser.isTagIdentifier("Train_Data"))
                    {
                        std::vector<DatabaseImageInformation*> samples;
                        while (!(parser.isTagIdentifier("Train_Data") && parser.isCloseTag()))
                        {
                            if (parser.isTagIdentifier("Sample"))
                            {
                                DatabaseImageInformation *dii;
                                dii = new DatabaseImageInformation();
                                dii->convertFromXML(parser);
                                dii->setDatabaseRoute(route); // Add database route to the original and mask files!
                                samples.push_back(dii);
                            }
                            else parser.getNext();
                        }
                        parser.getNext();
                        if (samples.size() > 0)
                        {
                            m_number_of_train_images = (unsigned int)samples.size();
                            m_train_set = new DatabaseImageInformation[samples.size()];
                            for (unsigned int i = 0; i < samples.size(); ++i)
                            {
                                m_train_set[i] = *samples[i];
                                delete samples[i];
                            }
                        }
                        else
                        {
                            m_train_set = 0;
                            m_number_of_train_images = 0;
                        }
                    }
                    else if (parser.isTagIdentifier("Validation_Data"))
                    {
                        std::vector<DatabaseImageInformation*> samples;
                        while (!(parser.isTagIdentifier("Validation_Data") && parser.isCloseTag()))
                        {
                            if (parser.isTagIdentifier("Sample"))
                            {
                                DatabaseImageInformation *dii;
                                dii = new DatabaseImageInformation();
                                dii->convertFromXML(parser);
                                dii->setDatabaseRoute(route); // Add database route to the original and mask files!
                                samples.push_back(dii);
                            }
                            else parser.getNext();
                        }
                        parser.getNext();
                        if (samples.size() > 0)
                        {
                            m_number_of_validation_images = (unsigned int)samples.size();
                            m_validation_set = new DatabaseImageInformation[samples.size()];
                            for (unsigned int i = 0; i < samples.size(); ++i)
                            {
                                m_validation_set[i] = *samples[i];
                                delete samples[i];
                            }
                        }
                        else
                        {
                            m_validation_set = 0;
                            m_number_of_validation_images = 0;
                        }
                    }
                    else if (parser.isTagIdentifier("Test_Data"))
                    {
                        std::vector<DatabaseImageInformation*> samples;
                        while (!(parser.isTagIdentifier("Test_Data") && parser.isCloseTag()))
                        {
                            if (parser.isTagIdentifier("Sample"))
                            {
                                DatabaseImageInformation *dii;
                                dii = new DatabaseImageInformation();
                                dii->convertFromXML(parser);
                                dii->setDatabaseRoute(route); // Add database route to the original and mask files!
                                samples.push_back(dii);
                            }
                            else parser.getNext();
                        }
                        parser.getNext();
                        if (samples.size() > 0)
                        {
                            m_number_of_test_images = (unsigned int)samples.size();
                            m_test_set = new DatabaseImageInformation[samples.size()];
                            for (unsigned int i = 0; i < samples.size(); ++i)
                            {
                                m_test_set[i] = *samples[i];
                                delete samples[i];
                            }
                        }
                        else
                        {
                            m_test_set = 0;
                            m_number_of_test_images = 0;
                        }
                    }
                    else parser.getNext();
                }
                parser.getNext();
            }
            database_file.close();
        }
        copy_str(route, m_database_route, ROUTE_SIZE);
        updateLabels(&m_categories_information);
    }
    
    DatabaseInformation::DatabaseInformation(const DatabaseInformation &copy) :
        m_current_categories_information((&copy.m_categories_information == copy.m_current_categories_information)?&m_categories_information:copy.m_current_categories_information),
        m_categories_information(copy.m_categories_information),
        m_number_of_train_images(copy.m_number_of_train_images),
        m_number_of_validation_images(copy.m_number_of_validation_images),
        m_number_of_test_images(copy.m_number_of_test_images)
    {
        if (copy.m_number_of_train_images != 0)
        {
            m_train_set = new DatabaseImageInformation[copy.m_number_of_train_images];
            for (unsigned int i = 0; i < copy.m_number_of_train_images; ++i)
                m_train_set[i] = copy.m_train_set[i];
        }
        else m_train_set = 0;
        if (copy.m_number_of_validation_images != 0)
        {
            m_validation_set = new DatabaseImageInformation[copy.m_number_of_validation_images];
            for (unsigned int i = 0; i < copy.m_number_of_validation_images; ++i)
                m_validation_set[i] = copy.m_validation_set[i];
        }
        else m_validation_set = 0;
        if (copy.m_number_of_test_images != 0)
        {
            m_test_set = new DatabaseImageInformation[copy.m_number_of_test_images];
            for (unsigned int i = 0; i < copy.m_number_of_test_images; ++i)
                m_test_set[i] = copy.m_test_set[i];
        }
        else m_test_set = 0;
    }
    
    DatabaseInformation::~DatabaseInformation(void)
    {
        if (m_train_set != 0) delete [] m_train_set;
        if (m_validation_set != 0) delete [] m_validation_set;
        if (m_test_set != 0) delete [] m_test_set;
    }
    
    DatabaseInformation& DatabaseInformation::operator=(const DatabaseInformation &copy)
    {
        if (this != &copy)
        {
            // Free -------------------------------------------------------
            if (m_train_set != 0) delete [] m_train_set;
            if (m_validation_set != 0) delete [] m_validation_set;
            if (m_test_set != 0) delete [] m_test_set;
            // Copy -------------------------------------------------------
            m_current_categories_information = (&copy.m_categories_information == copy.m_current_categories_information)?&m_categories_information:copy.m_current_categories_information;
            m_categories_information = copy.m_categories_information;
            m_number_of_train_images = copy.m_number_of_train_images;
            m_number_of_validation_images = copy.m_number_of_validation_images;
            m_number_of_test_images = copy.m_number_of_test_images;
            if (copy.m_number_of_train_images != 0)
            {
                m_train_set = new DatabaseImageInformation[copy.m_number_of_train_images];
                for (unsigned int i = 0; i < copy.m_number_of_train_images; ++i)
                    m_train_set[i] = copy.m_train_set[i];
            }
            else m_train_set = 0;
            if (copy.m_number_of_validation_images != 0)
            {
                m_validation_set = new DatabaseImageInformation[copy.m_number_of_validation_images];
                for (unsigned int i = 0; i < copy.m_number_of_validation_images; ++i)
                    m_validation_set[i] = copy.m_validation_set[i];
            }
            else m_validation_set = 0;
            if (copy.m_number_of_test_images != 0)
            {
                m_test_set = new DatabaseImageInformation[copy.m_number_of_test_images];
                for (unsigned int i = 0; i < copy.m_number_of_test_images; ++i)
                    m_test_set[i] = copy.m_test_set[i];
            }
            else m_test_set = 0;
        }
        return *this;
    }
    
    void DatabaseInformation::updateLabels(const CategoriesInformation *categories_information)
    {
        for (unsigned int i = 0; i < m_number_of_train_images; ++i)
            m_train_set[i].updateLabels(categories_information);
        for (unsigned int i = 0; i < m_number_of_validation_images; ++i)
            m_validation_set[i].updateLabels(categories_information);
        for (unsigned int i = 0; i < m_number_of_test_images; ++i)
            m_test_set[i].updateLabels(categories_information);
        m_current_categories_information = categories_information;
    }
    
    DatabaseInformation::Subset DatabaseInformation::getIterator(IMAGE_TYPE image_type, IMAGE_SET image_set) const
    {
        unsigned int number_of_images, index;
        const DatabaseImageInformation **database_images;
        // Count the number of the images of the iterator
        number_of_images = 0;
        // TRAIN GROUP IMAGES
        if ((image_set == TRAIN_SET) || (image_set == TRAIN_VALIDATION_SET) || (image_set  == ALL_SETS) || (image_set == TRAIN_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_train_images; ++i)
            {
                if (image_type == ALL_IMAGES) ++number_of_images;
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_train_set[i].hasMask()) ++number_of_images;
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_train_set[i].hasBoundingBoxes()) ++number_of_images;
            }
        }
        // VALIDATION GROUP IMAGES
        if ((image_set == VALIDATION_SET) || (image_set == TRAIN_VALIDATION_SET) || (image_set == ALL_SETS) || (image_set == VALIDATION_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_validation_images; ++i)
            {
                if (image_type == ALL_IMAGES) ++number_of_images;
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_validation_set[i].hasMask()) ++number_of_images;
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_validation_set[i].hasBoundingBoxes()) ++number_of_images;
            }
        }
        // TEST GROUP IMAGES
        if ((image_set == TEST_SET) || (image_set == ALL_SETS) || (image_set == TRAIN_TEST_SET) || (image_set == VALIDATION_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_test_images; ++i)
            {
                if (image_type == ALL_IMAGES) ++number_of_images;
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_test_set[i].hasMask()) ++number_of_images;
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_test_set[i].hasBoundingBoxes()) ++number_of_images;
            }
        }
        // Create the images array
        database_images = new const DatabaseImageInformation*[number_of_images];
        index = 0;
        // TRAIN GROUP IMAGES
        if ((image_set == TRAIN_SET) || (image_set == TRAIN_VALIDATION_SET) || (image_set  == ALL_SETS) || (image_set == TRAIN_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_train_images; ++i)
            {
                if (image_type == ALL_IMAGES)
                {
                    database_images[index] = &m_train_set[i];
                    ++index;
                }
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_train_set[i].hasMask())
                {
                    database_images[index] = &m_train_set[i];
                    ++index;
                }
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_train_set[i].hasBoundingBoxes())
                {
                    database_images[index] = &m_train_set[i];
                    ++index;
                }
            }
        }
        // VALIDATION GROUP IMAGES
        if ((image_set == VALIDATION_SET) || (image_set == TRAIN_VALIDATION_SET) || (image_set == ALL_SETS) || (image_set == VALIDATION_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_validation_images; ++i)
            {
                if (image_type == ALL_IMAGES)
                {
                    database_images[index] = &m_validation_set[i];
                    ++index;
                }
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_validation_set[i].hasMask())
                {
                    database_images[index] = &m_validation_set[i];
                    ++index;
                }
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_validation_set[i].hasBoundingBoxes())
                {
                    database_images[index] = &m_validation_set[i];
                    ++index;
                }
            }
        }
        // TEST GROUP IMAGES
        if ((image_set == TEST_SET) || (image_set == ALL_SETS) || (image_set == TRAIN_TEST_SET) || (image_set == VALIDATION_TEST_SET))
        {
            for (unsigned int i = 0; i < m_number_of_test_images; ++i)
            {
                if (image_type == ALL_IMAGES)
                {
                    database_images[index] = &m_test_set[i];
                    ++index;
                }
                else if ((image_type == SEGMENTATION_IMAGES_ONLY) && m_test_set[i].hasMask())
                {
                    database_images[index] = &m_test_set[i];
                    ++index;
                }
                else if ((image_type == BOUNDINGBOXES_IMAGES_ONLY) && m_test_set[i].hasBoundingBoxes())
                {
                    database_images[index] = &m_test_set[i];
                    ++index;
                }
            }
        }
        
        Subset iterator(database_images, number_of_images, m_current_categories_information);
        delete [] database_images;
        
        return iterator;
    }
    
    void DatabaseInformation::setDatabaseRoute(const char *route)
    {
        for (unsigned int i = 0; i < m_number_of_train_images; ++i)
            m_train_set[i].setDatabaseRoute(route);
        for (unsigned int i = 0; i < m_number_of_validation_images; ++i)
            m_validation_set[i].setDatabaseRoute(route);
        for (unsigned int i = 0; i < m_number_of_test_images; ++i)
            m_test_set[i].setDatabaseRoute(route);
        copy_str(route, m_database_route, ROUTE_SIZE);
    }
    
    void DatabaseInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Database");
        parser.addChildren();
        m_categories_information.convertToXML(parser);
        if (m_number_of_train_images > 0)
        {
            parser.openTag("Train_Data");
            parser.addChildren();
            for (unsigned int i = 0; i < m_number_of_train_images; ++i)
                m_train_set[i].convertToXML(parser);
            parser.closeTag();
        }
        if (m_number_of_validation_images > 0)
        {
            parser.openTag("Validation_Data");
            parser.addChildren();
            for (unsigned int i = 0; i < m_number_of_validation_images; ++i)
                m_validation_set[i].convertToXML(parser);
            parser.closeTag();
        }
        if (m_number_of_test_images > 0)
        {
            parser.openTag("Test_Data");
            parser.addChildren();
            for (unsigned int i = 0; i < m_number_of_test_images; ++i)
                m_test_set[i].convertToXML(parser);
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    void DatabaseInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Database"))
        {
            if (m_train_set != 0) delete [] m_train_set;
            if (m_validation_set != 0) delete [] m_validation_set;
            if (m_test_set != 0) delete [] m_test_set;
            m_number_of_train_images = 0;
            m_number_of_validation_images = 0;
            m_number_of_test_images = 0;
            m_train_set = 0;
            m_validation_set = 0;
            m_test_set = 0;
            while (!(parser.isTagIdentifier("Database") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Categories_Information"))
                {
                    m_categories_information.convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("Train_Data"))
                {
                    std::vector<DatabaseImageInformation*> samples;
                    while (!(parser.isTagIdentifier("Train_Data") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Sample"))
                        {
                            DatabaseImageInformation *dii;
                            dii = new DatabaseImageInformation();
                            dii->convertFromXML(parser);
                            samples.push_back(dii);
                        }
                        else parser.getNext();
                    }
                    parser.getNext();
                    if (samples.size() > 0)
                    {
                        m_number_of_train_images = (unsigned int)samples.size();
                        m_train_set = new DatabaseImageInformation[samples.size()];
                        for (unsigned int i = 0; i < samples.size(); ++i)
                        {
                            m_train_set[i] = *samples[i];
                            delete samples[i];
                        }
                    }
                    else
                    {
                        m_train_set = 0;
                        m_number_of_train_images = 0;
                    }
                }
                else if (parser.isTagIdentifier("Validation_Data"))
                {
                    std::vector<DatabaseImageInformation*> samples;
                    while (!(parser.isTagIdentifier("Validation_Data") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Sample"))
                        {
                            DatabaseImageInformation *dii;
                            dii = new DatabaseImageInformation();
                            dii->convertFromXML(parser);
                            samples.push_back(dii);
                        }
                        else parser.getNext();
                    }
                    parser.getNext();
                    if (samples.size() > 0)
                    {
                        m_number_of_validation_images = (unsigned int)samples.size();
                        m_validation_set = new DatabaseImageInformation[samples.size()];
                        for (unsigned int i = 0; i < samples.size(); ++i)
                        {
                            m_validation_set[i] = *samples[i];
                            delete samples[i];
                        }
                    }
                    else
                    {
                        m_validation_set = 0;
                        m_number_of_validation_images = 0;
                    }
                }
                else if (parser.isTagIdentifier("Test_Data"))
                {
                    std::vector<DatabaseImageInformation*> samples;
                    while (!(parser.isTagIdentifier("Test_Data") && parser.isCloseTag()))
                    {
                        if (parser.isTagIdentifier("Sample"))
                        {
                            DatabaseImageInformation *dii;
                            dii = new DatabaseImageInformation();
                            dii->convertFromXML(parser);
                            samples.push_back(dii);
                        }
                        else parser.getNext();
                    }
                    parser.getNext();
                    if (samples.size() > 0)
                    {
                        m_number_of_test_images = (unsigned int)samples.size();
                        m_test_set = new DatabaseImageInformation[samples.size()];
                        for (unsigned int i = 0; i < samples.size(); ++i)
                        {
                            m_test_set[i] = *samples[i];
                            delete samples[i];
                        }
                    }
                    else
                    {
                        m_test_set = 0;
                        m_number_of_test_images = 0;
                    }
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DATABASE INFORMATION ITERATOR (INNER)|
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    DatabaseInformation::Subset::Subset(void)
    {
        m_number_of_images = 0;
        m_images_information = 0;
        m_inner_pointer = 0;
    }
    
    DatabaseInformation::Subset::Subset(const DatabaseImageInformation **&iterative_array, unsigned int number_of_images, const CategoriesInformation *categories_information) : m_number_of_images(number_of_images), m_categories_information(categories_information)
    {
        if (number_of_images != 0)
        {
            m_images_information = new const DatabaseImageInformation*[number_of_images];
            for (unsigned int i = 0; i < number_of_images; ++i) m_images_information[i] = iterative_array[i];
            m_inner_pointer = m_images_information;
        }
        else m_images_information = m_inner_pointer = 0;
    }
    
    DatabaseInformation::Subset::Subset(const DatabaseInformation::Subset &copy) :
        m_number_of_images(copy.m_number_of_images),
        m_categories_information(copy.m_categories_information)
    {
        if (copy.m_images_information != 0)
        {
            m_images_information = new const DatabaseImageInformation*[copy.m_number_of_images];
            for (unsigned int i = 0; i < copy.m_number_of_images; ++i)
                m_images_information[i] = copy.m_images_information[i];
            m_inner_pointer = m_images_information;
        }
        else m_images_information = 0;
    }
    
    DatabaseInformation::Subset::~Subset(void)
    {
        if (m_images_information != 0) delete [] m_images_information;
    }
    
    DatabaseInformation::Subset& DatabaseInformation::Subset::operator=(const DatabaseInformation::Subset &copy)
    {
        if (this != &copy)
        {
            // Free -----------------------------------------------
            if (m_images_information != 0)
                delete [] m_images_information;
            // Copy -----------------------------------------------
            if (copy.m_images_information != 0)
            {
                m_number_of_images = copy.m_number_of_images;
                m_categories_information = copy.m_categories_information;
                m_images_information = new const DatabaseImageInformation*[copy.m_number_of_images];
                for (unsigned int i = 0; i < copy.m_number_of_images; ++i)
                    m_images_information[i] = copy.m_images_information[i];
                m_inner_pointer = m_images_information;
            }
            else m_images_information = 0;
        }
        return *this;
    }
    
    void DatabaseInformation::Subset::getOriginalColorImage(Image<unsigned char> &result) const
    {
        const char *image_filename = (*m_inner_pointer)->getImageFilename();
        result.load(image_filename);
        
        if (result.getNumberOfChannels() == 1) result = ImageRGB2Gray(result);
        else if (result.getNumberOfChannels() == 4) result = result.subimage(0, 2);
        else if (result.getNumberOfChannels() != 3) throw Exception("Incorrect number of channels in the database image '%s'.", image_filename);
    }
    
    void DatabaseInformation::Subset::getOriginalColorImage(unsigned int index, Image<unsigned char> &result) const
    {
        const char *image_filename = m_images_information[index]->getImageFilename();
        result.load(image_filename);
        
        if (result.getNumberOfChannels() == 1) result = ImageGray2RGB(result);
        else if (result.getNumberOfChannels() == 4) result = result.subimage(0, 2);
        else if (result.getNumberOfChannels() != 3) throw Exception("Incorrect number of channels in the database image '%s'.", image_filename);
    }
    
    void DatabaseInformation::Subset::getOriginalGrayImage(Image<unsigned char> &result) const
    {
        const char *image_filename = (*m_inner_pointer)->getImageFilename();
        result.load(image_filename);
        
        if (result.getNumberOfChannels() == 3) result = ImageRGB2Gray(result);
        else if (result.getNumberOfChannels() == 4) result = ImageRGB2Gray(result.subimage(0, 2));
        else if (result.getNumberOfChannels() != 1) throw Exception("Incorrect number of channels in the database image '%s'.", image_filename);
    }
    
    void DatabaseInformation::Subset::getOriginalGrayImage(unsigned int index, Image<unsigned char> &result) const
    {
        const char *image_filename = m_images_information[index]->getImageFilename();
        result.load(image_filename);
        
        if (result.getNumberOfChannels() == 3) result = ImageRGB2Gray(result);
        else if (result.getNumberOfChannels() == 4) result = ImageRGB2Gray(result.subimage(0, 2));
        else if (result.getNumberOfChannels() != 1) throw Exception("Incorrect number of channels in the database image '%s'.", image_filename);
    }
    
    void DatabaseInformation::Subset::getMaskImage(Image<unsigned char> &result) const
    {
        if (!(*m_inner_pointer)->hasMask()) throw Exception("The image pointed by the internal subset pointer has no mask.");
        const char *image_filename = (*m_inner_pointer)->getMaskFilename();
        
        result.load(image_filename);
        if (result.getNumberOfChannels() == 4) result = result.subimage(0, 2); // The image has an alpha channel which is ignored.
        else if (result.getNumberOfChannels() != 3) throw Exception("Incorrect number of channels in the mask image '%s'.", image_filename);
    }
    
    void DatabaseInformation::Subset::getMaskImage(unsigned int index, Image<unsigned char> &result) const
    {
        if (!m_images_information[index]->hasMask()) throw Exception("The image pointed by the internal subset pointer has no mask.");
        const char *image_filename = m_images_information[index]->getMaskFilename();
        
        result.load(image_filename);
        if (result.getNumberOfChannels() == 4) result = result.subimage(0, 2); // The image has an alpha channel which is ignored.
        else if (result.getNumberOfChannels() != 3) throw Exception("Incorrect number of channels in the mask image '%s'.", image_filename);
    }
    
    PixelSemanticInformation* DatabaseInformation::Subset::getPixelSegmentationInformation(void) const
    {
        const unsigned char background_color[3] = { m_categories_information->getBackgroundRed(), m_categories_information->getBackgroundGreen(), m_categories_information->getBackgroundBlue() };
        const DatabaseImageInformation *image_information = *m_inner_pointer;
        PixelSemanticInformation *pixel_semantic_information;
        Image<unsigned char> segmentation_image;
        
        // Load the segmentation mask.
        if (image_information->hasMask())
        {
            segmentation_image.load(image_information->getMaskFilename());
            if (segmentation_image.getNumberOfChannels() == 4) segmentation_image = segmentation_image.subimage(0, 2); // The image has an alpha channel, remove it.
            else if (segmentation_image.getNumberOfChannels() != 3) throw Exception("Database image mask '%s' cannot be loaded.", image_information->getMaskFilename());
        }
        else if (image_information->hasBoundingBoxes())
        {
            Draw::Pencil<unsigned char> bounding_box_pen;
            
            segmentation_image.setGeometry(image_information->getWidth(), image_information->getHeight(), 3);
            segmentation_image.setValues(background_color);
            bounding_box_pen.setBorderColor(0, 0, 0, 0);
            bounding_box_pen.setAntialiasing(false);
            bounding_box_pen.setBorderSize(1);
            
            for (unsigned int i = 0; i < image_information->getNumberOfBoundingBoxes(); ++i)
            {
                int x0, y0, x1, y1;
                int label;
                
                label = image_information->getBoundingBox(i).getLabel();
                x0 = (int)image_information->getBoundingBox(i).getXMin();
                y0 = (int)image_information->getBoundingBox(i).getYMin();
                x1 = (int)image_information->getBoundingBox(i).getXMax();
                y1 = (int)image_information->getBoundingBox(i).getYMax();
                if (label >= 0)
                {
                    const PartInformation *part = m_categories_information->getSubcategory(label);
                    bounding_box_pen.setBackgroundColor(part->getRed(), part->getGreen(), part->getBlue(), 255);
                    Draw::Rectangle(segmentation_image, x0, y0, x1, y1, bounding_box_pen);
                }
            }
        }
        else
        {
            segmentation_image.setGeometry(image_information->getWidth(), image_information->getHeight(), 3);
            unsigned int number_of_global_categories;
            
            number_of_global_categories = 0;
            for (unsigned int k = 0; k < image_information->getNumberOfGlobalCategories(); ++k)
                if (image_information->getGlobalCategory(k).getLabel() >= 0) ++number_of_global_categories;
            
            if (number_of_global_categories > 0)
            {
                unsigned char *semantic_colors[3];
                int label;
                
                semantic_colors[0] = new unsigned char[number_of_global_categories];
                semantic_colors[1] = new unsigned char[number_of_global_categories];
                semantic_colors[2] = new unsigned char[number_of_global_categories];
                
                number_of_global_categories = 0;
                for (unsigned int k = 0; k < image_information->getNumberOfGlobalCategories(); ++k)
                {
                    label = image_information->getGlobalCategory(k).getLabel();
                    
                    if (label >= 0)
                    {
                        const PartInformation *part = m_categories_information->getSubcategory(label);
                        
                        semantic_colors[0][number_of_global_categories] = part->getRed();
                        semantic_colors[1][number_of_global_categories] = part->getGreen();
                        semantic_colors[2][number_of_global_categories] = part->getBlue();
                        ++number_of_global_categories;
                    }
                }
                
                for (unsigned int y = 0, index = 0; y < segmentation_image.getHeight(); ++y)
                {
                    unsigned char * __restrict__ red_segmentation_image_ptr = segmentation_image.get(y, 0);
                    unsigned char * __restrict__ green_segmentation_image_ptr = segmentation_image.get(y, 1);
                    unsigned char * __restrict__ blue_segmentation_image_ptr = segmentation_image.get(y, 2);
                    
                    for (unsigned int x = 0; x < segmentation_image.getWidth(); ++x, ++index)
                    {
                        *red_segmentation_image_ptr = semantic_colors[0][index % number_of_global_categories];
                        *green_segmentation_image_ptr = semantic_colors[1][index % number_of_global_categories];
                        *blue_segmentation_image_ptr = semantic_colors[2][index % number_of_global_categories];
                    }
                }
                
                delete [] semantic_colors[0];
                delete [] semantic_colors[1];
                delete [] semantic_colors[2];
            }
            else segmentation_image.setValues(background_color);
        }
        pixel_semantic_information = new PixelSemanticInformation(segmentation_image, *m_categories_information);
        
        return pixel_semantic_information;
    }
    
    PixelSemanticInformation* DatabaseInformation::Subset::getPixelSegmentationInformation(unsigned int index) const
    {
        const unsigned char background_color[3] = { m_categories_information->getBackgroundRed(), m_categories_information->getBackgroundGreen(), m_categories_information->getBackgroundBlue() };
        const DatabaseImageInformation *image_information = m_images_information[index];
        PixelSemanticInformation *pixel_semantic_information;
        Image<unsigned char> segmentation_image;
        
        // Load the segmentation mask.
        if (image_information->hasMask())
        {
            segmentation_image.load(image_information->getMaskFilename());
            if (segmentation_image.getNumberOfChannels() == 4) segmentation_image = segmentation_image.subimage(0, 2); // The image has an alpha channel, remove it.
            else if (segmentation_image.getNumberOfChannels() != 3) throw Exception("Database image mask '%s' cannot be loaded.", image_information->getMaskFilename());
        }
        else if (image_information->hasBoundingBoxes())
        {
            Draw::Pencil<unsigned char> bounding_box_pen;
            
            segmentation_image.setGeometry(image_information->getWidth(), image_information->getHeight(), 3);
            segmentation_image.setValues(background_color);
            bounding_box_pen.setBorderColor(0, 0, 0, 0);
            bounding_box_pen.setAntialiasing(false);
            bounding_box_pen.setBorderSize(1);
            
            for (unsigned int i = 0; i < image_information->getNumberOfBoundingBoxes(); ++i)
            {
                int x0, y0, x1, y1;
                int label;
                
                label = image_information->getBoundingBox(i).getLabel();
                x0 = (int)image_information->getBoundingBox(i).getXMin();
                y0 = (int)image_information->getBoundingBox(i).getYMin();
                x1 = (int)image_information->getBoundingBox(i).getXMax();
                y1 = (int)image_information->getBoundingBox(i).getYMax();
                if (label >= 0)
                {
                    const PartInformation *part = m_categories_information->getSubcategory(label);
                    bounding_box_pen.setBackgroundColor(part->getRed(), part->getGreen(), part->getBlue(), 255);
                    Draw::Rectangle(segmentation_image, x0, y0, x1, y1, bounding_box_pen);
                }
            }
        }
        else
        {
            segmentation_image.setGeometry(image_information->getWidth(), image_information->getHeight(), 3);
            unsigned int number_of_global_categories;
            
            number_of_global_categories = 0;
            for (unsigned int k = 0; k < image_information->getNumberOfGlobalCategories(); ++k)
                if (image_information->getGlobalCategory(k).getLabel() >= 0) ++number_of_global_categories;
            
            if (number_of_global_categories > 0)
            {
                unsigned char *semantic_colors[3];
                int label;
                
                semantic_colors[0] = new unsigned char[number_of_global_categories];
                semantic_colors[1] = new unsigned char[number_of_global_categories];
                semantic_colors[2] = new unsigned char[number_of_global_categories];
                
                number_of_global_categories = 0;
                for (unsigned int k = 0; k < image_information->getNumberOfGlobalCategories(); ++k)
                {
                    label = image_information->getGlobalCategory(k).getLabel();
                    
                    if (label >= 0)
                    {
                        const PartInformation *part = m_categories_information->getSubcategory(label);
                        
                        semantic_colors[0][number_of_global_categories] = part->getRed();
                        semantic_colors[1][number_of_global_categories] = part->getGreen();
                        semantic_colors[2][number_of_global_categories] = part->getBlue();
                        ++number_of_global_categories;
                    }
                }
                
                for (unsigned int y = 0, category_index = 0; y < segmentation_image.getHeight(); ++y)
                {
                    unsigned char * __restrict__ red_segmentation_image_ptr = segmentation_image.get(y, 0);
                    unsigned char * __restrict__ green_segmentation_image_ptr = segmentation_image.get(y, 1);
                    unsigned char * __restrict__ blue_segmentation_image_ptr = segmentation_image.get(y, 2);
                    
                    for (unsigned int x = 0; x < segmentation_image.getWidth(); ++x, ++category_index)
                    {
                        *red_segmentation_image_ptr = semantic_colors[0][category_index % number_of_global_categories];
                        *green_segmentation_image_ptr = semantic_colors[1][category_index % number_of_global_categories];
                        *blue_segmentation_image_ptr = semantic_colors[2][category_index % number_of_global_categories];
                    }
                }
                
                delete [] semantic_colors[0];
                delete [] semantic_colors[1];
                delete [] semantic_colors[2];
            }
            else segmentation_image.setValues(background_color);
        }
        pixel_semantic_information = new PixelSemanticInformation(segmentation_image, *m_categories_information);
        
        return pixel_semantic_information;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

