// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_XML_HPP_HEADER_FILE__
#define __SRV_XML_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <cstdio>
#include <cstdlib>
// -[ SRV header files ]-----------------------------------------------
#include "srv_utilities.hpp"

namespace srv
{
    const unsigned int XML_STRING_SIZE = 1024;
    enum ScanCategory {SOpenTag = 1001, SCloseTag, SString, SAssignation, SIdentifier, SSlash};
    
    class ExceptionXmlEof : public std::exception {};
    
    class Proxy
    {
    public:
        Proxy(const char *data) { m_data = data; }
        ~Proxy(void) {}
        inline operator const char*() { return m_data; }
        inline operator bool() { return atoi(m_data) == 1; }
        inline operator char() { return (char)atoi(m_data); }
        inline operator unsigned char() { return (unsigned char)atoi(m_data); }
        inline operator short() { return (short)atoi(m_data); }
        inline operator unsigned short() { return (unsigned short)atoi(m_data); }
        inline operator int() { return atoi(m_data); }
        inline operator unsigned int() { return (unsigned int)atoi(m_data); }
        inline operator long() { return atol(m_data); }
        inline operator unsigned long() { return (unsigned long)atol(m_data); }
        inline operator float()
        {
            float value;
            std::istringstream istr(m_data);
            istr.imbue(std::locale("C"));
            istr >> value;
            return value;
        }
        inline operator double()
        {
            double value;
            std::istringstream istr(m_data);
            istr.imbue(std::locale("C"));
            istr >> value;
            return value;
        }
    private:
        const char *m_data;
    };
    
    class XmlParser
    {
    public:
        XmlParser(std::istream *input_stream, bool attributes_case_sensitive = false);
        XmlParser(std::ostream *output_stream, bool attributes_case_sensitive = false);
        ~XmlParser(void);
        // WRITTING XML FUNCTIONS
        void openTag(const char *name);
        void addChildren(void);
        void closeTag(void);
        // *************************************************************************
        inline void writeData(const char *value) {*m_output_stream << value;}
        inline void writeData(unsigned long value) {*m_output_stream << value;}
        inline void writeData(long value) {*m_output_stream << value;}
        inline void writeData(unsigned int value) {*m_output_stream << value;}
        inline void writeData(int value) {*m_output_stream << value;}
        inline void writeData(bool value) {*m_output_stream << value;}
        inline void writeData(unsigned short value) {*m_output_stream << value;}
        inline void writeData(short value) {*m_output_stream << value;}
        inline void writeData(unsigned char value) {*m_output_stream << value;}
        inline void writeData(char value) {*m_output_stream << value;}
        inline void writeData(float value) {*m_output_stream << value;}
        inline void writeData(double value) {*m_output_stream << value;}
        // *************************************************************************
        inline void setSucceedingAttribute(const char *name, const char *value) {m_succeeding_attributes[name] = value;}
        inline void setSucceedingAttribute(const char *name, unsigned long value) {char data[XML_STRING_SIZE]; sprintf(data, "%lu", value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, long value) {char data[XML_STRING_SIZE]; sprintf(data, "%ld", value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, unsigned int value) {char data[XML_STRING_SIZE]; sprintf(data, "%u", value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, int value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, bool value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, unsigned short value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, short value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, unsigned char value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, char value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, float value) {char data[XML_STRING_SIZE]; sprintf(data, "%.12e", (double)value); m_succeeding_attributes[name] = data;}
        inline void setSucceedingAttribute(const char *name, double value) {char data[XML_STRING_SIZE]; sprintf(data, "%.12e", value); m_succeeding_attributes[name] = data;}
        // *************************************************************************
        inline void setAttribute(const char *name, const char *value) { m_attributes[name] = value; }
        inline void setAttribute(const char *name, unsigned long value) {char data[XML_STRING_SIZE]; sprintf(data, "%lu", value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, long value) {char data[XML_STRING_SIZE]; sprintf(data, "%ld", value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, unsigned int value) {char data[XML_STRING_SIZE]; sprintf(data, "%u", value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, int value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, bool value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, unsigned short value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, short value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, unsigned char value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, char value) {char data[XML_STRING_SIZE]; sprintf(data, "%d", (int)value); m_attributes[name] = data;}
        inline void setAttribute(const char *name, float value)
        {
            char data[XML_STRING_SIZE];
            sprintf(data, "%.20e", (double)value);
            // Safety code: Check that the decimal point is saved as a dot not a comma. System locale configuration can modify the way that decimal numbers are represented.
            // Therefore, when a comma is used as a decimal point the atof functions fails to recover the original real value.
            for (unsigned int i = 0; (data[i] != '\0') && (i < XML_STRING_SIZE); ++i)
                if (data[i] == ',') {data[i] = '.'; break;}
            m_attributes[name] = data;
        }
        inline void setAttribute(const char *name, double value)
        {
            char data[XML_STRING_SIZE];
            sprintf(data, "%.20e", value);
            // Safety code: Check that the decimal point is saved as a dot not a comma. System locale configuration can modify the way that decimal numbers are represented.
            // Therefore, when a comma is used as a decimal point the atof functions fails to recover the original real value.
            for (unsigned int i = 0; (data[i] != '\0') && (i < XML_STRING_SIZE); ++i)
                if (data[i] == ',') {data[i] = '.'; break;}
            m_attributes[name] = data;
        }
        // -- READING XML PARSER FILE MEMBER FUNCTIONS -----------------------------
        inline bool isCloseTag(void) const {return m_is_close_tag;}
        inline bool isValue(void) const {return m_is_value;}
        inline const char* getStringValue(void) const {return m_node_data.c_str();}
        inline const char* getValue(void) const {return m_value;}
        inline void getNext(void)
        {
            ++m_token;
            if (m_is_eof) return;
            try
            {
                Parser();
            }
            catch (ExceptionXmlEof &e)
            {
                if (m_tag_identifier.size() > 0)
                    ThrowException("Unexpected end of file.");
                m_is_eof = true;
            }
        }
        inline const char * getTagIdentifier(void) const { return m_current_tag_identifier.c_str(); }
        inline bool isTagIdentifier(const char *identifier) { return m_current_tag_identifier == identifier; }
        inline Proxy getAttribute(const char *attribute_name) const
        {
            std::map<std::string, std::string>::const_iterator value;
            if (!m_attributes_case_sensitive)
            {
                char local_name[XML_STRING_SIZE];
                unsigned int index;
                
                for (index = 0; attribute_name[index] != '\0'; ++index)
                {
                    if ((attribute_name[index] >= 'A') && (attribute_name[index] <= 'Z'))
                        local_name[index] = (char)((int)attribute_name[index] + ((int)'a' - (int)'A'));
                    else local_name[index] = attribute_name[index];
                }
                local_name[index] = '\0';
                value = m_attributes.find(local_name);
            }
            else value = m_attributes.find(attribute_name);
            
            if (value != m_attributes.end()) return Proxy(value->second.c_str());
            else
            {
                char error_message[512];
                sprintf(error_message, "Attribute '%s' is not present in the current tag.", attribute_name);
                ThrowException(error_message);
            }
            return Proxy("");
        }
        inline unsigned int getNumberOfAttributes(void) const {return (unsigned int)m_attributes.size();}
        inline void firstAttribute(void) {m_attributes_iterator = m_attributes.begin();}
        inline Proxy getAttribute(void) const {return Proxy(m_attributes_iterator->second.c_str());}
        inline void nextAttribute(void) {++m_attributes_iterator;}
        inline bool isEof(void) const {return m_is_eof;}
        inline void ThrowException(const char *error) const {throw Exception("Line %d:%s", m_line + 1, error);}
        inline int getTokenCounter(void) const { return m_token; }
    private:
        void specialCharacters(const char * expected_values, unsigned int expected_size, char expected_character, std::list<char> &buffer, unsigned int &index);
        void Scanner(bool data = false);
        void Parser(void);
        inline int read(void)
        {
            int c = m_input_stream->get();
            if (m_input_stream->eof()) throw ExceptionXmlEof();
            if (c == '\n') ++m_line;
            return c;
        }
        inline void putback(char value)
        {
            if (value == '\n') --m_line;
            m_input_stream->putback(value);
        }
        // Parser information.
        ScanCategory m_category;
        char m_string_value[XML_STRING_SIZE];
        char m_value[XML_STRING_SIZE];
        int m_line;
        int m_token;
        // XML information.
        bool m_is_close_tag;
        bool m_is_value;
        bool m_is_eof;
        std::string m_node_data;
        std::map<std::string, std::string> m_attributes;
        std::map<std::string, std::string> m_succeeding_attributes;
        std::map<std::string, std::string>::const_iterator m_attributes_iterator;
        std::vector<std::string> m_tag_identifier;
        std::string m_current_tag_identifier;
        std::istream *m_input_stream;
        std::ostream *m_output_stream;
        bool m_attributes_case_sensitive;
    };
    /// Converts the value of a char (0 <= value < 64) form its value to the base 64 representation.
    inline char toBase64(char value)
    {
        if (value < 10) return (char)('0' + value);
        else if (value < 36) return (char)('A' + (value - 10));
        else if (value < 62) return (char)('a' + (value - 36));
        else if (value == 62) return '.';
        else if (value == 63) return '_';
        return '#';
    }
    /// Converts a base 64 character to its integer value
    inline char fromBase64(char value)
    {
        if ((value >= '0') && (value <= '9')) return (char)(value - '0');
        else if ((value >= 'A') && (value <= 'Z')) return (char)((value - 'A') + 10);
        else if ((value >= 'a') && (value <= 'z')) return (char)((value - 'a') + 36);
        else if (value == '.') return 62;
        else if (value == '_') return 63;
        return 64;
    }
    /// Converts a triplet of char values to four values in base 64.
    inline void toBase64(const char *input, char *output)
    {
        output[0] = toBase64((char)((input[0] & 0xFC) >> 2));
        output[1] = toBase64((char)(((input[0] & 0x03) << 4) | ((input[1] & 0xF0) >> 4)));
        output[2] = toBase64((char)(((input[1] & 0x0F) << 2) | ((input[2] & 0xC0) >> 6)));
        output[3] = toBase64((char)(input[2] & 0x3F));
    }
    /// Converts a quadruplet of char values in base 64 to a tripled with their original values.
    inline void fromBase64(const char *input, char *output)
    {
        char val[4];
        val[0] = fromBase64(input[0]);
        val[1] = fromBase64(input[1]);
        val[2] = fromBase64(input[2]);
        val[3] = fromBase64(input[3]);
        output[0] = (char)(((val[0] & 0x3F) << 2) | ((val[1] & 0x30) >> 4));
        output[1] = (char)(((val[1] & 0x0F) << 4) | ((val[2] & 0x3C) >> 2));
        output[2] = (char)(((val[2] & 0x03) << 6) | (val[3] & 0x3F));
    }
    
    /** Function which converts the content of the array into data of an XML object. Data
     *  is stored in binary format but using base 64 (i.e. using all letters (48), numbers (10),
     *  character '.' and character '_').
     *  \param[in] parser XML parser of the object.
     *  \param[in] array array of stored into the XML data.
     *  \param[in] n number of elements in the array.
     */
    template <class T>
    inline void writeToBase64Array(XmlParser &parser, const T * array, unsigned long n)
    {
        char BLOCK[XML_STRING_SIZE];
        unsigned long char_size;
        unsigned int j;
        const char *array_char_ptr;
        
        char_size = n * sizeof(T);
        array_char_ptr = (char*)array;
        j = 0;
        for (unsigned int i = 0; i < char_size; i += 3) // Blocks of three!
        {
            const unsigned int current_block_size = srvMin<unsigned int>((unsigned int)(char_size - i), 3);
            char local_input[] = {0, 0, 0};
            for (unsigned int k = 0; k < current_block_size; ++k)
                local_input[k] = array_char_ptr[k];
            toBase64(local_input, &BLOCK[j]);
            j += 4;
            if (j > XML_STRING_SIZE - 6)
            {
                BLOCK[j] = '\n';
                BLOCK[j + 1] = '\0';
                parser.writeData(BLOCK);
                j = 0;
            }
            array_char_ptr += 3;
        }
        if (j > 0)
        {
            BLOCK[j] = '\n';
            BLOCK[j + 1] = '\0';
            parser.writeData(BLOCK);
        }
    }
    
    /** Function which converts the content of the array into data of an XML object. Data
     *  is stored in binary format but using base 64 (i.e. using all letters (48), numbers (10),
     *  character '.' and character '_').
     *  \param[in] parser XML parser of the object.
     *  \param[in] array array of tuples stored into the XML data.
     *  \param[in] n number of elements in the array.
     */
    template <class T, class N>
    inline void writeToBase64Array(XmlParser &parser, const Tuple<T, N> * array, unsigned long n)
    {
        char BLOCK[XML_STRING_SIZE];
        unsigned long char_size;
        unsigned int j;
        const char *array_char_ptr;
        Tuple<T, N> * local_array;
        
        // ========================================================================================
        // Patch: The local copy of the tuples is done to avoid uninitialized problems with
        // VALGRIND.
        // ----------------------------------------------------------------------------------------
        local_array = new Tuple<T, N>[n];
        for (unsigned long i = 0; i < n; ++i)
        {
            local_array[i].setToZero();
            local_array[i].setFirst(array[i].getFirst());
            local_array[i].setSecond(array[i].getSecond());
        }
        // ========================================================================================
        char_size = n * sizeof(Tuple<T, N>);
        array_char_ptr = (char*)local_array;
        j = 0;
        for (unsigned int i = 0; i < char_size; i += 3) // Blocks of three!
        {
            const unsigned int current_block_size = srvMin<unsigned int>((unsigned int)(char_size - i), 3);
            char local_input[] = {0, 0, 0};
            for (unsigned int k = 0; k < current_block_size; ++k)
                local_input[k] = array_char_ptr[k];
            toBase64(local_input, &BLOCK[j]);
            j += 4;
            if (j > XML_STRING_SIZE - 6)
            {
                BLOCK[j] = '\n';
                BLOCK[j + 1] = '\0';
                parser.writeData(BLOCK);
                j = 0;
            }
            array_char_ptr += 3;
        }
        if (j > 0)
        {
            BLOCK[j] = '\n';
            BLOCK[j + 1] = '\0';
            parser.writeData(BLOCK);
        }
        delete [] local_array;
    }
    
    // Loading arrays of N values in base 64 to their original type.
    template <class T>
    inline void readFromBase64Array(XmlParser &parser, const char * end_tag, T * array, unsigned long n)
    {
        unsigned long char_size;
        char *char_array;
        
        char_array = (char*)array;
        char_size = sizeof(T) * n;
        for (unsigned long i = 0; !(parser.isTagIdentifier(end_tag) && parser.isCloseTag());)
        {
            if (parser.isValue())
            {
                if (i < char_size)
                {
                    const char *BLOCK = parser.getValue();
                    for (unsigned long index = 0; (index <= XML_STRING_SIZE - 6) && (BLOCK[index] != '\0'); index += 4)
                    {
                        char local_values[3];
                        fromBase64(&BLOCK[index], local_values);
                        for (unsigned int k = 0; (k < 3) && (i < char_size); ++k, ++i)
                            char_array[i] = local_values[k];
                    }
                }
                parser.getNext();
            }
            else parser.getNext();
        }
    }
}

#endif
