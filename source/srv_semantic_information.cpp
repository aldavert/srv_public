// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_semantic_information.hpp"
#include <list>

namespace srv
{
    //                   +--------------------------------------+
    //                   | PART INFORMATION CLASS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    PartInformation::PartInformation(void)
    {
        m_identifier[0] = '\0';
        m_red = 0;
        m_green = 0;
        m_blue = 0;
    }
    
    PartInformation::PartInformation(const char *identifier, unsigned char red, unsigned char green, unsigned char blue)
    {
        unsigned int index;
        
        for (index = 0; (index < 63) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        m_identifier[index] = '\0';
        m_red = red;
        m_green = green;
        m_blue = blue;
    }
    
    PartInformation::PartInformation(const PartInformation &other) :
        m_red(other.m_red),
        m_green(other.m_green),
        m_blue(other.m_blue)
    {
        unsigned int index;
        
        for (index = 0; (index < 63) && (other.m_identifier[index] != '\0'); ++index)
            m_identifier[index] = other.m_identifier[index];
        m_identifier[index] = '\0';
    }
    
    PartInformation::~PartInformation(void)
    {
    }
    
    PartInformation& PartInformation::operator=(const PartInformation &other)
    {
        if (this != &other)
        {
            unsigned int index;
            
            for (index = 0; (index < 63) && (other.m_identifier[index] != '\0'); ++index)
                m_identifier[index] = other.m_identifier[index];
            m_identifier[index] = '\0';
            m_red = other.m_red;
            m_green = other.m_green;
            m_blue = other.m_blue;
        }
        
        return *this;
    }
    
    void PartInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Part");
        parser.setAttribute("identifier", m_identifier);
        parser.setAttribute("red", m_red);
        parser.setAttribute("green", m_green);
        parser.setAttribute("blue", m_blue);
        parser.closeTag();
    }
    
    void PartInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Part"))
        {
            unsigned int index;
            const char *identifier;
            
            identifier = parser.getAttribute("identifier");
            for (index = 0; (index < 63) && (identifier[index] != '\0'); ++index)
                m_identifier[index] = identifier[index];
            m_identifier[index] = '\0';
            m_red = parser.getAttribute("red");
            m_green = parser.getAttribute("green");
            m_blue = parser.getAttribute("blue");
            
            while (!(parser.isTagIdentifier("Part") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CATEGORY INFORMATION CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    CategoryInformation::CategoryInformation(void)
    {
        m_parts = 0;
        m_number_of_parts = 0;
        m_identifier[0] = '\0';
        m_red = 0;
        m_green = 0;
        m_blue = 0;
    }
    
    CategoryInformation::CategoryInformation(const char *identifier, unsigned char red, unsigned char green, unsigned char blue, unsigned int number_of_parts, const PartInformation *parts) : m_number_of_parts(number_of_parts), m_red(red), m_green(green), m_blue(blue)
    {
        unsigned int index;
        
        if (number_of_parts > 0)
        {
            m_parts = new PartInformation[number_of_parts];
            for (unsigned int i = 0; i < number_of_parts; ++i)
                m_parts[i] = parts[i];
        }
        else m_parts = 0;
        
        for (index = 0; (index < 63) && (identifier[index] != '\0'); ++index)
            m_identifier[index] = identifier[index];
        m_identifier[index] = '\0';
    }
    
    CategoryInformation::CategoryInformation(const CategoryInformation &other) :
        m_parts((other.m_parts != 0)?new PartInformation[other.m_number_of_parts]:0),
        m_number_of_parts(other.m_number_of_parts),
        m_red(other.m_red),
        m_green(other.m_green),
        m_blue(other.m_blue)
    {
        unsigned int index;
        
        for (unsigned int i = 0; i < other.m_number_of_parts; ++i)
            m_parts[i] = other.m_parts[i];
        for (index = 0; (index < 63) && (other.m_identifier[index] != '\0'); ++index)
            m_identifier[index] = other.m_identifier[index];
        m_identifier[index] = '\0';
    }
    
    CategoryInformation::~CategoryInformation(void)
    {
        if (m_parts != 0) delete [] m_parts;
    }
    
    CategoryInformation& CategoryInformation::operator=(const CategoryInformation &other)
    {
        if (this != &other)
        {
            unsigned int index;
            
            if (m_parts != 0) delete [] m_parts;
            
            m_number_of_parts = other.m_number_of_parts;
            m_red = other.m_red;
            m_green = other.m_green;
            m_blue = other.m_blue;
            
            if (other.m_number_of_parts > 0)
            {
                m_parts = new PartInformation[other.m_number_of_parts];
                for (unsigned int i = 0; i < other.m_number_of_parts; ++i) m_parts[i] = other.m_parts[i];
            }
            else m_parts = 0;
            
            for (index = 0; (index < 63) && (other.m_identifier[index] != '\0'); ++index)
                m_identifier[index] = other.m_identifier[index];
            m_identifier[index] = '\0';
        }
        return *this;
    }
    
    void CategoryInformation::setNumberOfParts(unsigned int number_of_parts)
    {
        if (m_parts != 0) delete [] m_parts;
        
        m_number_of_parts = number_of_parts;
        if (number_of_parts != 0)
            m_parts = new PartInformation[number_of_parts];
        else m_parts = 0;
    }
    
    void CategoryInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Category");
        parser.setAttribute("identifier", m_identifier);
        parser.setAttribute("red", m_red);
        parser.setAttribute("green", m_green);
        parser.setAttribute("blue", m_blue);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_parts; ++i)
            m_parts[i].convertToXML(parser);
        parser.closeTag();
    }
    
    void CategoryInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Category"))
        {
            std::list<PartInformation *> parts;
            unsigned int index;
            const char *identifier;
            
            identifier = parser.getAttribute("identifier");
            for (index = 0; (index < 63) && (identifier[index] != '\0'); ++index)
                m_identifier[index] = identifier[index];
            m_identifier[index] = '\0';
            m_red = parser.getAttribute("red");
            m_green = parser.getAttribute("green");
            m_blue = parser.getAttribute("blue");
            
            while (!(parser.isTagIdentifier("Category") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Part"))
                {
                    PartInformation *p;
                    
                    p = new PartInformation();
                    p->convertFromXML(parser);
                    parts.push_back(p);
                }
                else parser.getNext();
            }
            parser.getNext();
            
            m_number_of_parts = (unsigned int)parts.size();
            if (parts.size() > 0)
            {
                unsigned int list_index;
                
                list_index = 0;
                m_parts = new PartInformation[(unsigned int)parts.size()];
                for (std::list<PartInformation *>::iterator begin = parts.begin(), end = parts.end(); begin != end; ++begin, ++list_index)
                {
                    m_parts[list_index] = *(*begin);
                    delete *begin;
                }
            }
            else m_parts = 0;
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CATEGORIES INFORMATION CLASS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    CategoriesInformation::CategoriesInformation(void) :
        m_categories(0),
        m_number_of_categories(0),
        m_number_of_subcategories(0),
        m_categories_access(0),
        m_part_access(0),
        m_background_red(0),
        m_background_green(0),
        m_background_blue(0),
        m_border_red(0),
        m_border_green(0),
        m_border_blue(0) {}
    
    CategoriesInformation::CategoriesInformation(unsigned char background_red, unsigned char background_green, unsigned char background_blue, unsigned char border_red, unsigned char border_green, unsigned char border_blue, unsigned int number_of_categories, const CategoryInformation *categories) :
        m_number_of_categories(number_of_categories),
        m_background_red(background_red),
        m_background_green(background_green),
        m_background_blue(background_blue),
        m_border_red(border_red),
        m_border_green(border_green),
        m_border_blue(border_blue)
    {
        if (number_of_categories > 0)
        {
            m_number_of_subcategories = 0;
            m_categories = new CategoryInformation[number_of_categories];
            for (unsigned int i = 0; i < number_of_categories; ++i)
            {
                m_categories[i] = categories[i];
                m_number_of_subcategories += categories[i].getNumberOfParts();
            }
            m_categories_access = new CategoryInformation*[m_number_of_subcategories];
            m_part_access = new PartInformation*[m_number_of_subcategories];
            for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
            {
                for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                {
                    m_categories_access[k] = &m_categories[i];
                    m_part_access[k] = &(m_categories[i].getParts(j));
                }
            }
        }
        else
        {
            m_number_of_subcategories = 0;
            m_categories = 0;
            m_categories_access = 0;
            m_part_access = 0;
        }
    }
    
    CategoriesInformation::CategoriesInformation(const CategoriesInformation &other) :
        m_number_of_categories(other.m_number_of_categories),
        m_background_red(other.m_background_red),
        m_background_green(other.m_background_green),
        m_background_blue(other.m_background_blue),
        m_border_red(other.m_border_red),
        m_border_green(other.m_border_green),
        m_border_blue(other.m_border_blue)
    {
        if (other.m_number_of_categories > 0)
        {
            m_number_of_subcategories = 0;
            m_categories = new CategoryInformation[other.m_number_of_categories];
            for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
            {
                m_categories[i] = other.m_categories[i];
                m_number_of_subcategories += other.m_categories[i].getNumberOfParts();
            }
            m_categories_access = new CategoryInformation*[m_number_of_subcategories];
            m_part_access = new PartInformation*[m_number_of_subcategories];
            for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
            {
                for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                {
                    m_categories_access[k] = &m_categories[i];
                    m_part_access[k] = &(m_categories[i].getParts(j));
                }
            }
        }
        else
        {
            m_number_of_subcategories = 0;
            m_categories = 0;
            m_categories_access = 0;
            m_part_access = 0;
        }
    }
    
    CategoriesInformation::~CategoriesInformation(void)
    {
        if (m_categories != 0) delete [] m_categories;
        if (m_categories_access != 0) delete [] m_categories_access;
        if (m_part_access != 0) delete [] m_part_access;
    }
    
    CategoriesInformation& CategoriesInformation::operator=(const CategoriesInformation &other)
    {
        if (this != &other)
        {
            if (m_categories != 0) delete [] m_categories;
            if (m_categories_access != 0) delete [] m_categories_access;
            if (m_part_access != 0) delete [] m_part_access;
            
            m_number_of_categories = other.m_number_of_categories;
            m_background_red = other.m_background_red;
            m_background_green = other.m_background_green;
            m_background_blue = other.m_background_blue;
            m_border_red = other.m_border_red;
            m_border_green = other.m_border_green;
            m_border_blue = other.m_border_blue;
            if (other.m_number_of_categories > 0)
            {
                m_number_of_subcategories = 0;
                m_categories = new CategoryInformation[other.m_number_of_categories];
                for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
                {
                    m_categories[i] = other.m_categories[i];
                    m_number_of_subcategories += other.m_categories[i].getNumberOfParts();
                }
                m_categories_access = new CategoryInformation*[m_number_of_subcategories];
                m_part_access = new PartInformation*[m_number_of_subcategories];
                for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
                {
                    for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                    {
                        m_categories_access[k] = &m_categories[i];
                        m_part_access[k] = &(m_categories[i].getParts(j));
                    }
                }
            }
            else
            {
                m_number_of_subcategories = 0;
                m_categories = 0;
                m_categories_access = 0;
                m_part_access = 0;
            }
        }
        return *this;
    }
    
    int CategoriesInformation::getSubcategoryIndex(const char *category_identifier, const char *subcategory_identifier) const
    {
        for (unsigned int i = 0; i < m_number_of_subcategories; ++i)
        {
            if ((strcasecmp(m_categories_access[i]->getIdentifier(), category_identifier) == 0) && (strcasecmp(m_part_access[i]->getIdentifier(), subcategory_identifier) == 0))
                return (int)i;
        }
        return -1;
    }
    
    int CategoriesInformation::getSubcategoryIndexRGB(unsigned char red, unsigned char green, unsigned char blue) const
    {
        if ((red == m_border_red) && (green == m_border_green) && (blue = m_border_blue)) return -2;
        for (unsigned int i = 0; i < m_number_of_subcategories; ++i)
        {
            if (m_part_access[i]->isSameRGB(red, green, blue))
                return (int)i;
        }
        return -1;
    }
    
    int CategoriesInformation::getSubcategoryIndexBGR(unsigned char blue, unsigned char green, unsigned char red) const
    {
        if ((red == m_border_red) && (green == m_border_green) && (blue = m_border_blue)) return -2;
        for (unsigned int i = 0; i < m_number_of_subcategories; ++i)
        {
            if (m_part_access[i]->isSameBGR(blue, green, red))
                return (int)i;
        }
        return -1;
    }
    
    int CategoriesInformation::getSubcategoryIndexRGB(unsigned char *ptr) const
    {
        if ((ptr[0] == m_border_red) && (ptr[1] == m_border_green) && (ptr[2] = m_border_blue)) return -2;
        for (unsigned int i = 0; i < m_number_of_subcategories; ++i)
        {
            if (m_part_access[i]->isSameRGB(ptr[0], ptr[1], ptr[2]))
                return (int)i;
        }
        return -1;
    }
    
    int CategoriesInformation::getSubcategoryIndexBGR(unsigned char *ptr) const
    {
        if ((ptr[2] == m_border_red) && (ptr[1] == m_border_green) && (ptr[0] = m_border_blue)) return -2;
        for (unsigned int i = 0; i < m_number_of_subcategories; ++i)
        {
            if (m_part_access[i]->isSameBGR(ptr[0], ptr[1], ptr[2]))
                return (int)i;
        }
        return -1;
    }
    
    void CategoriesInformation::setCategoriesInformation(const CategoryInformation * categories, unsigned int number_of_categories)
    {
        if (m_categories != 0) delete [] m_categories;
        if (m_categories_access != 0) delete [] m_categories_access;
        if (m_part_access != 0) delete [] m_part_access;
        
        m_number_of_categories = number_of_categories;
        if (number_of_categories > 0)
        {
            m_number_of_subcategories = 0;
            m_categories = new CategoryInformation[number_of_categories];
            for (unsigned int i = 0; i < number_of_categories; ++i)
            {
                m_categories[i] = categories[i];
                m_number_of_subcategories += categories[i].getNumberOfParts();
            }
            m_categories_access = new CategoryInformation*[m_number_of_subcategories];
            m_part_access = new PartInformation*[m_number_of_subcategories];
            for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
            {
                for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                {
                    m_categories_access[k] = &m_categories[i];
                    m_part_access[k] = &(m_categories[i].getParts(j));
                }
            }
        }
        else
        {
            m_number_of_subcategories = 0;
            m_categories = 0;
            m_categories_access = 0;
            m_part_access = 0;
        }
    }
    
    
    bool CategoriesInformation::merge(const CategoriesInformation &other)
    {
        std::vector<const CategoryInformation*> new_category;
        std::map<unsigned int, std::vector<const PartInformation*> > new_part;
        
        // 1) Search the parts and categories which are new in the 'other' CategoriesInformation object.
        for (unsigned int i = 0; i < other.m_number_of_categories; ++i)
        {
            bool category_found;
            
            category_found = false;
            for (unsigned int j = 0; j < m_number_of_categories; ++j)
            {
                if (strcasecmp(other.m_categories[i].getIdentifier(), m_categories[j].getIdentifier()) == 0)
                {
                    for (unsigned int k = 0; k < other.m_categories[i].getNumberOfParts(); ++k)
                    {
                        bool part_found;
                        
                        part_found = false;
                        for (unsigned int m = 0; m < m_categories[j].getNumberOfParts(); ++m)
                        {
                            if (strcasecmp(other.m_categories[i].getParts(k).getIdentifier(), m_categories[j].getParts(m).getIdentifier()) == 0)
                            {
                                part_found = true;
                                break;
                            }
                        }
                        
                        if (!part_found) new_part[j].push_back(&other.m_categories[i].getParts(k));
                    }
                    category_found = true;
                    break;
                }
            }
            
            if (!category_found) new_category.push_back(&other.m_categories[i]);
        }
        
        // 2) If there is any new category or category-part, update the array of categories information.
        if ((new_category.size() > 0) || (new_part.size() > 0))
        {
            std::map<unsigned int, std::vector<const PartInformation*> >::iterator find_part;
            CategoryInformation *new_categories;
            unsigned int new_number_of_categories;
            
            new_number_of_categories = m_number_of_categories + (unsigned int)new_category.size();
            new_categories = new CategoryInformation[m_number_of_categories + (unsigned int)new_category.size()];
            
            // Add the new parts to the already defined categories.
            for (unsigned int i = 0; i < m_number_of_categories; ++i)
            {
                find_part = new_part.find(i);
                if (find_part != new_part.end())
                {
                    PartInformation *new_parts;
                    unsigned int new_number_of_parts;
                    
                    new_number_of_parts = m_categories[i].getNumberOfParts() + (unsigned int)find_part->second.size();
                    new_parts = new PartInformation[m_categories[i].getNumberOfParts() + (unsigned int)find_part->second.size()];
                    
                    for (unsigned int m = 0; m < m_categories[i].getNumberOfParts(); ++m)
                        new_parts[m] = m_categories[i].getParts(m);
                    for (unsigned int m = m_categories[i].getNumberOfParts(), n = 0; n < find_part->second.size(); ++n, ++m)
                        new_parts[m] = *find_part->second[n];
                    
                    new_categories[i] = CategoryInformation(m_categories[i].getIdentifier(), m_categories[i].getRed(), m_categories[i].getGreen(), m_categories[i].getBlue(), new_number_of_parts, new_parts);
                    delete [] new_parts;
                }
                else new_categories[i] = m_categories[i];
            }
            
            // Copy the remaining object categories.
            for (unsigned int i = m_number_of_categories, j = 0; j < new_category.size(); ++j, ++i)
                new_categories[i] = *new_category[j];
            
            // Set the new vector of categories.
            if (m_categories != 0) delete [] m_categories;
            if (m_categories_access != 0) delete [] m_categories_access;
            if (m_part_access != 0) delete [] m_part_access;
            
            
            // Update categories and parts reference arrays.
            m_categories = new_categories;
            m_number_of_categories = new_number_of_categories;
            
            m_number_of_subcategories = 0;
            for (unsigned int i = 0; i < m_number_of_categories; ++i)
                m_number_of_subcategories += m_categories[i].getNumberOfParts();
            m_categories_access = new CategoryInformation*[m_number_of_subcategories];
            m_part_access = new PartInformation*[m_number_of_subcategories];
            for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
            {
                for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                {
                    m_categories_access[k] = &m_categories[i];
                    m_part_access[k] = &(m_categories[i].getParts(j));
                }
            }
            
            return true;
        }
        else return false;
    }
    
    void CategoriesInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Categories_Information");
        parser.addChildren();
        // -[ BACKGROUND INFORMATION ]-------------------------------------
        parser.openTag("Background");
        parser.setAttribute("red", m_background_red);
        parser.setAttribute("green", m_background_green);
        parser.setAttribute("blue", m_background_blue);
        parser.closeTag();
        parser.openTag("Border");
        parser.setAttribute("red", m_border_red);
        parser.setAttribute("green", m_border_green);
        parser.setAttribute("blue", m_border_blue);
        parser.closeTag();
        // -[ CATEGORIES INFORMATION ]-------------------------------------
        for (unsigned int i = 0; i < m_number_of_categories; ++i)
            m_categories[i].convertToXML(parser);
        // ----------------------------------------------------------------
        parser.closeTag();
    }
    
    void CategoriesInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Categories_Information"))
        {
            std::list<CategoryInformation *> categories;
            
            if (m_categories != 0) { delete [] m_categories; m_categories = 0; }
            if (m_categories_access != 0) { delete [] m_categories_access; m_categories_access = 0; }
            if (m_part_access != 0) { delete [] m_part_access; m_part_access = 0; }
            
            while (!(parser.isTagIdentifier("Categories_Information") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Background"))
                {
                    m_background_red = parser.getAttribute("red");
                    m_background_green = parser.getAttribute("green");
                    m_background_blue = parser.getAttribute("blue");
                    
                    while (!(parser.isTagIdentifier("Background") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Border"))
                {
                    m_border_red = parser.getAttribute("red");
                    m_border_green = parser.getAttribute("green");
                    m_border_blue = parser.getAttribute("blue");
                    
                    while (!(parser.isTagIdentifier("Border") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Category"))
                {
                    CategoryInformation * c;
                    c = new CategoryInformation();
                    c->convertFromXML(parser);
                    categories.push_back(c);
                }
                else parser.getNext();
            }
            parser.getNext();
            
            m_number_of_categories = (unsigned int)categories.size();
            if (categories.size() > 0)
            {
                unsigned int list_index;
                
                m_number_of_subcategories = 0;
                m_categories = new CategoryInformation[categories.size()];
                list_index = 0;
                for (std::list<CategoryInformation *>::iterator begin = categories.begin(), end = categories.end(); begin != end; ++begin, ++list_index)
                {
                    m_categories[list_index] = *(*begin);
                    m_number_of_subcategories += (*begin)->getNumberOfParts();
                    delete *begin;
                }
                m_categories_access = new CategoryInformation*[m_number_of_subcategories];
                m_part_access = new PartInformation*[m_number_of_subcategories];
                for (unsigned int i = 0, k = 0; i < m_number_of_categories; ++i)
                {
                    for (unsigned int j = 0; j < m_categories[i].getNumberOfParts(); ++j, ++k)
                    {
                        m_categories_access[k] = &m_categories[i];
                        m_part_access[k] = &(m_categories[i].getParts(j));
                    }
                }
            }
            else
            {
                m_number_of_subcategories = 0;
                m_categories = 0;
                m_categories_access = 0;
                m_part_access = 0;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BOUNDING BOX CLASS                   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    BoundingBox::BoundingBox(void) :
        m_xmin(-1),
        m_ymin(-1),
        m_xmax(-1),
        m_ymax(-1),
        m_label(-1),
        m_difficult(false),
        m_truncated(false)
    {
        m_pose[0] = '\0';
        m_category[0] = '\0';
        m_part[0] = '\0';
    }
    
    BoundingBox::BoundingBox(short xmin, short ymin, short xmax, short ymax, const char *pose, const char *category, const char *part, bool difficult, bool truncated, const CategoriesInformation *categories_information) :
        m_xmin(xmin),
        m_ymin(ymin),
        m_xmax(xmax),
        m_ymax(ymax),
        m_label(-1),
        m_difficult(difficult),
        m_truncated(truncated)
    {
        int index;
        
        for (index = 0; (index < 63) && (pose[index] != '\0'); ++index)
            m_pose[index] = pose[index];
        m_pose[index] = '\0';
        for (index = 0; (index < 63) && (category[index] != '\0'); ++index)
            m_category[index] = category[index];
        m_category[index] = '\0';
        for (index = 0; (index < 63) && (part[index] != '\0'); ++index)
            m_part[index] = part[index];
        m_part[index] = '\0';
        updateLabel(categories_information);
    }
    
    BoundingBox::BoundingBox(const BoundingBox &other) :
        m_xmin(other.m_xmin),
        m_ymin(other.m_ymin),
        m_xmax(other.m_xmax),
        m_ymax(other.m_ymax),
        m_label(other.m_label),
        m_difficult(other.m_difficult),
        m_truncated(other.m_truncated)
    {
        int index;
        
        for (index = 0; (index < 63) && (other.m_pose[index] != '\0'); ++index)
            m_pose[index] = other.m_pose[index];
        m_pose[index] = '\0';
        for (index = 0; (index < 63) && (other.m_category[index] != '\0'); ++index)
            m_category[index] = other.m_category[index];
        m_category[index] = '\0';
        for (index = 0; (index < 63) && (other.m_part[index] != '\0'); ++index)
            m_part[index] = other.m_part[index];
        m_part[index] = '\0';
    }
    
    BoundingBox& BoundingBox::operator=(const BoundingBox &other)
    {
        if (this != &other)
        {
            int index;
            
            m_xmin = other.m_xmin;
            m_ymin = other.m_ymin;
            m_xmax = other.m_xmax;
            m_ymax = other.m_ymax;
            m_label = other.m_label;
            m_difficult = other.m_difficult;
            m_truncated = other.m_truncated;
            
            for (index = 0; (index < 63) && (other.m_pose[index] != '\0'); ++index)
                m_pose[index] = other.m_pose[index];
            m_pose[index] = '\0';
            for (index = 0; (index < 63) && (other.m_category[index] != '\0'); ++index)
                m_category[index] = other.m_category[index];
            m_category[index] = '\0';
            for (index = 0; (index < 63) && (other.m_part[index] != '\0'); ++index)
                m_part[index] = other.m_part[index];
            m_part[index] = '\0';
        }
        
        return *this;
    }
    
    void BoundingBox::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Bounding_Box");
        parser.setAttribute("category", m_category);
        parser.setAttribute("part", m_part);
        parser.setAttribute("x_min", m_xmin);
        parser.setAttribute("y_min", m_ymin);
        parser.setAttribute("x_max", m_xmax);
        parser.setAttribute("y_max", m_ymax);
        parser.setAttribute("pose", m_pose);
        parser.setAttribute("difficult", m_difficult);
        parser.setAttribute("truncated", m_truncated);
        parser.closeTag();
    }
    
    void BoundingBox::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Bounding_Box"))
        {
            const char *pose, *category_name, *part_name;
            unsigned int index;
            
            m_xmin = parser.getAttribute("x_min");
            m_ymin = parser.getAttribute("y_min");
            m_xmax = parser.getAttribute("x_max");
            m_ymax = parser.getAttribute("y_max");
            pose = parser.getAttribute("pose");
            category_name = parser.getAttribute("category");
            part_name = parser.getAttribute("part");
            for (index = 0; (index < 63) && (pose[index] != '\0'); ++index)
                m_pose[index] = pose[index];
            m_pose[index] = '\0';
            for (index = 0; (index < 63) && (category_name[index] != '\0'); ++index)
                m_category[index] = category_name[index];
            m_category[index] = '\0';
            for (index = 0; (index < 63) && (part_name[index] != '\0'); ++index)
                m_part[index] = part_name[index];
            m_part[index] = '\0';
            m_label = -1;
            m_difficult = parser.getAttribute("difficult");
            m_truncated = parser.getAttribute("truncated");
            
            while(!(parser.isTagIdentifier("Bounding_Box") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GLOBAL CATEGORY CLASS                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    GlobalCategory::GlobalCategory(void) :
        m_label(-2),
        m_difficult(false)
    {
        m_category[0] = m_part[0] = '\0';
    }
    
    GlobalCategory::GlobalCategory(const char * category, const char * part, bool difficult, const CategoriesInformation * categories_information) :
        m_difficult(difficult)
    {
        unsigned int index;
        
        for (index = 0; (index < 63) && (category[index] != '\0'); ++index)
            m_category[index] = category[index];
        m_category[index] = '\0';
        for (index = 0; (index < 63) && (part[index] != '\0'); ++index)
            m_part[index] = part[index];
        m_part[index] = '\0';
        
        if (categories_information != 0) updateLabel(categories_information);
        else m_label = -2;
    }
    
    GlobalCategory::GlobalCategory(const GlobalCategory &other) :
        m_label(other.m_label),
        m_difficult(other.m_difficult)
    {
        unsigned int index;
        
        for (index = 0; (index < 63) && (other.m_category[index] != '\0'); ++index)
            m_category[index] = other.m_category[index];
        m_category[index] = '\0';
        for (index = 0; (index < 63) && (other.m_part[index] != '\0'); ++index)
            m_part[index] = other.m_part[index];
        m_part[index] = '\0';
    }
    
    GlobalCategory::~GlobalCategory(void)
    {
    }
    
    GlobalCategory& GlobalCategory::operator=(const GlobalCategory &other)
    {
        if (this != &other)
        {
            unsigned int index;
            
            for (index = 0; (index < 63) && (other.m_category[index] != '\0'); ++index)
                m_category[index] = other.m_category[index];
            m_category[index] = '\0';
            for (index = 0; (index < 63) && (other.m_part[index] != '\0'); ++index)
                m_part[index] = other.m_part[index];
            m_part[index] = '\0';
            m_label = other.m_label;
            m_difficult = other.m_difficult;
        }
        
        return *this;
    }
    
    void GlobalCategory::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Global_Category");
        parser.setAttribute("category", m_category);
        parser.setAttribute("part", m_part);
        parser.setAttribute("difficult", m_difficult);
        parser.closeTag();
    }
    
    void GlobalCategory::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Global_Category"))
        {
            const char *string_array;
            unsigned int index;
            
            m_difficult = parser.getAttribute("difficult");
            string_array = parser.getAttribute("category");
            for (index = 0; (index < 63) && (string_array[index] != '\0'); ++index)
                m_category[index] = string_array[index];
            m_category[index] = '\0';
            
            string_array = parser.getAttribute("part");
            for (index = 0; (index < 63) && (string_array[index] != '\0'); ++index)
                m_part[index] = string_array[index];
            m_part[index] = '\0';
            
            while (!(parser.isTagIdentifier("Global_Category") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

