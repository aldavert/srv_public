// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_TEMPLATES_HEADER_FILE__
#define __SRV_IMAGE_TEMPLATES_HEADER_FILE__

#include "image/srv_image.hpp"
#include "image/srv_image_operators.hpp"
#include "image/srv_image_color.hpp"
#include "image/srv_image_transform.hpp"
#include "image/srv_image_processing.hpp"
#include "image/srv_image_segmentation.hpp"
#include "image/srv_image_draw.hpp"
#include "image/srv_image_integral.hpp"
#ifndef __NO_GTK__
#include "image/srv_gtk_figure.hpp"
#include "image/srv_camera.hpp"
#endif
#ifdef __ENABLE_FFMPEG__
#include "image/srv_video.hpp"
#endif

namespace srv
{
}

#endif

