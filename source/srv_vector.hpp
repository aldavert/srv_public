// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_VECTOR_TEMPLATES_HEADER_FILE__
#define __SRV_VECTOR_TEMPLATES_HEADER_FILE__

#include "vector/srv_vector.hpp"
#include "vector/srv_vector_operators.hpp"
#include "vector/srv_vector_distances.hpp"
#include "srv_file_compression.hpp"
#include <iostream>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | VECTOR DISTANCE PARAMETERS CLASS     |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class ParametersVectorDistance
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ParametersVectorDistance(void);
        /** Parameters constructor which initializes all vector distance parameters.
         *  \param[in] method index of the selected vector distance.
         *  \param[in] minkowski_order order of the Minkowski distance.
         */
        ParametersVectorDistance(unsigned int method, double minkowski_order);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the index of the selected vector distance.
        inline unsigned int getMethod(void) const { return m_method; }
        /// Sets the index of the selected vector distance.
        inline void setMethod(unsigned int method)
        {
            if (method > 11)
                throw Exception("Unknown vector distance index '%d'.", method);
            m_method = method;
        }
        /// Returns the identifier of the selected distance.
        DISTANCE_IDENTIFIER getMethodIdentifier(void) const;
        /// Sets the selected vector distance.
        void setMethodIdentifier(DISTANCE_IDENTIFIER identifier);
        /// Returns the order of the Minkowski distance.
        inline double getMinkowskiOrder(void) const { return m_minkowski_order; }
        /// Sets the order of the Minkowski distance.
        inline void setMinkowskiOrder(double minkowski_order) { m_minkowski_order = minkowski_order; }
        
        /// This function updates the values of the variables which have been set by a parser parameters in another dimensionality reduction object parameters.
        void update(const ParametersVectorDistance &other);
        
        // -[ Boolean flags of the set by the parser ]-----------------------------------------------------------------------------------------------
        /// Returns the boolean flag which is true when the <b>method</b> variable has been set by a parameter parser.
        inline bool isSetMethod(void) const { return m_method_update_parameter; }
        /// Returns the boolean flag which is true when the <b>minkowski_order</b> variable has been set by a parameter parser.
        inline bool isSetMinkowskiOrder(void) const { return m_minkowski_order_update_parameter; }
        /// Returns true of any of the parameters of the vector distance has been set by the parameter parser.
        inline bool isAnySet(void) const { return m_method_update_parameter || m_minkowski_order_update_parameter; }
        /// Resets the parameter set flags to false.
        inline void resetFlags(void)
        {
            m_method_update_parameter = false;
            m_minkowski_order_update_parameter = false;
        }
        
        // -[ Parameter parser functions ]-----------------------------------------------------------------------------------------------------------
        /** Sets the parameters parser to the optional variables of the vector distance object.
         *  \param[in] parameters parameters parser object assigned to the object variables.
         *  \param[in] suffix optional char array suffix added to the parser parameters (--(<suffix>-)<parameter>).
         */
        void setParameters(ParameterParser &parameters, const char * suffix = "");
        
        // -[ XML Functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the parameters information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the parameters information from an XML object.
        void convertFromXML(XmlParser &parser);
        
        /// Friend function which shows the vector distance parameters information in an output stream.
        friend std::ostream& operator<<(std::ostream &out, const ParametersVectorDistance &parameters);
        
    protected:
        /// Distance type index.
        unsigned int m_method;
        /// Boolean flag which is true when the <b>m_method</b> variable has been set by the parameter parser.
        bool m_method_update_parameter;
        /// Order of the Minkowski distance.
        double m_minkowski_order;
        /// Boolean flag which is true when the <b>m_minkowski_order</b> variable has been set by the parameter parser.
        bool m_minkowski_order_update_parameter;
    };
    
    /// Shows the vector distance parameters information in an output stream.
    std::ostream& operator<<(std::ostream &out, const ParametersVectorDistance &parameters);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY VECTOR DISTANCE FUNCTIONS  |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Creates a vector distance object with the specified parameters.
     *  \param[in] parameters parameters of the created vector distance object.
     *  \param[out] object resulting vector distance object.
     */
    void create(const ParametersVectorDistance &parameters, VectorDistance &object);
    
    /** Shows the information of a vector distance object.
     *  \param[out] out standard library output stream.
     *  \param[in] object vector distance object.
     *  \returns the input standard library output stream.
     */
    std::ostream& operator<<(std::ostream &out, const VectorDistance &object);
    
    /** Loads the vector distance object information from an XML file.
     *  \param[in] filename file where the vector distance object is stored.
     *  \param[out] object vector distance object.
     *  \note this function does not deallocates the object pointed by the object pointer.
     */
    void loadObject(const char * filename, VectorDistance &object);
    
    /** Saves the vector distance object into an XML file.
     *  \param[in] filename file where the vector distance object is going to be stored.
     *  \param[in] object vector distance object.
     */
    void saveObject(const char * filename, const VectorDistance &object);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VECTOR NORM PARAMETERS CLASS         |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class ParametersVectorNorm
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ParametersVectorNorm(void);
        /** Parameters constructor which initializes all vector norm parameters.
         *  \param[in] method index of the selected vector norm.
         *  \param[in] order order of the L_p norm.
         */
        ParametersVectorNorm(unsigned int method, double order);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the index of the selected vector norm.
        inline unsigned int getMethod(void) const { return m_method; }
        /// Sets the index of the selected vector norm.
        inline void setMethod(unsigned int method)
        {
            if (method > 3)
                throw Exception("Unknown vector norm index '%d'.", method);
            m_method = method;
        }
        /// Returns the identifier of the selected norm.
        NORMAL_IDENTIFIER getMethodIdentifier(void) const;
        /// Sets the selected vector norm.
        void setMethodIdentifier(NORMAL_IDENTIFIER identifier);
        /// Returns the order of the L_p norm.
        inline double getOrder(void) const { return m_order; }
        /// Sets the order of the L_p norm.
        inline void setOrder(double order) { m_order = order; }
        
        /// This function updates the values of the variables which have been set by a parser parameters in another dimensionality reduction object parameters.
        void update(const ParametersVectorNorm &other);
        
        // -[ Boolean flags of the set by the parser ]-----------------------------------------------------------------------------------------------
        /// Returns the boolean flag which is true when the <b>method</b> variable has been set by a parameter parser.
        inline bool isSetMethod(void) const { return m_method_update_parameter; }
        /// Returns the boolean flag which is true when the <b>order</b> variable has been set by a parameter parser.
        inline bool isSetOrder(void) const { return m_order_update_parameter; }
        /// Returns true of any of the parameters of the vector normal has been set by the parameter parser.
        inline bool isAnySet(void) const { return m_method_update_parameter || m_order_update_parameter; }
        /// Resets the parameter set flags to false.
        inline void resetFlags(void)
        {
            m_method_update_parameter = false;
            m_order_update_parameter = false;
        }
        
        // -[ Parameter parser functions ]-----------------------------------------------------------------------------------------------------------
        /** Sets the parameters parser to the optional variables of the vector norm object.
         *  \param[in] parameters parameters parser object assigned to the object variables.
         *  \param[in] suffix optional char array suffix added to the parser parameters (--(<suffix>-)<parameter>).
         */
        void setParameters(ParameterParser &parameters, const char * suffix = "");
        
        // -[ XML Functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the parameters information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the parameters information from an XML object.
        void convertFromXML(XmlParser &parser);
        
        /// Shows the vector normal parameters information in an output stream.
        friend std::ostream& operator<<(std::ostream &out, const ParametersVectorNorm &parameters);
        
    protected:
        /// Norm type index.
        unsigned int m_method;
        /// Boolean flag which is true when the <b>m_method</b> variable has been set by the parameter parser.
        bool m_method_update_parameter;
        /// Order of the L_p norm.
        double m_order;
        /// Boolean flag which is true when the <b>m_order</b> variable has been set by the parameter parser.
        bool m_order_update_parameter;
    };
    
    /// Shows the vector normal parameters information in an output stream.
    std::ostream& operator<<(std::ostream &out, const ParametersVectorNorm &parameters);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY VECTOR NORM FUNCTIONS      |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Creates a vector norm object with the specified parameters.
     *  \param[in] parameters parameters of the vector norm object.
     *  \param[out] object resulting vector norm object.
     */
    void create(const ParametersVectorNorm &parameters, VectorNorm &object);
    
    /** Shows the information of a vector norm object.
     *  \param[out] out standard library output stream.
     *  \param[in] object vector norm object.
     *  \returns the input standard library output stream.
     */
    std::ostream& operator<<(std::ostream &out, const VectorNorm &object);
    
    /** Loads the vector norm object information from an XML file.
     *  \param[in] filename file where the vector norm object is stored.
     *  \param[out] object vector norm object.
     *  \note this function does not deallocates the object pointed by the object pointer.
     */
    void loadObject(const char * filename, VectorNorm &object);
    
    /** Saves the vector norm object into an XML file.
     *  \param[in] filename file where the vector norm object is going to be stored.
     *  \param[in] object vector norm object.
     */
    void saveObject(const char * filename, const VectorNorm &object);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

