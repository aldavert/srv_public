// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_logger.hpp"
#include <cstdarg>
#include <ctime>

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | BASE LOGGER CLASS                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void BaseLogger::log(const char *format, ...)
    {
        va_list args;
        va_start(args, format);
        vsprintf(m_message, format, args);
        va_end(args);
        process_message();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | OUTPUT STREAM LOGGER CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void StreamLogger::process_message(void)
    {
        char current_message[4096 + 128];
        bool flush;
        
        flush = false;
        for (unsigned int i = 0; this->m_message[i] != '\0'; ++i)
        {
            if (this->m_message[i] == '\r')
            {
                flush = true;
                break;
            }
        }
        if (!m_disable_timer)
            sprintf(current_message, "[ %09.2f ] %s", m_chronometer.getElapsedTime() / 1000.0, this->m_message);
        else sprintf(current_message, "%s", this->m_message);
        if (flush)
        {
            *m_output_stream << current_message;
            m_output_stream->flush();
        }
        else *m_output_stream << current_message << std::endl;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

