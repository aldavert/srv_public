// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_DATABASE_INFORMATION_HPP_HEADER_FILE__
#define __SRV_DATABASE_INFORMATION_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
// -[ SRV header files]------------------------------------------------
#include "srv_semantic_information.hpp"
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"

namespace srv
{
    const unsigned int FILENAME_SIZE = 128;
    const unsigned int ROUTE_SIZE = 256;
    
    //                   +--------------------------------------+
    //                   | PIXEL SEMANTIC INFORMATION CLASS     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Image semantic information at pixel-level.
    class PixelSemanticInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]--------------------------------------
        /// Default constructor.
        PixelSemanticInformation(void) {}
        /** Constructor which initializes the pixel annotation for a given categories information object.
         *  \param[in] semantic_labels image or sub-image with the image annotation with original color codes (i.e.\ pixels are labeled with a (R, G, B) color code).
         *  \param[in] categories_information categories information which decodes the (R, G, B) color code to its actual label.
         */
        template <template <class> class IMAGE, class T>
        PixelSemanticInformation(const IMAGE<T> &semantic_labels, const CategoriesInformation &categories_information);
        /** Function which initializes the pixel annotation for a given categories information object.
         *  \param[in] semantic_labels image or sub-image with the image annotation with original color codes (i.e.\ pixels are labeled with a (R, G, B) color code).
         *  \param[in] categories_information categories information which decodes the (R, G, B) color code to its actual label.
         */
        template <template <class> class IMAGE, class T>
        void set(const IMAGE<T> &semantic_labels, const CategoriesInformation &categories_information);
        
        // -[ Access to the pixel semantic information ]-----------------------------------------------
        /// Returns a constant reference to the pixels annotation image.
        inline const Image<int>& getAnnotationImage(void) const { return m_pixels_annotation; }
        /// Returns a copy of the pixels annotation image.
        template <template <class> class IMAGE, class TIMAGE>
        inline void getAnnotationImage(IMAGE<TIMAGE> &other) const { other = m_pixels_annotation; }
        /// Returns the width of the labels image.
        inline unsigned int getWidth(void) const { return m_pixels_annotation.getWidth(); }
        /// Returns the height of the labels image.
        inline unsigned int getHeight(void) const { return m_pixels_annotation.getHeight(); }
        /// Returns a constant pointer to the pixel semantic labels image.
        inline const int* getLabels(void) const { return m_pixels_annotation.get(0); }
        /// Returns a constant pointer to the index-th row of the labels image.
        inline const int* operator()(unsigned int y) const { return m_pixels_annotation.get(y, 0); }
        /// Returns the semantic label of the pixel in the (x, y) coordinate.
        inline int operator()(unsigned int x, unsigned int y) const { return *m_pixels_annotation.get(x, y, 0); }
        /// Returns a constant pointer to the index-th row of the labels image.
        inline const int* get(unsigned int y) const { return m_pixels_annotation.get(y, 0); }
        /// Returns the semantic label of the pixel in the (x, y) coordinate.
        inline int get(unsigned int x, unsigned int y) const { return *m_pixels_annotation.get(x, y, 0);}
        /** Creates an image where each pixel annotation is replaced with the color code specified by the given categories information object.
         *  \param[in] categories_information categories information object used to encode the each pixel annotation to its corresponding color codes.
         *  \returns a color image with the color codes annotation.
         */
        inline Image<unsigned char> createSemanticColorsImage(const CategoriesInformation &categories_information) const
        {
            Image<unsigned char> result;
            createSemanticColorsImage(categories_information, result);
            return result;
        }
        /** Creates an image where each pixel annotation is replaced with the color code specified by the given categories information object.
         *  \param[in] categories_information categories information object used to encode the each pixel annotation to its corresponding color codes.
         *  \param[out] color_codes image with the resulting color codes annotation.
         */
        template <class T>
        void createSemanticColorsImage(const CategoriesInformation &categories_information, Image<T> &color_codes) const;
        /** Creates an image where each pixel annotation is replaced with the color code specified by the given categories information object.
         *  \param[in] categories_information categories information object used to encode the each pixel annotation to its corresponding color codes.
         *  \param[out] color_codes sub-image with the resulting color codes annotation. The sub-image must have the same geometry as the annotation image, otherwise an exception is generated.
         */
        template <class T>
        void createSemanticColorsImage(const CategoriesInformation &categories_information, SubImage<T> &color_codes) const;
        
        // -[ Transform the semantic information ]-----------------------------------------------------
        /** This function creates a copy of the pixel semantic annotation deformed by a 3x3 transform.
         *  \param[in] projective double array with the weights of the 3x3 transform matrix stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]])
         *  \param[out] transformed_annotation resulting transformed pixel semantic object.
         */
        void projectiveTransform(const double *projective, PixelSemanticInformation &transformed_annotation) const;
        /** This function creates a copy of the pixel semantic annotation deformed by a 3x3 transform.
         *  \param[in] projective double array with the weights of the 3x3 transform matrix stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]])
         *  \returns the transformed pixel semantic object.
         */
        inline PixelSemanticInformation projectiveTransform(const double *projective) const { PixelSemanticInformation semantic_information; projectiveTransform(projective, semantic_information); return semantic_information; }
    private:
        /// Image with the category labels for each pixel of the original image.
        Image<int> m_pixels_annotation;
    };
    
    template <template <class> class IMAGE, class T>
    PixelSemanticInformation::PixelSemanticInformation(const IMAGE<T> &semantic_labels, const CategoriesInformation &categories_information) :
        m_pixels_annotation(semantic_labels.getWidth(), semantic_labels.getHeight(), 1)
    {
        for (unsigned int y = 0; y < semantic_labels.getHeight(); ++y)
        {
            const T * __restrict__ red_semantic_labels_ptr = semantic_labels.get(y, 0);
            const T * __restrict__ green_semantic_labels_ptr = semantic_labels.get(y, 1);
            const T * __restrict__ blue_semantic_labels_ptr = semantic_labels.get(y, 2);
            int * __restrict__ pixels_annotation_ptr = m_pixels_annotation.get(y, 0);
            
            for (unsigned int x = 0; x < semantic_labels.getWidth(); ++x)
            {
                *pixels_annotation_ptr = categories_information.getSubcategoryIndexRGB((unsigned char)*red_semantic_labels_ptr, (unsigned char)*green_semantic_labels_ptr, (unsigned char)*blue_semantic_labels_ptr);
                
                ++pixels_annotation_ptr;
                ++red_semantic_labels_ptr;
                ++green_semantic_labels_ptr;
                ++blue_semantic_labels_ptr;
            }
        }
    }
    
    template <template <class> class IMAGE, class T>
    void PixelSemanticInformation::set(const IMAGE<T> &semantic_labels, const CategoriesInformation &categories_information)
    {
        m_pixels_annotation.setGeometry(semantic_labels.getWidth(), semantic_labels.getHeight(), 1);
        for (unsigned int y = 0; y < semantic_labels.getHeight(); ++y)
        {
            const T * __restrict__ red_semantic_labels_ptr = semantic_labels.get(y, 0);
            const T * __restrict__ green_semantic_labels_ptr = semantic_labels.get(y, 1);
            const T * __restrict__ blue_semantic_labels_ptr = semantic_labels.get(y, 2);
            int * __restrict__ pixels_annotation_ptr = m_pixels_annotation.get(y, 0);
            
            for (unsigned int x = 0; x < semantic_labels.getWidth(); ++x)
            {
                *pixels_annotation_ptr = categories_information.getSubcategoryIndexRGB((unsigned char)*red_semantic_labels_ptr, (unsigned char)*green_semantic_labels_ptr, (unsigned char)*blue_semantic_labels_ptr);
                
                ++pixels_annotation_ptr;
                ++red_semantic_labels_ptr;
                ++green_semantic_labels_ptr;
                ++blue_semantic_labels_ptr;
            }
        }
    }
    
    template <class T>
    void PixelSemanticInformation::createSemanticColorsImage(const CategoriesInformation &categories_information, Image<T> &color_codes) const
    {
        color_codes.setGeometry(m_pixels_annotation.getWidth(), m_pixels_annotation.getHeight(), 3);
        
        for (unsigned int y = 0; y < m_pixels_annotation.getHeight(); ++y)
        {
            const int * __restrict__ pixels_annotation_ptr = m_pixels_annotation.get(y, 0);
            T * __restrict__ red_code_ptr = color_codes.get(y, 0);
            T * __restrict__ green_code_ptr = color_codes.get(y, 1);
            T * __restrict__ blue_code_ptr = color_codes.get(y, 2);
            
            for (unsigned int x = 0; x < m_pixels_annotation.getWidth(); ++x)
            {
                if (*pixels_annotation_ptr == -2)
                {
                    *red_code_ptr = categories_information.getBorderRed();
                    *green_code_ptr = categories_information.getBorderGreen();
                    *blue_code_ptr = categories_information.getBorderBlue();
                }
                else if (*pixels_annotation_ptr < 0)
                {
                    *red_code_ptr = categories_information.getBackgroundRed();
                    *green_code_ptr = categories_information.getBackgroundGreen();
                    *blue_code_ptr = categories_information.getBackgroundBlue();
                }
                else
                {
                    *red_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getRed();
                    *green_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getGreen();
                    *blue_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getBlue();
                }
                
                ++pixels_annotation_ptr;
                ++red_code_ptr;
                ++green_code_ptr;
                ++blue_code_ptr;
            }
        }
    }
    
    template <class T>
    void PixelSemanticInformation::createSemanticColorsImage(const CategoriesInformation &categories_information, SubImage<T> &color_codes) const
    {
        if ((color_codes.getWidth() != m_pixels_annotation.getWidth()) || (color_codes.getHeight() != m_pixels_annotation.getHeight()))
            throw Exception("Incorrect sub-image geometry. Expected a %dx%d sub-image instead of the given %dx%d sub-image.", m_pixels_annotation.getWidth(), m_pixels_annotation.getHeight(), color_codes.getWidth(), color_codes.getHeight());
        if (color_codes.getNumberOfChannels() != 3)
            throw Exception("Color codes sub-image is expected to have 3 channels instead of %d.", color_codes.getNumberOfChannels());
        
        for (unsigned int y = 0; y < m_pixels_annotation.getHeight(); ++y)
        {
            const int * __restrict__ pixels_annotation_ptr = m_pixels_annotation.get(y, 0);
            T * __restrict__ red_code_ptr = color_codes.get(y, 0);
            T * __restrict__ green_code_ptr = color_codes.get(y, 1);
            T * __restrict__ blue_code_ptr = color_codes.get(y, 2);
            
            for (unsigned int x = 0; x < m_pixels_annotation.getWidth(); ++x)
            {
                if (*pixels_annotation_ptr == -2)
                {
                    *red_code_ptr = categories_information.getBorderRed();
                    *green_code_ptr = categories_information.getBorderGreen();
                    *blue_code_ptr = categories_information.getBorderBlue();
                }
                else if (*pixels_annotation_ptr < 0)
                {
                    *red_code_ptr = categories_information.getBackgroundRed();
                    *green_code_ptr = categories_information.getBackgroundGreen();
                    *blue_code_ptr = categories_information.getBackgroundBlue();
                }
                else
                {
                    *red_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getRed();
                    *green_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getGreen();
                    *blue_code_ptr = categories_information.getSubcategory(*pixels_annotation_ptr)->getBlue();
                }
                
                ++pixels_annotation_ptr;
                ++red_code_ptr;
                ++green_code_ptr;
                ++blue_code_ptr;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DATABASE IMAGE INFORMATION CLASS     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which stores the semantic information (i.e.\ global category, object bounding boxes and segmentation information) associated to a image.
    class DatabaseImageInformation
    {
    public:
        // Constructors, destructor and assignation operator ..........................................
        /// Default constructor.
        DatabaseImageInformation(void);
        /** Constructor where the filenames of the image and its metadata and the geometry of the image is given as parameters.
         *  \param[in] image_filename filename of the original image.
         *  \param[in] mask_filename filename of the image with the semantic annotations for each pixel.
         *  \param[in] metadata_filename file containing supplementary semantic annotation for the image (for instance, textual information like the caption of the image or the text around it).
         *  \param[in] width width of the original image.
         *  \param[in] height height of the original image.
         */
        DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, unsigned int width, unsigned int height);
        /** Constructor where the image information the whole semantic information are provided as parameters.
         *  \param[in] image_filename filename of the original image.
         *  \param[in] mask_filename filename of the image with the semantic annotations for each pixel.
         *  \param[in] metadata_filename file containing supplementary semantic annotation for the image (for instance, textual information like the caption of the image or the text around it).
         *  \param[in] number_of_bounding_boxes number of objects annotated with bounding-boxes in the image.
         *  \param[in] bounding_boxes array with the object bounding boxes information.
         *  \param[in] number_of_global_categories number of global image categories.
         *  \param[in] global_categories array with the information of each global image category.
         *  \param[in] width width of the original image.
         *  \param[in] height height of the original image.
         */
        DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, unsigned int number_of_bounding_boxes, const BoundingBox *bounding_boxes, unsigned int number_of_global_categories, const GlobalCategory *global_categories, unsigned int width, unsigned int height);
        /** Constructor where the full route to the image and its metadata and the geometry of the image is given as parameters.
         *  \param[in] image_filename filename of the original image.
         *  \param[in] mask_filename filename of the image with the semantic annotations for each pixel.
         *  \param[in] metadata_filename file containing supplementary semantic annotation for the image (for instance, textual information like the caption of the image or the text around it).
         *  \param[in] route relative route to the database folder where the image and metadata information is stored.
         *  \param[in] width width of the original image.
         *  \param[in] height height of the original image.
         */
        DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, const char *route, unsigned int width, unsigned int height);
        /** Constructor where the image information the whole semantic information are provided as parameters.
         *  \param[in] image_filename filename of the original image.
         *  \param[in] mask_filename filename of the image with the semantic annotations for each pixel.
         *  \param[in] metadata_filename file containing supplementary semantic annotation for the image (for instance, textual information like the caption of the image or the text around it).
         *  \param[in] route relative route to the database folder where the image and metadata information is stored.
         *  \param[in] number_of_bounding_boxes number of objects annotated with bounding-boxes in the image.
         *  \param[in] bounding_boxes array with the object bounding boxes information.
         *  \param[in] number_of_global_categories number of global image categories.
         *  \param[in] global_categories array with the information of each global image category.
         *  \param[in] width width of the original image.
         *  \param[in] height height of the original image.
         */
        DatabaseImageInformation(const char *image_filename, const char *mask_filename, const char *metadata_filename, const char *route, unsigned int number_of_bounding_boxes, const BoundingBox *bounding_boxes, unsigned int number_of_global_categories, const GlobalCategory *global_categories, unsigned int width, unsigned int height);
        /// Copy constructor.
        DatabaseImageInformation(const DatabaseImageInformation &copy);
        /// Destructor.
        ~DatabaseImageInformation(void);
        /// Assignation operator.
        DatabaseImageInformation& operator=(const DatabaseImageInformation &copy);
        
        // Get/set images information .................................................................
        /// Returns a constant pointer to the string array containing the filename of the image originally stored in the database (without the full route).
        inline const char* getDatabaseImageFilename(void) const {return m_database_image_filename;}
        /// Returns a constant pointer to the string array containing the filename of the mask image originally stored in the database (without the full route).
        inline const char* getDatabaseMaskFilename(void) const {return m_database_mask_filename;}
        /// Returns a constant pointer to the string array containing the filename of the meta-data information file originally stored in the database (without the full route).
        inline const char *getDatabaseMetadataFilename(void) const {return m_database_metadata_filename;}
        /// Returns a constant string array to the original image filename.
        inline const char* getImageFilename(void) const {return m_image_filename;}
        /// Returns a constant string array to the mask image filename.
        inline const char* getMaskFilename(void) const {return m_mask_filename;}
        /// Returns a constant string array to the meta-data information filename.
        inline const char* getMetadataFilename(void) const {return m_metadata_filename;}
        /// Sets the string array of the image filename stored in the database file (without the full route to the image).
        inline void setDatabaseImageFilename(const char *filename) {copy_str(filename, m_database_image_filename, FILENAME_SIZE);}
        /// Sets the string array of the image filename stored in the database file (without the full route to the image).
        inline void setDatabaseMaskFilename(const char *filename) {copy_str(filename, m_database_mask_filename, FILENAME_SIZE);}
        /// Sets the string array of the meta-data information filename stored in the database (without the full route to the image).
        inline void setDatabaseMetadataFilename(const char *filename) {copy_str(filename, m_database_metadata_filename, FILENAME_SIZE);}
        /// Sets the string array of the original image filename.
        inline void setImageFilename(const char *filename) {copy_str(filename, m_image_filename, ROUTE_SIZE);}
        /// Sets the string array of the mask image filename.
        inline void setMaskFilename(const char *filename) {copy_str(filename, m_mask_filename, ROUTE_SIZE);}
        /// Sets the string array of the meta-data information filename.
        inline void setMetadataFilename(const char *filename) {copy_str(filename, m_metadata_filename, ROUTE_SIZE);}
        /// Sets the route of the database images (i.e. the route to access from the working folder to the database file)
        inline void setDatabaseRoute(const char *route)
        {
            unsigned int index;
            index = copy_str(route, m_image_filename, ROUTE_SIZE);
            copy_str(m_database_image_filename, m_image_filename, ROUTE_SIZE, index);
            if (m_database_mask_filename[0] != '\0')
            {
                index = copy_str(route, m_mask_filename, ROUTE_SIZE);
                copy_str(m_database_mask_filename, m_mask_filename, ROUTE_SIZE, index);
            }
            else m_mask_filename[0] = '\0';
            if (m_database_metadata_filename[0] != '\0')
            {
                index = copy_str(route, m_metadata_filename, ROUTE_SIZE);
                copy_str(m_database_metadata_filename, m_metadata_filename, ROUTE_SIZE, index);
            }
            else m_metadata_filename[0] = '\0';
        }
        /// Returns a boolean indicating if the image has a mask image associated or not.
        inline bool hasMask(void) const {return m_mask_filename[0] != '\0';}
        /// Returns a boolean indicating if the image has a meta-data information file associated to it or not.
        inline bool hasMetadata(void) const {return m_metadata_filename[0] != '\0';}
        /// Returns the with of the image.
        inline unsigned int getWidth(void) const {return m_width;}
        /// Sets the with of the image.
        inline void setWidth(unsigned int width) { m_width = width;}
        /// Returns the height of the image.
        inline unsigned int getHeight(void) const {return m_height;}
        /// Sets the height of the image.
        inline void setHeight(unsigned int height) { m_height = height;}
        
        // Get/set bounding boxes information .........................................................
        /// Returns the number of bounding boxes associated with the image.
        inline unsigned int getNumberOfBoundingBoxes(void) const {return m_number_of_bounding_boxes;}
        /// Returns a constant pointer to the array of bounding boxes associated with the image.
        inline const BoundingBox* getBoundingBoxes(void) const {return m_bounding_boxes;}
        /// Returns a constant reference to the 'index'-th bounding box associated with the image.
        inline const BoundingBox& getBoundingBox(unsigned int index) const {return m_bounding_boxes[index];}
        /// Returns a reference to the 'index'-th bounding box associated with the image.
        inline BoundingBox& getBoundingBox(unsigned int index) {return m_bounding_boxes[index];}
        /// Sets/changes the amount of bounding boxes associated with the image. Previously defined bounding boxes are copied to the new structure up to the new bounding boxes structure size.
        void setNumberOfBoundingBoxes(unsigned int number_of_bounding_boxes);
        /// Returns true if the image has bounding boxes associated; false otherwise.
        inline bool hasBoundingBoxes(void) const {return m_number_of_bounding_boxes != 0;}
        
        // Get/set global image category information ..................................................
        /// Returns true if the given label index is present in the global categories set; false otherwise.
        inline bool hasLabel(int label) const
        {
            for (unsigned int i = 0; i < m_number_of_global_categories; ++i)
                if (m_global_categories[i].getLabel() == label) return true;
            return false;
        }
        /// Returns the number of global labels associated with the image.
        inline unsigned int getNumberOfGlobalCategories(void) const {return m_number_of_global_categories;}
        /// Returns a constant pointer to the global categories array.
        inline const GlobalCategory* getGlobalCategories(void) const {return m_global_categories;}
        /// Returns a pointer to the global categories array.
        inline GlobalCategory* getGlobalCategories(void) {return m_global_categories;}
        /// Returns a constant reference to the 'index'-th object global category of the image.
        inline const GlobalCategory& getGlobalCategory(unsigned int index) const {return m_global_categories[index];}
        /// Returns a reference to the 'index'-th object global category of the image.
        inline GlobalCategory& getGlobalCategory(unsigned int index) {return m_global_categories[index];}
        /// Sets/changes the amount of global categories associated with the image. Previously defined global categories are copied to the new structure up to the new global categories structure size.
        void setNumberOfGlobalCategories(unsigned int number_of_global_categories);
        
        /// Updates the labels index of the bounding boxes and global categories information objects associated with the image.
        inline void updateLabels(const CategoriesInformation *categories_information)
        {
            for (unsigned int i = 0; i < m_number_of_bounding_boxes; ++i) m_bounding_boxes[i].updateLabel(categories_information);
            for (unsigned int i = 0; i < m_number_of_global_categories; ++i) m_global_categories[i].updateLabel(categories_information);
        }
        
        // Convert the image information object from/to an XML object .................................
        /// Convert the image information object to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the image information object from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Original filename of the image file.
        char m_database_image_filename[FILENAME_SIZE];
        /// Original filename of the file containing the segmentation mask of the image.
        char m_database_mask_filename[FILENAME_SIZE];
        /// Original filename of the file containing the meta-data associated to the image.
        char m_database_metadata_filename[FILENAME_SIZE];
        /// Route to the original image.
        char m_image_filename[ROUTE_SIZE];
        /// Route to the mask image.
        char m_mask_filename[ROUTE_SIZE];
        /// Route to the meta-data file.
        char m_metadata_filename[ROUTE_SIZE];
        /// Number of bounding boxes associated with the image.
        unsigned int m_number_of_bounding_boxes;
        /// Array of bounding boxes.
        BoundingBox *m_bounding_boxes;
        /// Number of global categories associated with the image.
        unsigned int m_number_of_global_categories;
        /// Array of global categories associated with the image.
        GlobalCategory *m_global_categories;
        /// Width of the image.
        unsigned int m_width;
        /// Height of the image.
        unsigned int m_height;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE INFORMATION CLASS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Database image and its semantic annotation.
    class ImageInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]--------------------------------------
        /// Default constructor.
        ImageInformation(void);
        /** Parameters constructor a database image information structure is given as parameter.
         *  \param[in] image_information database image information which contains the route to the original image and its associated semantic information.
         *  \param[in] categories_information categories information structure used to determine the semantic label associated with each semantic identifier of the database image information structures.
         */
        ImageInformation(const DatabaseImageInformation &image_information, const CategoriesInformation &categories_information);
        /// Copy constructor.
        ImageInformation(const ImageInformation &copy);
        /// Destructor.
        ~ImageInformation(void);
        /// Assignation operator.
        ImageInformation& operator=(const ImageInformation &copy);
        
        // -[ Access to the image information ]--------------------------------------------------------
        /// Returns a constant pointer to the image.
        inline const Image<unsigned char>& getImage(void) const { return m_image; }
        /// Returns a constant reference to the pixels semantic information.
        inline const PixelSemanticInformation& getPixelSemanticInformation(void) const { return m_pixel_semantic_information; }
        /// Returns a constant reference to the vector of global image categories.
        inline const GlobalCategory* getGlobalCategories(void) const { return m_global_categories; }
        /// Returns the number of global categories associated with the image.
        inline unsigned int getNumberOfGlobalCategories(void) const { return m_number_of_global_categories; }
        /// Returns a constant reference to the vector of object bounding boxes.
        inline const BoundingBox* getBoundingBoxes(void) const { return m_bounding_boxes; }
        /// Returns the number of bounding boxes associated with the image.
        inline unsigned int getNumberOfBoundingBoxes(void) const { return m_number_of_bounding_boxes; }
        /// Returns a constant reference to the index-th bounding box object information associated with the image.
        const BoundingBox& getBoundingBox(unsigned int index) const { return m_bounding_boxes[index]; }
        /// Checks if the global semantic label is associated with the image.
        inline bool hasGlobalCategory(int label) { for (unsigned int i = 0; i < m_number_of_global_categories; ++i) if (m_global_categories[i].getLabel() == label) return true; return false; }
        /// Returns a pointer to a new image information object which have been transformed using a projective matrix transform.
        inline ImageInformation transformInformation(const double *transform) const { ImageInformation image_information; transformInformation(transform, image_information); return image_information; }
        /// Returns a pointer to a new image information object which have been transformed using a projective matrix transform.
        void transformInformation(const double *transform, ImageInformation &image_information) const;
        /// Returns an image where each pixels has a color corresponding to its semantic categories.
        inline Image<unsigned char> getSemanticColorImage(void) const { return m_pixel_semantic_information.createSemanticColorsImage(*m_categories_information); }
        /// Returns an image where each pixels has a color corresponding to its semantic categories.
        inline void getSemanticColorImage(Image<unsigned char> &result) const { m_pixel_semantic_information.createSemanticColorsImage(*m_categories_information, result); }
        /// Returns the width of the image.
        inline unsigned int getWidth(void) const { return m_image.getWidth(); }
        /// Returns the height of the image.
        inline unsigned int getHeight(void) const { return m_image.getHeight(); }
        /// Returns a constant pointer to the information of the semantic categories of the image.
        inline const CategoriesInformation * getCategoriesInformation(void) const { return m_categories_information; }
    private:
        /// Color image to which the semantic information is associated.
        Image<unsigned char> m_image;
        /// Semantic information of each pixel of the image.
        PixelSemanticInformation m_pixel_semantic_information;
        /// Array of global categories associated with the image.
        GlobalCategory *m_global_categories;
        /// Number of global categories associated with the image.
        unsigned int m_number_of_global_categories;
        /// Array of object bounding boxes.
        BoundingBox *m_bounding_boxes;
        /// Number of bounding boxes associated with the image.
        unsigned int m_number_of_bounding_boxes;
        /// Constant pointer to the categories information structure used to obtain the semantic labels from the database information.
        const CategoriesInformation *m_categories_information;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DATABASE INFORMATION CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Information about the train, validation and test sets of images and their semantic annotation.
    class DatabaseInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]--------------------------------------
        
        /// Default constructor.
        DatabaseInformation(void);
        /// Constructor where the database filename is given as parameter.
        DatabaseInformation(const char *database_filename);
        /// Constructor where the categories information structure and the train, validation and test arrays are given as parameters.
        DatabaseInformation(const CategoriesInformation &categories_information, const DatabaseImageInformation *train_set, unsigned int number_of_train_images, const DatabaseImageInformation *validation_set, unsigned int number_of_validation_images, const DatabaseImageInformation *test_set, unsigned int number_of_test_images);
        /// Copy constructor.
        DatabaseInformation(const DatabaseInformation &copy);
        /// Destructor.
        ~DatabaseInformation(void);
        /// Assignation operator.
        DatabaseInformation& operator=(const DatabaseInformation &copy);
        
        // -[ Access to the train/validation/test information structures ]-----------------------------
        
        /// Return a constant reference to the categories information of the database.
        inline const CategoriesInformation& getCategoriesInformation(void) const { return m_categories_information; }
        /// Returns a reference to the categories information of the database.
        inline CategoriesInformation& getCategoriesInformation(void) { return m_categories_information; }
        
        /// Returns a constant reference to the train information of the index-th image of the database.
        inline const DatabaseImageInformation& getTrainImage(unsigned int index) const { return m_train_set[index]; }
        /// Returns a reference to the train information of the index-th image of the database.
        inline DatabaseImageInformation& getTrainImage(unsigned int index) { return m_train_set[index]; }
        /// Returns the number of train images of the database.
        inline unsigned int getNumberOfTrainImages(void) const { return m_number_of_train_images; }
        
        /// Returns a constant reference to the validation information of the index-th image of the database.
        inline const DatabaseImageInformation& getValidationImage(unsigned int index) const { return m_validation_set[index]; }
        /// Returns a reference to the validation information of the index-th image of the database.
        inline DatabaseImageInformation& getValidationImage(unsigned int index) { return m_validation_set[index]; }
        /// Returns the number of validation images of the database.
        inline unsigned int getNumberOfValidationImages(void) const { return m_number_of_validation_images; }
        
        /// Returns a constant reference to the test information of the index-th image of the database.
        inline const DatabaseImageInformation& getTestImage(unsigned int index) const { return m_test_set[index]; }
        /// Returns a reference to the test information of the index-th image of the database.
        inline DatabaseImageInformation& getTestImage(unsigned int index) { return m_test_set[index]; }
        /// Returns the number of test images of the database.
        inline unsigned int getNumberOfTestImages(void) const { return m_number_of_test_images; }
        
        /// Updates the semantic labels associated to the database images with the given categories information constant object (the database object is not updated).
        void updateLabels(const CategoriesInformation *categories_information);
        /// Updates the semantic labels associated to the database images, so that, they are synchronized with the database categories information structure.
        inline void updateLabels(void) { updateLabels(&m_categories_information); }
        /// Sets the route from the working folder to the database folder for all images of the database information object.
        void setDatabaseRoute(const char *route);
        /// Returns the route from the working folder to the database folder.
        const char* getDatabaseRoute(void) const { return m_database_route; }
        
        /// Sets the number of train images of the database.
        inline void setNumberOfTrainImages(unsigned int number_of_train_images)
        {
            if (m_train_set != 0) delete [] m_train_set;
            m_number_of_train_images = number_of_train_images;
            if (number_of_train_images != 0) m_train_set = new DatabaseImageInformation[number_of_train_images];
            else m_train_set = 0;
        }
        /// Sets the number of validation images of the database.
        inline void setNumberOfValidationImages(unsigned int number_of_validation_images)
        {
            if (m_validation_set != 0) delete [] m_validation_set;
            m_number_of_validation_images = number_of_validation_images;
            if (number_of_validation_images != 0) m_validation_set = new DatabaseImageInformation[number_of_validation_images];
            else m_validation_set = 0;
        }
        /// Sets the number of test images of the database.
        inline void setNumberOfTestImages(unsigned int number_of_test_images)
        {
            if (m_test_set != 0) delete [] m_test_set;
            m_number_of_test_images = number_of_test_images;
            if (number_of_test_images != 0) m_test_set = new DatabaseImageInformation[number_of_test_images];
            else m_test_set = 0;
        }
        /// Enumerate of the different image sets: train, validation, test.
        enum IMAGE_SET {TRAIN_SET = 1, VALIDATION_SET = 2, TEST_SET = 4, TRAIN_VALIDATION_SET = 8, ALL_SETS = 16, TRAIN_TEST_SET = 32, VALIDATION_TEST_SET = 64};
        /// Enumerate of the different kinds of the information provided by the image information structures.
        enum IMAGE_TYPE {ALL_IMAGES = 1, SEGMENTATION_IMAGES_ONLY = 2, BOUNDINGBOXES_IMAGES_ONLY = 4};
        
        /// This class contains a list of pointers to a subset of image information objects of the original database information structure.
        class Subset
        {
        public:
            // -[ Constructors, destructor and assignation operator ]----------------------------------
            /// Default constructor.
            Subset(void);
            /// Parameters constructor where an array pointing to the database images information subset and their associated categories information are given as a parameter.
            Subset(const DatabaseImageInformation **&iterative_array, unsigned int number_of_images, const CategoriesInformation *categories_information);
            /// Copy constructor.
            Subset(const Subset &subset);
            /// Destructor.
            ~Subset(void);
            /// Assignation operator.
            Subset& operator=(const Subset &iter);
            
            // -[ Iterator access functions ]----------------------------------------------------------
            /// Returns the total number of image information object which can be accessed with the iterator (it is a different from the number of the database images).
            inline unsigned int size(void) const { return m_number_of_images; }
            /// Returns a constant pointer to the index-th image information object of the iterator.
            inline const DatabaseImageInformation* operator[](unsigned int index) const { return m_images_information[index]; }
            /// Moves the internal subset pointer to the first image information object.
            inline void begin(void) { m_inner_pointer = &m_images_information[0]; }
            /// Moves the internal subset pointer to the last image information object.
            inline void end(void) { m_inner_pointer = &m_images_information[m_number_of_images]; }
            /// Returns a constant pointer to the image information object pointed by the internal subset pointer.
            inline const DatabaseImageInformation* operator*(void) const { return *m_inner_pointer; }
            /// Moves the internal subset pointer to the next image information object. The destination of the internal pointer is not checked, so that, it can go beyond the last image information object subset.
            inline void operator++(void) { ++m_inner_pointer; }
            /// Moves the internal subset pointer to the previous image information object. The destination of the internal pointer is not checked, so that, it can go beyond the first image information object subset.
            inline void operator--(void) { --m_inner_pointer; }
            /// Returns a boolean which indicates if the internal pointer points to the first image information object of the subset.
            inline bool isFirst(void) const { return m_inner_pointer == &m_images_information[0]; }
            /// Returns a boolean which indicates if the internal pointer points to the last image information object of the subset.
            inline bool isLast(void) const { return m_inner_pointer == &m_images_information[m_number_of_images]; }
            
            /// Returns a color copy of the original image pointed by the internal subset pointer.
            inline Image<unsigned char> getOriginalColorImage(void) const { Image<unsigned char> result; getOriginalColorImage(result); return result; }
            /// Returns a reference of the color original image pointed by the internal subset pointer.
            void getOriginalColorImage(Image<unsigned char> &result) const;
            /// Returns a color copy of the original image corresponding to the index-th subset image information object.
            inline Image<unsigned char> getOriginalColorImage(unsigned int index) const { Image<unsigned char> result; getOriginalColorImage(index, result); return result; }
            /// Returns a reference of the color original image corresponding to the index-th subset image information object.
            void getOriginalColorImage(unsigned int index, Image<unsigned char> &result) const;
            
            /// Returns a grayscale copy of the original image pointed by the internal subset pointer.
            inline Image<unsigned char> getOriginalGrayImage(void) const { Image<unsigned char> result; getOriginalGrayImage(result); return result; }
            /// Returns a reference of the grayscale original image pointed by the internal subset pointer.
            void getOriginalGrayImage(Image<unsigned char> &result) const;
            /// Returns a grayscale copy of the original image corresponding to the index-th subset image information object.
            inline Image<unsigned char> getOriginalGrayImage(unsigned int index) const { Image<unsigned char> result; getOriginalGrayImage(index, result); return result; }
            /// Returns a reference of the grayscale original image corresponding to the index-th subset image information object.
            void getOriginalGrayImage(unsigned int index, Image<unsigned char> &result) const;
            
            /// Returns the annotation image pointed by the internal subset pointer where category of each pixel is represented by a color code.
            inline Image<unsigned char> getMaskImage(void) const { Image<unsigned char> result; getMaskImage(result); return result; }
            /// Returns a reference to the annotation image pointed by the internal subset pointer where category of each pixel is represented by a color code.
            void getMaskImage(Image<unsigned char> &result) const;
            /// Returns the annotation image corresponding to the index-th subset image information object where category of each pixel is represented by a color code.
            inline Image<unsigned char> getMaskImage(unsigned int index) const { Image<unsigned char> result; getMaskImage(index, result); return result; }
            /// Returns a reference to the annotation image corresponding to the index-th subset image information object where category of each pixel is represented by a color code.
            void getMaskImage(unsigned int index, Image<unsigned char> &result) const;
            
            /// Returns a new pixel semantic information object of the current image information object pointed by the internal pointer.
            PixelSemanticInformation* getPixelSegmentationInformation(void) const;
            /// Returns a new pixel semantic information object of the index-th image information object of the subset.
            PixelSemanticInformation* getPixelSegmentationInformation(unsigned int index) const;
            
            /// Returns a new pixel semantic information object of the current image information object pointed by the internal pointer.
            inline ImageInformation* getImageInformation(void) const { return new ImageInformation(**m_inner_pointer, *m_categories_information); }
            /// Returns a new pixel semantic information object of the index-th image information object of the subset.
            inline ImageInformation* getImageInformation(unsigned int index) const { return new ImageInformation(*m_images_information[index], *m_categories_information); }
            
            /// Returns a constant pointer to the subset categories information object.
            inline const CategoriesInformation* getCategoriesInformation(void) const { return m_categories_information; }
        private:
            /// Number of images of the database subset.
            unsigned int m_number_of_images;
            /// Array storing all image information objects of the database.
            const DatabaseImageInformation **m_images_information;
            /// Internal pointer of the subset.
            const DatabaseImageInformation **m_inner_pointer;
            /// Constant pointer to the categories information associated with the images.
            const CategoriesInformation *m_categories_information;
        };
        /// Returns a subset for the specified images information types and sets of the database.
        Subset getIterator(IMAGE_TYPE image_type, IMAGE_SET image_set) const;
        /// Converts the database information object into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the database information object from a XML object.
        void convertFromXML(XmlParser &parser);
    private:
        /// Character array which stores the route to the database.
        char m_database_route[ROUTE_SIZE];
        /// Constant pointer to the categories information object currently used to assign semantic labels to the image information objects of the database.
        const CategoriesInformation *m_current_categories_information;
        /// Categories information object associated with the database.
        CategoriesInformation m_categories_information;
        /// Set of train images information objects of the database.
        DatabaseImageInformation *m_train_set;
        /// Number of train images in the database.
        unsigned int m_number_of_train_images;
        /// Set of validation images information objects of the database.
        DatabaseImageInformation *m_validation_set;
        /// Number of validation images in the database.
        unsigned int m_number_of_validation_images;
        /// Set of test images information objects of the database.
        DatabaseImageInformation *m_test_set;
        /// Number of test images in the database.
        unsigned int m_number_of_test_images;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif
