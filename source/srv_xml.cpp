// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_xml.hpp"

namespace srv
{
    XmlParser::XmlParser(std::istream *input_stream, bool attributes_case_sensitive) :
        m_line(0),
        m_token(0),
        m_is_eof(false),
        m_input_stream(input_stream),
        m_attributes_case_sensitive(attributes_case_sensitive)
    {
        try
        {
            Scanner();
            Parser();
        }
        catch (ExceptionXmlEof &e)
        {
            if (m_tag_identifier.size() > 0)
                ThrowException("Unexpected end of file.");
            m_is_eof = true;
        }
    }
    
    XmlParser::XmlParser(std::ostream *output_stream, bool attributes_case_sensitive) :
        m_output_stream(output_stream),
        m_attributes_case_sensitive(attributes_case_sensitive)
    {
        *m_output_stream << "<?xml version=\"1.0\" encoding='UTF-8'?>" << std::endl;
    }
    
    XmlParser::~XmlParser(void)
    {
    }
    
    void XmlParser::openTag(const char * name)
    {
        m_current_tag_identifier = name;
        m_tag_identifier.push_back(name);
        m_is_close_tag = true;
        m_attributes.clear();
    }
    
    void XmlParser::addChildren(void)
    {
        m_is_close_tag = false;
        *m_output_stream << "<" << m_current_tag_identifier;
        if (m_succeeding_attributes.size() > 0)
        {
            for (std::map<std::string, std::string>::iterator begin = m_succeeding_attributes.begin(), end = m_succeeding_attributes.end(); begin != end; ++begin)
                if (m_attributes.find(begin->first.c_str()) == m_attributes.end())
                    m_attributes[begin->first.c_str()] = begin->second.c_str();
            m_succeeding_attributes.clear();
        }
        if (m_attributes.size() > 0)
        {
            for (std::map<std::string, std::string>::iterator begin = m_attributes.begin(), end = m_attributes.end(); begin != end; ++begin)
            {
                const char * current_string = begin->second.c_str();
                *m_output_stream << " " << begin->first.c_str() << "='";
                for (unsigned int i = 0; current_string[i] != '\0'; ++i)
                {
                    if      (current_string[i] == '<')  *m_output_stream << "&lt;";
                    else if (current_string[i] == '&')  *m_output_stream << "&amp;";
                    else if (current_string[i] == '>')  *m_output_stream << "&gt;";
                    else if (current_string[i] == '"')  *m_output_stream << "&quot;";
                    else if (current_string[i] == '\'') *m_output_stream << "&apos;";
                    else  *m_output_stream << current_string[i];
                }
                *m_output_stream << "'";
            }
        }
        *m_output_stream << ">" << std::endl;
    }
    
    void XmlParser::closeTag(void)
    {
        if (m_is_close_tag)
        {
            *m_output_stream << "<" << m_current_tag_identifier;
            if (m_succeeding_attributes.size() > 0)
            {
                for (std::map<std::string, std::string>::iterator begin = m_succeeding_attributes.begin(), end = m_succeeding_attributes.end(); begin != end; ++begin)
                    if (m_attributes.find(begin->first.c_str()) == m_attributes.end())
                        m_attributes[begin->first.c_str()] = begin->second.c_str();
                m_succeeding_attributes.clear();
            }
            if (m_attributes.size() > 0)
            {
            for (std::map<std::string, std::string>::iterator begin = m_attributes.begin(), end = m_attributes.end(); begin != end; ++begin)
            {
                const char * current_string = begin->second.c_str();
                *m_output_stream << " " << begin->first.c_str() << "='";
                for (unsigned int i = 0; current_string[i] != '\0'; ++i)
                {
                    if      (current_string[i] == '<')  *m_output_stream << "&lt;";
                    else if (current_string[i] == '&')  *m_output_stream << "&amp;";
                    else if (current_string[i] == '>')  *m_output_stream << "&gt;";
                    else if (current_string[i] == '"')  *m_output_stream << "&quot;";
                    else if (current_string[i] == '\'') *m_output_stream << "&apos;";
                    else  *m_output_stream << current_string[i];
                }
                *m_output_stream << "'";
            }
            }
            *m_output_stream << "/>" << std::endl;
        }
        else *m_output_stream << "</" << m_current_tag_identifier << ">" << std::endl;
        m_tag_identifier.pop_back();
        if (m_tag_identifier.size() > 0) m_current_tag_identifier = m_tag_identifier.back();
        else m_current_tag_identifier = "";
        m_is_close_tag = false;
    }
    
    void XmlParser::Parser(void)
    {
        m_attributes.clear();
        m_is_close_tag = false;
        m_is_value = false;
        
        if (m_category == SOpenTag)
        {
            Scanner();
            if (m_category == SIdentifier) // Opening tag.
            {
                m_tag_identifier.push_back(std::string(m_string_value));
                m_current_tag_identifier = std::string(m_string_value);
                Scanner();
                while (m_category != SCloseTag)
                {
                    if (m_category == SIdentifier)
                    {
                        std::string attribute_name;
                        if (!m_attributes_case_sensitive)
                        {
                            for (unsigned int index = 0; m_string_value[index] != '\0'; ++index)
                                if ((m_string_value[index] >= 'A') && (m_string_value[index] <= 'Z'))
                                    m_string_value[index] = (char)((int)m_string_value[index] + ((int)'a' - (int)'A'));
                        }
                        attribute_name = m_string_value;
                        Scanner();
                        if (m_category != SAssignation) ThrowException("Expected assignation '=' symbol.");
                        Scanner();
                        m_attributes[attribute_name] = std::string(m_string_value);
                        Scanner();
                    }
                    if (m_category == SSlash)
                    {
                        m_is_close_tag = true;
                        m_tag_identifier.pop_back();
                        Scanner();
                        if (m_category != SCloseTag) ThrowException("Expected tag closing symbol '>'.");
                        Scanner();
                        return;
                    }
                }
                Scanner(true);
            }
            else if (m_category == SSlash) // Closing tag.
            {
                Scanner();
                if (m_category != SIdentifier) ThrowException("Expected a tag identifier");
                if (m_tag_identifier.size () == 0) ThrowException("No opening tag have been found to now find a closing tag.");
                if (std::string(m_string_value) != m_tag_identifier.back()) ThrowException("The closing tag is not matched with the opening tag");
                m_current_tag_identifier = m_tag_identifier.back();
                m_is_close_tag = true;
                m_tag_identifier.pop_back();
                Scanner();
                if (m_category != SCloseTag) ThrowException("Expected tag closing symbol '>'.");
                Scanner();
                return;
            }
            else ThrowException("Expected a tag identifier string or slash symbol.");
        }
        else if (m_category == SIdentifier) // Data inside the tags.
        {
            for (unsigned int i = 0; i < XML_STRING_SIZE; ++i)
                m_value[i] = m_string_value[i];
            // Data must be stored into an special array!!!
            Scanner(); // Reads the next tag... but what about the data?? 
            m_is_value = true;
            return;
        }
        else
        {
            ThrowException("Unexpected token.");
        }
    }
    
    void XmlParser::specialCharacters(const char * expected_values, unsigned int expected_size, char expected_character, std::list<char> &buffer, unsigned int &index)
    {
        unsigned int idx;
        char c;
        
        for (idx = 0; idx < expected_size; ++idx)
        {
            c = (char)read();
            buffer.push_back(c);
            if (c != expected_values[idx])
                break;
        }
        if (idx == expected_size)
        {
            m_string_value[index++] = expected_character;
            buffer.clear();
        }
    }
    
    void XmlParser::Scanner(bool data)
    {
        unsigned int index;
        int c;
        for (;;)
        {
            c = read();
            switch (c)
            {
            // Symbols which are ignored (spaces, tabs, backspace, enters, ...)
            case ' ':
            case '\n':
            case '\t':
            case '\r':
                break;
            case '<':
                c = read();
                // XML Comments <!-- COMENT -->
                if (c == '!')
                {
                    c = read();
                    if (c == '-')
                    {
                        c = read();
                        if (c == '-')
                        {
                            while (true)
                            {
                                c = read();
                                if (c == '-')
                                {
                                    c = read();
                                    if (c == '-')
                                    {
                                        c = read();
                                        if (c == '>') break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            putback((char)c); // Open tag symbol.
                            putback('-');
                            putback('!');
                            m_category = SOpenTag;
                            return;
                        }
                    }
                    else // Open tag symbol.
                    {
                        putback((char)c);
                        putback('!');
                        m_category = SOpenTag;
                        return;
                    }
                }
                else if (c == '?') // Version initial tag.
                {
                    while (true)
                    {
                        c = read();
                        if (c == '?')
                        {
                            c = read();
                            if (c == '>') break;
                        }
                    }
                    break;
                }
                else // Open tag symbol.
                {
                    putback((char)c);
                    m_category = SOpenTag;
                    return;
                }
                break;
            case '>': // Close tag symbol.
                m_category = SCloseTag;
                return;
            case '/': // Slash symbol.
                m_category = SSlash;
                return;
            case '=': // Assignation symbol.
                m_category = SAssignation;
                return;
            case '"': // Strings.
                index = 0;
                c = read();
                for (; c != '"'; )
                {
                    if (index >= XML_STRING_SIZE - 2) ThrowException("Too long string");
                    m_string_value[index++] = (char)c;
                    c = read();
                    if (c == '\\')
                    {
                        c = read();
                        if (c == '\'') m_string_value[index++] = '\'';
                        else if (c == '\"') m_string_value[index++] = '\"';
                        else
                        {
                            m_string_value[index++] = '\\';
                            m_string_value[index++] = (char)c;
                        }
                        
                        c = read();
                    }
                    else if (c == '&')
                    {
                        std::list<char> buffer;
                        
                        buffer.push_back((char)c);
                        c = read();
                        buffer.push_back((char)c);
                        if      (c == 'l') specialCharacters(  "t;", 2, '<' , buffer, index);
                        else if (c == 'a') specialCharacters( "mp;", 3, '&' , buffer, index);
                        else if (c == 'g') specialCharacters(  "t;", 3, '>' , buffer, index);
                        else if (c == 'q') specialCharacters("uot;", 4, '"' , buffer, index);
                        else if (c == 'a') specialCharacters("pos;", 4, '\'', buffer, index);
                        for (auto &b : buffer) m_string_value[index++] = b;
                    }
                }
                m_string_value[index] = '\0';
                m_category = SString;
                return;
            case '\'': // Strings.
                index = 0;
                c = read();
                for (; c != '\''; )
                {
                    if (index >= XML_STRING_SIZE - 2) ThrowException("Too long string");
                    if (c == '\\')
                    {
                        c = read();
                        if (c == '\'') m_string_value[index++] = '\'';
                        else if (c == '\"') m_string_value[index++] = '\"';
                        else
                        {
                            m_string_value[index++] = '\\';
                            m_string_value[index++] = (char)c;
                        }
                        
                        c = read();
                    }
                    else if (c == '&')
                    {
                        std::list<char> buffer;
                        
                        buffer.push_back((char)c);
                        c = read();
                        buffer.push_back((char)c);
                        if      (c == 'l') specialCharacters(  "t;", 2, '<' , buffer, index);
                        else if (c == 'a')
                        {
                            c = read();
                            buffer.push_back((char)c);
                            if      (c == 'm') specialCharacters( "p;", 2, '&' , buffer, index);
                            else if (c == 'p') specialCharacters("os;", 3, '\'', buffer, index);
                        }
                        else if (c == 'g') specialCharacters(  "t;", 3, '>' , buffer, index);
                        else if (c == 'q') specialCharacters("uot;", 4, '"' , buffer, index);
                        if (buffer.size() > 0)
                        {
                            c = buffer.back();
                            buffer.pop_back();
                            for (auto &b : buffer) m_string_value[index++] = b;
                        }
                        else c = read();
                    }
                    else
                    {
                        m_string_value[index++] = (char)c;
                        c = read();
                    }
                }
                m_string_value[index] = '\0';
                m_category = SString;
                return;
            default:
                if (data) // This is a fast patch... Spaghetti coding.
                {
                    index = 0;
                    for (; (c != '<') && (c != '\n');)
                    {
                        if (index >= XML_STRING_SIZE - 2) ThrowException("String is too long");
                        m_string_value[index++] = (char)c;
                        c = read();
                    }
                    m_string_value[index] = '\0';
                    putback((char)c);
                    m_category = SIdentifier;
                    return;
                }
                else
                {
                    // Identifiers.
                    if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || (c == '_') || ((c >= '0') && (c <= '9')) || (c == '.'))
                    {
                        index = 0;
                        for (; ((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')) || (c == '_') || ((c >= '0') && (c <= '9')) || (c == '.');)
                        {
                            if (index >= XML_STRING_SIZE - 2) ThrowException("Too long identifier");
                            m_string_value[index++] = (char)c;
                            c = read();
                        }
                        m_string_value[index] = '\0';
                        putback((char)c);
                        m_category = SIdentifier;
                        return;
                    }
                    else
                    {
                        char message[4096];
                        if (c >= 32)
                            sprintf(message, "Unexpected symbol %c (%d)", (char)c, c);
                        else sprintf(message, "Unexpected symbol (%d)", c);
                        ThrowException(message);
                    }
                }
            }
        }
    }
    
}

