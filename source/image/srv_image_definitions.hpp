// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_DEFINITIONS_HEADER_FILE__
#define __SRV_IMAGE_DEFINITIONS_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | IMAGE DEFINITIONS                    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Video format of the camera.
    enum CAMERA_IMAGE_FORMAT
    {
        YUYV_FORMAT,
        JPEG_FORMAT,
        MJPEG_FORMAT
    };
    
    /// Enumerate the different buttons of the mouse which can be active at the moment.
    enum FIGURE_MOUSE_BUTTON
    {
        BUTTON_LEFT,
        BUTTON_RIGHT,
        BUTTON_CENTER,
        BUTTON_NONE
    };
    
    /// Enumerate the different key mask modifier (e.g.\ control, alt, shift) while a mouse button is pressed.
    enum FIGURE_MOUSE_KEY
    {
        MASK_CONTROL,
        MASK_SHIFT,
        MASK_NONE
    };
    
    /// Image formats which can be loaded or saved to disk.
    enum IMAGE_FORMATS
    {
        IMAGE_FORMAT_EXTENSION = 11001, ///< Select the image format from the filename extension.
        IMAGE_FORMAT_PNG,               ///< PNG image.
        IMAGE_FORMAT_JPEG               ///< JPEG image.
    };
    
    /// Method used to approximate the recursive filter.
    enum RECURSIVE_GAUSSIAN_METHOD
    {
        DERICHE = 19301, ///< Method proposed in 'R.\ Deriche, "Recursively implementing the Gaussian and its Derivatives", ICIP 1992.
        VLIET_3RD,       ///< 3rd order filter of the method proposed in 'L.\ van Vliet, I.\ Young, P.\ Verbeek, "Recursive Gaussian derivative filters." ICPR 1998.
        VLIET_4TH,       ///< 4th order filter of the method proposed in 'L.\ van Vliet, I.\ Young, P.\ Verbeek, "Recursive Gaussian derivative filters." ICPR 1998.
        VLIET_5TH        ///< 5th order filter of the method proposed in 'L.\ van Vliet, I.\ Young, P.\ Verbeek, "Recursive Gaussian derivative filters." ICPR 1998.
    };
    
    /// Sampling method used by the image transformation algorithms.
    enum IMAGE_SAMPLING_METHOD
    {
        NEAREST = 17001, ///< Nearest pixel.
        BILINEAR         ///< Bi-linear interpolation.
    };
    
    /// Identifiers of the distances available to calculate the distance transform image.
    enum IMAGE_DISTANCE_TRANSFORM {
        IDT_CHESSBOARD = 19400,     ///< Identifier of the Chessboard or Chebyshev distance used to calculate the distance transform.
        IDT_TAXICAB,                ///< Identifier of the Taxicab or Manhattan distance used to calculate the distance transform.
        IDT_EUCLIDEAN               ///< Identifier of the Euclidean distance used to calculate the distance transform.
    };
    
    /// Identifier of the thinning algorithms.
    enum THINNING_METHOD
    {
        THIN_ZHANG_SUEN = 95000,    ///< Identifier of the Zhang-Suen thinning algorithm.
        THIN_GUO_HALL               ///< Identifier of the Guo-Hall thinning algorithm.
    };
    
    /// Identifier of the connectivity between pixels of the image.
    enum IMAGE_CONNECTIVITY
    {
        CONNECT4 = 19500,           ///< 4-Connectivity, i.e. only the surrounding pixels at north, east, south and west coordinates are considered neighbors.
        CONNECT8                    ///< 8-Connectivity, i.e. all surrounding eight pixels are considered neighbors.
    };
    
    /// Identifier of the pre-processing methods of the image color used by the Seeds algorithms.
    enum SEGMENTATION_PREPROCESS
    {
        CONVERT_TO_LAB = 19600, ///< Convert the input images to the L*a*b color space.
        CONVERT_TO_HSV,         ///< Convert the input images to the HSV color space.
        CONVERT_NOT             ///< Process the raw input images without changing their color space.
    };
    
    enum SPECIAL_KEYS
    {
        KEY_NONE            = -1,
        KEY_GRAVE           = 65104, // ` GRAVE
        KEY_ACUTE           = 65105, // ´ ACUTE
        KEY_CIRCUMFLEX      = 65106, // ^ CIRCUMFLEX
        KEY_DIAERESIS       = 65111, // " DIAERESIS
        KEY_SHIFT           = 65505,
        KEY_CTRL            = 65507,
        KEY_ALT             = 65513,
        KEY_ALT_GR          = 65027,
        KEY_SUPER_LEFT      = 65515,
        KEY_SUPER_RIGHT     = 65516,
        KEY_CMD             = 65383,
        KEY_KEYPAD_LEFT     = 65430,
        KEY_KEYPAD_UP       = 65431,
        KEY_KEYPAD_RIGHT    = 65432,
        KEY_KEYPAD_DOWN     = 65433,
        KEY_LEFT            = 65361,
        KEY_UP              = 65362,
        KEY_RIGHT           = 65363,
        KEY_DOWN            = 65364,
        KEY_HOME            = 65360,
        KEY_END             = 65367,
        KEY_NEXT_PAGE       = 65365,
        KEY_PREV_PAGE       = 65366,
        KEY_TAB             = 9,
        KEY_BACKSPACE       = 8,
        KEY_ESC             = 27,
        KEY_ENTER           = 13
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

