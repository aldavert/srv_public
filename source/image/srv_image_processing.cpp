// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_image_processing.hpp"
#include <complex>

namespace srv
{
    
#ifdef __ENABLE_FFTW__
    //                   +--------------------------------------+
    //                   | FREQUENCY DOMAIN IMAGES CLASS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ImageFFT::ImageFFT(void) :
        m_width(0),
        m_fft_width(0),
        m_height(0),
        m_number_of_channels(0),
        m_fft_image(0)
    {
    }
    
    ImageFFT::ImageFFT(unsigned int width, unsigned int height, unsigned int number_of_channels) :
        m_width(width),
        m_fft_width(width / 2 + 1),
        m_height(height),
        m_number_of_channels(number_of_channels),
        m_fft_image((number_of_channels > 0)?new fftw_complex * [number_of_channels]:0)
    {
        for (unsigned int c = 0; c < number_of_channels; ++c)
            m_fft_image[c] = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * m_fft_width * m_height);
    }
    
    ImageFFT::ImageFFT(const ImageFFT &other) :
        m_width(other.m_width),
        m_fft_width(other.m_fft_width),
        m_height(other.m_height),
        m_number_of_channels(other.m_number_of_channels),
        m_fft_image((other.m_number_of_channels > 0)?new fftw_complex * [other.m_number_of_channels]:0)
    {
        for (unsigned int c = 0; c < other.m_number_of_channels; ++c)
        {
            const fftw_complex * __restrict__ src_ptr = other.m_fft_image[c];
            fftw_complex * __restrict__ dst_ptr = m_fft_image[c] = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * other.m_fft_width * other.m_height);
            
            for (unsigned int y = 0; y < other.m_height; ++y)
            {
                for (unsigned int x = 0; x < other.m_fft_width; ++x, ++src_ptr, ++dst_ptr)
                {
                    (*dst_ptr)[0] = (*src_ptr)[0];
                    (*dst_ptr)[1] = (*src_ptr)[1];
                }
            }
        }
    }
    
    ImageFFT::~ImageFFT(void)
    {
        if (m_number_of_channels > 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c)
                fftw_free(m_fft_image[c]);
            delete [] m_fft_image;
        }
    }
    
    ImageFFT& ImageFFT::operator=(const ImageFFT &other)
    {
        if (this != &other)
        {
            // Free ...................................................................................................
            if (m_number_of_channels > 0)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c)
                    fftw_free(m_fft_image[c]);
                delete [] m_fft_image;
            }
            // Copy ...................................................................................................
            m_width = other.m_width;
            m_fft_width = other.m_fft_width;
            m_height = other.m_height;
            m_number_of_channels = other.m_number_of_channels;
            if (other.m_number_of_channels > 0)
            {
                m_fft_image = new fftw_complex * [other.m_number_of_channels];
                for (unsigned int c = 0; c < other.m_number_of_channels; ++c)
                {
                    const fftw_complex * __restrict__ src_ptr = other.m_fft_image[c];
                    fftw_complex * __restrict__ dst_ptr = m_fft_image[c] = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * other.m_fft_width * other.m_height);
                    
                    for (unsigned int y = 0; y < other.m_height; ++y)
                    {
                        for (unsigned int x = 0; x < other.m_fft_width; ++x, ++src_ptr, ++dst_ptr)
                        {
                            (*dst_ptr)[0] = (*src_ptr)[0];
                            (*dst_ptr)[1] = (*src_ptr)[1];
                        }
                    }
                }
            }
            else m_fft_image = 0;
        }
        
        return *this;
    }
    
    void ImageFFT::setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels)
    {
        // Free .......................................................................................................
        if (m_number_of_channels > 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c)
                fftw_free(m_fft_image[c]);
            delete [] m_fft_image;
        }
        // Allocate ...................................................................................................
        m_width = width;
        m_fft_width = width / 2 + 1;
        m_height = height;
        m_number_of_channels = number_of_channels;
        if (number_of_channels > 0)
        {
            m_fft_image = new fftw_complex * [number_of_channels];
            for (unsigned int c = 0; c < number_of_channels; ++c)
                m_fft_image[c] = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * m_fft_width * m_height);
        }
    }
    
    void ImageFFT::getImage(Image<double> &destination, unsigned int number_of_threads) const
    {
        if ((destination.getWidth() != m_width) || (destination.getHeight() != m_height) || (destination.getNumberOfChannels() != m_number_of_channels))
            throw Exception("The geometry of the given image (%dx%dx%d) differs from the expected image (%dx%dx%d).", destination.getWidth(), destination.getHeight(), destination.getNumberOfChannels(), m_width, m_height, m_number_of_channels);
        const double normalization_factor = (double)(m_width * m_height);
        if (number_of_threads > 1)
        {
            fftw_init_threads();
            fftw_plan_with_nthreads(number_of_threads);
        }
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            // FFTW_MEASURE -> Search the best parameters.
            // FFTW_ESTIMATE -> Uses a reasonably good parameters but, they may not be the best.
            fftw_plan plan  = fftw_plan_dft_c2r_2d(m_height, m_width, m_fft_image[c], destination.get(c), FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);
            fftw_cleanup();
            for (unsigned int y = 0; y < m_height; ++y)
            {
                double * __restrict__ dst_ptr = destination.get(y, c);
                for (unsigned int x = 0; x < m_width; ++x, ++dst_ptr)
                    *dst_ptr /= normalization_factor;
            }
        }
        if (number_of_threads > 1) fftw_cleanup_threads();
    }
    
    void ImageFFT::getComplex(Image<double> &real, Image<double> &imaginary) const
    {
        real.setGeometry(m_fft_width, m_height, m_number_of_channels);
        imaginary.setGeometry(m_fft_width, m_height, m_number_of_channels);
        
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            const fftw_complex * __restrict__ src_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                double * __restrict__ real_ptr = real.get(y, c);
                double * __restrict__ imaginary_ptr = imaginary.get(y, c);
                for (unsigned int x = 0; x < m_fft_width; ++x, ++src_ptr, ++real_ptr, ++imaginary_ptr)
                {
                    *real_ptr = (*src_ptr)[0];
                    *imaginary_ptr = (*src_ptr)[1];
                }
            }
        }
    }
    
    
    void ImageFFT::add(const ImageFFT &left, const ImageFFT &right)
    {
        operandGeometry(left, right);
        for (unsigned int c = 0; c < left.m_number_of_channels; ++c)
        {
            const fftw_complex * left_ptr = left.m_fft_image[c];
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * result_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr, ++result_ptr)
                {
                    (*result_ptr)[0] = (*left_ptr)[0] + (*right_ptr)[0];
                    (*result_ptr)[1] = (*left_ptr)[1] + (*right_ptr)[1];
                }
            }
        }
    }
    
    void ImageFFT::subtraction(const ImageFFT &left, const ImageFFT &right)
    {
        operandGeometry(left, right);
        for (unsigned int c = 0; c < left.m_number_of_channels; ++c)
        {
            const fftw_complex * __restrict__ left_ptr = left.m_fft_image[c];
            const fftw_complex * __restrict__ right_ptr = right.m_fft_image[c];
            fftw_complex * __restrict__ result_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr, ++result_ptr)
                {
                    (*result_ptr)[0] = (*left_ptr)[0] - (*right_ptr)[0];
                    (*result_ptr)[1] = (*left_ptr)[1] - (*right_ptr)[1];
                }
            }
        }
    }
    
    void ImageFFT::multiplication(const ImageFFT &left, const ImageFFT &right)
    {
        operandGeometry(left, right);
        for (unsigned int c = 0; c < left.m_number_of_channels; ++c)
        {
            const fftw_complex * left_ptr = left.m_fft_image[c];
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * result_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr, ++result_ptr)
                {
                    (*result_ptr)[0] = (*left_ptr)[0] * (*right_ptr)[0] - (*left_ptr)[1] * (*right_ptr)[1];
                    (*result_ptr)[1] = (*left_ptr)[0] * (*right_ptr)[1] + (*left_ptr)[1] * (*right_ptr)[0];
                }
            }
        }
    }
    
    void ImageFFT::division(const ImageFFT &left, const ImageFFT &right)
    {
        operandGeometry(left, right);
        for (unsigned int c = 0; c < left.m_number_of_channels; ++c)
        {
            const fftw_complex * left_ptr = left.m_fft_image[c];
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * result_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr, ++result_ptr)
                {
                    double denominator = (*right_ptr)[0] * (*right_ptr)[0] + (*right_ptr)[1] * (*right_ptr)[1];
                    (*result_ptr)[0] = ((*left_ptr)[0] * (*right_ptr)[0] + (*left_ptr)[1] * (*right_ptr)[1]) / denominator;
                    (*result_ptr)[1] = ((*left_ptr)[1] * (*right_ptr)[0] - (*left_ptr)[0] * (*right_ptr)[1]) / denominator;
                }
            }
        }
    }
    
    void ImageFFT::add(const ImageFFT &right)
    {
        if ((m_width != right.m_width) || (m_height != right.m_height) || (m_number_of_channels != right.m_number_of_channels))
            throw Exception("The current image (%dx%dx%d) and right image (%dx%dx%d) must have the same geometry.", m_width, m_height, m_number_of_channels, right.m_width, right.m_height, right.m_number_of_channels);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * left_ptr = m_fft_image[c];
            
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr)
                {
                    (*left_ptr)[0] += (*right_ptr)[0];
                    (*left_ptr)[1] += (*right_ptr)[1];
                }
            }
        }
    }
    
    void ImageFFT::subtraction(const ImageFFT &right)
    {
        if ((m_width != right.m_width) || (m_height != right.m_height) || (m_number_of_channels != right.m_number_of_channels))
            throw Exception("The current image (%dx%dx%d) and right image (%dx%dx%d) must have the same geometry.", m_width, m_height, m_number_of_channels, right.m_width, right.m_height, right.m_number_of_channels);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * left_ptr = m_fft_image[c];
            
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr)
                {
                    (*left_ptr)[0] -= (*right_ptr)[0];
                    (*left_ptr)[1] -= (*right_ptr)[1];
                }
            }
        }
    }
    
    void ImageFFT::multiplication(const ImageFFT &right)
    {
        if ((m_width != right.m_width) || (m_height != right.m_height) || (m_number_of_channels != right.m_number_of_channels))
            throw Exception("The current image (%dx%dx%d) and right image (%dx%dx%d) must have the same geometry.", m_width, m_height, m_number_of_channels, right.m_width, right.m_height, right.m_number_of_channels);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * left_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr)
                {
                    const double real = (*left_ptr)[0];
                    const double imaginary = (*left_ptr)[1];
                    (*left_ptr)[0] = real * (*right_ptr)[0] - imaginary * (*right_ptr)[1];
                    (*left_ptr)[1] = real * (*right_ptr)[1] + imaginary * (*right_ptr)[0];
                }
            }
        }
    }
    
    void ImageFFT::division(const ImageFFT &right)
    {
        if ((m_width != right.m_width) || (m_height != right.m_height) || (m_number_of_channels != right.m_number_of_channels))
            throw Exception("The current image (%dx%dx%d) and right image (%dx%dx%d) must have the same geometry.", m_width, m_height, m_number_of_channels, right.m_width, right.m_height, right.m_number_of_channels);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            const fftw_complex * right_ptr = right.m_fft_image[c];
            fftw_complex * left_ptr = m_fft_image[c];
            for (unsigned int y = 0; y < m_height; ++y)
            {
                for (unsigned int x = 0; x < m_fft_width; ++x, ++left_ptr, ++right_ptr)
                {
                    const double denominator = (*right_ptr)[0] * (*right_ptr)[0] + (*right_ptr)[1] * (*right_ptr)[1];
                    const double real = (*left_ptr)[0];
                    const double imaginary = (*left_ptr)[1];
                    
                    (*left_ptr)[0] = (real      * (*right_ptr)[0] + imaginary * (*right_ptr)[1]) / denominator;
                    (*left_ptr)[1] = (imaginary * (*right_ptr)[0] - real      * (*right_ptr)[1]) / denominator;
                }
            }
        }
    }
    
    void ImageFFT::operandGeometry(const ImageFFT &left, const ImageFFT &right)
    {
        if ((left.m_width != right.m_width) || (left.m_height != right.m_height) || (left.m_number_of_channels != right.m_number_of_channels))
            throw Exception("Left image (%dx%dx%d) and right image (%dx%dx%d) must have the same geometry.", left.m_width, left.m_height, left.m_number_of_channels, right.m_width, right.m_height, right.m_number_of_channels);
        if ((m_width != left.m_width) || (m_height != left.m_height) || (m_number_of_channels != left.m_number_of_channels))
        {
            // Free ...................................................................................................
            if (m_number_of_channels > 0)
            {
                for (unsigned int i = 0; i < m_number_of_channels; ++i)
                    fftw_free(m_fft_image[i]);
                delete [] m_fft_image;
            }
            
            // Allocate ...............................................................................................
            m_width = left.m_width;
            m_fft_width = left.m_fft_width;
            m_height = left.m_height;
            m_number_of_channels = left.m_number_of_channels;
            if (left.m_number_of_channels > 0)
            {
                m_fft_image = new fftw_complex * [left.m_number_of_channels];
                for (unsigned int i = 0; i < left.m_number_of_channels; ++i)
                    m_fft_image[i] = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * left.m_fft_width * left.m_height);
            }
            else m_fft_image = 0;
        }
    }
#endif
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE GAUSSIAN FILTERING             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    const double sqrt2 = 1.414213562;
    
    Gaussian2DFilterKernel::Gaussian2DFilterKernel(void) :
        m_sigma(0),
        m_horizontal(0),
        m_vertical(0),
        m_number_of_values(0),
        m_degree_x(0),
        m_degree_y(0),
        m_rotate_45(false) {}
    
    Gaussian2DFilterKernel::Gaussian2DFilterKernel(double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45) :
        m_sigma((rotate_45)?sigma / sqrt2:sigma),
        m_horizontal(0),
        m_vertical(0),
        m_number_of_values(0),
        m_degree_x(derivative_x),
        m_degree_y(derivative_y),
        m_rotate_45(rotate_45)
    {
        if (rotate_45) sigma /= sqrt2;
        if (sigma > 0)
        {
            double horizontal_norm, vertical_norm, sigma2, sigma4;
            int number_of_values;
            
            sigma2 = sigma * sigma;
            sigma4 = sigma2 * sigma2;
            switch (srvMax(derivative_x, derivative_y))
            {
            case 0:
                number_of_values = (int)ceil(sigma) * 3;
                break;
            case 1:
                number_of_values = (int)ceil(sigma) * 5;
                break;
            case 2:
                number_of_values = (int)ceil(sigma) * 7;
                break;
            default:
                throw Exception("Incorrect Gaussian derivative index. Possible values are 0, 1 and 2.");
            }
            
            m_number_of_values = number_of_values * 2 + 1;
            m_horizontal = new double[m_number_of_values];
            m_vertical = new double[m_number_of_values];
            horizontal_norm = vertical_norm = 0;
            for (int i = -number_of_values, j = 0; i <= number_of_values; ++i, ++j)
            {
                double gaussian;
                
                gaussian = exp(-((double)(i * i)) / (2.0 * sigma2));
                
                if (derivative_x == 0) m_horizontal[j] = gaussian;
                else if (derivative_x == 1) m_horizontal[j] = -(double)i * gaussian / sigma2;
                else m_horizontal[j] = ((sigma2 - (double)(i * i)) / sigma4) * gaussian;
                
                if (derivative_y == 0) m_vertical[j] = gaussian;
                else if (derivative_y == 1) m_vertical[j] = -(double)i * gaussian / sigma2;
                else m_vertical[j] = ((sigma2 - (double)(i * i)) / sigma4) * gaussian;
                
                horizontal_norm += srvAbs(m_horizontal[j]);
                vertical_norm += srvAbs(m_vertical[j]);
            }
            
            for (unsigned int i = 0; i < m_number_of_values; ++i)
            {
                m_horizontal[i] /= horizontal_norm;
                m_vertical[i] /= vertical_norm;
            }
        }
    }
    
    Gaussian2DFilterKernel::Gaussian2DFilterKernel(const Gaussian2DFilterKernel &other) :
        m_sigma(other.m_sigma),
        m_horizontal((other.m_horizontal != 0)?new double[other.m_number_of_values]:0),
        m_vertical((other.m_vertical != 0)?new double[other.m_number_of_values]:0),
        m_number_of_values(other.m_number_of_values),
        m_degree_x(other.m_degree_x),
        m_degree_y(other.m_degree_y),
        m_rotate_45(other.m_rotate_45)
    {
        for (unsigned int i = 0; i < other.m_number_of_values; ++i)
        {
            m_horizontal[i] = other.m_horizontal[i];
            m_vertical[i] = other.m_vertical[i];
        }
    }
    
    Gaussian2DFilterKernel::~Gaussian2DFilterKernel(void)
    {
        if (m_horizontal != 0) delete [] m_horizontal;
        if (m_vertical != 0) delete [] m_vertical;
    }
    
    Gaussian2DFilterKernel& Gaussian2DFilterKernel::operator=(const Gaussian2DFilterKernel &other)
    {
        if (this != &other)
        {
            if (m_horizontal != 0) delete [] m_horizontal;
            if (m_vertical != 0) delete [] m_vertical;
            
            m_sigma = other.m_sigma;
            if (other.m_number_of_values != 0)
            {
                m_horizontal = new double[other.m_number_of_values];
                m_vertical = new double[other.m_number_of_values];
                m_number_of_values = other.m_number_of_values;
                for (unsigned int i = 0; i < other.m_number_of_values; ++i)
                {
                    m_horizontal[i] = other.m_horizontal[i];
                    m_vertical[i] = other.m_vertical[i];
                }
            }
            else
            {
                m_horizontal = 0;
                m_vertical = 0;
                m_number_of_values = 0;
            }
            m_degree_x = other.m_degree_x;
            m_degree_y = other.m_degree_y;
            m_rotate_45 = other.m_rotate_45;
        }
        return *this;
    }
    
    void Gaussian2DFilterKernel::set(double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45)
    {
        if (m_horizontal != 0) delete [] m_horizontal;
        if (m_vertical != 0) delete [] m_vertical;
        
        if (rotate_45) sigma /= sqrt2;
        m_sigma = sigma;
        m_degree_x = derivative_x;
        m_degree_y = derivative_y;
        m_rotate_45 = rotate_45;
        
        if (sigma > 0)
        {
            double horizontal_norm, vertical_norm, sigma2, sigma4;
            int number_of_values;
            
            sigma2 = sigma * sigma;
            sigma4 = sigma2 * sigma2;
            switch (srvMax(derivative_x, derivative_y))
            {
            case 0:
                number_of_values = (int)ceil(sigma) * 3;
                break;
            case 1:
                number_of_values = (int)ceil(sigma) * 5;
                break;
            case 2:
                number_of_values = (int)ceil(sigma) * 7;
                break;
            default:
                throw Exception("Incorrect Gaussian derivative index. Possible values are 0, 1 and 2.");
            }
            
            m_number_of_values = number_of_values * 2 + 1;
            m_horizontal = new double[m_number_of_values];
            m_vertical = new double[m_number_of_values];
            horizontal_norm = vertical_norm = 0;
            for (int i = -number_of_values, j = 0; i <= number_of_values; ++i, ++j)
            {
                double gaussian;
                
                gaussian = exp(-((double)(i * i)) / (2.0 * sigma2));
                
                if (derivative_x == 0) m_horizontal[j] = gaussian;
                else if (derivative_x == 1) m_horizontal[j] = -(double)i * gaussian / sigma2;
                else m_horizontal[j] = ((sigma2 - (double)(i * i)) / sigma4) * gaussian;
                
                if (derivative_y == 0) m_vertical[j] = gaussian;
                else if (derivative_y == 1) m_vertical[j] = -(double)i * gaussian / sigma2;
                else m_vertical[j] = ((sigma2 - (double)(i * i)) / sigma4) * gaussian;
                
                horizontal_norm += srvAbs(m_horizontal[j]);
                vertical_norm += srvAbs(m_vertical[j]);
            }
            
            for (unsigned int i = 0; i < m_number_of_values; ++i)
            {
                m_horizontal[i] /= horizontal_norm;
                m_vertical[i] /= vertical_norm;
            }
        }
        else
        {
            m_horizontal = 0;
            m_vertical = 0;
            m_number_of_values = 0;
        }
    }
    
    void Gaussian2DFilterKernel::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Gaussian_2D_Filter");
        parser.setAttribute("Sigma", m_sigma);
        parser.setAttribute("Number_of_Values", m_number_of_values);
        parser.setAttribute("Degree_X", m_degree_x);
        parser.setAttribute("Degree_Y", m_degree_y);
        parser.setAttribute("Rotate", m_rotate_45);
        parser.addChildren();
        if (m_number_of_values > 0)
        {
            parser.openTag("Horizontal");
            parser.addChildren();
            writeToBase64Array(parser, m_horizontal, m_number_of_values);
            parser.closeTag();
            parser.openTag("Vertical");
            parser.addChildren();
            writeToBase64Array(parser, m_vertical, m_number_of_values);
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    void Gaussian2DFilterKernel::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Gaussian_2D_Filter"))
        {
            if (m_horizontal != 0) delete [] m_horizontal;
            if (m_vertical != 0) delete [] m_vertical;
            
            m_sigma = parser.getAttribute("Sigma");
            m_number_of_values = parser.getAttribute("Number_of_Values");
            m_degree_x = parser.getAttribute("Degree_X");
            m_degree_y = parser.getAttribute("Degree_Y");
            m_rotate_45 = parser.getAttribute("Rotate");
            
            if (m_number_of_values != 0)
            {
                m_horizontal = new double[m_number_of_values];
                m_vertical = new double[m_number_of_values];
            }
            else
            {
                m_horizontal = 0;
                m_vertical = 0;
            }
            
            while (!(parser.isTagIdentifier("Gaussian_2D_Filter") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Horizontal"))
                {
                    while (!(parser.isTagIdentifier("Horizontal") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Horizontal", m_horizontal, m_number_of_values);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Vertical"))
                {
                    while (!(parser.isTagIdentifier("Vertical") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Vertical", m_vertical, m_number_of_values);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE GAUSSIAN FILTERING             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    Gaussian2DFilterRecursiveKernel::Gaussian2DFilterRecursiveKernel(void) :
        m_method(VLIET_3RD),
        m_derivative_x(0),
        m_derivative_y(0),
        m_q_x(0.0),
        m_q_y(0.0),
        m_weight_x(1.0),
        m_weight_y(1.0),
        m_sigma(0.0),
        m_coefficients_x(0),
        m_coefficients_y(0),
        m_number_of_coefficients(0),
        m_rotate_45(false)
    {
    }
    
    Gaussian2DFilterRecursiveKernel::Gaussian2DFilterRecursiveKernel(RECURSIVE_GAUSSIAN_METHOD method, double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45) :
        m_method(method),
        m_derivative_x(derivative_x),
        m_derivative_y(derivative_y),
        m_sigma((rotate_45)?sigma / sqrt2:sigma),
        m_rotate_45(rotate_45)
    {
        if (method == DERICHE)
        {
            m_number_of_coefficients = 12;
            if (rotate_45)
            {
                m_coefficients_x = setDericheCoefficients(derivative_x, sigma / sqrt2, m_q_x, m_weight_x);
                m_coefficients_y = setDericheCoefficients(derivative_y, sigma / sqrt2, m_q_y, m_weight_y);
            }
            else
            {
                m_coefficients_x = setDericheCoefficients(derivative_x, sigma, m_q_x, m_weight_x);
                m_coefficients_y = setDericheCoefficients(derivative_y, sigma, m_q_y, m_weight_y);
            }
        }
        else
        {
            if (method == VLIET_3RD) m_number_of_coefficients = 4;
            else if (method == VLIET_4TH) m_number_of_coefficients = 5;
            else if (method == VLIET_5TH) m_number_of_coefficients = 6;
            if (rotate_45)
            {
                m_coefficients_x = setVlietYoungVerbeekCoefficients(derivative_x, sigma / sqrt2, m_number_of_coefficients - 1, m_q_x, m_weight_x);
                m_coefficients_y = setVlietYoungVerbeekCoefficients(derivative_y, sigma / sqrt2, m_number_of_coefficients - 1, m_q_y, m_weight_y);
            }
            else
            {
                m_coefficients_x = setVlietYoungVerbeekCoefficients(derivative_x, sigma, m_number_of_coefficients - 1, m_q_x, m_weight_x);
                m_coefficients_y = setVlietYoungVerbeekCoefficients(derivative_y, sigma, m_number_of_coefficients - 1, m_q_y, m_weight_y);
            }
        }
    }
    
    Gaussian2DFilterRecursiveKernel::Gaussian2DFilterRecursiveKernel(const Gaussian2DFilterRecursiveKernel &other) :
        m_method(other.m_method),
        m_derivative_x(other.m_derivative_x),
        m_derivative_y(other.m_derivative_y),
        m_q_x(other.m_q_x),
        m_q_y(other.m_q_y),
        m_weight_x(other.m_weight_x),
        m_weight_y(other.m_weight_y),
        m_sigma(other.m_sigma),
        m_coefficients_x((other.m_coefficients_x != 0)?new double[other.m_number_of_coefficients]:0),
        m_coefficients_y((other.m_coefficients_y != 0)?new double[other.m_number_of_coefficients]:0),
        m_number_of_coefficients(other.m_number_of_coefficients),
        m_rotate_45(other.m_rotate_45)
    {
        for (unsigned int i = 0; i < other.m_number_of_coefficients; ++i)
        {
            m_coefficients_x[i] = other.m_coefficients_x[i];
            m_coefficients_y[i] = other.m_coefficients_y[i];
        }
    }
    
    Gaussian2DFilterRecursiveKernel::~Gaussian2DFilterRecursiveKernel(void)
    {
        if (m_coefficients_x != 0) delete [] m_coefficients_x;
        if (m_coefficients_y != 0) delete [] m_coefficients_y;
    }
    
    Gaussian2DFilterRecursiveKernel& Gaussian2DFilterRecursiveKernel::operator=(const Gaussian2DFilterRecursiveKernel &other)
    {
        if (this != &other)
        {
            // Free .................................................................................................................................
            if (m_coefficients_x != 0) delete [] m_coefficients_x;
            if (m_coefficients_y != 0) delete [] m_coefficients_y;
            
            // Copy .................................................................................................................................
            m_method = other.m_method;
            m_derivative_x = other.m_derivative_x;
            m_derivative_y = other.m_derivative_y;
            m_q_x = other.m_q_x;
            m_q_y = other.m_q_y;
            m_weight_x = other.m_weight_x;
            m_weight_y = other.m_weight_y;
            m_sigma = other.m_sigma;
            m_number_of_coefficients = other.m_number_of_coefficients;
            m_rotate_45 = other.m_rotate_45;
            if (other.m_coefficients_x != 0)
            {
                m_coefficients_x = new double[other.m_number_of_coefficients];
                for (unsigned int i = 0; i < other.m_number_of_coefficients; ++i)
                    m_coefficients_x[i] = other.m_coefficients_x[i];
            }
            else m_coefficients_x = 0;
            if (other.m_coefficients_y != 0)
            {
                m_coefficients_y = new double[other.m_number_of_coefficients];
                for (unsigned int i = 0; i < other.m_number_of_coefficients; ++i)
                    m_coefficients_y[i] = other.m_coefficients_y[i];
            }
            else m_coefficients_y = 0;
        }
        return *this;
    }
    
    void Gaussian2DFilterRecursiveKernel::set(RECURSIVE_GAUSSIAN_METHOD method, double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45)
    {
        // Free .....................................................................................................................................
        if (m_coefficients_x != 0) delete [] m_coefficients_x;
        if (m_coefficients_y != 0) delete [] m_coefficients_y;
        // Generate .................................................................................................................................
        m_method = method;
        m_derivative_x = derivative_x;
        m_derivative_y = derivative_y;
        m_sigma = sigma;
        m_rotate_45 = rotate_45;
        
        if (method == DERICHE)
        {
            m_number_of_coefficients = 12;
            if (rotate_45)
            {
                m_coefficients_x = setDericheCoefficients(derivative_x, sigma / sqrt2, m_q_x, m_weight_x);
                m_coefficients_y = setDericheCoefficients(derivative_y, sigma / sqrt2, m_q_y, m_weight_y);
            }
            else
            {
                m_coefficients_x = setDericheCoefficients(derivative_x, sigma, m_q_x, m_weight_x);
                m_coefficients_y = setDericheCoefficients(derivative_y, sigma, m_q_y, m_weight_y);
            }
        }
        else
        {
            if (method == VLIET_3RD) m_number_of_coefficients = 4;
            else if (method == VLIET_4TH) m_number_of_coefficients = 5;
            else if (method == VLIET_5TH) m_number_of_coefficients = 6;
            if (rotate_45)
            {
                m_coefficients_x = setVlietYoungVerbeekCoefficients(derivative_x, sigma / sqrt2, m_number_of_coefficients - 1, m_q_x, m_weight_x);
                m_coefficients_y = setVlietYoungVerbeekCoefficients(derivative_y, sigma / sqrt2, m_number_of_coefficients - 1, m_q_y, m_weight_y);
            }
            else
            {
                m_coefficients_x = setVlietYoungVerbeekCoefficients(derivative_x, sigma, m_number_of_coefficients - 1, m_q_x, m_weight_x);
                m_coefficients_y = setVlietYoungVerbeekCoefficients(derivative_y, sigma, m_number_of_coefficients - 1, m_q_y, m_weight_y);
            }
        }
    }
    
    void Gaussian2DFilterRecursiveKernel::convertToXML(XmlParser &parser) const
    {
        double attributes[5];
        
        parser.openTag("Recursive_Gaussian_2D_Filter");
        parser.setAttribute("Method", (int)m_method);
        parser.setAttribute("Derivative_X", m_derivative_x);
        parser.setAttribute("Derivative_Y", m_derivative_y);
        parser.setAttribute("Number_Of_Coefficients", m_number_of_coefficients);
        parser.setAttribute("Rotate", m_rotate_45);
        parser.addChildren();
        attributes[0] = m_q_x;
        attributes[1] = m_q_y;
        attributes[2] = m_weight_x;
        attributes[3] = m_weight_y;
        attributes[4] = m_sigma;
        parser.openTag("Double_Attributes");
        parser.addChildren();
        writeToBase64Array(parser, attributes, 5);
        parser.closeTag();
        if (m_coefficients_x != 0)
        {
            parser.openTag("Coefficients_X");
            parser.addChildren();
            writeToBase64Array(parser, m_coefficients_x, m_number_of_coefficients);
            parser.closeTag();
        }
        if (m_coefficients_y != 0)
        {
            parser.openTag("Coefficients_Y");
            parser.addChildren();
            writeToBase64Array(parser, m_coefficients_y, m_number_of_coefficients);
            parser.closeTag();
        }
        parser.closeTag();
    }
    
    void Gaussian2DFilterRecursiveKernel::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Recursive_Gaussian_2D_Filter"))
        {
            double attributes[5];
            
            // Free .................................................................................................................................
            if (m_coefficients_x != 0) { delete [] m_coefficients_x; m_coefficients_x = 0; }
            if (m_coefficients_y != 0) { delete [] m_coefficients_y; m_coefficients_y = 0; }
            
            // Retrieve information from the XML object .............................................................................................
            m_method = (RECURSIVE_GAUSSIAN_METHOD)((int)parser.getAttribute("Method"));;
            m_derivative_x = parser.getAttribute("Derivative_X");
            m_derivative_y = parser.getAttribute("Derivative_Y");
            m_number_of_coefficients = parser.getAttribute("Number_Of_Coefficients");
            m_rotate_45 = parser.getAttribute("Rotate");
            if (m_number_of_coefficients > 0)
            {
                m_coefficients_x = new double[m_number_of_coefficients];
                m_coefficients_y = new double[m_number_of_coefficients];
            }
            
            while (!(parser.isTagIdentifier("Recursive_Gaussian_2D_Filter") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Double_Attributes"))
                {
                    while (!(parser.isTagIdentifier("Double_Attributes") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Double_Attributes", attributes, 5);
                        else parser.getNext();
                    }
                    parser.getNext();
                    
                    m_q_x = attributes[0];
                    m_q_y = attributes[1];
                    m_weight_x = attributes[2];
                    m_weight_y = attributes[3];
                    m_sigma = attributes[4];
                }
                else if (parser.isTagIdentifier("Coefficients_X"))
                {
                    while (!(parser.isTagIdentifier("Coefficients_X") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Coefficients_X", m_coefficients_x, m_number_of_coefficients);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Coefficients_Y"))
                {
                    while (!(parser.isTagIdentifier("Coefficients_Y") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Coefficients_Y", m_coefficients_y, m_number_of_coefficients);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    double* Gaussian2DFilterRecursiveKernel::setDericheCoefficients(unsigned int derivative, double sigma, double &q, double &weight)
    {
        double y01, y02, y03, y04, x01, x02, x03, x04, *coefficients;
        unsigned int n, m;
        double *sx, *sy;
        
        // Constant coefficients of the Deriches algorithm for each Gaussian derivative.
        const double va0[] = {  1.6800, -0.6472, -1.3310};
        const double va1[] = {  3.7350, -4.5310,  3.6610};
        const double vb0[] = {  1.7830,  1.5270,  1.2400};
        const double vb1[] = {  1.7230,  1.5160,  1.3140};
        const double vc0[] = { -0.6803,  0.6494,  0.3225};
        const double vc1[] = { -0.2598,  0.9557, -1.7380};
        const double vw0[] = {  0.6318,  0.6719,  0.7480};
        const double vw1[] = {  1.9970,  2.0720,  2.1660};
        
        const double a0 = va0[derivative];
        const double a1 = va1[derivative];
        const double b0 = vb0[derivative];
        const double b1 = vb1[derivative];
        const double c0 = vc0[derivative];
        const double c1 = vc1[derivative];
        const double w0 = vw0[derivative];
        const double w1 = vw1[derivative];
        
        // 1) Calculate the Derice's coefficients.
        coefficients = new double[12];
        // From n0 to n3
        coefficients[0] = (derivative == 1)?0:(a0 + c0);
        coefficients[1] = exp(-b1 / sigma) * (c1 * sin(w1 / sigma) -(c0 + 2.0 * a0) * cos(w1 / sigma)) +exp(-b0 / sigma) * (a1 * sin(w0 / sigma) -(2.0 * c0 + a0) * cos(w0 / sigma));
        coefficients[2] = 2.0 * exp(-(b0 + b1) / sigma) * ((a0 + c0) * cos(w1 / sigma) * cos(w0 / sigma) - a1 * cos(w1 / sigma) * sin(w0 / sigma) - c1 * cos(w0 / sigma) * sin(w1 / sigma)) + c0 * exp(-2.0 * b0 / sigma) + a0 * exp(-2.0 * b1 / sigma);
        coefficients[3] = exp(-(b1 + 2.0 * b0) / sigma) * (c1 * sin(w1 / sigma) -c0 * cos(w1 / sigma)) + exp(-(b0 + 2.0 * b1) / sigma) * (a1 * sin(w0 / sigma) -a0 * cos(w0 / sigma));
        // From d1 to d4
        coefficients[4] = -2.0 * exp(-b0 / sigma) * cos(w0 / sigma) -2.0 * exp(-b1 / sigma) * cos(w1 / sigma);
        coefficients[5] = 4.0 * exp(-(b0 + b1) / sigma) * cos(w0 / sigma) * cos(w1 / sigma) + exp(-2.0 * b0 / sigma) + exp(-2.0 * b1 / sigma);
        coefficients[6] = -2.0 * exp(-(b0 + 2.0 * b1) / sigma) * cos(w0 / sigma) - 2.0 * exp(-(b1 + 2.0 * b0) / sigma) * cos(w1 / sigma);
        coefficients[7] = exp(-2.0 * (b0 + b1) / sigma);
        
        // 2) Scale the coefficients to fit the current sigma.
        n = 1 + 2 * (unsigned int)(10.0 * sigma);
        m = (n - 1) / 2;
        sx = new double[n];
        sy = new double[n];
        for (unsigned int i = 0; i < n; ++i) sx[i] = 0.0;
        sx[m] = 1.0;
        
        y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                   x03 = 0.0; x02 = 0.0; x01 = 0.0;
        for (unsigned int k = 0; k < n; ++k) // Forward filtering...
        {
            double xi = sx[k];
            double yi = coefficients[0] * xi + coefficients[1] * x01 + coefficients[2] * x02 + coefficients[3] * x03
                                             - coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6] * y03 - coefficients[7] * y04;
            sy[k] = yi;
            y04 = y03; y03 = y02; y02 = y01; y01 = yi;
                       x03 = x02; x02 = x01; x01 = xi;
        }
        
        if (derivative == 1) // For non-symmetric filters the negative coefficients are calculated differently.
        {
            coefficients[8]  = -(coefficients[1] - coefficients[4] * coefficients[0]);
            coefficients[9]  = -(coefficients[2] - coefficients[5] * coefficients[0]);
            coefficients[10] = -(coefficients[3] - coefficients[6] * coefficients[0]);
            coefficients[11] = coefficients[7] * coefficients[0];
        }
        else
        {
            coefficients[8]  = coefficients[1] - coefficients[4] * coefficients[0];
            coefficients[9]  = coefficients[2] - coefficients[5] * coefficients[0];
            coefficients[10] = coefficients[3] - coefficients[6] * coefficients[0];
            coefficients[11] = -coefficients[7] * coefficients[0];
        }
        
        y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
        x04 = 0.0; x03 = 0.0; x02 = 0.0; x01 = 0.0;
        for (int k = (int)(n - 1); k >= 0; --k) // Backward filtering...
        {
            double xi = sx[k];
            double yi = coefficients[8] * x01 + coefficients[9] * x02 + coefficients[10] * x03 + coefficients[11] * x04 -
                        coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6]  * y03 - coefficients[7]  * y04;
            sy[k] += yi;
            y04 = y03; y03 = y02; y02 = y01; y01 = yi;
            x04 = x03; x03 = x02; x02 = x01; x01 = xi;
        }
        
        q = 0;
        if (derivative == 0)
        {
            
            for (unsigned int i = 0, j = n - 1; i < m; ++i, --j)
                q += sy[i] + sy[j];
            q += sy[m];
        }
        else if (derivative == 1)
        {
            for (unsigned int i = 0, j = n - 1; i < m; ++i, --j)
            {
                double factor = sin((-(double)m + (double)i) / sigma);
                q += factor * (sy[i] - sy[j]);
            }
            q *= sigma * 1.6487212707001282; // exp(0.5) = 1.6487212707001282;
        }
        else if (derivative == 2)
        {
            for (unsigned int i = 0, j = n - 1; i < m; ++i, --j)
            {
                double factor = cos((-(double)m + (double)i) * 1.4142135623730951 / sigma); // sqrt(2.0) = 1.4142135623730951;
                q += factor * (sy[i] + sy[j]);
            }
            q = (q + sy[m]) * (-(sigma * sigma) / 2.0) * 2.718281828459045; // exp(1.0) = 2.718281828459045;
        }
        
        q = srvAbs(q);
        coefficients[0] /= q;
        coefficients[1] /= q;
        coefficients[2] /= q;
        coefficients[3] /= q;
        
        if (derivative == 1) // For non-symmetric filters the negative coefficients are calculated differently.
        {
            coefficients[8]  = -(coefficients[1] - coefficients[4] * coefficients[0]);
            coefficients[9]  = -(coefficients[2] - coefficients[5] * coefficients[0]);
            coefficients[10] = -(coefficients[3] - coefficients[6] * coefficients[0]);
            coefficients[11] = coefficients[7] * coefficients[0];
        }
        else
        {
            coefficients[8]  = coefficients[1] - coefficients[4] * coefficients[0];
            coefficients[9]  = coefficients[2] - coefficients[5] * coefficients[0];
            coefficients[10] = coefficients[3] - coefficients[6] * coefficients[0];
            coefficients[11] = -coefficients[7] * coefficients[0];
        }
        
        // 3) Calculate the weight of the Gaussian filter.
        y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                   x03 = 0.0; x02 = 0.0; x01 = 0.0;
        for (unsigned int k = 0; k < n; ++k) // Forward filtering...
        {
            double xi = sx[k];
            double yi = coefficients[0] * xi + coefficients[1] * x01 + coefficients[2] * x02 + coefficients[3] * x03
                                             - coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6] * y03 - coefficients[7] * y04;
            sy[k] = yi;
            y04 = y03; y03 = y02; y02 = y01; y01 = yi;
                       x03 = x02; x02 = x01; x01 = xi;
        }
        
        y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
        x04 = 0.0; x03 = 0.0; x02 = 0.0; x01 = 0.0;
        for (int k = (int)(n - 1); k >= 0; --k) // Backward filtering...
        {
            double xi = sx[k];
            double yi = coefficients[8] * x01 + coefficients[9] * x02 + coefficients[10] * x03 + coefficients[11] * x04 -
                        coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6]  * y03 - coefficients[7]  * y04;
            sy[k] += yi;
            y04 = y03; y03 = y02; y02 = y01; y01 = yi;
            x04 = x03; x03 = x02; x02 = x01; x01 = xi;
        }
        weight = 0;
        for (unsigned int i = 0; i < n; ++i) weight += fabs(sy[i]);
        
        // 4) Free allocated memory.
        delete [] sx;
        delete [] sy;
        
        return coefficients;
    }
    
    double* Gaussian2DFilterRecursiveKernel::setVlietYoungVerbeekCoefficients(unsigned int derivative, double sigma, unsigned int order, double &q, double &weight)
    {
        std::complex<double> *d, prod, *previous_values, *values;
        double *coefficients, *module, *angle, b_const;
        unsigned int n, middle;
        double *sx, *sy, *queue;
        
        // 1) Set the poles of the recursive filter.
        d = new std::complex<double>[order];
        if (order == 3)
        {
            if (derivative == 0) d[0] = std::complex<double>(1.41650, 1.00829);
            else if (derivative == 1) d[0] = std::complex<double>(1.31553, 0.97057);
            else if (derivative == 2) d[0] = std::complex<double>(1.22886, 0.93058);
            d[1] = std::conj(d[0]);
            if (derivative == 0) d[2] = std::complex<double>(1.86543, 0.0);
            else if (derivative == 1) d[2] = std::complex<double>(1.77635, 0.0);
            else if (derivative == 2) d[2] = std::complex<double>(1.70493, 0.0);
        }
        else if (order == 4)
        {
            if (derivative == 0) d[0] = std::complex<double>(1.13228, 1.28114);
            else if (derivative == 1) d[0] = std::complex<double>(1.04185, 1.24034);
            else if (derivative == 2) d[0] = std::complex<double>(0.94570, 1.21064);
            d[1] = std::conj(d[0]);
            if (derivative == 0) d[2] = std::complex<double>(1.78534, 0.46763);
            else if (derivative == 1) d[2] = std::complex<double>(1.69747, 0.44790);
            else if (derivative == 2) d[2] = std::complex<double>(1.60161, 0.42647);
            d[3] = std::conj(d[2]);
        }
        else if (order == 5)
        {
            if (derivative == 0) d[0] = std::complex<double>(0.86430, 1.45389);
            else if (derivative == 1) d[0] = std::complex<double>(0.77934, 1.41423);
            else if (derivative == 2) d[0] = std::complex<double>(0.69843, 1.37655);
            d[1] = std::conj(d[0]);
            if (derivative == 0) d[2] = std::complex<double>(1.61433, 0.83134);
            else if (derivative == 1) d[2] = std::complex<double>(1.50941, 0.80828);
            else if (derivative == 2) d[2] = std::complex<double>(1.42631, 0.77399);
            d[3] = std::conj(d[2]);
            if (derivative == 0) d[4] = std::complex<double>(1.87504, 0.0);
            else if (derivative == 1) d[4] = std::complex<double>(1.77181, 0.0);
            else if (derivative == 2) d[4] = std::complex<double>(1.69668, 0.0);
        }
        else throw Exception("The specified order of the recursive filter is not implemented");
        
        coefficients = new double[order + 1];
        module = new double[order];
        angle = new double[order];
        for (unsigned int i = 0; i < order; ++i)
        {
            module[i] = std::abs(d[i]);
            angle[i] = std::arg(d[i]);
        }
        
        // 2) Rescale the poles to fit the selected sigma.
        q = 1.0;
        for (unsigned int iteration = 0; iteration < 1000; ++iteration)
        {
            double variance, current_module, current_angle;
            std::complex<double> complex_variance;
            
            complex_variance = std::complex<double>(0.0, 0.0);
            for (unsigned int i = 0; i < order; ++i)
            {
                current_module = pow(module[i], 1.0 / q);
                current_angle = angle[i] / q;
                complex_variance += 2.0 * std::polar(current_module, current_angle) * pow(current_module - std::polar(1.0, current_angle), -2.0);
            }
            variance = sqrt(std::real(complex_variance));
            
            q = q * sigma / variance;
            if (fabs(sigma - variance) < sigma * 1.0e-10) break;
        }
        
        for (unsigned int i = 0; i < order; ++i)
            d[i] = std::polar(pow(module[i], 1.0 / q), angle[i] / q);
        
        // 3) Calculate the coefficients of the rescaled poles.
        prod = d[0];
        for (unsigned int i = 1; i < order; ++i) prod *= d[i];
        b_const = 1.0 / std::real(prod);
        
        previous_values = new std::complex<double>[order];
        values = new std::complex<double>[order];
        for (unsigned int i = 0; i < order; ++i) { previous_values[i] = 1.0; values[i] = 0.0; }
        values[0] = 1.0;
        
        for (int index = (int)order - 1, k = 0; index >= 0; --index, ++k)
        {
            prod = values[0];
            for (unsigned int j = 1; j < order; ++j) prod += values[j];
            coefficients[index] = ((index % 2 == 1)?1.0:-1.0) * b_const * std::real(prod);
            //printf("--> %20.15f\n", coefficients[index]);
            for (unsigned int j = 0; j < order; ++j)
            {
                values[j] = d[j] * previous_values[j];
                //printf("%20.15f + i %20.15f\n", std::real(previous_values[j]), std::imag(previous_values[j]));
            }
            //printf("\n\n");
            
            previous_values[k] = 0;
            for (unsigned int j = k + 1; j < order; ++j)
            {
                prod = values[0];
                for (int m = 1; m < (int)j; ++m) prod += values[m];
                previous_values[j] = prod;
                //std::cout << j << "  " << previous_values[j] << std::endl;
            }
        }
        
        // 4) Set the alpha value of the recursive filter in the last coefficient.
        coefficients[order] = 1.0;
        for (unsigned int i = 0; i < order; ++i) coefficients[order] += coefficients[i]; // alpha = 1.0 + sum(b);
        
        // 5) Calculate the weight of the Gaussian filter.
        n = 1 + 2 * (unsigned int)(10.0 * sigma);
        middle = (n - 1) / 2;
        sx = new double[n];
        sy = new double[n];
        for (unsigned int i = 0; i < n; ++i) sx[i] = 0.0;
        sx[middle] = 1.0;
        queue = new double[order];
        
        for (unsigned int i = 0; i < order; ++i) queue[i] = 0.0;
        if (derivative == 0)
        {
            for (unsigned int k = 0, m = 0; k < n; ++k, ++m) // Forward filtering...
            {
                sy[k] = coefficients[order] * sx[k];
                for (unsigned int i = 0; i < order; ++i) sy[k] -= coefficients[i] * queue[i];
                for (unsigned int i = order - 1; i > 0; --i) queue[i] = queue[i - 1];
                queue[0] = sy[k];
            }
        }
        else if (derivative == 1)
        {
            sy[0] = 0.0;
            for (unsigned int k = 1, m = 0; k < n - 1; ++k, ++m) // Forward filtering...
            {
                sy[k] = coefficients[order] * (sx[k + 1] - sx[k - 1]) / 2.0;
                for (unsigned int i = 0; i < order; ++i) sy[k] -= coefficients[i] * queue[i];
                for (unsigned int i = order - 1; i > 0; --i) queue[i] = queue[i - 1];
                queue[0] = sy[k];
            }
            sy[n - 1] = 0.0;
        }
        else // Derivative == 2
        {
            sy[0] = 0.0;
            for (unsigned int k = 1, m = 0; k < n; ++k, ++m) // Forward filtering...
            {
                sy[k] = coefficients[order] * (sx[k] - sx[k - 1]);
                for (unsigned int i = 0; i < order; ++i) sy[k] -= coefficients[i] * queue[i];
                for (unsigned int i = order - 1; i > 0; --i) queue[i] = queue[i - 1];
                queue[0] = sy[k];
            }
        }
        
        for (unsigned int i = 0; i < order; ++i) queue[i] = 0.0;
        if (derivative == 2)
        {
            double previous = 0.0, aux;
            for (int k = (int)(n - 1), m = 0; k >= 0; --k, ++m) // Backward filtering...
            {
                aux = sy[k];
                sy[k] = coefficients[order] * (previous - sy[k]);
                previous = aux;
                for (unsigned int i = 0; i < order; ++i) sy[k] -= coefficients[i] * queue[i];
                for (unsigned int i = order - 1; i > 0; --i) queue[i] = queue[i - 1];
                queue[0] = sy[k];
            }
        }
        else // derivative == 0 OR 1.
        {
            for (int k = (int)(n - 1), m = 0; k >= 0; --k, ++m) // Backward filtering...
            {
                sy[k] = coefficients[order] * sy[k];
                for (unsigned int i = 0; i < order; ++i) sy[k] -= coefficients[i] * queue[i];
                for (unsigned int i = order - 1; i > 0; --i) queue[i] = queue[i - 1];
                queue[0] = sy[k];
            }
        }
        weight = 0;
        for (unsigned int i = 0; i < n; ++i) weight += fabs(sy[i]);
        
        // 6) Free allocated memory.
        delete [] d;
        delete [] module;
        delete [] angle;
        delete [] previous_values;
        delete [] values;
        delete [] queue;
        delete [] sx;
        delete [] sy;
        
        return coefficients;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}
