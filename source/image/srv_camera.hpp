// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CAMERA_HEADER_FILE__
#define __SRV_CAMERA_HEADER_FILE__

#include <iostream>
#include "srv_image.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
    /// Pure virtual class for the camera wrappers of the SRV library.
    class Camera
    {
    public:
        /// Virtual destructor.
        virtual ~Camera(void) {};
        /** Retrieves a frame of the camera and stores it into the internal memory.
         *  \param[in] time maximum time waited to retrieve a new frame from the camera in milliseconds.
         *  \param[in] sleep wait time in microseconds between consecutive image retrieval trials.
         */
        virtual void retrieveFrame(unsigned int wait, unsigned int sleep) = 0;
        /** Copies the internal image buffer into the given image.
         *  \param[out] image destination image where the camera image is stored.
         */
        virtual void copyImage(Image<unsigned char> &image) const = 0;
    };
    
    /** Implements an interface to retrieve camera images by means of the Video for Linux drivers.
     */
    class CameraLinux : public Camera
    {
        static const unsigned int CAMERA_FILENAME_LENGTH = 1024;
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /** Default constructor which initializes the camera wrapper.
         *  \param[in] camera_file route to the camera stream file (/dev/video0, ...).
         *  \param[in] width width of the captured images.
         *  \param[in] height height of the captured images.
         *  \param[in] frames_per_second number of frames captured per second.
         *  \param[in] image_format format of the images retrieved from the driver.
         *  \param[in] verbose show error messages into the standard error output.
         */
        CameraLinux(const char * camera_filename, unsigned int width, unsigned int height, unsigned int frames_per_second, CAMERA_IMAGE_FORMAT image_format, bool verbose);
        /// Destructor.
        ~CameraLinux(void);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the string array with the route to the file of the camera stream.
        inline const char * getCameraFilename(void) const { return m_camera_filename; }
        /// Returns the width of the camera image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the camera image.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the frames per second retrieved by the camera.
        inline unsigned int getFramesPerSecond(void) const { return m_frames_per_second; }
        /// Returns the format of the image retrieved from the camera driver.
        inline CAMERA_IMAGE_FORMAT getImageFormat(void) const { return m_image_format; }
        /// Returns the value of the verbose flag which enables or disables to show error meassages in the standard error output.
        inline bool getVerbose(void) const { return m_verbose; }
        /// Sets the value of the verbose flag which enables or disables to show error meassages in the standard error output.
        inline void setVerbose(bool verbose) { m_verbose = verbose; }
        
        /// Returns the minimum brightness value of the camera.
        inline int getMinimumBrightness(void) const { return m_brightness_minimum; }
        /// Returns the maximum brightness value of the camera.
        inline int getMaximumBrightness(void) const { return m_brightness_maximum; }
        /// Returns the brightness value of the camera.
        inline int getBrightness(void) const { return m_brightness_value; }
        /// Sets the brightness value of the camera.
        void setBrightness(int value);
        /// Return true when brightness control of the camera is enabled.
        inline bool isBrightnessEnabled(void) const { return m_brightness_enabled; }
        /// Returns the minimum contrast value of the camera.
        inline int getMinimumContrast(void) const { return m_contrast_minimum; }
        /// Returns the maximum contrast value of the camera.
        inline int getMaximumContrast(void) const { return m_contrast_maximum; }
        /// Returns the contrast value of the camera.
        inline int getContrast(void) const { return m_contrast_value; }
        /// Sets the contrast value of the camera.
        void setContrast(int value);
        /// Returns true when the contrast control of the camera is enabled.
        inline bool isContrastEnabled(void) const { return m_contrast_enabled; }
        /// Returns the minimum saturation value of the camera.
        inline int getMinimumSaturation(void) const { return m_saturation_minimum; }
        /// Returns the maximum saturation value of the camera.
        inline int getMaximumSaturation(void) const { return m_saturation_maximum; }
        /// Returns the saturation value of the camera.
        inline int getSaturation(void) const { return m_saturation_value; }
        /// Sets the saturation value of the camera.
        void setSaturation(int value);
        /// Returns true when the saturation control of the camera is enabled.
        inline bool isSaturationEnabled(void) const { return m_saturation_enabled; }
        /// Returns the minimum hue value of the camera.
        inline int getMinimumHue(void) const { return m_hue_minimum; }
        /// Returns the maximum hue value of the camera.
        inline int getMaximumHue(void) const { return m_hue_maximum; }
        /// Returns the hue value of the camera.
        inline int getHue(void) const { return m_hue_value; }
        /// Sets the hue value of the camera.
        void setHue(int value);
        /// Returns true when the hue control of the camera is enabled.
        inline bool isHueEnabled(void) const { return m_hue_enabled; }
        /// Returns true when the hue of the camera is automatically set.
        inline bool isAutomaticHue(void) const { return m_hue_auto; }
        /// Sets the automatic hue flag of the camera.
        void setAutomaticHue(bool value);
        /// Returns true when the hue control is automatically set.
        inline bool isAutomaticHueEnabled(void) const { return m_hue_auto_enabled; }
        /// Returns the minimum sharpness value of the camera.
        inline int getMinimumSharpness(void) const { return m_sharpness_minimum; }
        /// Returns the maximum sharpness value of the camera.
        inline int getMaximumSharpness(void) const { return m_sharpness_maximum; }
        /// Returns the sharpness value of the camera.
        inline int getSharpness(void) const { return m_sharpness_value; }
        /// Sets the sharpness value of the camera.
        void setSharpness(int value);
        /// Returns true when the sharpness control of the camera is enabled.
        inline bool isSharpnessEnabled(void) const { return m_sharpness_enabled; }
        
        // -[ Camera images ]------------------------------------------------------------------------------------------------------------------------
        /** Retrieves a frame of the camera and stores it into the internal memory.
         *  \param[in] time maximum time waited to retrieve a new frame from the camera in milliseconds.
         *  \param[in] sleep wait time in microseconds between consecutive image retrieval trials.
         */
        void retrieveFrame(unsigned int wait, unsigned int sleep);
        /** Copies the internal image buffer into the given image.
         *  \param[out] image destination image where the camera image is stored.
         */
        void copyImage(Image<unsigned char> &image) const;
        
        // -[ Camera information ]-------------------------------------------------------------------------------------------------------------------
        void information(std::ostream &out) const;
        
    protected:
        /// Buffer structure to the camera image.
        typedef struct
        {
            void * start;
            size_t length;
        } Buffer;
        
        static void jpegErrorFunction(j_common_ptr cinfo)
        {
            char buffer[JMSG_LENGTH_MAX];
            (*cinfo->err->format_message)(cinfo, buffer);
            throw Exception(buffer);
        }
        
        // -[ Private functions ]--------------------------------------------------------------------------------------------------------------------
        /// Opens the camera stream.
        void privateOpen(void);
        /// Closes the camera stream.
        void privateClose(void);
        /// Initializes the camera capture.
        void privateInitialize(void);
        /// Uninitialize the camera capture.
        void privateUninitialize(void);
        /// Starts to capture frames from the camera.
        void privateStart(void);
        /// Stops the capture of frames from the camera.
        void privateStop(void);
        
        // -[ Variables ]----------------------------------------------------------------------------------------------------------------------------
        /// Route to the file of the camera stream.
        char m_camera_filename[CAMERA_FILENAME_LENGTH];
        /// Width of the camera image.
        unsigned int m_width;
        /// Height of the camera image.
        unsigned int m_height;
        /// Frames per second of the camera.
        unsigned int m_frames_per_second;
        /// Format of the images retrieved from the driver.
        CAMERA_IMAGE_FORMAT m_image_format;
        /// Internal buffer where the camera image is stored.
        unsigned char * m_data;
        /// Number of elements copied to the internal buffer.
        unsigned int m_data_length;
        /// Descriptor of the camera stream file.
        int m_file_descriptor;
        /// Pointer to the camera memory buffers.
        Buffer * m_buffers;
        /// Number of buffers of the camera.
        unsigned int m_number_of_buffers;
        /// Boolean flag which enables or disables the display of error messages in the standard output.
        bool m_verbose;
        
        /// Minimum brightness value of the camera.
        int m_brightness_minimum;
        /// Maximum brightness value of the camera.
        int m_brightness_maximum;
        /// Brightness value of the camera.
        int m_brightness_value;
        /// Boolean flag which is true when the brightness value of the camera is available and false otherwise.
        bool m_brightness_enabled;
        /// Minimum contrast value of the camera.
        int m_contrast_minimum;
        /// Maximum contrast value of the camera.
        int m_contrast_maximum;
        /// Contrast value of the camera.
        int m_contrast_value;
        /// Boolean flag which is true when the contrast value of the camera is available and false otherwise.
        bool m_contrast_enabled;
        /// Minimum saturation value of the camera.
        int m_saturation_minimum;
        /// Maximum saturation value of the camera.
        int m_saturation_maximum;
        /// Saturation value of the camera.
        int m_saturation_value;
        /// Boolean flag which is true when the saturation value of the camera is available and false otherwise.
        bool m_saturation_enabled;
        /// Minimum hue value of the camera.
        int m_hue_minimum;
        /// Maximum hue value of the camera.
        int m_hue_maximum;
        /// Hue value of the camera.
        int m_hue_value;
        /// Boolean flag which is true when the hue value of the camera is available and false otherwise.
        bool m_hue_enabled;
        /// Boolean flag which is true when the hue value is set automatically.
        bool m_hue_auto;
        /// Boolean flag which is true when the hue value of the camera can be automatically set and false otherwise.
        bool m_hue_auto_enabled;
        /// Minimum sharpness value of the camera.
        int m_sharpness_minimum;
        /// Maximum sharpness value of the camera.
        int m_sharpness_maximum;
        /// Sharpness value of the camera.
        int m_sharpness_value;
        /// Boolean flag which is true when the sharpness value of the camera is available and false otherwise.
        bool m_sharpness_enabled;
        /// Index of the YUYV image format.
        int m_yuyv_format;
    private:
        // Define the copy constructor and the assignation operators as private to avoid multiple
        // copies of the same camera controller.
        /// Private definition of the copy constructor.
        CameraLinux(const CameraLinux &/* other */) {}
        /// Private definition of the assignation operator.
        CameraLinux& operator=(const CameraLinux &/* other */) { return *this; }
    };
}

#endif

