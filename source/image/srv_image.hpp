// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_HEADER_FILE__
#define __SRV_IMAGE_HEADER_FILE__

#include <iostream>
#include <png.h>
#include <jpeglib.h>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <limits>
#include <omp.h>
#include "../srv_vector.hpp"
#include "../srv_utilities.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
    // =[ DEFAULT NUMBER OF THREADS FOR THE IMAGE FUNCTIONS ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    class DefaultNumberOfThreads
    {
    public:
        /// Reference to the default number of threads for the image arithmetic functions.
        inline static unsigned int& ImageArithmetic(void) { static unsigned int m_default_number_of_threads_arithmetic = 1; return m_default_number_of_threads_arithmetic; }
        /// Reference to the default number of threads for the image processing functions.
        inline static unsigned int& ImageProcessing(void) { static unsigned int m_default_number_of_threads_processing = 1; return m_default_number_of_threads_processing; }
        /// Reference to the default number of threads for the image color functions.
        inline static unsigned int& ImageColor(void) { static unsigned int m_default_number_of_threads_color = 1; return m_default_number_of_threads_color; }
        /// Reference to the default number of threads for the image transform functions.
        inline static unsigned int& ImageTransform(void) { static unsigned int m_default_number_of_threads_transform = 1; return m_default_number_of_threads_transform; }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // ==============================================================================================================================================
    
    //                   +--------------------------------------+
    //                   | IMAGE AND SUB-IMAGE ARITHMETIC       |
    //                   | TEMPLATE FUNCTIONS                   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ GENERAL FUNCTIONS WHICH IMPLEMENT THE PIXEL-WISE OPERATOR FUNCTIONALITY ]==================================================================
    /** This template function calculates the operation defined with the template operators between the two input images and it stores the result
     *  in the given result image. The function applies the three different template operators depending on the amount of operands used (i.e.
     *  A = A OPERATOR A, A = A OPERATOR B or C = A OPERATOR B).
     *  \param[in] first_image first image or sub-image.
     *  \param[in] second_image second image or sub-image.
     *  \param[out] result_image image or sub-image where the result is going to be stored.
     *  \param[in] operator operator object which is applied between the two images.
     *  \param[in] number_of_threads number of threads used to concurrently process the images.
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT, class OPERATOR>
    void ImageGeneralOperator(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, OPERATOR, unsigned int number_of_threads)
    {
        if (((void*)&first_image == (void*)&result_image) && ((void*)&second_image == (void*)&result_image)) // A = A OPERATOR A;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Self(result_ptr[x]);
                    }
                }
            }
        }
        else if ((void*)&first_image == (void*)&result_image) // A = A OPERATOR B;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const ST * __restrict__ second_ptr = second_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Assignment(result_ptr[x], second_ptr[x]);
                    }
                }
            }
        }
        else if ((void*)&second_image == (void*)&result_image) // B = A OPERATOR B;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const FT * __restrict__ first_ptr = first_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], first_ptr[x], result_ptr[x]);
                    }
                }
            }
        }
        else // C = A OPERATOR B;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const FT * __restrict__ first_ptr = first_image.get(y, c);
                        const ST * __restrict__ second_ptr = second_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], first_ptr[x], second_ptr[x]);
                    }
                }
            }
        }
    }
    
    /** This template function calculates the operation defined with the template operators between a input image and the given value and it
     *  stores the obtained result in the given result image. The function applies the three different template operators depending on the
     *  amount of operands used (i.e. A = A OPERATOR A, A = A OPERATOR B or C = A OPERATOR B).
     *  \param[in] source_image left-operand image or sub-image.
     *  \param[in] value right-operand value applied at each pixel of the input image..
     *  \param[out] result_image image or sub-image where the result is going to be stored.
     *  \param[in] operator operator object which is applied between the two images.
     *  \param[in] number_of_threads number of threads used to concurrently process the images.
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT, class OPERATOR>
    void ImageGeneralOperator(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, OPERATOR, unsigned int number_of_threads)
    {
        if ((void*)&source_image == (void*)&result_image) // A = A OPERATOR VALUE;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Assignment(result_ptr[x], value);
                    }
                }
            }
        }
        else // B = A OPERATOR VALUE;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const FT * __restrict__ source_ptr = source_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], source_ptr[x], value);
                    }
                }
            }
        }
    }
    
    /** This template function calculates the operation defined with the template operators between the given value and a input image and it
     *  stores the obtained result in the given result image. The function applies the three different template operators depending on the
     *  amount of operands used (i.e. A = A OPERATOR A, A = A OPERATOR B or C = A OPERATOR B).
     *  \param[in] value left-operand value applied at each pixel of the input image.
     *  \param[in] source_image right-operand image or sub-image.
     *  \param[out] result_image image or sub-image where the result is going to be stored.
     *  \param[in] operator operator object which is applied between the two images.
     *  \param[in] number_of_threads number of threads used to concurrently process the images.
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT, class OPERATOR>
    void ImageGeneralOperator(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, OPERATOR, unsigned int number_of_threads)
    {
        if ((void*)&source_image == (void*)&result_image) // A = A OPERATOR VALUE;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], value, result_ptr[x]);
                    }
                }
            }
        }
        else // B = A OPERATOR VALUE;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const FT * __restrict__ source_ptr = source_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], value, source_ptr[x]);
                    }
                }
            }
        }
    }
    
    /** This template function calculates the result of applying an unary operator to each pixel of the given image. The function applies
     *  two different template operators depending on the source image is the output image or not (i.e. A = OPERATOR A or B = OPERATOR A).
     *  \param[in] source_image source image or sub-image.
     *  \param[out] result_image image or sub-image where the result is going to be stored.
     *  \param[in] operator operator object which is applied between the two images.
     *  \param[in] number_of_threads number of threads used to concurrently process the images.
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     */
    template <template <class> class SOURCE, class FT, template <class> class RESULT, class RT, class OPERATOR>
    void ImageGeneralOperator(const SOURCE<FT> &source_image, RESULT<RT> &result_image, OPERATOR, unsigned int number_of_threads)
    {
        if ((void*)&source_image == (void*)&result_image) // A = OPERATOR A;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Self(result_ptr[x]);
                    }
                }
            }
        }
        else // B = OPERATOR A;
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < result_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < result_image.getHeight(); y += number_of_threads)
                    {
                        const FT * __restrict__ source_ptr = source_image.get(y, c);
                        RT * __restrict__ result_ptr = result_image.get(y, c);
                        for (unsigned int x = 0; x < result_image.getWidth(); ++x) OPERATOR::Operator(result_ptr[x], source_ptr[x]);
                    }
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ STATIC FUNCTION CLASSES WITH ARITHMETIC OPERATORS FOR EACH PIXEL ]=========================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the pixel-wise addition operation.
    class PixelAddition
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(value + value); }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result + (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first + (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise subtraction operation.
    class PixelSubtraction
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result - (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first - (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise multiplication operation.
    class PixelMultiplication
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(value * value); }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result * (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first * (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise division operation.
    class PixelDivision
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)1; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result / (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first / (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise modulo operation.
    class PixelModulo
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result % (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first % (RT)second);
        }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ STATIC FUNCTION CLASSES WITH BITWISE OPERATORS FOR EACH PIXEL ]============================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the pixel-wise bitwise OR operation.
    class PixelBitwiseOr
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &/* value */) { }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            result = (T)((long)result | (long)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second) { result = (T)((long)first | (long)second); }
    };
    
    /// Class to calculate the pixel-wise bitwise AND operation.
    class PixelBitwiseAnd
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &/* value */) { }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value) { result = (T)((long)result & (long)value); }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second) { result = (T)((long)first & (long)second); }
    };
    
    /// Class to calculate the pixel-wise bitwise right shift operation.
    class PixelBitwiseRightShift
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(value >> value); }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value) { result = (T)((long)result >> (long)value); }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second) { result = (T)((long)first >> (long)second); }
    };
    
    /// Class to calculate the pixel-wise bitwise left shift operation.
    class PixelBitwiseLeftShift
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(value << value); }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value) { result = (T)((long)result << (long)value); }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second) { result = (T)((long)first << (long)second); }
    };
    
    /// Class to calculate the pixel-wise bitwise XOR operation.
    class PixelBitwiseXOR
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value) { result = (T)((long)result ^ (long)value); }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second) { result = (T)((long)first ^ (long)second); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ STATIC FUNCTION CLASSES WITH LOGIC OPERATORS FOR EACH PIXEL ]==============================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the logic AND operation.
    class PixelAnd
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = value && value; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result && (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first && (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise logic OR operation.
    class PixelOr
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = value || value; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result || (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first || (RT)second);
        }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ STATIC FUNCTION CLASSES WITH COMPARISON OPERATORS FOR EACH PIXEL ]=========================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the pixel-wise equal comparison operation.
    class PixelEqual
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)1; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result == (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first == (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise different comparison operation.
    class PixelDifferent
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result != (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first != (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise greater than comparison operation.
    class PixelGreaterThan
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<U, T>::TYPE RT;
            result = (T)((RT)result > (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first > (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise lesser than comparison operation.
    class PixelLesserThan
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)0; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result < (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first < (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise greater or equal comparison operation.
    class PixelGreaterEqualThan
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)1; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result >= (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first >= (RT)second);
        }
    };
    
    /// Class to calculate the pixel-wise lesser or equal comparison operation.
    class PixelLesserEqualThan
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)1; }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)((RT)result <= (RT)value);
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = (T)((RT)first <= (RT)second);
        }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ STATIC FUNCTION CLASSES WITH UNARY OPERATORS FOR EACH PIXEL ]==============================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the pixel-wise negate (sign change) unary operation.
    class PixelNegate
    {
    public:
        /** Static function which modifies the given value by the result of the unary operator result.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)-value; }
        /** Static function which returns the result of applying the unary operator to the given value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value input pixel value.
         */
        template <class T, class U> static inline void Operator(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            result = (T)(-(RT)value);
        }
    };
    
    /// Class to calculate the pixel-wise invert unary operation.
    class PixelInvert
    {
    public:
        /** Static function which modifies the given value by the result of the unary operator result.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(std::numeric_limits<T>::max() - value); }
        /** Static function which returns the result of applying the unary operator to the given value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value input pixel value.
         */
        template <class T, class U> static inline void Operator(T &result, const U &value) { result = (T)(std::numeric_limits<T>::max() - (T)value); }
    };
    
    /// Class to calculate the pixel-wise unary not operation.
    class PixelNot
    {
    public:
        /** Static function which modifies the given value by the result of the unary operator result.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(!value); }
        /** Static function which returns the result of applying the unary operator to the given value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value input pixel value.
         */
        template <class T, class U> static inline void Operator(T &result, const U &value) { result = (T)!value; }
    };
    
    /// Class to calculate the pixel-wise unary bitwise not operation.
    class PixelBitwiseNot
    {
    public:
        /** Static function which modifies the given value by the result of the unary operator result.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &value) { value = (T)(~(long)value); }
        /** Static function which returns the result of applying the unary operator to the given value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value input pixel value.
         */
        template <class T, class U> static inline void Operator(T &result, const U &value) { result = (T)(~(long)value); }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR MAXIMUM/MINIMUM OPERATORS BETWEEN IMAGES ]====================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /// Class to calculate the pixel-wise minimum value.
    class PixelMinimum
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &/* value */) { }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            if ((RT)value < (RT)result) result = (T)value;
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = ((RT)first < (RT)second)?(T)first:(T)second;
        }
    };
    
    /// Class to calculate the pixel-wise maximum value.
    class PixelMaximum
    {
    public:
        /** Static function which calculates the result operating the pixel value by itself.
         *  \param[in,out] value pixel value.
         */
        template <class T> static inline void Self(T &/*value*/) { }
        /** Static function which assigns to the pixel value the result of operating it with another value.
         *  \param[in, out] result resulting pixel value.
         *  \param[in] value other operand.
         */
        template <class T, class U> static inline void Assignment(T &result, const U &value)
        {
            typedef typename META_MGT<T, U>::TYPE RT;
            if ((RT)value > (RT)result) result = (T)value;
        }
        /** Static function which calculates the result of operating two different pixel values.
         *  \param[out] result where the result of the operation is stored.
         *  \param[in] first first operand value.
         *  \param[in] second second operand value.
         */
        template <class T, class U, class V> static inline void Operator(T &result, const U &first, const V &second)
        {
            typedef typename META_MGT<U, V>::TYPE RT;
            result = ((RT)first > (RT)second)?(T)first:(T)second;
        }
    };
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR ARITHMETIC OPERATORS ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise addition of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageAddition(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelAddition(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise addition of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageAddition(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelAddition(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise addition of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageAddition(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelAddition(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise subtraction of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageSubtraction(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelSubtraction(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise subtraction of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageSubtraction(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelSubtraction(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise subtraction of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageSubtraction(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelSubtraction(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise multiplication of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageMultiplication(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelMultiplication(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise multiplication of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageMultiplication(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelMultiplication(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise multiplication of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageMultiplication(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelMultiplication(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise division of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageDivision(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelDivision(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise division of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageDivision(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelDivision(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise division of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageDivision(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelDivision(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise modulo of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageModulo(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelModulo(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise modulo of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageModulo(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelModulo(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise modulo of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageModulo(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelModulo(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR BITWISE OPERATORS ]===========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise bitwise OR of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseOr(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelBitwiseOr(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise OR of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseOr(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelBitwiseOr(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise OR of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseOr(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelBitwiseOr(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise bitwise AND of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseAnd(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelBitwiseAnd(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise AND of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseAnd(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelBitwiseAnd(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise AND of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseAnd(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelBitwiseAnd(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise bitwise right shift of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseRightShift(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelBitwiseRightShift(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise right shift of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseRightShift(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelBitwiseRightShift(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise right shift of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseRightShift(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelBitwiseRightShift(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise bitwise left shift of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseLeftShift(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelBitwiseLeftShift(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise left shift of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseLeftShift(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelBitwiseLeftShift(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise left shift of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseLeftShift(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelBitwiseLeftShift(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise bitwise XOR of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseXOR(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelBitwiseXOR(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise XOR of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseXOR(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelBitwiseXOR(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise XOR of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseXOR(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelBitwiseXOR(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR LOGIC OPERATORS ]=============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise logic AND of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageAnd(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelAnd(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise AND of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageAnd(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelAnd(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise AND of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageAnd(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelAnd(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise logic OR of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageOr(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelOr(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise OR of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageOr(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelOr(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise bitwise OR of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageOr(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelOr(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR COMPARISON OPERATORS ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise equal comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageEqual(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelEqual(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise equal comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageEqual(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelEqual(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise equal comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageEqual(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelEqual(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise different comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageDifferent(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelDifferent(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise different comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageDifferent(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelDifferent(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise different comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageDifferent(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelDifferent(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise greater than comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterThan(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelGreaterThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise greater than comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterThan(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelGreaterThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise greater than comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterThan(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelGreaterThan(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise lesser than comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserThan(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelLesserThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise lesser than comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserThan(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelLesserThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise lesser than comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserThan(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelLesserThan(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise greater equal than comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterEqualThan(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelGreaterEqualThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise greater equal than comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterEqualThan(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelGreaterEqualThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise greater equal than comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageGreaterEqualThan(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelGreaterEqualThan(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise lesser equal than comparison of two images.
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserEqualThan(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelLesserEqualThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise lesser equal than comparison of a given value and an image.
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserEqualThan(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelLesserEqualThan(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise lesser equal than comparison of a given value and an image.
     *  \param[in] value left operand value.
     *  \param[in] source_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first and results images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageLesserEqualThan(const ST &value, const SOURCE<FT> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(value, source_image, result_image, PixelLesserEqualThan(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR UNARY OPERATORS ]=============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise unary negate operator.
     *  \param[in] source_image operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and results images can be the same image.
     */
    template <template <class> class SOURCE, class ST, template <class> class RESULT, class RT>
    inline void ImageNegate(const SOURCE<ST> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, result_image, PixelNegate(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise unary invert operator.
     *  \param[in] source_image operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and results images can be the same image.
     */
    template <template <class> class SOURCE, class ST, template <class> class RESULT, class RT>
    inline void ImageInvert(const SOURCE<ST> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, result_image, PixelInvert(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise unary not operator.
     *  \param[in] source_image operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and results images can be the same image.
     */
    template <template <class> class SOURCE, class ST, template <class> class RESULT, class RT>
    inline void ImageNot(const SOURCE<ST> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, result_image, PixelNot(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise unary bitwise not operator.
     *  \param[in] source_image operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and results images can be the same image.
     */
    template <template <class> class SOURCE, class ST, template <class> class RESULT, class RT>
    inline void ImageBitwiseNot(const SOURCE<ST> &source_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, result_image, PixelBitwiseNot(), number_of_threads); }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ SPECIFIC IMAGE FUNCTIONS FOR MAXIMUM/MINIMUM OPERATORS BETWEEN IMAGES ]====================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Returns the image obtained by calculating the pixel-wise minimum operation between two images (i.e.\ the minimum between the two given
     *  image pixels is assigned to the resulting image).
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageMinimum(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelMinimum(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise minimum operation between an image and the given value (i.e.\ the pixels of the resulting image
     *  will be set to the given value except for source image pixels which are lower than the value).
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and result images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageMinimum(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelMinimum(), number_of_threads); }
    
    /** Returns the image obtained by calculating the pixel-wise maximum operation between two images (i.e.\ the maximum between the two given
     *  image pixels is assigned to the resulting image).
     *  \param[in] first_image left operand image.
     *  \param[in] second_image right operand image.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The first, second and results images can be the same image.
     */
    template <template <class> class FIRST, class FT, template <class> class SECOND, class ST, template <class> class RESULT, class RT>
    inline void ImageMaximum(const FIRST<FT> &first_image, const SECOND<ST> &second_image, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(first_image, second_image, result_image, PixelMaximum(), number_of_threads); }
    /** Returns the image obtained by calculating the pixel-wise maximum operation between an image and the given value (i.e.\ the pixels of the resulting image
     *  will be set to the given value except for source image pixels which are lower than the value).
     *  \param[in] source_image left operand image.
     *  \param[in] value right operand value.
     *  \param[out] result_image image where the result is stored.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not check the geometry of the given images, so that, a \b segmentation \b fault error can be generated
     *        when images have an different geometry.
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and result images can be the same image.
     */
    template <template <class> class SOURCE, class FT, class ST, template <class> class RESULT, class RT>
    inline void ImageMaximum(const SOURCE<FT> &source_image, const ST &value, RESULT<RT> &result_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic()) { ImageGeneralOperator(source_image, value, result_image, PixelMaximum(), number_of_threads); }
    
    /** Returns the image obtained by calculating the absolute value to each pixel of the input image.
     *  \param[in] source_image source image.
     *  \param[out] destination_image destination_image.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \note The function does not change the geometry of the result image. Therefore, if the result image geometry is not properly set, then
     *        a \b segmentation \b fault error may occur.
     *  \note The source and destination images can be the same image.
     */
    template <template <class> class SOURCE, class ST, template <class> class DESTINATION, class DT>
    void ImageAbs(const SOURCE<ST> &source_image, DESTINATION<DT> &destination_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic())
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int c = 0; c < destination_image.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < destination_image.getHeight(); y += number_of_threads)
                {
                    const ST *source_ptr = source_image.get(y, c);
                    DT *destination_ptr = destination_image.get(y, c);
                    for (unsigned int x = 0; x < destination_image.getWidth(); ++x)
                    {
                        destination_ptr[x] = (DT)(srvAbs<ST>(source_ptr[x]));
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TEMPLATE AUXILIARY FUNCTION FOR      |
    //                   | IMAGES AND SUB-IMAGES                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Auxiliary function which generates an exception when the geometry of both images is different
     *  (i.e.\ when both images have a different width, height or number of channels).
     *  \param[in] first first image or sub-image.
     *  \param[in] second second image or sub-image.
     */
    template <template <class> class FIRST_IMAGE, template <class> class SECOND_IMAGE, class T1, class T2>
    inline void checkGeometry(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second)
    {
        if ((second.getWidth() != first.getWidth()) || (second.getHeight() != first.getHeight()) || (second.getNumberOfChannels() != first.getNumberOfChannels()))
            throw Exception("Images must have the same geometry, but left operand image is (%d, %d, %d) and right operand image is (%d, %d, %d)", first.getWidth(), first.getHeight(), first.getNumberOfChannels(), second.getWidth(), second.getHeight(), second.getNumberOfChannels());
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONSTANT SUB-IMAGE CLASS DECLARATION |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // Forward declaration of the image class.
    template <class T> class Image;
    
    /** Template sub-image class where each channel is stored separately, so that, different 2D arrays are used for each channel.
     *  The sub-images point to rectangular regions of a template image.
     */
    template <class T>
    class ConstantSubImage
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ConstantSubImage(void) : m_image(0), m_width(0), m_original_width(0), m_height(0), m_number_of_channels(0) {}
        /// Parameters constructor.
        ConstantSubImage(T const * const * image_pointer, unsigned int offset, unsigned int width, unsigned int original_width, unsigned int height, unsigned int number_of_channels);
        /// Copy constructor.
        ConstantSubImage(const ConstantSubImage<T> &other);
        /// Generic copy constructor
        template <template <class> class IMAGE>
        ConstantSubImage(const IMAGE<T> &other);
        /// Destructor
        ~ConstantSubImage(void);
        /// Assignation operator.
        ConstantSubImage<T>& operator=(const ConstantSubImage<T> &other);
        /// Assignation operator.
        template <template <class> class IMAGE>
        ConstantSubImage<T>& operator=(const IMAGE<T> &other);
        
        // -[ Access to the image geometric information ]--------------------------------------------------------------------------------------------
        /// Returns the width of the image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the width of the original image.
        inline unsigned int getOriginalWidth(void) const { return m_original_width; }
        /// Returns the height of the image.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of channels of the image.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        /// This function generates an exception when the size of the given image differs from the size of the sub-image.
        template <template <class> class IMAGE, class TIMAGE>
        void setGeometry(IMAGE<TIMAGE> &other)
        {
            if ((m_width != other.getWidth()) || (m_height != other.getHeight()) || (m_number_of_channels != other.getNumberOfChannels()))
                throw Exception("A sub-image cannot change its geometry.");
        }
        
        // -[ Access to the image content ]----------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the image information.
        inline T const * const * get(void) const { return m_image; }
        /// Returns a constant pointer to a channel array of the image.
        inline const T * get(unsigned int channel) const { return m_image[channel]; }
        /** Returns a constant pointer the channel row of the image.
         *  \param[in] row row of the image.
         *  \param[in] channel channel of the image.
         *  \returns constant pointer to the image selected row.
         */
        inline const T * get(unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_original_width]; }
        /** Return a constant pointer to the selected pixel.
         *  \param[in] column column of the image (i.e.\ the x-coordinate).
         *  \param[in] row row of the image (i.e.\ the y-coordinate).
         *  \param[in] channel channel of the image.
         *  \returns a constant pointer to the selected pixel.
         */
        inline const T * get(unsigned int column, unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_original_width + column]; }
        
        // -[ Content copy functions ]---------------------------------------------------------------------------------------------------------------
        /** This function returns a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedImage(U * &data_pointer) const;
        /** This function returns a RGB image in a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedRGBImage(U * &data_pointer) const;
        
        // -[ Access to parts of the image ]---------------------------------------------------------------------------------------------------------
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \returns the resulting constant sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h) const { return ConstantSubImage<T>(m_image, x0 + y0 * m_original_width, w, m_original_width, h, m_number_of_channels); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting constant sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], x0 + y0 * m_original_width, w, m_original_width, h, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image with the selected channels.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting constant sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], 0, m_width, m_original_width, m_height, srvMax(c0, c1) - c0 + 1); }
        
        // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
        /// This function returns the minimum value of the image.
        template <class U> U minimum(void) const;
        /// This function returns the minimum values per channel of the image.
        template <class U> void minimum(VectorDense<U> &result) const;
        /// This function returns the maximum value of the image.
        template <class U> U maximum(void) const;
        /// This function returns the maximum values per channel of the image.
        template <class U> void maximum(VectorDense<U> &result) const;
        /// This function returns the sum of the pixels of the image.
        template <class U> U sum(void) const;
        /// This function returns the sum of the pixels of the image per channel.
        template <class U> void sum(VectorDense<U> &result) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> U product(void) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> void product(VectorDense<U> &result) const;
        
    private:
        /// Constant pointer to the image information.
        T const * * m_image;
        /// Number of columns of the image.
        unsigned int m_width;
        /// Number of columns of the original image.
        unsigned int m_original_width;
        /// Number of rows of the image.
        unsigned int m_height;
        /// Number of channels of the image.
        unsigned int m_number_of_channels;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SUB-IMAGE CLASS IMPLEMENTATION       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // -[ Constructors, destructor and assignation operator ]----------------------------------------------------------------------------------------
    template <class T>
    ConstantSubImage<T>::ConstantSubImage(T const * const * image_pointer, unsigned int offset, unsigned int width, unsigned int original_width, unsigned int height, unsigned int number_of_channels) :
        m_image(new const T * [number_of_channels]),
        m_width(width),
        m_original_width(original_width),
        m_height(height),
        m_number_of_channels(number_of_channels)
    {
        for (unsigned int i = 0; i < number_of_channels; ++i)
            m_image[i] = &image_pointer[i][offset];
    }
    
    template <class T>
    ConstantSubImage<T>::ConstantSubImage(const ConstantSubImage<T> &other) :
        m_image((other.m_image != 0)?new const T * [other.m_number_of_channels]:0),
        m_width(other.m_width),
        m_original_width(other.m_original_width),
        m_height(other.m_height),
        m_number_of_channels(other.m_number_of_channels)
    {
        for (unsigned int i = 0; i < other.m_number_of_channels; ++i)
            m_image[i] = other.m_image[i];
    }
    
    template <class T>
    template <template <class> class IMAGE>
    ConstantSubImage<T>::ConstantSubImage(const IMAGE<T> &other) :
        m_image((other.get() != 0)?new const T * [other.getNumberOfChannels()]:0),
        m_width(other.getWidth()),
        m_original_width(other.getOriginalWidth()),
        m_height(other.getHeight()),
        m_number_of_channels(other.getNumberOfChannels())
    {
        for (unsigned int i = 0; i < other.getNumberOfChannels(); ++i)
            m_image[i] = other.get(i);
    }
    
    template <class T>
    ConstantSubImage<T>::~ConstantSubImage(void)
    {
        if (m_image != 0) delete [] m_image;
    }
    
    template <class T>
    ConstantSubImage<T>& ConstantSubImage<T>::operator=(const ConstantSubImage<T> &other)
    {
        if (this != &other)
        {
            // Free ...................................................................................
            if (m_image != 0) delete [] m_image;
            
            // Get pointers from the other image, sub-image or constant sub-image .....................
            if (other.m_image != 0)
            {
                m_image = new const T * [other.m_number_of_channels];
                m_width = other.m_width;
                m_original_width = other.m_original_width;
                m_height = other.m_height;
                m_number_of_channels = other.m_number_of_channels;
                for (unsigned int i = 0; i < other.m_number_of_channels; ++i)
                    m_image[i] = other.m_image[i];
            }
            else
            {
                m_width = m_original_width = m_height = m_number_of_channels = 0;
                m_image = 0;
            }
        }
        
        return *this;
    }
    
    template <class T>
    template <template <class> class IMAGE>
    ConstantSubImage<T>& ConstantSubImage<T>::operator=(const IMAGE<T> &other)
    {
        // Free ...................................................................................
        if (m_image != 0) delete [] m_image;
        
        // Get pointers from the other image, sub-image or constant sub-image .....................
        if (other.get() != 0)
        {
            m_image = new const T * [other.getNumberOfChannels()];
            m_width = other.getWidth();
            m_original_width = other.getOriginalWidth();
            m_height = other.getHeight();
            m_number_of_channels = other.getNumberOfChannels();
            for (unsigned int i = 0; i < other.getNumberOfChannels(); ++i)
                m_image[i] = other.get(i);
        }
        else
        {
            m_width = m_original_width = m_height = m_number_of_channels = 0;
            m_image = 0;
        }
        
        return *this;
    }
    
    template <class T>
    template <class U>
    void ConstantSubImage<T>::getInterleavedImage(U * &data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * m_number_of_channels];
            for (unsigned int c = 0; c < m_number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, c);
                    for (unsigned int x = 0; x < m_width; ++x)
                        data_pointer[(x + y * m_width) * m_number_of_channels + c] = (U)inner_ptr[x];
                }
            }
        }
        else data_pointer = 0;
    }
    
    template <class T>
    template <class U>
    void ConstantSubImage<T>::getInterleavedRGBImage(U * &data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * 3];
            if (m_number_of_channels == 3)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < m_height; ++y)
                    {
                        const T *inner_ptr = get(y, c);
                        for (unsigned int x = 0; x < m_width; ++x)
                            data_pointer[(x + y * m_width) * m_number_of_channels + c] = (U)inner_ptr[x];
                    }
                }
            }
            else
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, 0);
                    for (unsigned int x = 0; x < m_width; ++x)
                    {
                        data_pointer[(x + y * m_width) * 3] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 1] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 2] = (U)inner_ptr[x];
                    }
                }
            }
        }
        else data_pointer = 0;
    }
    
    // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
    template <class T>
    template <class U> U ConstantSubImage<T>::minimum(void) const
    {
        U minimum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < minimum_value) minimum_value = (U)inner_ptr[x];
            }
        }
        return minimum;
    }
    
    template <class T>
    template <class U> void ConstantSubImage<T>::minimum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U ConstantSubImage<T>::maximum(void) const
    {
        U maximum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > maximum_value) maximum_value = (U)inner_ptr[x];
            }
        }
        return maximum;
    }
    
    template <class T>
    template <class U> void ConstantSubImage<T>::maximum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U ConstantSubImage<T>::sum(void) const
    {
        U value = 0;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value += (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void ConstantSubImage<T>::sum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 0;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] += (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U ConstantSubImage<T>::product(void) const
    {
        U value = 1;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value *= (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void ConstantSubImage<T>::product(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 1;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] *= (U)inner_ptr[x];
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SUB-IMAGE CLASS DECLARATION          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Template sub-image class where each channel is stored separately, so that, different 2D arrays are used for each channel.
     *  The sub-images point to rectangular regions of a template image.
     */
    template <class T>
    class SubImage
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SubImage(void) : m_image(0), m_width(0), m_original_width(0), m_height(0), m_number_of_channels(0) {}
        /// Parameters constructor.
        SubImage(T * * image_pointer, unsigned int offset, unsigned int width, unsigned int original_width, unsigned int height, unsigned int number_of_channels);
        /// Copy constructor.
        SubImage(const SubImage<T> &other);
        /// Constructor which initializes the sub-image from another image, sub-image or constant sub-image.
        template <template <class> class IMAGE>
        SubImage(const IMAGE<T> &other);
        /// Constructor which initializes the sub-image from another image, sub-image or constant sub-image.
        template <template <class> class IMAGE>
        SubImage(IMAGE<T> &other);
        /// Destructor
        ~SubImage(void);
        /// Assignation operator.
        SubImage<T>& operator=(const SubImage<T> &other);
        /// Assignation operator which sets the content of the sub-image from another image, sub-image or constant sub-image.
        template <template <class> class IMAGE>
        SubImage<T>& operator=(IMAGE<T> &other);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /** Sets the content of the sub-image.
         *  \param[in] value value assigned to each pixel of the sub-image.
         */
        void setValue(const T &value);
        /** Sets the content of the sub-image.
         *  \param[in] values array with the values assigned to each channel of the sub-image.
         */
        void setValues(const T *values);
        
        // -[ Access to the image geometric information ]--------------------------------------------------------------------------------------------
        /// Returns the width of the image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the width of the original image.
        inline unsigned int getOriginalWidth(void) const { return m_original_width; }
        /// Returns the height of the image.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of channels of the image.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        /// This function generates an exception when the size of the given image differs from the size of the sub-image.
        template <template <class> class IMAGE, class TIMAGE>
        void setGeometry(const IMAGE<TIMAGE> &other)
        {
            if ((m_width != other.getWidth()) || (m_height != other.getHeight()) || (m_number_of_channels != other.getNumberOfChannels()))
                throw Exception("A sub-image cannot change its geometry.");
        }
        
        // -[ Access to the image content ]----------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the image information.
        inline T const * const * get(void) const { return m_image; }
        /// Returns a constant pointer to a channel array of the image.
        inline const T * get(unsigned int channel) const { return m_image[channel]; }
        /** Returns a constant pointer the channel row of the image.
         *  \param[in] row row of the image.
         *  \param[in] channel channel of the image.
         *  \returns constant pointer to the image selected row.
         */
        inline const T * get(unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_original_width]; }
        /** Return a constant pointer to the selected pixel.
         *  \param[in] column column of the image (i.e.\ the x-coordinate).
         *  \param[in] row row of the image (i.e.\ the y-coordinate).
         *  \param[in] channel channel of the image.
         *  \returns a constant pointer to the selected pixel.
         */
        inline const T * get(unsigned int column, unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_original_width + column]; }
        
        /// Returns a pointer to the image information.
        inline T * * get(void) { return m_image; }
        /// Returns a pointer to a channel array of the image.
        inline T * get(unsigned int channel) { return m_image[channel]; }
        /** Returns a pointer the channel row of the image.
         *  \param[in] row row of the image.
         *  \param[in] channel channel of the image.
         *  \returns pointer to the image selected row.
         */
        inline T * get(unsigned int row, unsigned int channel) { return &m_image[channel][row * m_original_width]; }
        /** Return a pointer to the selected pixel.
         *  \param[in] column column of the image (i.e.\ the x-coordinate).
         *  \param[in] row row of the image (i.e.\ the y-coordinate).
         *  \param[in] channel channel of the image.
         *  \returns a pointer to the selected pixel.
         */
        inline T * get(unsigned int column, unsigned int row, unsigned int channel) { return &m_image[channel][row * m_original_width + column]; }
        
        // -[ Access to parts of the image ]---------------------------------------------------------------------------------------------------------
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \returns the resulting sub-image.
         */
        inline SubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h) { return SubImage<T>(m_image, x0 + y0 * m_original_width, w, m_original_width, h, m_number_of_channels); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting sub-image.
         */
        inline SubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, unsigned int c0, unsigned int c1 = 0) { return SubImage<T>(&m_image[c0], x0 + y0 * m_original_width, w, m_original_width, h, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image with the selected channels.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         */
        inline SubImage<T> subimage(unsigned int c0, unsigned int c1 = 0) { return SubImage<T>(&m_image[c0], 0, m_width, m_original_width, m_height, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \returns the resulting sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h) const { return ConstantSubImage<T>(m_image, x0 + y0 * m_original_width, w, m_original_width, h, m_number_of_channels); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], x0 + y0 * m_original_width, w, m_original_width, h, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image with the selected channels.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         */
        inline ConstantSubImage<T> subimage(unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], 0, m_width, m_original_width, m_height, srvMax(c0, c1) - c0 + 1); }
        
        // -[ Content copy functions ]---------------------------------------------------------------------------------------------------------------
        /** Copies the content of the given sub-image to the sub-image.
         *  \note The function does not check that the geometry of both images is correct, so that,
         *  a segmentation fault can be generated if both sub-images have a different geometry.
         *  \param[in] source source sub-image.
         */
        template <template <class> class IMAGE, class U>
        SubImage<T>& copy(const IMAGE<U> &source);
        /** This function returns a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedImage(U *&data_pointer) const;
        /** This function returns a RGB image in a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedRGBImage(U *&data_pointer) const;
        
        // -[ Image modification functions ]---------------------------------------------------------------------------------------------------------
        /// This function flips the image vertically.
        void flipVertical(void);
        /// This function flips the image horizontally.
        void flipHorizontal(void);
        
        // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
        /// This function returns the minimum value of the image.
        template <class U> U minimum(void) const;
        /// This function returns the minimum values per channel of the image.
        template <class U> void minimum(VectorDense<U> &result) const;
        /// This function returns the maximum value of the image.
        template <class U> U maximum(void) const;
        /// This function returns the maximum values per channel of the image.
        template <class U> void maximum(VectorDense<U> &result) const;
        /// This function returns the sum of the pixels of the image.
        template <class U> U sum(void) const;
        /// This function returns the sum of the pixels of the image per channel.
        template <class U> void sum(VectorDense<U> &result) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> U product(void) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> void product(VectorDense<U> &result) const;
        
        // -[ Image the content of the current image to the extrema of the given two images ]--------------------------------------------------------
        /** Sets the content of the current sub-image to the minimum values of the two given images.
         *  \param[in] first first image or sub-image.
         *  \param[in] second second image or sub-image.
         */
        template <template <class> class FIRST_IMAGE, template <class> class SECOND_IMAGE, class T1, class T2>
        inline void min(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second) { checkGeometry(*this, first); checkGeometry(*this, second); ImageMinimum(first, second, *this); }
        /** Sets the content of the current sub-image equal to the values of the given image which are lower than the specified value.
         *  \param[in] image image or sub-image.
         *  \param[in] value upper threshold value.
         */
        template <template <class> class IMAGE_TYPE, class T1, class T2>
        inline void min(const IMAGE_TYPE<T1> &image, const T2 &value) { checkGeometry(*this, image); ImageMinimum(image, value, *this); }
        /** Sets the content of the current sub-image to the maximum values of the two given images.
         *  \param[in] first first image or sub-image.
         *  \param[in] second second image or sub-image.
         */
        template <template <class> class FIRST_IMAGE, template <class> class SECOND_IMAGE, class T1, class T2>
        inline void max(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second) { checkGeometry(*this, first); checkGeometry(*this, second); ImageMaximum(first, second, *this); }
        /** Sets the content of the current sub-image equal to the values of the given image which are greter than the specified value.
         *  \param[in] image image or sub-image.
         *  \param[in] value lower threshold value.
         */
        template <template <class> class IMAGE_TYPE, class T1, class T2>
        inline void max(const IMAGE_TYPE<T1> &image, const T2 &value) { checkGeometry(*this, image); ImageMaximum(image, value, *this); }
        /// Inverts the content of the sub-image.
        inline void invert(void) { ImageInvert(*this, *this); }
        
        /// Converts the sub-image by applying the absolute value operator.
        inline SubImage<T>& abs(void) { ImageAbs(*this, *this); return *this; }
        /// Returns a copy of the sub-image where the absolute value operator has been applied.
        inline Image<T> absCopy(void) const { Image<T> copy_image(m_width, m_height, m_number_of_channels); ImageAbs(*this, copy_image); return copy_image; }
        
    private:
        /// Constant pointer to the image information.
        T * * m_image;
        /// Number of columns of the image.
        unsigned int m_width;
        /// Number of columns of the original image.
        unsigned int m_original_width;
        /// Number of rows of the image.
        unsigned int m_height;
        /// Number of channels of the image.
        unsigned int m_number_of_channels;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SUB-IMAGE CLASS IMPLEMENTATION       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // -[ Constructors, destructor and assignation operator ]----------------------------------------------------------------------------------------
    template <class T>
    SubImage<T>::SubImage(T * * image_pointer, unsigned int offset, unsigned int width, unsigned int original_width, unsigned int height, unsigned int number_of_channels) :
        m_image(new T * [number_of_channels]),
        m_width(width),
        m_original_width(original_width),
        m_height(height),
        m_number_of_channels(number_of_channels)
    {
        for (unsigned int i = 0; i < number_of_channels; ++i)
            m_image[i] = &image_pointer[i][offset];
    }
    
    template <class T>
    SubImage<T>::SubImage(const SubImage<T> &other) :
        m_image((other.m_image != 0)?new T * [other.m_number_of_channels]:0),
        m_width(other.m_width),
        m_original_width(other.m_original_width),
        m_height(other.m_height),
        m_number_of_channels(other.m_number_of_channels)
    {
        for (unsigned int i = 0; i < other.m_number_of_channels; ++i)
            m_image[i] = other.m_image[i];
    }
    
    template <class T>
    template <template <class> class IMAGE>
    SubImage<T>::SubImage(const IMAGE<T> &other) :
        m_image((other.get() != 0)?new T * [other.getNumberOfChannels()]:0),
        m_width(other.getWidth()),
        m_original_width(other.getOriginalWidth()),
        m_height(other.getHeight()),
        m_number_of_channels(other.getNumberOfChannels())
    {
        for (unsigned int i = 0; i < other.getNumberOfChannels(); ++i)
            m_image[i] = other.get(i);
    }
    
    template <class T>
    template <template <class> class IMAGE>
    SubImage<T>::SubImage(IMAGE<T> &other) :
        m_image((other.get() != 0)?new T * [other.getNumberOfChannels()]:0),
        m_width(other.getWidth()),
        m_original_width(other.getOriginalWidth()),
        m_height(other.getHeight()),
        m_number_of_channels(other.getNumberOfChannels())
    {
        for (unsigned int i = 0; i < other.getNumberOfChannels(); ++i)
            m_image[i] = other.get(i);
    }
    
    template <class T>
    SubImage<T>::~SubImage(void)
    {
        if (m_image != 0) delete [] m_image;
    }
    
    template <class T>
    SubImage<T>& SubImage<T>::operator=(const SubImage<T> &other)
    {
        if (this != &other)
        {
            // Free ...............................................................................
            if (m_image != 0) delete [] m_image;
            
            // Get pointer from the other image, sub-image or constant sub-image ..................
            if (other.m_image != 0)
            {
                m_image = new T * [other.m_number_of_channels];
                m_width = other.m_width;
                m_original_width = other.m_original_width;
                m_height = other.m_height;
                m_number_of_channels = other.m_number_of_channels;
                for (unsigned int i = 0; i < other.m_number_of_channels; ++i)
                    m_image[i] = other.m_image[i];
            }
            else
            {
                m_width = m_original_width = m_height = m_number_of_channels = 0;
                m_image = 0;
            }
        }
        return *this;
    }
    
    
    template <class T>
    template <template <class> class IMAGE>
    SubImage<T>& SubImage<T>::operator=(IMAGE<T> &other)
    {
        if ((void*)this != (void*)&other)
        {
            // Free ...............................................................................
            if (m_image != 0) delete [] m_image;
            
            // Get pointer from the other image, sub-image or constant sub-image ..................
            if (other.get() != 0)
            {
                m_image = new T * [other.getNumberOfChannels()];
                m_width = other.getWidth();
                m_original_width = other.getOriginalWidth();
                m_height = other.getHeight();
                m_number_of_channels = other.getNumberOfChannels();
                for (unsigned int i = 0; i < other.getNumberOfChannels(); ++i)
                    m_image[i] = other.get(i);
            }
            else
            {
                m_width = m_original_width = m_height = m_number_of_channels = 0;
                m_image = 0;
            }
        }
        return *this;
    }
    
    template <class T>
    void SubImage<T>::setValue(const T &value)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T * inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x)
                    inner_ptr[x] = value;
            }
        }
    }
    
    template <class T>
    void SubImage<T>::setValues(const T *values)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T * inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x)
                    inner_ptr[x] = values[c];
            }
        }
    }
    
    //-[ Content copy functions ]----------------------------------------------------------------------------------------------------------------
    template <class T>
    template <template <class> class IMAGE, class U>
    SubImage<T>& SubImage<T>::copy(const IMAGE<U> &source)
    {
        for (unsigned int c = 0; c < source.getNumberOfChannels(); ++c)
        {
            for (unsigned int y = 0; y < source.getHeight(); ++y)
            {
                const U * ptr_source = source.get(y, c);
                T * ptr_destination = get(y, c);
                for (unsigned int x = 0; x < source.getWidth(); ++x)
                    ptr_destination[x] = (T)ptr_source[x];
            }
        }
        return *this;
    }
    
    template <class T>
    template <class U>
    void SubImage<T>::getInterleavedImage(U *&data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * m_number_of_channels];
            for (unsigned int c = 0; c < m_number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, c);
                    for (unsigned int x = 0; x < m_width; ++x)
                        data_pointer[(x + y * m_width) * m_number_of_channels + c] = (U)inner_ptr[x];
                }
            }
        }
        else data_pointer = 0;
    }
    
    template <class T>
    template <class U>
    void SubImage<T>::getInterleavedRGBImage(U *&data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * 3];
            if (m_number_of_channels == 3)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < m_height; ++y)
                    {
                        const T *inner_ptr = get(y, c);
                        for (unsigned int x = 0; x < m_width; ++x)
                            data_pointer[(x + y * m_width) * m_number_of_channels + c] = (U)inner_ptr[x];
                    }
                }
            }
            else
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, 0);
                    for (unsigned int x = 0; x < m_width; ++x)
                    {
                        data_pointer[(x + y * m_width) * 3] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 1] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 2] = (U)inner_ptr[x];
                    }
                }
            }
        }
        else data_pointer = 0;
    }
    
    // -[ Image modification functions ]---------------------------------------------------------------------------------------------------------
    template <class T>
    void SubImage<T>::flipVertical(void)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height / 2; ++y)
            {
                T * top_ptr = &m_image[c][y * m_original_width];
                T * bottom_ptr = &m_image[c][(m_height - y - 1) * m_original_width];
                for (unsigned int x = 0; x < m_width; ++x)
                    srvSwap(top_ptr[x], bottom_ptr[x]);
            }
        }
    }
    
    template <class T>
    void SubImage<T>::flipHorizontal(void)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T *left_ptr = &m_image[c][y * m_original_width];
                T *right_ptr = &m_image[c][y * m_original_width + m_width - 1];
                for (unsigned int x = 0; x < m_width / 2; ++x, ++left_ptr, --right_ptr)
                    srvSwap(*left_ptr, *right_ptr);
            }
        }
    }
    
    // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
    template <class T>
    template <class U> U SubImage<T>::minimum(void) const
    {
        U minimum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < minimum_value) minimum_value = (U)inner_ptr[x];
            }
        }
        return minimum;
    }
    
    template <class T>
    template <class U> void SubImage<T>::minimum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U SubImage<T>::maximum(void) const
    {
        U maximum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > maximum_value) maximum_value = (U)inner_ptr[x];
            }
        }
        return maximum;
    }
    
    template <class T>
    template <class U> void SubImage<T>::maximum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U SubImage<T>::sum(void) const
    {
        U value = 0;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value += (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void SubImage<T>::sum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 0;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] += (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U SubImage<T>::product(void) const
    {
        U value = 1;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value *= (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void SubImage<T>::product(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 1;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] *= (U)inner_ptr[x];
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE CLASS DECLARATION              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Template image class where each channel is stored separately, so that, different 2D arrays are used for each channel.
    template <class T>
    class Image
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        Image(void);
        /** Parameters constructor where the parameters of the image geometry are given as parameters.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] channels number of channels of the image.
         */
        Image(unsigned int width, unsigned int height, unsigned int channels);
        /** Parameters constructor where the parameters of the image geometry are given as parameters.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] channels number of channels of the image.
         *  \param[in] value value used to initialize the channels of the image.
         */
        Image(unsigned int width, unsigned int height, unsigned int channels, const T &value);
        /** Parameters constructor where the parameters of the image geometry are given as parameters.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] channels number of channels of the image.
         *  \param[in] values array with the values used to initialize each channel of the image.
         */
        Image(unsigned int width, unsigned int height, unsigned int channels, const T * values);
        /** Parameters constructor where the file where the image is stored is given as a parameter.
         *  \param[in] filename array with the filename of the image to be loaded.
         *  \param[in] image_format format of the disk image. By default, the format of the image is determined by its extension.
         */
        Image(const char * filename, IMAGE_FORMATS image_format = IMAGE_FORMAT_EXTENSION);
        /// Copy constructor.
        Image(const Image<T> &other);
        /// Copy constructor which initializes from an image, sub-image or constant sub-image.
        template <template <class> class IMAGE, class U>
        Image(const IMAGE<U> &other);
        /// Destructor.
        ~Image(void) { freeImage(); }
        /// Assignation operator.
        Image<T>& operator=(const Image<T> &other);
        /// Assignation operator which sets the image from another image, an sub-image or a constant sub-image.
        template <template <class> class IMAGE, class U>
        Image<T>& operator=(const IMAGE<U> &other);
        
        // -[ Set the image values and geometry ]----------------------------------------------------------------------------------------------------
        /// Deallocates all the memory used by the image object and sets the object as an empty image.
        inline void clear(void)
        {
            freeImage();
            m_image = 0;
            m_width = m_height = m_number_of_channels = 0;
        }
        /** Changes the geometry of the image.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] number_of_channels number of channels of the image.
         */
        void setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels);
        /** Set the geometry of the image to the geometry parameters of the given image.
         *  \param[in] other image of the same data-type.
         */
        template <template <class> class IMAGE, class U>
        inline void setGeometry(const IMAGE<U> &other) { setGeometry(other.getWidth(), other.getHeight(), other.getNumberOfChannels()); }
        /** Sets the content of the image.
         *  \param[in] value value assigned to each pixel of the image.
         */
        void setValue(const T &value);
        /** Sets the content of the image.
         *  \param[in] values array with the values assigned to each channel of the image.
         */
        void setValues(const T * values);
        
        // -[ Access to the image geometric information ]--------------------------------------------------------------------------------------------
        /// Returns the width of the image.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the width of the image (needed for template initialization of sub-images and constant sub-images).
        inline unsigned int getOriginalWidth(void) const { return m_width; }
        /// Returns the height of the image.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of channels of the image.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        
        // -[ Access to the image content ]----------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the image information.
        inline T const * const * get(void) const { return m_image; }
        /// Returns a constant pointer to a channel array of the image.
        inline const T * get(unsigned int channel) const { return m_image[channel]; }
        /** Returns a constant pointer the channel row of the image.
         *  \param[in] row row of the image.
         *  \param[in] channel channel of the image.
         *  \returns constant pointer to the image selected row.
         */
        inline const T * get(unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_width]; }
        /** Return a constant pointer to the selected pixel.
         *  \param[in] column column of the image (i.e.\ the x-coordinate).
         *  \param[in] row row of the image (i.e.\ the y-coordinate).
         *  \param[in] channel channel of the image.
         *  \returns a constant pointer to the selected pixel.
         */
        inline const T * get(unsigned int column, unsigned int row, unsigned int channel) const { return &m_image[channel][row * m_width + column]; }
        /// Returns a pointer to the image information.
        inline T * * get(void) { return m_image; }
        /// Returns a pointer to a channel array of the image.
        inline T * get(unsigned int channel) { return m_image[channel]; }
        /** Returns a pointer the channel row of the image.
         *  \param[in] row row of the image.
         *  \param[in] channel channel of the image.
         *  \returns pointer to the image selected row.
         */
        inline T * get(unsigned int row, unsigned int channel) { return &m_image[channel][row * m_width]; }
        /** Return a pointer to the selected pixel.
         *  \param[in] column column of the image (i.e.\ the x-coordinate).
         *  \param[in] row row of the image (i.e.\ the y-coordinate).
         *  \param[in] channel channel of the image.
         *  \returns a pointer to the selected pixel.
         */
        inline T * get(unsigned int column, unsigned int row, unsigned int channel) { return &m_image[channel][row * m_width + column]; }
        
        // -[ Access to parts of the image ]---------------------------------------------------------------------------------------------------------
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \returns the resulting sub-image.
         */
        inline SubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h) { return SubImage<T>(m_image, y0 * m_width + x0, w, m_width, h, m_number_of_channels); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting sub-image.
         */
        inline SubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, unsigned int c0, unsigned int c1 = 0) { return SubImage<T>(&m_image[c0], y0 * m_width + x0, w, m_width, h, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image with the selected channels.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         */
        inline SubImage<T> subimage(unsigned int c0, unsigned int c1 = 0) { return SubImage<T>(&m_image[c0], 0, m_width, m_width, m_height, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \returns the resulting sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h) const { return ConstantSubImage<T>(m_image, y0 * m_width + x0, w, m_width, h, m_number_of_channels); }
        /** Returns a sub-image of the current image.
         *  \param[in] x0 x-coordinate of the top-left border of the sub-image.
         *  \param[in] y0 y-coordinate of the top-left border of the sub-image.
         *  \param[in] x1 x-coordinate of the bottom-right border of the sub-image.
         *  \param[in] y1 y-coordinate of the bottom-right border of the sub-image.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         *  \returns the resulting sub-image.
         */
        inline ConstantSubImage<T> subimage(unsigned int x0, unsigned int y0, unsigned int w, unsigned int h, unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], y0 * m_width + x0, w, m_width, h, srvMax(c0, c1) - c0 + 1); }
        /** Returns a sub-image with the selected channels.
         *  \param[in] c0 initial channel of the sub-image.
         *  \param[in] c1 final channel of the sub-image. If not set, the resulting sub-image will be a single channel image with the channel selected by parameter c0.
         */
        inline ConstantSubImage<T> subimage(unsigned int c0, unsigned int c1 = 0) const { return ConstantSubImage<T>(&m_image[c0], 0, m_width, m_width, m_height, srvMax(c0, c1) - c0 + 1); }
        
        // -[ Content copy functions ]---------------------------------------------------------------------------------------------------------------
        /** Copies the content of the given sub-image to the sub-image.
         *  \note The function does not check that the geometry of both images is correct, so that,
         *  a segmentation fault can be generated if both sub-images have a different geometry.
         *  \param[in] source source sub-image.
         */
        template <template <class> class IMAGE, class U>
        Image<T>& copy(const IMAGE<U> &source);
        /** This function returns a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedImage(U *&data_pointer) const;
        /** This function returns a RGB image in a single array of the specified type where the pixel values of each channel of the image are interleaved (for instance,
         *  the pixels of a color image is returned in its usual red-green-blue order).
         *  \param[out] data_pointer a pointer with the interleaved image. This pointer is not managed by the object so that it has to be deallocated in order to avoid memory leaks.
         */
        template <class U>
        void getInterleavedRGBImage(U *&data_pointer) const;
        
        // -[ Image modification functions ]---------------------------------------------------------------------------------------------------------
        /// This function flips the image vertically.
        void flipVertical(void);
        /// This function flips the image horizontally.
        void flipHorizontal(void);
        
        // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
        /// This function returns the minimum value of the image.
        template <class U> U minimum(void) const;
        /// This function returns the minimum values per channel of the image.
        template <class U> void minimum(VectorDense<U> &result) const;
        /// This function returns the maximum value of the image.
        template <class U> U maximum(void) const;
        /// This function returns the maximum values per channel of the image.
        template <class U> void maximum(VectorDense<U> &result) const;
        /// This function returns the sum of the pixels of the image.
        template <class U> U sum(void) const;
        /// This function returns the sum of the pixels of the image per channel.
        template <class U> void sum(VectorDense<U> &result) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> U product(void) const;
        /// This function returns the product of the pixels of the image per channel.
        template <class U> void product(VectorDense<U> &result) const;
        
        // -[ Image the content of the current image to the extrema of the given two images ]--------------------------------------------------------
        /** Sets the content of the current sub-image to the minimum values of the two given images.
         *  \param[in] first first image or sub-image.
         *  \param[in] second second image or sub-image.
         */
        template <template <class> class FIRST_IMAGE, template <class> class SECOND_IMAGE, class T1, class T2>
        inline void min(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second) { checkGeometry(*this, first); checkGeometry(*this, second); ImageMinimum(first, second, *this); }
        /** Sets the content of the current sub-image equal to the values of the given image which are lower than the specified value.
         *  \param[in] image image or sub-image.
         *  \param[in] value upper threshold value.
         */
        template <template <class> class IMAGE_TYPE, class T1, class T2>
        inline void min(const IMAGE_TYPE<T1> &image, const T2 &value) { checkGeometry(*this, image); ImageMinimum(image, value, *this); }
        /** Sets the content of the current sub-image to the maximum values of the two given images.
         *  \param[in] first first image or sub-image.
         *  \param[in] second second image or sub-image.
         */
        template <template <class> class FIRST_IMAGE, template <class> class SECOND_IMAGE, class T1, class T2>
        inline void max(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second) { checkGeometry(*this, first); checkGeometry(*this, second); ImageMaximum(first, second, *this); }
        /** Sets the content of the current sub-image equal to the values of the given image which are greter than the specified value.
         *  \param[in] image image or sub-image.
         *  \param[in] value lower threshold value.
         */
        template <template <class> class IMAGE_TYPE, class T1, class T2>
        inline void max(const IMAGE_TYPE<T1> &image, const T2 &value) { checkGeometry(*this, image); ImageMaximum(image, value, *this); }
        /// Inverts the content of the image.
        inline void invert(void) { ImageInvert(*this, *this); }
        
        /// Converts the sub-image by applying the absolute value operator.
        inline Image<T>& abs(void) { ImageAbs(*this, *this); return *this; }
        /// Returns a copy of the sub-image where the absolute value operator has been applied.
        inline Image<T> absCopy(void) const { Image<T> copy_image(m_width, m_height, m_number_of_channels); ImageAbs(*this, copy_image); return copy_image; }
        
        // -[ Load and save images from disk ]-------------------------------------------------------------------------------------------------------
        /** Generates an output message with the different versions of the libraries used to load and save the images.
         *  \param[out] output output stream where the version information will be returned.
         */
        static void versions(std::ostream &output);
        /** Saves the image to disk with the specified format.
         *  \param[in] filename file where the image is going to be stored.
         *  \param[in] image_format format of the image. By default, the extension of the image is used to determine its format.
         *  \param[in] quality compression quality parameter (if applicable to the image format).
         */
        inline void save(const char * filename, IMAGE_FORMATS image_format, int quality = 98) const
        {
            if (m_image != 0) saveFilename(filename, image_format, quality);
            else throw Exception("The image is empty.");
        }
        /** Saves the image to disk with the specified format. The format of the image is determined by the extension of the filename.
         *  \param[in] filename file where the image is going to be stored.
         *  \param[in] quality compression quality parameter (if applicable to the image format).
         */
        inline void save(const char * filename, int quality = 98) const
        {
            if (m_image != 0) saveFilename(filename, IMAGE_FORMAT_EXTENSION, quality);
            else throw Exception("The image is empty.");
        }
        /** Loads the image information from an image file.
         *  \param[in] filename name of the file where the image is stored.
         *  \param[in] image_format format of the disk image. By default, the format of the image is determined by its extension.
         */
        inline void load(const char * filename, IMAGE_FORMATS image_format = IMAGE_FORMAT_EXTENSION) { clear(); loadFilename(filename, image_format); }
    private:
        // -[ Copy content functions ]---------------------------------------------------------------------------------------------------------------
        /// Frees the memory allocated by the image.
        inline void freeImage(void)
        {
            if (m_image != 0)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
                delete [] m_image;
            }
        }
        /// Copies the content from another image to the current image. This function does not allocate the image memory.
        template <template <class> class TIMAGE, class U>
        void copyContent(const TIMAGE<U> &other);
        // -[ Private load/save functions which call the system image libraries ]--------------------------------------------------------------------
        /// Given a filename, this function checks the extension of the image file and calls the appropriated load function.
        void loadFilename(const char * filename, IMAGE_FORMATS image_format);
        /// Given a filename, this function loads the image information from a PNG file.
        void loadPng(const char * filename);
        /// Given a filename, this function loads the image information from a JPEG file.
        void loadJpeg(const char * filename);
        /// Given a filename, this function checks the extension of the image file and calls the appropriated save function.
        void saveFilename(const char * filename, IMAGE_FORMATS image_format, int quality) const;
        /// Given a filename, this function saves the image information from a PNG file.
        void savePng(const char * filename) const;
        /// Given a filename, this function saves the image information from a JPEG file.
        void saveJpeg(const char * filename, int quality) const;
        
        // -[ Image member functions ]---------------------------------------------------------------------------------------------------------------
        /// Image with the different channels stored separately, i.e.\ a 2D array for each channel of the image.
        T * * m_image;
        /// Image width.
        unsigned int m_width;
        /// Image height.
        unsigned int m_height;
        /// Number of channels of the image.
        unsigned int m_number_of_channels;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE CLASS IMPLEMENTATION           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // -[ Constructors, destructor and assignation operator ]----------------------------------------------------------------------------------------
    template <class T>
    Image<T>::Image(void) :
        m_image(0),
        m_width(0),
        m_height(0),
        m_number_of_channels(0)
    {
    }
    
    template <class T>
    Image<T>::Image(unsigned int width, unsigned int height, unsigned int channels) :
        m_image(new T*[channels]),
        m_width(width),
        m_height(height),
        m_number_of_channels(channels)
    {
        for (unsigned int i = 0; i < channels; ++i)
            m_image[i] = new T[width * height];
    }
    
    template <class T>
    Image<T>::Image(unsigned int width, unsigned int height, unsigned int channels, const T &value) :
        m_image(new T*[channels]),
        m_width(width),
        m_height(height),
        m_number_of_channels(channels)
    {
        for (unsigned int i = 0; i < channels; ++i)
        {
            m_image[i] = new T[width * height];
            for (unsigned int j = 0; j < width * height; ++j)
                m_image[i][j] = value;
        }
    }
    
    template <class T>
    Image<T>::Image(unsigned int width, unsigned int height, unsigned int channels, const T * values) :
        m_image(new T*[channels]),
        m_width(width),
        m_height(height),
        m_number_of_channels(channels)
    {
        for (unsigned int i = 0; i < channels; ++i)
        {
            m_image[i] = new T[width * height];
            for (unsigned int j = 0; j < width * height; ++j)
                m_image[i][j] = values[i];
        }
    }
    
    template <class T>
    Image<T>::Image(const char * filename, IMAGE_FORMATS image_format) :
        m_image(0),
        m_width(0),
        m_height(0),
        m_number_of_channels(0)
    {
        loadFilename(filename, image_format);
    }
    
    template <class T>
    Image<T>::Image(const Image<T> &other) :
        m_image((other.m_image != 0)?new T * [other.m_number_of_channels]:0),
        m_width(other.m_width),
        m_height(other.m_height),
        m_number_of_channels(other.m_number_of_channels)
    {
        copyContent(other);
    }
    
    template <class T>
    template <template <class> class IMAGE, class U>
    Image<T>::Image(const IMAGE<U> &other) :
        m_image((other.get() != 0)?new T * [other.getNumberOfChannels()]:0),
        m_width(other.getWidth()),
        m_height(other.getHeight()),
        m_number_of_channels(other.getNumberOfChannels())
    {
        copyContent(other);
    }
    
    template <class T>
    template <template <class> class TIMAGE, class U>
    void Image<T>::copyContent(const TIMAGE<U> &other)
    {
        for (unsigned int c = 0; c < other.getNumberOfChannels(); ++c)
        {
            T * image_ptr;
            
            image_ptr = m_image[c] = new T[other.getWidth() * other.getHeight()];
            for (unsigned int y = 0; y < other.getHeight(); ++y)
            {
                const U * __restrict__ src_ptr = other.get(y, c);
                for (unsigned int x = 0; x < other.getWidth(); ++x, ++image_ptr, ++src_ptr)
                    *image_ptr = (T)*src_ptr;
            }
        }
    }
    
    template <class T>
    Image<T>& Image<T>::operator=(const Image<T> &other)
    {
        if ((void*)this != (void*)&other)
        {
            // Free ...............................................................................
            freeImage();
            
            // Copy the image content .............................................................
            if (other.get() != 0)
            {
                m_image = new T * [other.m_number_of_channels];
                m_width = other.m_width;
                m_height = other.m_height;
                m_number_of_channels = other.m_number_of_channels;
                copyContent(other);
            }
            else
            {
                m_image = 0;
                m_width = 0;
                m_height = 0;
                m_number_of_channels = 0;
            }
        }
        
        return *this;
    }
    
    template <class T>
    template <template <class> class IMAGE, class U>
    Image<T>& Image<T>::operator=(const IMAGE<U> &other)
    {
        // Free ...................................................................................
        freeImage();
        
        // Copy the image content .................................................................
        if (other.get() != 0)
        {
            m_image = new T * [other.getNumberOfChannels()];
            m_width = other.getWidth();
            m_height = other.getHeight();
            m_number_of_channels = other.getNumberOfChannels();
            copyContent(other);
        }
        else
        {
            m_image = 0;
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
        }
        
        return *this;
    }
    
    template <class T>
    void Image<T>::setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels)
    {
        freeImage();
        m_image = new T * [number_of_channels];
        for (unsigned int i = 0; i < number_of_channels; ++i)
            m_image[i] = new T[width * height];
        m_width = width;
        m_height = height;
        m_number_of_channels = number_of_channels;
    }
    
    template <class T>
    void Image<T>::setValue(const T &value)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T * inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x)
                    inner_ptr[x] = value;
            }
        }
    }
    
    template <class T>
    void Image<T>::setValues(const T * values)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T * inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x)
                    inner_ptr[x] = values[c];
            }
        }
    }
    
    //-[ Content copy functions ]----------------------------------------------------------------------------------------------------------------
    template <class T>
    template <template <class> class IMAGE, class U>
    Image<T>& Image<T>::copy(const IMAGE<U> &source)
    {
        for (unsigned int c = 0; c < source.getNumberOfChannels(); ++c)
        {
            for (unsigned int y = 0; y < source.getHeight(); ++y)
            {
                const U *source_ptr = source.get(y, c);
                T *destination_ptr = get(y, c);
                for (unsigned int x = 0; x < source.getWidth(); ++x)
                    destination_ptr[x] = (T)source_ptr[x];
            }
        }
        return *this;
    }
    
    template <class T>
    template <class U>
    void Image<T>::getInterleavedImage(U * &data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * m_number_of_channels];
            for (unsigned int c = 0; c < m_number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, c);
                    for (unsigned int x = 0; x < m_width; ++x)
                        data_pointer[(x + y * m_width) * m_number_of_channels + c] = inner_ptr[x];
                }
            }
        }
        else data_pointer = 0;
    }
    
    template <class T>
    template <class U>
    void Image<T>::getInterleavedRGBImage(U * &data_pointer) const
    {
        if (get() != 0)
        {
            data_pointer = new U[m_width * m_height * 3];
            if (m_number_of_channels == 3)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < m_height; ++y)
                    {
                        const T *inner_ptr = get(y, c);
                        for (unsigned int x = 0; x < m_width; ++x)
                            data_pointer[(x + y * m_width) * m_number_of_channels + c] = (U)inner_ptr[x];
                    }
                }
            }
            else
            {
                for (unsigned int y = 0; y < m_height; ++y)
                {
                    const T *inner_ptr = get(y, 0);
                    for (unsigned int x = 0; x < m_width; ++x)
                    {
                        data_pointer[(x + y * m_width) * 3] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 1] = (U)inner_ptr[x];
                        data_pointer[(x + y * m_width) * 3 + 2] = (U)inner_ptr[x];
                    }
                }
            }
        }
        else data_pointer = 0;
    }
    
    // -[ Image modification functions ]---------------------------------------------------------------------------------------------------------
    template <class T>
    void Image<T>::flipVertical(void)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int h = 0; h < m_height / 2; ++h)
            {
                T *top_ptr = &m_image[c][h * m_width];
                T *bottom_ptr = &m_image[c][(m_height - h - 1) * m_width];
                for (unsigned int w = 0; w < m_width; ++w)
                    srvSwap(top_ptr[w], bottom_ptr[w]);
            }
        }
    }
    
    template <class T>
    void Image<T>::flipHorizontal(void)
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int h = 0; h < m_height; ++h)
            {
                T *left_ptr = &m_image[c][h * m_width];
                T *right_ptr = &m_image[c][h * m_width + m_width - 1];
                for (unsigned int w = 0; w < m_width / 2; ++w, ++left_ptr, --right_ptr)
                    srvSwap(*left_ptr, *right_ptr);
            }
        }
    }
    
    
    // -[ Image content information functions ]--------------------------------------------------------------------------------------------------
    template <class T>
    template <class U> U Image<T>::minimum(void) const
    {
        U minimum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < minimum_value) minimum_value = (U)inner_ptr[x];
            }
        }
        return minimum_value;
    }
    
    template <class T>
    template <class U> void Image<T>::minimum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] < result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U Image<T>::maximum(void) const
    {
        U maximum_value = *get(0, 0, 0);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > maximum_value) maximum_value = (U)inner_ptr[x];
            }
        }
        return maximum_value;
    }
    
    template <class T>
    template <class U> void Image<T>::maximum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = *get(0, 0, c);
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) if ((U)inner_ptr[x] > result[c]) result[c] = (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U Image<T>::sum(void) const
    {
        U value = 0;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value += (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void Image<T>::sum(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 0;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] += (U)inner_ptr[x];
            }
        }
    }
    
    template <class T>
    template <class U> U Image<T>::product(void) const
    {
        U value = 1;
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) value *= (U)inner_ptr[x];
            }
        }
        return value;
    }
    
    template <class T>
    template <class U> void Image<T>::product(VectorDense<U> &result) const
    {
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            result[c] = 1;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                const T * __restrict__ inner_ptr = get(y, c);
                for (unsigned int x = 0; x < m_width; ++x) result[c] *= (U)inner_ptr[x];
            }
        }
    }
    
    /** Returns the image obtained by calculating the absolute value to each pixel of the input image.
     *  \param[in] source_image source image.
     *  \param[in] number_of_threads number of threads used to concurrently process the images. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageArithmetic().
     *  \returns the absolute value image.
     */
    template <template <class> class SOURCE, class ST>
    Image<ST> ImageAbs(const SOURCE<ST> &source_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageArithmetic())
    {
        srv::Image<ST> destination;
        destination.setGeometry(source_image);
        ImageAbs(source_image, destination, number_of_threads);
        return destination;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CALL TO THE IMAGE LIBRARIES TO LOAD  |
    //                   | AND SAVE IMAGES FROM DISK.           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    void Image<T>::versions(std::ostream &output)
    {
        output << "Compiled with libpng " << PNG_LIBPNG_VER_STRING << "; using libpng " << png_libpng_ver << std::endl;
        //// output << "Compiled with zlib " << ZLIB_VERSION << "; using zlib " << zlib_version << std::endl; /* DEPRECATED?? */
    }
    
    template <class T>
    void Image<T>::loadFilename(const char *filename, IMAGE_FORMATS image_format)
    {
        if (image_format == IMAGE_FORMAT_EXTENSION)
        {
            char extension[32];
            getFilenameExtension(filename, extension);
            if (strcasecmp(extension, "png") == 0) loadPng(filename);
            else if ((strcasecmp(extension, "jpeg") == 0) || (strcasecmp(extension, "jpg") == 0)) loadJpeg(filename);
            else throw Exception("Image format not supported. Unknown file extension %s.", extension);
        }
        else if (image_format == IMAGE_FORMAT_PNG) loadPng(filename);
        else if (image_format == IMAGE_FORMAT_JPEG) loadJpeg(filename);
    }
    
    template <class T>
    void Image<T>::loadPng(const char *filename)
    {
        unsigned char header[8];
        png_uint_32 local_width, local_height, i, rowbytes;
        int bit_depth, color_type;
        FILE *image_file;
        png_structp ptr_png;
        png_infop ptr_info;
        //double gamma, display_exponent = 1.0;
        png_bytepp row_pointers = 0;
        
        // -[ Initialize the PNG header structures ]-------------------------------------------------------------------------------------------------
        image_file = fopen(filename, "rb");
        if (!image_file)
            throw Exception("PNG image file '%s' cannot be opened.", filename);
        
        fread(header, 1, 8, image_file);
        if (!png_check_sig(header, 8))
        {
            fclose(image_file);
            throw Exception("PNG image file '%s' has a bad signature.", filename);
        }
        ptr_png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
        if (!ptr_png)
        {
            fclose(image_file);
            throw Exception("Error while loading PNG image file '%s': Out of memory", filename);
        }
        ptr_info = png_create_info_struct(ptr_png);
        if (!ptr_info)
        {
            png_destroy_read_struct(&ptr_png, 0, 0);
            fclose(image_file);
            throw Exception("Error while loading PNG image file '%s': Out of memory", filename);
        }
#if defined(PNG_LIBPNG_VER_MAJOR) && defined(PNG_LIBPNG_VER_MINOR) && ((PNG_LIBPNG_VER_MAJOR > 1) || ((PNG_LIBPNG_VER_MAJOR == 1) && (PNG_LIBPNG_VER_MINOR >= 5)))
        if (setjmp(png_jmpbuf(ptr_png)))
#else
        if (setjmp(ptr_png->jmpbuf))
#endif
        {
            png_destroy_read_struct(&ptr_png, &ptr_info, 0);
            fclose(image_file);
            throw Exception("Error while loading the PNG image file '%s'.", filename);
        }
        png_init_io(ptr_png, image_file);
        png_set_sig_bytes(ptr_png, 8);
        png_read_info(ptr_png, ptr_info);
        
        png_get_IHDR(ptr_png, ptr_info, &local_width, &local_height, &bit_depth, &color_type, 0, 0, 0);
        
        // -[ Get image ]----------------------------------------------------------------------------------------------------------------------------
        if (color_type == PNG_COLOR_TYPE_PALETTE) png_set_expand(ptr_png);
        if ((color_type == PNG_COLOR_TYPE_GRAY) && (bit_depth < 8)) png_set_expand(ptr_png);
        if (png_get_valid(ptr_png, ptr_info, PNG_INFO_tRNS)) png_set_expand(ptr_png);
        if (bit_depth == 16) png_set_strip_16(ptr_png);
        //if ((color_type == PNG_COLOR_TYPE_GRAY) || (color_type == PNG_COLOR_TYPE_GRAY_ALPHA)) png_set_gray_to_rgb(ptr_png);
        //if (png_get_gAMA(ptr_png, ptr_info, &gamma)) png_set_gamma(ptr_png, display_exponent, gamma);
        
        png_read_update_info(ptr_png, ptr_info);
        rowbytes = (png_uint_32)png_get_rowbytes(ptr_png, ptr_info);
        m_number_of_channels = (unsigned int)png_get_channels(ptr_png, ptr_info);
        
        unsigned char *image_data = (unsigned char*)malloc(rowbytes * local_height);
        if (image_data == 0)
        {
            png_destroy_read_struct(&ptr_png, &ptr_info, 0);
            fclose(image_file);
            throw Exception("Error while loading PNG file '%s': Cannot allocate memory for image data.", filename);
        }
        row_pointers = (png_bytepp)malloc(local_height * sizeof(png_bytep));
        if (row_pointers == 0)
        {
            png_destroy_read_struct(&ptr_png, &ptr_info, 0);
            free(image_data);
            fclose(image_file);
            throw Exception("Error while loading PNG file '%s': Cannot allocate memory for image data.", filename);
        }
        for (i = 0; i < local_height; ++i)
            row_pointers[i] = image_data + i * rowbytes;
        png_read_image(ptr_png, row_pointers);
        
        m_width = (unsigned int)local_width;
        m_height = (unsigned int)local_height;
        m_image = new T*[m_number_of_channels];
        for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            m_image[channel] = new T[m_width * m_height];
        
        for (unsigned int h = 0, index = 0; h < local_height; ++h)
        {
            unsigned char *image_ptr = row_pointers[h];
            for (unsigned int w = 0; w < local_width; ++w, ++index)
                for (unsigned int c = 0; c < m_number_of_channels; ++c, ++image_ptr)
                    m_image[c][index] = (T)*image_ptr;
        }
        
        png_read_end(ptr_png, 0);
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        png_destroy_read_struct(&ptr_png, &ptr_info, 0);
        free(row_pointers);
        free(image_data);
        fclose(image_file);
    }
    
    template <class T>
    void Image<T>::loadJpeg(const char *filename)
    {
        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;
        FILE *image_file;
        JSAMPARRAY buffer;
        unsigned int index;
        int row_stride;
        jmp_buf setjmp_buffer;
        
        // -[ Initialize the JPEG file header ]------------------------------------------------------------------------------------------------------
        image_file = fopen(filename, "rb");
        if (!image_file)
            throw Exception("JPEG image file '%s' cannot be opened.", filename);
        
        cinfo.err = jpeg_std_error(&jerr);
        if (setjmp(setjmp_buffer))
        {
            jpeg_destroy_decompress(&cinfo);
            fclose(image_file);
            throw Exception("JPEG image file '%s' cannot be opened. The JPEG code has signaled an error.", filename);
        }
        jpeg_create_decompress(&cinfo);
        jpeg_stdio_src(&cinfo, image_file);
        (void)jpeg_read_header(&cinfo, 1);
        ///// // Debug information of the jpeg header to detect differences between the image which generates valgrind errors/warnings
        ///// // and the image created with GIMP (load and saved as new) which can be opened without any problem.
        ///// std::cout << "[DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG]" << std::endl;
        ///// std::cout << "Jpeg information:" << std::endl;
        ///// std::cout << "------------------------------------------------------------------------------" << std::endl;
        ///// std::cout << "image_width = " << cinfo.image_width << std::endl;
        ///// std::cout << "image_height = " << cinfo.image_height << std::endl;
        ///// std::cout << "num_components = " << cinfo.num_components << std::endl;
        ///// std::cout << "jpeg_color_space = " << cinfo.jpeg_color_space << std::endl;
        ///// std::cout << "out_color_space = " << cinfo.out_color_space << std::endl;
        ///// std::cout << "scale_num = " << cinfo.scale_num << std::endl;
        ///// std::cout << "scale_denom = " << cinfo.scale_denom << std::endl;
        ///// std::cout << "output_gamma = " << cinfo.output_gamma << std::endl;
        ///// std::cout << "buffered_image = " << cinfo.output_gamma << std::endl;
        ///// std::cout << "raw_data_out = " << cinfo.raw_data_out << std::endl;
        ///// std::cout << "dct_method = " << cinfo.dct_method << std::endl;
        ///// std::cout << "do_fancy_upsampling = " << cinfo.do_fancy_upsampling << std::endl;
        ///// std::cout << "do_block_smoothing = " << cinfo.do_block_smoothing << std::endl;
        ///// std::cout << "quantize_colors = " << cinfo.quantize_colors << std::endl;
        ///// std::cout << "dither_mode = " << cinfo.dither_mode << std::endl;
        ///// std::cout << "two_pass_quantize = " << cinfo.two_pass_quantize << std::endl;
        ///// std::cout << "desired_number_of_colors = " << cinfo.desired_number_of_colors << std::endl;
        ///// std::cout << "enable_1pass_quant = " << cinfo.enable_1pass_quant << std::endl;
        ///// std::cout << "enable_external_quant = " << cinfo.enable_external_quant << std::endl;
        ///// std::cout << "enable_2pass_quant = " << cinfo.enable_2pass_quant << std::endl;
        ///// std::cout << "output_width = " << cinfo.output_width << std::endl;
        ///// std::cout << "output_height = " << cinfo.output_height << std::endl;
        ///// std::cout << "out_color_components = " << cinfo.out_color_components << std::endl;
        ///// std::cout << "output_components = " << cinfo.output_components << std::endl;
        ///// std::cout << "rec_outbuf_height = " << cinfo.rec_outbuf_height << std::endl;
        ///// std::cout << "actual_number_of_colors = " << cinfo.actual_number_of_colors << std::endl;
        ///// std::cout << "colormap = " << cinfo.colormap << std::endl;
        ///// std::cout << "output_scanline = " << cinfo.output_scanline << std::endl;
        ///// std::cout << "input_scan_number = " << cinfo.input_scan_number << std::endl;
        ///// std::cout << "input_iMCU_row = " << cinfo.input_iMCU_row << std::endl;
        ///// std::cout << "output_scan_number = " << cinfo.output_scan_number << std::endl;
        ///// std::cout << "output_iMCU_row = " << cinfo.output_iMCU_row << std::endl;
        ///// std::cout << "data_precision = " << cinfo.data_precision << std::endl;
        ///// std::cout << "restart_interval = " << cinfo.restart_interval << std::endl;
        ///// std::cout << "saw_JFIF_marker = " << cinfo.saw_JFIF_marker << std::endl;
        ///// std::cout << "JFIF_major_version = " << (int)cinfo.JFIF_major_version << std::endl;
        ///// std::cout << "JFIF_minor_version = " << (int)cinfo.JFIF_minor_version << std::endl;
        ///// std::cout << "density_unit = " << (int)cinfo.density_unit << std::endl; /* different */
        ///// std::cout << "X_density = " << cinfo.X_density << std::endl; /* different */
        ///// std::cout << "Y_density = " << cinfo.Y_density << std::endl; /* different */
        ///// std::cout << "saw_Adobe_marker = " << cinfo.saw_Adobe_marker << std::endl;
        ///// std::cout << "Adobe_transform = " << (int)cinfo.Adobe_transform << std::endl;
        ///// std::cout << "CCIR601_sampling = " << cinfo.CCIR601_sampling << std::endl;
        ///// std::cout << "max_h_samp_factor = " << cinfo.max_h_samp_factor << std::endl; /* different */
        ///// std::cout << "max_v_samp_factor = " << cinfo.max_v_samp_factor << std::endl; /* different */
        ///// std::cout << "total_iMCU_rows = " << cinfo.total_iMCU_rows << std::endl; /* different */
        ///// std::cout << "comps_in_scan = " << cinfo.comps_in_scan << std::endl;
        ///// std::cout << "MCUs_per_row = " << cinfo.MCUs_per_row << std::endl;
        ///// std::cout << "MCU_rows_in_scan = " << cinfo.MCU_rows_in_scan << std::endl;
        ///// std::cout << "blocks_in_MCU = " << cinfo.blocks_in_MCU << std::endl;
        ///// std::cout << "Ss = " << cinfo.Ss << std::endl;
        ///// std::cout << "Se = " << cinfo.Se << std::endl; /* different */
        ///// std::cout << "Ah = " << cinfo.Ah << std::endl;
        ///// std::cout << "Al = " << cinfo.Al << std::endl; /* different */
        ///// std::cout << "unread_marker = " << cinfo.unread_marker << std::endl;
        ///// std::cout << "------------------------------------------------------------------------------" << std::endl;
        ///// std::cout << "[DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG] - [DEBUG]" << std::endl;
        (void)jpeg_start_decompress(&cinfo);
        
        // -[ Read JPEG image from disc ]------------------------------------------------------------------------------------------------------------
        m_width = cinfo.output_width;
        m_height = cinfo.output_height;
        m_number_of_channels = cinfo.output_components;
        ///// std::cout << "IMAGE GEOMETRY: " << m_width << " " << m_height << " " << m_number_of_channels << std::endl;
        m_image = new T * [m_number_of_channels];
        for (unsigned int i = 0; i < m_number_of_channels; ++i)
            m_image[i] = new T[m_width * m_height];
        row_stride = cinfo.output_width * cinfo.output_components;
        buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr)&cinfo, JPOOL_IMAGE, row_stride + 5, 1);
        for (int i = 0; i < row_stride + 5; ++i)
            buffer[0][i] = 0;
        index = 0;
        while (cinfo.output_scanline < cinfo.output_height)
        {
            (void)jpeg_read_scanlines(&cinfo, buffer, 1);
            for (unsigned int w = 0, jpeg_w = 0; w < cinfo.output_width; ++w, ++index)
                for (unsigned int c = 0; c < m_number_of_channels; ++c, ++jpeg_w)
                    m_image[c][index] = (T)buffer[0][jpeg_w];
        }
        
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        (void)jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);
        fclose(image_file);
    }
    
    template <class T>
    void Image<T>::saveFilename(const char *filename, IMAGE_FORMATS image_format, int quality) const
    {
        if (image_format == IMAGE_FORMAT_EXTENSION)
        {
            char extension[32];
            getFilenameExtension(filename, extension);
            if (strcasecmp(extension, "png") == 0) savePng(filename);
            else if ((strcasecmp(extension, "jpeg") == 0) || (strcasecmp(extension, "jpg") == 0)) saveJpeg(filename, quality);
            else throw Exception("Image format not supported. Unknown file extension %s.", extension);
        }
        else if (image_format == IMAGE_FORMAT_PNG) savePng(filename);
        else if (image_format == IMAGE_FORMAT_JPEG) saveJpeg(filename, quality);
    }
    
    template <class T>
    void Image<T>::savePng(const char *filename) const
    {
        FILE *image_file;
        png_structp ptr_png = 0;
        png_infop ptr_info = 0;
        png_uint_32 rowbytes;
        png_bytepp row_pointers = 0;
        int color_type;
        
        // -[ Initialize the PNG header structures ]-------------------------------------------------------------------------------------------------
        image_file = fopen(filename, "wb");
        if (image_file == 0) throw Exception("File '%s' cannot be created to save the PNG image.", filename);
        ptr_png = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
        if (ptr_png == 0)
        {
            fclose(image_file);
            throw Exception("Error while creating file '%s'. Cannot allocate memory for the header of the PNG file.", filename);
        }
        ptr_info = png_create_info_struct(ptr_png);
        if (ptr_info == 0)
        {
            png_destroy_write_struct(&ptr_png, 0);
            fclose(image_file);
            throw Exception("Error while creating file '%s'. Cannot allocate memory for the header of the PNG file.", filename);
        }
        
        if (setjmp(png_jmpbuf(ptr_png)))
        {
            png_destroy_write_struct(&ptr_png, &ptr_info);
            fclose(image_file);
            throw Exception("Error while creating PNG file '%s'.", filename);
        }
        
        // -[ Allocate the memory to save the image ]------------------------------------------------------------------------------------------------
        if (m_number_of_channels == 1) color_type = PNG_COLOR_TYPE_GRAY;
        else if (m_number_of_channels == 2) color_type = PNG_COLOR_TYPE_GRAY_ALPHA;
        else if (m_number_of_channels == 3) color_type = PNG_COLOR_TYPE_RGB;
        else if (m_number_of_channels == 4) color_type = PNG_COLOR_TYPE_RGB_ALPHA;
        else color_type = PNG_COLOR_TYPE_RGB_ALPHA; // Only four channels will be saved to disk.
        png_set_IHDR(ptr_png, ptr_info, m_width, m_height, 8, color_type, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
        rowbytes = m_width * m_number_of_channels;
        row_pointers = (png_bytepp)png_malloc(ptr_png, m_height * sizeof(png_byte *));
        for (unsigned int y = 0, index = 0; y < m_height; ++y)
        {
            unsigned char *row = (unsigned char*)png_malloc(ptr_png, sizeof(unsigned char) * rowbytes);
            row_pointers[y] = (png_byte *)row;
            for (unsigned int x = 0; x < m_width; ++x, ++index)
                for (unsigned int c = 0; c < srvMin<unsigned int>(4, m_number_of_channels); ++c, ++row)
                    *row = (unsigned char)m_image[c][index];
        }
        
        // -[ Save the PNG image to disk ]-----------------------------------------------------------------------------------------------------------
        png_init_io(ptr_png, image_file);
        png_set_rows(ptr_png, ptr_info, row_pointers);
        png_write_png(ptr_png, ptr_info, PNG_TRANSFORM_IDENTITY, 0);
        
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        for (unsigned int y = 0; y < m_height; ++y) png_free(ptr_png, row_pointers[y]);
        png_free(ptr_png, row_pointers);
        png_destroy_write_struct(&ptr_png, &ptr_info);
        fclose(image_file);
    }
    
    template <class T>
    void Image<T>::saveJpeg(const char *filename, int quality) const
    {
        struct jpeg_compress_struct cinfo;
        struct jpeg_error_mgr jerr;
        FILE *image_file;
        JSAMPROW row_pointer[1];
        int row_stride, index;
        unsigned char *row_values;
        
        // -[ Initialize JPEG header ]---------------------------------------------------------------------------------------------------------------
        image_file = fopen(filename, "wb");
        if (image_file == 0) throw Exception("File '%s' cannot be created to save the JPEG image.", filename);
        cinfo.err = jpeg_std_error(&jerr);
        jpeg_create_compress(&cinfo);
        jpeg_stdio_dest(&cinfo, image_file);
        cinfo.image_width = m_width;
        cinfo.image_height = m_height;
        cinfo.input_components = m_number_of_channels;
        if (m_number_of_channels == 1) cinfo.in_color_space = JCS_GRAYSCALE;
        else if (m_number_of_channels == 3) cinfo.in_color_space = JCS_RGB;
        else cinfo.in_color_space = JCS_UNKNOWN;
        jpeg_set_defaults(&cinfo);
        jpeg_set_quality(&cinfo, quality, 1);
        jpeg_start_compress(&cinfo, 1);
        row_stride = m_width * m_number_of_channels;
        row_values = new unsigned char[row_stride];
        
        // -[ Save JPEG image to disk ]--------------------------------------------------------------------------------------------------------------
        index = 0;
        while (cinfo.next_scanline < cinfo.image_height)
        {
            for (unsigned int w = 0, row_w = 0; w < m_width; ++w, ++index)
                for (unsigned int c = 0; c < m_number_of_channels; ++c, ++row_w)
                    row_values[row_w] = (unsigned char)m_image[c][index];
            row_pointer[0] = row_values;
            (void)jpeg_write_scanlines(&cinfo, row_pointer, 1);
        }
        jpeg_finish_compress(&cinfo);
        
        // -[ Free allocated memory ]----------------------------------------------------------------------------------------------------------------
        delete [] row_values;
        fclose(image_file);
        jpeg_destroy_compress(&cinfo);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

