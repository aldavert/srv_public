// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_DRAW_HEADER_FILE__
#define __SRV_IMAGE_DRAW_HEADER_FILE__

#include <iostream>
#include <list>
#include <set>
#include <limits>
#include "../srv_vector.hpp"
#include "srv_image.hpp"
#ifdef __ENABLE_FREE_TYPE__
    #include <ft2build.h>
    #include FT_FREETYPE_H
    #include FT_SFNT_NAMES_H
    #include FT_TRUETYPE_IDS_H
#endif

namespace srv
{
    class Draw
    {
    public:
        //                   +--------------------------------------+
        //                   | PENCIL CLASS DECLARATION             |
        //                   +--------------------------------------+
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                ##############
        //                                  ##########
        //                                    ######
        //                                      ##
        
        /// The pencil class contains information about the attributes used to draw an object in the image (border size, border and background colors, etc...).
        template <class T = unsigned char>
        class Pencil
        {
        public:
            // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
            /// Default constructor.
            Pencil(void) :
                m_transparent_border(255),
                m_transparent_background(0),
                m_border_size(1),
                m_antialiasing(false)
            {
                m_border_color[0] = m_border_color[1] = m_border_color[2] = m_border_gray = 0;
                m_background_color[0] = m_background_color[1] = m_background_color[2] = m_background_gray = 255;
            }
            /** Parameter constructor where the parameters of the pencil for color images are set.
             *  \param[in] border_color three-dimensional array with the color assigned to the border of the object.
             *  \param[in] border_transparent set the transparency of the border color (from transparent at 0 to opaque at 255).
             *  \param[in] background_color three-dimensional array with the color assigned to the background of the object. For transparent borders, set it to zero (NULL pointer).
             *  \param[in] background_transparent set the transparency of the background color (from transparent at 0 to opaque at 255).
             *  \param[in] border_size size of the border in pixels.
             *  \param[in] antialiasing use of anti-aliasing algorithms to draw the object primitives.
             */
            Pencil(const T *border_color, unsigned char border_transparent, const T *background_color, unsigned char background_transparent, unsigned int border_size, bool antialiasing);
            /** Parameter constructor the pencil parameters for gray-scale images are set.
             *  \param[in] border_value gray-value assigned to the border pixels.
             *  \param[in] border_transparent set the transparency of the border color (from transparent at 0 to opaque at 255).
             *  \param[in] background_value gray-value assigned to the background pixels.
             *  \param[in] background_transparent set the transparency of the background color (from transparent at 0 to opaque at 255).
             *  \param[in] border_size size of the border in pixels.
             *  \param[in] antialiasing use of anti-aliasing algorithms to draw the object primitives.
             */
            Pencil(const T &border_value, unsigned char border_transparent, const T &background_value, unsigned char background_transparent, unsigned int border_size, bool antialiasing);
            
            /** Constructor which initializes the background and border color from the hexadecimal char arrays given as parameters.
             *  \param[in] border_color char array string with the hexadecimal color values of the border.
             *  \param[in] background_color char array string with the hexadecimal color values of the background.
             *  \param[in] border_size size of the border in pixels.
             *  \param[in] antialiasing use of anti-aliasing algorithms to draw the object primitives.
             *  \note use an empty char array to set to border or background color transparent.
             *  \note to set the transparency of the border or background color use a fourth tuple: "ff000080" is red color with a transparence of 128.
             */
            Pencil(const char * border_color, const char * background_color, unsigned int border_size = 1, bool antialiasing = false);
            /** Function which sets the background and border color from the hexadecimal char arrays given as parameters.
             *  \param[in] border_color char array string with the hexadecimal color values of the border.
             *  \param[in] background_color char array string with the hexadecimal color values of the background.
             *  \param[in] border_size size of the border in pixels.
             *  \param[in] antialiasing use of anti-aliasing algorithms to draw the object primitives.
             *  \note use an empty char array to set to border or background color transparent.
             *  \note to set the transparency of the border or background color use a fourth tuple: "ff000080" is red color with a transparence of 128.
             */
            void set(const char * border_color, const char * background_color, unsigned int border_size = 1, bool antialiasing = false);
            
            // -[ Border color ]-------------------------------------------------------------------------------------------------------------------------
            /** Sets the color of the border with the values of char arry with hexadecimal values.
             *  \param[in] border_color char array string with the hexadecimal color values of the border.
             *  \note use an empty char array to set to border color transparent.
             *  \note to set the transparency of the border color use a fourth tuple: "ff000080" is red color with a transparence of 128.
             */
            void setBorderColor(const char * border_color);
            /** Sets the color of the object border.
             *  \param[in] red red value of the object border.
             *  \param[in] green green value of the object border.
             *  \param[in] blue blue value of the object border.
             *  \param[in] transparency set the transparency of the border color (from transparent at 0 to opaque at 255). By default set to 255 (opaque).
             */
            inline void setBorderColor(const T &red, const T &green, const T &blue, unsigned char transparency = 255)
            {
                m_border_color[0] = red;
                m_border_color[1] = green;
                m_border_color[2] = blue;
                m_border_gray = (T)(((double)red + (double)green + (double)blue) / 3.0);
                m_transparent_border = transparency;
            }
            /** Set the gray-scale value of the object border.
             *  \param[in] value gray-scale value set to the object border.
             *  \param[in] transparency set the transparency of the border color (from transparent at 0 to opaque at 255). By default set to 255 (opaque).
             */
            inline void setBorderColor(const T &value, unsigned char transparency = 255) { m_border_color[0] = m_border_color[1] = m_border_color[2] = m_border_gray = value; m_transparent_border = transparency; }
            /// Returns a constant pointer to the array of the border color values.
            inline const T* getBorderColors(void) const { return m_border_color; }
            /// Returns a boolean which indicates if the border is transparent.
            inline unsigned char getBorderTransparency(void) const { return m_transparent_border; }
            /** Sets the transparency of the border.
             *  \param[in] value level of transparency. From transparent at 0 to opaque at 255.
             */
            inline void setBorderTransparency(unsigned char value) { m_transparent_border = value; }
            /// Returns a constant reference to the gray scale value of the border pixels.
            inline const T& getBorderGray(void) const { return m_border_gray; }
            /// Returns a constant reference to the red component of the border color.
            inline const T& getBorderRed(void) const { return m_border_color[0]; }
            /// Returns a constant reference to the green component of the border color.
            inline const T& getBorderGreen(void) const { return m_border_color[1]; }
            /// Returns a constant reference to the blue component of the border color.
            inline const T& getBorderBlue(void) const { return m_border_color[2]; }
            
            // -[ Background color ]---------------------------------------------------------------------------------------------------------------------
            /** Sets the color of the background with the values of char arry with hexadecimal values.
             *  \param[in] background_color char array string with the hexadecimal color values of the background.
             *  \note use an empty char array to set to background color transparent.
             *  \note to set the transparency of the background color use a fourth tuple: "ff000080" is red color with a transparence of 128.
             */
            void setBackgroundColor(const char * background_color);
            /** Sets the color of the object background.
             *  \param[in] red red value of the object background.
             *  \param[in] green green value of the object background.
             *  \param[in] blue blue value of the object background.
             *  \param[in] transparency set the transparency of the background color (from transparent at 0 to opaque at 255). By default set to 0 (transparent).
             */
            inline void setBackgroundColor(const T &red, const T &green, const T &blue, unsigned char transparency = 255)
            {
                m_background_color[0] = red;
                m_background_color[1] = green;
                m_background_color[2] = blue;
                m_background_gray = (T)(((double)red + (double)green + (double)blue) / 3.0);
                m_transparent_background = transparency;
            }
            /** Set the gray-scale value of the object background.
             *  \param[in] value gray-scale value set to the object background.
             *  \param[in] transparency set the transparency of the background color (from transparent at 0 to opaque at 255). By default set to 255 (opaque).
             */
            inline void setBackgroundColor(const T &value, unsigned char transparency = 255) { m_background_color[0] = m_background_color[1] = m_background_color[2] = m_background_gray = value; m_transparent_background = transparency; }
            /// Returns a constant pointer to the array of the background color values.
            inline const T* getBackgroundColor(void) const { return m_background_color; }
            /// Returns a boolean which indicates if the background is transparent.
            inline unsigned char getBackgroundTransparency(void) const { return m_transparent_background; }
            /** Sets the transparency of the background.
             *  \param[in] value level of transparency. From transparent at 0 to opaque at 255.
             */
            inline void setBackgroundTransparency(unsigned char value) { m_transparent_background = value; }
            /// Returns a constant reference to the gray scale value of the background pixels.
            inline const T& getBackgroundGray(void) const { return m_background_gray; }
            /// Returns a constant reference to the red component of the background color.
            inline const T& getBackgroundRed(void) const { return m_background_color[0]; }
            /// Returns a constant reference to the green component of the background color.
            inline const T& getBackgroundGreen(void) const { return m_background_color[1]; }
            /// Returns a constant reference to the blue component of the background color.
            inline const T& getBackgroundBlue(void) const { return m_background_color[2]; }
            
            // -[ Border size ]--------------------------------------------------------------------------------------------------------------------------
            /// Returns the size of the object border.
            inline unsigned int getBorderSize(void) const { return m_border_size; }
            /// Sets the size of the object border.
            inline void setBorderSize(unsigned int border_size) { m_border_size = border_size; }
            
            // -[ Anti-aliasing ]------------------------------------------------------------------------------------------------------------------------
            /// Returns the boolean which indicates if anti-aliasing is used to draw object primitives.
            inline bool useAntialiasing(void) const { return m_antialiasing; }
            /// Sets the anti-aliasing flag to draw object primitives.
            inline void setAntialiasing(bool antialiasing) { m_antialiasing = antialiasing; }
        private:
            /// Converts a char array with two values (in string format) to a integer value.
            int charHex2Int(const char * value) const;
            /// Pixel values of the border for color images.
            T m_border_color[3];
            /// Pixel values of the border for gray-scale images.
            T m_border_gray;
            /// Sets the degree of transparency of object border (from completely transparent at 0 to completely opaque at 255).
            unsigned char m_transparent_border;
            /// Pixel values of the background for color images.
            T m_background_color[3];
            /// Pixel values of the background for gray-scale images.
            T m_background_gray;
            /// Sets the degree of transparency of the object background (from completely transparent at 0 to completely opaque at 255).
            unsigned char m_transparent_background;
            /// Size of the border of the object.
            unsigned int m_border_size;
            /// Flag which enables or disables the use of anti-aliasing to draw object primitives.
            bool m_antialiasing;
        };
        
        //                                      ##
        //                                    ######
        //                                  ##########
        //                                ##############
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                   +--------------------------------------+
        //                   | IMAGE DRAW FUNCTIONS - DECLARATION   |
        //                   +--------------------------------------+
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                ##############
        //                                  ##########
        //                                    ######
        //                                      ##
        
        /** Draws a line in the image between the given origin and destination points.
         *  \param[in] image image where the line drawn.
         *  \param[in] x0 x-coordinate of the initial point.
         *  \param[in] y0 y-coordinate of the initial point.
         *  \param[in] x1 x-coordinate of the final point.
         *  \param[in] y1 y-coordinate of the final point.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <template <class> class IMAGE, class T>
        static inline void Line(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen) { Draw::PrivateLine(image, x0, y0, x1, y1, pen); }
        
        /** Draw the lines which join the given image points coordinates.
         *  \param[in] image image where the line is drawn.
         *  \param[in] x vector with the x-coordinates of the line.
         *  \param[in] y vector with the y-coordinates of the line.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class VT, class VN>
        static inline void Line(IMAGE<T> &image, const VECTOR<VT, VN> &x, const VECTOR<VT, VN> &y, const Pencil<T> &pen) { Draw::PrivateLine(image, x, y, pen); }
        
        /** Draw the lines which join the given image points coordinates.
         *  \param[in] image image where the line is drawn.
         *  \param[in] elements vector with the tuples of the line (x, y)-coordinates.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class TF, class TS, class VN>
        static inline void Line(IMAGE<T> &image, const VECTOR<Tuple<TF, TS>, VN> &elements, const Pencil<T> &pen) { Draw::PrivateLine(image, elements, pen); }
        
        /** Draw a circle in the image.
         *  \param[in] image image where the circle is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circumference.
         *  \param[in] pen \b pencil object with the attributes used to draw the circle.
         */
        template <template <class> class IMAGE, class T>
        static inline void Circle(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen) { Draw::PrivateCircle(image, x, y, radius, pen); }
        
        /** Draw an ellipse in the image.
         *  \param[out] image image where the ellipse is drawn.
         *  \param[in] x x-coordinate of the ellipse's center.
         *  \param[in] y y-coordinate of the ellipse's center.
         *  \param[in] a length of the semi-major axis of the ellipse.
         *  \param[in] b length of the semi-minor axis of the ellipse.
         *  \param[in] orientation orientation in radians of the ellipse.
         *  \param[in] pen \b pencil object with the attributes used to draw the ellipse.
         */
        template <template <class> class IMAGE, class T>
        static inline void Ellipse(IMAGE<T> &image, int x, int y, int a, int b, double orientation, const Pencil<T> &pen) { Draw::PrivateEllipse(image, x, y, a, b, orientation, pen); }
        
        /** Draws a regular polygon which is inscribed within a circle.
         *  \param[in,out] image image where the regular polygon is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circle.
         *  \param[in] angle angle where the initial edge of the polygon is set.
         *  \param[in] number_of_sides number of sides of the circle. The number of sides must be at least 3, otherwise an exception is generated.
         *  \param[in] pen \b pencil object with the attributes used to draw the regular polygon.
         */
        template <template <class> class IMAGE, class T>
        static inline void RegularPolygon(IMAGE<T> &image, int x, int y, int radius, double angle, unsigned int number_of_sides, const Pencil<T> &pen) { Draw::PrivateRegularPolygon(image, x, y, radius, angle, number_of_sides, pen); }
        
        /** Draws a regular polygon which is inscribed within a circle.
         *  \param[in,out] image image where the regular polygon is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circle.
         *  \param[in] number_of_sides number of sides of the circle. The number of sides must be at least 3, otherwise an exception is generated.
         *  \param[in] pen \b pencil object with the attributes used to draw the regular polygon.
         */
        template <template <class> class IMAGE, class T>
        static inline void RegularPolygon(IMAGE<T> &image, int x, int y, int radius, unsigned int number_of_sides, const Pencil<T> &pen) { Draw::PrivateRegularPolygon(image, x, y, radius, 0.0, number_of_sides, pen); }
        
        /** Draws a regular polygon.
         *  \param[in,out] image image where the polygon is drawn.
         *  \param[in] coordinates vector with the coordinates of the polygon vertices.
         *  \param[in] pen \b pencil object with the attributes used to draw the polygon.
         */
        template <template <class> class IMAGE, class T>
        static inline void Polygon(IMAGE<T> &image, const srv::VectorDense<srv::Tuple<int, int> > &coordinates, const Draw::Pencil<T> &pen) { Draw::PrivatePolygon(image, coordinates, pen); }
        
        /** Draws a rectangle in the image.
         *  \param[in] image image where the rectangle is drawn.
         *  \param[in] x0 x-coordinate of the top-left corner of the rectangle.
         *  \param[in] y0 y-coordinate of the top-left corner of the rectangle.
         *  \param[in] x1 x-coordinate of the bottom-right corner of the rectangle.
         *  \param[in] y1 y-coordinate of the bottom-right corner of the rectangle.
         *  \param[in] pen \b pencil object with the attributes used to draw the rectangle.
         */
        template <template <class> class IMAGE, class T>
        static inline void Rectangle(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen) { Draw::PrivateRectangle(image, x0, y0, x1, y1, pen); }
        
        /** Draws a rotated rectangle in the image.
         *  \param[in] image image where the rectangle is drawn.
         *  \param[in] center_x x-coordinate of the center of the rectangle.
         *  \param[in] center_y y-coordinate of the center of the rectangle.
         *  \param[in] width width of the rectangle.
         *  \param[in] height height of the rectangle.
         *  \param[in] orientation orientation of the rectangle in radians.
         *  \param[in] pen \b pencil object with the attributes used to draw the rectangle.
         */
        template <template <class> class IMAGE, class T>
        static inline void Rectangle(IMAGE<T> &image, int center_x, int center_y, int width, int height, double orientation, const Pencil<T> &pen) { Draw::PrivateRotatedRectangle(image, center_x, center_y, width, height, orientation, pen); }
        
        //                                      ##
        //                                    ######
        //                                  ##########
        //                                ##############
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                   +--------------------------------------+
        //                   | DRAWING FUNCTIONS FOR SUB-IMAGE      |
        //                   | WHICH ARE COPIED INSTEAD OF          |
        //                   | REFERENCED                           |
        //                   +--------------------------------------+
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                ##############
        //                                  ##########
        //                                    ######
        //                                      ##
        
        /** Draws a line in the sub-image between the given origin and destination points.
         *  \param[in] image sub-image where the line drawn.
         *  \param[in] x0 x-coordinate of the initial point.
         *  \param[in] y0 y-coordinate of the initial point.
         *  \param[in] x1 x-coordinate of the final point.
         *  \param[in] y1 y-coordinate of the final point.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <class T>
        static inline void Line(SubImage<T> image, int x0, int y0, int x1, int y1, const Pencil<T> &pen = Pencil<T>()) { Draw::PrivateLine(image, x0, y0, x1, y1, pen); }
        
        /** Draw the lines which join the given sub-image points coordinates.
         *  \param[in] image sub-image where the line is drawn.
         *  \param[in] x vector with the x-coordinates of the line.
         *  \param[in] y vector with the y-coordinates of the line.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <class T, template <class, class> class VECTOR, class VT, class VN>
        static inline void Line(SubImage<T> image, const VECTOR<VT, VN> &x, const VECTOR<VT, VN> &y, const Pencil<T> &pen) { Draw::PrivateLine(image, x, y, pen); }
        
        /** Draw the lines which join the given sub-image points coordinates.
         *  \param[in] image sub-image where the line is drawn.
         *  \param[in] elements vector with the tuples of the line (x, y)-coordinates.
         *  \param[in] pen \b pencil object with the attributes used to draw the line.
         */
        template <class T, template <class, class> class VECTOR, class TF, class TS, class VN>
        static inline void Line(SubImage<T> image, const VECTOR<Tuple<TF, TS>, VN> &elements, const Pencil<T> &pen) { Draw::PrivateLine(image, elements, pen); }
        
        /** Draw a circle in the sub-image.
         *  \param[in] image sub-image where the circle is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circumference.
         *  \param[in] pen \b pencil object with the attributes used to draw the circle.
         */
        template <class T>
        static inline void Circle(SubImage<T> image, int x, int y, int radius, const Pencil<T> &pen) { Draw::PrivateCircle(image, x, y, radius, pen); }
        
        /** Draw an ellipse in the image.
         *  \param[out] image sub-image where the ellipse is drawn.
         *  \param[in] x x-coordinate of the ellipse's center.
         *  \param[in] y y-coordinate of the ellipse's center.
         *  \param[in] a length of the semi-major axis of the ellipse.
         *  \param[in] b length of the semi-minor axis of the ellipse.
         *  \param[in] orientation orientation in radians of the ellipse.
         *  \param[in] pen \b pencil object with the attributes used to draw the ellipse.
         */
        template <class T>
        static inline void Ellipse(SubImage<T> image, int x, int y, int a, int b, double orientation, const Pencil<T> &pen) { Draw::PrivateEllipse(image, x, y, a, b, orientation, pen); }
        
        /** Draws a regular polygon which is inscribed within a circle.
         *  \param[in] image sub-image where the regular polygon is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circle.
         *  \param[in] angle angle where the initial edge of the polygon is set.
         *  \param[in] number_of_sides number of sides of the circle. The number of sides must be at least 3, otherwise an exception is generated.
         *  \param[in] pen \b pencil object with the attributes used to draw the regular polygon.
         */
        template <class T>
        static inline void RegularPolygon(SubImage<T> image, int x, int y, int radius, double angle, unsigned int number_of_sides, const Pencil<T> &pen) { Draw::PrivateRegularPolygon(image, x, y, radius, angle, number_of_sides, pen); }
        
        /** Draws a regular polygon which is inscribed within a circle.
         *  \param[in] image sub-image where the regular polygon is drawn.
         *  \param[in] x x-coordinate of the circle's center.
         *  \param[in] y y-coordinate of the circle's center.
         *  \param[in] radius radius of the circle.
         *  \param[in] number_of_sides number of sides of the circle. The number of sides must be at least 3, otherwise an exception is generated.
         *  \param[in] pen \b pencil object with the attributes used to draw the regular polygon.
         */
        template <class T>
        static inline void RegularPolygon(SubImage<T> image, int x, int y, int radius, unsigned int number_of_sides, const Pencil<T> &pen) { Draw::PrivateRegularPolygon(image, x, y, radius, 0.0, number_of_sides, pen); }
        
        /** Draws a regular polygon.
         *  \param[in,out] image sub-image where the polygon is drawn.
         *  \param[in] coordinates vector with the coordinates of the polygon vertices.
         *  \param[in] pen \b pencil object with the attributes used to draw the polygon.
         */
        template <template <class> class IMAGE, class T>
        static inline void Polygon(SubImage<T> image, const srv::VectorDense<srv::Tuple<int, int> > &coordinates, const Draw::Pencil<T> &pen) { Draw::PrivatePolygon(image, coordinates, pen); }
        
        /** Draws a rectangle in the sub-image.
         *  \param[in] image sub-image where the rectangle is drawn.
         *  \param[in] x0 x-coordinate of the top-left corner of the rectangle.
         *  \param[in] y0 y-coordinate of the top-left corner of the rectangle.
         *  \param[in] x1 x-coordinate of the bottom-right corner of the rectangle.
         *  \param[in] y1 y-coordinate of the bottom-right corner of the rectangle.
         *  \param[in] pen \b pencil object with the attributes used to draw the rectangle.
         */
        template <class T>
        static inline void Rectangle(SubImage<T> image, int x0, int y0, int x1, int y1, const Pencil<T> &pen) { Draw::PrivateRectangle(image, x0, y0, x1, y1, pen); }
        
        /** Draws a rotated rectangle in the sub-image.
         *  \param[in] image image where the rectangle is drawn.
         *  \param[in] center_x x-coordinate of the center of the rectangle.
         *  \param[in] center_y y-coordinate of the center of the rectangle.
         *  \param[in] width width of the rectangle.
         *  \param[in] height height of the rectangle.
         *  \param[in] orientation orientation of the rectangle in radians.
         *  \param[in] pen \b pencil object with the attributes used to draw the rectangle.
         */
        template <template <class> class IMAGE, class T>
        static inline void Rectangle(SubImage<T> &image, int center_x, int center_y, int width, int height, double orientation, const Pencil<T> &pen) { Draw::PrivateRotatedRectangle(image, center_x, center_y, width, height, orientation, pen); }
        
        //                                      ##
        //                                    ######
        //                                  ##########
        //                                ##############
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                   +--------------------------------------+
        //                   | FONT CLASS AND TEXT DRAW FUNCTIONS   |
        //                   +--------------------------------------+
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                ##############
        //                                  ##########
        //                                    ######
        //                                      ##
        
        enum FONT_TYPE { TINY_FONT_5x7 = 19201, SMALL_FONT_7x14 };
        
        /// Base font class.
        class Font
        {
        public:
            /// Default constructor.
            Font(void);
            /** Constructor which uses in-build fonts.
             *  \param[in] font_type identifier of the font.
             *  \param[in] break_lines flag which indicates that the break-line character (character value equal to 13) would break the
             *                         line when true or will be draw when false. By default, it is set to false.
             */
            Font(FONT_TYPE font_type, bool break_lines = false);
#ifdef __ENABLE_FREE_TYPE__
            /** Constructor which load the font information from a TTF file using the FreeType2 library.
             *  \param[in] filename file containing the TTF font.
             *  \param[in] font_size height in pixels of the font.
             *  \param[in] break_lines flag which indicates that the break-line character (character value equal to 13) would break the
             *                         line when true or will be draw when false. By default, it is set to false.
             */
            Font(const char * filename, unsigned int font_size, bool break_line = false);
#endif
            /// Copy constructor.
            Font(const Font &other);
            /// Destructor.
            ~Font(void);
            /// Assignation operator.
            Font& operator=(const Font &other);
            /// Returns the width of the character.
            inline unsigned int getWidth(unsigned char character) const { return m_width[character]; }
            /// Returns the height of the character.
            inline unsigned int getHeight(unsigned char character) const { return m_height[character]; }
            /// Returns the number of pixels which has to advance the pen to draw the next character.
            inline unsigned int getAdvance(unsigned char character) const { return m_advance[character]; }
            /// Returns the displacement of the pen in the x-direction to reach the relative location the selected character.
            inline int getOffsetX(unsigned char character) const { return m_offset_x[character]; }
            /// Returns the displacement of the pen in the y-direction to reach the relative location the selected character.
            inline int getOffsetY(unsigned char character) const { return m_offset_y[character]; }
            /// Returns the separation between lines.
            inline unsigned int getLineHeight(void) const { return m_line_height; }
            /// Returns the break line flag indicator.
            inline bool getBreakLine(void) const { return m_break_line; }
            /// Sets the break line flag.
            inline void setBreakLine(bool break_line) { m_break_line = break_line; }
            /// Returns a constant pointer to the pixels boolean flags which activate or deactivate image pixels to draw the specified character.
            inline const unsigned char * getPixels(unsigned char character) const { return m_characters[character]; }
            /** This function calculates the coordinates of the box which surround given text.
             *  \param[in] text text string.
             *  \param[in] x0 X-coordinate of the top-left corner of the box.
             *  \param[in] y0 Y-coordinate of the top-left corner of the box.
             *  \param[in] x1 X-coordinate of the bottom-right corner of the box.
             *  \param[in] y1 Y-coordinate of the bottom-right corner of the box.
             *  \param[in] y_offset white space in pixels at the top of the box added by the draw function.
             */
            void calculateBox(const char * text, int &x0, int &y0, int &x1, int &y1, int &y_offset) const;
            /** Calculates the bounding box of each character of the text. 
             *  \param[in] text text string.
             *  \param[in] x0 array with the X-coordinate of the top-left corner of the box at each character.
             *  \param[in] y0 array with the Y-coordinate of the top-left corner of the box at each character.
             *  \param[in] x1 array with the X-coordinate of the bottom-right corner of the box at each character.
             *  \param[in] y1 array with the Y-coordinate of the bottom-right corner of the box at each character.
             *  \returns number of characters in the text array.
             */
            unsigned int calculateBox(const char * text, int * x0, int * y0, int * x1, int * y1) const;
            /** Calculates the average and maximum height of the alphanumeric characters.
             *  \param[out] avg_height average height of the characters.
             *  \param[out] max_height maximum height of the characters.
             */
            void calculateHeight(int &avg_height, int &max_height) const;
#ifdef __ENABLE_FREE_TYPE__
            inline const std::string& getFontName(void) const { return m_font_name; }
#endif
        protected:
            // -[ Auxiliary functions ]------------------------------------------------------------
#ifdef __ENABLE_FREE_TYPE__
            /** Function which handles the errors generated by the Free Type library and generates the correct exception message for each error.
             *  \param[in] error index of the error.
             */
            void freeTypeErrorHandle(int error, const char * filename, FT_Library &library) const;
#endif
            
            // -[ Member variables ]---------------------------------------------------------------
            /// Width of the character in pixels.
            unsigned int m_width[256];
            /// Height of the character in pixels.
            unsigned int m_height[256];
            /// Width of the spaces in pixels.
            unsigned int m_advance[256];
            /// Displacement of the pen in the x-direction to reach the relative location of each character.
            int m_offset_x[256];
            /// Displacement of the pen in the y-direction to reach the relative location of each character.
            int m_offset_y[256];
            /// Separation between lines.
            unsigned int m_line_height;
            /// Break line indicator.
            bool m_break_line;
            /// Binary pattern used to draw the characters in the images.
            unsigned char * m_characters[256];
#ifdef __ENABLE_FREE_TYPE__
            /// String which stores the name of the font.
            std::string m_font_name;
#endif
        };
        
        template <template <class> class IMAGE, class T>
        static void Text(IMAGE<T> &image, int x, int y, const Pencil<T> &pen, const Font &font, const char *format, ...);
        template <class T>
        static void Text(SubImage<T> image, int x, int y, const Pencil<T> &pen, const Font &font, const char *format, ...);
        
    private:
        /// Auxiliary function which draw a line in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateLine(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen = Pencil<T>());
        
        /// Auxiliary function which draw a line in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class VT, class VN>
        static void PrivateLine(IMAGE<T> &image, const VECTOR<VT, VN> &x, const VECTOR<VT, VN> &y, const Pencil<T> &pen);
        
        /// Auxiliary function which draw a line in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class TF, class TS, class VN>
        static void PrivateLine(IMAGE<T> &image, const VECTOR<Tuple<TF, TS>, VN> &elements, const Pencil<T> &pen);
        
        /// Auxiliary function which draw a circle in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateCircle(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        /// Auxiliary function which draw an ellipse in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateEllipse(IMAGE<T> &image, int x, int y, int a, int b, double orientation, const Pencil<T> &pen);
        
        /// Auxiliary function which draw a regular polygon in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateRegularPolygon(IMAGE<T> &image, int x, int y, int radius, double angle, unsigned int number_of_sides, const Pencil<T> &pen);
        
        /// Auxiliary function which draw a polygon in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivatePolygon(IMAGE<T> &image, const srv::VectorDense<srv::Tuple<int, int> > &coordinates, const Draw::Pencil<T> &pen);
        
        /// Auxiliary function which draw a rectangle in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateRectangle(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen);
        
        /// Auxiliary function which draw a rotated rectangle in the image. Defined as private to call it both from general image reference and sub-image copy functions.
        template <template <class> class IMAGE, class T>
        static void PrivateRotatedRectangle(IMAGE<T> &image, int center_x, int center_y, int width, int height, double orientation, const Pencil<T> &pen);
        
        /// Auxiliary function which returns a list of points of a line calculated using the Bresenham line algorithm.
        static void LinePoints(int x0, int y0, int x1, int y1, int width, int height, bool connectivity4, std::list<Tuple<int, int> > &points);
        
        /// Auxiliary function which returns the point coordinates of a circle.
        static void CirclePoints(int x, int y, int radius, std::list<Tuple<double, double> > &points);
        
        /// Auxiliary function which returns a list of points of a line calculated using the Bresenham line algorithm without filtering points which fall outside the image.
        static void LinePoints(int x0, int y0, int x1, int y1, bool connectivity4, std::list<Tuple<int, int> > &points);
        
        /// Cuts the coordinates, so that the points are within the image.
        static bool CutLineCoordinates(int &x0, int &y0, int &x1, int &y1, int width, int height);
        
        /// Auxiliary function which draws the background of the circle.
        template <template <class> class IMAGE, class T>
        static void CircleBackground(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        /// Auxiliary function which draws a simple aliased circumference.
        template <template <class> class IMAGE, class T>
        static void Circumference(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        /// Auxiliary function which draws a simple anti-aliased circumference.
        template <template <class> class IMAGE, class T>
        static void CircumferenceAntialiased(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        /// Auxiliary function which draws an aliased border circle.
        template <template <class> class IMAGE, class T>
        static void CircleBorder(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        /// Auxiliary function which draws an anti-aliased border circle.
        template <template <class> class IMAGE, class T>
        static void CircleAntialiasedBorder(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen);
        
        //                                      ##
        //                                    ######
        //                                  ##########
        //                                ##############
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
        //                                    ######
    };
    
    //                   +--------------------------------------+
    //                   | PEN CLASS IMPLEMENTATION             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    Draw::Pencil<T>::Pencil(const T *border_color, unsigned char border_transparent, const T *background_color, unsigned char background_transparent, unsigned int border_size, bool antialiasing) :
        m_transparent_border(border_transparent),
        m_transparent_background(background_transparent),
        m_border_size(border_size),
        m_antialiasing(antialiasing)
    {
        if (border_color != 0)
        {
            m_border_color[0] = border_color[0];
            m_border_color[1] = border_color[1];
            m_border_color[2] = border_color[2];
            m_border_gray = (T)(((double)border_color[0] + (double)border_color[1] + (double)border_color[2]) / 3.0);
        }
        else m_border_color[0] = m_border_color[1] = m_border_color[2] = 0;
        if (background_color != 0)
        {
            m_background_color[0] = background_color[0];
            m_background_color[1] = background_color[1];
            m_background_color[2] = background_color[2];
            m_background_gray = (T)(((double)background_color[0] + (double)background_color[1] + (double)background_color[2]) / 3.0);
        }
        else m_background_color[0] = m_background_color[1] = m_background_color[2] = 255;
    }
    
    template <class T>
    Draw::Pencil<T>::Pencil(const T &border_value, unsigned char border_transparent, const T &background_value, unsigned char background_transparent, unsigned int border_size, bool antialiasing) :
        m_border_gray(border_value),
        m_transparent_border(border_transparent),
        m_background_gray(background_value),
        m_transparent_background(background_transparent),
        m_border_size(border_size),
        m_antialiasing(antialiasing)
    {
        m_border_color[0] = m_border_color[1] = m_border_color[2] = border_value;
        m_background_color[0] = m_background_color[1] = m_background_color[2] = background_value;
    }
    
    template <class T>
    int Draw::Pencil<T>::charHex2Int(const char * value) const
    {
        int first = 0, second = 0;
        if      ((value[0] >= '0') && (value[0] <= '9')) first =  value[0] - '0';
        else if ((value[0] >= 'a') && (value[0] <= 'f')) first = (value[0] - 'a') + 10;
        else if ((value[0] >= 'A') && (value[0] <= 'F')) first = (value[0] - 'A') + 10;
        else throw Exception("Character value '%c' is not an hexadecimal value.", value[0]);
        if      ((value[1] >= '0') && (value[1] <= '9')) second =  value[1] - '0';
        else if ((value[1] >= 'a') && (value[1] <= 'f')) second = (value[1] - 'a') + 10;
        else if ((value[1] >= 'A') && (value[1] <= 'F')) second = (value[1] - 'A') + 10;
        else throw Exception("Character value '%c' is not an hexadecimal value.", value[1]);
        return (first << 4) | second;
    }
    
    template <class T>
    Draw::Pencil<T>::Pencil(const char * border_color, const char * background_color, unsigned int border_size, bool antialiasing) :
        m_border_size(border_size),
        m_antialiasing(antialiasing)
    {
        setBorderColor(border_color);
        setBackgroundColor(background_color);
    }
    
    template <class T>
    void Draw::Pencil<T>::set(const char * border_color, const char * background_color, unsigned int border_size, bool antialiasing)
    {
        setBorderColor(border_color);
        setBackgroundColor(background_color);
        m_border_size = border_size;
        m_antialiasing = antialiasing;
    }
    
    template <class T>
    void Draw::Pencil<T>::setBorderColor(const char * border_color)
    {
        unsigned int length;
        
        if (border_color == 0) length = 0;
        else for (length = 0; (border_color[length] != '\0') && (length < 8); ++length);
        if (length == 0)
        {
            m_border_color[0] = m_border_color[1] = m_border_color[2] = m_border_gray = 0;
            m_transparent_border = 0;
        }
        else if (length == 6)
        {
            m_border_color[0] = (T)charHex2Int(border_color);
            m_border_color[1] = (T)charHex2Int(border_color + 2);
            m_border_color[2] = (T)charHex2Int(border_color + 4);
            m_border_gray = (T)(((int)m_border_color[0] + (int)m_border_color[1] + (int)m_border_color[2]) / 3);
            m_transparent_border = 255;
        }
        else if (length == 8)
        {
            m_border_color[0] = (T)charHex2Int(border_color);
            m_border_color[1] = (T)charHex2Int(border_color + 2);
            m_border_color[2] = (T)charHex2Int(border_color + 4);
            m_border_gray = (T)(((int)m_border_color[0] + (int)m_border_color[1] + (int)m_border_color[2]) / 3);
            m_transparent_border = (unsigned char)charHex2Int(border_color + 6);
        }
        else throw Exception("Incorrect color string '%s'.", border_color);
    }
    
    template <class T>
    void Draw::Pencil<T>::setBackgroundColor(const char * background_color)
    {
        unsigned int length;
        
        if (background_color == 0) length = 0;
        else for (length = 0; (background_color[length] != '\0') && (length < 8); ++length);
        if (length == 0)
        {
            m_background_color[0] = m_background_color[1] = m_background_color[2] = m_background_gray = 0;
            m_transparent_background = 0;
        }
        else if (length == 6)
        {
            m_background_color[0] = (T)charHex2Int(background_color);
            m_background_color[1] = (T)charHex2Int(background_color + 2);
            m_background_color[2] = (T)charHex2Int(background_color + 4);
            m_background_gray = (T)(((int)m_background_color[0] + (int)m_background_color[1] + (int)m_background_color[2]) / 3);
            m_transparent_background = 255;
        }
        else if (length == 8)
        {
            m_background_color[0] = (T)charHex2Int(background_color);
            m_background_color[1] = (T)charHex2Int(background_color + 2);
            m_background_color[2] = (T)charHex2Int(background_color + 4);
            m_background_gray = (T)(((int)m_background_color[0] + (int)m_background_color[1] + (int)m_background_color[2]) / 3);
            m_transparent_background = (unsigned char)charHex2Int(background_color + 6);
        }
        else throw Exception("Incorrect color string '%s'.", background_color);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE DRAW FUNCTIONS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ IMAGE LINE FUNCTIONS ]=====================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateLine(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen)
    {
        if ((pen.getBorderTransparency() > 0) && (pen.getBorderSize() > 0)) // If the border color is not transparent.
        {
            std::list<Tuple<int, int> > points;
            double gradient;
            bool reversed;
            
            // Set the line coordinates inside the image.
            if (!Draw::CutLineCoordinates(x0, y0, x1, y1, (int)image.getWidth(), (int)image.getHeight()))
                return; // The line is outside the image frame, stop.
            
            // Calculate the line point coordinates.
            Draw::LinePoints(x0, y0, x1, y1, image.getWidth(), image.getHeight(), true, points);
            // Calculate line variables.
            reversed = srvAbs(x1 - x0) < srvAbs(y1 - y0);
            gradient = (double)(x1 - x0) / (double)(y1 - y0);
            
            // Paint the image.
            if (image.getNumberOfChannels() != 3)
            {
                const double w0 = (double)pen.getBorderTransparency() / 255.0;
                const double w1 = 1.0 - w0;
                const int width = (int)image.getWidth();
                const int height = (int)image.getHeight();
                if (reversed)
                {
                    double angle = atan2(x1 - x0, y1 - y0);
                    int border_size = (int)((double)pen.getBorderSize() / fabs(cos(angle)));
                    for (std::list<Tuple<int, int> >::const_iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
                    {
                        bool min_border, max_border;
                        int xmin, xmax, y;
                        
                        y = begin->getSecond();
                        xmin = begin->getFirst() - border_size;
                        xmax = begin->getFirst() + border_size;
                        
                        if (xmin < 0) { xmin = 0; min_border = false; }
                        else if (xmin > width) { xmin = width - 1; min_border = false; }
                        else min_border = pen.useAntialiasing();
                        if (xmax < 0) { xmax = 0; max_border = false; }
                        else if (xmax > width) { xmax = width - 1; max_border = false; }
                        else max_border = pen.useAntialiasing();
                        
                        // Estimate the real 'x' at the current location.
                        double weight = (double)(begin->getSecond() - y0) * gradient + x0;
                        // The weight is calculated by the difference between the current location and the real location.
                        weight = weight - (double)begin->getFirst();
                        
                        if (weight < 0)
                        {
                            double wa0, wa1;
                            int x;
                            
                            weight += 1;
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                x = xmin;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                x = xmax - 1;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                                --xmax;
                            }
                        }
                        else
                        {
                            double wa0, wa1;
                            int x;
                            
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                x = xmin + 1;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                                ++xmin;
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                x = xmax;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                            }
                        }
                        for (int x = xmin + 1; x <= xmax - 1; ++x)
                            for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                *image.get(x, y, cc) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, cc));
                    }
                }
                else
                {
                    double angle = atan2(y1 - y0, x1 - x0);
                    int border_size = (int)((double)pen.getBorderSize() / fabs(cos(angle)));
                    for (std::list<Tuple<int, int> >::const_iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
                    {
                        bool min_border, max_border;
                        int x, ymin, ymax;
                        
                        x = begin->getFirst();
                        ymin = begin->getSecond() - border_size;
                        ymax = begin->getSecond() + border_size;
                        
                        if (ymin < 0) { ymin = 0; min_border = false; }
                        else if (ymin > height) { ymin = height - 1; min_border = false; }
                        else min_border = pen.useAntialiasing();
                        if (ymax < 0) { ymax = 0; max_border = false; }
                        else if (ymax > height) { ymax = height - 1; max_border = false; }
                        else max_border = pen.useAntialiasing();
                        
                        // Estimate the real 'y' at the current location.
                        double weight = (double)(begin->getFirst() - x0) / gradient + y0;
                        // The weight is calculated by the difference between the current location and the real location.
                        weight = weight - (double)begin->getSecond();
                        
                        if (weight < 0)
                        {
                            double wa0, wa1;
                            int y;
                            
                            weight += 1;
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                y = ymin;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                y = ymax - 1;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                                --ymax;
                            }
                        }
                        else
                        {
                            double wa0, wa1;
                            int y;
                            
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                y = ymin + 1;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                                ++ymin;
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                y = ymax;
                                for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                    *image.get(x, y, cc) = (T)(wa0 * (double)pen.getBorderGray() + wa1 * (double)*image.get(x, y, cc));
                            }
                        }
                        
                        for (int y = ymin + 1; y <= ymax - 1; ++y)
                            for (unsigned int cc = 0; cc < image.getNumberOfChannels(); ++cc)
                                *image.get(x, y, cc) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, cc));
                    }
                }
            }
            else
            {
                const double w0 = (double)pen.getBorderTransparency() / 255.0;
                const double w1 = 1.0 - w0;
                const int width = (int)image.getWidth();
                const int height = (int)image.getHeight();
                if (reversed)
                {
                    double angle = atan2(x1 - x0, y1 - y0);
                    int border_size = (int)((double)pen.getBorderSize() / fabs(cos(angle)));
                    for (std::list<Tuple<int, int> >::const_iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
                    {
                        bool min_border, max_border;
                        int xmin, xmax, y;
                        
                        y = begin->getSecond();
                        xmin = begin->getFirst() - border_size;
                        xmax = begin->getFirst() + border_size;
                        
                        if (xmin < 0) { xmin = 0; min_border = false; }
                        else if (xmin >= width) { xmin = width - 1; min_border = false; }
                        else min_border = pen.useAntialiasing();
                        if (xmax < 0) { xmax = 0; max_border = false; }
                        else if (xmax >= width) { xmax = width - 1; max_border = false; }
                        else max_border = pen.useAntialiasing();
                        
                        // Estimate the real 'x' at the current location.
                        double weight = (double)(begin->getSecond() - y0) * gradient + x0;
                        // The weight is calculated by the difference between the current location and the real location.
                        weight = weight - (double)begin->getFirst();
                        
                        if (weight < 0)
                        {
                            double wa0, wa1;
                            int x;
                            
                            weight += 1;
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                x = xmin;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                x = xmax - 1;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                                --xmax;
                            }
                        }
                        else
                        {
                            double wa0, wa1;
                            int x;
                            
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                x = xmin + 1;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                                ++xmin;
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                x = xmax;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                            }
                        }
                        for (int x = xmin + 1; x <= xmax - 1; ++x)
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
                else
                {
                    double angle = atan2(y1 - y0, x1 - x0);
                    int border_size = (int)((double)pen.getBorderSize() / fabs(cos(angle)));
                    for (std::list<Tuple<int, int> >::const_iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
                    {
                        bool min_border, max_border;
                        int x, ymin, ymax;
                        
                        x = begin->getFirst();
                        ymin = begin->getSecond() - border_size;
                        ymax = begin->getSecond() + border_size;
                        
                        if (ymin < 0) { ymin = 0; min_border = false; }
                        else if (ymin >= height) { ymin = height - 1; min_border = false; }
                        else min_border = pen.useAntialiasing();
                        if (ymax < 0) { ymax = 0; max_border = false; }
                        else if (ymax >= height) { ymax = height - 1; max_border = false; }
                        else max_border = pen.useAntialiasing();
                        
                        // Estimate the real 'y' at the current location.
                        double weight = (double)(begin->getFirst() - x0) / gradient + y0;
                        // The weight is calculated by the difference between the current location and the real location.
                        weight = weight - (double)begin->getSecond();
                        
                        if (weight < 0)
                        {
                            double wa0, wa1;
                            int y;
                            
                            weight += 1;
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                y = ymin;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                y = ymax - 1;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                                --ymax;
                            }
                        }
                        else
                        {
                            double wa0, wa1;
                            int y;
                            
                            if (min_border)
                            {
                                wa0 = w0 * (1.0 - weight);
                                wa1 = 1.0 - wa0;
                                y = ymin + 1;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                                ++ymin;
                            }
                            if (max_border)
                            {
                                wa0 = w0 * (weight);
                                wa1 = 1.0 - wa0;
                                y = ymax;
                                *image.get(x, y, 0) = (T)(wa0 * (double)pen.getBorderRed() + wa1 * (double)*image.get(x, y, 0));
                                *image.get(x, y, 1) = (T)(wa0 * (double)pen.getBorderGreen() + wa1 * (double)*image.get(x, y, 1));
                                *image.get(x, y, 2) = (T)(wa0 * (double)pen.getBorderBlue() + wa1 * (double)*image.get(x, y, 2));
                            }
                        }
                        
                        for (int y = ymin + 1; y <= ymax - 1; ++y)
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
            }
        }
    }
    
    template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class VT, class VN>
    void Draw::PrivateLine(IMAGE<T> &image, const VECTOR<VT, VN> &x, const VECTOR<VT, VN> &y, const Pencil<T> &pen)
    {
        if (x.size() != y.size()) throw Exception("X-coordinates vector has %d elements while Y-coordinates vector has %d elements.", x.size(), y.size());
        if (x.size() > 0)
            for (unsigned int i = 0; i < x.size() - 1; ++i)
                Draw::Line(image, (int)x[i], (int)y[i], (int)x[i + 1], (int)y[i + 1], pen);
    }
    
    template <template <class> class IMAGE, class T, template <class, class> class VECTOR, class TF, class TS, class VN>
    void Draw::PrivateLine(IMAGE<T> &image, const VECTOR<Tuple<TF, TS>, VN> &elements, const Pencil<T> &pen)
    {
        if (elements.size() > 0)
            for (unsigned int i = 0; i < elements.size() - 1; ++i)
                Draw::Line(image, (int)elements[i].getFirst(), (int)elements[i].getSecond(), (int)elements[i + 1].getFirst(), (int)elements[i + 1].getSecond(), pen);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ IMAGE CIRCLE FUNCTIONS ]===================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateCircle(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen)
    {
        if (pen.getBackgroundTransparency() > 0) Draw::CircleBackground(image, x, y, radius, pen);
        
        if (pen.getBorderTransparency() > 0)
        {
            if (pen.getBorderSize() == 1)
            {
                if (pen.useAntialiasing()) CircumferenceAntialiased(image, x, y, radius, pen);
                else Circumference(image, x, y, radius, pen);
            }
            else
            {
                if (pen.useAntialiasing()) CircleAntialiasedBorder(image, x, y, radius, pen);
                else CircleBorder(image, x, y, radius, pen);
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ IMAGE ELLIPSE FUNCTIONS ]==================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateEllipse(IMAGE<T> &image, int x, int y, int a, int b, double orientation, const Pencil<T> &pen)
    {
        VectorDense<std::set<int> > boundaries(image.getHeight());
        VectorDense<T *> current_ptr(image.getNumberOfChannels());
        std::list<Tuple<int, int> > points;
        int x0, y0, x1, y1, current_x, current_y;
        const double cos_or = cos(orientation);
        const double sin_or = sin(orientation);
        const double w0_bac = (double)pen.getBackgroundTransparency() / 255.0;
        const double w0_bor = (double)pen.getBorderTransparency() / 255.0;
        const double w1_bac = 1.0 - w0_bac;
        const double w1_bor = 1.0 - w0_bor;
        
        // 1) Get the ellipse boundaries in the image.
        x0 = (int)round( cos_or *  a - sin_or *  b) + x;
        y0 = (int)round( sin_or *  a + cos_or *  b) + y;
        x1 = (int)round( cos_or * -a - sin_or *  b) + x;
        y1 = (int)round( sin_or * -a + cos_or *  b) + y;
        LinePoints(x0, y0, x1, y1, true, points);
        for (std::list<Tuple<int, int> >::iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
        {
            current_x = begin->getFirst();
            current_y = begin->getSecond();
            if (current_x < 0) current_x = 0;
            if (current_x > (int)image.getWidth() - 1) current_x = (int)image.getWidth() - 1;
            if (current_y < 0) current_y = 0;
            if (current_y > (int)image.getHeight() - 1) current_y = (int)image.getHeight() - 1;
            boundaries[current_y].insert(current_x);
        }
        
        points.clear();
        x0 = x1;
        y0 = y1;
        x1 = (int)round( cos_or * -a - sin_or * -b) + x;
        y1 = (int)round( sin_or * -a + cos_or * -b) + y;
        LinePoints(x0, y0, x1, y1, true, points);
        for (std::list<Tuple<int, int> >::iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
        {
            current_x = begin->getFirst();
            current_y = begin->getSecond();
            if (current_x < 0) current_x = 0;
            if (current_x > (int)image.getWidth() - 1) current_x = (int)image.getWidth() - 1;
            if (current_y < 0) current_y = 0;
            if (current_y > (int)image.getHeight() - 1) current_y = (int)image.getHeight() - 1;
            boundaries[current_y].insert(current_x);
        }
        
        points.clear();
        x0 = x1;
        y0 = y1;
        x1 = (int)round( cos_or *  a - sin_or * -b) + x;
        y1 = (int)round( sin_or *  a + cos_or * -b) + y;
        LinePoints(x0, y0, x1, y1, true, points);
        for (std::list<Tuple<int, int> >::iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
        {
            current_x = begin->getFirst();
            current_y = begin->getSecond();
            if (current_x < 0) current_x = 0;
            if (current_x > (int)image.getWidth() - 1) current_x = (int)image.getWidth() - 1;
            if (current_y < 0) current_y = 0;
            if (current_y > (int)image.getHeight() - 1) current_y = (int)image.getHeight() - 1;
            boundaries[current_y].insert(current_x);
        }
        
        points.clear();
        x0 = x1;
        y0 = y1;
        x1 = (int)round( cos_or *  a - sin_or *  b) + x;
        y1 = (int)round( sin_or *  a + cos_or *  b) + y;
        LinePoints(x0, y0, x1, y1, true, points);
        for (std::list<Tuple<int, int> >::iterator begin = points.begin(), end = points.end(); begin != end; ++begin)
        {
            current_x = begin->getFirst();
            current_y = begin->getSecond();
            if (current_x < 0) current_x = 0;
            if (current_x > (int)image.getWidth() - 1) current_x = (int)image.getWidth() - 1;
            if (current_y < 0) current_y = 0;
            if (current_y > (int)image.getHeight() - 1) current_y = (int)image.getHeight() - 1;
            boundaries[current_y].insert(current_x);
        }
        
        // 2) Draw the ellipse within the calculated boundaries.
        if ((pen.getBackgroundTransparency() > 0) || (pen.getBorderTransparency() > 0))
        {
            for (int cy = 0; cy < (int)image.getHeight(); ++cy)
            {
                if (boundaries[cy].size() > 0)
                {
                    double rx, ry, distance;
                    x0 = *boundaries[cy].begin();
                    x1 = *boundaries[cy].rbegin();
                    for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                        current_ptr[c] = image.get(x0, cy, c);
                    
                    for (int cx = x0; cx <= x1; ++cx)
                    {
                        rx = ( cos_or * (double)(cx - x) +  sin_or * (double)(cy - y)) / (double)a;
                        ry = (-sin_or * (double)(cx - x) +  cos_or * (double)(cy - y)) / (double)b;
                        distance = sqrt(rx * rx + ry * ry);
                        
                        if ((pen.getBackgroundTransparency() > 0) && (distance <= 1))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *current_ptr[0] = (T)(w0_bac * (double)pen.getBackgroundRed()   + w1_bac * (double)*current_ptr[0]);
                                *current_ptr[1] = (T)(w0_bac * (double)pen.getBackgroundGreen() + w1_bac * (double)*current_ptr[1]);
                                *current_ptr[2] = (T)(w0_bac * (double)pen.getBackgroundBlue()  + w1_bac * (double)*current_ptr[2]);
                            }
                            else if (image.getNumberOfChannels() == 1) *current_ptr[0] = (T)(w0_bac * (double)pen.getBackgroundGray() + w1_bac * *current_ptr[0]);;
                        }
                        if ((pen.getBorderTransparency() > 0) && ((unsigned int)srvAbs(distance * (double)a - (double)a) < pen.getBorderSize()))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *current_ptr[0] = (T)(w0_bor * (double)pen.getBorderRed()   + w1_bor * (double)*current_ptr[0]);
                                *current_ptr[1] = (T)(w0_bor * (double)pen.getBorderGreen() + w1_bor * (double)*current_ptr[1]);
                                *current_ptr[2] = (T)(w0_bor * (double)pen.getBorderBlue()  + w1_bor * (double)*current_ptr[2]);
                            }
                            else if (image.getNumberOfChannels() == 1) *current_ptr[0] = (T)(w0_bor * (double)pen.getBorderGray() + w1_bor * (double)*current_ptr[0]);
                        }
                        
                        for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                            ++current_ptr[c];
                    }
                }
            }
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ IMAGE POLYGON FUNCTIONS ]==================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateRegularPolygon(IMAGE<T> &image, int center_x, int center_y, int radius, double initial_angle, unsigned int number_of_sides, const Pencil<T> &pen)
    {
        if (number_of_sides < 3) throw Exception("Incorrect number of sides. A polygon must have at least 3 sides.");
        VectorDense<Tuple<int, int> > points(number_of_sides + 1);
        for (unsigned int p = 0; p < number_of_sides; ++p)
        {
            double angle = ((double)p / (double)number_of_sides) * 2.0 * 3.1415926 + initial_angle;
            points[p].setFirst((int)round((double)radius * cos(angle)) + center_x);
            points[p].setSecond((int)round((double)radius * sin(angle)) + center_y);
        }
        points[number_of_sides] = points[0];
        
        if (pen.getBackgroundTransparency() > 0)
        {
            VectorDense<int> initial_point(image.getHeight(), (int)image.getWidth() + 1);
            VectorDense<int> final_point(image.getHeight(), -1);
            const double w0 = (double)pen.getBackgroundTransparency() / 255.0;
            const double w1 = 1.0 - w0;
            
            for (unsigned int c = 0; c < number_of_sides; ++c)
            {
                std::list<Tuple<int, int> > line_points;
                Draw::LinePoints(points[c].getFirst(), points[c].getSecond(), points[c + 1].getFirst(), points[c + 1].getSecond(), true, line_points);
                
                for (std::list<Tuple<int, int> >::iterator begin = line_points.begin(), end = line_points.end(); begin != end; ++begin)
                {
                    if ((begin->getSecond() >= 0) && (begin->getSecond() < (int)image.getHeight()))
                    {
                        int x = srvMax<int>(0, srvMin<int>(image.getWidth(), begin->getFirst()));
                        if (x < initial_point[begin->getSecond()]) initial_point[begin->getSecond()] = x;
                        if (x > final_point[begin->getSecond()]) final_point[begin->getSecond()] = x;
                    }
                }
            }
            
            if (image.getNumberOfChannels() == 3)
            {
                for (int y = 0; y < (int)image.getHeight(); ++y)
                {
                    for (int x = initial_point[y]; x < final_point[y]; ++x)
                    {
                        *image.get(x, y, 0) = (T)(w0 * (double)pen.getBackgroundRed() + w1 * (double)*image.get(x, y, 0));
                        *image.get(x, y, 1) = (T)(w0 * (double)pen.getBackgroundGreen() + w1 * (double)*image.get(x, y, 1));
                        *image.get(x, y, 2) = (T)(w0 * (double)pen.getBackgroundBlue() + w1 * (double)*image.get(x, y, 2));
                    }
                }
            }
            else
            {
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                    for (int y = 0; y < (int)image.getHeight(); ++y)
                        for (int x = initial_point[y] + 1; x < final_point[y]; ++x)
                            *image.get(x, y, c) = (T)(w0 * (double)pen.getBackgroundGray() + w1 * (double)*image.get(x, y, c));
            }
            
        }
        Draw::Line(image, points, pen);
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateRectangle(IMAGE<T> &image, int x0, int y0, int x1, int y1, const Pencil<T> &pen)
    {
        if (pen.getBackgroundTransparency() > 0)
        {
            int xb0, yb0, xb1, yb1;
            const double w0 = (double)pen.getBackgroundTransparency() / 255.0;
            const double w1 = 1.0 - w0;
            
            if (x1 < x0) srvSwap(x0, x1);
            if (y1 < y0) srvSwap(y0, y1);
            xb0 = srvMax(0, srvMin((int)image.getWidth() - 1, x0));
            yb0 = srvMax(0, srvMin((int)image.getHeight() - 1, y0));
            xb1 = srvMax(0, srvMin((int)image.getWidth() - 1, x1));
            yb1 = srvMax(0, srvMin((int)image.getHeight() - 1, y1));
            
            if ((xb0 != xb1) && (yb0 != yb1))
            {
                if (image.getNumberOfChannels() == 3)
                {
                    for (int y = yb0; y <= yb1; ++y)
                    {
                        for (int x = xb0; x <= xb1; ++x)
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBackgroundRed()   + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBackgroundGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBackgroundBlue()  + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
                else
                {
                    for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                        for (int y = yb0; y <= yb1; ++y)
                            for (int x = xb0; x <= xb1; ++x)
                                *image.get(x, y, c) = (T)(w0 * (double)pen.getBackgroundGray() + w1 * (double)*image.get(x, y, c));
                }
            }
        }
        if ((pen.getBorderTransparency() > 0) && (pen.getBorderSize() > 0))
        {
            const double w0 = (double)pen.getBorderTransparency() / 255.0;
            const double w1 = 1.0 - w0;
            
            if (image.getNumberOfChannels() == 3)
            {
                int border_size = (int)pen.getBorderSize() - 1;
                // Top border (x0, x1)-y0
                for (int y = y0 - border_size; y <= y0 + border_size; ++y)
                {
                    for (int x = x0 - border_size; x <= x1 + border_size; ++x)
                    {
                        if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
                // Bottom border (x0, x1)-y1
                for (int y = y1 - border_size; y <= y1 + border_size; ++y)
                {
                    for (int x = x0 - border_size; x <= x1 + border_size; ++x)
                    {
                        if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
                // Left border x0-(y0, y1)
                for (int y = y0 - border_size; y <= y1 + border_size; ++y)
                {
                    for (int x = x0 - border_size; x <= x0 + border_size; ++x)
                    {
                        if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
                // Right border x1-(y0, y1)
                for (int y = y0 - border_size; y <= y1 + border_size; ++y)
                {
                    for (int x = x1 - border_size; x <= x1 + border_size; ++x)
                    {
                        if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                        {
                            *image.get(x, y, 0) = (T)(w0 * (double)pen.getBorderRed() + w1 * (double)*image.get(x, y, 0));
                            *image.get(x, y, 1) = (T)(w0 * (double)pen.getBorderGreen() + w1 * (double)*image.get(x, y, 1));
                            *image.get(x, y, 2) = (T)(w0 * (double)pen.getBorderBlue() + w1 * (double)*image.get(x, y, 2));
                        }
                    }
                }
            }
            else
            {
                // Top border (x0, x1)-y0
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                    for (int y = y0 - (int)pen.getBorderSize(); y < y0 + (int)pen.getBorderSize(); ++y)
                        for (int x = x0 - (int)pen.getBorderSize(); x < x1 + (int)pen.getBorderSize(); ++x)
                            if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                                *image.get(x, y, c) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, c));
                // Bottom border (x0, x1)-y1
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                    for (int y = y1 - (int)pen.getBorderSize(); y < y1 + (int)pen.getBorderSize(); ++y)
                        for (int x = x0 - (int)pen.getBorderSize(); x < x1 + (int)pen.getBorderSize(); ++x)
                            if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                                *image.get(x, y, c) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, c));
                // Left border x0-(y0, y1)
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                    for (int y = y0 - (int)pen.getBorderSize(); y < y1 + (int)pen.getBorderSize(); ++y)
                        for (int x = x0 - (int)pen.getBorderSize(); x < x0 + (int)pen.getBorderSize(); ++x)
                            if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                                *image.get(x, y, c) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, c));
                // Right border x1-(y0, y1)
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                    for (int y = y0 - (int)pen.getBorderSize(); y < y1 + (int)pen.getBorderSize(); ++y)
                        for (int x = x1 - (int)pen.getBorderSize(); x < x1 + (int)pen.getBorderSize(); ++x)
                            if ((x >= 0) && (y >= 0) && (x < (int)image.getWidth()) && (y < (int)image.getHeight()))
                                *image.get(x, y, c) = (T)(w0 * (double)pen.getBorderGray() + w1 * (double)*image.get(x, y, c));
            }
        }
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivateRotatedRectangle(IMAGE<T> &image, int center_x, int center_y, int width, int height, double orientation, const Pencil<T> &pen)
    {
        int corners_x[4], corners_y[4];
        double cx0, cy0, cx1, cy1;
        
        if (width < 0) width = -width;
        if (height < 0) height = -height;
        cx0 = (double)-width  / 2.0;
        cy0 = (double)-height / 2.0;
        cx1 = (double) width  / 2.0;
        cy1 = (double) height / 2.0;
        corners_x[0] = (int)( cx0 * cos(orientation) + cy0 * sin(orientation)) + center_x;
        corners_y[0] = (int)(-cx0 * sin(orientation) + cy0 * cos(orientation)) + center_y;
        corners_x[1] = (int)( cx0 * cos(orientation) + cy1 * sin(orientation)) + center_x;
        corners_y[1] = (int)(-cx0 * sin(orientation) + cy1 * cos(orientation)) + center_y;
        corners_x[2] = (int)( cx1 * cos(orientation) + cy1 * sin(orientation)) + center_x;
        corners_y[2] = (int)(-cx1 * sin(orientation) + cy1 * cos(orientation)) + center_y;
        corners_x[3] = (int)( cx1 * cos(orientation) + cy0 * sin(orientation)) + center_x;
        corners_y[3] = (int)(-cx1 * sin(orientation) + cy0 * cos(orientation)) + center_y;
        
        if (pen.getBackgroundTransparency() > 0)
        {
            VectorDense<int> initial(image.getHeight(), (int)image.getWidth() + 1), final(image.getHeight(), -1);
            std::list<Tuple<int, int> > line_points;
            const double w0 = (double)pen.getBackgroundTransparency() / 255.0;
            const double w1 = 1.0 - w0;
            
            for (unsigned int k = 0; k < 4; ++k)
            {
                Draw::LinePoints(corners_x[k], corners_y[k], corners_x[(k + 1) % 4], corners_y[(k + 1) % 4], true, line_points);
                
                for (std::list<Tuple<int, int> >::iterator begin = line_points.begin(), end = line_points.end(); begin != end; ++begin)
                {
                    if ((begin->getSecond() >= 0) && (begin->getSecond() < (int)image.getHeight()))
                    {
                        int x = srvMax<int>(0, srvMin<int>(image.getWidth(), begin->getFirst()));
                        if (x < initial[begin->getSecond()]) initial[begin->getSecond()] = x;
                        if (x > final[begin->getSecond()]) final[begin->getSecond()] = x;
                    }
                }
                line_points.clear();
            }
            
            if (image.getNumberOfChannels() == 3)
            {
                for (int y = 0; y < (int)image.getHeight(); ++y)
                {
                    T * __restrict__ image_ptr[3] = { image.get(y, 0), image.get(y, 1), image.get(y, 2) };
                    
                    for (int x = initial[y]; x < final[y]; ++x)
                    {
                        image_ptr[0][x] = (T)(w0 * (double)pen.getBackgroundRed()   + w1 * (double)image_ptr[0][x]);
                        image_ptr[1][x] = (T)(w0 * (double)pen.getBackgroundGreen() + w1 * (double)image_ptr[1][x]);
                        image_ptr[2][x] = (T)(w0 * (double)pen.getBackgroundBlue()  + w1 * (double)image_ptr[2][x]);
                    }
                }
            }
            else
            {
                for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                {
                    for (int y = 0; y < (int)image.getHeight(); ++y)
                    {
                        T * __restrict__ image_ptr = image.get(y, c);
                        for (int x = initial[y] + 1; x < final[y]; ++x)
                            image_ptr[x] = (T)(w0 * (double)pen.getBackgroundGray() + w1 * (double)image_ptr[x]);
                    }
                }
            }
        }
        for (unsigned int k = 0; k < 4; ++k)
            Draw::Line(image, corners_x[k], corners_y[k], corners_x[(k + 1) % 4], corners_y[(k + 1) % 4], pen);
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::PrivatePolygon(IMAGE<T> &image, const srv::VectorDense<srv::Tuple<int, int> > &coordinates, const Draw::Pencil<T> &pen)
    {
        // Draw the inside of the polygon.
        if ((coordinates.size() > 2) && (pen.getBackgroundTransparency() > 0))
        {
            const int width  = (int)image.getWidth();
            const int height = (int)image.getHeight();
            
            // Internal structures ....................................................................................
            struct PolygonEdge
            {
                /// Y-coordinate at the start of the edge.
                int y0;
                /// Y-coordinate at the end of the edge.
                int y1;
                /// X-coordinate at the start of the edge.
                int x;
                /// Step of the edge line.
                int dx;
                /// Pointer to the next element edge.
                PolygonEdge * next;
            };
            // Internal variables .....................................................................................
            VectorDense<PolygonEdge> edges(coordinates.size() + 1);
            int x_min, x_max, y_min, y_max, nedges, idx, total;
            PolygonEdge * edge_current, edge_temporal;
            
            x_min = image.getWidth();
            y_min = image.getHeight();
            nedges = x_max = y_max = 0;
            for (unsigned int i = 0; i < coordinates.size(); ++i)
            {
                const Tuple<int, int> &point_start = coordinates[i];
                const Tuple<int, int> &point_end = coordinates[(i + 1) % coordinates.size()];
                // Compute the bounding box of the polygon.
                if (point_start.getFirst()  < x_min) x_min = point_start.getFirst();
                if (point_start.getFirst()  > x_max) x_max = point_start.getFirst();
                if (point_start.getSecond() < y_min) y_min = point_start.getSecond();
                if (point_start.getSecond() > y_max) y_max = point_start.getSecond();
                // Add the current edge to the edge list.
                if (point_start.getSecond() != point_end.getSecond()) // Avoid horizontal lines
                {
                    if (point_end.getSecond() > point_start.getSecond())
                    {
                        edges[nedges].y0 = point_start.getSecond();
                        edges[nedges].y1 = point_end.getSecond();
                        edges[nedges].x = point_start.getFirst();
                    }
                    else
                    {
                        edges[nedges].y0 = point_end.getSecond();
                        edges[nedges].y1 = point_start.getSecond();
                        edges[nedges].x = point_end.getFirst();
                    }
                    edges[nedges].dx = (point_end.getFirst() - point_start.getFirst()) / (point_end.getSecond() - point_start.getSecond());
                    ++nedges;
                }
            }
            // Sort the edges with priority: top Y coordinate > top X coordinate > dX.
            std::sort(&edges[0], &edges[nedges], [](const PolygonEdge &first, const PolygonEdge &second) {
                    return (first.y0 - second.y0)?(first.y0 < second.y0):((first.x - second.x)?(first.x < second.x):(first.dx < second.dx));});
            
            total = nedges;
            edges[nedges].y0 = std::numeric_limits<int>::max();
            edge_temporal = edges[nedges];
            ++nedges;
            
            idx = 0;
            edge_current = &edges[idx];
            edge_temporal.next = 0;
            for (int y = edge_current->y0; y < y_max; ++y)
            {
                PolygonEdge * edge_last, * edge_penultimate, * edge_penultimate_copy;
                bool sort_flag, draw, clipline;
                
                sort_flag = draw = false;
                clipline = (y < 0) || (y > height - 1);
                edge_penultimate = &edge_temporal;
                edge_last = edge_temporal.next;
                while (edge_last || edge_current->y0 == y)
                {
                    if (edge_last && edge_last->y1 == y)
                    {
                        edge_penultimate->next = edge_last->next;                                   // Exclude edge if Y reaches its lower point.
                        edge_last = edge_last->next;
                        continue;
                    }
                    edge_penultimate_copy = edge_penultimate;
                    if (edge_last && ((edge_current->y0 > y) || (edge_last->x < edge_current->x)))
                    {
                        edge_penultimate = edge_last;                                               // Go to the next edge in active list.
                        edge_last = edge_last->next;
                    }
                    else if (idx < total)
                    {
                        edge_penultimate->next = edge_current;                                      // Insert the new edge into the active list if Y reach its upper point.
                        edge_current->next = edge_last;
                        edge_penultimate = edge_current;
                        edge_current = &edges[++idx];
                    }
                    else break;
                    
                    if (draw)
                    {
                        if (!clipline)
                        {
                            int x_start, x_end;
                            
                            if (edge_penultimate_copy->x > edge_penultimate->x)
                            {
                                x_start = edge_penultimate->x;
                                x_end = edge_penultimate_copy->x;
                            }
                            else
                            {
                                x_start = edge_penultimate_copy->x;
                                x_end = edge_penultimate->x;
                            }
                            x_start = std::min(width - 1, std::max(0, x_start));
                            x_end   = std::min(width - 1, std::max(0, x_end  ));
                            
                            if ((x_start < (int)image.getWidth()) && (x_end >= 0))
                            {
                                if (x_start < 0) x_start = 0;
                                if (x_end >= (int)image.getWidth()) x_end = (int)(image.getWidth() - 1);
                                if (image.getNumberOfChannels() == 3)
                                {
                                    unsigned char * __restrict__ ptr_r = image.get(x_start, y, 0);
                                    unsigned char * __restrict__ ptr_g = image.get(x_start, y, 1);
                                    unsigned char * __restrict__ ptr_b = image.get(x_start, y, 2);
                                    for (int pos = x_start; pos <= x_end; ++pos, ++ptr_r, ++ptr_g, ++ptr_b)
                                    {
                                        *ptr_r = pen.getBackgroundRed();
                                        *ptr_g = pen.getBackgroundGreen();
                                        *ptr_b = pen.getBackgroundBlue();
                                    }
                                }
                                else
                                {
                                    unsigned char * __restrict__ ptr = image.get(x_start, y, 0);
                                    for (int pos = x_start; pos <= x_end; ++pos, ++ptr)
                                        *ptr = pen.getBackgroundGray();
                                }
                            }
                        }
                        edge_penultimate_copy->x += edge_penultimate_copy->dx;
                        edge_penultimate->x += edge_penultimate->dx;
                    }
                    draw = !draw;
                }
                // End of while ...................................................................
                edge_penultimate_copy = 0;
                do
                {
                    edge_penultimate = &edge_temporal;
                    edge_last = edge_temporal.next;
                    while ((edge_last != edge_penultimate_copy) && (edge_last->next != 0))
                    {
                        PolygonEdge * te;
                        
                        te = edge_last->next;
                        if (edge_last->x > te->x)                                                   // Swap edges.
                        {
                            edge_penultimate->next = te;
                            edge_last->next = te->next;
                            te->next = edge_last;
                            edge_penultimate = te;
                            sort_flag = true;
                        }
                        else
                        {
                            edge_penultimate = edge_last;
                            edge_last = te;
                        }
                    }
                    edge_penultimate_copy = edge_penultimate;
                }
                while (sort_flag && (edge_penultimate_copy != edge_temporal.next) && (edge_penultimate_copy != &edge_temporal));
            }
        }
        
        // Draw the border of the polygon.
        for (unsigned int i = 0; i < coordinates.size(); ++i)
        {
            const srv::Tuple<int, int> &point_start = coordinates[i];
            const srv::Tuple<int, int> &point_end = coordinates[(i + 1) % coordinates.size()];
            Draw::Line(image, point_start.getFirst(), point_start.getSecond(), point_end.getFirst(), point_end.getSecond(), pen);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CIRCLE AUXILIARY FUNCTIONS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <template <class> class IMAGE, class T>
    void Draw::CircleBackground(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen)
    {
        const double pen_weight = (double)pen.getBackgroundTransparency() / 255.0;
        std::list<Tuple<double, double> > circle_points;
        VectorDense<int> min_x(image.getHeight(), (int)image.getWidth() + 1), max_x(image.getHeight(), -1);
        
        CirclePoints(x, y, radius, circle_points);
        for (std::list<Tuple<double, double> >::iterator begin = circle_points.begin(), end = circle_points.end(); begin != end; ++begin)
        {
            int px = (int)begin->getFirst();
            int py = (int)begin->getSecond();
            
            if ((py >= 0) && (py < (int)image.getHeight()))
            {
                if (px < min_x[py]) min_x[py] = px;
                if (px > max_x[py]) max_x[py] = px;
            }
        }
        for (unsigned int cy = 0; cy < image.getHeight(); ++cy)
        {
            min_x[cy] = srvMax<int>(0, srvMin<int>(image.getWidth() - 1, min_x[cy]));
            max_x[cy] = srvMax<int>(0, srvMin<int>(image.getWidth() - 1, max_x[cy]));
        }
        if (image.getNumberOfChannels() == 3)
        {
            for (unsigned int cy = 0; cy < image.getHeight(); ++cy)
            {
                for (int cx = min_x[cy]; cx <= max_x[cy]; ++cx)
                {
                    *image.get(cx, cy, 0) = (T)(pen_weight * (double)pen.getBackgroundRed()   + (1.0 - pen_weight) * (double)*image.get(cx, cy, 0));
                    *image.get(cx, cy, 1) = (T)(pen_weight * (double)pen.getBackgroundGreen() + (1.0 - pen_weight) * (double)*image.get(cx, cy, 1));
                    *image.get(cx, cy, 2) = (T)(pen_weight * (double)pen.getBackgroundBlue()  + (1.0 - pen_weight) * (double)*image.get(cx, cy, 2));
                }
            }
        }
        else
        {
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                for (unsigned int cy = 0; cy < image.getHeight(); ++cy)
                    for (int cx = min_x[cy]; cx <= max_x[cy]; ++cx)
                        *image.get(cx, cy, c) = (T)(pen_weight * (double)pen.getBackgroundGray() + (1.0 - pen_weight) * (double)*image.get(cx, cy, c));
        }
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::Circumference(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        std::list<Tuple<double, double> > circle_points;
        
        CirclePoints(x, y, radius, circle_points);
        
        if (image.getNumberOfChannels() == 3) // Color image (3-channels).
        {
            for (std::list<Tuple<double, double> >::iterator begin = circle_points.begin(), end = circle_points.end(); begin != end; ++begin)
            {
                int px = (int)begin->getFirst();
                int py = (int)begin->getSecond();
                if ((px >= 0) && (py >= 0) && (px < (int)image.getWidth()) && (py < (int)image.getHeight()))
                {
                    *image.get(px, py, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(px, py, 0));
                    *image.get(px, py, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(px, py, 1));
                    *image.get(px, py, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(px, py, 2));
                }
            }
        }
        else // Image with any other number of channels.
        {
            for (std::list<Tuple<double, double> >::iterator begin = circle_points.begin(), end = circle_points.end(); begin != end; ++begin)
            {
                int px = (int)begin->getFirst();
                int py = (int)begin->getSecond();
                if ((px >= 0) && (py >= 0) && (px < (int)image.getWidth()) && (py < (int)image.getHeight()))
                {
                    for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                        *image.get(px, py, c) = (T)(pen_weight * (double)pen.getBorderGray()   + (1.0 - pen_weight) * (double)*image.get(px, py, c));
                }
            }
        }
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::CircumferenceAntialiased(IMAGE<T> &image, int x, int y, int radius, const Pencil<T> &pen)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        double w0, w1;
        std::list<Tuple<double, double> > circle_points;
        
        CirclePoints(x, y, radius, circle_points);
        
        if (image.getNumberOfChannels() == 3) // Color image (3-channels).
        {
            for (std::list<Tuple<double, double> >::iterator begin = circle_points.begin(), end = circle_points.end(); begin != end; ++begin)
            {
                int px = (int)begin->getFirst();
                int py = (int)begin->getSecond();
                if ((px >= 0) && (py >= 0) && (px < (int)image.getWidth()) && (py < (int)image.getHeight()))
                {
                    if (px == begin->getFirst())
                    {
                        w0 = pen_weight * (1.0 - (begin->getSecond() - (double)py));
                        w1 = pen_weight * (begin->getSecond() - (double)py);
                        
                        *image.get(px, py, 0) = (T)(w0 * (double)pen.getBorderRed()   + (1.0 - w0) * (double)*image.get(px, py, 0));
                        *image.get(px, py, 1) = (T)(w0 * (double)pen.getBorderGreen() + (1.0 - w0) * (double)*image.get(px, py, 1));
                        *image.get(px, py, 2) = (T)(w0 * (double)pen.getBorderBlue()  + (1.0 - w0) * (double)*image.get(px, py, 2));
                        
                        if (py + 1 < (int)image.getHeight())
                        {
                            *image.get(px, py + 1, 0) = (T)(w1 * (double)pen.getBorderRed()   + (1.0 - w1) * (double)*image.get(px, py + 1, 0));
                            *image.get(px, py + 1, 1) = (T)(w1 * (double)pen.getBorderGreen() + (1.0 - w1) * (double)*image.get(px, py + 1, 1));
                            *image.get(px, py + 1, 2) = (T)(w1 * (double)pen.getBorderBlue()  + (1.0 - w1) * (double)*image.get(px, py + 1, 2));
                        }
                    }
                    else
                    {
                        w0 = pen_weight * (1.0 - (begin->getFirst() - (double)px));
                        w1 = pen_weight * (begin->getFirst() - (double)px);
                        
                        *image.get(px, py, 0) = (T)(w0 * (double)pen.getBorderRed()   + (1.0 - w0) * (double)*image.get(px, py, 0));
                        *image.get(px, py, 1) = (T)(w0 * (double)pen.getBorderGreen() + (1.0 - w0) * (double)*image.get(px, py, 1));
                        *image.get(px, py, 2) = (T)(w0 * (double)pen.getBorderBlue()  + (1.0 - w0) * (double)*image.get(px, py, 2));
                        
                        if (px + 1 < (int)image.getWidth())
                        {
                            *image.get(px + 1, py, 0) = (T)(w1 * (double)pen.getBorderRed()   + (1.0 - w1) * (double)*image.get(px + 1, py, 0));
                            *image.get(px + 1, py, 1) = (T)(w1 * (double)pen.getBorderGreen() + (1.0 - w1) * (double)*image.get(px + 1, py, 1));
                            *image.get(px + 1, py, 2) = (T)(w1 * (double)pen.getBorderBlue()  + (1.0 - w1) * (double)*image.get(px + 1, py, 2));
                        }
                    }
                }
            }
        }
        else // Image with any other number of channels.
        {
            for (std::list<Tuple<double, double> >::iterator begin = circle_points.begin(), end = circle_points.end(); begin != end; ++begin)
            {
                int px = (int)begin->getFirst();
                int py = (int)begin->getSecond();
                if ((px >= 0) && (py >= 0) && (px < (int)image.getWidth()) && (py < (int)image.getHeight()))
                {
                    if (px == begin->getFirst())
                    {
                        w0 = pen_weight * (1.0 - (begin->getSecond() - (double)py));
                        w1 = pen_weight * (begin->getSecond() - (double)py);
                        
                        for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                            *image.get(px, py, c) = (T)(w0 * (double)pen.getBorderGray()   + (1.0 - w0) * (double)*image.get(px, py, c));
                        if (py + 1 < (int)image.getHeight())
                            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(px, py + 1, c) = (T)(w1 * (double)pen.getBorderGray() + (1.0 - w1) * (double)*image.get(px, py + 1, c));
                    }
                    else
                    {
                        w0 = pen_weight * (1.0 - (begin->getFirst() - (double)px));
                        w1 = pen_weight * (begin->getFirst() - (double)px);
                        
                        for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                            *image.get(px, py, c) = (T)(w0 * (double)pen.getBorderGray()   + (1.0 - w0) * (double)*image.get(px, py, c));
                        if (px + 1 < (int)image.getWidth())
                            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(px + 1, py, c) = (T)(w1 * (double)pen.getBorderGray()   + (1.0 - w1) * (double)*image.get(px + 1, py, c));
                    }
                }
            }
        }
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::CircleBorder(IMAGE<T> &image, int circle_x, int circle_y, int radius, const Pencil<T> &pen)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        std::list<Tuple<double, double> > inner_circle_points, outer_circle_points;
        VectorDense<int> min_inner_x(image.getHeight(), (int)image.getWidth() + 1), max_inner_x(image.getHeight(), -1);
        VectorDense<int> min_outer_x(image.getHeight(), (int)image.getWidth() + 1), max_outer_x(image.getHeight(), -1);
        
        CirclePoints(circle_x, circle_y, radius - (int)pen.getBorderSize(), inner_circle_points);
        CirclePoints(circle_x, circle_y, radius + (int)pen.getBorderSize(), outer_circle_points);
        
        for (std::list<Tuple<double, double> >::iterator begin = inner_circle_points.begin(), end = inner_circle_points.end(); begin != end; ++begin)
        {
            int px = (int)begin->getFirst();
            int py = (int)begin->getSecond();
            
            if ((py >= 0) && (py < (int)image.getHeight()))
            {
                if (px < min_inner_x[py]) min_inner_x[py] = px;
                if (px > max_inner_x[py]) max_inner_x[py] = px;
            }
        }
        for (std::list<Tuple<double, double> >::iterator begin = outer_circle_points.begin(), end = outer_circle_points.end(); begin != end; ++begin)
        {
            int px = (int)begin->getFirst();
            int py = (int)begin->getSecond();
            
            if ((py >= 0) && (py < (int)image.getHeight()))
            {
                if (px < min_outer_x[py]) min_outer_x[py] = px;
                if (px > max_outer_x[py]) max_outer_x[py] = px;
            }
        }
        
        if (image.getNumberOfChannels() == 3)
        {
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                if (min_outer_x[y] <= max_outer_x[y])
                {
                    int lx0, lx1;
                    if (min_inner_x[y] < max_inner_x[y])
                    {
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_outer_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_inner_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                        {
                            *image.get(xx, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(xx, y, 0));
                            *image.get(xx, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(xx, y, 1));
                            *image.get(xx, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(xx, y, 2));
                        }
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_inner_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_outer_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                        {
                            *image.get(xx, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(xx, y, 0));
                            *image.get(xx, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(xx, y, 1));
                            *image.get(xx, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(xx, y, 2));
                        }
                    }
                    else
                    {
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_outer_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_outer_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                        {
                            *image.get(xx, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(xx, y, 0));
                            *image.get(xx, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(xx, y, 1));
                            *image.get(xx, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(xx, y, 2));
                        }
                    }
                }
            }
        }
        else
        {
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                if (min_outer_x[y] <= max_outer_x[y])
                {
                    int lx0, lx1;
                    if (min_inner_x[y] < min_inner_x[y])
                    {
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_outer_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_inner_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(xx, y, c) = (T)(pen_weight * (double)pen.getBorderGray() + (1.0 - pen_weight) * (double)*image.get(xx, y, c));
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_inner_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_outer_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(xx, y, c) = (T)(pen_weight * (double)pen.getBorderGray() + (1.0 - pen_weight) * (double)*image.get(xx, y, c));
                    }
                    else
                    {
                        lx0 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, min_outer_x[y]));
                        lx1 = srvMax<int>(0, srvMin<int>((int)image.getWidth() - 1, max_outer_x[y]));
                        for (int xx = lx0; xx < lx1; ++xx)
                            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(xx, y, c) = (T)(pen_weight * (double)pen.getBorderGray() + (1.0 - pen_weight) * (double)*image.get(xx, y, c));
                    }
                }
            }
        }
    }
    
    template <template <class> class IMAGE, class T>
    void Draw::CircleAntialiasedBorder(IMAGE<T> &image, int center_x, int center_y, int radius, const Pencil<T> &pen)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        const int width = (int)image.getWidth();
        const int height = (int)image.getHeight();
        std::list<Tuple<double, double> > inner_circle_points, outer_circle_points;
        VectorDense<std::list<Tuple<int, double> > > inner_left(image.getHeight()), inner_right(image.getHeight());
        VectorDense<std::list<Tuple<int, double> > > outer_left(image.getHeight()), outer_right(image.getHeight());
        
        CirclePoints(center_x, center_y, radius + (int)pen.getBorderSize(), outer_circle_points);
        CirclePoints(center_x, center_y, radius - (int)pen.getBorderSize(), inner_circle_points);
        
        // 1) Process the pixels of the outer circle.
        for (std::list<Tuple<double, double> >::iterator begin = outer_circle_points.begin(), end = outer_circle_points.end(); begin != end; ++begin)
        {
            int px = (int)begin->getFirst();
            int py = (int)begin->getSecond();
            
            if ((py >= 0) && (py < height))
            {
                if (py == begin->getSecond())
                {
                    if (px < center_x) // Left part of the circle.
                    {
                        outer_left[py].push_back(Tuple<int, double>(px, pen_weight * (1.0 - (begin->getFirst() - (double)px))));
                    }
                    else // Right part of the circle.
                    {
                        outer_right[py].push_back(Tuple<int, double>(px + 1, pen_weight * (begin->getFirst() - (double)px)));
                    }
                }
                else
                {
                    if (px < center_x) // Left part of the circle.
                    {
                        outer_left[py].push_back(Tuple<int, double>(px, pen_weight * (1.0 - (begin->getSecond() - (double)py))));
                        if (py + 1 < height) outer_left[py + 1].push_back(Tuple<int, double>(px, pen_weight * (begin->getSecond() - (double)py)));
                    }
                    else // Right part of the circle.
                    {
                        outer_right[py].push_back(Tuple<int, double>(px, pen_weight * (1.0 - (begin->getSecond() - (double)py))));
                        if (py + 1 < height) outer_right[py + 1].push_back(Tuple<int, double>(px, pen_weight * (begin->getSecond() - (double)py)));
                    }
                }
            }
        }
        
        // 2) Process the pixels of the inner circle.
        for (std::list<Tuple<double, double> >::iterator begin = inner_circle_points.begin(), end = inner_circle_points.end(); begin != end; ++begin)
        {
            int px = (int)begin->getFirst();
            int py = (int)begin->getSecond();
            
            if ((py >= 0) && (py < height))
            {
                if (py == begin->getSecond())
                {
                    const double w0 = pen_weight * (1.0 - (begin->getFirst() - (double)px));
                    const double w1 = pen_weight * (begin->getFirst() - (double)px);
                    if (px < center_x) // Left part of the circle.
                    {
                        //inner_left[py].push_back(Tuple<int, double>(px, w0));
                        inner_left[py].push_back(Tuple<int, double>(px + 1, w1));
                    }
                    else // Right part of the circle.
                    {
                        inner_right[py].push_back(Tuple<int, double>(px, w0));
                        //inner_right[py].push_back(Tuple<int, double>(px + 1, w1));
                    }
                }
                else
                {
                    const double w0 = pen_weight * (1.0 - (begin->getSecond() - (double)py));
                    const double w1 = pen_weight * (begin->getSecond() - (double)py);
                    if (px < center_x) // Left part of the circle.
                    {
                        inner_left[py].push_back(Tuple<int, double>(px, w0));
                        if (py + 1 < height) inner_left[py + 1].push_back(Tuple<int, double>(px, w1));
                    }
                    else // Right part of the circle.
                    {
                        inner_right[py].push_back(Tuple<int, double>(px, w0));
                        if (py + 1 < height) inner_right[py + 1].push_back(Tuple<int, double>(px, w1));
                    }
                }
            }
        }
        
        for (int y = 0; y < height; ++y)
        {
            // 3) Sort the pixel coordinates according to their x coordinate and filter incorrect weight points.
            if (inner_left[y].size() > 1)
            {
                std::list<Tuple<int, double> >::iterator previous, search_iterator;
                inner_left[y].sort(); // Sort
                
                previous = search_iterator = inner_left[y].begin(); // Remove descending weights.
                ++search_iterator;
                for (; search_iterator != inner_left[y].end(); ++search_iterator, ++previous)
                    if (search_iterator->getSecond() < previous->getSecond()) break;
                if (search_iterator != inner_left[y].end())
                {
                    previous = inner_left[y].begin();
                    while (previous != search_iterator) previous = inner_left[y].erase(previous);
                }
                else
                {
                    while (inner_left[y].size() > 1) inner_left[y].pop_front();
                }
            }
            if (inner_right[y].size() > 1)
            {
                std::list<Tuple<int, double> >::iterator previous, search_iterator;
                inner_right[y].sort(); // Sort
                
                previous = search_iterator = inner_right[y].begin(); // Remove ascending weights.
                ++search_iterator;
                for (; search_iterator != inner_right[y].end(); ++search_iterator, ++previous)
                    if (search_iterator->getSecond() < previous->getSecond()) break;
                while (search_iterator != inner_right[y].end()) search_iterator = inner_right[y].erase(search_iterator);
            }
            if (outer_left[y].size() > 1)
            {
                std::list<Tuple<int, double> >::iterator previous, search_iterator;
                outer_left[y].sort(); // Sort
                
                previous = search_iterator = outer_left[y].begin(); // Remove descending weights.
                ++search_iterator;
                for (; search_iterator != outer_left[y].end(); ++search_iterator, ++previous)
                    if (search_iterator->getSecond() < previous->getSecond()) break;
                while (search_iterator != outer_left[y].end()) search_iterator = outer_left[y].erase(search_iterator);
            }
            if (outer_right[y].size() > 1)
            {
                std::list<Tuple<int, double> >::iterator previous, search_iterator;
                outer_right[y].sort(); // Sort
                
                previous = search_iterator = outer_right[y].begin(); // Remove ascending weights.
                ++search_iterator;
                for (; search_iterator != outer_right[y].end(); ++search_iterator, ++previous)
                    if (search_iterator->getSecond() < previous->getSecond()) break;
                if (search_iterator != outer_right[y].end())
                {
                    previous = outer_right[y].begin();
                    while (previous != search_iterator) previous = outer_right[y].erase(previous);
                }
                else
                {
                    while (outer_right[y].size() > 1) outer_right[y].pop_front();
                }
            }
            
            // 4) and paint the circle.
            if ((outer_left[y].size() > 0) && (outer_right[y].size() > 0))
            {
                int current_x = -1;
                
                for (std::list<Tuple<int, double> >::iterator begin = outer_left[y].begin(), end = outer_left[y].end(); begin != end; ++begin)
                {
                    const double w = begin->getSecond();
                    current_x = begin->getFirst();
                    
                    if ((current_x >= 0) && (current_x < width))
                    {
                        if (image.getNumberOfChannels() == 3)
                        {
                            *image.get(current_x, y, 0) = (T)(w * (double)pen.getBorderRed()   + (1.0 - w) * (double)*image.get(current_x, y, 0));
                            *image.get(current_x, y, 1) = (T)(w * (double)pen.getBorderGreen() + (1.0 - w) * (double)*image.get(current_x, y, 1));
                            *image.get(current_x, y, 2) = (T)(w * (double)pen.getBorderBlue()  + (1.0 - w) * (double)*image.get(current_x, y, 2));
                        }
                        else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                            *image.get(current_x, y, c) = (T)(w * (double)pen.getBorderGray()  + (1.0 - w) * (double)*image.get(current_x, y, c));
                    }
                }
                ++current_x;
                
                if (current_x >= width) continue;
                
                if ((inner_left[y].size() > 0) && (inner_right[y].size() > 0)) // There is an inner circle...
                {
                    // Paint the normal border of the left inner region.
                    while (current_x < inner_left[y].front().getFirst())
                    {
                        if ((current_x >= 0) && (current_x < width))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *image.get(current_x, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(current_x, y, 0));
                                *image.get(current_x, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(current_x, y, 1));
                                *image.get(current_x, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, 2));
                            }
                            else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(current_x, y, c) = (T)(pen_weight * (double)pen.getBorderGray()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, c));
                        }
                        ++current_x;
                    }
                    
                    // Paint the anti-aliased part of the border of the inner region.
                    for (std::list<Tuple<int, double> >::iterator begin = inner_left[y].begin(), end = inner_left[y].end(); begin != end; ++begin)
                    {
                        const double w = begin->getSecond();
                        current_x = begin->getFirst();
                        
                        if ((current_x >= 0) && (current_x < width))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *image.get(current_x, y, 0) = (T)(w * (double)pen.getBorderRed()   + (1.0 - w) * (double)*image.get(current_x, y, 0));
                                *image.get(current_x, y, 1) = (T)(w * (double)pen.getBorderGreen() + (1.0 - w) * (double)*image.get(current_x, y, 1));
                                *image.get(current_x, y, 2) = (T)(w * (double)pen.getBorderBlue()  + (1.0 - w) * (double)*image.get(current_x, y, 2));
                            }
                            else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(current_x, y, c) = (T)(w * (double)pen.getBorderGray()  + (1.0 - w) * (double)*image.get(current_x, y, c));
                        }
                    }
                    
                    // Paint the anti-aliased part of the border of the inner region.
                    for (std::list<Tuple<int, double> >::iterator begin = inner_right[y].begin(), end = inner_right[y].end(); begin != end; ++begin)
                    {
                        const double w = begin->getSecond();
                        current_x = begin->getFirst();
                        
                        if ((current_x >= 0) && (current_x < width))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *image.get(current_x, y, 0) = (T)(w * (double)pen.getBorderRed()   + (1.0 - w) * (double)*image.get(current_x, y, 0));
                                *image.get(current_x, y, 1) = (T)(w * (double)pen.getBorderGreen() + (1.0 - w) * (double)*image.get(current_x, y, 1));
                                *image.get(current_x, y, 2) = (T)(w * (double)pen.getBorderBlue()  + (1.0 - w) * (double)*image.get(current_x, y, 2));
                            }
                            else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(current_x, y, c) = (T)(w * (double)pen.getBorderGray()  + (1.0 - w) * (double)*image.get(current_x, y, c));
                        }
                    }
                    ++current_x;
                    
                    // Paint the normal border of the right inner region.
                    while (current_x < outer_right[y].front().getFirst())
                    {
                        if ((current_x >= 0) && (current_x < width))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *image.get(current_x, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(current_x, y, 0));
                                *image.get(current_x, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(current_x, y, 1));
                                *image.get(current_x, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, 2));
                            }
                            else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(current_x, y, c) = (T)(pen_weight * (double)pen.getBorderGray()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, c));
                        }
                        ++current_x;
                    }
                }
                else // Otherwise...
                {
                    // Paint the normal border of the outer region.
                    while (current_x < outer_right[y].front().getFirst())
                    {
                        if ((current_x >= 0) && (current_x < width))
                        {
                            if (image.getNumberOfChannels() == 3)
                            {
                                *image.get(current_x, y, 0) = (T)(pen_weight * (double)pen.getBorderRed()   + (1.0 - pen_weight) * (double)*image.get(current_x, y, 0));
                                *image.get(current_x, y, 1) = (T)(pen_weight * (double)pen.getBorderGreen() + (1.0 - pen_weight) * (double)*image.get(current_x, y, 1));
                                *image.get(current_x, y, 2) = (T)(pen_weight * (double)pen.getBorderBlue()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, 2));
                            }
                            else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                *image.get(current_x, y, c) = (T)(pen_weight * (double)pen.getBorderGray()  + (1.0 - pen_weight) * (double)*image.get(current_x, y, c));
                        }
                        ++current_x;
                    }
                }
                // Paint the anti-aliased part of the border of the outer region.
                for (std::list<Tuple<int, double> >::iterator begin = outer_right[y].begin(), end = outer_right[y].end(); begin != end; ++begin)
                {
                    const double w = begin->getSecond();
                    current_x = begin->getFirst();
                    
                    if ((current_x >= 0) && (current_x < width))
                    {
                        if (image.getNumberOfChannels() == 3)
                        {
                            *image.get(current_x, y, 0) = (T)(w * (double)pen.getBorderRed()   + (1.0 - w) * (double)*image.get(current_x, y, 0));
                            *image.get(current_x, y, 1) = (T)(w * (double)pen.getBorderGreen() + (1.0 - w) * (double)*image.get(current_x, y, 1));
                            *image.get(current_x, y, 2) = (T)(w * (double)pen.getBorderBlue()  + (1.0 - w) * (double)*image.get(current_x, y, 2));
                        }
                        else for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                            *image.get(current_x, y, c) = (T)(w * (double)pen.getBorderGray()  + (1.0 - w) * (double)*image.get(current_x, y, c));
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DRAWING FUNCTIONS FOR SUB-IMAGE      |
    //                   | WHICH ARE COPIED INSTEAD OF          |
    //                   | REFERENCED                           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE TEXT FUNCTIONS                 |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <template <class> class IMAGE, class T>
    void Draw::Text(IMAGE<T> &image, int x, int y, const Pencil<T> &pen, const Font &font, const char *format, ...)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        int original_x = x;
        va_list args;
        char text[4096];
        
        // Get the text from the format string and the parameters.
        va_start(args, format);
        vsprintf(text, format, args);
        va_end(args);
        
        for (unsigned int character = 0; text[character] != '\0'; ++character)
        {
            if (font.getBreakLine() && (text[character] == '\n'))
            {
                x = original_x;
                y += font.getLineHeight();
            }
            else
            {
                const unsigned char character_value = (unsigned char)text[character];
                int cx, cy, cw, ch;
                
                cx = x + font.getOffsetX(character_value);
                cy = y + font.getOffsetY(character_value);
                cw = (int)font.getWidth(character_value);
                ch = (int)font.getHeight(character_value);
                // Check if the current character is within the image boundaries.
                if ((cx + cw >= 0) && (cy + ch >= 0) && (cx < (int)image.getWidth()) && (cy < (int)image.getHeight()))
                {
                    const unsigned char * bitcode = font.getPixels(character_value);
                    for (int h = cy, i = 0, index = 0; i < ch; ++i, ++h)
                    {
                        for (int w = cx, j = 0; j < cw; ++j, ++w, ++index)
                        {
                            double current_weight = pen_weight * (double)bitcode[index] / 255.0;
                            if ((current_weight > 0.0) && (w >= 0) && (h >= 0) && (w < (int)image.getWidth()) && (h < (int)image.getHeight()))
                            {
                                if (image.getNumberOfChannels() == 3)
                                {
                                    *image.get(w, h, 0) = (T)(current_weight * (double)pen.getBorderRed()   + (1.0 - current_weight) * (double)*image.get(w, h, 0));
                                    *image.get(w, h, 1) = (T)(current_weight * (double)pen.getBorderGreen() + (1.0 - current_weight) * (double)*image.get(w, h, 1));
                                    *image.get(w, h, 2) = (T)(current_weight * (double)pen.getBorderBlue()  + (1.0 - current_weight) * (double)*image.get(w, h, 2));
                                }
                                else
                                {
                                    for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                     *image.get(w, h, c) = (T)(current_weight * (double)pen.getBorderGray() + (1.0 - current_weight) * (double)*image.get(w, h, c));
                                }
                            }
                        }
                    }
                }
                x += font.getAdvance(character_value);
            }
        }
    }
    
    template <class T>
    void Draw::Text(SubImage<T> image, int x, int y, const Pencil<T> &pen, const Font &font, const char *format, ...)
    {
        const double pen_weight = (double)pen.getBorderTransparency() / 255.0;
        int original_x = x;
        va_list args;
        char text[4096];
        
        // Get the text from the format string and the parameters.
        va_start(args, format);
        vsprintf(text, format, args);
        va_end(args);
        
        for (unsigned int character = 0; text[character] != '\0'; ++character)
        {
            if (font.getBreakLine() && (text[character] == '\n'))
            {
                x = original_x;
                y += font.getLineHeight();
            }
            else
            {
                const unsigned char character_value = (unsigned char)text[character];
                int cx, cy, cw, ch;
                
                cx = x + font.getOffsetX(character_value);
                cy = y + font.getOffsetY(character_value);
                cw = (int)font.getWidth(character_value);
                ch = (int)font.getHeight(character_value);
                // Check if the current character is within the image boundaries.
                if ((cx + cw >= 0) && (cy + ch >= 0) && (cx < (int)image.getWidth()) && (cy < (int)image.getHeight()))
                {
                    const unsigned char * bitcode = font.getPixels(character_value);
                    for (int h = cy, i = 0, index = 0; i < ch; ++i, ++h)
                    {
                        for (int w = cx, j = 0; j < cw; ++j, ++w, ++index)
                        {
                            double current_weight = pen_weight * (double)bitcode[index] / 255.0;
                            if ((current_weight > 0.0) && (w >= 0) && (h >= 0) && (w < (int)image.getWidth()) && (h < (int)image.getHeight()))
                            {
                                if (image.getNumberOfChannels() == 3)
                                {
                                    *image.get(w, h, 0) = (T)(current_weight * (double)pen.getBorderRed()   + (1.0 - current_weight) * (double)*image.get(w, h, 0));
                                    *image.get(w, h, 1) = (T)(current_weight * (double)pen.getBorderGreen() + (1.0 - current_weight) * (double)*image.get(w, h, 1));
                                    *image.get(w, h, 2) = (T)(current_weight * (double)pen.getBorderBlue()  + (1.0 - current_weight) * (double)*image.get(w, h, 2));
                                }
                                else
                                {
                                    for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                                     *image.get(w, h, c) = (T)(current_weight * (double)pen.getBorderGray() + (1.0 - current_weight) * (double)*image.get(w, h, c));
                                }
                            }
                        }
                    }
                }
                x += font.getAdvance(character_value);
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

