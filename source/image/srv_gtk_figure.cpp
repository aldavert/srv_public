// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_gtk_figure.hpp"

namespace srv
{
    Figure::Figure(const char *mask, ...)
    {
        char window_name[4096];
        va_list args;
        
        va_start(args, mask);
        vsprintf(window_name, mask, args);
        va_end(args);
        
        if (!isInitialized())
        {
            isInitialized() = true;
            gtk_init(0, NULL);
        }
        m_window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
        gtk_window_set_title(GTK_WINDOW(m_window), window_name);
        gtk_window_set_resizable(GTK_WINDOW(m_window), false);
        gtk_widget_set_size_request(m_window, 320, 240);
        gtk_window_set_deletable(GTK_WINDOW(m_window), false);
        
        {   // Add a icon to the window border.
            const char icon[16][17] = { "                ",
                                        "                ",
                                        "                ",
                                        "                ",
                                        "  ### ###  #  # ",
                                        " #...#...##.##.#",
                                        "#.####.##.#.##.#",
                                        "#.## #.##.#.##.#",
                                        " #..##...##.##.#",
                                        "  ##.#..# #.##.#",
                                        " ###.#.#.##.#.# ",
                                        "#...##.##.#..#  ",
                                        " ###  #  # ##   ",
                                        "                ",
                                        "                ",
                                        "                "};
            unsigned int rowstride;
            guchar *pixels;
            GdkPixbuf *pixbuf;
            
            pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB, true, 8, 16, 16);
            rowstride = gdk_pixbuf_get_rowstride(pixbuf);
            pixels = gdk_pixbuf_get_pixels(pixbuf);
            // Set the background as transparent.
            for (unsigned int y = 0; y < 16; ++y)
            {
                guchar * p = pixels + y * rowstride;
                for (unsigned int x = 0; x < 16; ++x)
                {
                    if (icon[y][x] == ' ')
                    {
                        p[x * 4 + 0] = 0; // Red.
                        p[x * 4 + 1] = 0; // Green.
                        p[x * 4 + 2] = 0; // Blue.
                        p[x * 4 + 3] = 0; // Alpha.
                    }
                    else if (icon[y][x] == '#')
                    {
                        p[x * 4 + 0] = 0; // Red.
                        p[x * 4 + 1] = 0; // Green.
                        p[x * 4 + 2] = 0; // Blue.
                        p[x * 4 + 3] = 255; // Alpha.
                    }
                    else if (icon[y][x] == '.')
                    {
                        p[x * 4 + 0] = 255; // Red.
                        p[x * 4 + 1] = 255; // Green.
                        p[x * 4 + 2] = 255; // Blue.
                        p[x * 4 + 3] = 255; // Alpha.
                    }
                }
            }
            
            gtk_window_set_icon(GTK_WINDOW(m_window), pixbuf);
            g_object_unref(pixbuf);
        }
        
        m_image = gtk_image_new();
        m_event_box = gtk_event_box_new();
        gtk_container_add(GTK_CONTAINER(m_event_box), m_image);
        gtk_container_add(GTK_CONTAINER(m_window), m_event_box);
        
        m_image_data = 0;
        m_mouse_callback = 0;
        m_destroy_event = g_signal_connect(m_window, "destroy", G_CALLBACK(gtk_main_quit), 0);
        m_key_event = g_signal_connect(m_window, "key_press_event", G_CALLBACK(key_press_event), 0);
        gtk_widget_set_events(m_event_box, gtk_widget_get_events(m_event_box) | GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK );
        m_mouse_press_event = 0;
        m_mouse_release_event = 0;
        m_mouse_motion_event = 0;
        gtk_widget_show_all(m_window);
#if GTK_MINOR_VERSION >= 12
        gdk_window_set_event_compression(gtk_widget_get_window(m_window), false);
#endif
        GdkCursorType cursor_type;
        cursor_type = GDK_DOTBOX;
        //cursor_type = GDK_CROSSHAIR;
        //cursor_type = GDK_TCROSS;
        GdkCursor * cursor = gdk_cursor_new_for_display(gdk_display_get_default(), cursor_type);
        gdk_window_set_cursor(gtk_widget_get_window(m_window), cursor);
        g_object_unref(cursor);
    }
    
    Figure::~Figure(void)
    {
        g_signal_handler_disconnect(m_window, m_destroy_event);
        g_signal_handler_disconnect(m_window, m_key_event);
        if (m_mouse_callback != 0)
        {
            g_signal_handler_disconnect(m_event_box, m_mouse_press_event);
            g_signal_handler_disconnect(m_event_box, m_mouse_release_event);
            g_signal_handler_disconnect(m_event_box, m_mouse_motion_event);
        }
        gtk_widget_destroy(m_window);
        if (m_image_data != 0) delete [] m_image_data;
    }
    
    void Figure::setTitle(const char * mask, ...)
    {
        char window_name[4096];
        va_list args;
        
        va_start(args, mask);
        vsprintf(window_name, mask, args);
        va_end(args);
        
        gtk_window_set_title(GTK_WINDOW(m_window), window_name);
    }
    
    void Figure::setKeyboardCallback(const FigureKeyboardCallback * keyboard_callback)
    {
        g_signal_handler_disconnect(m_window, m_key_event);                                                                         // Disconnect the key handler.
        m_key_event = g_signal_connect(m_window, "key_press_event", G_CALLBACK(key_press_event), (gpointer)keyboard_callback);      // Reconnect the handler with the callback function.
    }
    
    void Figure::setMouseCallback(const FigureMouseCallback * mouse_callback)
    {
        // Remove the current mouse callbacks...
        if (m_mouse_callback != 0)
        {
            g_signal_handler_disconnect(m_event_box, m_mouse_press_event);
            g_signal_handler_disconnect(m_event_box, m_mouse_release_event);
            g_signal_handler_disconnect(m_event_box, m_mouse_motion_event);
        }
        m_mouse_callback = mouse_callback;
        // Reconnect the new callbacks for the new mouse callback.
        if (mouse_callback != 0)
        {
            m_mouse_press_event   = g_signal_connect(m_event_box, "button-press-event",   G_CALLBACK(mouse_press_callback),   (gpointer)m_mouse_callback);
            m_mouse_release_event = g_signal_connect(m_event_box, "button-release-event", G_CALLBACK(mouse_release_callback), (gpointer)m_mouse_callback);
            m_mouse_motion_event  = g_signal_connect(m_event_box, "motion_notify_event",  G_CALLBACK(mouse_motion_callback),  (gpointer)m_mouse_callback);
        }
    }
    
    char Figure::wait(int time)
    {
        keyPress() = 0;
        if (time <= 0)
        {
            gtk_main();
        }
        else
        {
            guint id = g_timeout_add((guint)time, timer_stop, 0);
            gtk_main();
            if (g_main_context_find_source_by_id(0, id))            // Needed before?
                g_source_remove(id);
        }
        return (char)keyPress();
    }
    
    int Figure::key_press_event(GtkWidget * /* widget */, GdkEventKey * event, gpointer user_data)
    {
        FigureKeyboardCallback * keyboard_data = (FigureKeyboardCallback *)user_data;
        int key_press, key_status, key_value;
        
        if (keyboard_data != 0) key_status = keyboard_data->getStatus();
        else key_status = keyStatus();
        
        key_value = (int)event->keyval;
        key_press = gdk_keyval_to_unicode(key_value);
        if ((key_value != KEY_SHIFT) &&
            (key_value != KEY_CTRL) &&
            (key_value != KEY_ALT) &&
            (key_value != KEY_ALT_GR) &&
            (key_value != KEY_SUPER_LEFT) &&
            (key_value != KEY_SUPER_RIGHT) &&
            (key_value != KEY_CMD))
        {
            if (key_status == KEY_GRAVE)
            {
                if      (key_press == 'A') key_press = 192;
                else if (key_press == 'E') key_press = 200;
                else if (key_press == 'I') key_press = 204;
                else if (key_press == 'O') key_press = 210;
                else if (key_press == 'U') key_press = 217;
                else if (key_press == 'a') key_press = 224;
                else if (key_press == 'e') key_press = 232;
                else if (key_press == 'i') key_press = 236;
                else if (key_press == 'o') key_press = 242;
                else if (key_press == 'u') key_press = 249;
                key_status = -1;
            }
            else if (key_status == KEY_ACUTE)
            {
                if      (key_press == 'A') key_press = 193;
                else if (key_press == 'E') key_press = 201;
                else if (key_press == 'I') key_press = 205;
                else if (key_press == 'O') key_press = 211;
                else if (key_press == 'U') key_press = 218;
                else if (key_press == 'Y') key_press = 221;
                else if (key_press == 'a') key_press = 225;
                else if (key_press == 'e') key_press = 233;
                else if (key_press == 'i') key_press = 237;
                else if (key_press == 'o') key_press = 243;
                else if (key_press == 'u') key_press = 250;
                else if (key_press == 'y') key_press = 253;
                key_status = -1;
            }
            else if (key_status == KEY_CIRCUMFLEX)
            {
                if      (key_press == 'A') key_press = 194;
                else if (key_press == 'E') key_press = 202;
                else if (key_press == 'I') key_press = 206;
                else if (key_press == 'O') key_press = 212;
                else if (key_press == 'U') key_press = 219;
                else if (key_press == 'a') key_press = 226;
                else if (key_press == 'e') key_press = 234;
                else if (key_press == 'i') key_press = 238;
                else if (key_press == 'o') key_press = 244;
                else if (key_press == 'u') key_press = 251;
                key_status = -1;
            }
            else if (key_status == KEY_DIAERESIS)
            {
                if      (key_press == 'A') key_press = 196;
                else if (key_press == 'E') key_press = 203;
                else if (key_press == 'I') key_press = 207;
                else if (key_press == 'O') key_press = 214;
                else if (key_press == 'U') key_press = 220;
                else if (key_press == 'a') key_press = 228;
                else if (key_press == 'e') key_press = 235;
                else if (key_press == 'i') key_press = 239;
                else if (key_press == 'o') key_press = 246;
                else if (key_press == 'u') key_press = 252;
                else if (key_press == 'y') key_press = 255;
                key_status = -1;
            }
        }
        if      (key_value == KEY_GRAVE)        key_status = KEY_GRAVE;
        else if (key_value == KEY_ACUTE)        key_status = KEY_ACUTE;
        else if (key_value == KEY_CIRCUMFLEX)   key_status = KEY_CIRCUMFLEX;
        else if (key_value == KEY_DIAERESIS)    key_status = KEY_DIAERESIS;
        else if (key_value == KEY_SHIFT)        key_status = KEY_SHIFT;
        else if (key_value == KEY_CTRL)         key_status = KEY_CTRL;
        else if (key_value == KEY_ALT)          key_status = KEY_ALT;
        else if (key_value == KEY_ALT_GR)       key_status = KEY_ALT_GR;
        else if (key_value == KEY_SUPER_LEFT)   key_status = KEY_SUPER_LEFT;
        else if (key_value == KEY_SUPER_RIGHT)  key_status = KEY_SUPER_RIGHT;
        else if (key_value == KEY_CMD)          key_status = KEY_CMD;
        else if (key_value == KEY_KEYPAD_LEFT)  key_status = KEY_KEYPAD_LEFT;
        else if (key_value == KEY_KEYPAD_UP)    key_status = KEY_KEYPAD_UP;
        else if (key_value == KEY_KEYPAD_RIGHT) key_status = KEY_KEYPAD_RIGHT;
        else if (key_value == KEY_KEYPAD_DOWN)  key_status = KEY_KEYPAD_DOWN;
        else if (key_value == KEY_LEFT)         key_status = KEY_LEFT;
        else if (key_value == KEY_UP)           key_status = KEY_UP;
        else if (key_value == KEY_RIGHT)        key_status = KEY_RIGHT;
        else if (key_value == KEY_DOWN)         key_status = KEY_DOWN;
        else if (key_value == KEY_HOME)         key_status = KEY_HOME;
        else if (key_value == KEY_END)          key_status = KEY_END;
        else if (key_value == KEY_NEXT_PAGE)    key_status = KEY_NEXT_PAGE;
        else if (key_value == KEY_PREV_PAGE)    key_status = KEY_PREV_PAGE;
        else key_status = KEY_NONE;
        /***
        if      (key_value == 65104) key_status =  0; // 65104 ` GRAVE
        else if (key_value == 65105) key_status =  1; // 65105 ´ ACUTE
        else if (key_value == 65106) key_status =  2; // 65106 ^ CIRCUMFLEX
        else if (key_value == 65111) key_status =  3; // 65111 " DIAERESIS
        else if (key_value == 65505) key_status =  4; // SHIFT
        else if (key_value == 65507) key_status =  5; // CTRL
        else if (key_value == 65513) key_status =  6; // ALT
        else if (key_value == 65027) key_status =  7; // ALT GR.
        else if (key_value == 65515) key_status =  8; // SUPER LEFT
        else if (key_value == 65516) key_status =  9; // SUPER RIGHT
        else if (key_value == 65383) key_status = 10; // CMD
        else if (key_value == 65430) key_status = 11; // 65430 // KEYPAD LEFT
        else if (key_value == 65431) key_status = 12; // 65431 // KEYPAD UP
        else if (key_value == 65432) key_status = 13; // 65432 // KEYPAD RIGHT
        else if (key_value == 65433) key_status = 14; // 65433 // KEYPAD DOWN
        else if (key_value == 65361) key_status = 15; // 65361 // LEFT
        else if (key_value == 65362) key_status = 16; // 65362 // UP
        else if (key_value == 65363) key_status = 17; // 65363 // RIGHT
        else if (key_value == 65364) key_status = 18; // 65364 // DOWN
        else key_status = KEY_NONE;
        */
        
        if (keyboard_data != 0)
        {
            keyboard_data->setData(key_press, key_status);
            if (keyboard_data->processKey())
            {
                keyPress() = key_press;
                keyStatus() = key_status;
                gtk_main_quit();
            }
        }
        else
        {
            keyPress() = key_press;
            keyStatus() = key_status;
            gtk_main_quit();
        }
        return 1;
    }
    
    gboolean Figure::mouse_press_callback(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
    {
        if ((user_data != 0) && (event->type == GDK_BUTTON_PRESS))
        {
            gint width, height;
            FigureMouseCallback * mouse_data = (FigureMouseCallback *)user_data;
            gtk_window_get_size(GTK_WINDOW(gtk_widget_get_toplevel(widget)), &width, &height);
            gint width_image, height_image;
            GList * list = gtk_container_get_children(GTK_CONTAINER(widget));
            const GdkPixbuf *pb = gtk_image_get_pixbuf(GTK_IMAGE(list->data));
            width_image = gdk_pixbuf_get_width(pb);
            height_image = gdk_pixbuf_get_height(pb);
            g_list_free(list);
            mouse_data->setX((unsigned int)srvMax<double>(0.0, srvMin<double>(event->x - (width - width_image) / 2, width)));
            mouse_data->setY((unsigned int)srvMax<double>(0.0, srvMin<double>(event->y - (height - height_image) / 2, height)));
            switch (event->button)
            {
            case 1:
                mouse_data->setButton(BUTTON_LEFT);
                break;
            case 3:
                mouse_data->setButton(BUTTON_RIGHT);
                break;
            case 2:
                mouse_data->setButton(BUTTON_CENTER);
                break;
            default:
                mouse_data->setButton(BUTTON_NONE);
                break;
            }
            if      ((event->state & GDK_SHIFT_MASK) == GDK_SHIFT_MASK) mouse_data->setKeyMask(MASK_SHIFT);
            else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) mouse_data->setKeyMask(MASK_CONTROL);
            else mouse_data->setKeyMask(MASK_NONE);
            mouse_data->processButtonPress();
        }
        return true;
    }
    
    gboolean Figure::mouse_release_callback(GtkWidget *widget, GdkEventButton *event, gpointer user_data)
    {
        if (user_data != 0)
        {
            gint width, height;
            FigureMouseCallback *mouse_data = (FigureMouseCallback*)user_data;
            gtk_window_get_size(GTK_WINDOW(gtk_widget_get_toplevel(widget)), &width, &height);
            gint width_image, height_image;
            GList * list = gtk_container_get_children(GTK_CONTAINER(widget));
            const GdkPixbuf *pb = gtk_image_get_pixbuf(GTK_IMAGE(list->data));
            width_image = gdk_pixbuf_get_width(pb);
            height_image = gdk_pixbuf_get_height(pb);
            g_list_free(list);
            mouse_data->setX((unsigned int)srvMax<double>(0.0, srvMin<double>(event->x - (width - width_image) / 2, width)));
            mouse_data->setY((unsigned int)srvMax<double>(0.0, srvMin<double>(event->y - (height - height_image) / 2, height)));
            mouse_data->processButtonRelease();
            mouse_data->setButton(BUTTON_NONE);
            if ((event->state & GDK_SHIFT_MASK) == GDK_SHIFT_MASK) mouse_data->setKeyMask(MASK_SHIFT);
            else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) mouse_data->setKeyMask(MASK_CONTROL);
            else mouse_data->setKeyMask(MASK_NONE);
        }
        return true;
    }
    
    gboolean Figure::mouse_motion_callback(GtkWidget *widget, GdkEventMotion *event, gpointer user_data)
    {
        if (user_data != 0)
        {
            gint width, height;
            FigureMouseCallback *mouse_data = (FigureMouseCallback*)user_data;
            gtk_window_get_size(GTK_WINDOW(gtk_widget_get_toplevel(widget)), &width, &height);
            gint width_image, height_image;
            GList * list = gtk_container_get_children(GTK_CONTAINER(widget));
            const GdkPixbuf *pb = gtk_image_get_pixbuf(GTK_IMAGE(list->data));
            width_image = gdk_pixbuf_get_width(pb);
            height_image = gdk_pixbuf_get_height(pb);
            g_list_free(list);
            mouse_data->setX((unsigned int)srvMax<double>(0.0, srvMin<double>(event->x - (width - width_image) / 2, width)));
            mouse_data->setY((unsigned int)srvMax<double>(0.0, srvMin<double>(event->y - (height - height_image) / 2, height)));
            if      ((event->state & GDK_BUTTON1_MASK) == GDK_BUTTON1_MASK) mouse_data->setButton(BUTTON_LEFT);
            else if ((event->state & GDK_BUTTON3_MASK) == GDK_BUTTON3_MASK) mouse_data->setButton(BUTTON_RIGHT);
            else if ((event->state & GDK_BUTTON2_MASK) == GDK_BUTTON2_MASK) mouse_data->setButton(BUTTON_CENTER);
            else mouse_data->setButton(BUTTON_NONE);
            if      ((event->state & GDK_SHIFT_MASK) == GDK_SHIFT_MASK) mouse_data->setKeyMask(MASK_SHIFT);
            else if ((event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) mouse_data->setKeyMask(MASK_CONTROL);
            else mouse_data->setKeyMask(MASK_NONE);
            mouse_data->processMouseMove();
        }
        return true;
    }
}

