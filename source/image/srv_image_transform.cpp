// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_image_transform.hpp"
#include "../srv_xml.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | IMAGE WRAPPING FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // =[ CONSTRUCTORS, DESTRUCTOR AND ASSIGNATION OPERATOR ]========================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    TransformValues::TransformValues(void) :
        m_src_width(0),
        m_src_height(0),
        m_dst_width(0),
        m_dst_height(0),
        m_offsets_x(0),
        m_offsets_y(0),
        m_weights(0)
    {
    }
    
    TransformValues::TransformValues(const double * transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads) :
        m_src_width(src_width),
        m_src_height(src_height),
        m_dst_width(dst_width),
        m_dst_height(dst_height),
        m_offsets_x(new int[dst_width * dst_height]),
        m_offsets_y(new int[dst_width * dst_height]),
        m_weights(new double[dst_width * dst_height * 4])
    {
        setTransform(transform, number_of_threads);
    }
    
    TransformValues::TransformValues(const Matrix<double> &transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads) :
        m_src_width(src_width),
        m_src_height(src_height),
        m_dst_width(dst_width),
        m_dst_height(dst_height),
        m_offsets_x(new int[dst_width * dst_height]),
        m_offsets_y(new int[dst_width * dst_height]),
        m_weights(new double[dst_width * dst_height * 4])
    {
        setTransform(transform(), number_of_threads);
    }
    
    TransformValues::TransformValues(unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads) :
        m_src_width(src_width),
        m_src_height(src_height),
        m_dst_width(dst_width),
        m_dst_height(dst_height),
        m_offsets_x(new int[dst_width * dst_height]),
        m_offsets_y(new int[dst_width * dst_height]),
        m_weights(new double[dst_width * dst_height * 4])
    {
        setResize(number_of_threads);
    }
    
    TransformValues::TransformValues(double angle, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads) :
        m_src_width(src_width),
        m_src_height(src_height),
        m_dst_width(dst_width),
        m_dst_height(dst_height),
        m_offsets_x(new int[dst_width * dst_height]),
        m_offsets_y(new int[dst_width * dst_height]),
        m_weights(new double[dst_width * dst_height * 4])
    {
        setRotation(angle, number_of_threads);
    }
    
    TransformValues::TransformValues(const double * x, const double * y, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads) :
        m_src_width(src_width),
        m_src_height(src_height),
        m_dst_width(dst_width),
        m_dst_height(dst_height),
        m_offsets_x(new int[dst_width * dst_height]),
        m_offsets_y(new int[dst_width * dst_height]),
        m_weights(new double[dst_width * dst_height * 4])
    {
        setMapping(x, y, number_of_threads);
    }
    
    TransformValues::TransformValues(const TransformValues &other) :
        m_src_width(other.m_src_width),
        m_src_height(other.m_src_height),
        m_dst_width(other.m_dst_width),
        m_dst_height(other.m_dst_height),
        m_offsets_x((other.m_offsets_x != 0)?new int[other.m_dst_width * other.m_dst_height]:0),
        m_offsets_y((other.m_offsets_y != 0)?new int[other.m_dst_width * other.m_dst_height]:0),
        m_weights((other.m_weights != 0)?new double[other.m_dst_width * other.m_dst_height * 4]:0)
    {
        for (unsigned int i = 0, j = 0; i < other.m_dst_width * other.m_dst_height; ++i, j += 4)
        {
            m_offsets_x[i] = other.m_offsets_x[i];
            m_offsets_y[i] = other.m_offsets_y[i];
            m_weights[j] = other.m_weights[j];
            m_weights[j + 1] = other.m_weights[j + 1];
            m_weights[j + 2] = other.m_weights[j + 2];
            m_weights[j + 3] = other.m_weights[j + 3];
        }
    }
    
    TransformValues::~TransformValues(void)
    {
        if (m_offsets_x != 0) delete [] m_offsets_x;
        if (m_offsets_y != 0) delete [] m_offsets_y;
        if (m_weights != 0) delete [] m_weights;
    }
    
    TransformValues& TransformValues::operator=(const TransformValues &other)
    {
        if (this != &other)
        {
            // Free .................................................................................................................................
            if (m_offsets_x != 0) delete [] m_offsets_x;
            if (m_offsets_y != 0) delete [] m_offsets_y;
            if (m_weights != 0) delete [] m_weights;
            
            // Copy .................................................................................................................................
            m_src_width = other.m_src_width;
            m_src_height = other.m_src_height;
            m_dst_width = other.m_dst_width;
            m_dst_height = other.m_dst_height;
            if ((other.m_dst_width > 0) && (other.m_dst_height > 0))
            {
                m_offsets_x = new int[other.m_dst_width * other.m_dst_height];
                m_offsets_y = new int[other.m_dst_width * other.m_dst_height];
                m_weights = new double[other.m_dst_width * other.m_dst_height * 4];
                
                for (unsigned int i = 0, j = 0; i < other.m_dst_width * other.m_dst_height; ++i, j += 4)
                {
                    m_offsets_x[i] = other.m_offsets_x[i];
                    m_offsets_y[i] = other.m_offsets_y[i];
                    m_weights[j] = other.m_weights[j];
                    m_weights[j + 1] = other.m_weights[j + 1];
                    m_weights[j + 2] = other.m_weights[j + 2];
                    m_weights[j + 3] = other.m_weights[j + 3];
                }
            }
            else
            {
                m_offsets_x = 0;
                m_offsets_y = 0;
                m_weights = 0;
            }
        }
        
        return *this;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ INITIALIZATION FUNCTIONS ]=================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    void TransformValues::set(const double * transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_offsets_x != 0) delete [] m_offsets_x;
        if (m_offsets_y != 0) delete [] m_offsets_y;
        if (m_weights != 0) delete [] m_weights;
        
        // Initialize ...............................................................................................................................
        m_src_width = src_width;
        m_src_height = src_height;
        m_dst_width = dst_width;
        m_dst_height = dst_height;
        m_offsets_x = new int[dst_width * dst_height];
        m_offsets_y = new int[dst_width * dst_height];
        m_weights = new double[dst_width * dst_height * 4];
        
        // Calculate transform values ...............................................................................................................
        setTransform(transform, number_of_threads);
    }
    
    void TransformValues::set(unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_offsets_x != 0) delete [] m_offsets_x;
        if (m_offsets_y != 0) delete [] m_offsets_y;
        if (m_weights != 0) delete [] m_weights;
        
        // Initialize ...............................................................................................................................
        m_src_width = src_width;
        m_src_height = src_height;
        m_dst_width = dst_width;
        m_dst_height = dst_height;
        m_offsets_x = new int[dst_width * dst_height];
        m_offsets_y = new int[dst_width * dst_height];
        m_weights = new double[dst_width * dst_height * 4];
        
        // Calculate transform values ...............................................................................................................
        setResize(number_of_threads);
    }
    
    void TransformValues::set(double angle, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_offsets_x != 0) delete [] m_offsets_x;
        if (m_offsets_y != 0) delete [] m_offsets_y;
        if (m_weights != 0) delete [] m_weights;
        
        // Initialize ...............................................................................................................................
        m_src_width = src_width;
        m_src_height = src_height;
        m_dst_width = dst_width;
        m_dst_height = dst_height;
        m_offsets_x = new int[dst_width * dst_height];
        m_offsets_y = new int[dst_width * dst_height];
        m_weights = new double[dst_width * dst_height * 4];
        
        // Calculate transform values ...............................................................................................................
        setRotation(angle, number_of_threads);
    }
    
    void TransformValues::set(const double * vector_x, const double * vector_y, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_offsets_x != 0) delete [] m_offsets_x;
        if (m_offsets_y != 0) delete [] m_offsets_y;
        if (m_weights != 0) delete [] m_weights;
        
        // Initialize ...............................................................................................................................
        m_src_width = src_width;
        m_src_height = src_height;
        m_dst_width = dst_width;
        m_dst_height = dst_height;
        m_offsets_x = new int[dst_width * dst_height];
        m_offsets_y = new int[dst_width * dst_height];
        m_weights = new double[dst_width * dst_height * 4];
        
        // Calculate transform values ...............................................................................................................
        setMapping(vector_x, vector_y, number_of_threads);
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ AUXILIARY INITIALIZATION FUNCTIONS ]=======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    void TransformValues::setTransform(const double * transform, unsigned int number_of_threads)
    {
        const double src_center_x = (double)m_src_width / 2.0;
        const double src_center_y = (double)m_src_height / 2.0;
        const double dst_center_x = (double)m_dst_width / 2.0;
        const double dst_center_y = (double)m_dst_height / 2.0;
        const double normalization_src = (double)srvMax(m_src_width, m_src_height);
        const double normalization_dst = (double)srvMax(m_dst_width, m_dst_height);
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            unsigned int index_offset = thread_identifier * m_dst_width;
            
            for (unsigned int y = thread_identifier; y < m_dst_height; y += number_of_threads)
            {
                for (unsigned int x = 0; x < m_dst_width; ++x, ++index_offset)
                {
                    double rx, ry, cx, cy, tx, ty, tz;
                    
                    // Calculate the coordinates of the destination image pixel in the source image (Coordinates <rx, ry>).
                    // --------------------------------------------------------------------------------------------------------------
                    cx = ((double)x - dst_center_x) / normalization_dst;
                    cy = ((double)y - dst_center_y) / normalization_dst;
                    tx = transform[0] * cx + transform[3] * cy + transform[6];
                    ty = transform[1] * cx + transform[4] * cy + transform[7];
                    tz = transform[2] * cx + transform[5] * cy + transform[8];
                    rx = (tx / tz) * normalization_src + src_center_x;
                    ry = (ty / tz) * normalization_src + src_center_y;
                    // --------------------------------------------------------------------------------------------------------------
                    setMappingValue(rx, ry, index_offset);
                }
                index_offset += (number_of_threads - 1) * m_dst_width;
            }
        }
    }
    
    void TransformValues::setResize(unsigned int number_of_threads)
    {
        const double factor_x = (double)m_src_width / (double)m_dst_width;
        const double factor_y = (double)m_src_height / (double)m_dst_height;
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            unsigned int index_offset = thread_identifier * m_dst_width;
            
            for (unsigned int y = thread_identifier; y < m_dst_height; y += number_of_threads)
            {
                for (unsigned int x = 0; x < m_dst_width; ++x, ++index_offset)
                {
                    double rx, ry;
                    
                    // Calculate the coordinates of the destination image pixel in the source image (Coordinates <rx, ry>).
                    // --------------------------------------------------------------------------------------------------------------
                    rx = (double)x * factor_x;
                    ry = (double)y * factor_y;
                    // --------------------------------------------------------------------------------------------------------------
                    setMappingValue(rx, ry, index_offset);
                }
                index_offset += (number_of_threads - 1) * m_dst_width;
            }
        }
    }
    
    void TransformValues::setRotation(double angle, unsigned int number_of_threads)
    {
        const double cos_angle = cos(angle);
        const double sin_angle = sin(angle);
        const double src_center_x = (double)m_src_width / 2.0;
        const double src_center_y = (double)m_src_height / 2.0;
        const double dst_center_x = (double)m_dst_width / 2.0;
        const double dst_center_y = (double)m_dst_height / 2.0;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            unsigned int index_offset = thread_identifier * m_dst_width;
            
            for (unsigned int y = thread_identifier; y < m_dst_height; y += number_of_threads)
            {
                for (unsigned int x = 0; x < m_dst_width; ++x, ++index_offset)
                {
                    double cx, cy, rx, ry;
                    
                    // Calculate the coordinates of the destination image pixel in the source image (Coordinates <rx, ry>).
                    // --------------------------------------------------------------------------------------------------------------
                    cx = (double)x - dst_center_x;
                    cy = (double)y - dst_center_y;
                    rx =  cx * cos_angle +  cy * sin_angle + src_center_x;
                    ry = -cx * sin_angle +  cy * cos_angle + src_center_y;
                    // --------------------------------------------------------------------------------------------------------------
                    setMappingValue(rx, ry, index_offset);
                }
                index_offset += (number_of_threads - 1) * m_dst_width;
            }
        }
    }
    
    void TransformValues::setMapping(const double * vector_x, const double * vector_y, unsigned int number_of_threads)
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            unsigned int index_offset = thread_identifier * m_dst_width;
            
            for (unsigned int y = thread_identifier; y < m_dst_height; y += number_of_threads)
            {
                for (unsigned int x = 0; x < m_dst_width; ++x, ++index_offset)
                {
                    double rx, ry;
                    
                    // Calculate the coordinates of the destination image pixel in the source image (Coordinates <rx, ry>).
                    // --------------------------------------------------------------------------------------------------------------
                    rx = vector_x[index_offset];
                    ry = vector_y[index_offset];
                    // --------------------------------------------------------------------------------------------------------------
                    setMappingValue(rx, ry, index_offset);
                }
                index_offset += (number_of_threads - 1) * m_dst_width;
            }
        }
    }
    
    void TransformValues::setMappingValue(double rx, double ry, unsigned int index)
    {
        const unsigned int index_weight = index * 4;
        int px, py;
        
        px = (int)rx;
        py = (int)ry;
        // If the resulting pixel is within the source image boundaries...
        if ((px >= 0) && (py >= 0) && (px < (int)m_src_width) && (py < (int)m_src_height))
        {
            double w00, w01, w10, w11, dx, dy;
            
            // Check if the corresponding bilinear point will be outside the image and calculate
            // the residual weight for each direction.
            if (px == (int)m_src_width - 1) { --px; dx = 1.0; }
            else dx = rx - (double)px;
            if (py == (int)m_src_height - 1) { --py; dy = 1.0; }
            else dy = ry - (double)py;
            
            // Calculate the bilinear weights.
            w00 = (1.0 - dx) * (1.0 - dy);
            w01 = dx * (1.0 - dy);
            w10 = (1.0 - dx) * dy;
            w11 = dx * dy;
            
            // Set the source coordinates and weights.
            m_offsets_x[index] = px;
            m_offsets_y[index] = py;
            m_weights[index_weight] = w00;
            m_weights[index_weight + 1] = w01;
            m_weights[index_weight + 2] = w10;
            m_weights[index_weight + 3] = w11;
        }
        else // otherwise, set the offsets as out pixels.
        {
            m_offsets_x[index] = -1;
            m_offsets_y[index] = -1;
            m_weights[index_weight] = 0.0;
            m_weights[index_weight + 1] = 0.0;
            m_weights[index_weight + 2] = 0.0;
            m_weights[index_weight + 3] = 0.0;
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ XML FUNCTIONS ]============================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    void TransformValues::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Transform_Values");
        parser.setAttribute("Source_Width", m_src_width);
        parser.setAttribute("Source_Height", m_src_height);
        parser.setAttribute("Destination_Width", m_dst_width);
        parser.setAttribute("Destination_Height", m_dst_height);
        parser.addChildren();
        parser.openTag("Offset_X");
        parser.addChildren();
        writeToBase64Array(parser, m_offsets_x, m_dst_width * m_dst_height);
        parser.closeTag();
        parser.openTag("Offset_Y");
        parser.addChildren();
        writeToBase64Array(parser, m_offsets_y, m_dst_width * m_dst_height);
        parser.closeTag();
        parser.openTag("Weights");
        parser.addChildren();
        writeToBase64Array(parser, m_weights, m_dst_width * m_dst_height * 4);
        parser.closeTag();
        parser.closeTag();
    }
    
    void TransformValues::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Transform_Values"))
        {
            // Free .................................................................................................................................
            if (m_offsets_x != 0) delete [] m_offsets_x;
            if (m_offsets_y != 0) delete [] m_offsets_y;
            if (m_weights != 0) delete [] m_weights;
            
            // Retrieve images geometry .............................................................................................................
            m_src_width = parser.getAttribute("Source_Width");
            m_src_height = parser.getAttribute("Source_Height");
            m_dst_width = parser.getAttribute("Destination_Width");
            m_dst_height = parser.getAttribute("Destination_Height");
            
            // Allocate memory ......................................................................................................................
            if ((m_dst_width > 0) && (m_dst_height > 0))
            {
                m_offsets_x = new int[m_dst_width * m_dst_height];
                m_offsets_y = new int[m_dst_width * m_dst_height];
                m_weights = new double[m_dst_width * m_dst_height * 4];
            }
            else
            {
                m_offsets_x = 0;
                m_offsets_y = 0;
                m_weights = 0;
            }
            
            // Retrieve vector information from the XML body ........................................................................................
            while (!(parser.isTagIdentifier("Transform_Values") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Offset_X"))
                {
                    while (!(parser.isTagIdentifier("Offset_X") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Offset_X", m_offsets_x, m_dst_width * m_dst_height);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Offset_Y"))
                {
                    while (!(parser.isTagIdentifier("Offset_Y") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Offset_Y", m_offsets_y, m_dst_width * m_dst_height);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Weights"))
                {
                    while (!(parser.isTagIdentifier("Weights") && parser.isCloseTag()))
                    {
                        if (parser.isValue()) readFromBase64Array(parser, "Weights", m_weights, m_dst_width * m_dst_height * 4);
                        else parser.getNext();
                    }
                    parser.getNext();
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

