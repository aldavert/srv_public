// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_SEGMENTATION_HEADER_FILE__
#define __SRV_IMAGE_SEGMENTATION_HEADER_FILE__
#include <omp.h>
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "srv_image.hpp"
#include "srv_image_integral.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | UNSUPERVISED SEGMENTATION FUNCTIONS  |
    //                   | AND CLASSES DECLARATION              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // Class with unsupervised segmentation algorithms.
    class Segmentation
    {
    public:
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ IMAGE SEGMENTATION FUNCTIONS ]=============================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** This function implements the connected-components labeling algorithm.
         *  \param[in] src source image to label.
         *  \param[in] connectivity identifier of the connectivity method used to define the neighborhood between pixels.
         *  \param[out] dst resulting connected components image.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the image connected components.
         *  \return number of connected components found in the image.
         */
        template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST>
        static unsigned int ConnectedComponents(const IMAGE_SRC<TSRC> &src, IMAGE_CONNECTIVITY connectivity, IMAGE_DST<TDST> &dst, unsigned int number_of_threads);
        /** This function calculates the watershed of an image.
         *  \param[in] src source image
         *  \param[in] connectivity identifier of the method used to define the connectivity between pixel neighbors.
         *  \param[out] dst resulting image with the watershed regions.
         *  \param[in] number_of_threads number of threads used to calculate the watershed algorithm.
         */
        template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST>
        static unsigned int Watershed(const IMAGE_SRC<TSRC> &src, IMAGE_CONNECTIVITY connectivity, IMAGE_DST<TDST> &dst, unsigned int number_of_threads);
        
        /** Thinning algorithm to extract the skeleton of a (binary) image.
         *  \param[in] input input image.
         *  \param[in] background_value value of the pixel assigned to background.
         *  \param[out] output resulting thin image.
         *  \param[in] number_of_threads number of threads used to process the image concurrently.
         */
        template <class TINPUT, class TOUTPUT> static void thinning(const ConstantSubImage<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method, SubImage<TOUTPUT>  output, unsigned int number_of_threads);
        // Inline functions calls with variations of the input/output parameters.
        template <class TINPUT, class TOUTPUT> static void thinning(const            Image<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); thinning(ConstantSubImage<TINPUT>(input), background_value, method, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void thinning(const            Image<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                          ; thinning(ConstantSubImage<TINPUT>(input), background_value, method,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void thinning(const         SubImage<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); thinning(ConstantSubImage<TINPUT>(input), background_value, method, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void thinning(const         SubImage<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                          ; thinning(ConstantSubImage<TINPUT>(input), background_value, method,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void thinning(const ConstantSubImage<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); thinning(                         input , background_value, method, SubImage<TOUTPUT>(output), number_of_threads); }
        
        /** Algorithm to detect joints in a thin image.
         *  \param[in] input input thin image.
         *  \param[in] background_value value of the pixel assigned to background.
         *  \param[out] coordinates list with <x, y, channel>-tuples with the positions of the joints in the image.
         *  \param[in] number_of_threads number of threads used to process the image concurrently.
         */
        template <template <class> class IMAGE, class TIMAGE> static void detectJoints(const IMAGE<TIMAGE> &input, TIMAGE background, std::list<std::tuple<int, int, int> > &coordinates, unsigned int number_of_threads);
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SLIC CLASS DECLARATION ]===================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Simple Linear Interactive Clustering (SLIC) algorithm to group image pixels into super-pixels. This function implements
         *  the algorithm proposed in:<br>
         *  Radhakrishna Achanta, Appu Shaji, Kevin Smith, Aurelien Lucchi, Pascal Fua, and Sabine Süsstrunk,
         *  <b>"SLIC Superpixels Compared to State-of-the-art Superpixel Methods</b>,
         *  <i>IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 34, num. 11, p. 2274 - 2282, May 2012</i>.
         *  
         *  \param[in] source_image original image.
         *  \param[in] step separation between seeds.
         *  \param[in] compactness compactness value. The higher the values, the more compact the resulting super-pixels are.
         *  \param[in] perturb_seeds enable or disable to perturb the location initial seeds to avoid edges.
         *  \param[in] convert_cielab enable or disable to convert the image to the CIE Lab color space.
         *  \param[out] cluster_selected image with the label assigned to each pixel of the image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         *  \returns number of super-pixels extracted from the image.
         *  \note Reference the authors PAMI paper when using this algorithm.
         */
        template <template <class> class IMAGE, class T>
        static unsigned int Slic(const IMAGE<T> &source_image, unsigned int step, double compactness, bool perturb_seeds, bool convert_cielab, Image<unsigned int> &cluster_selected, unsigned int number_of_threads);
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ ADAPTIVE SLIC CLASS DECLARATION ]==========================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Version of the Simple Linear Interactive Clustering (SLIC) algorithm which adaptively selects the compactness
         *  of the super-pixels, so that, only the only parameter set by the user is the step size.
         *  This function implements the algorithm proposed in:<br>
         *  Radhakrishna Achanta, Appu Shaji, Kevin Smith, Aurelien Lucchi, Pascal Fua, and Sabine Süsstrunk,
         *  <b>"SLIC Superpixels Compared to State-of-the-art Superpixel Methods</b>,
         *  <i>IEEE Transactions on Pattern Analysis and Machine Intelligence, vol. 34, num. 11, p. 2274 - 2282, May 2012</i>.
         *  
         *  \param[in] source_image original image.
         *  \param[in] step separation between seeds.
         *  \param[in] perturb_seeds enable or disable to perturb the location initial seeds to avoid edges.
         *  \param[in] convert_cielab enable or disable to convert the image to the CIE Lab color space.
         *  \param[out] cluster_selected image with the label assigned to each pixel of the image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         *  \returns number of super-pixels extracted from the image.
         *  \note Reference the authors PAMI paper when using this algorithm.
         */
        template <template <class> class IMAGE, class T>
        static unsigned int AdaptiveSlic(const IMAGE<T> &source_image, unsigned int step, bool perturb_seeds, bool convert_cielab, Image<unsigned int> &cluster_selected, unsigned int number_of_threads);
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SEEDS ALGORITHM CLASS DECLARATION ]========================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** This class implements the Superpixels Extracted via Energy-Driven Sampling (SEEDS) algorithm. Details of the algorithm
         *  can be found in:
         *  Michael Van den Bergh, Xavier Boix, Gemma Roig, Benjamin de Capitani and Luc Van Gool, <b>"SEEDS: Superpixels Extracted
         *  via Energy-Driven Sampling"</b>, <i>Proceedings of the 12th European Conference on Computer Vision (ECCV), October 2012,
         *  Firenze, Italy.</i>
         *  \note Reference the authors ECCV paper when using this algorithm.
         */
        template <class T>
        class Seeds
        {
        public:
            // -[ Constructors, destructor and assignation operator ]----------------------------------
            /// Default constructor.
            Seeds(void);
            /** Constructor which the basic parameters of the algorithm.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_bins number of bins of the color histograms.
             *  \param[in] preprocess identifier of the method used to pre-process images.
             *  \param[in] double_steps boolean flag which enables or disables to iterate twice the block updates (better segmentation when true, faster when false).
             */
            Seeds(unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps);
            /** Constructor which the basic parameters of the algorithm and the geometry of the processed images.
             *  \param[in] width width of the source images.
             *  \param[in] height height of the source images.
             *  \param[in] number_of_channels number of channels of the source image.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_bins number of bins of the color histograms.
             *  \param[in] preprocess identifier of the method used to pre-process images.
             *  \param[in] double_steps boolean flag which enables or disables to iterate twice the block updates (better segmentation when true, faster when false).
             */
            Seeds(unsigned int width, unsigned int height, unsigned int number_of_channels, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps);
            /// Copy constructor.
            Seeds(const Seeds<T> &other);
            /// Destructor.
            ~Seeds(void) { freeImage(); freeBase(); }
            /// Assignation operator.
            Seeds<T>& operator=(const Seeds<T> &other);
            
            // -[ Initialization functions ]-----------------------------------------------------------
            /** This function sets the basic parameters of the algorithm.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_bins number of bins of the color histograms.
             *  \param[in] preprocess identifier of the method used to pre-process images.
             *  \param[in] double_steps boolean flag which enables or disables to iterate twice the block updates (better segmentation when true, faster when false).
             */
            void initialize(unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps);
            /** Initializes the structures that the object uses to calculate the super-pixels.
             *  \param[in] width width of the input images.
             *  \param[in] height height of the input images.
             *  \param[in] number_of_channels number of channels of the input image.
             */
            void setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels);
            /** Initializes the structures that the object uses to calculate the super-pixels.
             *  \param[in] image image which has the same geometry (width, height, number_of_channels) as the images processes by the object.
             */
            template <template <class> class IMAGE>
            inline void setGeometry(const IMAGE<T> &image) { setGeometry(image.getWidth(), image.getHeight(), image.getNumberOfChannels()); }
            /** This function sets the basic parameters of the algorithm and initializes the structures that the object uses to calculate the super-pixels.
             *  \param[in] width width of the input images.
             *  \param[in] height height of the input images.
             *  \param[in] number_of_channels number of channels of the input image.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_bins number of bins of the color histograms.
             *  \param[in] preprocess identifier of the method used to pre-process images.
             *  \param[in] double_steps boolean flag which enables or disables to iterate twice the block updates (better segmentation when true, faster when false).
             */
            void initialize(unsigned int width, unsigned int height, unsigned int number_of_channels, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps);
            
            // -[ Processing functions ]---------------------------------------------------------------
            /** Calculates the super-pixels of the given image using the internal object structures.
             *  \param[in] source source image.
             *  \param[out] segmentation image with the identifiers of the super-pixels.
             *  \param[in] number_of_threads number of threads used to concurrently pre-process the input image.
             *  \returns the number of super-pixels generated.
             */
            template <class TSOURCE>
            unsigned int process(const Image<TSOURCE> &source, Image<unsigned int> &segmentation, unsigned int number_of_threads);
            /** Calculates the super-pixels of the given image.
             *  \param[in] source source image.
             *  \param[out] segmentation image with the identifiers of the super-pixels.
             *  \param[in] number_of_threads number of threads used to concurrently pre-process the input image.
             *  \returns the number of super-pixels generated.
             */
            template <class TSOURCE>
            inline unsigned int process(const Image<TSOURCE> &source, Image<unsigned int> &segmentation, unsigned int number_of_threads) const
            {
                return process(source, m_step_x, m_step_y, m_number_of_levels, m_number_of_bins, m_preprocess_identifier, m_double_steps, segmentation, number_of_threads);
            }
            /** Calculates the super-pixels of the given image and algorithm parameters.
             *  \param[in] source source image.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_bins number of bins of the color histograms.
             *  \param[in] preprocess identifier of the method used to pre-process images.
             *  \param[in] double_steps boolean flag which enables or disables to iterate twice the block updates (better segmentation when true, faster when false).
             *  \param[out] segmentation image with the identifiers of the super-pixels.
             *  \param[in] number_of_threads number of threads used to concurrently pre-process the input image.
             *  \returns the number of super-pixels generated.
             */
            template <class TSOURCE>
            static unsigned int process(const Image<TSOURCE> &source, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps, Image<unsigned int> &segmentation, unsigned int number_of_threads);
            
            /** Calculates the initial step of the pyramid to generate as many partitions as specified at the top level of the pyramid.
             *  \param[in] final_step desired separation between the seeds at the final step.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \returns the initial step needed to obtain the desired separation between super-pixels.
             */
            inline static unsigned int initialStep(unsigned int final_step, unsigned int number_of_levels)
            {
                unsigned int factor = 1;
                for (unsigned int l = 0; l < number_of_levels - 1; ++l) factor *= 2;
                return srvMax<unsigned int>(1, (unsigned int)ceil((double)final_step / (double)factor));
            }
            // -[ Access functions ]-------------------------------------------------------------------
            /// Returns the degree of the spatial pyramid in the X-direction.
            inline unsigned int getStepX(void) const { return m_step_x; }
            /// Returns the degree of the spatial pyramid in the Y-direction.
            inline unsigned int getStepY(void) const { return m_step_y; }
            /// Returns the number of seeds in the X direction at the index-th level of the spatial pyramid.
            inline unsigned int getNumberOfSeedsX(unsigned int index) const { return m_number_of_seeds_x[index]; }
            /// Returns the number of seeds in the Y direction at the index-th level of the spatial pyramid.
            inline unsigned int getNumberOfSeedsY(unsigned int index) const { return m_number_of_seeds_y[index]; }
            /// Returns the number of seeds at the index-th level of the spatial pyramid.
            inline unsigned int getNumberOfSeeds(unsigned int index) const { return m_number_of_seeds[index]; }
            /// Returns the number of levels of the spatial pyramid.
            inline unsigned int getNumberOfLevels(void) const { return m_number_of_levels; }
            /// Returns the number of bins of the color histograms.
            inline unsigned int getNumberOfBins(void) const { return m_number_of_bins; }
            /// Returns the total number of bins of the histogram.
            inline unsigned int getHistogramSize(void) const { return m_histogram_size; }
            /// Returns the identifier of the method used to pre-process images before calculating the super-pixels.
            inline SEGMENTATION_PREPROCESS getPreprocessID(void) const { return m_preprocess_identifier; }
            /// Returns the boolean flag which is true when each block update is iterated twice.
            inline bool getDoubleSteps(void) const { return m_double_steps; }
            /// Sets the boolean flag which is true when each block update is iterated twice.
            inline void setDoubleSteps(bool double_steps) { m_double_steps = double_steps; }
        private:
            // -[ Auxiliary functions ]----------------------------------------------------------------
            /// Frees the structures created by the basic algorithm.
            void freeBase(void);
            /// Frees the structures created to process the images.
            void freeImage(void);
            // -[ Auxiliary static functions ]---------------------------------------------------------
            /** Auxiliary function which assigns the pixel label to each pixel of the image.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_channels number of channels of the work image.
             *  \param[out] number_of_seeds_x number of partitions in the X-direction at each scale of the spatial pyramid.
             *  \param[out] number_of_seeds_y number of partitions in the Y-direction at each scale of the spatial pyramid.
             *  \param[out] number_of_seeds number of seeds at each scale of the spatial pyramid.
             *  \param[out] number_of_partitions number of partitions at each scale of the spatial pyramid.
             *  \param[out] parent_label identifier of the label at the parent level of the spatial pyramid.
             *  \param[out] labels image with the identifier of the region at each different scale.
             *  \param[out] histogram color histogram of each seed.
             *  \param[out] block_size number of pixels of each histogram.
             *  \param[in] histogram_size number of color bins of the histogram of each super-pixel.
             *  \param[out] mean mean color values for each seed of the top level of the pyramid.
             */
            static void createStructures(unsigned int step_x, unsigned int step_y, unsigned int number_of_channels, unsigned int * number_of_seeds_x, unsigned int * number_of_seeds_y, unsigned int * number_of_seeds, unsigned int * * number_of_partitions, unsigned int * * parent_label, Image<unsigned int> &labels, unsigned int * * * &histogram, unsigned int * * &block_size, unsigned int histogram_size, typename META_GENERAL_TYPE<T>::TYPE * * &mean);
            /** Auxiliary function which sets the initial values of the structures needed calculate the Seeds super-pixels.
             *  \param[in] step_x separation between seeds in the X direction.
             *  \param[in] step_y separation between seeds in the Y direction.
             *  \param[in] number_of_seeds_x number of partitions in the X-direction at each scale of the spatial pyramid.
             *  \param[in] number_of_seeds_y number of partitions in the Y-direction at each scale of the spatial pyramid.
             *  \param[in] number_of_seeds number of seeds at each scale of the spatial pyramid.
             *  \param[out] number_of_partitions number of partitions at each scale of the spatial pyramid.
             *  \param[out] parent_label identifier of the label at the parent level of the spatial pyramid.
             *  \param[out] labels image with the identifier of the region at each different scale.
             */
            static void initStructures(unsigned int step_x, unsigned int step_y, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, const unsigned int * number_of_seeds, unsigned int * * number_of_partitions, unsigned int * * parent_label, Image<unsigned int> &labels);
            /** Auxiliary function which create the color histogram.
             *  \param[in] histogram_size number of bins of the color histogram.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_seeds number of seeds at each level of the spatial pyramid.
             *  \param[in] parent_label identifier of the label at the parent level of the spatial pyramid..
             *  \param[in] bins image with the bin of the color histogram of each pixel.
             *  \param[in] labels image with the super-pixel identifier at each scale of the spatial pyramid.
             *  \param[out] histogram color histogram of each seed.
             *  \param[out] block_size number of pixels of each histogram.
             *  \param[out] number_of_partitions number of partitions at each scale of the spatial pyramid.
             */
            static void computeHistogram(unsigned int histogram_size, unsigned int number_of_levels, const unsigned int * number_of_seeds, unsigned int * * parent_label, Image<unsigned int> &bins, Image<unsigned int> &labels, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions);
            /** Auxiliary function which convert the source image to the selected color space and calculate the histogram bin of each image pixel.
             *  \param[in] source source image.
             *  \param[in] convert_identifier identifier of the destination color space.
             *  \param[in] number_of_bins number of bins for each channel of the color histogram.
             *  \param[out] work_image converted image.
             *  \param[out] bins image with the bin of the color histogram of each pixel.
             *  \param[in] number_of_threads number of threads used to concurrently process the images.
             */
            template <class TSOURCE>
            static void convertColor(const Image<TSOURCE> &source, SEGMENTATION_PREPROCESS convert_identifier, unsigned int number_of_bins, Image<T> &work_image, Image<unsigned int> &bins, unsigned int number_of_threads);
            /** Auxiliary function which accumulates the histogram blocks at the upper levels of the pyramid.
             *  \param[in] level current level.
             *  \param[in] label current label.
             *  \param[in] sublevel index of the level below.
             *  \param[in] sublabel label of the level below.
             *  \param[in] histogram_size number of bins of the color histogram.
             *  \param[in,out] histogram color histogram of each seed at each scale.
             *  \param[in,out] block_size number of pixels assigned to each color histogram
             *  \param[out] number_of_partitions number of partitions of each seed.
             *  \param[out] parent_label identifier of the label at the parent level.
             */
            static void addBlock(unsigned int level, unsigned int label, unsigned int sublevel, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label);
            /** Auxiliary function which subtracts the histogram blocks at the upper levels of the pyramid.
             *  \param[in] level current level.
             *  \param[in] label current label.
             *  \param[in] sublevel index of the level below.
             *  \param[in] sublabel label of the level below.
             *  \param[in] histogram_size number of bins of the color histogram.
             *  \param[in,out] histogram color histogram of each seed at each scale.
             *  \param[in,out] block_size number of pixels assigned to each color histogram
             *  \param[out] number_of_partitions number of partitions of each seed.
             *  \param[out] parent_label identifier of the label at the parent level.
             */
            static void removeBlock(unsigned int level, unsigned int label, unsigned int sublevel, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label);
            /** Auxiliary function which update the segmentation blocks.
             *  \param[in] top_level top level of the spatial pyramid
             *  \param[in] level current level
             *  \param[in] ref_confidence minimum intersection distance to add two blocks.
             *  \param[in] label_a identifier of the first seeds region.
             *  \param[in] label_b identifier of the second seed region.
             *  \param[in] sublabel label of the region at a lower level of the pyramid.
             *  \param[in] histogram_size number of bins of the color histogram.
             *  \param[in,out] histogram color histogram of each seed at each scale.
             *  \param[in,out] block_size number of pixels assigned to each color histogram
             *  \param[out] number_of_partitions number of partitions of each seed.
             *  \param[out] parent_label identifier of the label at the parent level.
             *  \returns a flag which indicates that the current block has been already updated.
             */
            static bool updateBlock(unsigned int top_level, unsigned int level, float ref_confidence, unsigned int label_a, unsigned int label_b, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label);
            /** Update the super-pixels at the pixel level.
             *  \param[in] forward_backward boolean flag which indicates the direction of the update.
             *  \param[in] histogram array with the histogram of the super-pixels at each scale.
             *  \param[in] block_size number of pixels of each super-pixel.
             *  \param[in] bins image with the bin of the color histogram of each pixel.
             *  \param[in] labels image with the super-pixel identifier at each scale of the spatial pyramid.
             *  \param[in] work_image image with original image converted to the segmentation color space.
             *  \param[in] mean array with the mean super-pixel color.
             */
            static void updatePixels(bool forward_backward, unsigned int * * * histogram, unsigned int * * block_size, Image<unsigned int> &bins, Image<unsigned int> &labels, Image<T> &work_image, typename META_GENERAL_TYPE<T>::TYPE * * mean);
            /** Auxiliary function which calculates the intersection distance between two histogram blocks.
             *  \param[in] level_first level of the first histogram.
             *  \param[in] label_first seed index of the first histogram.
             *  \param[in] level_second level of the second histogram.
             *  \param[in] label_second seed index of the second histogram.
             *  \param[in] histogram color histogram of each seed at each scale.
             *  \param[in] block_size number of pixels assigned to each histogram.
             */
            static float intersection(unsigned int level_first, unsigned int label_first, unsigned int level_second, unsigned int label_second, unsigned int const * const * const * histogram, unsigned int const * const * block_size, unsigned int histogram_size);
            /// Auxiliary function which checks that two different seeds can be merged or not.
            inline static bool checkSplit(unsigned int a11, unsigned int a12, unsigned int a13, unsigned int a21, unsigned int a22, unsigned int a23, unsigned int a31, unsigned int a32, unsigned int a33, bool horizontal, bool forward)
            {
                if (horizontal)
                {
                    if (forward) return (((a22 != a21) && (a22 == a12) && (a22 == a32)) || ((a22 != a11) && (a22 == a12) && (a22 == a21)) || ((a22 != a31) && (a22 == a32) && (a22 == a21)));
                    else return (((a22 != a23) && (a22 == a12) && (a22 == a32)) || ((a22 != a13) && (a22 == a12) && (a22 == a23)) || ((a22 != a33) && (a22 == a32) && (a22 == a23)));
                }
                else
                {
                    if (forward) return (((a22 != a12) && (a22 == a21) && (a22 == a23)) || ((a22 != a11) && (a22 == a21) && (a22 == a12)) || ((a22 != a13) && (a22 == a23) && (a22 == a12)));
                    else return (((a22 != a32) && (a22 == a21) && (a22 == a23)) || ((a22 != a31) && (a22 == a21) && (a22 == a32)) || ((a22 != a33) && (a22 == a23) && (a22 == a32)));
                }
            }
            /** Auxiliary function which update the labels of the seeds in the upper levels of the spatial pyramid.
             *  \param[in] level level of the spatial pyramid.
             *  \param[in] ref_confidence minimum intersection distance to add two blocks.
             *  \param[in] number_of_seeds_x number of seeds in the X-direction.
             *  \param[in] number_of_seeds_y number of seeds in the Y-direction.
             *  \param[in] number_of_partitions number of partitions at each level.
             *  \param[in] parent_label identifier of the label at the parent level of the spatial pyramid.
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] histogram color histogram of each seed at each scale of the pyramid.
             *  \param[in] block_size number of pixels assigned to each color histogram.
             *  \param[in] histogram_size number of bins of the color histogram.
             *  \param[in] labels image with the seed label assigned to each pixel.
             */
            static void updateBlocks(int level, float ref_confidence, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, unsigned int * * number_of_partitions, unsigned int * * parent_label, unsigned int number_of_levels, unsigned int * * * histogram, unsigned int * * block_size, unsigned int histogram_size, Image<unsigned int> &labels, unsigned int number_of_threads);
            /** Auxiliary function which changes the level of the spatial pyramid to process.
             *  \param[in] current_level current level of the spatial pyramid.
             *  \param[in] number_of_seeds_x number of seeds in the X-direction.
             *  \param[in] number_of_seeds_y number of seeds in the Y-direction.
             *  \param[out] number_of_partitions number of partitions at each level.
             *  \param[out] parent_label identifier of the label at the parent level of the spatial pyramid..
             *  \param[in] number_of_levels number of levels of the spatial pyramid.
             *  \param[in] number_of_threads number of threads used to concurrently update the partitions and parent images.
             *  \returns the index of the new spatial pyramid level.
             */
            static int updateLevelIndex(int current_level, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, unsigned int * * number_of_partitions, unsigned int * * parent_label, unsigned int number_of_levels, unsigned int number_of_threads);
            /** Auxiliary function which updates the label of the super-pixels.
             *  \param[in] bins image with the bin of the color histogram of each pixel.
             *  \param[in] work_image converted image.
             *  \param[in] label_new identifier of the new label of the pixel.
             *  \param[in] x x-coordinate of the pixel.
             *  \param[in] y y-coordinate of the pixel.
             *  \param[out] labels image with the super-pixel identifier at each scale of the spatial pyramid.
             *  \param[out] histogram color histogram of each seed at each scale of the pyramid.
             *  \param[out] block_size number of pixels assigned to each color histogram.
             *  \param[out] mean array with the mean super-pixel color.
             */
            static void update(const Image<unsigned int> &bins, const Image<T> &work_image, unsigned int label_new, unsigned int x, unsigned int y, Image<unsigned int> &labels, unsigned int * * * histogram, unsigned int * * block_size, typename META_GENERAL_TYPE<T>::TYPE * * mean);
            /** Auxiliary function which counts how many pixels have the selected label in a 3 by 4 region.
             *  \param[in] labels image with the label assigned to each pixel.
             *  \param[in] x X-coordinate of the region center.
             *  \param[in] y Y-coordinate of the region center.
             *  \param[in] level level of the spatial pyramid.
             *  \param[in] label selected label.
             *  \param[in] count_last boolean flag which enables to calculate the last column of the 3 by 4 region when true (to avoid overflow in image border).
             *  \return number of pixels with the same label.
             */
            static unsigned int count3x4(Image<unsigned int> &labels, unsigned int x, unsigned int y, unsigned int level, unsigned int label, bool count_last);
            /** Auxiliary function which counts how many pixels have the selected label in a 4 by 3 region.
             *  \param[in] labels image with the label assigned to each pixel.
             *  \param[in] x X-coordinate of the region center.
             *  \param[in] y Y-coordinate of the region center.
             *  \param[in] level level of the spatial pyramid.
             *  \param[in] label selected label.
             *  \param[in] count_last boolean flag which enables to calculate the last row of the 3 by 4 region when true (to avoid overflow in image border).
             *  \return number of pixels with the same label.
             */
            static unsigned int count4x3(Image<unsigned int> &labels, unsigned int x, unsigned int y, unsigned int level, unsigned int label, bool count_last);
            /** Auxiliary function which calculates the mean color of each seed of the top level.
             *  \param[in] work_image image with the working color image.
             *  \param[in] labels image with the index of the seed assigned to each pixel of the image.
             *  \param[out] mean resulting color mean of the seeds.
             *  \param[in] number_of_seeds number of seeds in the image.
             *  \param[in] number_of_threads number of threads used to concurrently calculate the seeds color mean.
             */
            static void computeMean(const Image<T> &work_image, const Image<unsigned int> &labels, typename META_GENERAL_TYPE<T>::TYPE * * mean, unsigned int number_of_seeds, unsigned int number_of_threads);
            /** Auxiliary function which calculates the probability that a pixels belong to a super-pixel.
             *  \param[in] image_ptr an array with pointers to the current pixel image values.
             *  \param[in] offset offset of the current image pixel pointers.
             *  \param[in] mean array with the mean color value of each super-pixel.
             *  \param[in] block_size number of pixels of each histogram.
             *  \param[in] prior_a a tuple with the identifier and the priori value of the first super-pixel.
             *  \param[in] prior_b a tuple with the identifier and the priori value of the second super-pixel.
             *  \param[in] top_level highest level of the spatial pyramid (i.e. height of the pixel-level information).
             *  \param[in] number_of_channels number of channels of the working image.
             *  \return a boolean flag which indicates if the pixel label must be updated or not.
             */
            static bool probability(T const * const * image_ptr, int offset, typename META_GENERAL_TYPE<T>::TYPE const * const * mean, unsigned int const * const * block_size, const Tuple<unsigned int, int> &prior_a, const Tuple<unsigned int, int> &prior_b, unsigned int top_level, unsigned int number_of_channels);
            inline static unsigned int parentLabelValue(const unsigned int * parent_label, int offset_x, int offset_y, unsigned int width, unsigned int height, unsigned int boundary_value)
            {
                if ((offset_x < 0) || (offset_y < 0) || (offset_x >= (int)width) || (offset_y >= (int)height)) return boundary_value;
                else return parent_label[offset_x + offset_y * width];
            }
            /** Generates the final segmentation image.
             *  \param[in] labels image with the index of the seed assigned to each pixel of the image.
             *  \param[out] segmentation image with the identifiers of the super-pixels.
             *  \param[in] number_of_threads number of threads used to concurrently pre-process the input image.
             *  \returns the number of generated super-pixels.
             */
            static unsigned int generateSegmentationImage(const Image<unsigned int> &labels, Image<unsigned int> &segmentation, unsigned int number_of_threads);
            
            // -[ Member variables ]-------------------------------------------------------------------
            /// Degree of the spatial pyramid in the X-direction.
            unsigned int m_step_x;
            /// Degree of the spatial pyramid in the Y-direction.
            unsigned int m_step_y;
            /// Number of seeds in the X direction at each level of the spatial pyramid.
            unsigned int * m_number_of_seeds_x;
            /// Number of seeds in the Y direction at each level of the spatial pyramid.
            unsigned int * m_number_of_seeds_y;
            /// Number of seeds at each level of the spatial pyramid.
            unsigned int * m_number_of_seeds;
            /// Number of partitions the seeds at each level of the spatial pyramid.
            unsigned int * * m_number_of_partitions;
            /// Identifier of parent label at each level of the spatial pyramid.
            unsigned int * * m_parent_label;
            /// Number of levels of the spatial pyramid.
            unsigned int m_number_of_levels;
            /// Image with the spatial bin assigned to each pixel.
            Image<unsigned int> m_bins;
            /// Image with the information used to generate the super-pixels.
            Image<T> m_work_image;
            /// Array with the mean color value for each seed of the last level of the pyramid.
            typename META_GENERAL_TYPE<T>::TYPE * * m_mean;
            /// Image with the identifier of the spatial pyramid region at each level of the spatial pyramid.
            Image<unsigned int> m_labels;
            /// Color histogram of each seed at each scale.
            unsigned int * * * m_histogram;
            /// Number of pixels assigned to each color histogram.
            unsigned int * * m_block_size;
            /// Number of bins of the color histograms.
            unsigned int m_number_of_bins;
            /// Total number of bins of the histogram.
            unsigned int m_histogram_size;
            /// Identifier of the method used to pre-process images before calculating the super-pixels.
            SEGMENTATION_PREPROCESS m_preprocess_identifier;
            /// Boolean flag which allows to iterate twice the iterations of the block updates.
            bool m_double_steps;
        };
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ BINARIZATION FUNCTIONS ]=============================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        
        /** Applies the Otsu binarization algorithm to the given image.
         *  \param[in] input input constant sub-image.
         *  \param[in] weight_factor factor applied to the Otsu threshold to reduce or strengthen the response of the binarization.
         *  \param[out] output resulting binarized sub-image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const ConstantSubImage<TINPUT> &input, double weight_factor, SubImage<TOUTPUT>  output, unsigned int number_of_threads);
        // Inline functions call with variations of the input/output image parameters.
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const            Image<TINPUT> &input, double weight_factor,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeOtsu(ConstantSubImage<TINPUT>(input), weight_factor, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const            Image<TINPUT> &input, double weight_factor, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeOtsu(ConstantSubImage<TINPUT>(input), weight_factor,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const         SubImage<TINPUT> &input, double weight_factor,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeOtsu(ConstantSubImage<TINPUT>(input), weight_factor, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const         SubImage<TINPUT> &input, double weight_factor, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeOtsu(ConstantSubImage<TINPUT>(input), weight_factor,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeOtsu(const ConstantSubImage<TINPUT> &input, double weight_factor,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeOtsu(                         input , weight_factor, SubImage<TOUTPUT>(output), number_of_threads); }
        
        /** Applies the Sauvola binarization algorithm to the given image.
         *  \param[in] input input constant sub-image.
         *  \param[in] sauvola_factor weights the contribution of the standard deviation in the estimation of the threshold of each pixel. This parameters typically takes values between 0.2 and 0.5.
         *  \param[in] window_size size of the square-region defined around each pixel used to calculate the local mean and standard deviation of the pixel values.
         *  \param[out] output resulting binarized sub-image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads);
        // Inline functions call with variations of the input/output image parameters.
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const            Image<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeSauvola(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const            Image<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeSauvola(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const         SubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeSauvola(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const         SubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeSauvola(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeSauvola(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeSauvola(                         input , sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        
        /** Applies the Bradley binarization to the given image.
         *  \param[in] input input constant sub-image.
         *  \param[in] weight_factor weight factor applied to the Bradley threshold to reduce or strengthen the response of the binarization.
         *  \param[in] window_size size of the square-region defined around each pixel used to calculate the local mean of the pixel values.
         *  \param[out] output resulting binarized sub-image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const ConstantSubImage<TINPUT> &input, double weight_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads);
        // Inline functions call with variations of the input/output image parameters.
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const            Image<TINPUT> &input, double weight_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeBradley(ConstantSubImage<TINPUT>(input), weight_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const            Image<TINPUT> &input, double weight_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeBradley(ConstantSubImage<TINPUT>(input), weight_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const         SubImage<TINPUT> &input, double weight_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeBradley(ConstantSubImage<TINPUT>(input), weight_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const         SubImage<TINPUT> &input, double weight_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeBradley(ConstantSubImage<TINPUT>(input), weight_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeBradley(const ConstantSubImage<TINPUT> &input, double weight_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeBradley(                         input , weight_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        
        /** Applies the Sauvola binarization algorithm to the given image.
         *  \param[in] input input constant sub-image.
         *  \param[in] sauvola_factor weights of the Sauvola binarization algorithm applied to obtain an initial estimation of the image background.
         *  \param[in] window_size size of the windows used calculate the Sauvola binarization and the image background estimation.
         *  \param[out] output resulting binarized sub-image.
         *  \param[in] number_of_threads number of threads used to concurrently process the image.
         */
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads);
        // Inline functions call with variations of the input/output image parameters.
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const            Image<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeGatos(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const            Image<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeGatos(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const         SubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeGatos(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const         SubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            binarizeGatos(ConstantSubImage<TINPUT>(input), sauvola_factor, window_size,                   output , number_of_threads); }
        template <class TINPUT, class TOUTPUT> static void binarizeGatos(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); binarizeGatos(                         input , sauvola_factor, window_size, SubImage<TOUTPUT>(output), number_of_threads); }
        
    private:
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ CONNECTED COMPONENTS AUXILIARY FUNCTIONS ]===========================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Auxiliary function to search the parent node in the 'spaghetti stack' used by the connected components labeling algorithm
         *  to keep the relationships between regions.
         *  \param[in] stack_tabel vector with the static spaghetti stack entries.
         *  \param[in] index query index.
         *  \returns the index of the parent node.
         */
        template <class T>
        static inline unsigned int ConnectedComponentsStackFind(VectorDense<T> &stack_table, unsigned int index);
        
        /** Auxiliary function used by the connected components labeling algorithm to search labeling incoherences between lines.
         *  \param[in] src source image with the original values.
         *  \param[in] dst image with the connected components labels.
         *  \param[in] connectivity identifier of the connectivity method used to define the neighborhood between pixels.
         *  \param[in,out] lut vector with the 'spaghetti stack' used to keep the relationships between connected components.
         *  \param[in] line index of the image line currently processed.
         *  \param[in] channel channel of the current processed line.
         */
        template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST, class TLUT>
        static void ConnectedComponentLine(const IMAGE_SRC<TSRC> &src, const IMAGE_DST<TDST> &dst, IMAGE_CONNECTIVITY connectivity, VectorDense<TLUT> &lut, unsigned int line, unsigned int channel);
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ WATERSHED AUXILIARY FUNCTIONS ]======================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Auxiliary function to calculate the relationships between regions of the image.
         *  \param[in] first_index index of the first image region.
         *  \param[in] first_value value of the pixels in the first image region.
         *  \param[in] second_index index of the second image region.
         *  \param[in] second_value value of the pixels in the second image region.
         *  \param[out] relations vector with the index of the lowest neighboring region.
         *  \param[out] values vector with the value of the lowest neighboring region.
         */
        template <class TSRC>
        static inline void WatershedRelationUpdate(unsigned int first_index, const TSRC &first_value, unsigned int second_index, const TSRC &second_value, VectorDense<unsigned int> &relations, VectorDense<TSRC> &values);
        
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SLIC SEGMENTATION AUXILIARY CLASS AND FUNCTIONS ]====================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Container class which stores the information of the cluster centroids used by the SLIC segmentation algorithm
         *  to define the image super-pixels.
         */
        template <class T>
        class SlicCentroid
        {
        public:
            // -[ Constructors, destructor and assignation operators ]-------------------------------------
            /// Default constructor.
            SlicCentroid(void) : m_x(0), m_y(0), m_l(0), m_a(0), m_b(0) {}
            
            // -[ Access functions ]-----------------------------------------------------------------------
            /// Returns the X-coordinate of the cluster centroid.
            inline T getX(void) const { return m_x; }
            /// Returns a reference to the X-coordinate of the cluster centroid.
            inline T& getX(void) { return m_x; }
            /// Sets the X-coordinate of the cluster centroid.
            inline void setX(T x) { m_x = x; }
            /// Returns the Y-coordinate of the cluster centroid.
            inline T getY(void) const { return m_y; }
            /// Returns a reference to the Y-coordinate of the cluster centroid.
            inline T& getY(void) { return m_y; }
            /// Sets the Y-coordinate of the cluster centroid.
            inline void setY(T y) { m_y = y; }
            /// Returns the L-coordinate of the cluster centroid.
            inline T getL(void) const { return m_l; }
            /// Returns a reference to the L-coordinate of the cluster centroid.
            inline T& getL(void) { return m_l; }
            /// Sets the L-coordinate of the cluster centroid.
            inline void setL(T l) { m_l = l; }
            /// Returns the A-coordinate of the cluster centroid.
            inline T getA(void) const { return m_a; }
            /// Returns a reference to the A-coordinate of the cluster centroid.
            inline T& getA(void) { return m_a; }
            /// Sets the A-coordinate of the cluster centroid.
            inline void setA(T a) { m_a = a; }
            /// Returns the B-coordinate of the cluster centroid.
            inline T getB(void) const { return m_b; }
            /// Returns a reference to the B-coordinate of the cluster centroid.
            inline T& getB(void) { return m_b; }
            /// Sets the B-coordinate of the cluster centroid.
            inline void setB(T b) { m_b = b; }
            
            /// Sets the image coordinate parameters of the cluster centroid.
            inline void set(T x, T y) { m_x = x; m_y = y; }
            /// Sets all coordinates of the cluster centroid.
            inline void set(T x, T y, T l, T a, T b) { m_x = x; m_y = y; m_l = l; m_a = a; m_b = b; }
        private:
            /// X-coordinate of the cluster centroid.
            T m_x;
            /// Y-coordinate of the cluster centroid.
            T m_y;
            /// L-coordinate of the cluster centroid.
            T m_l;
            /// A-coordinate of the cluster centroid.
            T m_a;
            /// B-coordinate of the cluster centroid.
            T m_b;
        };
        
        /** Auxiliary function which initializes the centroids given a image geometry.
         *  \param[in] width width of the image.
         *  \param[in] height height of the image.
         *  \param[in] step separation between seeds.
         *  \param[out] centroids vector with the centroid structures of each superpixel.
         */
        template <class TMODE>
        static void SlicInitializeCentroids(unsigned int width, unsigned int height, unsigned int step, VectorDense<SlicCentroid<TMODE> > &centroids);
        
        /** Auxiliary function which initializes the seeds of the SLIC segmentation algorithm.
         *  \param[in] image source image where the seeds are sampled.
         *  \param[in] perturb_seeds enable or disable to perturb the location initial seeds to avoid edges.
         *  \param[out] centroids vector with the centroid structures of each superpixel.
         *  \param[in] number_of_threads number of treads used to concurrently calculate the initial seeds.
         */
        template <class T, class TMODE>
        static void SlicInitializeSeeds(Image<T> &lab_image, bool perturb_seeds, VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int number_of_threads);
        /** Auxiliary function which enforces the connectivity between the super-pixels generated by the SLIC algorithm.
         *  \param[in] step separation between seeds.
         *  \param[out] cluster_selected image with the label assigned to each pixel of the image.
         */
        template <class T>
        static unsigned int SlicEnforceConnectivity(unsigned int step, Image<T> &cluster_selected);
        /** Auxiliary function which updates the centroid position processing with the values of the image in the given line.
         *  \param[in] image source image.
         *  \param[in] x0 initial location of the cluster in the X-coordinate.
         *  \param[in] x1 final location of the cluster in the X-coordinate.
         *  \param[in] y Y-coordinate of the cluster in the image.
         *  \param[in] centroids vector with the information of the updated SLIC centroids.
         *  \param[in] id index of the SLIC centroid currently updated.
         *  \param[in] invwt factor used to weight the contribution of the color in the distance between the pixels and the centroid.
         *  \param[out] cluster_distance updated image with the distance between the cluster and the pixels.
         *  \param[out] cluster_selected updated image with the index of the super-pixel assigned to each pixel.
         */
        template <class T, class TMODE>
        static inline void SlicLineDistance(const Image<T> &image, int x0, int x1, int y, const VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int id, double invwt, Image<TMODE> &cluster_distance, Image<unsigned int> &cluster_selected);
        /** Auxiliary function which updates the centroid position processing with the values of the image in the given line.
         *  This function is used by the adaptive version of the SLIC algorithm where the compactness of the clusters
         *  is set automatically.
         *  \param[in] image source image.
         *  \param[in] x0 initial location of the cluster in the X-coordinate.
         *  \param[in] x1 final location of the cluster in the X-coordinate.
         *  \param[in] y Y-coordinate of the cluster in the image.
         *  \param[in] centroids vector with the information of the updated SLIC centroids.
         *  \param[in] id index of the SLIC centroid currently updated.
         *  \param[in] maxlab maximum distance of the color part between the cluster and the pixel descriptors.
         *  \param[in] invwt factor used to weight the contribution of the color in the distance between the pixels and the centroid.
         *  \param[out] cluster_distance updated image with the distance between the cluster and the pixels.
         *  \param[out] cluster_selected updated image with the index of the super-pixel assigned to each pixel.
         *  \param[out] maxlab_update updated maximum distance of the color part between the cluster and the pixel descriptors.
         */
        template <class T, class TMODE>
        static inline void SlicLineDistanceAdapt(const Image<T> &image, int x0, int x1, int y, const VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int id, TMODE maxlab, double invwt, Image<TMODE> &cluster_distance, Image<unsigned int> &cluster_selected, TMODE &maxlab_update);
        /** Auxiliary function which recalculates the SLIC centroids after processing all image pixels.
         *  \param[in] image source image.
         *  \param[in] cluster_distance image with the distance between the pixels and the nearest centroids.
         *  \param[in] cluster_selected image with the cluster centroid assigned to each pixel.
         *  \param[out] centroids vector with the update centroids.
         *  \param[out] cluster_size vector with the number of pixels associated to each SLIC centroid.
         */
        template <class T, class TMODE>
        static inline void SlicRecalculateCentroids(const Image<T> &image, const Image<TMODE> &cluster_distance, const Image<unsigned int> &cluster_selected, VectorDense<SlicCentroid<TMODE> > &centroids, VectorDense<unsigned int> &cluster_size);
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONNECTED COMPONENTS FUNCTIONS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function implements the connected-components labeling algorithm.
     *  \param[in] src source image to label.
     *  \param[in] connectivity identifier of the connectivity method used to define the neighborhood between pixels.
     *  \param[out] dst resulting connected components image.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the image connected components.
     *  \return number of connected components found in the image.
     */
    template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST>
    unsigned int Segmentation::ConnectedComponents(const IMAGE_SRC<TSRC> &src, IMAGE_CONNECTIVITY connectivity, IMAGE_DST<TDST> &dst, unsigned int number_of_threads)
    {
        const unsigned int width = src.getWidth();
        const unsigned int height = src.getHeight();
        const unsigned int number_of_channels = src.getNumberOfChannels();
        std::map<unsigned int, unsigned int> components_relabel;
        VectorDense<unsigned int> lut;
        unsigned int current_label;
        
        // Initialize structures ..................................................................
        current_label = 0;
        if ((width != dst.getWidth()) || (height != dst.getHeight()) || (number_of_channels != dst.getNumberOfChannels()))
            dst.setGeometry(src);
        
        if (number_of_threads > 1) // Multi-threading version of the algorithm
        {
            number_of_threads = std::min(number_of_threads, height);
            VectorDense<unsigned int> y_begin(number_of_threads), y_end(number_of_threads), thread_number_of_regions(number_of_threads);
            
            // Initialize the limits of the image regions processed by each thread.
            for (unsigned int t = 0; t < number_of_threads - 1; ++t)
            {
                y_begin[t] = t * height / number_of_threads;
                y_end[t] = (t + 1) * height / number_of_threads;
            }
            y_begin[number_of_threads - 1] = (number_of_threads - 1) * height / number_of_threads;
            y_end[number_of_threads - 1] = height;
            
            // Use a thread to process each image region: each thread creates a sub-image of the assigned region and recursively
            // call the ConnectedComponents function with the number of threads equal to zero.
            #pragma omp parallel num_threads(number_of_threads) reduction(+:current_label)
            {
                const unsigned int thread_id = omp_get_thread_num();
                const unsigned int local_height = y_end[thread_id] - y_begin[thread_id];
                ConstantSubImage<TSRC> sub_src = src.subimage(0, y_begin[thread_id], width, local_height);
                SubImage<TDST> sub_dst = dst.subimage(0, y_begin[thread_id], width, local_height);
                current_label += thread_number_of_regions[thread_id] = ConnectedComponents(sub_src, connectivity, sub_dst, 1);
            }
            
            // Add the offset to the partial labeling of each image region.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                unsigned int offset;
                
                offset = 0;
                for (unsigned int t = 0; t < thread_id; ++t)
                    offset += thread_number_of_regions[t];
                if (offset > 0)
                {
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        for (unsigned int y = y_begin[thread_id]; y < y_end[thread_id]; ++y)
                        {
                            TDST * __restrict__ ptr = dst.get(y, c);
                            for (unsigned int x = 0; x < width; ++x, ++ptr)
                                *ptr += offset;
                        }
                    }
                }
            }
            
            // Build the lookup table to merge the different region labellings.
            lut.set(current_label);
            for (unsigned int i = 0; i < current_label; ++i)
                lut[i] = i;
            for (unsigned int c = 0; c < number_of_channels; ++c)
                for (unsigned int t = 1; t < number_of_threads; ++t)
                    ConnectedComponentLine(src, dst, connectivity, lut, y_begin[t], c);
            
        }
        else
        {
            if (connectivity == CONNECT4)
            {
                // 1st pass) Set connected components without taking into account the label
                // discrepancies ......................................................................
                for (unsigned int c = 0; c < number_of_channels; ++c, ++current_label)
                {
                    {   // First row of the image (no top row condition).
                        const TSRC * __restrict__ src_ptr = src.get(1, 0, c);
                        TDST * __restrict__ dst_ptr = dst.get(0, c);
                        
                        *dst_ptr = (TDST)current_label;
                        ++dst_ptr;
                        for (unsigned int x = 1; x < width; ++x, ++src_ptr, ++dst_ptr)
                        {
                            if (*src_ptr == *(src_ptr - 1))
                                *dst_ptr = *(dst_ptr - 1);
                            else
                            {
                                ++current_label;
                                *dst_ptr = (TDST)current_label;
                            }
                        }
                    }
                    for (unsigned int y = 1; y < height; ++y)
                    {
                        const TSRC * __restrict__ src_previous_ptr = src.get(y - 1, c);
                        const TDST * __restrict__ dst_previous_ptr = dst.get(y - 1, c);
                        const TSRC * __restrict__ src_current_ptr = src.get(y, c);
                        TDST * __restrict__ dst_current_ptr = dst.get(y, c);
                        
                        // Process the first row pixel.
                        if (*src_current_ptr == *src_previous_ptr) // Up pixel neighbor.
                            *dst_current_ptr = *dst_previous_ptr;
                        else
                        {
                            ++current_label;
                            *dst_current_ptr = current_label;
                        }
                        ++src_previous_ptr;
                        ++src_current_ptr;
                        ++dst_previous_ptr;
                        ++dst_current_ptr;
                        // Process the remaining pixels.
                        for (unsigned int x = 1; x < width; ++x)
                        {
                            if (*src_current_ptr == *(src_current_ptr - 1)) // Left pixel neighbor.
                                *dst_current_ptr = *(dst_current_ptr - 1);
                            else if (*src_current_ptr == *src_previous_ptr) // Up pixel neighbor.
                                *dst_current_ptr = *dst_previous_ptr;
                            else // Assign a new label to the pixel since it has a different value than its neighbors.
                            {
                                ++current_label;
                                *dst_current_ptr = current_label;
                            }
                            
                            ++src_previous_ptr;
                            ++src_current_ptr;
                            ++dst_previous_ptr;
                            ++dst_current_ptr;
                        }
                    }
                }
            }
            else if (connectivity == CONNECT8)
            {
                // 1st pass) Set connected components without taking into account the label
                // discrepancies ......................................................................
                for (unsigned int c = 0; c < number_of_channels; ++c, ++current_label)
                {
                    {   // First row of the image (no top row condition).
                        const TSRC * __restrict__ src_ptr = src.get(1, 0, c);
                        TDST * __restrict__ dst_ptr = dst.get(0, c);
                        
                        *dst_ptr = (TDST)current_label;
                        ++dst_ptr;
                        for (unsigned int x = 1; x < width; ++x, ++src_ptr, ++dst_ptr)
                        {
                            if (*src_ptr == *(src_ptr - 1))
                                *dst_ptr = *(dst_ptr - 1);
                            else
                            {
                                ++current_label;
                                *dst_ptr = (TDST)current_label;
                            }
                        }
                    }
                    for (unsigned int y = 1; y < height; ++y)
                    {
                        const TSRC * __restrict__ src_previous_ptr = src.get(y - 1, c);
                        const TDST * __restrict__ dst_previous_ptr = dst.get(y - 1, c);
                        const TSRC * __restrict__ src_current_ptr  = src.get(y    , c);
                        TDST       * __restrict__ dst_current_ptr  = dst.get(y    , c);
                        
                        // Process the first pixel of the row.
                        if (*src_current_ptr == *src_previous_ptr) // Up-middle pixel.
                            *dst_current_ptr = *dst_previous_ptr;
                        else if (*src_current_ptr == *(src_previous_ptr + 1)) // Up-right pixel.
                            *dst_current_ptr = *(dst_previous_ptr + 1);
                        else
                        {
                            ++current_label;
                            *dst_current_ptr = current_label;
                        }
                        ++src_previous_ptr;
                        ++src_current_ptr;
                        ++dst_previous_ptr;
                        ++dst_current_ptr;
                        // Process the pixels between the first and last rows.
                        for (unsigned int x = 1; x < width - 1; ++x)
                        {
                            if (*src_current_ptr == *(src_current_ptr - 1)) // Left pixel.
                                *dst_current_ptr = *(dst_current_ptr - 1);
                            else if (*src_current_ptr == *(src_previous_ptr - 1)) // Up-left pixel.
                                *dst_current_ptr = *(dst_previous_ptr - 1);
                            else if (*src_current_ptr == *src_previous_ptr) // Up-middle pixel.
                                *dst_current_ptr = *dst_previous_ptr;
                            else if (*src_current_ptr == *(src_previous_ptr + 1)) // Up-right pixel.
                                *dst_current_ptr = *(dst_previous_ptr + 1);
                            else // Assign a new label to the pixel since it has a different value than its neighbors.
                            {
                                ++current_label;
                                *dst_current_ptr = current_label;
                            }
                            
                            ++src_previous_ptr;
                            ++src_current_ptr;
                            ++dst_previous_ptr;
                            ++dst_current_ptr;
                        }
                        // Process the last pixel of the row.
                        if (*src_current_ptr == *(src_current_ptr - 1)) // Left pixel.
                            *dst_current_ptr = *(dst_current_ptr - 1);
                        else if (*src_current_ptr == *(src_previous_ptr - 1)) // Up-left pixel.
                            *dst_current_ptr = *(dst_previous_ptr - 1);
                        else if (*src_current_ptr == *src_previous_ptr) // Up-middle pixel.
                            *dst_current_ptr = *dst_previous_ptr;
                        else // Assign a new label to the pixel since it has a different value than its neighbors.
                        {
                            ++current_label;
                            *dst_current_ptr = current_label;
                        }
                    }
                }
            }
            // 2on pass) Find the incoherent labels and build the lookup table ....................
            ++current_label;
            lut.set(current_label);
            for (unsigned int i = 0; i < current_label; ++i)
                lut[i] = i;
            for (unsigned int c = 0; c < number_of_channels; ++c)
                for (unsigned int y = 1; y < height; ++y)
                    ConnectedComponentLine(src, dst, connectivity, lut, y, c);
        }
        
        for (unsigned int i = 0; i < current_label; ++i)
        {
            unsigned int parent;
            
            for (parent = i; lut[parent] != parent; parent = lut[parent]);
            for (unsigned int index = i; lut[index] != parent; index = lut[index]) lut[index] = parent;
        }
        // Relabel the connected components so that the labels are numbered from 0 to the number of labels.
        for (unsigned int i = 0, k = 0; i < current_label; ++i)
        {
            if (lut[i] == i)
            {
                components_relabel[i] = k;
                ++k;
            }
        }
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < current_label; i += number_of_threads)
                lut[i] = components_relabel[lut[i]];
        }
        
        // Change the region labels using the equivalence lookup table ............................
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    TDST * __restrict__ ptr = dst.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr)
                        *ptr = lut[(unsigned int)*ptr];
                }
            }
        }
        
        return (unsigned int)components_relabel.size();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | WATERSHED FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function calculates the watershed of an image.
     *  \param[in] src source image
     *  \param[in] connectivity identifier of the method used to define the connectivity between pixel neighbors.
     *  \param[out] dst resulting image with the watershed regions.
     *  \param[in] number_of_threads number of threads used to calculate the watershed algorithm.
     */
    template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST>
    unsigned int Segmentation::Watershed(const IMAGE_SRC<TSRC> &src, IMAGE_CONNECTIVITY connectivity, IMAGE_DST<TDST> &dst, unsigned int number_of_threads)
    {
        const unsigned int number_of_channels = src.getNumberOfChannels();
        const unsigned int width = src.getWidth();
        const unsigned int height = src.getHeight();
        std::map<unsigned int, unsigned int> relabeling_lut;
        VectorDense<unsigned int> relations;
        unsigned int number_of_components;
        VectorDense<TSRC> values;
        
        if ((width != dst.getWidth()) || (height != dst.getHeight()) || (number_of_channels != dst.getNumberOfChannels()))
            dst.setGeometry(src);
        if ((connectivity != CONNECT4) && (connectivity != CONNECT8))
            throw Exception("Connectivity (with id '%d') does not apply with the watershed algorithm or it is not implemented yet.", (int)connectivity);
        // Calculate the connected components of the source image.
        number_of_components = ConnectedComponents(src, connectivity, dst, number_of_threads);
        // Initialize the connected components relationship structures.
        relations.set(number_of_components);
        values.set(number_of_components);
        for (unsigned int c = 0; c < number_of_channels; ++c)
        {
            for (unsigned int y = 0; y < height; ++y)
            {
                const TDST * __restrict__ cc_ptr = dst.get(y, c);
                const TSRC * __restrict__ val_ptr = src.get(y, c);
                for (unsigned int x = 0; x < width; ++x, ++val_ptr, ++cc_ptr)
                    values[*cc_ptr] = *val_ptr;
            }
        }
        for (unsigned int i = 0; i < number_of_components; ++i)
            relations[i] = i;
        // Set relationships between the connected components so that each connected component points to the connected component
        // with a lower value.
        for (unsigned int c = 0; c < number_of_channels; ++c)
        {
            // First row of the image.
            {
                const TDST * __restrict__ region_ptr = dst.get(1, 0, c);
                const TSRC * __restrict__ value_ptr = src.get(1, 0, c);
                
                for (unsigned int x = 1; x < width; ++x, ++region_ptr, ++value_ptr)
                    WatershedRelationUpdate(*region_ptr, *value_ptr, *(region_ptr - 1), *(value_ptr - 1), relations, values);
            }
            // Remaining rows of the image.
            if (connectivity == CONNECT4)
            {
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDST * __restrict__ current_region_ptr = dst.get(y, c);
                    const TDST * __restrict__ previous_region_ptr = dst.get(y - 1, c);
                    const TSRC * __restrict__ current_value_ptr = src.get(y, c);
                    const TSRC * __restrict__ previous_value_ptr = src.get(y - 1, c);
                    
                    // Top-middle relationship.
                    WatershedRelationUpdate(*previous_region_ptr, *previous_value_ptr, *current_region_ptr, *current_value_ptr, relations, values);
                    ++current_region_ptr;
                    ++previous_region_ptr;
                    ++current_value_ptr;
                    ++previous_value_ptr;
                    for (unsigned int x = 1; x < width; ++x, ++current_region_ptr, ++previous_region_ptr, ++current_value_ptr, ++previous_value_ptr)
                    {
                        // Left relationship.
                        WatershedRelationUpdate(*(current_region_ptr - 1), *(current_value_ptr - 1), *current_region_ptr, *current_value_ptr, relations, values);
                        // Top-middle relationship.
                        WatershedRelationUpdate(*previous_region_ptr, *previous_value_ptr, *current_region_ptr, *current_value_ptr, relations, values);
                    }
                }
            }
            else/*** if (connectivity == CONNECT8) */
            {
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDST * __restrict__ current_region_ptr = dst.get(y, c);
                    const TDST * __restrict__ previous_region_ptr = dst.get(y - 1, c);
                    const TSRC * __restrict__ current_value_ptr = src.get(y, c);
                    const TSRC * __restrict__ previous_value_ptr = src.get(y - 1, c);
                    
                    // Top-middle relationship.
                    WatershedRelationUpdate(*previous_region_ptr, *previous_value_ptr, *current_region_ptr, *current_value_ptr, relations, values);
                    // Top-right relationship.
                    WatershedRelationUpdate(*(previous_region_ptr + 1), *(previous_value_ptr + 1), *current_region_ptr, *current_value_ptr, relations, values);
                    ++current_region_ptr;
                    ++previous_region_ptr;
                    ++current_value_ptr;
                    ++previous_value_ptr;
                    for (unsigned int x = 1; x < width - 1; ++x, ++current_region_ptr, ++previous_region_ptr, ++current_value_ptr, ++previous_value_ptr)
                    {
                        // Left relationship.
                        WatershedRelationUpdate(*(current_region_ptr - 1), *(current_value_ptr - 1), *current_region_ptr, *current_value_ptr, relations, values);
                        // Top-left relationship.
                        WatershedRelationUpdate(*(previous_region_ptr - 1), *(previous_value_ptr - 1), *current_region_ptr, *current_value_ptr, relations, values);
                        // Top-middle relationship.
                        WatershedRelationUpdate(*previous_region_ptr, *previous_value_ptr, *current_region_ptr, *current_value_ptr, relations, values);
                        // Top-right relationship.
                        WatershedRelationUpdate(*(previous_region_ptr + 1), *(previous_value_ptr + 1), *current_region_ptr, *current_value_ptr, relations, values);
                    }
                    // Left relationship.
                    WatershedRelationUpdate(*(current_region_ptr - 1), *(current_value_ptr - 1), *current_region_ptr, *current_value_ptr, relations, values);
                    // Top-left relationship.
                    WatershedRelationUpdate(*(previous_region_ptr - 1), *(previous_value_ptr - 1), *current_region_ptr, *current_value_ptr, relations, values);
                    // Top-middle relationship.
                    WatershedRelationUpdate(*previous_region_ptr, *previous_value_ptr, *current_region_ptr, *current_value_ptr, relations, values);
                }
            }
        }
        
        for (unsigned int i = 0; i < number_of_components; ++i)
        {
            unsigned int parent;
            
            for (parent = i; relations[parent] != parent; parent = relations[parent]);
            for (unsigned int index = i; relations[index] != parent; index = relations[index]) relations[index] = parent;
        }
        
        for (unsigned int i = 0, k = 0; i < number_of_components; ++i)
        {
            if (relations[i] == i)
            {
                relabeling_lut[i] = k;
                ++k;
            }
        }
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_components; i += number_of_threads)
                relations[i] = relabeling_lut[relations[i]];
        }
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    TDST * __restrict__ ptr = dst.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr)
                        *ptr = relations[(unsigned int)*ptr];
                }
            }
        }
        
        return (unsigned int)relabeling_lut.size();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | THINNING FUNCTIONS                   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TINPUT, class TOUTPUT>
    void Segmentation::thinning(const ConstantSubImage<TINPUT> &input, TOUTPUT background_value, THINNING_METHOD method, SubImage<TOUTPUT>  output, unsigned int number_of_threads)
    {
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int nchannels = input.getNumberOfChannels();
        // ----------------------------------------------------------------------------------------
        std::list<std::tuple<int, int> > * selected_elements_thr;
        bool * change_made_thr;
        Image<TOUTPUT> work_image;
        
        if ((width != output.getWidth()) || (height != output.getHeight()) || (nchannels != output.getNumberOfChannels()))
            throw Exception("Input and output images have a different geometry [%dx%dx%d] != [%dx%dx%d]", width, height, nchannels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        work_image.setGeometry(width + 2, height + 2, 1);
        if (number_of_threads > 1)
        {
            selected_elements_thr = new std::list<std::tuple<int, int> >[number_of_threads];
            change_made_thr = new bool[number_of_threads];
        }
        else { selected_elements_thr = 0; change_made_thr = 0; }
        for (unsigned int c = 0; c < nchannels; ++c)
        {
            bool change_made;
            
            work_image.setValue(background_value);
            work_image.subimage(1, 1, width, height).copy(input.subimage(c));
            change_made = false;
            for (unsigned int iter = 0; ; ++iter)
            {
                const bool iter_even = iter % 2 == 0;
                
                if (number_of_threads > 1)
                {
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        
                        selected_elements_thr[thread_identifier].clear();
                        for (unsigned int y = thread_identifier; y < height + 2; y += number_of_threads)
                        {
                            const unsigned char * __restrict__ ptr = work_image.get(y, 0);
                            for (unsigned int x = 0; x < width + 2; ++x, ++ptr)
                            {
                                if (*ptr != background_value)
                                {
                                    bool p2, p3, p4, p5, p6, p7, p8, p9, select;
                                    
                                    p2 = *work_image.get(x    , y - 1, 0);
                                    p3 = *work_image.get(x + 1, y - 1, 0);
                                    p4 = *work_image.get(x + 1, y    , 0);
                                    p5 = *work_image.get(x + 1, y + 1, 0);
                                    p6 = *work_image.get(x    , y + 1, 0);
                                    p7 = *work_image.get(x - 1, y + 1, 0);
                                    p8 = *work_image.get(x - 1, y    , 0);
                                    p9 = *work_image.get(x - 1, y - 1, 0);
                                    if (method == THIN_ZHANG_SUEN) // Zhang-Suen thinning algorithm.
                                    {
                                        int A, B, m1, m2;
                                        
                                        A  = (!p2 && p3) + (!p3 && p4) + (!p4 && p5) + (!p5 && p6) + (!p6 && p7) + (!p7 && p8) + (!p8 && p9) + (!p9 && p2);
                                        B  = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
                                        m1 = ((iter_even)?(p2 * p4 * p6):(p2 * p4 * p8));
                                        m2 = ((iter_even)?(p4 * p6 * p8):(p2 * p6 * p8));
                                        select = ((A == 1) && ((B >= 2) && (B <= 6)) && !m1 && !m2);
                                    }
                                    else // THIN_GUO_HALL          // Guo-Hall thinning algorithm. [THERE IS NO NEED TO CHECK, THERE ARE ONLY TWO METHODS].
                                    {
                                        int C, N1, N2, N, m;
                                        
                                        C  = (!p2 && (p3 | p4)) + (!p4 && (p5 | p6)) + (!p6 && (p7 | p8)) + (!p8 && (p9 | p2));
                                        N1 = (p9 | p2) + (p3 | p4) + (p5 | p6) + (p7 | p8);
                                        N2 = (p2 | p3) + (p4 | p5) + (p6 | p7) + (p8 | p9);
                                        N  = (N1 < N2)?N1:N2;
                                        m  = (iter_even)?((p6 | p7 | !p9) & p8):((p2 | p3 | !p5) & p4);
                                        select = (C == 1 && (N >= 2 && N <= 3) && m == 0);
                                    }
                                    
                                    if (select) selected_elements_thr[thread_identifier].push_back(std::make_tuple(x, y));
                                }
                            }
                        }
                    }
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        
                        change_made_thr[thread_identifier] = change_made;
                        for (auto &pos : selected_elements_thr[thread_identifier])
                        {
                            TOUTPUT * value = work_image.get(std::get<0>(pos), std::get<1>(pos), 0);
                            if (!change_made_thr[thread_identifier]) change_made_thr[thread_identifier] = *value != background_value;
                            *value = background_value;
                        }
                    }
                    for (unsigned int t = 0; t < number_of_threads; ++t)
                        change_made = change_made || change_made_thr[t];
                }
                else
                {
                    std::list<std::tuple<int, int> > selected_elements;
                    
                    for (unsigned int y = 0; y < height + 2; ++y)
                    {
                        const unsigned char * __restrict__ ptr = work_image.get(y, 0);
                        for (unsigned int x = 0; x < width + 2; ++x, ++ptr)
                        {
                            if (*ptr != background_value)
                            {
                                bool p2, p3, p4, p5, p6, p7, p8, p9, select;
                                
                                p2 = *work_image.get(x    , y - 1, 0);
                                p3 = *work_image.get(x + 1, y - 1, 0);
                                p4 = *work_image.get(x + 1, y    , 0);
                                p5 = *work_image.get(x + 1, y + 1, 0);
                                p6 = *work_image.get(x    , y + 1, 0);
                                p7 = *work_image.get(x - 1, y + 1, 0);
                                p8 = *work_image.get(x - 1, y    , 0);
                                p9 = *work_image.get(x - 1, y - 1, 0);
                                if (method == THIN_ZHANG_SUEN) // Zhang-Suen thinning algorithm.
                                {
                                    int A, B, m1, m2;
                                    
                                    A  = (!p2 && p3) + (!p3 && p4) + (!p4 && p5) + (!p5 && p6) + (!p6 && p7) + (!p7 && p8) + (!p8 && p9) + (!p9 && p2);
                                    B  = p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9;
                                    m1 = ((iter_even)?(p2 * p4 * p6):(p2 * p4 * p8));
                                    m2 = ((iter_even)?(p4 * p6 * p8):(p2 * p6 * p8));
                                    select = ((A == 1) && ((B >= 2) && (B <= 6)) && !m1 && !m2);
                                }
                                else // THIN_GUO_HALL          // Guo-Hall thinning algorithm. [THERE IS NO NEED TO CHECK, THERE ARE ONLY TWO METHODS].
                                {
                                    int C, N1, N2, N, m;
                                    
                                    C  = (!p2 && (p3 | p4)) + (!p4 && (p5 | p6)) + (!p6 && (p7 | p8)) + (!p8 && (p9 | p2));
                                    N1 = (p9 | p2) + (p3 | p4) + (p5 | p6) + (p7 | p8);
                                    N2 = (p2 | p3) + (p4 | p5) + (p6 | p7) + (p8 | p9);
                                    N  = (N1 < N2)?N1:N2;
                                    m  = (iter_even)?((p6 | p7 | !p9) & p8):((p2 | p3 | !p5) & p4);
                                    select = (C == 1 && (N >= 2 && N <= 3) && m == 0);
                                }
                                
                                if (select) selected_elements.push_back(std::make_tuple(x, y));
                            }
                        }
                    }
                    
                    for (auto &pos : selected_elements)
                    {
                        TOUTPUT * value = work_image.get(std::get<0>(pos), std::get<1>(pos), 0);
                        if (!change_made) change_made = *value != background_value;
                        *value = background_value;
                    }
                }
                
                if (!iter_even)
                {
                    if (change_made) change_made = false;
                    else break;
                }
            }
            output.subimage(c).copy(work_image.subimage(1, 1, width, height));
        }
        if (number_of_threads > 1)
        {
            delete [] selected_elements_thr;
            delete [] change_made_thr;
        }
    }
    
    template <template <class> class IMAGE, class TIMAGE>
    void Segmentation::detectJoints(const IMAGE<TIMAGE> &input, TIMAGE background, std::list<std::tuple<int, int, int> > &coordinates, unsigned int number_of_threads)
    {
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int nchannels = input.getNumberOfChannels();
        
        coordinates.clear();
        for (unsigned int c = 0; c < nchannels; ++c)
        {
            if (number_of_threads > 1)
            {
                std::list<std::tuple<int, int, int> > * coordinates_thr;
                
                coordinates_thr = new std::list<std::tuple<int, int, int> >[number_of_threads];
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    for (unsigned int y = 1 + thread_id; y < height - 1; y += number_of_threads)
                    {
                        const TIMAGE * __restrict__ ptr_prev = input.get(1, y - 1, c);
                        const TIMAGE * __restrict__ ptr_curr = input.get(1, y    , c);
                        const TIMAGE * __restrict__ ptr_next = input.get(1, y + 1, c);
                        for (unsigned int x = 1; x < width - 1; ++x, ++ptr_prev, ++ptr_curr, ++ptr_next)
                        {
                            if (*ptr_curr != background)
                            {
                                unsigned int counter;
                                
                                counter = 1;
                                if (*(ptr_prev - 1) != background) ++counter;
                                if ( *ptr_prev      != background) ++counter;
                                if (*(ptr_prev + 1) != background) ++counter;
                                if (*(ptr_curr - 1) != background) ++counter;
                                if (*(ptr_curr + 1) != background) ++counter;
                                if (*(ptr_next - 1) != background) ++counter;
                                if ( *ptr_next      != background) ++counter;
                                if (*(ptr_next + 1) != background) ++counter;
                                if (counter > 3)
                                    coordinates_thr[thread_id].push_back(std::make_tuple(x, y, c));
                            }
                        }
                    }
                }
                for (unsigned int t = 0; t < number_of_threads; ++t)
                    for (auto const &v : coordinates_thr[t])
                        coordinates.push_back(v);
                delete [] coordinates_thr;
            }
            else
            {
                for (unsigned int y = 1; y < height - 1; ++y)
                {
                    const TIMAGE * __restrict__ ptr_prev = input.get(1, y - 1, c);
                    const TIMAGE * __restrict__ ptr_curr = input.get(1, y    , c);
                    const TIMAGE * __restrict__ ptr_next = input.get(1, y + 1, c);
                    for (unsigned int x = 1; x < width - 1; ++x, ++ptr_prev, ++ptr_curr, ++ptr_next)
                    {
                        if (*ptr_curr != background)
                        {
                            unsigned int counter;
                            
                            counter = 1;
                            if (*(ptr_prev - 1) != background) ++counter;
                            if ( *ptr_prev      != background) ++counter;
                            if (*(ptr_prev + 1) != background) ++counter;
                            if (*(ptr_curr - 1) != background) ++counter;
                            if (*(ptr_curr + 1) != background) ++counter;
                            if (*(ptr_next - 1) != background) ++counter;
                            if ( *ptr_next      != background) ++counter;
                            if (*(ptr_next + 1) != background) ++counter;
                            if (counter > 3)
                                coordinates.push_back(std::make_tuple(x, y, c));
                        }
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SLIC SEGMENTATION FUNCTION IMPLEMEN. |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <template <class> class IMAGE, class T>
    unsigned int Segmentation::Slic(const IMAGE<T> &source_image, unsigned int step, double compactness, bool perturb_seeds, bool convert_cielab, Image<unsigned int> &cluster_selected, unsigned int number_of_threads)
    {
        typedef typename MODE_TYPE<T>::TYPE TMODE;
        if (source_image.getNumberOfChannels() != 3) throw Exception("Input image must have 3 channels.");
        const unsigned int width = source_image.getWidth();
        const unsigned int height = source_image.getHeight();
        const double invwt = compactness * compactness / (double)(step * step);
        const unsigned int number_of_iterations = 10;
        
        Image<T> lab_image(width, height, 3);
        VectorDense<SlicCentroid<TMODE> > centroids;
        Image<TMODE> cluster_distance(width, height, 1);
        VectorDense<unsigned int> cluster_size;
        unsigned int number_of_labels;
        if ((cluster_selected.getWidth() != width) || (cluster_selected.getHeight() != height) || (cluster_selected.getNumberOfChannels() != 1))
            cluster_selected.setGeometry(source_image.getWidth(), source_image.getHeight(), 1);
        
        // Convert the image to the Lab color-space ...............................................
        if (convert_cielab)
            ImageRGB2CIELab(source_image, lab_image, number_of_threads);
        else lab_image.copy(source_image);
        
        // Set the location of the seeds in the original image ....................................
        SlicInitializeCentroids(lab_image.getWidth(), lab_image.getHeight(), step, centroids);
        SlicInitializeSeeds(lab_image, perturb_seeds, centroids, number_of_threads);
        
        // Perform k-means segmentation ...........................................................
        cluster_selected.setValue(20);
        cluster_size.set(centroids.size());
        if (number_of_threads > 1)
        {
            for (unsigned int iteration = 0; iteration < number_of_iterations; ++iteration)
            {
                // Search the closest centroid for each pixel of the image.
                cluster_distance.setValue(std::numeric_limits<TMODE>::max());
                for (unsigned int id = 0; id < centroids.size(); ++id)
                {
                    int x0, y0, x1, y1;
                    
                    x0 = srvMax<int>(0, (int)centroids[id].getX() - step);
                    y0 = srvMax<int>(0, (int)centroids[id].getY() - step);
                    x1 = srvMin<int>((int)width, (int)centroids[id].getX() + step);
                    y1 = srvMin<int>((int)height, (int)centroids[id].getY() + step);
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (int y = y0 + omp_get_thread_num(); y < y1; y += number_of_threads)
                            SlicLineDistance(lab_image, x0, x1, y, centroids, id, invwt, cluster_distance, cluster_selected);
                    }
                }
                // Re-calculate the centroids.
                SlicRecalculateCentroids(lab_image, cluster_distance, cluster_selected, centroids, cluster_size);
            }
        }
        else
        {
            for (unsigned int iteration = 0; iteration < number_of_iterations; ++iteration)
            {
                // Search the closest centroid for each pixel of the image.
                cluster_distance.setValue(std::numeric_limits<TMODE>::max());
                for (unsigned int id = 0; id < centroids.size(); ++id)
                {
                    int x0, y0, x1, y1;
                    
                    x0 = srvMax<int>(0, (int)centroids[id].getX() - step);
                    y0 = srvMax<int>(0, (int)centroids[id].getY() - step);
                    x1 = srvMin<int>((int)width, (int)centroids[id].getX() + step);
                    y1 = srvMin<int>((int)height, (int)centroids[id].getY() + step);
                    
                    for (int y = y0; y < y1; ++y)
                        SlicLineDistance(lab_image, x0, x1, y, centroids, id, invwt, cluster_distance, cluster_selected);
                }
                // Re-calculate the centroids.
                SlicRecalculateCentroids(lab_image, cluster_distance, cluster_selected, centroids, cluster_size);
            }
        }
        
        // Enforce label connectivity .............................................................
        number_of_labels = SlicEnforceConnectivity(step, cluster_selected);
        
        return number_of_labels;
    }
    
    template <template <class> class IMAGE, class T>
    unsigned int Segmentation::AdaptiveSlic(const IMAGE<T> &source_image, unsigned int step, bool perturb_seeds, bool convert_cielab, Image<unsigned int> &cluster_selected, unsigned int number_of_threads)
    {
        typedef float TMODE;
        ////typedef double TMODE;
        if (source_image.getNumberOfChannels() != 3) throw Exception("Input image must have 3 channels.");
        const unsigned int width = source_image.getWidth();
        const unsigned int height = source_image.getHeight();
        const double invwt = 1.0 / (double)(step * step);
        const unsigned int number_of_iterations = 10;
        
        Image<T> lab_image(width, height, 3);
        VectorDense<SlicCentroid<TMODE> > centroids;
        Image<TMODE> cluster_distance(width, height, 1);
        VectorDense<unsigned int> cluster_size;
        VectorDense<TMODE> cluster_maxlab;
        unsigned int number_of_labels;
        if ((cluster_selected.getWidth() != width) || (cluster_selected.getHeight() != height) || (cluster_selected.getNumberOfChannels() != 1))
            cluster_selected.setGeometry(source_image.getWidth(), source_image.getHeight(), 1);
        
        // Convert the image to the Lab color-space ...............................................
        if (convert_cielab)
            ImageRGB2CIELab(source_image, lab_image, number_of_threads);
        else lab_image.copy(source_image);
        
        // Set the location of the seeds in the original image ....................................
        SlicInitializeCentroids(lab_image.getWidth(), lab_image.getHeight(), step, centroids);
        SlicInitializeSeeds(lab_image, perturb_seeds, centroids, number_of_threads);
        
        // Perform k-means segmentation ...........................................................
        cluster_selected.setValue(20);
        cluster_size.set(centroids.size());
        cluster_maxlab.set(centroids.size(), 100);
        if (number_of_threads > 1)
        {
            for (unsigned int iteration = 0; iteration < number_of_iterations; ++iteration)
            {
                // Search the closest centroid for each pixel of the image.
                cluster_distance.setValue(std::numeric_limits<TMODE>::max());
                for (unsigned int id = 0; id < centroids.size(); ++id)
                {
                    VectorDense<TMODE> maxlab_update(number_of_threads, ((iteration == 0)?1:(cluster_maxlab[id])));
                    int x0, y0, x1, y1;
                    
                    x0 = srvMax<int>(0, (int)centroids[id].getX() - step);
                    y0 = srvMax<int>(0, (int)centroids[id].getY() - step);
                    x1 = srvMin<int>((int)width, (int)centroids[id].getX() + step);
                    y1 = srvMin<int>((int)height, (int)centroids[id].getY() + step);
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_id = omp_get_thread_num();
                        for (int y = y0 + thread_id; y < y1; y += number_of_threads)
                            SlicLineDistanceAdapt(lab_image, x0, x1, y, centroids, id, cluster_maxlab[id], invwt, cluster_distance, cluster_selected, maxlab_update[thread_id]);
                    }
                    cluster_maxlab[id] = maxlab_update[0];
                    for (unsigned int t = 1; t < number_of_threads; ++t)
                        if (maxlab_update[t] > cluster_maxlab[id])
                            cluster_maxlab[id] = maxlab_update[t];
                }
                // Re-calculate the centroids.
                SlicRecalculateCentroids(lab_image, cluster_distance, cluster_selected, centroids, cluster_size);
            }
        }
        else
        {
            for (unsigned int iteration = 0; iteration < number_of_iterations; ++iteration)
            {
                // Search the closest centroid for each pixel of the image.
                cluster_distance.setValue(std::numeric_limits<TMODE>::max());
                for (unsigned int id = 0; id < centroids.size(); ++id)
                {
                    TMODE maxlab_update;
                    int x0, y0, x1, y1;
                    
                    x0 = srvMax<int>(0, (int)centroids[id].getX() - step);
                    y0 = srvMax<int>(0, (int)centroids[id].getY() - step);
                    x1 = srvMin<int>((int)width, (int)centroids[id].getX() + step);
                    y1 = srvMin<int>((int)height, (int)centroids[id].getY() + step);
                    if (iteration == 0) maxlab_update = 1;
                    else maxlab_update = cluster_maxlab[id];
                    
                    for (int y = y0; y < y1; ++y)
                        SlicLineDistanceAdapt(lab_image, x0, x1, y, centroids, id, cluster_maxlab[id], invwt, cluster_distance, cluster_selected, maxlab_update);
                    cluster_maxlab[id] = maxlab_update;
                }
                // Re-calculate the centroids.
                SlicRecalculateCentroids(lab_image, cluster_distance, cluster_selected, centroids, cluster_size);
            }
        }
        
        // Enforce label connectivity .............................................................
        number_of_labels = SlicEnforceConnectivity(step, cluster_selected);
        
        return number_of_labels;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | WATERSHED AUXILIARY FUNCTIONS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TSRC>
    void Segmentation::WatershedRelationUpdate(unsigned int first_index, const TSRC &first_value, unsigned int second_index, const TSRC &second_value, VectorDense<unsigned int> &relations, VectorDense<TSRC> &values)
    {
        if (first_index != second_index)
        {
            if (values[second_index] > first_value)
            {
                values[second_index] = first_value;
                relations[second_index] = first_index;
            }
            else if (values[first_index] > second_value)
            {
                values[first_index] = second_value;
                relations[first_index] = second_index;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONNECTED COMPONENTS AUXILIARY       |
    //                   | FUNCTIONS                            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    unsigned int Segmentation::ConnectedComponentsStackFind(VectorDense<T> &stack_table, unsigned int index)
    {
        unsigned int current_parent;
        
        // Search the head node of the 'spaghetti stack'.
        for (current_parent = index; stack_table[current_parent] != current_parent; current_parent = stack_table[current_parent]);
        // Modify the pointers of the stack so that it is faster to search the parent for following queries.
        while (stack_table[index] != current_parent)
        {
            unsigned int aux = index;
            stack_table[index] = current_parent;
            index = stack_table[aux];
        }
        return current_parent;
    }
    
    template <template <class> class IMAGE_SRC, class TSRC, template <class> class IMAGE_DST, class TDST, class TLUT>
    void Segmentation::ConnectedComponentLine(const IMAGE_SRC<TSRC> &src, const IMAGE_DST<TDST> &dst, IMAGE_CONNECTIVITY connectivity, VectorDense<TLUT> &lut, unsigned int line, unsigned int channel)
    {
        const unsigned int width = src.getWidth();
        
        if (connectivity == CONNECT4)
        {
            const TDST * __restrict__ dst_previous_ptr = dst.get(line - 1, channel);
            const TSRC * __restrict__ src_previous_ptr = src.get(line - 1, channel);
            const TDST * __restrict__ dst_current_ptr = dst.get(line, channel);
            const TSRC * __restrict__ src_current_ptr = src.get(line, channel);
            
            for (unsigned int x = 0; x < width; ++x)
            {
                // If there is an incoherence in the connected component labeling modify the lookup table.
                if ((*src_current_ptr == *src_previous_ptr) && (*dst_current_ptr != *dst_previous_ptr))
                {
                    unsigned int current_parent, previous_parent;
                    
                    // Search the parents of each pixel...
                    current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                    previous_parent = ConnectedComponentsStackFind(lut, *dst_previous_ptr);
                    // ...and update the parents in they are different.
                    if (previous_parent != current_parent)
                        lut[current_parent] = previous_parent;
                }
                ++src_previous_ptr;
                ++src_current_ptr;
                ++dst_previous_ptr;
                ++dst_current_ptr;
            }
        }
        else if (connectivity == CONNECT8)
        {
            const TDST * __restrict__ dst_previous_ptr = dst.get(line - 1, channel);
            const TSRC * __restrict__ src_previous_ptr = src.get(line - 1, channel);
            const TDST * __restrict__ dst_current_ptr = dst.get(line, channel);
            const TSRC * __restrict__ src_current_ptr = src.get(line, channel);
            
            // Search for incoherences in the first pixel of the row.
            if ((*src_current_ptr == *src_previous_ptr) && (*dst_current_ptr != *dst_previous_ptr)) // Up-middle pixel.
            {
                unsigned int current_parent, previous_parent;
                
                // Search the parents of each pixel...
                current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                previous_parent = ConnectedComponentsStackFind(lut, *dst_previous_ptr);
                // ...and update the parents in they are different.
                if (previous_parent != current_parent)
                    lut[current_parent] = previous_parent;
            }
            if ((*src_current_ptr == *(src_previous_ptr + 1)) && (*dst_current_ptr != *(dst_previous_ptr + 1))) // Up-right pixel.
            {
                unsigned int current_parent, previous_parent;
                
                // Search the parents of each pixel...
                current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                previous_parent = ConnectedComponentsStackFind(lut, *(dst_previous_ptr + 1));
                // ...and update the parents in they are different.
                if (previous_parent != current_parent)
                    lut[current_parent] = previous_parent;
            }
            ++src_previous_ptr;
            ++src_current_ptr;
            ++dst_previous_ptr;
            ++dst_current_ptr;
            for (unsigned int x = 1; x < width - 1; ++x)
            {
                if ((*src_current_ptr == *(src_previous_ptr - 1)) && (*dst_current_ptr != *(dst_previous_ptr - 1))) // Up-left pixel.
                {
                    unsigned int current_parent, previous_parent;
                    
                    // Search the parents of each pixel...
                    current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                    previous_parent = ConnectedComponentsStackFind(lut, *(dst_previous_ptr - 1));
                    // ...and update the parents in they are different.
                    if (previous_parent != current_parent)
                        lut[current_parent] = previous_parent;
                }
                if ((*src_current_ptr == *src_previous_ptr) && (*dst_current_ptr != *dst_previous_ptr)) // Up-middle pixel.
                {
                    unsigned int current_parent, previous_parent;
                    
                    // Search the parents of each pixel...
                    current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                    previous_parent = ConnectedComponentsStackFind(lut, *dst_previous_ptr);
                    // ...and update the parents in they are different.
                    if (previous_parent != current_parent)
                        lut[current_parent] = previous_parent;
                }
                if ((*src_current_ptr == *(src_previous_ptr + 1)) && (*dst_current_ptr != *(dst_previous_ptr + 1))) // Up-right pixel.
                {
                    unsigned int current_parent, previous_parent;
                    
                    // Search the parents of each pixel...
                    current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                    previous_parent = ConnectedComponentsStackFind(lut, *(dst_previous_ptr + 1));
                    // ...and update the parents in they are different.
                    if (previous_parent != current_parent)
                        lut[current_parent] = previous_parent;
                }
                ++src_previous_ptr;
                ++src_current_ptr;
                ++dst_previous_ptr;
                ++dst_current_ptr;
            }
            // Search for incoherences in the last pixel of the row.
            if ((*src_current_ptr == *(src_previous_ptr - 1)) && (*dst_current_ptr != *(dst_previous_ptr - 1))) // Up-left pixel.
            {
                unsigned int current_parent, previous_parent;
                
                // Search the parents of each pixel...
                current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                previous_parent = ConnectedComponentsStackFind(lut, *(dst_previous_ptr - 1));
                // ...and update the parents in they are different.
                if (previous_parent != current_parent)
                    lut[current_parent] = previous_parent;
            }
            if ((*src_current_ptr == *src_previous_ptr) && (*dst_current_ptr != *dst_previous_ptr)) // Up-middle pixel.
            {
                unsigned int current_parent, previous_parent;
                
                // Search the parents of each pixel...
                current_parent = ConnectedComponentsStackFind(lut, *dst_current_ptr);
                previous_parent = ConnectedComponentsStackFind(lut, *dst_previous_ptr);
                // ...and update the parents in they are different.
                if (previous_parent != current_parent)
                    lut[current_parent] = previous_parent;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SLIC AUXILIARY FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TMODE>
    void Segmentation::SlicInitializeCentroids(unsigned int width, unsigned int height, unsigned int step, VectorDense<SlicCentroid<TMODE> > &centroids)
    {
        unsigned int x_strips, y_strips, x_offset, y_offset;
        double x_residual, y_residual;
        
        x_strips = (unsigned int)(0.5 + (double)width / (double)step);
        y_strips = (unsigned int)(0.5 + (double)height / (double)step);
        if (x_strips * step > width) --x_strips;
        if (y_strips * step > height) --y_strips;
        x_residual = (double)(width - x_strips * step) / (double)x_strips;
        y_residual = (double)(height - y_strips * step) / (double)y_strips;
        x_offset = step / 2;
        y_offset = step / 2;
        centroids.set(x_strips * y_strips);
        for (unsigned int y = 0, k = 0; y < y_strips; ++y)
        {
            const TMODE y_coordinate = (TMODE)(y_offset + y * step + (double)y * y_residual);
            for (unsigned int x = 0; x < x_strips; ++x, ++k)
                centroids[k].set((TMODE)(x_offset + x * step + (double)x * x_residual), y_coordinate);
        }
    }
    
    template <class T, class TMODE>
    void Segmentation::SlicInitializeSeeds(Image<T> &image, bool perturb_seeds, VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int number_of_threads)
    {
        const int dx8[] = { -1,  0,  1, -1,  0,  1, -1,  0,  1 };
        const int dy8[] = { -1, -1, -1,  0,  0,  0,  1,  1,  1 };
        const unsigned int width = image.getWidth();
        const unsigned int height = image.getHeight();
        
        if (perturb_seeds) // Change the location of the seeds to avoid edges.
        {
            // Calculate the value of the edges module at each pixel.
            Image<TMODE> edges(width, height, 1);
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                TMODE * __restrict__ edges_first = edges.get(0, 0);
                TMODE * __restrict__ edges_last = edges.get(height - 1, 0);
                
                for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                {
                    *edges_first = 0;
                    *edges_last = 0;
                }
                
                for (unsigned int y = 1; y < height - 1; ++y)
                {
                    const T * __restrict__ l_ptr = image.get(y, 0) + 1;
                    const T * __restrict__ a_ptr = image.get(y, 1) + 1;
                    const T * __restrict__ b_ptr = image.get(y, 2) + 1;
                    TMODE * __restrict__ edge_ptr = edges.get(y, 0);
                    
                    for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                    {
                        if ((x == 0) || (x == width - 1)) *edge_ptr = 0;
                        else
                        {
                            TMODE dxl, dxa, dxb, dyl, dya, dyb, dx, dy;
                            
                            dxl = (TMODE)*(l_ptr - 1) - (TMODE)*(l_ptr + 1);
                            dxa = (TMODE)*(a_ptr - 1) - (TMODE)*(a_ptr + 1);
                            dxb = (TMODE)*(b_ptr - 1) - (TMODE)*(b_ptr + 1);
                            dyl = (TMODE)*(l_ptr - width) - (TMODE)*(l_ptr + width);
                            dya = (TMODE)*(a_ptr - width) - (TMODE)*(a_ptr + width);
                            dyb = (TMODE)*(b_ptr - width) - (TMODE)*(b_ptr + width);
                            dx = dxl * dxl + dxa * dxa + dxb * dxb;
                            dy = dyl * dyl + dya * dya + dyb * dyb;
                            *edge_ptr = dx * dx + dy * dy;
                        }
                        l_ptr += number_of_threads;
                        a_ptr += number_of_threads;
                        b_ptr += number_of_threads;
                        edge_ptr += number_of_threads;
                    }
                }
            }
            // Use the module edges image to avoid local extrema as initial seed location.
            for (unsigned int k = 0; k < centroids.size(); ++k)
            {
                int selected_x, selected_y;
                TMODE minimum_value;
                
                minimum_value = *edges.get((int)centroids[k].getX(), (int)centroids[k].getY(), 0);
                selected_x = (int)centroids[k].getX();
                selected_y = (int)centroids[k].getY();
                for (unsigned int i = 0; i < 8; ++i)
                {
                    const int cx = (int)centroids[k].getX() + dx8[i];
                    const int cy = (int)centroids[k].getY() + dy8[i];
                    TMODE current_value;
                    
                    if ((cx >= 0) && (cx < (signed)width) && (cy >= 0) && (cy < (signed)height))
                    {
                        current_value = *edges.get(cx, cy, 0);
                        if (current_value < minimum_value)
                        {
                            minimum_value = current_value;
                            selected_x = cx;
                            selected_y = cy;
                        }
                    }
                }
                centroids[k].set((TMODE)selected_x, (TMODE)selected_y,   // Seed coordinates in the image.
                                 *image.get(selected_x, selected_y, 0),  // Values of the pixel at each channel (e.g.
                                 *image.get(selected_x, selected_y, 1),  // values in channels L, a and b of the CIE 
                                 *image.get(selected_x, selected_y, 2)); // L*a*b color space).
            }
        }
        else
        {
            for (unsigned int k = 0; k < centroids.size(); ++k)
            {
                const unsigned int selected_x = (unsigned int)centroids[k].getX();
                const unsigned int selected_y = (unsigned int)centroids[k].getY();
                centroids[k].set((TMODE)selected_x, (TMODE)selected_y,   // Seed coordinates in the image.
                                 *image.get(selected_x, selected_y, 0),  // Values of the pixel at each channel (e.g.
                                 *image.get(selected_x, selected_y, 1),  // values in channels L, a and b of the CIE
                                 *image.get(selected_x, selected_y, 2)); // L*a*b color space).
            }
        }
    }
    
    template <class T>
    unsigned int Segmentation::SlicEnforceConnectivity(unsigned int step, Image<T> &cluster_selected)
    {
        // Constants ..............................................................................
        const unsigned int width = cluster_selected.getWidth();
        const unsigned int height = cluster_selected.getHeight();
        const int dx4[] = { -1,  0,  1,  0};
        const int dy4[] = {  0, -1,  0,  1};
        ////const unsigned int size_threshold = (width * height) / (4 * step * step);
        const unsigned int size_threshold = step * step / 4;
        // Variables ..............................................................................
        Image<int> new_cluster_selected(width, height, 1);
        VectorDense<int> xvec(width * height), yvec(width * height);
        int label, adjacent_label;
        
        new_cluster_selected.setValue(-1);
        label = 0;
        adjacent_label = 0;
        for (unsigned int y = 0; y < height; ++y)
        {
            unsigned int * __restrict__ old_ptr = cluster_selected.get(y, 0);
            int * __restrict__ new_ptr = new_cluster_selected.get(y, 0);
            
            for (unsigned int x = 0; x < width; ++x, ++new_ptr, ++old_ptr)
            {
                if (*new_ptr == -1)
                {
                    unsigned int count;
                    
                    *new_ptr = label;
                    xvec[0] = (signed)x; // Start a new segment.
                    yvec[0] = (signed)y;
                    for (unsigned int n = 0; n < 4; ++n) // Find an adjacent label for use later if needed.
                    {
                        int lx, ly;
                        
                        lx = xvec[0] + dx4[n];
                        ly = yvec[0] + dy4[n];
                        if ((lx >= 0) && (lx < (signed)width) && (ly >= 0) && (ly < (signed)height))
                        {
                            int neighbor_label = *new_cluster_selected.get(lx, ly, 0);
                            if (neighbor_label >= 0) adjacent_label = neighbor_label;
                        }
                    }
                    
                    count = 1;
                    for (unsigned int c = 0; c < count; ++c)
                    {
                        for (unsigned int n = 0; n < 4; ++n)
                        {
                            int lx, ly;
                            
                            lx = xvec[c] + dx4[n];
                            ly = yvec[c] + dy4[n];
                            if ((lx >= 0) && (lx < (signed)width) && (ly >= 0) && (ly < (signed)height))
                            {
                                int * new_location = new_cluster_selected.get(lx, ly, 0);
                                if ((*new_location == -1) && // Unassigned pixel and ...
                                    (*cluster_selected.get(lx, ly, 0) == *old_ptr)) // both pixels have the same label.
                                {
                                    xvec[count] = lx;
                                    yvec[count] = ly;
                                    *new_location = label;
                                    ++count;
                                }
                            }
                        }
                    }
                    // If segment size is less than the limit, assign an adjacent label found before and decrement label count.
                    if (count <= size_threshold)
                    {
                        for (unsigned int c = 0; c < count; ++c)
                            *new_cluster_selected.get(xvec[c], yvec[c], 0) = adjacent_label;
                        --label;
                    }
                    ++label;
                }
            }
        }
        
        // Copy the new segmentation to the result image.
        cluster_selected.copy(new_cluster_selected);
        
        return (unsigned int)label;
    }
    
    template <class T, class TMODE>
    void Segmentation::SlicLineDistance(const Image<T> &image, int x0, int x1, int y, const VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int id, double invwt, Image<TMODE> &cluster_distance, Image<unsigned int> &cluster_selected)
    {
        TMODE * __restrict__ distance_ptr = cluster_distance.get(x0, y, 0);
        unsigned int * __restrict__ selected_ptr = cluster_selected.get(x0, y, 0);
        const T * __restrict__ current_l = image.get(x0, y, 0);
        const T * __restrict__ current_a = image.get(x0, y, 1);
        const T * __restrict__ current_b = image.get(x0, y, 2);
        
        for (int x = x0; x < x1; ++x)
        {
            TMODE distance, dx, dy, dl, da, db;
            
            dx = ((TMODE)x - centroids[id].getX());
            dy = ((TMODE)y - centroids[id].getY());
            dl = ((TMODE)*current_l - centroids[id].getL());
            da = ((TMODE)*current_a - centroids[id].getA());
            db = ((TMODE)*current_b - centroids[id].getB());
            distance = dl * dl + da * da + db * db;
            distance += (TMODE)(invwt * (double)(dx * dx + dy * dy));
            
            if (distance < *distance_ptr)
            {
                *distance_ptr = distance;
                *selected_ptr = id;
            }
            ++distance_ptr;
            ++selected_ptr;
            ++current_l;
            ++current_a;
            ++current_b;
        }
    }
    
    template <class T, class TMODE>
    void Segmentation::SlicLineDistanceAdapt(const Image<T> &image, int x0, int x1, int y, const VectorDense<SlicCentroid<TMODE> > &centroids, unsigned int id, TMODE maxlab, double invwt, Image<TMODE> &cluster_distance, Image<unsigned int> &cluster_selected, TMODE &maxlab_update)
    {
        TMODE * __restrict__ distance_ptr = cluster_distance.get(x0, y, 0);
        unsigned int * __restrict__ selected_ptr = cluster_selected.get(x0, y, 0);
        const T * __restrict__ current_l = image.get(x0, y, 0);
        const T * __restrict__ current_a = image.get(x0, y, 1);
        const T * __restrict__ current_b = image.get(x0, y, 2);
        
        for (int x = x0; x < x1; ++x)
        {
            TMODE distance, distance_lab, dx, dy, dl, da, db;
            
            dx = ((TMODE)x - centroids[id].getX());
            dy = ((TMODE)y - centroids[id].getY());
            dl = ((TMODE)*current_l - centroids[id].getL());
            da = ((TMODE)*current_a - centroids[id].getA());
            db = ((TMODE)*current_b - centroids[id].getB());
            distance_lab = (dl * dl + da * da + db * db);
            distance = (TMODE)((double)distance_lab / (double)maxlab + invwt * (double)(dx * dx + dy * dy));
            
            if (maxlab_update < distance_lab) maxlab_update = distance_lab;
            if (distance < *distance_ptr)
            {
                *distance_ptr = distance;
                *selected_ptr = id;
            }
            ++distance_ptr;
            ++selected_ptr;
            ++current_l;
            ++current_a;
            ++current_b;
        }
    }
    
    template <class T, class TMODE>
    void Segmentation::SlicRecalculateCentroids(const Image<T> &image, const Image<TMODE> &cluster_distance, const Image<unsigned int> &cluster_selected, VectorDense<SlicCentroid<TMODE> > &centroids, VectorDense<unsigned int> &cluster_size)
    {
        const unsigned int width = image.getWidth();
        const unsigned int height = image.getHeight();
        
        cluster_size.setValue(0);
        for (unsigned int id = 0; id < centroids.size(); ++id)
            centroids[id].set(0, 0, 0, 0, 0);
        for (unsigned int y = 0; y < height; ++y)
        {
            const T * __restrict__ l_ptr = image.get(y, 0);
            const T * __restrict__ a_ptr = image.get(y, 1);
            const T * __restrict__ b_ptr = image.get(y, 2);
            const TMODE * __restrict__ distance_ptr = cluster_distance.get(y, 0);
            const unsigned int * __restrict__ selected_ptr = cluster_selected.get(y, 0);
            
            for (unsigned int x = 0; x < width; ++x)
            {
                centroids[*selected_ptr].getX() += (TMODE)x;
                centroids[*selected_ptr].getY() += (TMODE)y;
                centroids[*selected_ptr].getL() += (TMODE)*l_ptr;
                centroids[*selected_ptr].getA() += (TMODE)*a_ptr;
                centroids[*selected_ptr].getB() += (TMODE)*b_ptr;
                ++cluster_size[*selected_ptr];
                ++l_ptr;
                ++a_ptr;
                ++b_ptr;
                ++distance_ptr;
                ++selected_ptr;
            }
        }
        for (unsigned int id = 0; id < centroids.size(); ++id)
        {
            if (cluster_size[id] == 0) cluster_size[id] = 1;
            centroids[id].getX() /= (TMODE)cluster_size[id];
            centroids[id].getY() /= (TMODE)cluster_size[id];
            centroids[id].getL() /= (TMODE)cluster_size[id];
            centroids[id].getA() /= (TMODE)cluster_size[id];
            centroids[id].getB() /= (TMODE)cluster_size[id];
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SEEDS SEGMENTATION IMPLEMENTATION    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T>
    Segmentation::Seeds<T>::Seeds(void) :
        m_step_x(0),
        m_step_y(0),
        m_number_of_seeds_x(0),
        m_number_of_seeds_y(0),
        m_number_of_seeds(0),
        m_number_of_partitions(0),
        m_parent_label(0),
        m_number_of_levels(0),
        m_mean(0),
        m_histogram(0),
        m_block_size(0),
        m_number_of_bins(0),
        m_histogram_size(0),
        m_preprocess_identifier(CONVERT_TO_LAB),
        m_double_steps(true)
    {
    }
    
    template <class T>
    Segmentation::Seeds<T>::Seeds(unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps) :
        m_step_x(step_x),
        m_step_y(step_y),
        m_number_of_seeds_x((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_seeds_y((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_seeds((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_partitions((number_of_levels > 0)?new unsigned int * [number_of_levels]:0),
        m_parent_label((number_of_levels > 0)?new unsigned int * [number_of_levels]:0),
        m_number_of_levels(number_of_levels),
        m_mean(0),
        m_histogram(0),
        m_block_size(0),
        m_number_of_bins(number_of_bins),
        m_histogram_size(0),
        m_preprocess_identifier(preprocess),
        m_double_steps(double_steps)
    {
        for (unsigned int i = 0; i < number_of_levels; ++i)
        {
            m_number_of_seeds_x[i] = m_number_of_seeds_y[i] = m_number_of_seeds[i] = 0;
            m_number_of_partitions[i] = 0;
        }
        for (unsigned int i = 0; i < number_of_levels; ++i)
            m_parent_label[i] = 0;
    }
    
    template <class T>
    Segmentation::Seeds<T>::Seeds(unsigned int width, unsigned int height, unsigned int number_of_channels, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps) :
        m_step_x(step_x),
        m_step_y(step_y),
        m_number_of_seeds_x((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_seeds_y((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_seeds((number_of_levels > 0)?new unsigned int[number_of_levels]:0),
        m_number_of_partitions((number_of_levels > 0)?new unsigned int * [number_of_levels]:0),
        m_parent_label((number_of_levels > 0)?new unsigned int * [number_of_levels]:0),
        m_number_of_levels(number_of_levels),
        m_bins(width, height, 1),
        m_work_image(width, height, number_of_channels),
        m_mean(0),
        m_labels(width, height, number_of_levels),
        m_histogram(0),
        m_block_size(0),
        m_number_of_bins(number_of_bins),
        m_histogram_size(0),
        m_preprocess_identifier(preprocess),
        m_double_steps(double_steps)
    {
        if ((number_of_channels > 0) && (number_of_bins > 0))
        {
            m_histogram_size = 1;
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                m_histogram_size *= number_of_bins;
        }
        createStructures(step_x, step_y, number_of_channels, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_seeds, m_number_of_partitions, m_parent_label, m_labels, m_histogram, m_block_size, m_histogram_size, m_mean);
    }
    
    template <class T>
    Segmentation::Seeds<T>::Seeds(const Seeds<T> &other) :
        m_step_x(other.m_step_x),
        m_step_y(other.m_step_y),
        m_number_of_seeds_x((other.m_number_of_levels > 0)?new unsigned int[other.m_number_of_levels]:0),
        m_number_of_seeds_y((other.m_number_of_levels > 0)?new unsigned int[other.m_number_of_levels]:0),
        m_number_of_seeds((other.m_number_of_levels > 0)?new unsigned int[other.m_number_of_levels]:0),
        m_number_of_partitions((other.m_number_of_levels > 0)?new unsigned int * [other.m_number_of_levels]:0),
        m_parent_label((other.m_number_of_levels > 0)?new unsigned int * [other.m_number_of_levels]:0),
        m_number_of_levels(other.m_number_of_levels),
        m_bins(other.m_bins),
        m_work_image(other.m_work_image),
        m_mean(0),
        m_labels(other.m_labels),
        m_histogram((other.m_histogram != 0)?new unsigned int * * [other.m_number_of_levels]:0),
        m_block_size((other.m_block_size != 0)?new unsigned int * [other.m_number_of_levels]:0),
        m_number_of_bins(other.m_number_of_bins),
        m_histogram_size(other.m_histogram_size),
        m_preprocess_identifier(other.m_preprocess_identifier),
        m_double_steps(other.m_double_steps)
    {
        for (unsigned int i = 0; i < other.m_number_of_levels; ++i)
        {
            m_number_of_seeds_x[i] = other.m_number_of_seeds_x[i];
            m_number_of_seeds_y[i] = other.m_number_of_seeds_y[i];
            m_number_of_seeds[i] = other.m_number_of_seeds[i];
            m_number_of_partitions[i] = new unsigned int[other.m_number_of_seeds[i]];
            for (unsigned int j = 0; j < other.m_number_of_seeds[i]; ++j)
                m_number_of_partitions[i][j] = other.m_number_of_partitions[i][j];
        }
        for (unsigned int i = 0; i < other.m_number_of_levels; ++i)
        {
            m_parent_label[i] = new unsigned int[other.m_number_of_seeds[i]];
            for (unsigned int j = 0; j < other.m_number_of_seeds[i]; ++j)
                m_parent_label[i][j] = other.m_parent_label[i][j];
        }
        if (other.m_histogram != 0)
        {
            for (unsigned int level = 0; level < other.m_number_of_levels; ++level)
            {
                m_histogram[level] = new unsigned int * [other.m_number_of_seeds[level]];
                for (unsigned int seed = 0; seed < other.m_number_of_seeds[level]; ++seed)
                {
                    m_histogram[level][seed] = new unsigned int[other.m_histogram_size];
                    for (unsigned int bin = 0; bin < other.m_histogram_size; ++bin)
                        m_histogram[level][seed][bin] = other.m_histogram[level][seed][bin];
                }
            }
        }
        if (other.m_block_size != 0)
        {
            for (unsigned int level = 0; level < other.m_number_of_levels; ++level)
            {
                m_block_size[level] = new unsigned int[other.m_number_of_seeds[level]];
                for (unsigned int seed = 0; seed < other.m_number_of_seeds[level]; ++seed)
                    m_block_size[level][seed] = other.m_block_size[level][seed];
            }
        }
        if (other.m_mean != 0)
        {
            const unsigned int top_number_of_seeds = other.m_number_of_seeds[other.m_number_of_levels - 1];
            
            m_mean = new typename META_GENERAL_TYPE<T>::TYPE * [other.m_work_image.getNumberOfChannels()];
            for (unsigned int channel = 0; channel < other.m_work_image.getNumberOfChannels(); ++channel)
            {
                m_mean[channel] = new typename META_GENERAL_TYPE<T>::TYPE[top_number_of_seeds];
                for (unsigned int seed = 0; seed < top_number_of_seeds; ++seed)
                    m_mean[channel][seed] = other.m_mean[channel][seed];
            }
        }
    }
    
    template <class T>
    Segmentation::Seeds<T>& Segmentation::Seeds<T>::operator=(const Seeds<T> &other)
    {
        if (this != &other)
        {
            // Free ...............................................................................
            if (m_histogram != 0)
            {
                for (unsigned int level = 0; level < m_number_of_levels; ++level)
                {
                    for (unsigned int seed = 0; seed < m_number_of_seeds[level]; ++seed)
                        delete [] m_histogram[level][seed];
                    delete [] m_histogram[level];
                }
                delete [] m_histogram;
            }
            if (m_block_size != 0)
            {
                for (unsigned int level = 0; level < m_number_of_levels; ++level)
                    delete [] m_block_size[level];
                delete [] m_block_size;
            }
            if (m_number_of_seeds_x != 0) delete [] m_number_of_seeds_x;
            if (m_number_of_seeds_y != 0) delete [] m_number_of_seeds_y;
            if (m_number_of_seeds != 0) delete [] m_number_of_seeds;
            if (m_number_of_partitions != 0)
            {
                for (unsigned int i = 0; i < m_number_of_levels; ++i)
                    if (m_number_of_partitions[i] != 0) delete [] m_number_of_partitions[i];
                delete [] m_number_of_partitions;
            }
            if (m_parent_label != 0)
            {
                for (unsigned int i = 0; i < m_number_of_levels; ++i)
                    if (m_parent_label[i] != 0) delete [] m_parent_label[i];
                delete [] m_parent_label;
            }
            if (m_mean != 0)
            {
                for (unsigned int channel = 0; channel < m_work_image.getNumberOfChannels(); ++channel)
                    delete [] m_mean[channel];
                delete [] m_mean;
            }
            
            // Copy variables .....................................................................
            m_step_x = other.m_step_x;
            m_step_y = other.m_step_y;
            m_number_of_levels = other.m_number_of_levels;
            m_number_of_bins = other.m_number_of_bins;
            m_histogram_size = other.m_histogram_size;
            m_preprocess_identifier = other.m_preprocess_identifier;
            m_double_steps = other.m_double_steps;
            
            // Copy structures ....................................................................
            if (other.m_number_of_levels > 0)
            {
                m_number_of_seeds_x = new unsigned int[other.m_number_of_levels];
                m_number_of_seeds_y = new unsigned int[other.m_number_of_levels];
                m_number_of_seeds = new unsigned int[other.m_number_of_levels];
                m_number_of_partitions = new unsigned int * [other.m_number_of_levels];
                for (unsigned int i = 0; i < other.m_number_of_levels; ++i)
                {
                    m_number_of_seeds_x[i] = other.m_number_of_seeds_x[i];
                    m_number_of_seeds_y[i] = other.m_number_of_seeds_y[i];
                    m_number_of_seeds[i] = other.m_number_of_seeds[i];
                    m_number_of_partitions[i] = new unsigned int[other.m_number_of_seeds[i]];
                    for (unsigned int j = 0; j < other.m_number_of_seeds[i]; ++j)
                        m_number_of_partitions[i][j] = other.m_number_of_partitions[i][j];
                }
            }
            else
            {
                m_number_of_seeds_x = m_number_of_seeds_y = m_number_of_seeds = 0;
                m_number_of_partitions = 0;
            }
            if (other.m_number_of_levels > 0)
            {
                m_parent_label = new unsigned int * [other.m_number_of_levels];
                for (unsigned int i = 0; i < other.m_number_of_levels; ++i)
                {
                    m_parent_label[i] = new unsigned int[other.m_number_of_seeds[i]];
                    for (unsigned int j = 0; j < other.m_number_of_seeds[i]; ++j)
                        m_parent_label[i][j] = other.m_parent_label[i][j];
                }
            }
            else m_parent_label = 0;
            if (other.m_histogram != 0)
            {
                m_histogram = new unsigned int * * [other.m_number_of_levels];
                for (unsigned int level = 0; level < other.m_number_of_levels; ++level)
                {
                    m_histogram[level] = new unsigned int * [other.m_number_of_seeds[level]];
                    for (unsigned int seed = 0; seed < other.m_number_of_seeds[level]; ++seed)
                    {
                        m_histogram[level][seed] = new unsigned int[other.m_histogram_size];
                        for (unsigned int bin = 0; bin < other.m_histogram_size; ++bin)
                            m_histogram[level][seed][bin] = other.m_histogram[level][seed][bin];
                    }
                }
            }
            else m_histogram = 0;
            if (other.m_block_size != 0)
            {
                m_block_size = new unsigned int[other.m_number_of_levels];
                for (unsigned int level = 0; level < other.m_number_of_levels; ++level)
                {
                    m_block_size[level] = new unsigned int[other.m_number_of_seeds[level]];
                    for (unsigned int seed = 0; seed < other.m_number_of_seeds[level]; ++seed)
                        m_block_size[level][seed] = other.m_block_size[level][seed];
                }
            }
            else m_block_size = 0;
            if (other.m_mean != 0)
            {
                const unsigned int top_number_of_seeds = other.m_number_of_seeds[other.m_number_of_levels - 1];
                
                m_mean = new typename META_GENERAL_TYPE<T>::TYPE * [other.m_work_image.getNumberOfChannels()];
                for (unsigned int channel = 0; channel < other.m_work_image.getNumberOfChannels(); ++channel)
                {
                    m_mean[channel] = new typename META_GENERAL_TYPE<T>::TYPE[top_number_of_seeds];
                    for (unsigned int seed = 0; seed < top_number_of_seeds; ++seed)
                        m_mean[channel][seed] = other.m_mean[channel][seed];
                }
            }
            else m_mean = 0;
            
            // Copy images ........................................................................
            m_bins = other.m_bins;
            m_work_image = other.m_work_image;
            m_labels = other.m_labels;
        }
        return *this;
    }
    
    template <class T>
    void Segmentation::Seeds<T>::freeBase(void)
    {
        if (m_number_of_seeds_x != 0) { delete [] m_number_of_seeds_x; m_number_of_seeds_x = 0; }
        if (m_number_of_seeds_y != 0) { delete [] m_number_of_seeds_y; m_number_of_seeds_y = 0; }
        if (m_number_of_seeds != 0) { delete [] m_number_of_seeds; m_number_of_seeds = 0; }
        if (m_number_of_partitions != 0)
        {
            for (unsigned int i = 0; i < m_number_of_levels; ++i)
                if (m_number_of_partitions[i] != 0) delete [] m_number_of_partitions[i];
            delete [] m_number_of_partitions;
            m_number_of_partitions = 0;
        }
        if (m_parent_label != 0)
        {
            for (unsigned int i = 0; i < m_number_of_levels; ++i)
                if (m_parent_label[i] != 0) delete [] m_parent_label[i];
            delete [] m_parent_label;
            m_parent_label = 0;
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::freeImage(void)
    {
        if (m_histogram != 0)
        {
            for (unsigned int level = 0; level < m_number_of_levels; ++level)
            {
                for (unsigned int seed = 0; seed < m_number_of_seeds[level]; ++seed)
                    delete [] m_histogram[level][seed];
                delete [] m_histogram[level];
            }
            delete [] m_histogram;
            m_histogram = 0;
        }
        if (m_block_size != 0)
        {
            for (unsigned int level = 0; level < m_number_of_levels; ++level)
                delete [] m_block_size[level];
            delete [] m_block_size;
            m_block_size = 0;
        }
        if (m_mean != 0)
        {
            for (unsigned int channel = 0; channel < m_work_image.getNumberOfChannels(); ++channel)
                delete [] m_mean[channel];
            delete [] m_mean;
            m_mean = 0;
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::initialize(unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps)
    {
        // Free ...................................................................................
        freeImage();
        freeBase();
        m_histogram_size = 0;
        
        // Initialize variables ...................................................................
        m_step_x = step_x;
        m_step_y = step_y;
        m_number_of_levels = number_of_levels;
        m_number_of_bins = number_of_bins;
        m_preprocess_identifier = preprocess;
        m_double_steps = double_steps;
        
        // Create structures ......................................................................
        if (number_of_levels > 0)
        {
            m_number_of_seeds_x = new unsigned int[number_of_levels];
            m_number_of_seeds_y = new unsigned int[number_of_levels];
            m_number_of_seeds = new unsigned int[number_of_levels];
            m_number_of_partitions = new unsigned int * [number_of_levels];
            m_parent_label = new unsigned int * [number_of_levels];
            for (unsigned int i = 0; i < number_of_levels; ++i)
                m_parent_label[i] = m_number_of_partitions[i] = 0;
        }
        else
        {
            m_number_of_seeds_x = m_number_of_seeds_y = m_number_of_seeds = 0;
            m_number_of_partitions = 0;
            m_parent_label = 0;
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels)
    {
        // Free ...................................................................................
        if (m_parent_label != 0)
            for (unsigned int l = 0; l < m_number_of_levels; ++l)
                if (m_parent_label[l] != 0) delete [] m_parent_label[l];
        if (m_number_of_partitions != 0)
            for (unsigned int l = 0; l < m_number_of_levels; ++l)
                if (m_number_of_partitions[l] != 0) delete [] m_number_of_partitions[l];
        freeImage();
        
        // Initialize variables ...................................................................
        if ((number_of_channels > 0) && (m_number_of_bins > 0))
        {
            m_histogram_size = 1;
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                m_histogram_size *= m_number_of_bins;
        }
        else m_histogram_size = 0;
        
        // Initialize inner-images ................................................................
        m_bins.setGeometry(width, height, 1);
        m_work_image.setGeometry(width, height, number_of_channels);
        m_labels.setGeometry(width, height, m_number_of_levels);
        
        // Initialize the structure values ........................................................
        createStructures(m_step_x, m_step_y, number_of_channels, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_seeds, m_number_of_partitions, m_parent_label, m_labels, m_histogram, m_block_size, m_histogram_size, m_mean);
    }
    
    template <class T>
    void Segmentation::Seeds<T>::initialize(unsigned int width, unsigned int height, unsigned int number_of_channels, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess, bool double_steps)
    {
        initialize(step_x, step_y, number_of_levels, number_of_bins, preprocess, double_steps);
        setGeometry(width, height, number_of_channels);
    }
    
    template <class T>
    template <class TSOURCE>
    unsigned int Segmentation::Seeds<T>::process(const Image<TSOURCE> &source, Image<unsigned int> &segmentation, unsigned int number_of_threads)
    {
        if ((segmentation.getWidth() != m_work_image.getWidth()) ||
            (segmentation.getHeight() != m_work_image.getHeight()) ||
            (segmentation.getNumberOfChannels() != 1)) segmentation.setGeometry(m_work_image.getWidth(), m_work_image.getHeight(), 1);
        // Build the color histograms .............................................................
        initStructures(m_step_x, m_step_y, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_seeds, m_number_of_partitions, m_parent_label, m_labels);
        convertColor(source, m_preprocess_identifier, m_number_of_bins, m_work_image, m_bins, number_of_threads);
        computeHistogram(m_histogram_size, m_number_of_levels, m_number_of_seeds, m_parent_label, m_bins, m_labels, m_histogram, m_block_size, m_number_of_partitions);
        
        // Iterate at blocks level ................................................................
        if (m_double_steps)
        {
            for (int current_level = (int)m_number_of_levels - 2; current_level >= 0;)
            {
                updateBlocks(current_level, 0.1f, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_partitions, m_parent_label, m_number_of_levels, m_histogram, m_block_size, m_histogram_size, m_labels, number_of_threads);
                updateBlocks(current_level, 0.0f, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_partitions, m_parent_label, m_number_of_levels, m_histogram, m_block_size, m_histogram_size, m_labels, number_of_threads);
                current_level = updateLevelIndex(current_level, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_partitions, m_parent_label, m_number_of_levels, number_of_threads);
            }
        }
        else
        {
            for (int current_level = (int)m_number_of_levels - 2; current_level >= 0;)
            {
                updateBlocks(current_level, 0.0f, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_partitions, m_parent_label, m_number_of_levels, m_histogram, m_block_size, m_histogram_size, m_labels, number_of_threads);
                current_level = updateLevelIndex(current_level, m_number_of_seeds_x, m_number_of_seeds_y, m_number_of_partitions, m_parent_label, m_number_of_levels, number_of_threads);
            }
        }
        // Iterate at pixel level .................................................................
        computeMean(m_work_image, m_labels, m_mean, m_number_of_seeds[m_number_of_levels - 1], number_of_threads);
        updatePixels( true, m_histogram, m_block_size, m_bins, m_labels, m_work_image, m_mean);
        updatePixels(false, m_histogram, m_block_size, m_bins, m_labels, m_work_image, m_mean);
        updatePixels( true, m_histogram, m_block_size, m_bins, m_labels, m_work_image, m_mean);
        updatePixels(false, m_histogram, m_block_size, m_bins, m_labels, m_work_image, m_mean);
        
        // Generate the segmentation image.
        return generateSegmentationImage(m_labels, segmentation, number_of_threads);
    }
    
    template <class T>
    template <class TSOURCE>
    unsigned int Segmentation::Seeds<T>::process(const Image<TSOURCE> &source, unsigned int step_x, unsigned int step_y, unsigned int number_of_levels, unsigned int number_of_bins, SEGMENTATION_PREPROCESS preprocess_identifier, bool double_steps, Image<unsigned int> &segmentation, unsigned int number_of_threads)
    {
        // Constant declarations ..................................................................
        const unsigned int width = source.getWidth();
        const unsigned int height = source.getHeight();
        const unsigned int number_of_channels = source.getNumberOfChannels();
        // Variable declarations ..................................................................
        unsigned int * number_of_seeds_x;
        unsigned int * number_of_seeds_y;
        unsigned int * number_of_seeds;
        unsigned int * * number_of_partitions;
        unsigned int * * parent_label;
        Image<unsigned int> bins;
        Image<T> work_image;
        typename META_GENERAL_TYPE<T>::TYPE * * mean;
        Image<unsigned int> labels;
        unsigned int * * * histogram;
        unsigned int * * block_size;
        unsigned int histogram_size;
        unsigned int number_of_super_pixels;
        
        // Initialize the structures ..............................................................
        segmentation.setGeometry(width, height, 1);
        if ((number_of_channels > 0) && (number_of_bins > 0))
        {
            histogram_size = 1;
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                histogram_size *= number_of_bins;
        }
        else return 0;
        
        // Initialize inner-images ................................................................
        bins.setGeometry(width, height, 1);
        work_image.setGeometry(width, height, number_of_channels);
        labels.setGeometry(width, height, number_of_levels);
        
        // Create structures ......................................................................
        if (number_of_levels > 0)
        {
            number_of_seeds_x = new unsigned int[number_of_levels];
            number_of_seeds_y = new unsigned int[number_of_levels];
            number_of_seeds = new unsigned int[number_of_levels];
            number_of_partitions = new unsigned int * [number_of_levels];
            for (unsigned int i = 0; i < number_of_levels; ++i)
                number_of_partitions[i] = 0;
            parent_label = new unsigned int * [number_of_levels];
        }
        else return 0;
        
        // Initialize the structure values ........................................................
        createStructures(step_x, step_y, number_of_channels, number_of_seeds_x, number_of_seeds_y, number_of_seeds, number_of_partitions, parent_label, labels, histogram, block_size, histogram_size, mean);
        
        // Build the color histograms .............................................................
        initStructures(step_x, step_y, number_of_seeds_x, number_of_seeds_y, number_of_seeds, number_of_partitions, parent_label, labels);
        convertColor(source, preprocess_identifier, number_of_bins, work_image, bins, number_of_threads);
        computeHistogram(histogram_size, number_of_levels, number_of_seeds, parent_label, bins, labels, histogram, block_size, number_of_partitions);
        
        // Iterate at blocks level ................................................................
        if (double_steps)
        {
            for (int current_level = (int)number_of_levels - 2; current_level >= 0;)
            {
                updateBlocks(current_level, 0.1f, number_of_seeds_x, number_of_seeds_y, number_of_partitions, parent_label, number_of_levels, histogram, block_size, histogram_size, labels, number_of_threads);
                updateBlocks(current_level, 0.0f, number_of_seeds_x, number_of_seeds_y, number_of_partitions, parent_label, number_of_levels, histogram, block_size, histogram_size, labels, number_of_threads);
                current_level = updateLevelIndex(current_level, number_of_seeds_x, number_of_seeds_y, number_of_partitions, parent_label, number_of_levels, number_of_threads);
            }
        }
        else
        {
            for (int current_level = (int)number_of_levels - 2; current_level >= 0;)
            {
                updateBlocks(current_level, 0.0f, number_of_seeds_x, number_of_seeds_y, number_of_partitions, parent_label, number_of_levels, histogram, block_size, histogram_size, labels, number_of_threads);
                current_level = updateLevelIndex(current_level, number_of_seeds_x, number_of_seeds_y, number_of_partitions, parent_label, number_of_levels, number_of_threads);
            }
        }
        // Iterate at pixel level .................................................................
        computeMean(work_image, labels, mean, number_of_seeds[number_of_levels - 1], number_of_threads);
        updatePixels( true, histogram, block_size, bins, labels, work_image, mean);
        updatePixels(false, histogram, block_size, bins, labels, work_image, mean);
        updatePixels( true, histogram, block_size, bins, labels, work_image, mean);
        updatePixels(false, histogram, block_size, bins, labels, work_image, mean);
        
        number_of_super_pixels = generateSegmentationImage(labels, segmentation, number_of_threads);
        
        // Free memory ............................................................................
        for (unsigned int level = 0; level < number_of_levels; ++level)
        {
            for (unsigned int label = 0; label < number_of_seeds[level]; ++label)
                delete [] histogram[level][label];
            delete [] histogram[level];
            delete [] block_size[level];
        }
        delete [] histogram;
        delete [] block_size;
        delete [] number_of_seeds_x;
        delete [] number_of_seeds_y;
        delete [] number_of_seeds;
        for (unsigned int i = 0; i < number_of_levels; ++i)
        {
            delete [] number_of_partitions[i];
            delete [] parent_label[i];
        }
        delete [] number_of_partitions;
        delete [] parent_label;
        for (unsigned int i = 0; i < number_of_channels; ++i)
            delete [] mean[i];
        delete [] mean;
        
        return number_of_super_pixels;
    }
    
    template <class T>
    void Segmentation::Seeds<T>::createStructures(unsigned int step_x, unsigned int step_y, unsigned int number_of_channels, unsigned int * number_of_seeds_x, unsigned int * number_of_seeds_y, unsigned int * number_of_seeds, unsigned int * * number_of_partitions, unsigned int * * parent_label, Image<unsigned int> &labels, unsigned int * * * &histogram, unsigned int * * &block_size, unsigned int histogram_size, typename META_GENERAL_TYPE<T>::TYPE * * &mean)
    {
        const unsigned int width = labels.getWidth();
        const unsigned int height = labels.getHeight();
        const unsigned int number_of_levels = labels.getNumberOfChannels();
        
        // Initialize the structures at the base level of the pyramid.
        number_of_seeds_x[0] = srvMax((unsigned int)1, width / step_x);
        number_of_seeds_y[0] = srvMax((unsigned int)1, height / step_y);
        number_of_seeds[0] = number_of_seeds_x[0] * number_of_seeds_y[0];
        number_of_partitions[0] = new unsigned int[number_of_seeds[0]];
        parent_label[0] = new unsigned int[number_of_seeds[0]];
        
        // Initialize the structures for the remaining levels of the pyramid.
        for (unsigned int level = 1; level < number_of_levels; ++level)
        {
            step_x *= 2;
            step_y *= 2;
            number_of_seeds_x[level] = srvMax((unsigned int)1, width / step_x);
            number_of_seeds_y[level] = srvMax((unsigned int)1, height / step_y);
            number_of_seeds[level] = number_of_seeds_x[level] * number_of_seeds_y[level];
            number_of_partitions[level] = new unsigned int[number_of_seeds[level]];
            parent_label[level] = new unsigned int[number_of_seeds[level]];
        }
        
        // Initialize the color histograms of each region.
        histogram = new unsigned int * * [number_of_levels];
        block_size = new unsigned int * [number_of_levels];
        for (unsigned int level = 0; level < number_of_levels; ++level)
        {
            histogram[level] = new unsigned int * [number_of_seeds[level]];
            block_size[level] = new unsigned int[number_of_seeds[level]];
            for (unsigned int label = 0; label < number_of_seeds[level]; ++label)
                histogram[level][label] = new unsigned int[histogram_size];
        }
        
        mean = new typename META_GENERAL_TYPE<T>::TYPE * [number_of_channels];
        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            mean[channel] = new typename META_GENERAL_TYPE<T>::TYPE[number_of_seeds[number_of_levels - 1]];
    }
    
    template <class T>
    void Segmentation::Seeds<T>::initStructures(unsigned int step_x, unsigned int step_y, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, const unsigned int * number_of_seeds, unsigned int * * number_of_partitions, unsigned int * * parent_label, Image<unsigned int> &labels)
    {
        const unsigned int width = labels.getWidth();
        const unsigned int height = labels.getHeight();
        const unsigned int number_of_levels = labels.getNumberOfChannels();
        
        // Initialize the structures at the base level of the pyramid.
        for (unsigned int i = 0; i < number_of_seeds[0]; ++i) number_of_partitions[0][i] = 1;
        for (unsigned int y = 0; y < height; ++y)
        {
            unsigned int * __restrict__ labels_ptr = labels.get(y, 0);
            for (unsigned int x = 0; x < width; ++x, ++labels_ptr)
            {
                unsigned int label_x, label_y;
                
                label_x = srvMin(number_of_seeds_x[0] - 1, x / step_x);
                label_y = srvMin(number_of_seeds_y[0] - 1, y / step_y);
                *labels_ptr = label_y * number_of_seeds_x[0] + label_x;
            }
        }
        
        // Initialize the structures for the remaining levels of the pyramid.
        for (unsigned int level = 1; level < number_of_levels; ++level)
        {
            step_x *= 2;
            step_y *= 2;
            for (unsigned int i = 0; i < number_of_seeds[level]; ++i) number_of_partitions[level][i] = 0;
            for (unsigned int y = 0; y < height; ++y)
            {
                unsigned int * __restrict__ current_labels_ptr = labels.get(y, level);
                unsigned int * __restrict__ previous_labels_ptr = labels.get(y, level - 1);
                
                for (unsigned int x = 0; x < width; ++x, ++current_labels_ptr, ++previous_labels_ptr)
                {
                    unsigned int label_x, label_y;
                    
                    label_x = srvMin(number_of_seeds_x[level] - 1, x / step_x);
                    label_y = srvMin(number_of_seeds_y[level] - 1, y / step_y);
                    *current_labels_ptr = label_y * number_of_seeds_x[level] + label_x;
                    parent_label[level - 1][*previous_labels_ptr] = *current_labels_ptr;
                }
            }
        }
    }
    
    template <class T>
    template <class TSOURCE>
    void Segmentation::Seeds<T>::convertColor(const Image<TSOURCE> &source, SEGMENTATION_PREPROCESS convert_identifier, unsigned int number_of_bins, Image<T> &work_image, Image<unsigned int> &bins, unsigned int number_of_threads)
    {
        const unsigned int width = work_image.getWidth();
        const unsigned int height = work_image.getHeight();
        const unsigned int number_of_channels = work_image.getNumberOfChannels();
        
        if ((source.getWidth() != width) || (source.getHeight() != height))
            throw Exception("Source image is expected to have a %dx%d geometry but it has %dx%d instead.", width, height, source.getWidth(), source.getHeight());
        if (convert_identifier == CONVERT_TO_LAB)
        {
            const unsigned int number_of_bins2 = number_of_bins * number_of_bins;
            const unsigned int sampled_width = width / 5;
            const unsigned int sampled_height = height / 5;
            const unsigned int sampled_n = sampled_width * sampled_height;
            VectorDense<T> cutoff_l(number_of_bins), cutoff_a(number_of_bins), cutoff_b(number_of_bins);
            Image<T> sampled_rgb_image(sampled_width, sampled_height, 3);
            Image<T> sampled_lab_image(sampled_width, sampled_height, 3);
            
            if (source.getNumberOfChannels() != 3)
                throw Exception("Source image must have 3 channels to be converted to the L*a*b color space.");
            
            // 1) Sample down the original image to a fifth of its original size.
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int yd = omp_get_thread_num(); yd < sampled_height; yd += number_of_threads)
                {
                    const TSOURCE * __restrict__ src_r = source.get(yd * 5, 0);
                    const TSOURCE * __restrict__ src_g = source.get(yd * 5, 1);
                    const TSOURCE * __restrict__ src_b = source.get(yd * 5, 2);
                    T * __restrict__ dst_r = sampled_rgb_image.get(yd, 0);
                    T * __restrict__ dst_g = sampled_rgb_image.get(yd, 1);
                    T * __restrict__ dst_b = sampled_rgb_image.get(yd, 2);
                    for (unsigned int x = 0; x < sampled_width; ++x, src_r += 5, src_g += 5, src_b += 5, ++dst_r, ++dst_g, ++dst_b)
                    {
                        *dst_r = (T)*src_r;
                        *dst_g = (T)*src_g;
                        *dst_b = (T)*src_b;
                    }
                }
            }
            // 2) Convert the sampled image from RGB to CIELAB.
            ImageRGB2CIELab(sampled_rgb_image, sampled_lab_image, number_of_threads);
            // 3) Calculate the cutoff values.
            for (unsigned int i = 0; i < number_of_bins - 1; ++i)
            {
                unsigned int offset;
                
                offset = (unsigned int)floor((float)((i + 1) * sampled_width * sampled_height) / (float)number_of_bins);
                std::nth_element(sampled_lab_image.get(0), sampled_lab_image.get(0) + offset, sampled_lab_image.get(0) + sampled_n);
                std::nth_element(sampled_lab_image.get(1), sampled_lab_image.get(1) + offset, sampled_lab_image.get(1) + sampled_n);
                std::nth_element(sampled_lab_image.get(2), sampled_lab_image.get(2) + offset, sampled_lab_image.get(2) + sampled_n);
                cutoff_l[i] = *(sampled_lab_image.get(0) + offset);
                cutoff_a[i] = *(sampled_lab_image.get(1) + offset);
                cutoff_b[i] = *(sampled_lab_image.get(2) + offset);
            }
            cutoff_l[number_of_bins - 1] = 300;
            cutoff_a[number_of_bins - 1] = 300;
            cutoff_b[number_of_bins - 1] = 300;
            
            // 4) Convert the source image to the CIELAB color space.
            ImageRGB2CIELab(source, work_image, number_of_threads);
            
            // 5) Calculate the histogram bin of each CIELAB color value.
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    unsigned int * __restrict__ bin_ptr = bins.get(y, 0);
                    const T * __restrict__ l_ptr = work_image.get(y, 0);
                    const T * __restrict__ a_ptr = work_image.get(y, 1);
                    const T * __restrict__ b_ptr = work_image.get(y, 2);
                    for (unsigned int x = 0; x < width; ++x, ++l_ptr, ++a_ptr, ++b_ptr, ++bin_ptr)
                    {
                        int bin_l, bin_a, bin_b;
                        
                        for (bin_l = 0; *l_ptr > cutoff_l[bin_l]; ++bin_l);
                        for (bin_a = 0; *a_ptr > cutoff_a[bin_a]; ++bin_a);
                        for (bin_b = 0; *b_ptr > cutoff_b[bin_b]; ++bin_b);
                        *bin_ptr = bin_l + bin_a * number_of_bins + bin_b * number_of_bins2;
                    }
                }
            }
        }
        else if (convert_identifier == CONVERT_TO_HSV)
        {
            const unsigned int number_of_bins2 = number_of_bins * number_of_bins;
            if (source.getNumberOfChannels() != 3)
                throw Exception("Source image must have 3 channels to be converted to the HSV color space.");
            // 1) Convert the image to the HSV color space.
            ImageRGB2HSV(source, work_image, number_of_threads);
            // 2) Calculate the histogram bin of each HSV color value.
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    unsigned int * __restrict__ bin_ptr = bins.get(y, 0);
                    const T * __restrict__ h_ptr = work_image.get(y, 0);
                    const T * __restrict__ s_ptr = work_image.get(y, 1);
                    const T * __restrict__ v_ptr = work_image.get(y, 2);
                    for (unsigned int x = 0; x < width; ++x, ++h_ptr, ++s_ptr, ++v_ptr, ++bin_ptr)
                    {
                        int bin_h, bin_s, bin_v;
                        
                        bin_h = (int)((double)number_of_bins * (double)*h_ptr / 256.0);
                        bin_s = (int)((double)number_of_bins * (double)*s_ptr / 256.0);
                        bin_v = (int)((double)number_of_bins * (double)*v_ptr / 256.0);
                        *bin_ptr = bin_h + bin_s * number_of_bins + bin_v * number_of_bins2;
                    }
                }
            }
        }
        else
        {
            if (source.getNumberOfChannels() != number_of_channels)
                throw Exception("Source image expected to have '%d' channel but it has '%d' channels instead.", number_of_channels, source.getNumberOfChannels());
            // Copy the source image to the work image and calculate the bin histograms.
            #pragma omp parallel num_threads(number_of_threads)
            {
                VectorDense<const TSOURCE *> src_ptrs(number_of_channels);
                VectorDense<T *> dst_ptrs(number_of_channels);
                
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    unsigned int * __restrict__ bin_ptr = bins.get(y, 0);
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        src_ptrs[c] = source.get(y, c);
                        dst_ptrs[c] = work_image.get(y, c);
                    }
                    for (unsigned int x = 0; x < width; ++x, ++bin_ptr)
                    {
                        unsigned int current_bin;
                        
                        current_bin = 0;
                        for (unsigned int c = 0, factor = 1; c < number_of_channels; ++c, factor *= number_of_bins)
                        {
                            T current_value;
                            current_value = *dst_ptrs[c] = (T)*src_ptrs[c];
                            current_bin += factor * ((unsigned int)((double)number_of_bins * (double)current_value / 256.0));
                            ++dst_ptrs[c];
                            ++src_ptrs[c];
                        }
                        *bin_ptr = current_bin;
                    }
                }
            }
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::computeHistogram(unsigned int histogram_size, unsigned int number_of_levels, const unsigned int * number_of_seeds, unsigned int * * parent_label, Image<unsigned int> &bins, Image<unsigned int> &labels, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions)
    {
        const unsigned int width = bins.getWidth();
        const unsigned int height = bins.getHeight();
        // Initialize the histogram values
        for (unsigned int level = 0; level < number_of_levels; ++level)
        {
            for (unsigned int label = 0; label < number_of_seeds[level]; ++label)
            {
                for (unsigned int bin = 0; bin < histogram_size; ++bin)
                    histogram[level][label][bin] = 0;
                block_size[level][label] = 0;
            }
        }
        // Build the histogram of the first level by adding the pixels to the blocks.
        for (unsigned int y = 0; y < height; ++y)
        {
            const unsigned int * __restrict__ bin_ptr = bins.get(y, 0);
            const unsigned int * __restrict__ label_ptr = labels.get(y, 0);
            for (unsigned int x = 0; x < width; ++x, ++bin_ptr, ++label_ptr)
            {
                ++histogram[0][*label_ptr][*bin_ptr];
                ++block_size[0][*label_ptr];
            }
        }
        // Build the histograms of the upper levels by adding the histograms from the levels below.
        for (unsigned int level = 1; level < number_of_levels; ++level)
            for (unsigned int label = 0; label < number_of_seeds[level - 1]; ++label)
                addBlock(level, parent_label[level - 1][label], level - 1, label, histogram_size, histogram, block_size, number_of_partitions, parent_label);
    }
    
    template <class T>
    void Segmentation::Seeds<T>::addBlock(unsigned int level, unsigned int label, unsigned int sublevel, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label)
    {
        parent_label[sublevel][sublabel] = label;
        for (unsigned int bin = 0; bin < histogram_size; ++bin)
            histogram[level][label][bin] += histogram[sublevel][sublabel][bin];
        block_size[level][label] += block_size[sublevel][sublabel];
        ++number_of_partitions[level][label];
    }
    
    template <class T>
    void Segmentation::Seeds<T>::removeBlock(unsigned int level, unsigned int label, unsigned int sublevel, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label)
    {
        parent_label[sublevel][sublabel] = -1;
        for (unsigned int bin = 0; bin < histogram_size; ++bin)
            histogram[level][label][bin] -= histogram[sublevel][sublabel][bin];
        block_size[level][label] -= block_size[sublevel][sublabel];
        --number_of_partitions[level][label];
    }
    
    template <class T>
    float Segmentation::Seeds<T>::intersection(unsigned int level_first, unsigned int label_first, unsigned int level_second, unsigned int label_second, unsigned int const * const * const * histogram, unsigned int const * const * block_size, unsigned int histogram_size)
    {
        float intersection_value;
        
        intersection_value = 0.0;
        for (unsigned int bin = 0; bin < histogram_size; ++bin)
            intersection_value += srvMin((float)histogram[level_first][label_first][bin] / (float)block_size[level_first][label_first],
                                   (float)histogram[level_second][label_second][bin] / (float)block_size[level_second][label_second]);
        return intersection_value;
    }
    
    template <class T>
    bool Segmentation::Seeds<T>::updateBlock(unsigned int top_level, unsigned int level, float ref_confidence, unsigned int label_a, unsigned int label_b, unsigned int sublabel, unsigned int histogram_size, unsigned int * * * histogram, unsigned int * * block_size, unsigned int * * number_of_partitions, unsigned int * * parent_label)
    {
        float intersection_a, intersection_b, confidence;
        bool done;
        
        // Run algorithm as usual.
        removeBlock(top_level, label_a, level, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
        intersection_a = intersection(top_level, label_a, level, sublabel, histogram, block_size, histogram_size);
        intersection_b = intersection(top_level, label_b, level, sublabel, histogram, block_size, histogram_size);
        confidence = srvAbs(intersection_a - intersection_b);
        // Add to the label with the highest intersection distance.
        if ((intersection_b > intersection_a) && (confidence > ref_confidence))
        {
            addBlock(top_level, label_b, level, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
            done = true;
        }
        else
        {
            addBlock(top_level, label_a, level, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
            done = false;
        }
        
        return done;
    }
    
    template <class T>
    void Segmentation::Seeds<T>::updateBlocks(int level, float ref_confidence, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, unsigned int * * number_of_partitions, unsigned int * * parent_label, unsigned int number_of_levels, unsigned int * * * histogram, unsigned int * * block_size, unsigned int histogram_size, Image<unsigned int> &labels, unsigned int number_of_threads)
    {
        const unsigned int boundary_value = -1;
        const unsigned int width = labels.getWidth();
        const unsigned int height = labels.getHeight();
        const unsigned int pwidth = number_of_seeds_x[level];
        const unsigned int pheight = number_of_seeds_y[level];
        const unsigned int top_level = number_of_levels - 1;
        
        // Horizontal bidirectional block updating.
        for (unsigned int y = 0; y < pheight; ++y)
        {
            for (unsigned int x = 0; x < pwidth - 1; ++x)
            {
                unsigned int label_a, label_b;
                
                label_a = parent_label[level][y * pwidth + x    ];
                label_b = parent_label[level][y * pwidth + x + 1];
                if (label_a != label_b)
                {
                    unsigned int a11, a12, a13, a14, a21, a22, a23, a24, a31, a32, a33, a34, sublabel;
                    bool done;
                    
                    sublabel = y * pwidth + x;
                    a11 = parentLabelValue(parent_label[level], (int)x - 1, (int)y - 1, pwidth, pheight, boundary_value);
                    a12 = parentLabelValue(parent_label[level], (int)x + 0, (int)y - 1, pwidth, pheight, boundary_value);
                    a13 = parentLabelValue(parent_label[level], (int)x + 1, (int)y - 1, pwidth, pheight, boundary_value);
                    a14 = parentLabelValue(parent_label[level], (int)x + 2, (int)y - 1, pwidth, pheight, boundary_value);
                    a21 = parentLabelValue(parent_label[level], (int)x - 1, (int)y + 0, pwidth, pheight, boundary_value);
                    a22 = parentLabelValue(parent_label[level], (int)x + 0, (int)y + 0, pwidth, pheight, boundary_value);
                    a23 = parentLabelValue(parent_label[level], (int)x + 1, (int)y + 0, pwidth, pheight, boundary_value);
                    a24 = parentLabelValue(parent_label[level], (int)x + 2, (int)y + 0, pwidth, pheight, boundary_value);
                    a31 = parentLabelValue(parent_label[level], (int)x - 1, (int)y + 1, pwidth, pheight, boundary_value);
                    a32 = parentLabelValue(parent_label[level], (int)x + 0, (int)y + 1, pwidth, pheight, boundary_value);
                    a33 = parentLabelValue(parent_label[level], (int)x + 1, (int)y + 1, pwidth, pheight, boundary_value);
                    a34 = parentLabelValue(parent_label[level], (int)x + 2, (int)y + 1, pwidth, pheight, boundary_value);
                    
                    done = false;
                    if (number_of_partitions[top_level][label_a] > 1)
                    {
                        if (number_of_partitions[top_level][label_a] <= 2) // Two partitions.
                            done = done || updateBlock(top_level, level, ref_confidence, label_a, label_b, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
                        // Three or more partitions.
                        else if (!checkSplit(a11, a12, a13, a21, a22, a23, a31, a32, a33, true, true))
                            done = done || updateBlock(top_level, level, ref_confidence, label_a, label_b, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
                    }
                    
                    // Try opposite direction.
                    if ((!done) && (number_of_partitions[top_level][label_b] > 1))
                    {
                        // Two partitions.
                        if (number_of_partitions[top_level][label_b] <= 2)
                        {
                            if (updateBlock(top_level, level, ref_confidence, label_b, label_a, sublabel + 1, histogram_size, histogram, block_size, number_of_partitions, parent_label))
                                ++x;
                        }
                        // Three or more partitions.
                        else if ((!checkSplit(a12, a13, a14, a22, a23, a24, a32, a33, a34, true, false)) &&
                                 (updateBlock(top_level, level, ref_confidence, label_b, label_a, sublabel + 1, histogram_size, histogram, block_size, number_of_partitions, parent_label)))
                                    ++x;
                    }
                }
            }
        }
        // Update the labels.
        if (number_of_threads <= 1)
        {
            for (unsigned int y = 0; y < height; ++y)
            {
                const unsigned int * __restrict__ current_ptr = labels.get(y, level);
                unsigned int * __restrict__ top_ptr = labels.get(y, top_level);
                
                for (unsigned int x = 0; x < width; ++x, ++current_ptr, ++top_ptr)
                    *top_ptr = parent_label[level][*current_ptr];
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    const unsigned int * __restrict__ current_ptr = labels.get(y, level);
                    unsigned int * __restrict__ top_ptr = labels.get(y, top_level);
                    
                    for (unsigned int x = 0; x < width; ++x, ++current_ptr, ++top_ptr)
                        *top_ptr = parent_label[level][*current_ptr];
                }
            }
        }
        // Vertical bidirectional block updating.
        for (unsigned int x = 0; x < pwidth; ++x)
        {
            for (unsigned int y = 0; y < pheight - 1; ++y)
            {
                unsigned int label_a, label_b;
                
                label_a = parent_label[level][ y      * pwidth + x];
                label_b = parent_label[level][(y + 1) * pwidth + x];
                if (label_a != label_b)
                {
                    unsigned int a11, a12, a13, a21, a22, a23, a31, a32, a33, a41, a42, a43, sublabel;
                    bool done;
                    
                    sublabel = y * pwidth + x;
                    a11 = parentLabelValue(parent_label[level], (int)x - 1, (int)y - 1, pwidth, pheight, boundary_value);
                    a12 = parentLabelValue(parent_label[level], (int)x + 0, (int)y - 1, pwidth, pheight, boundary_value);
                    a13 = parentLabelValue(parent_label[level], (int)x + 1, (int)y - 1, pwidth, pheight, boundary_value);
                    a21 = parentLabelValue(parent_label[level], (int)x - 1, (int)y + 0, pwidth, pheight, boundary_value);
                    a22 = parentLabelValue(parent_label[level], (int)x + 0, (int)y + 0, pwidth, pheight, boundary_value);
                    a23 = parentLabelValue(parent_label[level], (int)x + 1, (int)y + 0, pwidth, pheight, boundary_value);
                    a31 = parentLabelValue(parent_label[level], (int)x - 1, (int)y + 1, pwidth, pheight, boundary_value);
                    a32 = parentLabelValue(parent_label[level], (int)x + 0, (int)y + 1, pwidth, pheight, boundary_value);
                    a33 = parentLabelValue(parent_label[level], (int)x + 1, (int)y + 1, pwidth, pheight, boundary_value);
                    a41 = parentLabelValue(parent_label[level], (int)x - 1, (int)y + 2, pwidth, pheight, boundary_value);
                    a42 = parentLabelValue(parent_label[level], (int)x + 0, (int)y + 2, pwidth, pheight, boundary_value);
                    a43 = parentLabelValue(parent_label[level], (int)x + 1, (int)y + 2, pwidth, pheight, boundary_value);
                    
                    done = false;
                    if (number_of_partitions[top_level][label_a] > 1)
                    {
                        if (number_of_partitions[top_level][label_a] <= 2) // Two partitions.
                            done = done || updateBlock(top_level, level, ref_confidence, label_a, label_b, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
                        // Three or more partitions.
                        else if (!checkSplit(a11, a12, a13, a21, a22, a23, a31, a32, a33, false, true))
                            done = done || updateBlock(top_level, level, ref_confidence, label_a, label_b, sublabel, histogram_size, histogram, block_size, number_of_partitions, parent_label);
                    }
                    
                    // Try opposite direction.
                    if ((!done) && (number_of_partitions[top_level][label_b] > 1))
                    {
                        // Two partitions.
                        if (number_of_partitions[top_level][label_b] <= 2)
                        {
                            if (updateBlock(top_level, level, ref_confidence, label_b, label_a, sublabel + pwidth, histogram_size, histogram, block_size, number_of_partitions, parent_label))
                                ++y;
                        }
                        // Three or more partitions.
                        else if ((!checkSplit(a21, a22, a23, a31, a32, a33, a41, a42, a43, false, false)) &&
                                 (updateBlock(top_level, level, ref_confidence, label_b, label_a, sublabel + pwidth, histogram_size, histogram, block_size, number_of_partitions, parent_label)))
                                ++y;
                    }
                }
            }
        }
        // Update the labels.
        if (number_of_threads <= 1)
        {
            for (unsigned int y = 0; y < height; ++y)
            {
                const unsigned int * __restrict__ current_ptr = labels.get(y, level);
                unsigned int * __restrict__ top_ptr = labels.get(y, top_level);
                
                for (unsigned int x = 0; x < width; ++x, ++current_ptr, ++top_ptr)
                    *top_ptr = parent_label[level][*current_ptr];
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    const unsigned int * __restrict__ current_ptr = labels.get(y, level);
                    unsigned int * __restrict__ top_ptr = labels.get(y, top_level);
                    
                    for (unsigned int x = 0; x < width; ++x, ++current_ptr, ++top_ptr)
                        *top_ptr = parent_label[level][*current_ptr];
                }
            }
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::computeMean(const Image<T> &work_image, const Image<unsigned int> &labels, typename META_GENERAL_TYPE<T>::TYPE * * mean, unsigned int number_of_seeds, unsigned int number_of_threads)
    {
        const unsigned int top_level = labels.getNumberOfChannels() - 1;
        const unsigned int width = work_image.getWidth();
        const unsigned int height = work_image.getHeight();
        const unsigned int number_of_channels = work_image.getNumberOfChannels();
        
        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            for (unsigned int seed = 0; seed < number_of_seeds; ++seed)
                mean[channel][seed] = 0;
        
        if (number_of_threads <= 1)
        {
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const unsigned int * __restrict__ label_ptr = labels.get(y, top_level);
                    const T * __restrict__ color_ptr = work_image.get(y, channel);
                    for (unsigned int x = 0; x < width; ++x, ++color_ptr, ++label_ptr)
                        mean[channel][*label_ptr] += *color_ptr;
                }
            }
        }
        else
        {
            typename META_GENERAL_TYPE<T>::TYPE * * * thread_mean;
            
            thread_mean = new typename META_GENERAL_TYPE<T>::TYPE * * [number_of_threads];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                
                thread_mean[thread_id] = new typename META_GENERAL_TYPE<T>::TYPE * [number_of_channels];
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                {
                    thread_mean[thread_id][channel] = new typename META_GENERAL_TYPE<T>::TYPE[number_of_seeds];
                    for (unsigned int seed = 0; seed < number_of_seeds; ++seed)
                        thread_mean[thread_id][channel][seed] = 0;
                    for (unsigned int y = thread_id; y < height; y += number_of_threads)
                    {
                        const unsigned int * __restrict__ label_ptr = labels.get(y, top_level);
                        const T * __restrict__ color_ptr = work_image.get(y, channel);
                        for (unsigned int x = 0; x < width; ++x, ++color_ptr, ++label_ptr)
                            thread_mean[thread_id][channel][*label_ptr] += *color_ptr;
                    }
                }
            }
            for (unsigned int t = 0; t < number_of_threads; ++t)
            {
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                {
                    for (unsigned int seed = 0; seed < number_of_seeds; ++seed)
                        mean[channel][seed] += thread_mean[t][channel][seed];
                    delete [] thread_mean[t][channel];
                }
                delete [] thread_mean[t];
            }
            delete [] thread_mean;
        }
    }
    
    template <class T>
    bool Segmentation::Seeds<T>::probability(T const * const * image_ptr, int offset, typename META_GENERAL_TYPE<T>::TYPE const * const * mean, unsigned int const * const * block_size, const Tuple<unsigned int, int> &prior_a, const Tuple<unsigned int, int> &prior_b, unsigned int top_level, unsigned int number_of_channels)
    {
        float prob_a, prob_b;
        
        prob_a = prob_b = 0;
        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
        {
            float value_a, value_b;
            
            value_a = (float)(*(image_ptr[channel] + offset)) - (float)mean[channel][prior_a.getFirst()] / (float)block_size[top_level][prior_a.getFirst()];
            value_b = (float)(*(image_ptr[channel] + offset)) - (float)mean[channel][prior_b.getFirst()] / (float)block_size[top_level][prior_b.getFirst()];
            prob_a += value_a * value_a;
            prob_b += value_b * value_b;
        }
        return (prob_a / (float)prior_a.getSecond()) > (prob_b / (float)prior_b.getSecond());
    }
    
    template <class T>
    void Segmentation::Seeds<T>::update(const Image<unsigned int> &bins, const Image<T> &work_image, unsigned int label_new, unsigned int x, unsigned int y, Image<unsigned int> &labels, unsigned int * * * histogram, unsigned int * * block_size, typename META_GENERAL_TYPE<T>::TYPE * * mean)
    {
        const unsigned int number_of_channels = work_image.getNumberOfChannels();
        const unsigned int top_level = labels.getNumberOfChannels() - 1;
        unsigned int label_old, bin_index;
        
        label_old = *labels.get(x, y, top_level);
        *labels.get(x, y, top_level) = label_new;
        bin_index = *bins.get(x, y, 0);
        --histogram[top_level][label_old][bin_index];
        ++histogram[top_level][label_new][bin_index];
        --block_size[top_level][label_old];
        ++block_size[top_level][label_new];
        for (unsigned int c = 0; c < number_of_channels; ++c)
        {
            const T current_value = *work_image.get(x, y, c);
            mean[c][label_old] -= current_value;
            mean[c][label_new] += current_value;
        }
    }
    
    template <class T>
    void Segmentation::Seeds<T>::updatePixels(bool forward_backward, unsigned int * * * histogram, unsigned int * * block_size, Image<unsigned int> &bins, Image<unsigned int> &labels, Image<T> &work_image, typename META_GENERAL_TYPE<T>::TYPE * * mean)
    {
        const unsigned int number_of_channels = work_image.getNumberOfChannels();
        const unsigned int width = labels.getWidth();
        const unsigned int height = labels.getHeight();
        const unsigned int top_level = labels.getNumberOfChannels() - 1;
        VectorDense<const T *> work_ptrs(number_of_channels);
        unsigned int r30, r31, r32;
        
        if (forward_backward) // Forward direction ................................................
        {
            // Horizontal bidirectional ...........................................................
            for (unsigned int y = 1; y < height - 1; ++y)
            {
                unsigned int * __restrict__ r00_ptr = labels.get(y - 1, top_level);
                unsigned int * __restrict__ r01_ptr = labels.get(y    , top_level);
                unsigned int * __restrict__ r02_ptr = labels.get(y + 1, top_level);
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                    work_ptrs[channel] = work_image.get(1, y, channel);
                for (unsigned int x = 1; x < width - 1; ++x)
                {
                    if ((r01_ptr[1] != r01_ptr[2]) &&
                        (!checkSplit(r00_ptr[0], r00_ptr[1], r00_ptr[2], r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], true, true)))
                    {
                        const bool count_last = x < width - 2;
                        Tuple<unsigned int, int> prior_a, prior_b;
                        
                        prior_a.setData(r01_ptr[1], count3x4(labels, x, y, top_level, r01_ptr[1], count_last));
                        prior_b.setData(r01_ptr[2], count3x4(labels, x, y, top_level, r01_ptr[2], count_last));
                        
                        if (probability(work_ptrs.getData(), 0, mean, block_size, prior_a, prior_b, top_level, number_of_channels))
                            update(bins, work_image, prior_b.getFirst(), x, y, labels, histogram, block_size, mean);
                        else
                        {
                            if (count_last) { r30 = r00_ptr[3]; r31 = r01_ptr[3]; r32 = r02_ptr[3]; }
                            else r30 = r31 = r32 = -1;
                            if ((!checkSplit(r00_ptr[1], r00_ptr[2], r30, r01_ptr[1], r01_ptr[2], r31, r02_ptr[1], r02_ptr[2], r32, true, false)) &&
                                 (probability(work_ptrs.getData(), 1, mean, block_size, prior_b, prior_a, top_level, number_of_channels)))
                            {
                                update(bins, work_image, prior_a.getFirst(), x + 1, y, labels, histogram, block_size, mean);
                                for (unsigned int c = 0; c < number_of_channels; ++c)
                                    work_ptrs[c] += 1;
                                ++r00_ptr;
                                ++r01_ptr;
                                ++r02_ptr;
                                ++x;
                            }
                        }
                    }
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                        work_ptrs[c] += 1;
                    ++r00_ptr;
                    ++r01_ptr;
                    ++r02_ptr;
                }
            }
            
            // Vertical bidirectional .............................................................
            for (unsigned int x = 1; x < width - 1; ++x)
            {
                unsigned int * __restrict__ r00_ptr = labels.get(x - 1, 0, top_level);
                unsigned int * __restrict__ r01_ptr = labels.get(x - 1, 1, top_level);
                unsigned int * __restrict__ r02_ptr = labels.get(x - 1, 2, top_level);
                unsigned int * __restrict__ r03_ptr = labels.get(x - 1, 3, top_level);
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                    work_ptrs[channel] = work_image.get(x, 1, channel);
                for (unsigned int y = 1; y < height - 1; ++y)
                {
                    if ((r01_ptr[1] != r02_ptr[1]) &&
                        (!checkSplit(r00_ptr[0], r00_ptr[1], r00_ptr[2], r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], false, true)))
                    {
                        const bool count_last = y < height - 2;
                        Tuple<unsigned int, int> prior_a, prior_b;
                        
                        prior_a.setData(r01_ptr[1], count4x3(labels, x, y, top_level, r01_ptr[1], count_last));
                        prior_b.setData(r02_ptr[1], count4x3(labels, x, y, top_level, r02_ptr[1], count_last));
                        
                        if (probability(work_ptrs.getData(), 0, mean, block_size, prior_a, prior_b, top_level, number_of_channels))
                            update(bins, work_image, prior_b.getFirst(), x, y, labels, histogram, block_size, mean);
                        else
                        {
                            if (count_last) { r30 = r03_ptr[0]; r31 = r03_ptr[1]; r32 = r03_ptr[2]; }
                            else r30 = r31 = r32 = -1;
                            if ((!checkSplit(r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], r30, r31, r32, false, false)) &&
                                (probability(work_ptrs.getData(), width, mean, block_size, prior_b, prior_a, top_level, number_of_channels)))
                            {
                                update(bins, work_image, prior_a.getFirst(), x, y + 1, labels, histogram, block_size, mean);
                                for (unsigned int c = 0; c < number_of_channels; ++c)
                                    work_ptrs[c] += width;
                                r00_ptr += width;
                                r01_ptr += width;
                                r02_ptr += width;
                                r03_ptr += width;
                                ++y;
                            }
                        }
                    }
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                        work_ptrs[c] += width;
                    r00_ptr += width;
                    r01_ptr += width;
                    r02_ptr += width;
                    r03_ptr += width;
                }
            }
        }
        else // Backward direction ................................................................
        {
            // Horizontal bidirectional ...........................................................
            for (unsigned int y = 1; y < height - 1; ++y)
            {
                unsigned int * __restrict__ r00_ptr = labels.get(y - 1, top_level);
                unsigned int * __restrict__ r01_ptr = labels.get(y    , top_level);
                unsigned int * __restrict__ r02_ptr = labels.get(y + 1, top_level);
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                    work_ptrs[channel] = work_image.get(1, y, channel);
                for (unsigned int x = 1; x < width - 1; ++x)
                {
                    const bool count_last = x < width - 2;
                    if (count_last) { r30 = r00_ptr[3]; r31 = r01_ptr[3]; r32 = r02_ptr[3]; }
                    else r30 = r31 = r32 = -1;
                    if ((r01_ptr[1] != r01_ptr[2]) &&
                        (!checkSplit(r00_ptr[1], r00_ptr[2], r30, r01_ptr[1], r01_ptr[2], r31, r02_ptr[1], r02_ptr[2], r32, true, false)))
                    {
                        Tuple<unsigned int, int> prior_a, prior_b;
                        
                        prior_a.setData(r01_ptr[1], count3x4(labels, x, y, top_level, r01_ptr[1], count_last));
                        prior_b.setData(r01_ptr[2], count3x4(labels, x, y, top_level, r01_ptr[2], count_last));
                        
                        if (probability(work_ptrs.getData(), 1, mean, block_size, prior_b, prior_a, top_level, number_of_channels))
                        {
                            update(bins, work_image, prior_a.getFirst(), x + 1, y, labels, histogram, block_size, mean);
                            for (unsigned int c = 0; c < number_of_channels; ++c)
                                work_ptrs[c] += 1;
                            ++r00_ptr;
                            ++r01_ptr;
                            ++r02_ptr;
                            ++x;
                        }
                        else if ((!checkSplit(r00_ptr[0], r00_ptr[1], r00_ptr[2], r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], true, true)) &&
                                 (probability(work_ptrs.getData(), 0, mean, block_size, prior_a, prior_b, top_level, number_of_channels)))
                            update(bins, work_image, prior_b.getFirst(), x, y, labels, histogram, block_size, mean);
                    }
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                        work_ptrs[c] += 1;
                    ++r00_ptr;
                    ++r01_ptr;
                    ++r02_ptr;
                }
            }
            
            // Vertical bidirectional .............................................................
            for (unsigned int x = 1; x < width - 1; ++x)
            {
                unsigned int * __restrict__ r00_ptr = labels.get(x - 1, 0, top_level);
                unsigned int * __restrict__ r01_ptr = labels.get(x - 1, 1, top_level);
                unsigned int * __restrict__ r02_ptr = labels.get(x - 1, 2, top_level);
                unsigned int * __restrict__ r03_ptr = labels.get(x - 1, 3, top_level);
                for (unsigned int channel = 0; channel < number_of_channels; ++channel)
                    work_ptrs[channel] = work_image.get(x, 1, channel);
                for (unsigned int y = 1; y < height - 1; ++y)
                {
                    const bool count_last = y < height - 2;
                    if (count_last) { r30 = r03_ptr[0]; r31 = r03_ptr[1]; r32 = r03_ptr[2]; }
                    else r30 = r31 = r32 = -1;
                    if ((r01_ptr[1] != r02_ptr[1]) &&
                        (!checkSplit(r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], r30, r31, r32, false, false)))
                    {
                        Tuple<unsigned int, int> prior_a, prior_b;
                        
                        prior_a.setData(r01_ptr[1], count4x3(labels, x, y, top_level, r01_ptr[1], count_last));
                        prior_b.setData(r02_ptr[1], count4x3(labels, x, y, top_level, r02_ptr[1], count_last));
                        
                        if (probability(work_ptrs.getData(), width, mean, block_size, prior_b, prior_a, top_level, number_of_channels))
                        {
                            update(bins, work_image, prior_a.getFirst(), x, y + 1, labels, histogram, block_size, mean);
                            for (unsigned int c = 0; c < number_of_channels; ++c)
                                work_ptrs[c] += width;
                            r00_ptr += width;
                            r01_ptr += width;
                            r02_ptr += width;
                            r03_ptr += width;
                            ++y;
                        }
                        else if ((!checkSplit(r00_ptr[0], r00_ptr[1], r00_ptr[2], r01_ptr[0], r01_ptr[1], r01_ptr[2], r02_ptr[0], r02_ptr[1], r02_ptr[2], false, true)) &&
                                (probability(work_ptrs.getData(), 0, mean, block_size, prior_a, prior_b, top_level, number_of_channels)))
                            update(bins, work_image, prior_b.getFirst(), x, y, labels, histogram, block_size, mean);
                    }
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                        work_ptrs[c] += width;
                    r00_ptr += width;
                    r01_ptr += width;
                    r02_ptr += width;
                    r03_ptr += width;
                }
            }
        }
        // Update border pixels ...................................................................
        {
            unsigned int * __restrict__ top_a_ptr = labels.get(0, 0, top_level);
            unsigned int * __restrict__ top_b_ptr = labels.get(0, 1, top_level);
            unsigned int * __restrict__ bottom_a_ptr = labels.get(0, height - 1, top_level);
            unsigned int * __restrict__ bottom_b_ptr = labels.get(0, height - 2, top_level);
            for (unsigned int x = 0; x < width; ++x, ++top_a_ptr, ++top_b_ptr, ++bottom_a_ptr, ++bottom_b_ptr)
            {
                if (*top_a_ptr != *top_b_ptr)
                    update(bins, work_image, *top_b_ptr, x, 0, labels, histogram, block_size, mean);
                if (*bottom_a_ptr != *bottom_b_ptr)
                    update(bins, work_image, *bottom_b_ptr, x, height - 1, labels, histogram, block_size, mean);
            }
        }
        {
            unsigned int * __restrict__ left_ptr = labels.get(0, 0, top_level);
            unsigned int * __restrict__ right_ptr = labels.get(width - 2, 0, top_level);
            for (unsigned int y = 0; y < height; ++y, left_ptr += width, right_ptr += width)
            {
                if (left_ptr[0] != left_ptr[1])
                    update(bins, work_image, left_ptr[1], 0, y, labels, histogram, block_size, mean);
                if (right_ptr[0] != right_ptr[1])
                    update(bins, work_image, right_ptr[0], width - 1, y, labels, histogram, block_size, mean);
            }
        }
    }
    
    template <class T>
    int Segmentation::Seeds<T>::updateLevelIndex(int current_level, const unsigned int * number_of_seeds_x, const unsigned int * number_of_seeds_y, unsigned int * * number_of_partitions, unsigned int * * parent_label, unsigned int number_of_levels, unsigned int number_of_threads)
    {
        const unsigned int top_level = number_of_levels - 1;
        int new_level, old_level;
        
        new_level = current_level - 1;
        old_level = current_level;
        if (new_level < 0) return -1;
        
        if (number_of_threads <= 1)
        {
            const unsigned int twidth = number_of_seeds_x[top_level];
            const unsigned int theight = number_of_seeds_y[top_level];
            
            unsigned int * __restrict__ partition_ptr = number_of_partitions[top_level];
            for (unsigned int y = 0; y < theight; ++y)
                for (unsigned int x = 0; x < twidth; ++x, ++partition_ptr)
                    *partition_ptr = 0;
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                const unsigned int twidth = number_of_seeds_x[top_level];
                const unsigned int theight = number_of_seeds_y[top_level];
                const unsigned int row_update = twidth * (number_of_threads - 1);
                
                unsigned int * __restrict__ partition_ptr = number_of_partitions[top_level] + thread_id * twidth;
                for (unsigned int y = thread_id; y < theight; y += number_of_threads, partition_ptr += row_update)
                    for (unsigned int x = 0; x < twidth; ++x, ++partition_ptr)
                        *partition_ptr = 0;
            }
        }
        
        const unsigned int nwidth = number_of_seeds_x[new_level];
        const unsigned int nheight = number_of_seeds_y[new_level];
        unsigned int * __restrict__ parent_ptr = parent_label[new_level];
        unsigned int * __restrict__ partition_ptr = number_of_partitions[new_level];
        for (unsigned int y = 0; y < nheight; ++y)
        {
            for (unsigned int x = 0; x < nwidth; ++x, ++parent_ptr, ++partition_ptr)
            {
                unsigned int p;
                
                // Set the current parent to the parent of the old level.
                p = parent_label[old_level][*parent_ptr];
                *parent_ptr = p;
                // Add the number of partitions to the parent.
                number_of_partitions[top_level][p] += *partition_ptr;
            }
        }
        
        return new_level;
    }
    
    template <class T>
    unsigned int Segmentation::Seeds<T>::count3x4(Image<unsigned int> &labels, unsigned int x, unsigned int y, unsigned int level, unsigned int label, bool count_last)
    {
        unsigned int count, * ptr;
        
        count = 0;
        ptr = labels.get(x - 1, y - 1, level);
        count += (ptr[0] == label);
        count += (ptr[1] == label);
        count += (ptr[2] == label);
        if (count_last)
            count += (ptr[3] == label);
        ptr = labels.get(x - 1, y    , level);
        count += (ptr[0] == label);
        if (count_last)
            count += (ptr[3] == label);
        ptr = labels.get(x - 1, y + 1, level);
        count += (ptr[0] == label);
        count += (ptr[1] == label);
        count += (ptr[2] == label);
        if (count_last)
            count += (ptr[3] == label);
        return count;
    }
    
    template <class T>
    unsigned int Segmentation::Seeds<T>::count4x3(Image<unsigned int> &labels, unsigned int x, unsigned int y, unsigned int level, unsigned int label, bool count_last)
    {
        unsigned int count, * ptr;
        
        count = 0;
        ptr = labels.get(x - 1, y - 1, level);
        count += (ptr[0] == label);
        count += (ptr[1] == label);
        count += (ptr[2] == label);
        ptr = labels.get(x - 1, y    , level);
        count += (ptr[0] == label);
        count += (ptr[3] == label);
        ptr = labels.get(x - 1, y + 1, level);
        count += (ptr[0] == label);
        count += (ptr[3] == label);
        if (count_last)
        {
            ptr = labels.get(x - 1, y + 2, level);
            count += (ptr[0] == label);
            count += (ptr[1] == label);
            count += (ptr[2] == label);
        }
        return count;
    }
    
    template <class T>
    unsigned int Segmentation::Seeds<T>::generateSegmentationImage(const Image<unsigned int> &labels, Image<unsigned int> &segmentation, unsigned int number_of_threads)
    {
        const unsigned int top_level = labels.getNumberOfChannels() - 1;
        const unsigned int width = labels.getWidth();
        const unsigned int height = labels.getHeight();
        VectorDense<unsigned int> lut;
        std::map<unsigned int, unsigned int> map_lut;
        unsigned int maximum_index, index, number_of_regions;
        
        // 1) Get the maximum identifier of the labels image.
        if (number_of_threads > 1)
        {
            VectorDense<unsigned int> maximum_index_thr(number_of_threads);
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                unsigned int local_maximum = 0;
                for (unsigned int y = thread_id; y < height; y += number_of_threads)
                {
                    const unsigned int * __restrict__ ptr = labels.get(y, top_level);
                    for (unsigned int x = 0; x < width; ++x, ++ptr)
                        if (*ptr > local_maximum) local_maximum = *ptr;
                }
                maximum_index_thr[thread_id] = local_maximum;
            }
            maximum_index = maximum_index_thr[0];
            for (unsigned int t = 1; t < number_of_threads; ++t)
                if (maximum_index_thr[t] > maximum_index)
                    maximum_index = maximum_index_thr[t];
        }
        else
        {
            maximum_index = 0;
            for (unsigned int y = 0; y < height; ++y)
            {
                const unsigned int * __restrict__ ptr = labels.get(y, top_level);
                for (unsigned int x = 0; x < width; ++x, ++ptr)
                    if (*ptr > maximum_index) maximum_index = *ptr;
            }
        }
        // 2) Build a LUT which converts each identifier to the nearest contiguous index.
        number_of_regions = 0;
        for (unsigned int y = 0; y < height; ++y)
        {
            const unsigned int * __restrict__ ptr = labels.get(y, top_level);
            for (unsigned int x = 0; x < width; ++x, ++ptr)
            {
                std::map<unsigned int, unsigned int>::iterator search;
                
                search = map_lut.find(*ptr);
                if (search == map_lut.end())
                {
                    map_lut[*ptr] = number_of_regions;
                    ++number_of_regions;
                }
            }
        }
        // 3) Put the LUT of the map in a vector.
        lut.set(maximum_index + 1, 0);
        index = 0;
        for (std::map<unsigned int, unsigned int>::iterator begin = map_lut.begin(), end = map_lut.end(); begin != end; ++begin, ++index)
            lut[begin->first] = begin->second;
        // 4) Copy the labels image to the segmentation image with the corrected labels.
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                {
                    const unsigned int * __restrict__ ptr_src = labels.get(y, top_level);
                    unsigned int * __restrict__ ptr_dst = segmentation.get(y, 0);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_src, ++ptr_dst)
                        *ptr_dst = lut[*ptr_src];
                }
            }
        }
        else
        {
            for (unsigned int y = 0; y < height; ++y)
            {
                const unsigned int * __restrict__ ptr_src = labels.get(y, top_level);
                unsigned int * __restrict__ ptr_dst = segmentation.get(y, 0);
                for (unsigned int x = 0; x < width; ++x, ++ptr_src, ++ptr_dst)
                    *ptr_dst = lut[*ptr_src];
            }
        }
        return number_of_regions;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | BINARIZATION FUNCTIONS               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TINPUT, class TOUTPUT>
    void Segmentation::binarizeOtsu(const ConstantSubImage<TINPUT> &input, double weight_factor, SubImage<TOUTPUT> output, unsigned int number_of_threads)
    {
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        const unsigned int number_of_bins = 1000;
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw Exception("[function binarizeOtsu] Input and output images must have the same geometry ([%d, %d, %d] != [%d, %d, %d])", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        
        for (unsigned int channel = 0; channel < number_of_channels; ++channel)
        {
            if (number_of_threads == 1)
            {
                VectorDense<unsigned int> frequency_histogram(number_of_bins, 0);
                TINPUT maximum_value, minimum_value;
                TINPUT threshold;
                double factor;
                
                // Calculate the maximum and minimum values of the image.
                maximum_value = minimum_value = *input.get(0, 0, channel);
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr = input.get(y, channel);
                    for (unsigned int x = 0; x < width; ++x, ++ptr)
                    {
                        if (*ptr > maximum_value) maximum_value = *ptr;
                        if (*ptr < minimum_value) minimum_value = *ptr;
                    }
                }
                // Build the image histogram.
                factor = (double)number_of_bins / (double)(maximum_value - minimum_value);
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr = input.get(y, channel);
                    for (unsigned int x = 0; x < width; ++x, ++ptr)
                        ++frequency_histogram[std::max(0, std::min((int)number_of_bins - 1, (int)round((double)(*ptr - minimum_value) * factor)))];
                }
                // Calculate the Otsu threshold.
                threshold = (TINPUT)(weight_factor * ((double)(maximum_value - minimum_value) * (double)otsuThreshold(frequency_histogram.getData(), number_of_bins) / (double)number_of_bins + (double)minimum_value));
                // Binarize the current image layer.
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr_in = input.get(y, channel);
                    TOUTPUT * __restrict__ ptr_out = output.get(y, channel);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_in, ++ptr_out)
                        *ptr_out = (*ptr_in < threshold)?0:1;
                }
            }
            else
            {
                VectorDense<unsigned int> * frequency_histogram;
                TINPUT * maximum_value, * minimum_value;
                TINPUT threshold;
                double factor;
                
                frequency_histogram = new VectorDense<unsigned int>[number_of_threads];
                maximum_value = new TINPUT[number_of_threads];
                minimum_value = new TINPUT[number_of_threads];
                for (unsigned int t = 0; t < number_of_threads; ++t)
                {
                    frequency_histogram[t].set(number_of_bins, 0);
                    maximum_value[t] = minimum_value[t] = *input.get(t, 0, channel);
                }
                
                // Calculate the maximum and minimum values of the image.
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr = input.get(thread_identifier, y, channel);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr += number_of_threads)
                        {
                            if (*ptr > maximum_value[thread_identifier]) maximum_value[thread_identifier] = *ptr;
                            if (*ptr < minimum_value[thread_identifier]) minimum_value[thread_identifier] = *ptr;
                        }
                    }
                }
                for (unsigned int t = 1; t < number_of_threads; ++t)
                {
                    if (maximum_value[t] > maximum_value[0]) maximum_value[0] = maximum_value[t];
                    if (minimum_value[t] < minimum_value[0]) minimum_value[0] = minimum_value[t];
                }
                // Build the image histogram.
                factor = (double)number_of_bins / (double)(maximum_value[0] - minimum_value[0]);
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr = input.get(thread_identifier, y, channel);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr += number_of_threads)
                            ++frequency_histogram[thread_identifier][std::max(0, std::min((int)number_of_bins - 1, (int)round((double)(*ptr - minimum_value[0]) * factor)))];
                    }
                }
                for (unsigned int t = 1; t < number_of_threads; ++t)
                    for (unsigned int b = 0; b < number_of_bins; ++b)
                        frequency_histogram[0][b] += frequency_histogram[t][b];
                // Calculate the Otsu threshold.
                threshold = (TINPUT)(weight_factor * ((double)(maximum_value[0] - minimum_value[0]) * (double)otsuThreshold(frequency_histogram[0].getData(), number_of_bins) / (double)number_of_bins + (double)minimum_value[0]));
                // Binarize the current image layer.
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr_in = input.get(thread_identifier, y, channel);
                        TOUTPUT * __restrict__ ptr_out = output.get(thread_identifier, y, channel);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_in += number_of_threads, ptr_out += number_of_threads)
                            *ptr_out = (*ptr_in < threshold)?0:1;
                    }
                }
                
                delete [] frequency_histogram;
                delete [] maximum_value;
                delete [] minimum_value;
            }
        }
    }
    
    template <class TINPUT, class TOUTPUT>
    void Segmentation::binarizeSauvola(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT> output, unsigned int number_of_threads)
    {
        typedef typename META_IF<std::numeric_limits<TINPUT>::is_integer, long, double>::RESULT TINTEGRAL;
        const TINTEGRAL area = (TINTEGRAL)window_size * (TINTEGRAL)window_size;
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw Exception("[function binarizeSauvola] Input and output images must have the same geometry ([%d, %d, %d] != [%d, %d, %d])", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        
        IntegralImage<TINTEGRAL> integral_simple, integral_squared;
        Image<TINTEGRAL> image_simple, image_squared;
        
        image_squared.setGeometry(input);
        image_simple.setGeometry(input);
        integral_simple.set(input, number_of_threads);
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr_input = input.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_squared = image_squared.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_input += number_of_threads, ptr_squared += number_of_threads)
                            *ptr_squared = (TINTEGRAL)*ptr_input * (TINTEGRAL)*ptr_input;
                    }
                }
            }
        }
        else
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr_input = input.get(y, c);
                    TINTEGRAL * __restrict__ ptr_squared = image_squared.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_input, ++ptr_squared)
                        *ptr_squared = (TINTEGRAL)*ptr_input * (TINTEGRAL)*ptr_input;
                }
            }
        }
        integral_squared.set(image_squared, number_of_threads);
        
        integralImageSum(integral_simple, window_size, window_size, image_simple, number_of_threads);
        integralImageSum(integral_squared, window_size, window_size, image_squared, number_of_threads);
        
        if (number_of_threads > 1)
        {
            VectorDense<TINTEGRAL> maximum_standard(number_of_threads);
            
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    maximum_standard[thread_identifier] = 0;
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        TINTEGRAL * __restrict__ ptr_simple = image_simple.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_squared = image_squared.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_simple += number_of_threads, ptr_squared += number_of_threads)
                        {
                            // Mean of the window.
                            *ptr_simple /= area;
                            // Standard deviation of the window.
                            *ptr_squared = (TINTEGRAL)std::sqrt((*ptr_squared - area * *ptr_simple * *ptr_simple) / area);
                            if (*ptr_squared > maximum_standard[thread_identifier])
                                maximum_standard[thread_identifier] = *ptr_squared;
                        }
                    }
                }
                for (unsigned int t = 1; t < number_of_threads; ++t)
                    if (maximum_standard[t] > maximum_standard[0])
                        maximum_standard[0] = maximum_standard[t];
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINTEGRAL * __restrict__ ptr_simple = image_simple.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_squared = image_squared.get(thread_identifier, y, c);
                        const TINPUT * __restrict__ ptr_in = input.get(thread_identifier, y, c);
                        TOUTPUT * __restrict__ ptr_out = output.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_simple += number_of_threads, ptr_squared += number_of_threads, ptr_in += number_of_threads, ptr_out += number_of_threads)
                        {
                            const TINPUT threshold = (TINPUT)((double)*ptr_simple * (1.0 + sauvola_factor * ((double)*ptr_squared / (double)maximum_standard[0] - 1.0)));
                            *ptr_out = (*ptr_in <= threshold)?0:1;
                        }
                    }
                }
            }
        }
        else
        {
            TINTEGRAL maximum_standard;
            
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                maximum_standard = 0;
                for (unsigned int y = 0; y < height; ++y)
                {
                    TINTEGRAL * __restrict__ ptr_simple = image_simple.get(y, c);
                    TINTEGRAL * __restrict__ ptr_squared = image_squared.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_simple, ++ptr_squared)
                    {
                        // Mean of the window.
                        *ptr_simple /= area;
                        // Standard deviation of the window.
                        *ptr_squared = (TINTEGRAL)std::sqrt((*ptr_squared - area * *ptr_simple * *ptr_simple) / area);
                        if (*ptr_squared > maximum_standard)
                            maximum_standard = *ptr_squared;
                    }
                }
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINTEGRAL * __restrict__ ptr_simple = image_simple.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_squared = image_squared.get(y, c);
                    const TINPUT * __restrict__ ptr_in = input.get(y, c);
                    TOUTPUT * __restrict__ ptr_out = output.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_simple, ++ptr_squared, ++ptr_in, ++ptr_out)
                    {
                        const TINPUT threshold = (TINPUT)((double)*ptr_simple * (1.0 + sauvola_factor * ((double)*ptr_squared / (double)maximum_standard - 1.0)));
                        *ptr_out = (*ptr_in <= threshold)?0:1;
                    }
                }
            }
        }
    }
    
    template <class TINPUT, class TOUTPUT>
    void Segmentation::binarizeBradley(const ConstantSubImage<TINPUT> &input, double weight_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads)
    {
        typedef typename META_IF<std::numeric_limits<TINPUT>::is_integer, long, double>::RESULT TINTEGRAL;
        const TINTEGRAL area = (TINTEGRAL)window_size * (TINTEGRAL)window_size;
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw Exception("[function binarizeBradley] Input and output images must have the same geometry ([%d, %d, %d] != [%d, %d, %d])", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        
        IntegralImage<TINTEGRAL> integral;
        Image<TINTEGRAL> image_sum;
        
        integral.set(input, number_of_threads);
        image_sum.setGeometry(input);
        integralImageSum(integral, window_size, window_size, image_sum, number_of_threads);
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINTEGRAL * __restrict__ ptr_mean = image_sum.get(thread_identifier, y, 0);
                    const TINPUT * __restrict__ ptr_input = input.get(thread_identifier, y, 0);
                    TOUTPUT * __restrict__ ptr_output = output.get(thread_identifier, y, 0);
                    for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_mean += number_of_threads, ptr_input += number_of_threads, ptr_output += number_of_threads)
                        *ptr_output = ((TINTEGRAL)*ptr_input * area > (TINTEGRAL)((double)*ptr_mean * (1.0 - weight_factor)))?1:0;
                }
            }
        }
        else
        {
            for (unsigned int y = 0; y < height; ++y)
            {
                const TINTEGRAL * __restrict__ ptr_mean = image_sum.get(y, 0);
                const TINPUT * __restrict__ ptr_input = input.get(y, 0);
                TOUTPUT * __restrict__ ptr_output = output.get(y, 0);
                for (unsigned int x = 0; x < width; ++x, ++ptr_mean, ++ptr_input, ++ptr_output)
                    *ptr_output = ((TINTEGRAL)*ptr_input * area > (TINTEGRAL)((double)*ptr_mean * (1.0 - weight_factor)))?1:0;
            }
        }
    }
    
    template <class TINPUT, class TOUTPUT>
    void Segmentation::binarizeGatos(const ConstantSubImage<TINPUT> &input, double sauvola_factor, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads)
    {
        typedef typename META_IF<std::numeric_limits<TINPUT>::is_integer, long, double>::RESULT TINTEGRAL;
        const double q = 0.6;  // Magical empirical constants.
        const double p1 = 0.5;
        const double p2 = 0.8;
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw Exception("[function binarizeBradley] Input and output images must have the same geometry ([%d, %d, %d] != [%d, %d, %d])", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        srv::IntegralImage<TINTEGRAL> integral_image;
        srv::Image<TINTEGRAL> image_sum, image_count, image_count_not, image_distance, image_weight;
        srv::Image<unsigned char> image_threshold;
        srv::Image<TINPUT> image_filtered, image_background;
        
        // Filter the image high-frequency noise.
        srv::filterWiener(input, 7, image_filtered, number_of_threads);
        // Obtain an initial segmentation with the Sauvola algorithm.
        srv::Segmentation::binarizeSauvola(image_filtered, sauvola_factor, window_size, image_threshold, number_of_threads);
        
        // Estimate the image background.
        image_background.setGeometry(input);
        image_distance.setGeometry(input);
        image_weight.setGeometry(input);
        integral_image.set(image_filtered * image_threshold, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, image_sum, number_of_threads);
        integral_image.set(image_threshold, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, image_count, number_of_threads);
        integral_image.set(1 - image_threshold, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, image_count_not, number_of_threads);
        
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINTEGRAL * __restrict__ ptr_sum = image_sum.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_count = image_count.get(thread_identifier, y, c);
                        const unsigned char * __restrict__ ptr_thr = image_threshold.get(thread_identifier, y, c);
                        const TINPUT * __restrict__ ptr_in = image_filtered.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_distance = image_distance.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_weight = image_weight.get(thread_identifier, y, c);
                        TINPUT * __restrict__ ptr_background = image_background.get(thread_identifier, y, c);
                        
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_sum += number_of_threads, ptr_count += number_of_threads, ptr_thr += number_of_threads, ptr_in += number_of_threads, ptr_distance += number_of_threads, ptr_weight += number_of_threads, ptr_background += number_of_threads)
                        {
                            if (*ptr_thr > 0)
                            {
                                *ptr_background = *ptr_in;
                                *ptr_distance = *ptr_weight = 0;
                            }
                            else if (*ptr_count > 0)
                            {
                                *ptr_background = (TINPUT)(*ptr_weight = *ptr_sum / *ptr_count);
                                *ptr_distance = *ptr_weight - (TINTEGRAL)*ptr_in;
                            }
                            else { *ptr_background = 0; *ptr_distance = *ptr_weight = 0;}
                        }
                    }
                }
            }
        }
        else
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINTEGRAL * __restrict__ ptr_sum = image_sum.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_count = image_count.get(y, c);
                    const unsigned char * __restrict__ ptr_thr = image_threshold.get(y, c);
                    const TINPUT * __restrict__ ptr_in = image_filtered.get(y, c);
                    TINTEGRAL * __restrict__ ptr_distance = image_distance.get(y, c);
                    TINTEGRAL * __restrict__ ptr_weight = image_weight.get(y, c);
                    TINPUT * __restrict__ ptr_background = image_background.get(y, c);
                    
                    for (unsigned int x = 0; x < width; ++x, ++ptr_sum, ++ptr_count, ++ptr_thr, ++ptr_in, ++ptr_distance, ++ptr_weight, ++ptr_background)
                    {
                        if (*ptr_thr > 0)
                        {
                            *ptr_background = *ptr_in;
                            *ptr_distance = *ptr_weight = 0;
                        }
                        else if (*ptr_count > 0)
                        {
                            *ptr_background = (TINPUT)(*ptr_weight = *ptr_sum / *ptr_count);
                            *ptr_distance = *ptr_weight - (TINTEGRAL)*ptr_in;
                        }
                        else { *ptr_background = 0; *ptr_distance = *ptr_weight = 0;}
                    }
                }
            }
        }
        integral_image.set(image_distance, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, image_distance, number_of_threads);
        
        // Calculate the final binarization.
        if (number_of_threads > 1)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr_in = image_filtered.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_distance = image_distance.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_count = image_count_not.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_weight = image_weight.get(thread_identifier, y, c);
                        const TINPUT * __restrict__ ptr_background = image_background.get(thread_identifier, y, c);
                        TOUTPUT * __restrict__ ptr_output = output.get(thread_identifier, y, c);
                        
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_in += number_of_threads, ptr_distance += number_of_threads, ptr_count += number_of_threads, ptr_weight += number_of_threads, ptr_background += number_of_threads, ptr_output += number_of_threads)
                        {
                            if (*ptr_count > 0)
                            {
                                double threshold;
                                
                                threshold = q * ((double)*ptr_distance / (double)*ptr_count) * ((1.0 - p2) / (1 + std::exp(-4 * (double)*ptr_background / ((double)*ptr_weight * (1 - p1)) + 2 * (1 + p1) / (1 - p1))) + p2);
                                *ptr_output = (*ptr_background - *ptr_in >= (TINTEGRAL)threshold)?0:1;
                            }
                            else *ptr_output = 1;
                        }
                    }
                }
            }
        }
        else
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr_in = image_filtered.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_distance = image_distance.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_count = image_count_not.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_weight = image_weight.get(y, c);
                    const TINPUT * __restrict__ ptr_background = image_background.get(y, c);
                    TOUTPUT * __restrict__ ptr_output = output.get(y, c);
                    
                    for (unsigned int x = 0; x < width; ++x, ++ptr_in, ++ptr_distance, ++ptr_count, ++ptr_weight, ++ptr_background, ++ptr_output)
                    {
                        if (*ptr_count > 0)
                        {
                            double threshold;
                            
                            threshold = q * ((double)*ptr_distance / (double)*ptr_count) * ((1.0 - p2) / (1 + std::exp(-4 * (double)*ptr_background / ((double)*ptr_weight * (1 - p1)) + 2 * (1 + p1) / (1 - p1))) + p2);
                            *ptr_output = (*ptr_background - *ptr_in >= (TINTEGRAL)threshold)?0:1;
                        }
                        else *ptr_output = 1;
                    }
                }
            }
        }
        
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// 
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// for (unsigned int c = 0; c < number_of_channels; ++c)
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// {
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////     for (unsigned int y = 0; y < height; ++y)
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////     {
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         const TINTEGRAL * __restrict__ ptr_sum = image_sum.get(y, c);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         const TINTEGRAL * __restrict__ ptr_count = image_count.get(y, c);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         const unsigned char * __restrict__ ptr_thr = image_threshold.get(y, c);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         const TINPUT * __restrict__ ptr_in = image_filtered.get(y, c);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         TINPUT * __restrict__ ptr_out = image_background.get(y, c);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         for (unsigned int x = 0; x < width; ++x, ++ptr_sum, ++ptr_count, ++ptr_thr, ++ptr_in, ++ptr_out)
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         {
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////             if (*ptr_thr > 0) *ptr_out = *ptr_in;
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////             else if (*ptr_count > 0) *ptr_out = (TINPUT)(*ptr_sum / *ptr_count);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////             else *ptr_out = 0;
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////         }
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE /////     }
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// }
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// srv::Figure figure_debug("background");
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// figure_debug(image_background);
        ///// CODE TO OBTAIN THE BACKGROUND IMAGE ///// for (char key = 0; key != 27; key = srv::Figure::wait());
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

