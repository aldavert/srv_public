// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_OPERATORS_HEADER_FILE__
#define __SRV_IMAGE_OPERATORS_HEADER_FILE__

#include "srv_image.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | MACROS                               |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <template <class> class FIRST_IMAGE, class T1, template <class> class SECOND_IMAGE, class T2, class OPERATOR>
    inline void templateImageOperator(const FIRST_IMAGE<T1> &first, const SECOND_IMAGE<T2> &second, Image< typename META_MGT<T1, T2>::TYPE > &destination, const OPERATOR &image_operator)
    {
        destination.setGeometry(first.getWidth(), first.getHeight(), first.getNumberOfChannels());
        checkGeometry(first, second);
        ImageGeneralOperator(first, second, destination, image_operator, DefaultNumberOfThreads::ImageArithmetic());
    }
    
    template <template <class> class IMAGE, class T1, class T2, class OPERATOR>
    inline void templateImageOperator(const IMAGE<T1> &source, T2 value, Image< typename META_MGT<T1, T2>::TYPE > &destination, const OPERATOR &image_operator)
    {
        destination.setGeometry(source.getWidth(), source.getHeight(), source.getNumberOfChannels());
        ImageGeneralOperator(source, value, destination, image_operator, DefaultNumberOfThreads::ImageArithmetic());
    }
    template <class T2, template <class> class IMAGE, class T1, class OPERATOR>
    inline void templateImageOperator(T2 value, const IMAGE<T1> &source, Image< typename META_MGT<T1, T2>::TYPE > &destination, const OPERATOR &image_operator)
    {
        destination.setGeometry(source.getWidth(), source.getHeight(), source.getNumberOfChannels());
        ImageGeneralOperator(value, source, destination, image_operator, DefaultNumberOfThreads::ImageArithmetic());
    }

#define IMAGE_OPERATOR_CALLS(op, image_operator)\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const Image<T1> &first, const Image<T2> &second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const Image<T1> &first, const SubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const Image<T1> &first, const ConstantSubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const SubImage<T1> first, const Image<T2> &second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const SubImage<T1> first, const SubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const SubImage<T1> first, const ConstantSubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const ConstantSubImage<T1> first, const Image<T2> &second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const ConstantSubImage<T1> first, const SubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const ConstantSubImage<T1> first, const ConstantSubImage<T2> second)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(first, second, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const Image<T1> &source, T2 value)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(source, value, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const SubImage<T1> source, T2 value)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(source, value, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(const ConstantSubImage<T1> source, T2 value)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(source, value, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(T2 value, const Image<T1> &source)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(value, source, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(T2 value, const SubImage<T1> source)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(value, source, result, image_operator);\
        return result;\
    }\
    template <class T1, class T2>\
    inline Image<typename META_MGT<T1, T2>::TYPE> op(T2 value, const ConstantSubImage<T1> source)\
    {\
        Image<typename META_MGT<T1, T2>::TYPE> result;\
        templateImageOperator(value, source, result, image_operator);\
        return result;\
    }
    
#define IMAGE_UNARY_OPERATOR_CALLS(op, image_operator)\
    template <class T>\
    inline Image<T> op(const Image<T> &image)\
    {\
        Image<T> result(image.getWidth(), image.getHeight(), image.getNumberOfChannels());\
        image_operator(image, result);\
        return result;\
    }\
    template <class T>\
    inline Image<T> op(const SubImage<T> image)\
    {\
        Image<T> result(image.getWidth(), image.getHeight(), image.getNumberOfChannels());\
        image_operator(image, result);\
        return result;\
    }\
    template <class T>\
    inline Image<T> op(const ConstantSubImage<T> image)\
    {\
        Image<T> result(image.getWidth(), image.getHeight(), image.getNumberOfChannels());\
        image_operator(image, result);\
        return result;\
    }
    
#define IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(op, image_operator)\
    template <class T1, class T2>\
    inline Image<T1>& op(Image<T1> &self, const Image<T2> &second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline Image<T1>& op(Image<T1> &self, const SubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline Image<T1>& op(Image<T1> &self, const ConstantSubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline Image<T1>& op(Image<T1> &self, T2 value)\
    {\
        image_operator(self, value, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1>& op(SubImage<T1> &self, const Image<T2> &second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1>& op(SubImage<T1> &self, const SubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1>& op(SubImage<T1> &self, const ConstantSubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1>& op(SubImage<T1> &self, T2 value)\
    {\
        image_operator(self, value, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1> op(SubImage<T1> self, const Image<T2> &second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1> op(SubImage<T1> self, const SubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1> op(SubImage<T1> self, const ConstantSubImage<T2> second)\
    {\
        checkGeometry(self, second);\
        image_operator(self, second, self);\
        return self;\
    }\
    template <class T1, class T2>\
    inline SubImage<T1> op(SubImage<T1> self, T2 value)\
    {\
        image_operator(self, value, self);\
        return self;\
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE AND SUB-IMAGE OPERATORS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    // ARITHMETIC OPERATOR ..........................................................................................................................
    IMAGE_OPERATOR_CALLS(operator+, PixelAddition())
    IMAGE_OPERATOR_CALLS(operator-, PixelSubtraction())
    IMAGE_OPERATOR_CALLS(operator*, PixelMultiplication())
    IMAGE_OPERATOR_CALLS(operator/, PixelDivision())
    IMAGE_OPERATOR_CALLS(operator%, PixelModulo())
    
    // BITWISE OPERATOR .............................................................................................................................
    IMAGE_OPERATOR_CALLS(operator|, PixelBitwiseOr())
    IMAGE_OPERATOR_CALLS(operator&, PixelBitwiseAnd())
    IMAGE_OPERATOR_CALLS(operator>>, PixelBitwiseRightShift())
    IMAGE_OPERATOR_CALLS(operator<<, PixelBitwiseLeftShift())
    IMAGE_OPERATOR_CALLS(operator^, PixelBitwiseXOR())
    
    // LOGIC OPERATORS ..............................................................................................................................
    IMAGE_OPERATOR_CALLS(operator||, PixelOr())
    IMAGE_OPERATOR_CALLS(operator&&, PixelAnd())
    
    // COMPARISON OPERATORS .........................................................................................................................
    IMAGE_OPERATOR_CALLS(operator==, PixelEqual())
    IMAGE_OPERATOR_CALLS(operator!=, PixelDifferent())
    IMAGE_OPERATOR_CALLS(operator>, PixelGreaterThan())
    IMAGE_OPERATOR_CALLS(operator<, PixelLesserThan())
    IMAGE_OPERATOR_CALLS(operator>=, PixelGreaterEqualThan())
    IMAGE_OPERATOR_CALLS(operator<=, PixelLesserEqualThan())
    
    // =[ UNARY OPERATORS ]==========================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    IMAGE_UNARY_OPERATOR_CALLS(operator-, ImageNegate)
    IMAGE_UNARY_OPERATOR_CALLS(operator!, ImageNot)
    IMAGE_UNARY_OPERATOR_CALLS(operator~, ImageBitwiseNot)
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ COMPOUND ASSIGNMENT OPERATORS ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.

    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator+=, ImageAddition)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator-=, ImageSubtraction)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator*=, ImageMultiplication)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator/=, ImageDivision)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator%=, ImageModulo)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator&=, ImageBitwiseAnd)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator|=, ImageBitwiseOr)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator^=, ImageBitwiseXOR)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator>>=, ImageBitwiseRightShift)
    IMAGE_COMPOUND_ASSIGNMENT_OPERATOR_CALLS(operator<<=, ImageBitwiseLeftShift)
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

