// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_PROCESSING_HEADER_FILE__
#define __SRV_IMAGE_PROCESSING_HEADER_FILE__

#ifdef __ENABLE_FFTW__
#include <fftw3.h>
#endif
#include <omp.h>
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "srv_image.hpp"
#include "srv_image_integral.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
#ifdef __ENABLE_FFTW__
    //                   +--------------------------------------+
    //                   | FREQUENCY DOMAIN IMAGES CLASS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Image in the frequency domain. The original image is converted into the frequency domain by 
     *  using the Discrete Fourier Transform (DFT) of the FTTw3 library.
     */
    class ImageFFT
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        ImageFFT(void);
        /** Constructor which sets the geometry of the frequency image.
         *  The resulting spatial/frequency image is in the spatial domain.
         *  \param[in] width width of the original spatial domain image.
         *  \param[in] height height of the original spatial domain image.
         *  \param[in] number_of_channels number of channels of the original spatial domain image.
         */
        ImageFFT(unsigned int width, unsigned int height, unsigned int number_of_channels);
        /// Copy constructor.
        ImageFFT(const ImageFFT &other);
        /// Destructor.
        ~ImageFFT(void);
        /// Assignation operator.
        ImageFFT& operator=(const ImageFFT &other);
        /** This function sets the geometry of the frequency domain image.
         *  \param[in] width width of the original spatial domain image.
         *  \param[in] height height of the original spatial domain image.
         *  \param[in] number_of_channels number of channels of the original spatial domain image.
         */
        void setGeometry(unsigned int width, unsigned int height, unsigned int number_of_channels);
        /** This function sets the content of the frequency domain image. A margin is added for images
         *  which are smaller than the frequency domain image. This margin is set to zero.
         *  \param[in] source image or sub-image used to set the content of the frequency domain image.
         *  \param[in] number_of_threads number of threads used to concurrently transform the image.
         */
        template <template <class> class IMAGE, class T>
        void setImage(const IMAGE<T> &source, unsigned int number_of_threads);
        /** This function converts the frequency domain image into a spatial domain image.
         *  \param[out] destination image or sub-image used to set the content of the frequency domain image.
         *  \param[in] number_of_threads number of threads used to concurrently transform the image.
         *  \note the destination image is expected to have the same geometry as the frequency image.
         */
        template <template <class> class IMAGE, class T>
        void getImage(IMAGE<T> &destination, unsigned int number_of_threads) const;
        /** This function converts the frequency domain image into a spatial domain image.
         *  \param[out] destination resulting double image.
         *  \param[in] number_of_threads number of threads used to concurrently transform the image.
         */
        void getImage(Image<double> &destination, unsigned int number_of_threads) const;
        /** This function returns the images of the real and imaginary values of the frequency domain image.
         *  \param[out] real real part of the complex frequency image.
         *  \param[out] imaginary imaginary part of the complex frequency image.
         */
        void getComplex(Image<double> &real, Image<double> &imaginary) const;
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the width of both the spatial domain and frequency domain images.
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of both the spatial domain and frequency domain images.
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the width of the frequency domain image.
        inline unsigned int getFFTWidth(void) const { return m_fft_width; }
        /// Returns the number of channels of both the spatial domain and frequency domain images.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        /// Returns a constant pointer to the frequency domain complex image of the index-th channel.
        inline const fftw_complex * get(unsigned int index) const { return m_fft_image[index]; }
        /// Returns a pointer to the frequency domain complex image of the index-th channel.
        inline fftw_complex * get(unsigned int index) { return m_fft_image[index]; }
        
        // -[ Operation functions ]------------------------------------------------------------------------------------------------------------------
        /** Sets the current image as the addition between two frequency domain images.
         *  \param[in] left left operand image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void add(const ImageFFT &left, const ImageFFT &right);
        /** Sets the current image as the subtraction between two frequency domain images.
         *  \param[in] left left operand image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void subtraction(const ImageFFT &left, const ImageFFT &right);
        /** Sets the current image as the multiplication between two frequency domain images.
         *  \param[in] left left operand image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void multiplication(const ImageFFT &left, const ImageFFT &right);
        /** Sets the current image as the division between two frequency domain images.
         *  \param[in] left left operand image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void division(const ImageFFT &left, const ImageFFT &right);
        
        /** Adds to the current image another frequency domain image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void add(const ImageFFT &right);
        /** Subtracts to the current image another frequency domain image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void subtraction(const ImageFFT &right);
        /** Multiplies the current image with another frequency domain image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void multiplication(const ImageFFT &right);
        /** Divides the current image with another frequency domain image.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        void division(const ImageFFT &right);
        
        /** Addition assignment operator.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        inline ImageFFT& operator+=(const ImageFFT &right) { add(right); return *this; }
        /** Subtraction assignment operator.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        inline ImageFFT& operator-=(const ImageFFT &right) { subtraction(right); return *this; }
        /** Multiplication assignment operator.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        inline ImageFFT& operator*=(const ImageFFT &right) { multiplication(right); return *this; }
        /** Division assignment operator.
         *  \param[in] right right operand image.
         *  \note both images must have the same geometry.
         */
        inline ImageFFT& operator/=(const ImageFFT &right) { division(right); return *this; }
        
        // -[ Static functions ]---------------------------------------------------------------------------------------------------------------------
        /** Flips the left-right and up-down quadrants of the image. To be used after an image is convolved in the frequency space.
         *  \param[in] image space domain image to be transformed.
         */
        template <template <class> class IMAGE, class T>
        static void shift(IMAGE<T> &image)
        {
            const unsigned int half_width = image.getWidth() / 2;
            const unsigned int half_height = image.getHeight() / 2;
            for (unsigned int channel = 0; channel < image.getNumberOfChannels(); ++channel)
            {
                for (unsigned int y = 0; y < image.getHeight(); ++y)
                {
                    T * __restrict__ dst_ptr = image.get(y, channel);
                    for (unsigned int x = 0; x < half_width; ++x)
                        srvSwap(dst_ptr[x], dst_ptr[x + half_width]);
                }
                for (unsigned int y = 0; y < half_height; ++y)
                {
                    T * __restrict__ dst01_ptr = image.get(y, channel);
                    T * __restrict__ dst02_ptr = image.get(y + half_height, channel);
                    for (unsigned int x = 0; x < image.getWidth(); ++x)
                        srvSwap(dst01_ptr[x], dst02_ptr[x]);
                }
            }
        }
        
    protected:
        /// Protected function which checks the geometry of the spatial/frequency images before applying the operator.
        void operandGeometry(const ImageFFT &left, const ImageFFT &right);
        
        /// Width of the spatial image.
        unsigned int m_width;
        /// Width of the frequency image.
        unsigned int m_fft_width;
        /// Height of both the spatial and frequency images.
        unsigned int m_height;
        /// Number of channels of both the spatial and the frequency image.
        unsigned int m_number_of_channels;
        /// Pointer to the frequency domain images.
        fftw_complex * * m_fft_image;
    };
    
    template <template <class> class IMAGE, class T>
    void ImageFFT::setImage(const IMAGE<T> &source, unsigned int number_of_threads)
    {
        unsigned int padding_x0, padding_y0, padding_x1, padding_y1;
        double * padding_image, * padding_ptr;
        if ((source.getWidth() > m_width) || (source.getHeight() > m_height))
            throw Exception("The given image (%dx%d) is larger than the frequency domain image.", source.getWidth(), source.getHeight(), m_width, m_height);
        if (source.getNumberOfChannels() != m_number_of_channels)
            throw Exception("The given image has a different number of channels than the frequency domain image.");
        
        if (number_of_threads > 1)
        {
            fftw_init_threads();
            fftw_plan_with_nthreads(number_of_threads);
        }
        padding_image = new double[m_width * m_height];
        if (m_width > source.getWidth())
            padding_x0 = (m_width - source.getWidth()) / 2 + (source.getWidth() % 2);
        else padding_x0 = 0;
        if (m_height > source.getHeight())
            padding_y0 = (m_height - source.getHeight()) / 2 + (source.getHeight() % 2);
        else padding_y0 = 0;
        padding_x1 = padding_x0 + source.getWidth();
        padding_y1 = padding_y0 + source.getHeight();
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            unsigned int y;
            
            padding_ptr = padding_image;
            for (y = 0; y < padding_y0; ++y)
                for (unsigned int x = 0; x < m_width; ++x, ++padding_ptr)
                    *padding_ptr = 0;
            for (; y < padding_y1; ++y)
            {
                const T * source_ptr = source.get(y - padding_y0, c);
                unsigned int x;
                
                for (x = 0; x < padding_x0; ++x, ++padding_ptr)
                    *padding_ptr = 0;
                for (; x < padding_x1; ++x, ++padding_ptr, ++source_ptr)
                    *padding_ptr = (double)*source_ptr;
                for (; x < m_width; ++x, ++padding_ptr)
                    *padding_ptr = 0;
            }
            for (; y < m_height; ++y)
                for (unsigned int x = 0; x < m_width; ++x, ++padding_ptr)
                    *padding_ptr = 0;
            
            // FFTW_MEASURE -> Search the best parameters.
            // FFTW_ESTIMATE -> Uses a reasonably good parameters but, they may not be the best.
            fftw_plan plan  = fftw_plan_dft_r2c_2d(m_height, m_width, padding_image, m_fft_image[c], FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);
            fftw_cleanup();
        }
        if (number_of_threads > 1) fftw_cleanup_threads();
        delete [] padding_image;
    }
    
    template <template <class> class IMAGE, class T>
    void ImageFFT::getImage(IMAGE<T> &destination, unsigned int number_of_threads) const
    {
        double * double_image, * double_ptr;
        if ((destination.getWidth() != m_width) || (destination.getHeight() != m_height) || (destination.getNumberOfChannels() != m_number_of_channels))
            throw Exception("The geometry of the given image (%dx%dx%d) differs from the expected image (%dx%dx%d).", destination.getWidth(), destination.getHeight(), destination.getNumberOfChannels(), m_width, m_height, m_number_of_channels);
        
        if (number_of_threads > 1)
        {
            fftw_init_threads();
            fftw_plan_with_nthreads(number_of_threads);
        }
        double_image = new double[m_width * m_height];
        const double normalization_factor = (double)(m_width * m_height);
        for (unsigned int c = 0; c < m_number_of_channels; ++c)
        {
            fftw_plan plan  = fftw_plan_dft_c2r_2d(m_height, m_width, m_fft_image[c], double_image, FFTW_ESTIMATE);
            fftw_execute(plan);
            fftw_destroy_plan(plan);
            fftw_cleanup();
            
            double_ptr = double_image;
            for (unsigned int y = 0; y < m_height; ++y)
            {
                T * __restrict__ destination_ptr = destination.get(y, c);
                for (unsigned int x = 0; x < m_width; ++x, ++double_ptr, ++destination_ptr)
                    *destination_ptr = (T)(*double_ptr / normalization_factor);
            }
        }
        if (number_of_threads > 1) fftw_cleanup_threads();
        delete [] double_image;
    }
#endif
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE GAUSSIAN FILTERING             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    class Gaussian2DFilterKernel
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default kernel.
        Gaussian2DFilterKernel(void);
        /** Constructor which sets the Gaussian kernel values.
         *  \param[in] sigma_x sigma of the Gaussian kernel.
         *  \param[in] derivative_x derivative degree of the Gaussian function in the X direction.
         *  \param[in] derivative_y derivative degree of the Gaussian function in the Y direction.
         */
        Gaussian2DFilterKernel(double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45);
        /// Copy constructor.
        Gaussian2DFilterKernel(const Gaussian2DFilterKernel &other);
        /// Destructor.
        ~Gaussian2DFilterKernel(void);
        /// Assignation operator.
        Gaussian2DFilterKernel& operator=(const Gaussian2DFilterKernel &other);
        /** Function which sets the Gaussian kernel values.
         *  \param[in] sigma sigma of the Gaussian kernel.
         *  \param[in] derivative_x derivative degree of the Gaussian function in the X direction.
         *  \param[in] derivative_y derivative degree of the Gaussian function in the Y direction.
         */
        void set(double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Number of values of the Gaussian kernel.
        inline unsigned int getNumberOfValues(void) const { return m_number_of_values; }
        /// Returns a constant pointer to the horizontal values of the Gaussian kernel.
        inline const double* getHorizontal(void) const { return m_horizontal; }
        /// Returns a constant reference to the index-th value of the horizontal values of the Gaussian kernel.
        inline const double& getHorizontal(unsigned int index) const { return m_horizontal[index]; }
        /// Returns a reference to the index-th value of the horizontal values of the Gaussian kernel.
        inline double& getHorizontal(unsigned int index) { return m_horizontal[index]; }
        /// Returns a constant pointer to the vertical values of the Gaussian kernel.
        inline const double* getVertical(void) const { return m_vertical; }
        /// Returns a constant reference to the index-th value of the vertical values of the Gaussian kernel.
        inline const double& getVertical(unsigned int index) const { return m_vertical[index]; }
        /// Returns a reference to the index-th value of the vertical values of the Gaussian kernel.
        inline double& getVertical(unsigned int index) { return m_vertical[index]; }
        /// Returns the degree of the Gaussian kernel derivative in the X direction.
        inline unsigned int getDegreeX(void) const { return m_degree_x; }
        /// Returns the degree of the Gaussian kernel derivative in the Y direction.
        inline unsigned int getDegreeY(void) const { return m_degree_y; }
        /// Returns a boolean which is true when the Gaussian kernel is rotated 45 degrees.
        inline bool getRotate45(void) const { return m_rotate_45; }
        /** Applies the X-direction Gaussian kernel derivative to the given array.
         *  \param[in] input array with the input values.
         *  \param[out] output array with the resulting filtered values.
         *  \param[in] size size of the input and output arrays.
         */
        template <class T, class U>
        inline void filterHorizontal(const T * __restrict__ input, U * __restrict__ output, unsigned int size) const { convolution(input, m_horizontal, output, size); }
        /** Applies the X-direction Gaussian kernel derivative to the given vector.
         *  \param[in] input input vector.
         *  \param[out] output filtered output vector.
         */
        template <template <class, class> class VECTORA, class T1, class N1, template <class, class> class VECTORB, class T2, class N2>
        inline void filterHorizontal(const VECTORA<T1, N1> &input, VECTORB<T2, N2> &output) const
        {
            if (input.size() != output.size()) output.set((N2)input.size());
            filterHorizontal(input.getData(), output.getData(), (unsigned int)input.size());
        }
        /** Applies the Y-direction Gaussian kernel derivative to the given array.
         *  \param[in] input array with the input values.
         *  \param[out] output array with the resulting filtered values.
         *  \param[in] size size of the input and output arrays.
         */
        template <class T, class U>
        inline void filterVertical(const T * __restrict__ input, U * __restrict__ output, unsigned int size) const  { convolution(input, m_vertical, output, size); }
        /** Applies the Y-direction Gaussian kernel derivative to the given vector.
         *  \param[in] input input vector.
         *  \param[out] output filtered output vector.
         */
        template <template <class, class> class VECTORA, class T1, class N1, template <class, class> class VECTORB, class T2, class N2>
        inline void filterVertical(const VECTORA<T1, N1> &input, VECTORB<T2, N2> &output) const
        {
            if (input.size() != output.size()) output.set((N2)input.size());
            filterVertical(input.getData(), output.getData(), (unsigned int)input.size());
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Convert the 2D Gaussian kernel into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the 2D Gaussian kernel information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        template <class T, class U>
        void convolution(const T * __restrict__ input, const double * __restrict__ gaussian, U * __restrict__ output, unsigned int size) const;
        /// Sigma of the Gaussian kernel.
        double m_sigma;
        /// Array with the Gaussian kernel values for the horizontal part of the kernel.
        double *m_horizontal;
        /// Array with the Gaussian kernel values for the vertical part of the kernel.
        double *m_vertical;
        /// Number of values.
        unsigned int m_number_of_values;
        /// Degree of the derivative in the X-direction.
        unsigned int m_degree_x;
        /// Degree of the derivative in the Y-direction.
        unsigned int m_degree_y;
        /// Flag which enables to rotate 45 degrees the Gaussian kernel.
        bool m_rotate_45;
    };
    
    template <class T, class U>
    void Gaussian2DFilterKernel::convolution(const T * __restrict__ input, const double * __restrict__ gaussian, U * __restrict__ output, unsigned int size) const
    {
        const int half_size = (int)m_number_of_values / 2;
        
        if (size > m_number_of_values)
        {
            double value;
            int index;
            
            for (index = 0; index < half_size; ++index)
            {
                value = 0;
                for (int k = -half_size, j = 0; j < (int)m_number_of_values; ++k, ++j)
                    value += gaussian[j] * (double)input[srvAbs(k + index)];
                output[index] = (U)value;
            }
            for (; index < (int)size - half_size; ++index)
            {
                value = 0;
                for (int k = -half_size, j = 0; j < (int)m_number_of_values; ++k, ++j)
                    value += gaussian[j] * (double)input[k + index];
                output[index] = (U)value;
            }
            for (; index < (int)size; ++index)
            {
                value = 0;
                for (int k = -half_size, j = 0; j < (int)m_number_of_values; ++k, ++j)
                {
                    int current_location = k + index;
                    if (current_location >= (int)size)
                        current_location = 2 * size - current_location - 1;
                    value += gaussian[j] * (double)input[current_location];
                }
                output[index] = (U)value;
            }
        }
        else
        {
            double value, weight;
            
            for (int index = 0; index < (int)size; ++index)
            {
                value = weight = 0.0;
                for (int k = -half_size, j = 0; j < (int)m_number_of_values; ++k, ++j)
                {
                    int current_position = index + k;
                    if ((current_position >= 0) && (current_position < (int)size))
                    {
                        value += gaussian[j] * (double)input[current_position];
                        weight += srvAbs(gaussian[j]);
                    }
                }
                output[index] = (U)((weight > 0)?value / weight:0);
            }
        }
    }
    
    /** Filters the input image with a Gaussian filter.
     *  \param[in] source_image original image.
     *  \param[in] kernel Gaussian kernel used to filter the image.
     *  \param[out] destination_image resulting filtered image. 
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageProcessing().
     */
    template <template <class> class SOURCE_IMAGE, class T1, template <class> class DESTINATION_IMAGE, class T2>
    void GaussianFilter(const SOURCE_IMAGE<T1> &source_image, const Gaussian2DFilterKernel &kernel, DESTINATION_IMAGE<T2> &destination_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        double *intermid_image;
        
        // 1) Check the geometry of both images.
        checkGeometry(source_image, destination_image);
        
        // 2) If the images is not empty.
        if ((source_image.getWidth() > kernel.getNumberOfValues()) &&  (source_image.getHeight() > kernel.getNumberOfValues()))
        {
            const int half_kernel = kernel.getNumberOfValues() / 2;
            const int width = (int)source_image.getWidth();
            const int height = (int)source_image.getHeight();
            
            // 3.1) Allocate memory for the intermediate image where the result of the horizontal kernel filter is going to be stored.
            intermid_image = new double[width * height];
            if (kernel.getRotate45())
            {
                const unsigned int max_diagonal = srvMin(width, height);
                
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
                {
                    // Filter in the X-direction of the rotated image ...............................................................................
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        VectorDense<double> current_row(max_diagonal), filtered_row(max_diagonal);
                        unsigned int length, current_index;
                        
                        // Top row of the image.
                        current_index = thread_identifier;
                        for (unsigned int x = 0; x < (unsigned int)width; ++x, ++current_index)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                length = srvMin<unsigned int>(height, width - x);
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = *source_image.get(x + i, i, channel);
                                // Filter the current diagonal row.
                                kernel.filterHorizontal(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) intermid_image[(x + i) + i * width] = filtered_row[i];
                            }
                            
                        }
                        // Left column of the image.
                        for (unsigned int y = 1; y < (unsigned int)height; ++y, ++current_index)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                length = srvMin<unsigned int>(width, height - y);
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = *source_image.get(i, y + i, channel);
                                // Filter the current diagonal row.
                                kernel.filterHorizontal(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) intermid_image[i + (y + i) * width] = filtered_row[i];
                            }
                        }
                    }
                    
                    // Filter in the Y-direction of the rotated image ...............................................................................
                    // Top row of the image.
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        VectorDense<double> current_row(max_diagonal), filtered_row(max_diagonal);
                        unsigned int length, current_index;
                        
                        current_index = thread_identifier;
                        for (unsigned int x = 0; x < (unsigned int)width; ++x)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                length = srvMin<unsigned int>(height, x + 1);
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = intermid_image[(x - i) + i * width];
                                // Filter the current diagonal row.
                                kernel.filterVertical(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) *destination_image.get(x - i, i, channel) = (T2)filtered_row[i];
                            }
                        }
                        // Right column of the image.
                        for (unsigned int y = 1; y < (unsigned int)height; ++y)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                length = srvMin<unsigned int>(width, height - y);
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = intermid_image[width - i - 1 + (y + i) * width];
                                // Filter the current diagonal row.
                                kernel.filterVertical(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) *destination_image.get(width - i - 1, y + i, channel) = (T2)filtered_row[i];
                            }
                        }
                    }
                }
            }
            else
            {
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel) // For each channel of the image...
                {
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const int thread_identifier = omp_get_thread_num();
                        // 3.2) Calculate the result of filtering the image by the horizontal Gaussian kernel.
                        for (int y = thread_identifier; y < height; y += number_of_threads)
                        {
                            const T1 * __restrict__ source_ptr = source_image.get(y, channel);
                            double * __restrict__ column = intermid_image + y;
                            int x;
                            
                            // 3.2.1) Calculate the correlation of the image by the Gaussian kernel in the where the pixels fall outside for the initial positions
                            //        of the image. This is an special case as the kernel correlation is not well defined.
                            for (x = 0; x < half_kernel; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                    value += (double)source_ptr[srvAbs(x + k)] * kernel.getHorizontal(j);
                                *column = value;
                                column += height;
                            }
                            // 3.2.2) Calculate the correlation of the image by the Gaussian kernel in the general case.
                            for (; x < width - half_kernel; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                    value += (double)source_ptr[x + k] * kernel.getHorizontal(j);
                                *column = value;
                                column += height;
                            }
                            // 3.2.3) Calculate the correlation of the image by the Gaussian kernel in the where the pixels fall outside for the final positions 
                            //        of the image. This is an special case as the kernel correlation is not well defined.
                            for (; x < width; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                {
                                    int position = x + k;
                                    if (position >= width) position = 2 * width - position - 1;
                                    value += (double)source_ptr[position] * kernel.getHorizontal(j);
                                }
                                *column = value;
                                column += height;
                            }
                        }
                    }
                    
                    // 3.3) Calculate the result of filtering the image by the vertical Gaussian kernel.
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const int thread_identifier = omp_get_thread_num();
                        for (int y = thread_identifier; y < width; y += number_of_threads) // The intermediate image is transposed.
                        {
                            const double * __restrict__ column = intermid_image + y * height;
                            int x;
                            
                            // 3.2.1) Calculate the correlation of the image by the Gaussian kernel in the where the pixels fall outside for the initial positions
                            //        of the image. This is an special case as the kernel correlation is not well defined.
                            for (x = 0; x < half_kernel; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                    value += column[srvAbs(x + k)] * kernel.getVertical(j);
                                *destination_image.get(y, x, channel) = (T2)value;
                            }
                            // 3.2.2) Calculate the correlation of the image by the Gaussian kernel in the general case.
                            for (; x < height - half_kernel; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                    value += column[x + k] * kernel.getVertical(j);
                                *destination_image.get(y, x, channel) = (T2)value;
                            }
                            // 3.2.3) Calculate the correlation of the image by the Gaussian kernel in the where the pixels fall outside for the final positions 
                            //        of the image. This is an special case as the kernel correlation is not well defined.
                            for (; x < height; ++x)
                            {
                                double value;
                                
                                value = 0;
                                for (int k = -half_kernel, j = 0; j < (int)kernel.getNumberOfValues(); ++j, ++k)
                                {
                                    int position = x + k;
                                    if (position >= height) position = 2 * height - position - 1;
                                    value += column[position] * kernel.getVertical(j);
                                }
                                *destination_image.get(y, x, channel) = (T2)value;
                            }
                        }
                    }
                }
            }
            
            // 4) Free the allocated memory.
            delete [] intermid_image;
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < destination_image.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < destination_image.getHeight(); y += number_of_threads)
                    {
                        T2 * __restrict__ output_ptr = destination_image.get(y, c);
                        for (unsigned int x = 0; x < destination_image.getWidth(); ++x) output_ptr[x] = 0;
                    }
                }
            }
        }
    }
    
    /** Filters the input image with a Gaussian filter.
     *  \param[in] source_image original image.
     *  \param[in] kernel Gaussian kernel used to filter the image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageProcessing().
     *  \returns resulting filtered image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> GaussianFilter(const IMAGE<T> &source_image, const Gaussian2DFilterKernel &kernel, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        Image<T> destination_image(source_image.getWidth(), source_image.getHeight(), source_image.getNumberOfChannels());
        GaussianFilter(source_image, kernel, destination_image, number_of_threads);
        return destination_image;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE GRADIENT FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Calculates the gradient in the X and Y direction for the specified image. This function can down-sample the resulting gradient images.
     *  \param[in] integral_image integral image of the original image.
     *  \param[in] size size of the square patch used to approximate the Gaussian kernel.
     *  \param[out] gradient_dx resulting image or sub-image with the gradient in the X direction.
     *  \param[out] gradient_dy resulting image or sub-image with the gradient in the Y direction.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageProcessing().
     */
    template <class TINTEGRAL, template <class> class IMAGE_DX, class TDX, template <class> class IMAGE_DY, class TDY>
    void ImageGradient(const IntegralImage<TINTEGRAL> &integral_image, unsigned int size, IMAGE_DX<TDX> &gradient_dx, IMAGE_DY<TDY> &gradient_dy, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        // Initialize constant variables and check image geometry.
        if ((gradient_dx.getWidth() != gradient_dy.getWidth()) || (gradient_dx.getHeight() != gradient_dy.getHeight())) throw Exception("Both gradient images must have the same geometry.");
        if (gradient_dx.getNumberOfChannels() != gradient_dy.getNumberOfChannels()) throw Exception("Both gradient images must have the same number of channels");
        if (integral_image.getNumberOfChannels() != gradient_dx.getNumberOfChannels()) throw Exception("The integral image and gradient images must have the same amount of channels.");
        const unsigned int width = integral_image.getWidth() - 1;
        const unsigned int height = integral_image.getHeight() - 1;
        
        if (size % 2 == 1) ++size; // Make the size even.
        const unsigned int size_half = size / 2;
        const unsigned int size2 = (size * size) / 2;
        const unsigned int step_x = (unsigned int)ceil((double)width / (double)gradient_dx.getWidth());
        const unsigned int step_y = (unsigned int)ceil((double)height / (double)gradient_dx.getHeight());
        
        if (step_x == 0) throw Exception("The step in the X-direction is zero because the integral image width is smaller than the gradient images width.");
        if (step_y == 0) throw Exception("The step in the Y-direction is zero because the integral image height is smaller than the gradient images height.");
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            const int thread_identifier = omp_get_thread_num();
            
            for (unsigned int c = 0; c < integral_image.getNumberOfChannels(); ++c)
            {
                unsigned int y = step_y * thread_identifier;
                unsigned int ry = thread_identifier;
                
                // Set to zero the first rows of the resulting image.
                for (; y < size_half; y += step_y * number_of_threads, ry += number_of_threads)
                {
                    TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                    TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                    for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                    {
                        *dx_ptr = 0;
                        *dy_ptr = 0;
                        ++dx_ptr;
                        ++dy_ptr;
                    }
                }
                
                // Calculate the Gaussian filter approximate for the middle image rows.
                for (; y < height - size_half; y += step_y * number_of_threads, ry += number_of_threads)
                {
                    const TINTEGRAL * __restrict__ top_integral_ptr = integral_image.getData(c) + (y - size_half) * integral_image.getWidth();
                    const TINTEGRAL * __restrict__ middle_integral_ptr = integral_image.getData(c) + y * integral_image.getWidth();
                    const TINTEGRAL * __restrict__ bottom_integral_ptr = integral_image.getData(c) + (y + size_half) * integral_image.getWidth();
                    
                    TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                    TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                    unsigned int x = 0, rx = 0;
                    
                    // First half columns set to zero.
                    for (; x < size_half; x += step_x, ++rx)
                    {
                        *dx_ptr = 0;
                        *dy_ptr = 0;
                        ++dx_ptr;
                        ++dy_ptr;
                    }
                    
                    // Middle columns where the gradient is well defined.
                    for (; x < width - size_half; x += step_x, ++rx)
                    {
                        TINTEGRAL area = top_integral_ptr[0] + bottom_integral_ptr[size] - top_integral_ptr[size] - bottom_integral_ptr[0];
                        TINTEGRAL dx = top_integral_ptr[0] + bottom_integral_ptr[size_half] - top_integral_ptr[size_half] - bottom_integral_ptr[0];
                        TINTEGRAL dy = middle_integral_ptr[0] + bottom_integral_ptr[size] - middle_integral_ptr[size] - bottom_integral_ptr[0];
                        *dx_ptr = (TDX)((-(long)area + 2 * (long)dx) / size2);
                        *dy_ptr = (TDY)(( (long)area - 2 * (long)dy) / size2);
                        
                        top_integral_ptr += step_x;
                        bottom_integral_ptr += step_x;
                        middle_integral_ptr += step_x;
                        ++dx_ptr;
                        ++dy_ptr;
                    }
                    
                    // Last half columns set to zero.
                    for (; rx < gradient_dx.getWidth(); ++rx)
                    {
                        *dx_ptr = 0;
                        *dy_ptr = 0;
                        ++dx_ptr;
                        ++dy_ptr;
                    }
                }
                
                // Set to zero the last rows of the resulting image.
                for (; y < height; y += step_y * number_of_threads, ry += number_of_threads)
                {
                    TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                    TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                    
                    for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                    {
                        *dx_ptr = 0;
                        *dy_ptr = 0;
                        ++dx_ptr;
                        ++dy_ptr;
                    }
                }
            }
        }
    }
    
    /** Calculates the gradient in the X and Y direction for the specified image using 45 degrees rotated Haar-like features
     *  \param[in] integral_image rotated integral image of the original image.
     *  \param[in] size size of the square patch used to approximate the Gaussian kernel.
     *  \param[out] gradient_dx resulting image or sub-image with the gradient in the X direction.
     *  \param[out] gradient_dy resulting image or sub-image with the gradient in the Y direction.
     *  \param[in] sqrt2 Flag which indicates that the integral image is resized by sqrt(2), so that, regions have the same area as non-rotated regions. By default set to true.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageProcessing().
     */
    template <class TINTEGRAL, template <class> class IMAGE_DX, class TDX, template <class> class IMAGE_DY, class TDY>
    void ImageGradient(const IntegralImageRotated<TINTEGRAL> &integral_image, unsigned int size, IMAGE_DX<TDX> &gradient_dx, IMAGE_DY<TDY> &gradient_dy, bool sqrt2 = true, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        // Initialize constant variables and check image geometry.
        if ((gradient_dx.getWidth() != gradient_dy.getWidth()) || (gradient_dx.getHeight() != gradient_dy.getHeight())) throw Exception("Both gradient images must have the same geometry.");
        if (gradient_dx.getNumberOfChannels() != gradient_dy.getNumberOfChannels()) throw Exception("Both gradient images must have the same number of channels");
        if (integral_image.getNumberOfChannels() != gradient_dx.getNumberOfChannels()) throw Exception("The integral image and gradient images must have the same amount of channels.");
        
        if (sqrt2)
        {
            const double const_sqrt2 = 1.4142135623730951;
            const unsigned int width = (unsigned int)((integral_image.getWidth() - 2) / const_sqrt2);
            const unsigned int height = (unsigned int)((integral_image.getHeight() - 1) / const_sqrt2);
            if (size % 2 == 1) ++size; // Make the size even.
            const unsigned int size_half = size / 2;
            const unsigned int size_twice = size * 2;
            const unsigned int size2 = size * size;
            const unsigned int border = (unsigned int)ceil((double)size / const_sqrt2);
            
            const unsigned int step_x = (unsigned int)ceil((double)width / (double)gradient_dx.getWidth());
            const unsigned int step_y = (unsigned int)ceil((double)height / (double)gradient_dy.getHeight());
            if (step_x == 0) throw Exception("The step in the X-direction is zero because the integral image width is smaller than the gradient images width.");
            if (step_y == 0) throw Exception("The step in the Y-direction is zero because the integral image height is smaller than the gradient images height.");
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                const int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < integral_image.getNumberOfChannels(); ++c)
                {
                    unsigned int y = step_y * thread_identifier;
                    unsigned int ry = thread_identifier;
                    
                    // Set to zero the first rows of the resulting image.
                    for (; y < border; y += step_y * number_of_threads, ry += number_of_threads)
                    {
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                    
                    // Calculate the Gaussian filter approximate for the middle image rows.
                    for (; y < height - border; y += step_y * number_of_threads, ry += number_of_threads)
                    {
                        unsigned int current_y = (unsigned int)floor((double)y * const_sqrt2);
                        const TINTEGRAL * __restrict__ top_integral_ptr       = integral_image.getData(c) + (current_y - size)      * integral_image.getWidth() + size;
                        const TINTEGRAL * __restrict__ midtop_integral_ptr    = integral_image.getData(c) + (current_y - size_half) * integral_image.getWidth() + size_half;
                        const TINTEGRAL * __restrict__ middle_integral_ptr    = integral_image.getData(c) +  current_y              * integral_image.getWidth();
                        const TINTEGRAL * __restrict__ midbottom_integral_ptr = integral_image.getData(c) + (current_y + size_half) * integral_image.getWidth() + size_half;
                        const TINTEGRAL * __restrict__ bottom_integral_ptr    = integral_image.getData(c) + (current_y + size)      * integral_image.getWidth() + size;
                        
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        const double real_step_x = (double)step_x * const_sqrt2;
                        unsigned int x, rx, uint_x_location = 0, current_x_step;
                        double real_x_location = 0;
                        
                        // First half columns set to zero.
                        for (x = 0, rx = 0; x < border; x += step_x, ++rx)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                        
                        // Middle columns where the gradient is well defined.
                        for (; x < width - border; x += step_x, ++rx)
                        {
                            TINTEGRAL area = top_integral_ptr[0] + bottom_integral_ptr[0] - middle_integral_ptr[0] - middle_integral_ptr[size_twice];
                            TINTEGRAL dx = bottom_integral_ptr[0] + midtop_integral_ptr[size] - midbottom_integral_ptr[0] - middle_integral_ptr[size_twice];
                            TINTEGRAL dy = top_integral_ptr[0] + midbottom_integral_ptr[size] - midtop_integral_ptr[0] - middle_integral_ptr[size_twice];
                            *dx_ptr = (TDX)(((long)area - 2 * (long)dx) / size2);
                            *dy_ptr = (TDY)(((long)area - 2 * (long)dy) / size2);
                            
                            real_x_location += real_step_x;
                            current_x_step = (unsigned int)real_x_location - uint_x_location;
                            uint_x_location = (unsigned int)real_x_location;
                            top_integral_ptr += current_x_step;
                            midtop_integral_ptr += current_x_step;
                            middle_integral_ptr += current_x_step;
                            midbottom_integral_ptr += current_x_step;
                            bottom_integral_ptr += current_x_step;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                        
                        // Last half columns set to zero.
                        for (; rx < gradient_dx.getWidth(); ++rx)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                    
                    // Set to zero the last rows of the resulting image.
                    for (; ry < gradient_dx.getHeight(); ry += number_of_threads)
                    {
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        
                        for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                }
            }
        }
        else
        {
            const unsigned int width = integral_image.getWidth() - 2;
            const unsigned int height = integral_image.getHeight() - 1;
            if (size % 2 == 1) ++size; // Make the size even.
            const unsigned int size_half = size / 2;
            const unsigned int size_twice = size * 2;
            const unsigned int size2 = size * size;
            
            const unsigned int step_x = (unsigned int)ceil((double)width / (double)gradient_dx.getWidth());
            const unsigned int step_y = (unsigned int)ceil((double)height / (double)gradient_dy.getHeight());
            if (step_x == 0) throw Exception("The step in the X-direction is zero because the integral image width is smaller than the gradient images width.");
            if (step_y == 0) throw Exception("The step in the Y-direction is zero because the integral image height is smaller than the gradient images height.");
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                const int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < integral_image.getNumberOfChannels(); ++c)
                {
                    unsigned int y = step_y * thread_identifier;
                    unsigned int ry = thread_identifier;
                    
                    // Set to zero the first rows of the resulting image.
                    for (; y < size; y += step_y * number_of_threads, ry += number_of_threads)
                    {
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                    
                    // Calculate the Gaussian filter approximate for the middle image rows.
                    for (; y < height - size; y += step_y * number_of_threads, ry += number_of_threads)
                    {
                        const TINTEGRAL * __restrict__ top_integral_ptr       = integral_image.getData(c) + (y - size)      * integral_image.getWidth() + size;
                        const TINTEGRAL * __restrict__ midtop_integral_ptr    = integral_image.getData(c) + (y - size_half) * integral_image.getWidth() + size_half;
                        const TINTEGRAL * __restrict__ middle_integral_ptr    = integral_image.getData(c) +  y              * integral_image.getWidth();
                        const TINTEGRAL * __restrict__ midbottom_integral_ptr = integral_image.getData(c) + (y + size_half) * integral_image.getWidth() + size_half;
                        const TINTEGRAL * __restrict__ bottom_integral_ptr    = integral_image.getData(c) + (y + size)      * integral_image.getWidth() + size;
                        
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        unsigned int x, rx;
                        
                        // First half columns set to zero.
                        for (x = 0, rx = 0; x < size; x += step_x, ++rx)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                        
                        // Middle columns where the gradient is well defined.
                        for (; x < width - size; x += step_x, ++rx)
                        {
                            TINTEGRAL area = top_integral_ptr[0] + bottom_integral_ptr[0] - middle_integral_ptr[0] - middle_integral_ptr[size_twice];
                            TINTEGRAL dx = bottom_integral_ptr[0] + midtop_integral_ptr[size] - midbottom_integral_ptr[0] - middle_integral_ptr[size_twice];
                            TINTEGRAL dy = top_integral_ptr[0] + midbottom_integral_ptr[size] - midtop_integral_ptr[0] - middle_integral_ptr[size_twice];
                            *dx_ptr = (TDX)(((long)area - 2 * (long)dx) / size2);
                            *dy_ptr = (TDY)(((long)area - 2 * (long)dy) / size2);
                            
                            top_integral_ptr += step_x;
                            midtop_integral_ptr += step_x;
                            middle_integral_ptr += step_x;
                            midbottom_integral_ptr += step_x;
                            bottom_integral_ptr += step_x;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                        
                        // Last half columns set to zero.
                        for (; rx < gradient_dx.getWidth(); ++rx)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                    
                    // Set to zero the last rows of the resulting image.
                    for (; ry < gradient_dx.getHeight(); ry += number_of_threads)
                    {
                        TDX * __restrict__ dx_ptr = gradient_dx.get(ry, c);
                        TDY * __restrict__ dy_ptr = gradient_dy.get(ry, c);
                        
                        for (unsigned int x = 0; x < gradient_dx.getWidth(); ++x)
                        {
                            *dx_ptr = 0;
                            *dy_ptr = 0;
                            ++dx_ptr;
                            ++dy_ptr;
                        }
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GAUSSIAN FILTERING WITH RECURSIVE    |
    //                   | FILTERS                              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Information for the 2D recursive kernel function.
    class Gaussian2DFilterRecursiveKernel
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        Gaussian2DFilterRecursiveKernel(void);
        /** Constructor where the parameters of the recursive Gaussian kernel are given as parameters.
         *  \param[in] method recursive algorithm used to approximate the Gaussian kernel.
         *  \param[in] sigma sigma of the Gaussian kernel.
         *  \param[in] derivative_x derivative of the Gaussian kernel in the X direction.
         *  \param[in] derivative_y derivative of the Gaussian kernel in the Y direction.
         *  \param[in] rotate_45 creates a 45 degree rotated Gaussian kernel.
         */
        Gaussian2DFilterRecursiveKernel(RECURSIVE_GAUSSIAN_METHOD method, double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45);
        /// Copy constructor.
        Gaussian2DFilterRecursiveKernel(const Gaussian2DFilterRecursiveKernel &other);
        /// Destructor.
        ~Gaussian2DFilterRecursiveKernel(void);
        /// Assignation operator.
        Gaussian2DFilterRecursiveKernel& operator=(const Gaussian2DFilterRecursiveKernel &other);
        /** This function set the parameters of the recursive Gaussian kernel.
         *  \param[in] method recursive algorithm used to approximate the Gaussian kernel.
         *  \param[in] sigma sigma of the Gaussian kernel.
         *  \param[in] derivative_x derivative of the Gaussian kernel in the X direction.
         *  \param[in] derivative_y derivative of the Gaussian kernel in the Y direction.
         *  \param[in] rotate_45 creates a 45 degree rotated Gaussian kernel.
         */
        void set(RECURSIVE_GAUSSIAN_METHOD method, double sigma, unsigned int derivative_x, unsigned int derivative_y, bool rotate_45);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the recursive method used to approximate the Gaussian kernel.
        inline RECURSIVE_GAUSSIAN_METHOD getRecursiveMethod(void) const { return m_method; }
        /// Returns the degree of the Gaussian kernel derivative in the X-direction.
        inline unsigned int getDerivativeX(void) const { return m_derivative_x; }
        /// Returns the degree of the Gaussian kernel derivative in the Y-direction.
        inline unsigned int getDerivativeY(void) const { return m_derivative_y; }
        /// Returns the q-factor corresponding to the sigma of the Gaussian kernel in the X-direction.
        inline double getQFactorX(void) const { return m_q_x; }
        /// Returns the q-factor corresponding to the sigma of the Gaussian kernel in the Y-direction.
        inline double getQFactorY(void) const { return m_q_y; }
        /// Returns the weight applied to the X-direction Gaussian filter so that its "energy" is 1 for the unitary impulse.
        inline double getWeightX(void) const { return m_weight_x; }
        /// Returns the weight applied to the Y-direction Gaussian filter so that its "energy" is 1 for the unitary impulse.
        inline double getWeightY(void) const { return m_weight_y; }
        /// Returns the sigma of the Gaussian function.
        inline double getSigma(void) const { return m_sigma; }
        /// Returns the coefficients of the recursive Gaussian kernel in the X-direction.
        inline const double* getCoefficientsX(void) const { return m_coefficients_x; }
        /// Returns the coefficients of the recursive Gaussian kernel in the Y-direction.
        inline const double* getCoefficientsY(void) const { return m_coefficients_y; }
        /// Returns the number of coefficients of the recursive Gaussian kernel.
        inline unsigned int getNumberOfCoefficients(void) const { return m_number_of_coefficients; }
        /// Returns the flag which indicates if the Gaussian kernel is rotated 45 degrees.
        inline bool getRotate45(void) const { return m_rotate_45; }
        /** Applies the X-direction Gaussian kernel derivative to the given array.
         *  \param[in] input array with the input values.
         *  \param[out] output array with the resulting filtered values.
         *  \param[in] size size of the input and output arrays.
         */
        template <class T, class U>
        inline void filterHorizontal(const T * __restrict__ input, U * __restrict__ output, unsigned int size) const { convolution(input, m_coefficients_x, m_derivative_x, m_weight_x, output, size); }
        /** Applies the X-direction Gaussian kernel derivative to the given vector.
         *  \param[in] input input vector.
         *  \param[out] output filtered output vector.
         */
        template <template <class, class> class VECTORA, class T1, class N1, template <class, class> class VECTORB, class T2, class N2>
        inline void filterHorizontal(const VECTORA<T1, N1> &input, VECTORB<T2, N2> &output) const
        {
            if (input.size() != output.size()) output.set((N2)input.size());
            filterHorizontal(input.getData(), output.getData(), (unsigned int)input.size());
        }
        /** Applies the Y-direction Gaussian kernel derivative to the given array.
         *  \param[in] input array with the input values.
         *  \param[out] output array with the resulting filtered values.
         *  \param[in] size size of the input and output arrays.
         */
        template <class T, class U>
        inline void filterVertical(const T * __restrict__ input, U * __restrict__ output, unsigned int size) const  { convolution(input, m_coefficients_y, m_derivative_y, m_weight_y, output, size); }
        /** Applies the Y-direction Gaussian kernel derivative to the given vector.
         *  \param[in] input input vector.
         *  \param[out] output filtered output vector.
         */
        template <template <class, class> class VECTORA, class T1, class N1, template <class, class> class VECTORB, class T2, class N2>
        inline void filterVertical(const VECTORA<T1, N1> &input, VECTORB<T2, N2> &output) const
        {
            if (input.size() != output.size()) output.set((N2)input.size());
            filterVertical(input.getData(), output.getData(), (unsigned int)input.size());
        }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the recursive Gaussian kernel information from an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the recursive Gaussian kernel information from an XML object.
        void convertFromXML(XmlParser &parser);
    private:
        template <class T, class U>
        void convolution(const T * __restrict__ input, const double * __restrict__ coefficients, unsigned int order, double weight, U * __restrict__ output, unsigned int size) const;
        /** This private function sets the coefficients of the recursive filter for the Deriche's algorithm.
         *  \param[in] derivative degree of the derivative of the Gaussian kernel.
         *  \param[in] sigma sigma of the Gaussian kernel.
         *  \param[out] q factor value related to the Gaussian kernel sigma for the recursive filter.
         *  \param[out] weight weight applied to the Gaussian filter so that its "energy" is 1 for the unitary impulse.
         *  \returns an array with the coefficients of the recursive filter.
         */
        double* setDericheCoefficients(unsigned int derivative, double sigma, double &q, double &weight);
        /** This private function sets the coefficients of the recursive filter for the Vliet-Young-Verbeek algorithm.
         *  \param[in] derivative degree of the derivative of the Gaussian kernel.
         *  \param[in] sigma sigma of the Gaussian kernel.
         *  \param[in] order order of the recursive filter (3rd, 4th or 5th order recursive filters).
         *  \param[out] q factor value related to the Gaussian kernel sigma for the recursive filter.
         *  \param[out] weight weight applied to the Gaussian filter so that its "energy" is 1 for the unitary impulse.
         *  \returns an array with the coefficients of the recursive filter.
         */
        double* setVlietYoungVerbeekCoefficients(unsigned int derivative, double sigma, unsigned int order, double &q, double &weight);
        /// Method used by the 
        RECURSIVE_GAUSSIAN_METHOD m_method;
        /// Degree of the Gaussian kernel derivative in the X-direction.
        unsigned int m_derivative_x;
        /// Degree of the Gaussian kernel derivative in the Y-direction.
        unsigned int m_derivative_y;
        /// Value related to the sigma of the recursive Gaussian kernel applied in the X-direction.
        double m_q_x;
        /// Value related to the sigma of the recursive Gaussian kernel applied in the Y-direction.
        double m_q_y;
        /// Weight applied to the X-direction Gaussian filter so that its "energy" is 1 for the unitary impulse.
        double m_weight_x;
        /// Weight applied to the Y-direction Gaussian filter so that its "energy" is 1 for the unitary impulse.
        double m_weight_y;
        /// Sigma of the Gaussian kernel.
        double m_sigma;
        /// Coefficients for the recursive kernel applied in the X-direction.
        double *m_coefficients_x;
        /// Coefficients for the recursive kernel applied in the X-direction.
        double *m_coefficients_y;
        /// Number of coefficients of the recursive kernel.
        unsigned int m_number_of_coefficients;
        /// Flag which enables to rotate 45 degrees the Gaussian kernel.
        bool m_rotate_45;
    };
    
    template <class T, class U>
    void Gaussian2DFilterRecursiveKernel::convolution(const T * __restrict__ input, const double * __restrict__ coefficients, unsigned int order, double weight, U * __restrict__ output, unsigned int size) const
    {
        switch (m_method)
        {
            case DERICHE:
                {
                    double y01, y02, y03, y04, x01, x02, x03, x04;
                    const double sign = (order == 0)?1.0:-1.0;
                    
                    y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                               x03 = 0.0; x02 = 0.0; x01 = 0.0;
                    // 3.2.1) Forward filter.
                    for (int x = 0; x < (int)size; ++x)
                    {
                        double current_x = (double)input[x];
                        output[x] = coefficients[0] * current_x + coefficients[1] * x01 + coefficients[2] * x02 + coefficients[3] * x03
                            - coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6] * y03 - coefficients[7] * y04;
                        
                        y04 = y03; y03 = y02; y02 = y01; y01 = output[x];
                                   x03 = x02; x02 = x01; x01 = current_x;
                    }
                    // 3.2.2) Backward filter.
                    y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                    x04 = 0.0; x03 = 0.0; x02 = 0.0; x01 = 0.0;
                    for (int x = (int)size - 1; x >= 0; --x)
                    {
                        double value = coefficients[8] * x01 + coefficients[9] * x02 + coefficients[10] * x03 + coefficients[11] * x04
                            -coefficients[4] * y01 - coefficients[5] * y02 - coefficients[6] * y03 - coefficients[7] * y04;
                        output[x] = sign * (output[x] + value) / weight;
                        y04 = y03; y03 = y02; y02 = y01; y01 = value;
                        x04 = x03; x03 = x02; x02 = x01; x01 = (double)input[x];
                    }
                }
                break;
            case VLIET_3RD:
            case VLIET_4TH:
            case VLIET_5TH:
                {
                    const double alpha = coefficients[m_number_of_coefficients - 1];
                    const double * b = coefficients;
                    VectorDense<double> v(m_number_of_coefficients - 1, 0);
                    
                    // Forward filter.
                    if (order == 0)
                    {
                        for (int x = 0; x < (int)size; ++x)
                        {
                            double current = input[x] * alpha;
                            for (unsigned int k = 0; k < m_number_of_coefficients - 1; ++k)
                                current -= v[k] * b[k];
                            output[x] = current;
                            
                            for (unsigned int k = m_number_of_coefficients - 2; k > 0; --k)
                                v[k] = v[k - 1];
                            v[0] = current;
                        }
                    }
                    else if (order == 1)
                    {
                        output[0] = 0; output[size - 1] = 0;
                        for (int x = 1; x < (int)size - 1; ++x)
                        {
                            double current = ((double)input[x - 1] - (double)input[x + 1]) * alpha / 2.0;
                            for (unsigned int k = 0; k < m_number_of_coefficients - 1; ++k)
                                current -= v[k] * b[k];
                            output[x] = current;
                            
                            for (unsigned int k = m_number_of_coefficients - 2; k > 0; --k)
                                v[k] = v[k - 1];
                            v[0] = current;
                        }
                    }
                    else
                    {
                        output[0] = 0;
                        for (int x = 1; x < (int)size; ++x)
                        {
                            double current = ((double)input[x] - (double)input[x - 1]) * alpha;
                            for (unsigned int k = 0; k < m_number_of_coefficients - 1; ++k)
                                current -= v[k] * b[k];
                            output[x] = current;
                            
                            for (unsigned int k = m_number_of_coefficients - 2; k > 0; --k)
                                v[k] = v[k - 1];
                            v[0] = current;
                        }
                    }
                    
                    // Backward filter.
                    v.setValue(0);
                    
                    if (order == 2)
                    {
                        double previous = 0.0, aux, current;
                        for (int x = (int)size - 1; x >= 0; --x)
                        {
                            aux = output[x];
                            current = (previous - output[x]) * alpha;
                            previous = aux;
                            for (unsigned int k = 0; k < m_number_of_coefficients - 1; ++k)
                                current -= v[k] * b[k];
                            output[x] = -current / weight;
                            
                            for (unsigned int k = m_number_of_coefficients - 2; k > 0; --k)
                                v[k] = v[k - 1];
                            v[0] = current;
                        }
                    }
                    else
                    {
                        for (int x = (int)size - 1; x >= 0; --x)
                        {
                            double current = output[x] * alpha;
                            for (unsigned int k = 0; k < m_number_of_coefficients - 1; ++k)
                                current -= v[k] * b[k];
                            output[x] = current / weight;
                            
                            for (unsigned int k = m_number_of_coefficients - 2; k > 0; --k)
                                v[k] = v[k - 1];
                            v[0] = current;
                        }
                    }
                }
                break;
            default:
                throw Exception("Unknown method identifier '%d'", (int)m_method);
                break;
        }
    }
    
    /** Filters the input image with a Gaussian filter.
     *  \param[in] source_image original image.
     *  \param[in] kernel recursive Gaussian kernel used to filter the image.
     *  \param[out] destination_image resulting filtered image. 
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageProcessing().
     */
    template <template <class> class SOURCE_IMAGE, class T1, template <class> class DESTINATION_IMAGE, class T2>
    void GaussianFilter(const SOURCE_IMAGE<T1> &source_image, const Gaussian2DFilterRecursiveKernel &kernel, DESTINATION_IMAGE<T2> &destination_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        double *intermid_image;
        
        // 1) Check the geometry of both images.
        checkGeometry(source_image, destination_image);
        
        // 2) If the images is not empty.
        if ((source_image.getWidth() > 2 * kernel.getNumberOfCoefficients()) &&  (source_image.getHeight() > 2 * kernel.getNumberOfCoefficients()))
        {
            const int width = (int)source_image.getWidth();
            const int height = (int)source_image.getHeight();
            
            if (kernel.getRotate45())
            {
                const unsigned int max_diagonal = srvMin(width, height);
                // 3.1) Allocate memory for the intermediate image where the result of the horizontal kernel filter is going to be stored.
                intermid_image = new double[width * height];
                
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
                {
                    // Filter in the X-direction of the rotated image ...............................................................................
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        VectorDense<double> current_row(max_diagonal), filtered_row(max_diagonal);
                        unsigned int length, current_index;
                        
                        // Top row of the image.
                        current_index = thread_identifier;
                        for (unsigned int x = 0; x < (unsigned int)width; ++x, ++current_index)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                if (x + max_diagonal > (unsigned int)width) length = width - x;
                                else length = max_diagonal;
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = *source_image.get(x + i, i, channel);
                                // Filter the current diagonal row.
                                kernel.filterHorizontal(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) intermid_image[(x + i) + i * width] = filtered_row[i];
                            }
                            
                        }
                        // Left column of the image.
                        for (unsigned int y = 1; y < (unsigned int)height; ++y, ++current_index)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                if (y + max_diagonal > (unsigned int)height) length = height - y;
                                else length = max_diagonal;
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = *source_image.get(i, y + i, channel);
                                // Filter the current diagonal row.
                                kernel.filterHorizontal(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) intermid_image[i + (y + i) * width] = filtered_row[i];
                            }
                        }
                    }
                    
                    // Filter in the Y-direction of the rotated image ...............................................................................
                    // Top row of the image.
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        VectorDense<double> current_row(max_diagonal), filtered_row(max_diagonal);
                        unsigned int length, current_index;
                        
                        current_index = thread_identifier;
                        for (unsigned int x = 0; x < (unsigned int)width; ++x)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                if (x < max_diagonal) length = x + 1;
                                else length = max_diagonal;
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = intermid_image[(x - i) + i * width];
                                // Filter the current diagonal row.
                                kernel.filterVertical(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) *destination_image.get(x - i, i, channel) = (T2)filtered_row[i];
                            }
                        }
                        // Right column of the image.
                        for (unsigned int y = 1; y < (unsigned int)height; ++y)
                        {
                            if (current_index % number_of_threads == 0)
                            {
                                // Calculate the length for the current diagonal.
                                if (y + max_diagonal > (unsigned int)height) length = height - y;
                                else length = max_diagonal;
                                // Copy the values of the image for the current diagonal.
                                for (unsigned int i = 0; i < length; ++i) current_row[i] = intermid_image[width - i - 1 + (y + i) * width];
                                // Filter the current diagonal row.
                                kernel.filterVertical(current_row.getData(), filtered_row.getData(), length);
                                // Apply the Gaussian filter in the local vector.
                                for (unsigned int i = 0; i < length; ++i) *destination_image.get(width - i - 1, y + i, channel) = (T2)filtered_row[i];
                            }
                        }
                    }
                }
                
                delete [] intermid_image;
            }
            else
            {
                double **current_line = new double*[number_of_threads];
                for (unsigned int i = 0; i < number_of_threads; ++i) current_line[i] = new double[srvMax<int>(width, height)];
                
                // 3.1) Allocate memory for the intermediate image where the result of the horizontal kernel filter is going to be stored.
                intermid_image = new double[width * height];
                
                if (kernel.getRecursiveMethod() == DERICHE)
                {
                    const double sign_x = (kernel.getDerivativeX() == 0)?1.0:-1.0;
                    const double sign_y = (kernel.getDerivativeY() == 0)?1.0:-1.0;
                    for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel) // For each channel of the image...
                    {
                        // 3.2) Calculate the result of filtering the image by the horizontal Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double y04, y03, y02, y01, x04, x03, x02, x01;
                            const double co00 = kernel.getCoefficientsX()[0];
                            const double co01 = kernel.getCoefficientsX()[1];
                            const double co02 = kernel.getCoefficientsX()[2];
                            const double co03 = kernel.getCoefficientsX()[3];
                            const double co10 = kernel.getCoefficientsX()[4];
                            const double co11 = kernel.getCoefficientsX()[5];
                            const double co12 = kernel.getCoefficientsX()[6];
                            const double co13 = kernel.getCoefficientsX()[7];
                            const double co20 = kernel.getCoefficientsX()[8];
                            const double co21 = kernel.getCoefficientsX()[9];
                            const double co22 = kernel.getCoefficientsX()[10];
                            const double co23 = kernel.getCoefficientsX()[11];
                            
                            for (int y = thread_identifier; y < height; y += number_of_threads)
                            {
                                const T1 * __restrict__ source_ptr = source_image.get(y, channel);
                                double * __restrict__ column = intermid_image + y + width * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                                           x03 = 0.0; x02 = 0.0; x01 = 0.0;
                                
                                // 3.2.1) Forward filter.
                                for (int x = 0; x < width; ++x)
                                {
                                    double current_x = (double)*source_ptr;
                                    *line_ptr = co00 * current_x + co01 * x01 + co02 * x02 + co03 * x03 - co10 * y01 - co11 * y02 - co12 * y03 - co13 * y04;
                                    
                                    y04 = y03; y03 = y02; y02 = y01; y01 = *line_ptr;
                                               x03 = x02; x02 = x01; x01 = current_x;
                                    ++source_ptr;
                                    ++line_ptr;
                                }
                                // 3.2.2) Backward filter.
                                y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                                x04 = 0.0; x03 = 0.0; x02 = 0.0; x01 = 0.0;
                                for (int x = width - 1; x >= 0; --x)
                                {
                                    --line_ptr;
                                    --source_ptr;
                                    column -= height;
                                    double value = co20 * x01 + co21 * x02 + co22 * x03 + co23 * x04 - co10 * y01 - co11 * y02 - co12 * y03 - co13 * y04;
                                    *column = sign_x * (*line_ptr + value) / kernel.getWeightX();
                                    y04 = y03; y03 = y02; y02 = y01; y01 = value;
                                    x04 = x03; x03 = x02; x02 = x01; x01 = (double)*source_ptr;
                                }
                            }
                        }
                        
                        // 3.3) Calculate the result of filtering the image by the vertical Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double y04, y03, y02, y01, x04, x03, x02, x01;
                            const double co00 = kernel.getCoefficientsY()[0];
                            const double co01 = kernel.getCoefficientsY()[1];
                            const double co02 = kernel.getCoefficientsY()[2];
                            const double co03 = kernel.getCoefficientsY()[3];
                            const double co10 = kernel.getCoefficientsY()[4];
                            const double co11 = kernel.getCoefficientsY()[5];
                            const double co12 = kernel.getCoefficientsY()[6];
                            const double co13 = kernel.getCoefficientsY()[7];
                            const double co20 = kernel.getCoefficientsY()[8];
                            const double co21 = kernel.getCoefficientsY()[9];
                            const double co22 = kernel.getCoefficientsY()[10];
                            const double co23 = kernel.getCoefficientsY()[11];
                            
                            for (int y = thread_identifier; y < width; y += number_of_threads)
                            {
                                const double * __restrict__ column = intermid_image + y * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                                           x03 = 0.0; x02 = 0.0; x01 = 0.0;
                                
                                // 3.3.1) Forward filter.
                                for (int x = 0; x < height; ++x)
                                {
                                    double current_x = (double)*column;
                                    *line_ptr = co00 * current_x + co01 * x01 + co02 * x02 + co03 * x03 - co10 * y01 - co11 * y02 - co12 * y03 - co13 * y04;
                                    
                                    y04 = y03; y03 = y02; y02 = y01; y01 = *line_ptr;
                                               x03 = x02; x02 = x01; x01 = current_x;
                                    ++column;
                                    ++line_ptr;
                                }
                                // 3.3.2) Backward filter.
                                y04 = 0.0; y03 = 0.0; y02 = 0.0; y01 = 0.0;
                                x04 = 0.0; x03 = 0.0; x02 = 0.0; x01 = 0.0;
                                for (int x = height - 1; x >= 0; --x)
                                {
                                    --line_ptr;
                                    --column;
                                    double value = co20 * x01 + co21 * x02 + co22 * x03 + co23 * x04 - co10 * y01 - co11 * y02 - co12 * y03 - co13 * y04;
                                    y04 = y03; y03 = y02; y02 = y01; y01 = value;
                                    x04 = x03; x03 = x02; x02 = x01; x01 = (double)*column;
                                    
                                    *destination_image.get(y, x, channel) = (T2)(sign_y * (*line_ptr + value) / kernel.getWeightY());
                                }
                            }
                        }
                    }
                }
                else if (kernel.getRecursiveMethod() == VLIET_3RD)
                {
                    for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel) // For each channel of the image...
                    {
                        // 3.2) Calculate the result of filtering the image by the horizontal Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2;
                            const double b0 = kernel.getCoefficientsX()[0];
                            const double b1 = kernel.getCoefficientsX()[1];
                            const double b2 = kernel.getCoefficientsX()[2];
                            const double alpha = kernel.getCoefficientsX()[3];
                            for (int y = thread_identifier; y < height; y += number_of_threads)
                            {
                                const T1 * __restrict__ source_ptr = source_image.get(y, channel);
                                double * __restrict__ column = intermid_image + y + width * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.2.1) Forward filter.
                                if (kernel.getDerivativeX() == 0)
                                {
                                    for (int x = 0; x < width; ++x)
                                    {
                                        double current = (double)*source_ptr * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeX() == 1)
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width - 1; ++x)
                                    {
                                        double current = ((double)*(source_ptr - 1) - (double)*(source_ptr + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                }
                                else // X derivative == 2
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width; ++x)
                                    {
                                        double current = ((double)*source_ptr - (double)*(source_ptr - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.2.2) Backward filter.
                                v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeX() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        *column = -value / kernel.getWeightX();
                                        v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                                else // X derivative == 1 OR 0
                                {
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        *column = value / kernel.getWeightX();
                                        v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                            }
                        }
                        
                        // 3.3) Calculate the result of filtering the image by the vertical Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2;
                            const double b0 = kernel.getCoefficientsY()[0];
                            const double b1 = kernel.getCoefficientsY()[1];
                            const double b2 = kernel.getCoefficientsY()[2];
                            const double alpha = kernel.getCoefficientsY()[3];
                            
                            for (int y = thread_identifier; y < width; y += number_of_threads)
                            {
                                const double * __restrict__ column = intermid_image + y * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.3.1) Forward filter.
                                if (kernel.getDerivativeY() == 0)
                                {
                                    for (int x = 0; x < height; ++x)
                                    {
                                        double current = (double)*column * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeY() == 1)
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height - 1; ++x)
                                    {
                                        double current = ((double)*(column - 1) - (double)*(column + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                }
                                else // Y derivative == 2
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height; ++x)
                                    {
                                        double current = ((double)*column - (double)*(column - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        
                                        v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.3.2) Backward filter.
                                v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeY() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(-value / kernel.getWeightY());
                                    }
                                }
                                else // Y derivative == 0 OR 1.
                                {
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2;
                                        v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(value / kernel.getWeightY());
                                    }
                                }
                            }
                        }
                    }
                }
                else if (kernel.getRecursiveMethod() == VLIET_4TH)
                {
                    for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel) // For each channel of the image...
                    {
                        // 3.2) Calculate the result of filtering the image by the horizontal Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2, v3;
                            const double b0 = kernel.getCoefficientsX()[0];
                            const double b1 = kernel.getCoefficientsX()[1];
                            const double b2 = kernel.getCoefficientsX()[2];
                            const double b3 = kernel.getCoefficientsX()[3];
                            const double alpha = kernel.getCoefficientsX()[4];
                            for (int y = thread_identifier; y < height; y += number_of_threads)
                            {
                                const T1 * __restrict__ source_ptr = source_image.get(y, channel);
                                double * __restrict__ column = intermid_image + y + width * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.2.1) Forward filter.
                                if (kernel.getDerivativeX() == 0)
                                {
                                    for (int x = 0; x < width; ++x)
                                    {
                                        double current = (double)*source_ptr * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeX() == 1)
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width - 1; ++x)
                                    {
                                        double current = ((double)*(source_ptr - 1) - (double)*(source_ptr + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                }
                                else // X derivative == 2
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width; ++x)
                                    {
                                        double current = ((double)*source_ptr - (double)*(source_ptr - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.2.2) Backward filter.
                                v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeX() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        *column = -value / kernel.getWeightX();
                                        v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                                else // X derivative == 1 OR 0
                                {
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        *column = value / kernel.getWeightX();
                                        v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                            }
                        }
                        
                        // 3.3) Calculate the result of filtering the image by the vertical Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2, v3;
                            const double b0 = kernel.getCoefficientsY()[0];
                            const double b1 = kernel.getCoefficientsY()[1];
                            const double b2 = kernel.getCoefficientsY()[2];
                            const double b3 = kernel.getCoefficientsY()[3];
                            const double alpha = kernel.getCoefficientsY()[4];
                            
                            for (int y = thread_identifier; y < width; y += number_of_threads)
                            {
                                const double * __restrict__ column = intermid_image + y * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.3.1) Forward filter.
                                if (kernel.getDerivativeY() == 0)
                                {
                                    for (int x = 0; x < height; ++x)
                                    {
                                        double current = (double)*column * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeY() == 1)
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height - 1; ++x)
                                    {
                                        double current = ((double)*(column - 1) - (double)*(column + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                }
                                else // Y derivative == 2
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height; ++x)
                                    {
                                        double current = ((double)*column - (double)*(column - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        
                                        v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.3.2) Backward filter.
                                v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeY() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(-value / kernel.getWeightY());
                                    }
                                }
                                else // Y derivative == 0 OR 1.
                                {
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3;
                                        v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(value / kernel.getWeightY());
                                    }
                                }
                            }
                        }
                    }
                }
                else if (kernel.getRecursiveMethod() == VLIET_5TH)
                {
                    for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel) // For each channel of the image...
                    {
                        // 3.2) Calculate the result of filtering the image by the horizontal Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2, v3, v4;
                            const double b0 = kernel.getCoefficientsX()[0];
                            const double b1 = kernel.getCoefficientsX()[1];
                            const double b2 = kernel.getCoefficientsX()[2];
                            const double b3 = kernel.getCoefficientsX()[3];
                            const double b4 = kernel.getCoefficientsX()[4];
                            const double alpha = kernel.getCoefficientsX()[5];
                            for (int y = thread_identifier; y < height; y += number_of_threads)
                            {
                                const T1 * __restrict__ source_ptr = source_image.get(y, channel);
                                double * __restrict__ column = intermid_image + y + width * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v4 = 0.0; v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.2.1) Forward filter.
                                if (kernel.getDerivativeX() == 0)
                                {
                                    for (int x = 0; x < width; ++x)
                                    {
                                        double current = (double)*source_ptr * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeX() == 1)
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width - 1; ++x)
                                    {
                                        double current = ((double)*(source_ptr - 1) - (double)*(source_ptr + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                }
                                else // X derivative == 2
                                {
                                    *line_ptr = 0;
                                    ++line_ptr;
                                    ++source_ptr;
                                    for (int x = 1; x < width; ++x)
                                    {
                                        double current = ((double)*source_ptr - (double)*(source_ptr - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++source_ptr;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.2.2) Backward filter.
                                v4 = 0.0; v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeX() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        *column = -value / kernel.getWeightX();
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                                else // X derivative == 1 OR 0
                                {
                                    for (int x = width - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --source_ptr;
                                        column -= height;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        *column = value / kernel.getWeightX();
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                    }
                                }
                            }
                        }
                        
                        // 3.3) Calculate the result of filtering the image by the vertical Gaussian kernel.
                        #pragma omp parallel num_threads(number_of_threads)
                        {
                            const int thread_identifier = omp_get_thread_num();
                            double v0, v1, v2, v3, v4;
                            const double b0 = kernel.getCoefficientsY()[0];
                            const double b1 = kernel.getCoefficientsY()[1];
                            const double b2 = kernel.getCoefficientsY()[2];
                            const double b3 = kernel.getCoefficientsY()[3];
                            const double b4 = kernel.getCoefficientsY()[4];
                            const double alpha = kernel.getCoefficientsY()[5];
                            
                            for (int y = thread_identifier; y < width; y += number_of_threads)
                            {
                                const double * __restrict__ column = intermid_image + y * height;
                                double * __restrict__ line_ptr = current_line[thread_identifier];
                                v4 = 0.0; v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                
                                // 3.3.1) Forward filter.
                                if (kernel.getDerivativeY() == 0)
                                {
                                    for (int x = 0; x < height; ++x)
                                    {
                                        double current = (double)*column * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                else if (kernel.getDerivativeY() == 1)
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height - 1; ++x)
                                    {
                                        double current = ((double)*(column - 1) - (double)*(column + 1)) * alpha / 2.0;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                }
                                else // Y derivative == 2
                                {
                                    *line_ptr = 0.0;
                                    ++column;
                                    ++line_ptr;
                                    for (int x = 1; x < height; ++x)
                                    {
                                        double current = ((double)*column - (double)*(column - 1)) * alpha;
                                        *line_ptr = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = *line_ptr;
                                        ++column;
                                        ++line_ptr;
                                    }
                                }
                                
                                // 3.3.2) Backward filter.
                                v4 = 0.0; v3 = 0.0; v2 = 0.0; v1 = 0.0; v0 = 0.0;
                                if (kernel.getDerivativeY() == 2)
                                {
                                    double previous = 0.0, aux;
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        aux = *line_ptr;
                                        double current = (previous - *line_ptr) * alpha;
                                        previous = aux;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(-value / kernel.getWeightY());
                                    }
                                }
                                else // Y derivative == 0 OR 1.
                                {
                                    for (int x = height - 1; x >= 0; --x)
                                    {
                                        --line_ptr;
                                        --column;
                                        
                                        double current = *line_ptr * alpha;
                                        double value = current - v0 * b0 - v1 * b1 - v2 * b2 - v3 * b3 - v4 * b4;
                                        v4 = v3; v3 = v2; v2 = v1; v1 = v0; v0 = value;
                                        
                                        *destination_image.get(y, x, channel) = (T2)(value / kernel.getWeightY());
                                    }
                                }
                            }
                        }
                    }
                }
                
                // 4) Free the allocated memory.
                for (unsigned int i = 0; i < number_of_threads; ++i) delete [] current_line[i];
                delete [] current_line;
                delete [] intermid_image;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | ORIENTED LINE DERIVATIVES            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Calculates the oriented derivative on the given image.
     *  \param[in] source_image input image where the oriented derivative is applied.
     *  \param[in] left_weight weight of the left point.
     *  \param[in] central_weight weight of the middle point.
     *  \param[in] right_weight weight of the right point.
     *  \param[in] angle angle in radians of the axis where the derivative is applied.
     *  \param[out] destination_image image where the resulting filtered image is stored.
     *  \param[in] number_of_threads number of threads used to process the image concurrently.
     */
    template <template <class> class SOURCE_IMAGE, class TSOURCE, template <class> class DESTINATION_IMAGE, class TDESTINATION>
    void imageOrientedDerivative(const SOURCE_IMAGE<TSOURCE> &source_image, double left_weight, double central_weight, double right_weight, double angle, DESTINATION_IMAGE<TDESTINATION> &destination_image, unsigned int number_of_threads)
    {
        // Check image geometry .....................................................................................................................
        if ((source_image.getWidth() != destination_image.getWidth()) || (source_image.getHeight() != destination_image.getHeight()))
            throw Exception("The geometry of the source image (%dx%d) is different than the geometry of the destination image (%dx%d).", source_image.getWidth(), source_image.getHeight(), destination_image.getWidth(), destination_image.getHeight());
        if (source_image.getNumberOfChannels() != destination_image.getNumberOfChannels())
            throw Exception("The number of channels of the source image (%d) is different than the number of channels of the destination image (%d).", source_image.getNumberOfChannels(), destination_image.getNumberOfChannels());
        
        if ((source_image.getWidth() >= 3) && (source_image.getHeight() >= 3))
        {
            double px, py, weight_left_00, weight_left_01, weight_left_10, weight_left_11, weight_right_00;
            double weight_right_01, weight_right_10, weight_right_11, weight_x, weight_y;
            int left_x, left_y, right_x, right_y;
            
            // Calculate the coordinates and weight of the right point ..............................................................................
            px = -cos(angle);
            py = sin(angle);
            left_x = (int)srvMin(0.0, floor(px));
            left_y = (int)srvMin(0.0, floor(py));
            weight_x = 1.0 - (px - (double)left_x);
            weight_y = 1.0 - (py - (double)left_y);
            weight_left_00 = weight_x * weight_y;
            weight_left_01 = (1.0 - weight_x) * weight_y;
            weight_left_10 = weight_x * (1.0 - weight_y);
            weight_left_11 = (1.0 - weight_x) * (1.0 - weight_y);
            ///////printf(" [left|angle=%.1f] x=%2d y=%2d | weights=[%0.3f %0.3f %0.3f %0.3f]\n", angle * 180.0 / 3.1415926, left_x, left_y, weight_left_00, weight_left_01, weight_left_10, weight_left_11);
            ++left_x;
            ++left_y;
            
            // Calculate the coordinates and weight of the left point ...............................................................................
            px = -px;
            py = -py;
            right_x = (int)srvMin(0.0, floor(px));
            right_y = (int)srvMin(0.0, floor(py));
            weight_x = 1.0 - (px - (double)right_x);
            weight_y = 1.0 - (py - (double)right_y);
            weight_right_00 = weight_x * weight_y;
            weight_right_01 = (1.0 - weight_x) * weight_y;
            weight_right_10 = weight_x * (1.0 - weight_y);
            weight_right_11 = (1.0 - weight_x) * (1.0 - weight_y);
            ///////printf("[right|angle=%.1f] x=%2d y=%2d | weights=[%0.3f %0.3f %0.3f %0.3f]\n", angle * 180.0 / 3.1415926, right_x, right_y, weight_right_00, weight_right_01, weight_right_10, weight_right_11);
            ++right_x;
            ++right_y;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
                {
                    TDESTINATION * __restrict__ destination_ptr = destination_image.get(0, channel);
                    for (unsigned int x = 0; x < source_image.getWidth(); ++x, ++destination_ptr)
                        *destination_ptr = 0;
                }
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
                {
                    for (unsigned int y = thread_identifier + 1; y < source_image.getHeight() - 1; y += number_of_threads)
                    {
                        const TSOURCE * source_ptr[3] = { source_image.get(y - 1, channel), source_image.get(y, channel), source_image.get(y + 1, channel) };
                        TDESTINATION * __restrict__ destination_ptr = destination_image.get(y, channel);
                        
                        *destination_ptr = 0;
                        ++destination_ptr;
                        for (unsigned int x = 1; x < source_image.getWidth() - 1; ++x)
                        {
                            double left_value, central_value, right_value;
                            
                            left_value  =  weight_left_00 * (double)source_ptr[left_y    ][left_x    ] +
                                           weight_left_01 * (double)source_ptr[left_y    ][left_x + 1] +
                                           weight_left_10 * (double)source_ptr[left_y + 1][left_x    ] +
                                           weight_left_11 * (double)source_ptr[left_y + 1][left_x + 1];
                            central_value = (double)source_ptr[1][1];
                            right_value = weight_right_00 * (double)source_ptr[right_y    ][right_x    ] +
                                          weight_right_01 * (double)source_ptr[right_y    ][right_x + 1] +
                                          weight_right_10 * (double)source_ptr[right_y + 1][right_x    ] +
                                          weight_right_11 * (double)source_ptr[right_y + 1][right_x + 1];
                            
                            *destination_ptr = (TDESTINATION)(left_weight * left_value + central_weight * central_value + right_weight * right_value);
                            ++source_ptr[0];
                            ++source_ptr[1];
                            ++source_ptr[2];
                            ++destination_ptr;
                        }
                        *destination_ptr = 0;
                    }
                }
                for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
                {
                    TDESTINATION * __restrict__ destination_ptr = destination_image.get(source_image.getHeight() - 1, channel);
                    for (unsigned int x = 0; x < source_image.getWidth(); ++x, ++destination_ptr)
                        *destination_ptr = 0;
                }
            }
        }
    }
    
    template <template <class> class SOURCE_IMAGE, class TSOURCE, template <class> class DESTINATION_IMAGE, class TDESTINATION>
    inline void imageOrientedDerivative(const SOURCE_IMAGE<TSOURCE> &source_image, unsigned int order, double angle, DESTINATION_IMAGE<TDESTINATION> &destination_image, unsigned int number_of_threads)
    {
        if      (order == 1) imageOrientedDerivative(source_image, -0.5,  0.0,  0.5, angle, destination_image, number_of_threads);
        else if (order == 2) imageOrientedDerivative(source_image, 0.25, -0.5, 0.25, angle, destination_image, number_of_threads);
        else throw Exception("Image derivative of order %d is not implemented.", order);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CONVOLUTION OF THE IMAGE WITH A      |
    //                   | GENERIC KERNEL                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function calculates the correlation function for a pattern over the pixels of the image.
     *  \param[in] source_image original image.
     *  \param[in] kernel_image pattern correlated over the original image.
     *  \param[out] destination_image output image with the correlation result.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the correlation.
     *  \param[in] use_fft enables or disables the use of the Fast Fourier Transform to perform the correlation into the frequency domain.
     */
    template <template <class> class SOURCE_IMAGE, class T1, template <class> class DESTINATION_IMAGE, class T2, template <class> class KERNEL_IMAGE, class T3>
    void ImageCorrelation(const SOURCE_IMAGE<T1> &source_image, const KERNEL_IMAGE<T3> &kernel_image, DESTINATION_IMAGE<T2> &destination_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing(), bool use_fft = false)
    {
        // Check the geometry of the image ..........................................................................................................
        checkGeometry(source_image, destination_image);
        if (source_image.getNumberOfChannels() != kernel_image.getNumberOfChannels())
            throw Exception("Both the source and the convolution kernel must have the same number of channels.");
        if (kernel_image.getWidth() > source_image.getWidth())
            throw Exception("The convolution kernel (%dx%d) must be smaller than the convolved image (%dx%d).", kernel_image.getWidth(), kernel_image.getHeight(), source_image.getWidth(), source_image.getHeight());
        if (kernel_image.getHeight() > source_image.getHeight())
            throw Exception("The convolution kernel (%dx%d) must be smaller than the convolved image (%dx%d).", kernel_image.getWidth(), kernel_image.getHeight(), source_image.getWidth(), source_image.getHeight());
        
        if (use_fft)
        {
#ifdef __ENABLE_FFTW__
            const unsigned int half_width = destination_image.getWidth() / 2;
            const unsigned int half_height = destination_image.getHeight() / 2;
            ImageFFT kernel_fft(source_image.getWidth(), source_image.getHeight(), source_image.getNumberOfChannels());
            ImageFFT source_fft(source_image.getWidth(), source_image.getHeight(), source_image.getNumberOfChannels());
            ImageFFT result_fft(source_image.getWidth(), source_image.getHeight(), source_image.getNumberOfChannels());
            
            source_fft.setImage(source_image, number_of_threads);
            kernel_fft.setImage(kernel_image, number_of_threads);
            result_fft.multiplication(source_fft, kernel_fft);
            result_fft.getImage(destination_image, number_of_threads);
            ImageFFT::shift(destination_image);
#else
            throw Exception("The use of FFTW3 library is disabled.");
#endif
        }
        else
        {
            unsigned int kernel_width, kernel_height;
            
            kernel_width = kernel_image.getWidth() / 2;
            kernel_height = kernel_image.getHeight() / 2;
            // Convolve the kernel with the source image ................................................................................................
            destination_image.setValue(0);
            for (unsigned int channel = 0; channel < source_image.getNumberOfChannels(); ++channel)
            {
                for (unsigned int kernel_y = 0; kernel_y < kernel_image.getHeight(); ++kernel_y)
                {
                    // Get kernel row (mirrored) ........................................................................................................
                    const T2 * __restrict__ kernel_row = kernel_image.get(kernel_y, channel);
                    unsigned int begin_dst_y, end_dst_y, begin_src_y, end_src_y;
                    
                    // Convolve the image with the current kernel row ...................................................................................
                    if (kernel_y < kernel_height)
                    {
                        begin_dst_y = kernel_height - kernel_y;
                        begin_src_y = 0;
                    }
                    else
                    {
                        begin_dst_y = 0;
                        begin_src_y = kernel_y - kernel_height;
                    }
                    if (kernel_y > kernel_height)
                    {
                        end_dst_y = source_image.getHeight() - (kernel_y - kernel_height);
                        end_src_y = source_image.getHeight();
                    }
                    else
                    {
                        end_dst_y = source_image.getHeight();
                        end_src_y = source_image.getHeight() - (kernel_height - kernel_y);
                    }
                    
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        const unsigned int thread_identifier = omp_get_thread_num();
                        VectorDense<T2> source_row(kernel_image.getWidth());
                        
                        for (unsigned int y_dst = begin_dst_y + thread_identifier, y_src = begin_src_y + thread_identifier; (y_src < end_src_y) && (y_dst < end_dst_y); y_src += number_of_threads, y_dst += number_of_threads)
                        {
                            const T1 * __restrict__ source_ptr = source_image.get(y_src, channel);
                            T2 * __restrict__ destination_ptr = destination_image.get(y_dst, channel);
                            
                            for (unsigned int x = 0; x < kernel_width; ++x)
                                source_row[x] = 0;
                            for (unsigned int x = kernel_width; x < kernel_image.getWidth(); ++x)
                                source_row[x] = source_ptr[x - kernel_width];
                            
                            for (unsigned int x = 0; x < source_image.getWidth(); ++x, ++destination_ptr)
                            {
                                T2 value;
                                
                                value = 0;
                                for (unsigned int t = 0, s = x % source_row.size(); t < kernel_image.getWidth(); ++t, ++s)
                                    value += source_row[s % source_row.size()] * kernel_row[t];
                                *destination_ptr += value;
                                
                                if (x + kernel_width + 1 < source_image.getWidth())
                                    source_row[x % source_row.size()] = source_ptr[x + kernel_width + 1];
                                else source_row[x % source_row.size()] = 0;
                            }
                        }
                    }
                }
            }
        }
    }
    
    /** This function calculates the convolution function for a pattern over the pixels of the image.
     *  \param[in] source_image original image.
     *  \param[in] kernel_image pattern correlated over the original image.
     *  \param[out] destination_image output image with the convolution result.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the convolution.
     *  \param[in] use_fft enables or disables the use of the Fast Fourier Transform to perform the convolution into the frequency domain.
     */
    template <template <class> class SOURCE_IMAGE, class T1, template <class> class DESTINATION_IMAGE, class T2, template <class> class KERNEL_IMAGE, class T3>
    void ImageConvolution(const SOURCE_IMAGE<T1> &source_image, const KERNEL_IMAGE<T3> &kernel_image, DESTINATION_IMAGE<T2> &destination_image, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing(), bool use_fft = false)
    {
        Image<T2> kernel_mirrored_image(kernel_image.getWidth(), kernel_image.getHeight(), kernel_image.getNumberOfChannels());
        
        for (unsigned int c = 0; c < kernel_image.getNumberOfChannels(); ++c)
        {
            for (unsigned int y = 0; y < kernel_image.getHeight(); ++y)
            {
                const T3 * __restrict__ src_ptr = kernel_image.get(y, c);
                T2 * __restrict__ dst_ptr = kernel_mirrored_image.get(kernel_image.getHeight() - y - 1, c);
                for (unsigned int x_src = 0, x_dst = kernel_image.getWidth() - 1; x_src < kernel_image.getWidth(); ++x_src, --x_dst)
                    dst_ptr[x_dst] = src_ptr[x_src];
            }
        }
        
        ImageCorrelation(source_image, kernel_mirrored_image, destination_image, number_of_threads, use_fft);
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | WINDOW FUNCTIONS FOR THE IMAGE.      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function implements the exhaustive 1-nearest neighbor local extrema search in images.
     *  \param[in] source image where the local extrema is searched.
     *  \param[in] connect identifier of the connectivity used to search the local extrema.
     *  \param[in] threshold minimum (or maximum) value of the extrema.
     *  \param[out] extrema_values array with the resulting extrema values. The first two values of the triplet are the (X, Y)-coordinates of the extrema, while the last is the channel of the image.
     *  \param[in] search_maxima boolean flag which sets local maxima search when true, while it sets local minima search when false.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the local extrema.
     */
    template <template <class> class IMAGE, class T>
    void ImageFind1NNLocalExtrema(const IMAGE<T> &source, IMAGE_CONNECTIVITY connect, const typename Identity<T>::type &threshold, VectorDense<Triplet<unsigned int, unsigned int, unsigned int> > &extrema_values, bool search_maxima, unsigned int number_of_threads)
    {
        const unsigned int width = source.getWidth();
        const unsigned int height = source.getHeight();
        const unsigned int number_of_channels = source.getNumberOfChannels();
        std::list<Triplet<unsigned int, unsigned int, unsigned int> > *extrema_list;
        unsigned int number_of_extrema_points;
        
        extrema_list = new std::list<Triplet<unsigned int, unsigned int, unsigned int> >[number_of_threads];
        // Search local maxima in the image .......................................................
        if (search_maxima)
        {
            if (connect == CONNECT4) // -----------------------------------------------------------
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        for (unsigned int y = thread_id; y < height; y += number_of_threads)
                        {
                            const T * __restrict__ current_ptr = source.get(y, c);
                            
                            // First row of the image .............................................
                            if (y == 0)
                            {
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++next_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *next_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // Last row of the image ..............................................
                            else if (y == height - 1)
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *previous_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *previous_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *previous_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // No border rows .....................................................
                            else
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr, ++next_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *previous_ptr) &&
                                        (*current_ptr > *next_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                        }
                    }
                }
            }
            else if (connect == CONNECT8) // ------------------------------------------------------
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        for (unsigned int y = thread_id; y < height; y += number_of_threads)
                        {
                            const T * __restrict__ current_ptr = source.get(y, c);
                            
                            // First row of the image .............................................
                            if (y == 0)
                            {
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *next_ptr) &&
                                    (*current_ptr > *(next_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++next_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *next_ptr) &&
                                        (*current_ptr > *(next_ptr - 1)) &&
                                        (*current_ptr > *(next_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *next_ptr) &&
                                    (*current_ptr > *(next_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // Last row of the image ..............................................
                            else if (y == height - 1)
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *(previous_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *previous_ptr) &&
                                        (*current_ptr > *(previous_ptr - 1)) &&
                                        (*current_ptr > *(previous_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *(previous_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // No border rows .....................................................
                            else
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr + 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *(previous_ptr + 1)) &&
                                    (*current_ptr > *next_ptr) &&
                                    (*current_ptr > *(next_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr, ++next_ptr)
                                    if ((*current_ptr >= threshold) &&
                                        (*current_ptr > *(current_ptr - 1)) &&
                                        (*current_ptr > *(current_ptr + 1)) &&
                                        (*current_ptr > *previous_ptr) &&
                                        (*current_ptr > *(previous_ptr - 1)) &&
                                        (*current_ptr > *(previous_ptr + 1)) &&
                                        (*current_ptr > *next_ptr) &&
                                        (*current_ptr > *(next_ptr - 1)) &&
                                        (*current_ptr > *(next_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr >= threshold) &&
                                    (*current_ptr > *(current_ptr - 1)) &&
                                    (*current_ptr > *previous_ptr) &&
                                    (*current_ptr > *(previous_ptr - 1)) &&
                                    (*current_ptr > *next_ptr) &&
                                    (*current_ptr > *(next_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                        }
                    }
                }
            }
            else throw Exception("Connectivity type with identifier '%d' cannot be used to search local extrema in images or it is not yet implemented.", (int)connect);
        }
        // Search local minima in the image .......................................................
        else
        {
            if (connect == CONNECT4) // -----------------------------------------------------------
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        for (unsigned int y = thread_id; y < height; y += number_of_threads)
                        {
                            const T * __restrict__ current_ptr = source.get(y, c);
                            
                            // First row of the image .............................................
                            if (y == 0)
                            {
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++next_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *next_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // Last row of the image ..............................................
                            else if (y == height - 1)
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *previous_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *previous_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *previous_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // No border rows .....................................................
                            else
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr, ++next_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *previous_ptr) &&
                                        (*current_ptr < *next_ptr))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *next_ptr))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                        }
                    }
                }
            }
            else if (connect == CONNECT8) // ------------------------------------------------------
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int c = 0; c < number_of_channels; ++c)
                    {
                        for (unsigned int y = thread_id; y < height; y += number_of_threads)
                        {
                            const T * __restrict__ current_ptr = source.get(y, c);
                            
                            // First row of the image .............................................
                            if (y == 0)
                            {
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *next_ptr) &&
                                    (*current_ptr < *(next_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++next_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *next_ptr) &&
                                        (*current_ptr < *(next_ptr - 1)) &&
                                        (*current_ptr < *(next_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *next_ptr) &&
                                    (*current_ptr < *(next_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // Last row of the image ..............................................
                            else if (y == height - 1)
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *(previous_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *previous_ptr) &&
                                        (*current_ptr < *(previous_ptr - 1)) &&
                                        (*current_ptr < *(previous_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *(previous_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                            // No border rows .....................................................
                            else
                            {
                                const T * __restrict__ previous_ptr = source.get(y - 1, c);
                                const T * __restrict__ next_ptr = source.get(y + 1, c);
                                
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr + 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *(previous_ptr + 1)) &&
                                    (*current_ptr < *next_ptr) &&
                                    (*current_ptr < *(next_ptr + 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(0, y, c));
                                ++current_ptr;
                                ++previous_ptr;
                                ++next_ptr;
                                for (unsigned int x = 1; x < width - 1; ++x, ++current_ptr, ++previous_ptr, ++next_ptr)
                                    if ((*current_ptr <= threshold) &&
                                        (*current_ptr < *(current_ptr - 1)) &&
                                        (*current_ptr < *(current_ptr + 1)) &&
                                        (*current_ptr < *previous_ptr) &&
                                        (*current_ptr < *(previous_ptr - 1)) &&
                                        (*current_ptr < *(previous_ptr + 1)) &&
                                        (*current_ptr < *next_ptr) &&
                                        (*current_ptr < *(next_ptr - 1)) &&
                                        (*current_ptr < *(next_ptr + 1)))
                                        extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                                if ((*current_ptr <= threshold) &&
                                    (*current_ptr < *(current_ptr - 1)) &&
                                    (*current_ptr < *previous_ptr) &&
                                    (*current_ptr < *(previous_ptr - 1)) &&
                                    (*current_ptr < *next_ptr) &&
                                    (*current_ptr < *(next_ptr - 1)))
                                    extrema_list[thread_id].push_back(Triplet<unsigned int, unsigned int, unsigned int>(width - 1, y, c));
                            }
                        }
                    }
                }
            }
            else throw Exception("Connectivity type with identifier '%d' cannot be used to search local extrema in images or it is not yet implemented.", (int)connect);
        }
        
        number_of_extrema_points = 0;
        for (unsigned int t = 0; t < number_of_threads; ++t)
            number_of_extrema_points += (unsigned int)extrema_list[t].size();
        extrema_values.set(number_of_extrema_points);
        for (unsigned int t = 0, k = 0; t < number_of_threads; ++t)
        {
            for (std::list<Triplet<unsigned int, unsigned int, unsigned int> >::iterator begin = extrema_list[t].begin(), end = extrema_list[t].end(); begin != end; ++begin, ++k)
                extrema_values[k] = *begin;
        }
        delete [] extrema_list;
    }
    
    /** This function implements the exhaustive local extrema search in the images.
     *  \param[in] source image where the local extrema is searched.
     *  \param[in] window_size number of neighbors checked around each pixel. The total area checked is a square of side 2 * window_size + 1 pixels.
     *  \param[in] threshold minimum value of the extrema.
     *  \param[out] extrema_values array with the resulting extrema values. The first two values of the triplet are the (X, Y)-coordinates of the extrema, while the last is the channel of the image.
     *  \param[in] search_maxima boolean flag which sets local maxima search when true, while it sets local minima search when false.
     */
    template <template <class> class IMAGE, class T>
    void ImageFindLocalExtrema(const IMAGE<T> &source, unsigned int window_size, const typename Identity<T>::type &threshold, VectorDense<Triplet<unsigned int, unsigned int, unsigned int> > &extrema_values, bool search_maxima = true)
    {
        const unsigned int full_window_size = 2 * window_size + 1;
        std::list<Triplet<unsigned int, unsigned int, unsigned int> > extrema_values_list;
        unsigned int index;
        
        if (search_maxima)
        {
            for (unsigned int c = 0; c < source.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = window_size; y < source.getHeight() - window_size; ++y)
                {
                    for (unsigned int x = window_size; x < source.getWidth() - window_size; ++x)
                    {
                        T current_value = *source.get(x, y, c);
                        
                        if (current_value >= threshold)
                        {
                            bool current_extrema = true;
                            
                            for (unsigned int wy = y - window_size, yi = 0; current_extrema && (yi < full_window_size); ++wy, ++yi)
                            {
                                const unsigned int window_begin = x - window_size;
                                const T * __restrict__ source_ptr = source.get(wy, c) + window_begin;
                                
                                for (unsigned int wx = window_begin, xi = 0; current_extrema && (xi < full_window_size); ++wx, ++source_ptr, ++xi)
                                    if ((wx != x) || (wy != y))
                                        current_extrema = current_extrema && (current_value > *source_ptr);
                            }
                            if (current_extrema)
                                extrema_values_list.push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                        }
                    }
                }
            }
        }
        else
        {
            for (unsigned int c = 0; c < source.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = window_size; y < source.getHeight() - window_size; ++y)
                {
                    for (unsigned int x = window_size; x < source.getWidth() - window_size; ++x)
                    {
                        T current_value = *source.get(x, y, c);
                        
                        if (current_value <= threshold)
                        {
                            bool current_extrema = true;
                            
                            for (unsigned int wy = y - window_size, yi = 0; current_extrema && (yi < full_window_size); ++wy, ++yi)
                            {
                                const unsigned int window_begin = x - window_size;
                                const T * __restrict__ source_ptr = source.get(wy, c) + window_begin;
                                
                                for (unsigned int wx = window_begin, xi = 0; current_extrema && (xi < full_window_size); ++wx, ++source_ptr, ++xi)
                                    if ((wx != x) || (wy != y))
                                        current_extrema = current_extrema && (current_value < *source_ptr);
                            }
                            if (current_extrema)
                                extrema_values_list.push_back(Triplet<unsigned int, unsigned int, unsigned int>(x, y, c));
                        }
                    }
                }
            }
        }
        extrema_values.set((unsigned int)extrema_values_list.size());
        index = 0;
        for (std::list<Triplet<unsigned int, unsigned int, unsigned int> >::iterator begin = extrema_values_list.begin(), end = extrema_values_list.end(); begin != end; ++begin, ++index)
            extrema_values[index] = *begin;
    } 
    
    /** Calculates the local extrema of the of the given square region of the image.
     *  \param[in] input input image.
     *  \param[in] window_size_horizontal width of the local region.
     *  \param[in] window_size_vertical height of the local region.
     *  \param[in] maxima boolean flag set to true to calculate the local maxima and false to calculate the local minima.
     *  \param[in] output resulting image.
     *  \param[in] number_of_threads number of threads used to concurrently process the image.
     */
    template <class TINPUT, class TOUTPUT>
    void ImageLocalExtrema(const ConstantSubImage<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima, SubImage<TOUTPUT> output, unsigned int number_of_threads)
    {
        window_size_horizontal = std::max(1U, window_size_horizontal / 2);
        window_size_vertical = std::max(1U, window_size_vertical / 2);
        // Constant definitions ...................................................................
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        const unsigned int size_horizontal = window_size_horizontal * 2 + 1;
        const unsigned int size_vertical = window_size_vertical * 2 + 1;
        const TINPUT extrema_value = (maxima)?std::numeric_limits<TINPUT>::lowest():std::numeric_limits<TINPUT>::max();
        const TINPUT& (*compare)(const TINPUT &, const TINPUT &);
        if (maxima) compare = std::max;
        else compare = std::min;
        // Variables ..............................................................................
        Image<TINPUT> maxima_partial(width, height, number_of_channels);
        TINPUT * column_d, * column_c, * * row_d_thread, * * row_c_thread;
        
        if ((output.getNumberOfChannels() != number_of_channels) || (output.getWidth() != width) || (output.getHeight() != height))
            throw srv::Exception("Both the input and the output images must have the same geometry (%dx%dx%d != %dx%dx%d).", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        // Initialize column structures ...........................................................
        column_d = new TINPUT[width * (size_vertical - 1)];
        column_c = new TINPUT[width * (size_vertical - 1)];
        
        // Process columns ........................................................................
        for (unsigned int c = 0; c < number_of_channels; ++c)
        {
            for (unsigned int y = window_size_vertical; y - window_size_vertical < height; y += size_vertical - 1)
            {
                const unsigned int block_size = width * std::min(height - (y - window_size_vertical), size_vertical - 1);
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    // Check if the center of the processing window is inside the image.
                    if (y < height)
                    {
                        TINPUT * __restrict__ ptr_d = column_d;
                        unsigned int idx;
                        
                        // Initialize the 'd' vector with the values at the center of the window.
                        {
                            const TINPUT * __restrict__ ptr_forward = input.get(y, c);
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                                ptr_d[x] = ptr_forward[x];
                        }
                        
                        // Compare the previous 'd' value with the current image value for each forward position.
                        for (idx = 1; (idx < size_vertical - 1) && (idx + y < height); ++idx, ptr_d += width)
                        {
                            const TINPUT * __restrict__ ptr_forward = input.get(thread_identifier, y + idx, c);
                            
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_forward += number_of_threads)
                                ptr_d[x + width] = compare(ptr_d[x], *ptr_forward);
                        }
                        // Copy the remaining values for the windows which are outside the image.
                        for (; (idx < size_vertical - 1); ++idx, ptr_d += width)
                            for (unsigned int x = 0; x < width; ++x)
                                ptr_d[x + width] = ptr_d[x];
                    }
                    else // Otherwise set all values to the minimum.
                        for (unsigned int k = thread_identifier; k < width * (size_vertical - 1); k += number_of_threads)
                            column_d[k] = extrema_value;
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    TINPUT * __restrict__ ptr_c = column_c + width * (size_vertical - 1) - width;
                    
                    // Initialize the 'c' vector with the values of before the center of the window.
                    {
                        const TINPUT * __restrict__ ptr_backward = input.get(y - 1, c);
                        if (y - 1 < height)
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                                ptr_c[x] = ptr_backward[x];
                        else
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                                ptr_c[x] = extrema_value;
                        ptr_c -= width;
                    }
                    // Compare the last 'c' value with the previous value on the image.
                    for (unsigned int i = 1; i < size_vertical - 1; ++i, ptr_c -= width)
                    {
                        const int idx = (int)y - (int)i - 1;
                        const TINPUT * __restrict__ ptr_backward = input.get(idx, c);
                        
                        if ((idx >= 0) && (idx < (int)height))
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                                ptr_c[x] = compare(ptr_c[x + width], ptr_backward[x]);
                        else
                            for (unsigned int x = thread_identifier; x < width; x += number_of_threads)
                                ptr_c[x] = ptr_c[x + width];
                    }
                }
                
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    const TINPUT * __restrict__ ptr_c = column_c + thread_identifier;
                    const TINPUT * __restrict__ ptr_d = column_d + thread_identifier;
                    TINPUT * __restrict__ ptr_output = maxima_partial.get(thread_identifier, y - window_size_vertical, c);
                    
                    for (unsigned int i = thread_identifier; i < block_size; i += number_of_threads, ptr_c += number_of_threads, ptr_d += number_of_threads, ptr_output += number_of_threads)
                        *ptr_output = compare(*ptr_c, *ptr_d);
                }
            }
        }
        
        // Free column structures .................................................................
        delete [] column_d;
        delete [] column_c;
        
        // Allocate row structures ................................................................
        row_d_thread = new TINPUT * [number_of_threads];
        row_c_thread = new TINPUT * [number_of_threads];
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            row_d_thread[t] = new TINPUT[size_horizontal - 1];
            row_c_thread[t] = new TINPUT[size_horizontal - 1];
        }
        
        // Process rows ...........................................................................
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int y = thread_identifier; y < height; y += number_of_threads)
                {
                    const TINPUT * __restrict__ ptr_input = maxima_partial.get(y, c);
                    TOUTPUT * __restrict__ ptr_result = output.get(y, c);
                    TINPUT * __restrict__ row_d = row_d_thread[thread_identifier];
                    TINPUT * __restrict__ row_c = row_c_thread[thread_identifier];
                    for (unsigned int x = window_size_horizontal; x - window_size_horizontal < width; x += size_horizontal - 1)
                    {
                        TINPUT previous;
                        
                        if (x < width)
                        {
                            const TINPUT * __restrict__ ptr = ptr_input + x + 1;
                            unsigned int idx;
                            
                            previous = row_d[0] = ptr_input[x];
                            for (idx = 1; (idx < size_horizontal - 1) && (idx + x < width); ++idx, ++ptr)
                                previous = row_d[idx] = compare(previous, *ptr);
                            for (; idx < size_horizontal - 1; ++idx)
                                row_d[idx] = previous;
                        }
                        else for (unsigned int i = 0; i < size_horizontal - 1; ++i)
                            row_d[i] = extrema_value;
                        
                        TINPUT * __restrict__ current_c = row_c + size_horizontal - 2;
                        previous = *current_c = (x - 1 < width)?ptr_input[x - 1]:extrema_value;
                        --current_c;
                        for (unsigned int i = 1; i < size_horizontal - 1; ++i, --current_c)
                        {
                            int idx = (int)x - (int)i - 1;
                            if ((idx >= 0) && (idx < (int)width)) previous = *current_c = compare(previous, ptr_input[idx]);
                            else *current_c = previous;
                        }
                        
                        for (unsigned int i = 0, j = x - window_size_horizontal; i < size_horizontal - 1; ++i, ++j)
                            if (j < width)
                                ptr_result[j] = compare(row_c[i], row_d[i]);
                    }
                }
            }
        }
        
        // Free row structures ....................................................................
        for (unsigned int t = 0; t < number_of_threads; ++t)
        {
            delete [] row_d_thread[t];
            delete [] row_c_thread[t];
        }
        delete [] row_d_thread;
        delete [] row_c_thread;
    }
    template <class TINPUT, class TOUTPUT> void ImageLocalExtrema(const         SubImage<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                             ImageLocalExtrema(ConstantSubImage<TINPUT>(input), window_size_horizontal, window_size_vertical, maxima,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageLocalExtrema(const            Image<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                             ImageLocalExtrema(ConstantSubImage<TINPUT>(input), window_size_horizontal, window_size_vertical, maxima,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageLocalExtrema(const ConstantSubImage<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input);  ImageLocalExtrema(                         input , window_size_horizontal, window_size_vertical, maxima, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageLocalExtrema(const         SubImage<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input);  ImageLocalExtrema(ConstantSubImage<TINPUT>(input), window_size_horizontal, window_size_vertical, maxima, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageLocalExtrema(const            Image<TINPUT> &input, unsigned int window_size_horizontal, unsigned int window_size_vertical, bool maxima,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input);  ImageLocalExtrema(ConstantSubImage<TINPUT>(input), window_size_horizontal, window_size_vertical, maxima, SubImage<TOUTPUT>(output), number_of_threads); }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DISTANCE TRANSFORM FUNCTIONS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function calculates the distance map of the given image.
     *  \param[in] input source image.
     *  \param[out] distances distances image.
     *  \param[in] method identifier of the distance used to calculate the distance transform.
     *  \param[in] emtpy_value value of the pixels labeled as empty or free space.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the distance transform.
     */
    template <template <class> class TIMAGE_INPUT, class TINPUT, template <class> class TIMAGE_DIST, class TDIST, class TEMPTY>
    void ImageDistanceTransform(const TIMAGE_INPUT<TINPUT> &input, TIMAGE_DIST<TDIST> &distances, IMAGE_DISTANCE_TRANSFORM method, TEMPTY empty_value, unsigned int number_of_threads)
    {
        // Define constants .......................................................................
        const unsigned int number_of_channels = input.getNumberOfChannels();
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const TINPUT selected_value = (TINPUT)empty_value;
        // Initialize distances image .............................................................
        if ((input.getWidth() != distances.getWidth()) || (input.getHeight() != distances.getHeight()) || (input.getNumberOfChannels() != distances.getNumberOfChannels()))
            distances.setGeometry(input);
        
        if (method == IDT_CHESSBOARD)
        {
            const TDIST maximum_distance = (TDIST)(width + height);
            // Calculate the Chessboard or Chebyshev distance .....................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr)
                        {
                            if (*in_ptr != selected_value) *dst_ptr = 0;
                            else *dst_ptr = maximum_distance;
                        }
                    }
                }
                // Forward distance estimation ....................................................
                {
                    TDIST * __restrict__ distance_current_ptr = distances.get(0, channel);
                    for (unsigned int x = 1; x < width; ++x, ++distance_current_ptr)
                        distance_current_ptr[1] = srvMin(distance_current_ptr[1], distance_current_ptr[0] + 1);
                }
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(y - 1, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(y, channel);
                    
                    distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1]) + 1);
                    for (unsigned int x = 1; x < width - 1; ++x, ++distance_current_ptr, ++distance_previous_ptr)
                        distance_current_ptr[1] = srvMin(distance_current_ptr[1], srvMin(srvMin(distance_current_ptr[0], distance_previous_ptr[2]), srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                    distance_current_ptr[1] = srvMin(distance_current_ptr[1], srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                }
                // Backward distance estimation ...................................................
                {
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 2, height - 1, channel);
                    for (unsigned int x = 1; x < width; ++x, --distance_current_ptr)
                        distance_current_ptr[0] = srvMin(distance_current_ptr[0], distance_current_ptr[1] + 1);
                }
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(width - 2, height - y, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 1, height - y - 1, channel);
                    
                    distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1]) + 1);
                    --distance_current_ptr;
                    --distance_previous_ptr;
                    for (unsigned int x = 1; x < width - 1; ++x, --distance_current_ptr, --distance_previous_ptr)
                        distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(srvMin(distance_current_ptr[1], distance_previous_ptr[2]), srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                    distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_current_ptr[1], srvMin(distance_previous_ptr[1], distance_previous_ptr[2])) + 1); 
                }
            }
        }
        else if (method == IDT_TAXICAB)
        {
            const TDIST maximum_distance = (TDIST)(width + height);
            // Calculate the Taxicab or Manhattan distance ........................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr)
                        {
                            if (*in_ptr != selected_value) *dst_ptr = 0;
                            else *dst_ptr = maximum_distance;
                        }
                    }
                }
                // Forward distance estimation ....................................................
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get((y > 0)?(y - 1):0, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(y, channel);
                    TDIST previous_value = maximum_distance;
                    
                    for (unsigned int x = 0; x < width; ++x, ++distance_current_ptr, ++distance_previous_ptr)
                        previous_value = *distance_current_ptr = srvMin(*distance_current_ptr, srvMin(*distance_previous_ptr, previous_value) + 1);
                }
                // Backward distance estimation ...................................................
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(width - 1, (y > 0)?(height - y):(height - 1), channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 1, height - y - 1, channel);
                    TDIST previous_value = maximum_distance;
                    
                    for (unsigned int x = 0; x < width; ++x, --distance_current_ptr, --distance_previous_ptr)
                        previous_value = *distance_current_ptr = srvMin(*distance_current_ptr, srvMin(*distance_previous_ptr, previous_value) + 1);
                }
            }
        }
        else if (method == IDT_EUCLIDEAN)
        {
            // Calculate the Euclidean distance ...................................................
            const TDIST maximum_distance = (TDIST)(width * width + height * height);
            const unsigned int mn = srvMax(width, height);
            VectorDense<TDIST> *aux, *z;
            VectorDense<unsigned int> *v;
            
            // Initialize structures ..............................................................
            aux = new VectorDense<TDIST>[number_of_threads];
            z = new VectorDense<TDIST>[number_of_threads];
            v = new VectorDense<unsigned int>[number_of_threads];
            for (unsigned int t = 0; t < number_of_threads; ++t)
            {
                aux[t].set(mn);
                z[t].set(mn + 1);
                v[t].set(mn);
            }
            
            // Calculate the Euclidean distance ...................................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr)
                        {
                            if (*in_ptr != selected_value) *dst_ptr = 0;
                            else *dst_ptr = maximum_distance;
                        }
                    }
                }
                
                // Transform along columns ........................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int x = thread_id; x < width; x += number_of_threads)
                    {
                        // Make a copy of the distances of the row.
                        for (unsigned int y = 0; y < height; ++y)
                            aux[thread_id][y] = *distances.get(x, y, channel);
                        // Calculate the distances along the current line.
                        v[thread_id][0] = 0;
                        z[thread_id][0] = -maximum_distance;
                        z[thread_id][1] = +maximum_distance;
                        for (unsigned int q = 1, k = 0; q < height; ++q)
                        {
                            TDIST dv, dq, q2, v2, s;
                            
                            dq = (TDIST)q;
                            dv = (TDIST)v[thread_id][k];
                            q2 = dq * dq;
                            v2 = dv * dv;
                            s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                            while (s <= z[thread_id][k])
                            {
                                --k;
                                dv = (TDIST)v[thread_id][k];
                                v2 = dv * dv;
                                s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                                if (k == 0) break;
                            }
                            ++k;
                            v[thread_id][k] = q;
                            z[thread_id][k] = s;
                            z[thread_id][k + 1] = +maximum_distance;
                        }
                        // Put the calculated distances back to the row.
                        for (unsigned int q = 0, k = 0; q < height; ++q)
                        {
                            TDIST dqv;
                            while (z[thread_id][k + 1] < (TDIST)q) ++k;
                            
                            dqv = (TDIST)q - (TDIST)v[thread_id][k];
                            *distances.get(x, q, channel) = dqv * dqv + aux[thread_id][v[thread_id][k]];
                        }
                    }
                }
                // Transform along rows ...........................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int y = thread_id; y < height; y += number_of_threads)
                    {
                        // Make a copy of the distances of the row.
                        for (unsigned int x = 0; x < width; ++x)
                            aux[thread_id][x] = *distances.get(x, y, channel);
                        // Calculate the distances along the current line.
                        v[thread_id][0] = 0;
                        z[thread_id][0] = -maximum_distance;
                        z[thread_id][1] = +maximum_distance;
                        for (unsigned int q = 1, k = 0; q < width; ++q)
                        {
                            TDIST dv, dq, q2, v2, s;
                            
                            dq = (TDIST)q;
                            dv = (TDIST)v[thread_id][k];
                            q2 = dq * dq;
                            v2 = dv * dv;
                            s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                            while (s <= z[thread_id][k])
                            {
                                --k;
                                dv = (TDIST)v[thread_id][k];
                                v2 = dv * dv;
                                s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                                if (k == 0) break;
                            }
                            ++k;
                            v[thread_id][k] = q;
                            z[thread_id][k] = s;
                            z[thread_id][k + 1] = +maximum_distance;
                        }
                        // Put the calculated distances back to the row.
                        for (unsigned int q = 0, k = 0; q < width; ++q)
                        {
                            TDIST dqv;
                            while (z[thread_id][k + 1] < (TDIST)q) ++k;
                            
                            dqv = (TDIST)q - (TDIST)v[thread_id][k];
                            *distances.get(q, y, channel) = dqv * dqv + aux[thread_id][v[thread_id][k]];
                        }
                    }
                }
            }
            
            // Free structures ....................................................................
            delete [] aux;
            delete [] z;
            delete [] v;
        }
    }
    
    /** This function calculates the distance map of the given image.
     *  \param[in] input source image.
     *  \param[out] distances distances image.
     *  \param[out] offsets offset (offset = x + y * width) of the closest pixel.
     *  \param[in] method identifier of the distance used to calculate the distance transform.
     *  \param[in] emtpy_value value of the pixels labeled as empty or free space.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the distance transform.
     */
    template <template <class> class TIMAGE_INPUT, class TINPUT, template <class> class TIMAGE_DIST, class TDIST, template <class> class TIMAGE_DISTANCE, class TEMPTY>
    void ImageDistanceTransform(const TIMAGE_INPUT<TINPUT> &input, TIMAGE_DIST<TDIST> &distances, TIMAGE_DISTANCE<unsigned int> &offsets, IMAGE_DISTANCE_TRANSFORM method, TEMPTY empty_value, unsigned int number_of_threads)
    {
        // Define constants .......................................................................
        const unsigned int number_of_channels = input.getNumberOfChannels();
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int maximum_offset = width * height * 2;
        const TINPUT selected_value = (TINPUT)empty_value;
        // Initialize distances image .............................................................
        if ((input.getWidth() != distances.getWidth()) || (input.getHeight() != distances.getHeight()) || (input.getNumberOfChannels() != distances.getNumberOfChannels()))
            distances.setGeometry(input);
        if ((input.getWidth() != offsets.getWidth()) || (input.getHeight() != offsets.getHeight()) || (input.getNumberOfChannels() != offsets.getNumberOfChannels()))
            offsets.setGeometry(input);
        
        if (method == IDT_CHESSBOARD)
        {
            const TDIST maximum_distance = (TDIST)(width + height);
            // Calculate the Chessboard or Chebyshev distance .....................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        unsigned int * __restrict__ offset_ptr = offsets.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr, ++offset_ptr)
                        {
                            if (*in_ptr != selected_value)
                            {
                                *dst_ptr = 0;
                                *offset_ptr = x + y * width;
                            }
                            else
                            {
                                *dst_ptr = maximum_distance;
                                *offset_ptr = maximum_offset;
                            }
                        }
                    }
                }
                // Forward distance estimation ....................................................
                {
                    TDIST * __restrict__ distance_current_ptr = distances.get(0, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(0, channel);
                    for (unsigned int x = 1; x < width; ++x, ++distance_current_ptr, ++offset_current_ptr)
                    {
                        if (distance_current_ptr[1] > distance_current_ptr[0] + 1)
                        {
                            distance_current_ptr[1] = distance_current_ptr[0] + 1;
                            offset_current_ptr[1] = offset_current_ptr[0];
                        }
                    }
                }
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(y - 1, channel);
                    const unsigned int * __restrict__ offset_previous_ptr = offsets.get(y - 1, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(y, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(y, channel);
                    
                    //// distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1]) + 1);
                    if (distance_current_ptr[0] > distance_previous_ptr[0] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[0] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[0];
                    }
                    if (distance_current_ptr[0] > distance_previous_ptr[1] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[1] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[1];
                    }
                    
                    for (unsigned int x = 1; x < width - 1; ++x, ++distance_current_ptr, ++distance_previous_ptr, ++offset_current_ptr, ++offset_previous_ptr)
                    {
                        //// distance_current_ptr[1] = srvMin(distance_current_ptr[1], srvMin(srvMin(distance_current_ptr[0], distance_previous_ptr[2]), srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                        if (distance_current_ptr[1] > distance_current_ptr[0] + 1)
                        {
                            distance_current_ptr[1] = distance_current_ptr[0] + 1;
                            offset_current_ptr[1] = offset_current_ptr[0];
                        }
                        if (distance_current_ptr[1] > distance_previous_ptr[0] + 1)
                        {
                            distance_current_ptr[1] = distance_previous_ptr[0] + 1;
                            offset_current_ptr[1] = offset_previous_ptr[0];
                        }
                        if (distance_current_ptr[1] > distance_previous_ptr[1] + 1)
                        {
                            distance_current_ptr[1] = distance_previous_ptr[1] + 1;
                            offset_current_ptr[1] = offset_previous_ptr[1];
                        }
                        if (distance_current_ptr[1] > distance_previous_ptr[2] + 1)
                        {
                            distance_current_ptr[1] = distance_previous_ptr[2] + 1;
                            offset_current_ptr[1] = offset_previous_ptr[2];
                        }
                        
                    }
                    
                    //// distance_current_ptr[1] = srvMin(distance_current_ptr[1], srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                    if (distance_current_ptr[1] > distance_previous_ptr[0] + 1)
                    {
                        distance_current_ptr[1] = distance_previous_ptr[0] + 1;
                        offset_current_ptr[1] = offset_previous_ptr[0];
                    }
                    if (distance_current_ptr[1] > distance_previous_ptr[1] + 1)
                    {
                        distance_current_ptr[1] = distance_previous_ptr[1] + 1;
                        offset_current_ptr[1] = offset_previous_ptr[1];
                    }
                }
                // Backward distance estimation ...................................................
                
                {
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 2, height - 1, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(width - 2, height - 1, channel);
                    for (unsigned int x = 1; x < width; ++x, --distance_current_ptr, --offset_current_ptr)
                    {
                        //distance_current_ptr[0] = srvMin(distance_current_ptr[0], distance_current_ptr[1] + 1);
                        if (distance_current_ptr[0] > distance_current_ptr[1] + 1)
                        {
                            distance_current_ptr[0] = distance_current_ptr[1] + 1;
                            offset_current_ptr[0] = offset_current_ptr[1];
                        }
                    }
                }
                for (unsigned int y = 1; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(width - 2, height - y, channel);
                    const unsigned int * __restrict__ offset_previous_ptr = offsets.get(width - 2, height - y, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 1, height - y - 1, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(width - 1, height - y - 1, channel);
                    
                    //// distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_previous_ptr[0], distance_previous_ptr[1]) + 1);
                    if (distance_current_ptr[0] > distance_previous_ptr[0] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[0] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[0];
                    }
                    if (distance_current_ptr[0] > distance_previous_ptr[1] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[1] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[1];
                    }
                    --distance_current_ptr;
                    --distance_previous_ptr;
                    --offset_current_ptr;
                    --offset_previous_ptr;
                    for (unsigned int x = 1; x < width - 1; ++x, --distance_current_ptr, --distance_previous_ptr, --offset_current_ptr, --offset_previous_ptr)
                    {
                        //// distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(srvMin(distance_current_ptr[1], distance_previous_ptr[2]), srvMin(distance_previous_ptr[0], distance_previous_ptr[1])) + 1); 
                        if (distance_current_ptr[0] > distance_current_ptr[1] + 1)
                        {
                            distance_current_ptr[0] = distance_current_ptr[1] + 1;
                            offset_current_ptr[0] = offset_current_ptr[1];
                        }
                        if (distance_current_ptr[0] > distance_previous_ptr[0] + 1)
                        {
                            distance_current_ptr[0] = distance_previous_ptr[0] + 1;
                            offset_current_ptr[0] = offset_previous_ptr[0];
                        }
                        if (distance_current_ptr[0] > distance_previous_ptr[1] + 1)
                        {
                            distance_current_ptr[0] = distance_previous_ptr[1] + 1;
                            offset_current_ptr[0] = offset_previous_ptr[1];
                        }
                        if (distance_current_ptr[0] > distance_previous_ptr[2] + 1)
                        {
                            distance_current_ptr[0] = distance_previous_ptr[2] + 1;
                            offset_current_ptr[0] = offset_previous_ptr[2];
                        }
                    }
                    //// distance_current_ptr[0] = srvMin(distance_current_ptr[0], srvMin(distance_current_ptr[1], srvMin(distance_previous_ptr[1], distance_previous_ptr[2])) + 1); 
                    if (distance_current_ptr[0] > distance_current_ptr[1] + 1)
                    {
                        distance_current_ptr[0] = distance_current_ptr[1] + 1;
                        offset_current_ptr[0] = offset_current_ptr[1];
                    }
                    if (distance_current_ptr[0] > distance_previous_ptr[1] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[1] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[1];
                    }
                    if (distance_current_ptr[0] > distance_previous_ptr[2] + 1)
                    {
                        distance_current_ptr[0] = distance_previous_ptr[2] + 1;
                        offset_current_ptr[0] = offset_previous_ptr[2];
                    }
                }
            }
        }
        else if (method == IDT_TAXICAB)
        {
            const TDIST maximum_distance = (TDIST)(width + height);
            // Calculate the Taxicab or Manhattan distance ........................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        unsigned int * __restrict__ offset_ptr = offsets.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr, ++offset_ptr)
                        {
                            if (*in_ptr != selected_value)
                            {
                                *dst_ptr = 0;
                                *offset_ptr = x + y * width;
                            }
                            else
                            {
                                *dst_ptr = maximum_distance;
                                *offset_ptr = maximum_offset;
                            }
                        }
                    }
                }
                // Forward distance estimation ....................................................
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get((y > 0)?(y - 1):0, channel);
                    const unsigned int * __restrict__ offset_previous_ptr = offsets.get((y > 0)?(y - 1):0, channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(y, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(y, channel);
                    TDIST previous_value = maximum_distance;
                    unsigned int previous_offset = maximum_offset;
                    
                    for (unsigned int x = 0; x < width; ++x, ++distance_current_ptr, ++distance_previous_ptr, ++offset_current_ptr, ++offset_previous_ptr)
                    {
                        //previous_value = *distance_current_ptr = srvMin(*distance_current_ptr, srvMin(*distance_previous_ptr, previous_value) + 1);
                        if (*distance_current_ptr > *distance_previous_ptr + 1)
                        {
                            *distance_current_ptr = *distance_previous_ptr + 1;
                            *offset_current_ptr = *offset_previous_ptr;
                        }
                        if (*distance_current_ptr > previous_value + 1)
                        {
                            *distance_current_ptr = previous_value + 1;
                            *offset_current_ptr = previous_offset;
                        }
                        previous_offset = *offset_current_ptr;
                        previous_value = *distance_current_ptr;
                    }
                }
                // Backward distance estimation ...................................................
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TDIST * __restrict__ distance_previous_ptr = distances.get(width - 1, (y > 0)?(height - y):(height - 1), channel);
                    const unsigned int * __restrict__ offset_previous_ptr = offsets.get(width - 1, (y > 0)?(height - y):(height - 1), channel);
                    TDIST * __restrict__ distance_current_ptr = distances.get(width - 1, height - y - 1, channel);
                    unsigned int * __restrict__ offset_current_ptr = offsets.get(width - 1, height - y - 1, channel);
                    TDIST previous_value = maximum_distance;
                    unsigned int previous_offset = maximum_offset;
                    
                    for (unsigned int x = 0; x < width; ++x, --distance_current_ptr, --distance_previous_ptr, --offset_current_ptr, --offset_previous_ptr)
                    {
                        //// previous_value = *distance_current_ptr = srvMin(*distance_current_ptr, srvMin(*distance_previous_ptr, previous_value) + 1);
                        if (*distance_current_ptr > *distance_previous_ptr + 1)
                        {
                            *distance_current_ptr = *distance_previous_ptr + 1;
                            *offset_current_ptr = *offset_previous_ptr;
                        }
                        if (*distance_current_ptr > previous_value + 1)
                        {
                            *distance_current_ptr = previous_value + 1;
                            *offset_current_ptr = previous_offset;
                        }
                        previous_value = *distance_current_ptr;
                        previous_offset = *offset_current_ptr;
                    }
                }
            }
        }
        else if (method == IDT_EUCLIDEAN)
        {
            // Calculate the Euclidean distance ...................................................
            const TDIST maximum_distance = (TDIST)(width * width + height * height);
            const unsigned int mn = srvMax(width, height);
            VectorDense<TDIST> *aux, *z;
            VectorDense<unsigned int> *v, *offsets_aux;
            
            // Initialize structures ..............................................................
            aux = new VectorDense<TDIST>[number_of_threads];
            z = new VectorDense<TDIST>[number_of_threads];
            v = new VectorDense<unsigned int>[number_of_threads];
            offsets_aux = new VectorDense<unsigned int>[number_of_threads];
            for (unsigned int t = 0; t < number_of_threads; ++t)
            {
                aux[t].set(mn);
                z[t].set(mn + 1);
                v[t].set(mn);
                offsets_aux[t].set(mn);
            }
            
            // Calculate the Euclidean distance ...................................................
            for (unsigned int channel = 0; channel < number_of_channels; ++channel)
            {
                // Set the initial distances ......................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int y = omp_get_thread_num(); y < height; y += number_of_threads)
                    {
                        const TINPUT * __restrict__ in_ptr = input.get(y, channel);
                        unsigned int * __restrict__ offset_ptr = offsets.get(y, channel);
                        TDIST * __restrict__ dst_ptr = distances.get(y, channel);
                        
                        for (unsigned int x = 0; x < width; ++x, ++in_ptr, ++dst_ptr, ++offset_ptr)
                        {
                            if (*in_ptr != selected_value)
                            {
                                *dst_ptr = 0;
                                *offset_ptr = x + y * width;
                            }
                            else
                            {
                                *dst_ptr = maximum_distance;
                                *offset_ptr = maximum_offset;
                            }
                        }
                    }
                }
                
                // Transform along columns ........................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int x = thread_id; x < width; x += number_of_threads)
                    {
                        // Make a copy of the distances of the row.
                        for (unsigned int y = 0; y < height; ++y)
                            aux[thread_id][y] = *distances.get(x, y, channel);
                        // Calculate the distances along the current line.
                        v[thread_id][0] = 0;
                        z[thread_id][0] = -maximum_distance;
                        z[thread_id][1] = +maximum_distance;
                        for (unsigned int q = 1, k = 0; q < height; ++q)
                        {
                            TDIST dv, dq, q2, v2, s;
                            
                            dq = (TDIST)q;
                            dv = (TDIST)v[thread_id][k];
                            q2 = dq * dq;
                            v2 = dv * dv;
                            s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                            while (s <= z[thread_id][k])
                            {
                                --k;
                                dv = (TDIST)v[thread_id][k];
                                v2 = dv * dv;
                                s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                                if (k == 0) break;
                            }
                            ++k;
                            v[thread_id][k] = q;
                            z[thread_id][k] = s;
                            z[thread_id][k + 1] = +maximum_distance;
                        }
                        // Put the calculated distances back to the row.
                        for (unsigned int q = 0, k = 0; q < height; ++q)
                        {
                            TDIST dqv;
                            while (z[thread_id][k + 1] < (TDIST)q) ++k;
                            
                            dqv = (TDIST)q - (TDIST)v[thread_id][k];
                            *distances.get(x, q, channel) = dqv * dqv + aux[thread_id][v[thread_id][k]];
                            *offsets.get(x, q, channel) = *offsets.get(x, v[thread_id][k], channel);
                        }
                    }
                }
                // Transform along rows ...........................................................
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_id = omp_get_thread_num();
                    
                    for (unsigned int y = thread_id; y < height; y += number_of_threads)
                    {
                        // Make a copy of the distances of the row.
                        for (unsigned int x = 0; x < width; ++x)
                            aux[thread_id][x] = *distances.get(x, y, channel);
                        // Calculate the distances along the current line.
                        v[thread_id][0] = 0;
                        z[thread_id][0] = -maximum_distance;
                        z[thread_id][1] = +maximum_distance;
                        for (unsigned int q = 1, k = 0; q < width; ++q)
                        {
                            TDIST dv, dq, q2, v2, s;
                            
                            dq = (TDIST)q;
                            dv = (TDIST)v[thread_id][k];
                            q2 = dq * dq;
                            v2 = dv * dv;
                            s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                            while (s <= z[thread_id][k])
                            {
                                --k;
                                dv = (TDIST)v[thread_id][k];
                                v2 = dv * dv;
                                s  = ((aux[thread_id][q] + q2) - (aux[thread_id][v[thread_id][k]] + v2)) / (2 * dq - 2 * dv);
                                if (k == 0) break;
                            }
                            ++k;
                            v[thread_id][k] = q;
                            z[thread_id][k] = s;
                            z[thread_id][k + 1] = +maximum_distance;
                        }
                        // Put the calculated distances back to the row.
                        for (unsigned int q = 0, * ptr = offsets.get(y, channel); q < width; ++q, ++ptr)
                            offsets_aux[thread_id][q] = *ptr;
                        for (unsigned int q = 0, k = 0; q < width; ++q)
                        {
                            TDIST dqv;
                            while (z[thread_id][k + 1] < (TDIST)q) ++k;
                            
                            dqv = (TDIST)q - (TDIST)v[thread_id][k];
                            *distances.get(q, y, channel) = dqv * dqv + aux[thread_id][v[thread_id][k]];
                            
                            //// PREVIOUSLY:
                            //// *offsets.get(q, y, channel) = *offsets.get(v[thread_id][k], y, channel);
                            *offsets.get(q, y, channel) = offsets_aux[thread_id][v[thread_id][k]];
                        }
                    }
                }
            }
            
            // Free structures ....................................................................
            delete [] aux;
            delete [] z;
            delete [] v;
            delete [] offsets_aux;
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE FILTERING FUNCTIONS            |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** 2-D adaptive noise-removal function which applies the Weiner low-pass filter using the statistics of the pixel local neighborhood.
     *  \param[in] input constant input sub-image.
     *  \param[in] window_size size of the window where the statistics are calculated from.
     *  \param[out] output resulting filtered sub-image.
     *  \param[in] number_of_threads number of threads used to concurrently process the image.
     *  \note The algorithm is explained in J.S. Lim, "Two-Dimensional Signal and Image Processing", PH, (Section 9.2.4 - The adaptive Wiener filter, pages 536[557]-539[560]).
     */
    template <class TINPUT, class TOUTPUT>
    void filterWiener(const ConstantSubImage<TINPUT> &input, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageProcessing())
    {
        // Constant and type definitions.
        typedef typename META_IF<std::numeric_limits<TINPUT>::is_integer, long, double>::RESULT TINTEGRAL;
        const TINTEGRAL area = (TINTEGRAL)window_size * (TINTEGRAL)window_size;
        const TINTEGRAL minimum_value = (TINTEGRAL)std::numeric_limits<TOUTPUT>::lowest();
        const TINTEGRAL maximum_value = (TINTEGRAL)std::numeric_limits<TOUTPUT>::max();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw Exception("[function filterWiener] Input and output images must have the same geometry ([%d, %d, %d] != [%d, %d, %d])", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        // Variable definitions.
        Image<TINTEGRAL> input_mean, input_variance;
        IntegralImage<TINTEGRAL> integral_image;
        srv::VectorDense<TINTEGRAL> variance_mean(number_of_channels);
        
        // Use the integral image to calculate the mean of the pixel value.
        integral_image.set(input, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, input_mean, number_of_threads);
        // Calculate the squared values of the integral image.
        input_variance.setGeometry(input);
        if (number_of_threads == 1)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr_input = input.get(y, c);
                    TINTEGRAL * __restrict__ ptr_squared = input_variance.get(y, c);
                    TINTEGRAL * __restrict__ ptr_mean = input_mean.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_input, ++ptr_squared, ++ptr_mean)
                    {
                        *ptr_squared = (TINTEGRAL)*ptr_input * (TINTEGRAL)*ptr_input;
                        *ptr_mean /= area;
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINPUT * __restrict__ ptr_input = input.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_squared = input_variance.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_mean = input_mean.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_input += number_of_threads, ptr_squared += number_of_threads, ptr_mean += number_of_threads)
                        {
                            *ptr_squared = (TINTEGRAL)*ptr_input * (TINTEGRAL)*ptr_input;
                            *ptr_mean /= area;
                        }
                    }
                }
            }
        }
        integral_image.set(input_variance, number_of_threads);
        integralImageSum(integral_image, window_size, window_size, input_variance, number_of_threads);
        // Calculate the variance at each pixel.
        if (number_of_threads == 1)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                variance_mean[c] = 0;
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINTEGRAL * __restrict__ ptr_mean = input_mean.get(y, c);
                    TINTEGRAL * __restrict__ ptr_variance = input_variance.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_variance, ++ptr_mean)
                        variance_mean += (*ptr_variance = (*ptr_variance - area * *ptr_mean * *ptr_mean) / area);
                }
                variance_mean[c] /= (width * height);
            }
        }
        else
        {
            srv::VectorDense<TINTEGRAL> variance_mean_thread(number_of_threads);
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    
                    variance_mean_thread[thread_identifier] = 0;
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINTEGRAL * __restrict__ ptr_mean = input_mean.get(thread_identifier, y, c);
                        TINTEGRAL * __restrict__ ptr_variance = input_variance.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_variance += number_of_threads, ptr_mean += number_of_threads)
                            variance_mean_thread[thread_identifier] += (*ptr_variance = (*ptr_variance - area * *ptr_mean * *ptr_mean) / area);
                    }
                }
                variance_mean[c] = variance_mean_thread[0];
                for (unsigned int t = 1; t < number_of_threads; ++t)
                    variance_mean[c] += variance_mean_thread[t];
                variance_mean[c] /= (width * height);
            }
        }
        
        // Calculate the output of the Wiener filter.
        if (number_of_threads == 1)
        {
            for (unsigned int c = 0; c < number_of_channels; ++c)
            {
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINTEGRAL * __restrict__ ptr_mean = input_mean.get(y, c);
                    const TINTEGRAL * __restrict__ ptr_variance = input_variance.get(y, c);
                    const TINPUT * __restrict__ ptr_input = input.get(y, c);
                    TOUTPUT * __restrict__ ptr_out = output.get(y, c);
                    for (unsigned int x = 0; x < width; ++x, ++ptr_variance, ++ptr_mean, ++ptr_out, ++ptr_input)
                    {
                        TINTEGRAL divisor = *ptr_variance + variance_mean[c];
                        *ptr_out = (TOUTPUT)std::max(minimum_value, std::min(maximum_value, (*ptr_mean * divisor + *ptr_variance * ((TINTEGRAL)*ptr_input - *ptr_mean)) / divisor));
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < number_of_channels; ++c)
                {
                    for (unsigned int y = 0; y < height; ++y)
                    {
                        const TINTEGRAL * __restrict__ ptr_mean = input_mean.get(thread_identifier, y, c);
                        const TINTEGRAL * __restrict__ ptr_variance = input_variance.get(thread_identifier, y, c);
                        const TINPUT * __restrict__ ptr_input = input.get(thread_identifier, y, c);
                        TOUTPUT * __restrict__ ptr_out = output.get(thread_identifier, y, c);
                        for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_variance += number_of_threads, ptr_mean += number_of_threads, ptr_out += number_of_threads, ptr_input += number_of_threads)
                        {
                            TINTEGRAL divisor = *ptr_variance + variance_mean[c];
                            *ptr_out = (TOUTPUT)std::max(minimum_value, std::min(maximum_value, (*ptr_mean * divisor + *ptr_variance * ((TINTEGRAL)*ptr_input - *ptr_mean)) / divisor));
                        }
                    }
                }
            }
        }
    }
    // Functions call with variations of the input/output image parameters.
    template <class TINPUT, class TOUTPUT> void filterWiener(const         SubImage<TINPUT> &input, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            filterWiener(ConstantSubImage<TINPUT>(input), window_size,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void filterWiener(const            Image<TINPUT> &input, unsigned int window_size, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            filterWiener(ConstantSubImage<TINPUT>(input), window_size,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void filterWiener(const ConstantSubImage<TINPUT> &input, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); filterWiener(                         input , window_size, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void filterWiener(const         SubImage<TINPUT> &input, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); filterWiener(ConstantSubImage<TINPUT>(input), window_size, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void filterWiener(const            Image<TINPUT> &input, unsigned int window_size,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); filterWiener(ConstantSubImage<TINPUT>(input), window_size, SubImage<TOUTPUT>(output), number_of_threads); }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

#endif

