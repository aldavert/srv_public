// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <getopt.h>
#include <fcntl.h>
#include <unistd.h>
#include <cerrno>

#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <asm/types.h>
#include <linux/videodev2.h>
#include <jpeglib.h>

#include "srv_camera.hpp"

#define CLEAR(x) memset (&(x), 0, sizeof (x))

namespace srv
{
    //                   +--------------------------------------+
    //                   | JPEG AUXILIARY FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ///// static void init_source (j_decompress_ptr cinfo) {}
    ///// 
    ///// static bool fill_input_buffer (j_decompress_ptr cinfo)
    ///// {
    /////     ERREXIT(cinfo, JERR_INPUT_EMPTY);
    /////     return true;
    ///// }
    ///// 
    ///// static void skip_input_data(j_decompress_ptr cinfo, long num_bytes)
    ///// {
    /////     struct jpeg_source_mgr* src = (struct jpeg_source_mgr*) cinfo->src;
    /////     
    /////     if (num_bytes > 0)
    /////     {
    /////         src->next_input_byte += (size_t) num_bytes;
    /////         src->bytes_in_buffer -= (size_t) num_bytes;
    /////     }
    ///// }
    ///// 
    ///// static void term_source(j_decompress_ptr cinfo) {}
    ///// 
    ///// static void jpeg_mem_src(j_decompress_ptr cinfo, void * buffer, long nbytes)
    ///// {
    /////     struct jpeg_source_mgr * src;
    /////     
    /////     if (cinfo->src == NULL) // First time for this JPEG object?
    /////         cinfo->src = (struct jpeg_source_mgr *)(*cinfo->mem->alloc_small) ((j_common_ptr) cinfo, JPOOL_PERMANENT, sizeof(struct jpeg_source_mgr));
    /////     
    /////     src = (struct jpeg_source_mgr*) cinfo->src;
    /////     src->init_source = init_source;
    /////     src->fill_input_buffer = fill_input_buffer;
    /////     src->skip_input_data = skip_input_data;
    /////     src->resync_to_restart = jpeg_resync_to_restart; // Use default method.
    /////     src->term_source = term_source;
    /////     src->bytes_in_buffer = nbytes;
    /////     src->next_input_byte = (JOCTET*)buffer;
    ///// }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | HUFFMAN TABLES                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    #define DHT_SIZE     432
    const static unsigned char dht_data[DHT_SIZE] = {
      0xff, 0xc4, 0x00, 0x1f, 0x00, 0x00, 0x01, 0x05, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
      0x00, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0xff, 0xc4, 0x00, 0xb5, 0x10, 0x00, 0x02,
      0x01, 0x03, 0x03, 0x02, 0x04, 0x03, 0x05, 0x05, 0x04, 0x04, 0x00, 0x00, 0x01, 0x7d, 0x01, 0x02, 0x03, 0x00, 0x04, 0x11,
      0x05, 0x12, 0x21, 0x31, 0x41, 0x06, 0x13, 0x51, 0x61, 0x07, 0x22, 0x71, 0x14, 0x32, 0x81, 0x91, 0xa1, 0x08, 0x23, 0x42,
      0xb1, 0xc1, 0x15, 0x52, 0xd1, 0xf0, 0x24, 0x33, 0x62, 0x72, 0x82, 0x09, 0x0a, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x25, 0x26,
      0x27, 0x28, 0x29, 0x2a, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x53,
      0x54, 0x55, 0x56, 0x57, 0x58, 0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x73, 0x74, 0x75, 0x76, 0x77,
      0x78, 0x79, 0x7a, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a,
      0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3,
      0xc4, 0xc5, 0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xe1, 0xe2, 0xe3, 0xe4,
      0xe5, 0xe6, 0xe7, 0xe8, 0xe9, 0xea, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa, 0xff, 0xc4, 0x00, 0x1f,
      0x01, 0x00, 0x03, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02,
      0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0xff, 0xc4, 0x00, 0xb5, 0x11, 0x00, 0x02, 0x01, 0x02, 0x04, 0x04,
      0x03, 0x04, 0x07, 0x05, 0x04, 0x04, 0x00, 0x01, 0x02, 0x77, 0x00, 0x01, 0x02, 0x03, 0x11, 0x04, 0x05, 0x21, 0x31, 0x06,
      0x12, 0x41, 0x51, 0x07, 0x61, 0x71, 0x13, 0x22, 0x32, 0x81, 0x08, 0x14, 0x42, 0x91, 0xa1, 0xb1, 0xc1, 0x09, 0x23, 0x33,
      0x52, 0xf0, 0x15, 0x62, 0x72, 0xd1, 0x0a, 0x16, 0x24, 0x34, 0xe1, 0x25, 0xf1, 0x17, 0x18, 0x19, 0x1a, 0x26, 0x27, 0x28,
      0x29, 0x2a, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x43, 0x44, 0x45, 0x46, 0x47, 0x48, 0x49, 0x4a, 0x53, 0x54, 0x55, 0x56,
      0x57, 0x58, 0x59, 0x5a, 0x63, 0x64, 0x65, 0x66, 0x67, 0x68, 0x69, 0x6a, 0x73, 0x74, 0x75, 0x76, 0x77, 0x78, 0x79, 0x7a,
      0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8a, 0x92, 0x93, 0x94, 0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0xa2, 0xa3,
      0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xb2, 0xb3, 0xb4, 0xb5, 0xb6, 0xb7, 0xb8, 0xb9, 0xba, 0xc2, 0xc3, 0xc4, 0xc5,
      0xc6, 0xc7, 0xc8, 0xc9, 0xca, 0xd2, 0xd3, 0xd4, 0xd5, 0xd6, 0xd7, 0xd8, 0xd9, 0xda, 0xe2, 0xe3, 0xe4, 0xe5, 0xe6, 0xe7,
      0xe8, 0xe9, 0xea, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7, 0xf8, 0xf9, 0xfa
    };
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CAMERA FUNCTIONS IMPLEMENTATION      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    static int xioctl(int file_descriptor, long request, void * arg)
    {
        int r;
        do { r = ioctl(file_descriptor, request, arg); } while ((r == -1) && (errno == EINTR));
        return r;
    }
    
    CameraLinux::CameraLinux(const char * camera_filename, unsigned int width, unsigned int height, unsigned int frames_per_second, CAMERA_IMAGE_FORMAT image_format, bool verbose) :
        m_width(width),
        m_height(height),
        m_frames_per_second(frames_per_second),
        m_image_format(image_format),
        m_data(new unsigned char[width * height * 4]),
        m_data_length(0),
        m_file_descriptor(-1),
        m_buffers(0),
        m_number_of_buffers(0),
        m_verbose(verbose),
        m_brightness_minimum(-100),
        m_brightness_maximum(100),
        m_brightness_value(0),
        m_brightness_enabled(false),
        m_contrast_minimum(-100),
        m_contrast_maximum(100),
        m_contrast_value(0),
        m_contrast_enabled(false),
        m_saturation_minimum(-100),
        m_saturation_maximum(100),
        m_saturation_value(0),
        m_saturation_enabled(false),
        m_hue_minimum(-100),
        m_hue_maximum(100),
        m_hue_value(0),
        m_hue_enabled(false),
        m_hue_auto(false),
        m_hue_auto_enabled(false),
        m_sharpness_minimum(-100),
        m_sharpness_maximum(100),
        m_sharpness_value(0),
        m_sharpness_enabled(false),
        m_yuyv_format(0)
    {
        unsigned int char_index;
        for (char_index = 0; (camera_filename[char_index] != '\0') && (char_index < CAMERA_FILENAME_LENGTH); ++char_index)
            m_camera_filename[char_index] = camera_filename[char_index];
        for (; char_index < CAMERA_FILENAME_LENGTH; ++char_index)
            m_camera_filename[char_index] = '\0';
        
        privateOpen();
        privateInitialize();
        privateStart();
    }
    
    CameraLinux::~CameraLinux(void)
    {
        privateStop();
        privateUninitialize();
        privateClose();
        delete [] m_data;
    }
    
    void CameraLinux::privateOpen(void)
    {
        struct stat status;
        
        if (stat(m_camera_filename, &status) == -1)
            throw Exception("Cannot identify '%s': (error %d) %s.", m_camera_filename, errno, strerror(errno));
        
        if (!S_ISCHR(status.st_mode))
            throw Exception("'%s' is not a device.", m_camera_filename);
        
        m_file_descriptor = open(m_camera_filename, O_RDWR | O_NONBLOCK, 0);
        if (m_file_descriptor == -1)
            throw Exception("Cannot open '%s': (error %d) %s.", m_camera_filename, errno, strerror(errno));
    }
    
    void CameraLinux::privateClose(void)
    {
        if (close(m_file_descriptor) == -1)
            throw Exception("Error while closing the camera file '%s'.", m_camera_filename);
        m_file_descriptor = -1;
    }
    
    void CameraLinux::privateInitialize(void)
    {
        struct v4l2_capability cap;
        struct v4l2_cropcap cropcap;
        struct v4l2_crop crop;
        struct v4l2_format fmt;
        struct v4l2_queryctrl queryctrl;
        struct v4l2_requestbuffers req;
        unsigned int min;
        
        
        if (xioctl(m_file_descriptor, VIDIOC_QUERYCAP, &cap) == -1)
        {
            if (errno == EINVAL)
                throw Exception("'%s' is not a video for linux 2 device.", m_camera_filename);
            else throw Exception("Unexpected error while checking 'VIDIOC_QUERYCAP'.");
        }
        if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
            throw Exception("'%s' is not a video capture device.", m_camera_filename);
        if (!(cap.capabilities & V4L2_CAP_STREAMING))
            throw Exception("'%s' does not support streaming i/o.", m_camera_filename);
        
        CLEAR(cropcap);
        cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (xioctl(m_file_descriptor, VIDIOC_CROPCAP, &cropcap) == 0)
        {
            crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            crop.c = cropcap.defrect; // Reset to default.
            xioctl(m_file_descriptor, VIDIOC_S_CROP, &crop);
        }
        
        CLEAR(fmt);
        fmt.type                = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        fmt.fmt.pix.width       = m_width;
        fmt.fmt.pix.height      = m_height;
        if (m_image_format == YUYV_FORMAT)
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        else if (m_image_format == JPEG_FORMAT)
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_JPEG;
        else if (m_image_format == MJPEG_FORMAT)
            fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_MJPEG;
        //fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;
        fmt.fmt.pix.field       = V4L2_FIELD_ANY;
        if (xioctl(m_file_descriptor, VIDIOC_S_FMT, &fmt) == -1)
            throw Exception("Unexpected error while checking 'VIDIOC_S_FMT'.");
        
        struct v4l2_streamparm p;
        p.type=V4L2_BUF_TYPE_VIDEO_CAPTURE;
        p.parm.capture.timeperframe.numerator = 1;
        p.parm.capture.timeperframe.denominator = m_frames_per_second;
        p.parm.output.timeperframe.numerator = 1;
        p.parm.output.timeperframe.denominator = m_frames_per_second;
        if (xioctl(m_file_descriptor, VIDIOC_S_PARM, &p) == -1)
            throw Exception("Unexpected error while checking 'VIDIOC_S_PARM'.");
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_BRIGHTNESS;
        m_brightness_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_brightness_enabled = false;
        m_brightness_minimum = queryctrl.minimum;
        m_brightness_maximum = queryctrl.maximum;
        m_brightness_value = queryctrl.default_value;
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_CONTRAST;
        m_contrast_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_contrast_enabled = false;
        m_contrast_minimum = queryctrl.minimum;
        m_contrast_maximum = queryctrl.maximum;
        m_contrast_value = queryctrl.default_value;
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_SATURATION;
        m_saturation_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_saturation_enabled = false;
        m_saturation_minimum = queryctrl.minimum;
        m_saturation_maximum = queryctrl.maximum;
        m_saturation_value = queryctrl.default_value;
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_HUE;
        m_hue_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_hue_enabled = false;
        m_hue_minimum = queryctrl.minimum;
        m_hue_maximum = queryctrl.maximum;
        m_hue_value = queryctrl.default_value;
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_HUE_AUTO;
        m_hue_auto_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_hue_auto_enabled = false;
        m_hue_auto = queryctrl.default_value;
        
        memset(&queryctrl, 0, sizeof(queryctrl));
        queryctrl.id = V4L2_CID_SHARPNESS;
        m_sharpness_enabled = true;
        if ((xioctl(m_file_descriptor, VIDIOC_QUERYCTRL, &queryctrl) == -1) || (queryctrl.flags & V4L2_CTRL_FLAG_DISABLED))
            m_sharpness_enabled = false;
        m_sharpness_minimum = queryctrl.minimum;
        m_sharpness_maximum = queryctrl.maximum;
        m_sharpness_value = queryctrl.default_value;
        
        min = fmt.fmt.pix.width * 2;
        if (fmt.fmt.pix.bytesperline < min) fmt.fmt.pix.bytesperline = min;
        min = fmt.fmt.pix.bytesperline * fmt.fmt.pix.height;
        if (fmt.fmt.pix.sizeimage < min) fmt.fmt.pix.sizeimage = min;
        
        CLEAR(req);
        req.count  = 4;
        req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        req.memory = V4L2_MEMORY_MMAP;
        if (xioctl(m_file_descriptor, VIDIOC_REQBUFS, &req) == -1)
        {
            if (errno == EINVAL)
                throw Exception("'%s' does not support memory mapping.", m_camera_filename);
            else throw Exception("Unexpected error while checking 'VIDIOC_REQBUFS'.");
        }
        if (req.count < 2) throw Exception("Insufficient buffer memory on '%s'", m_camera_filename);
        
        m_buffers = (Buffer *)calloc(req.count, sizeof(*m_buffers));
        if (!m_buffers) throw Exception("Out of memory.");
        
        for (m_number_of_buffers = 0; m_number_of_buffers < (unsigned int)req.count; ++m_number_of_buffers)
        {
            struct v4l2_buffer buf;
            CLEAR (buf);
            buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index  = m_number_of_buffers;
            
            if (xioctl(m_file_descriptor, VIDIOC_QUERYBUF, &buf) == -1)
                throw Exception("Unexpected error while checking 'VIDIOC_QUERYBUF'.");
            m_buffers[m_number_of_buffers].length = buf.length;
            m_buffers[m_number_of_buffers].start = mmap(NULL /* start anywhere */,
                                                        buf.length,
                                                        PROT_READ | PROT_WRITE /* required */,
                                                        MAP_SHARED /* recommended */,
                                                        m_file_descriptor, buf.m.offset);
            
            if (m_buffers[m_number_of_buffers].start == MAP_FAILED)
                throw Exception("Unexpected error while calling the function 'mmap'.");
        }
        
        // Crappy code to recognize a different YUYV image format.
        if (m_image_format == YUYV_FORMAT)
        {
            struct v4l2_fmtdesc fmt_desc;
            
            for (unsigned int i = 0; ; ++i)
            {
                fmt_desc.index = i;
                fmt_desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
                if (ioctl(m_file_descriptor, VIDIOC_ENUM_FMT, &fmt_desc) == -1)
                    break;
                if (strcmp((const char *)fmt_desc.description, "Planar YUV 4:2:0") == 0)
                {
                    m_yuyv_format = 1;
                    break;
                }
            }
        }
    }
    
    void CameraLinux::privateUninitialize(void)
    {
        for(unsigned int i = 0; i < m_number_of_buffers; ++i)
            if (munmap(m_buffers[i].start, m_buffers[i].length) == -1)
                throw Exception("Unexpected error while calling 'munmap'.");
        free(m_buffers);
    }
    
    void CameraLinux::privateStart(void)
    {
        enum v4l2_buf_type type;
        
        for (unsigned int i = 0; i < m_number_of_buffers; ++i)
        {
            struct v4l2_buffer buf;
            
            CLEAR (buf);
            buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            buf.index  = i;
            if (xioctl(m_file_descriptor, VIDIOC_QBUF, &buf) == -1)
                throw Exception("Unexpected error while checking 'VIDIOC_QBUF'.");
        }
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (xioctl(m_file_descriptor, VIDIOC_STREAMON, &type) == -1)
            throw Exception("Unexpected error while checking 'VIDIOC_STREAMON'.");
    }
    
    void CameraLinux::privateStop(void)
    {
        enum v4l2_buf_type type;
        
        type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        if (xioctl(m_file_descriptor, VIDIOC_STREAMOFF, &type) == -1)
            throw Exception("Unexpected error while checking 'VIDIOC_STREAMOFF'.");
    }
    
    void CameraLinux::setBrightness(int value)
    {
        if (m_brightness_enabled && (value >= m_brightness_minimum) && (value <= m_brightness_maximum))
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_BRIGHTNESS;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera brightness value.");
            
            m_brightness_value = value;
        }
    }
    
    void CameraLinux::setContrast(int value)
    {
        if (m_contrast_enabled && (value >= m_contrast_minimum) && (value <= m_contrast_maximum))
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_CONTRAST;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera contrast value.");
            
            m_contrast_value = value;
        }
    }
    
    void CameraLinux::setSaturation(int value)
    {
        if (m_saturation_enabled && (value >= m_saturation_minimum) && (value <= m_saturation_maximum))
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_SATURATION;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera saturation value.");
            
            m_saturation_value = value;
        }
    }
    
    void CameraLinux::setHue(int value)
    {
        if (m_hue_enabled && (value >= m_hue_minimum) && (value <= m_hue_maximum))
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_HUE;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera hue value.");
            
            m_hue_value = value;
        }
    }
    
    void CameraLinux::setAutomaticHue(bool value)
    {
        if (m_hue_auto_enabled)
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_HUE_AUTO;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera automatic hue value.");
            
            m_hue_auto = value;
        }
    }
    
    void CameraLinux::setSharpness(int value)
    {
        if (m_sharpness_enabled && (value >= m_sharpness_minimum) && (value <= m_sharpness_maximum))
        {
            struct v4l2_control control;
            
            control.id = V4L2_CID_SHARPNESS;
            control.value = value;
            if (ioctl(m_file_descriptor, VIDIOC_S_CTRL, &control) == -1)
                throw Exception("Unexpected exception while setting the camera sharpness value.");
            
            m_sharpness_value = value;
        }
    }
    
    /** Retrieves a frame of the camera and stores it into the internal memory.
     *  \param[in] time maximum time waited to retrieve a new frame from the camera in milliseconds.
     *  \param[in] sleep wait time in microseconds between consecutive image retrieval trials.
     */
    void CameraLinux::retrieveFrame(unsigned int wait, unsigned int sleep)
    {
        struct v4l2_buffer buf;
        
        for (unsigned int wait_time = 0; wait_time * 1000 < wait; wait_time += sleep)
        {
            CLEAR(buf);
            buf.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            buf.memory = V4L2_MEMORY_MMAP;
            if (xioctl(m_file_descriptor, VIDIOC_DQBUF, &buf) == -1)
            {
                usleep(wait);
                continue;
            }
            if (buf.index > m_number_of_buffers)
                throw Exception("The index of the buffer is greater than the number of indexes of the camera.");
            if (m_image_format == MJPEG_FORMAT)
            {
                const unsigned char * ptr = (unsigned char *)m_buffers[buf.index].start;
                unsigned int header_size;
                m_data_length = (unsigned int)buf.bytesused + DHT_SIZE;
                header_size = 0;
                for (unsigned int k = 0; (((int)ptr[0] << 8 | (int)ptr[1]) != 0xffc0) && (k < buf.bytesused); ++ptr, ++header_size, ++k);
                memcpy(m_data                         , (unsigned char *)m_buffers[buf.index].start               , header_size);
                memcpy(m_data + header_size           , dht_data                                                  , DHT_SIZE);
                memcpy(m_data + header_size + DHT_SIZE, (unsigned char *)m_buffers[buf.index].start + header_size, buf.bytesused - header_size);
            }
            else
            {
                memcpy(m_data, (unsigned char *)m_buffers[buf.index].start, m_buffers[buf.index].length);
                m_data_length = (unsigned int)m_buffers[buf.index].length;
            }
            if (xioctl(m_file_descriptor, VIDIOC_QBUF, &buf) == 0)
            {
                usleep(wait);
                continue;
            }
            
            break;
        }
    }
    
    /** Copies the internal image buffer into the given image.
     *  \param[out] image destination image where the camera image is stored.
     */
    void CameraLinux::copyImage(Image<unsigned char> &image) const
    {
        if ((m_width != image.getWidth()) || (m_height != image.getHeight()) || (image.getNumberOfChannels() != 3))
            image.setGeometry(m_width, m_height, 3);
        
        if (m_image_format == YUYV_FORMAT)
        {
            if (m_yuyv_format == 1) // Planar YUV 4:2:0
            {
                const unsigned int width2 = m_width / 2;
                const unsigned int height2 = m_height / 2;
                
                // It has the luma "luminance" plane Y first, then the U chroma plane and last the V chroma plane.
                for (unsigned int y = 0, il = 0, iu = m_width * m_height, iv = m_width * m_height + width2 * height2; y < m_height; ++y)
                {
                    unsigned char * __restrict__ ptr_r = image.get(y, 0);
                    unsigned char * __restrict__ ptr_g = image.get(y, 1);
                    unsigned char * __restrict__ ptr_b = image.get(y, 2);
                    for (unsigned int x = 0; x < m_width; ++x, ++ptr_r, ++ptr_g, ++ptr_b, ++il)
                    {
                        float r, g, b, y0, u0, v0;
                        
                        y0 = (float)m_data[il];
                        u0 = (float)m_data[iu] - 128.0f;
                        v0 = (float)m_data[iv] - 128.0f;
                        r = std::min(255.0f, std::max(0.0f, y0 + 1.370705f * v0));
                        g = std::min(255.0f, std::max(0.0f, y0 - 0.698001f * v0 - 0.337633f * u0));
                        b = std::min(255.0f, std::max(0.0f, y0 + 1.732446f * u0));
                        *ptr_r = (unsigned char)r;
                        *ptr_g = (unsigned char)g;
                        *ptr_b = (unsigned char)b;
                        if ((x + 1) % 2 == 0) { ++iu; ++iv; }
                    }
                    if (y % 2 == 0) { iu -= width2; iv -= width2; }
                }
            }
            else
            {
                const unsigned int width2 = m_width / 2;
                for (unsigned int x = 0; x < width2; ++x)
                {
                    for (unsigned int y = 0; y < m_height; ++y)
                    {
                        int y0, y1, u, v, i, r, g, b;
                        
                        i = (y * width2 + x) * 4;
                        y0 = m_data[i];
                        u  = m_data[i + 1];
                        y1 = m_data[i + 2];
                        v  = m_data[i + 3];
                        
                        r = srvMin<int>(255, srvMax<int>(0, (int)((float)y0 + (1.370705f * ((float)v - 128.0f)))));
                        g = srvMin<int>(255, srvMax<int>(0, (int)((float)y0 - (0.698001f * ((float)v - 128.0f)) - (0.337633f * ((float)u - 128.0f)))));
                        b = srvMin<int>(255, srvMax<int>(0, (int)((float)y0 + (1.732446f * ((float)u - 128.0f)))));
                        *image.get(2 * x, y, 0) = (unsigned char)r;
                        *image.get(2 * x, y, 1) = (unsigned char)g;
                        *image.get(2 * x, y, 2) = (unsigned char)b;
                        
                        r = srvMin<int>(255, srvMax<int>(0, (int)((float)y1 + (1.370705f * ((float)v - 128.0f)))));
                        g = srvMin<int>(255, srvMax<int>(0, (int)((float)y1 - (0.698001f * ((float)v - 128.0f)) - (0.337633f * ((float)u - 128.0f)))));
                        b = srvMin<int>(255, srvMax<int>(0, (int)((float)y1 + (1.732446f * ((float)u - 128.0f)))));
                        *image.get(2 * x + 1, y, 0) = (unsigned char)r;
                        *image.get(2 * x + 1, y, 1) = (unsigned char)g;
                        *image.get(2 * x + 1, y, 2) = (unsigned char)b;
                    }
                }
            }
        }
        else if ((m_image_format == JPEG_FORMAT) || (m_image_format == MJPEG_FORMAT))
        {
            struct jpeg_decompress_struct cinfo;
            struct jpeg_error_mgr jerr;
            unsigned int index;
            JSAMPARRAY buffer;
            jmp_buf setjmp_buffer;
            int row_stride;
            FILE * image_file;
            
            try
            {
                if (m_data_length == 0)
                    return;
                image_file = fmemopen(m_data, (size_t)m_data_length, "rb");
                if (image_file == 0)
                    throw Exception("Calling 'fmemopen' has failed.");
                cinfo.err = jpeg_std_error(&jerr);
                jerr.error_exit = jpegErrorFunction;
                if (setjmp(setjmp_buffer))
                {
                    jpeg_destroy_decompress(&cinfo);
                    fclose(image_file);
                    throw Exception("The JPEG code has signaled an error.");
                }
                jpeg_create_decompress(&cinfo);
                jpeg_stdio_src(&cinfo, image_file);
                
                (void)jpeg_read_header(&cinfo, 1);
                (void)jpeg_start_decompress(&cinfo);
                
                row_stride = cinfo.output_width * cinfo.output_components;
                buffer = (*cinfo.mem->alloc_sarray)((j_common_ptr)&cinfo, JPOOL_IMAGE, row_stride + 5, 1);
                for (int i = 0; i < row_stride + 5; ++i)
                    buffer[0][i] = 0;
                index = 0;
                while (cinfo.output_scanline < cinfo.output_height)
                {
                    (void)jpeg_read_scanlines(&cinfo, buffer, 1);
                    for (unsigned int w = 0, jpeg_w = 0; w < cinfo.output_width; ++w, ++index)
                        for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c, ++jpeg_w)
                            image.get(c)[index] = (unsigned char)buffer[0][jpeg_w];
                }
                fclose(image_file);
            }
            catch (Exception &e)
            {
                if (m_verbose)
                    std::cerr << e.what() << std::endl;
                image.setValue(0);
            }
        }
    }
    
    void CameraLinux::information(std::ostream &out) const
    {
        struct v4l2_fmtdesc fmt_desc;
        struct v4l2_frmsizeenum frame_size;
        struct v4l2_frmivalenum frame_rate;
        
        out << "Capture options:" << std::endl;
        for (unsigned int i = 0; ; ++i)
        {
            fmt_desc.index = i;
            fmt_desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
            if (ioctl(m_file_descriptor, VIDIOC_ENUM_FMT, &fmt_desc) == -1)
                break;
            out << "Method " << i << ": " << fmt_desc.description << std::endl;
            
            for (unsigned int j = 0; ; ++j)
            {
                frame_size.index = j;
                frame_size.pixel_format = fmt_desc.pixelformat;
                if (ioctl(m_file_descriptor, VIDIOC_ENUM_FRAMESIZES, &frame_size) == -1)
                    break;
                out << "  + Resolution " << j << ": " << frame_size.discrete.width << "x" << frame_size.discrete.height << std::endl;
                
                out << "    + Supported rates:";
                for (unsigned int k = 0; ; ++k)
                {
                    frame_rate.index = k;
                    frame_rate.pixel_format = fmt_desc.pixelformat;
                    frame_rate.width = frame_size.discrete.width;
                    frame_rate.height = frame_size.discrete.height;
                    if (ioctl(m_file_descriptor, VIDIOC_ENUM_FRAMEINTERVALS, &frame_rate) == -1)
                        break;
                    out << " " << frame_rate.discrete.numerator << "/" << frame_rate.discrete.denominator;
                }
                out << std::endl;
            }
        }
        
    }
    
}
