// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_TRANSFORM_HEADER_FILE__
#define __SRV_IMAGE_TRANSFORM_HEADER_FILE__

#include <omp.h>
#include "../srv_matrix.hpp"
#include "srv_image.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | IMAGE ROTATION FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Flip the image pixels from left to right.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image. An exception is generated when the geometry of the image is not correct.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageFlipHorizontal(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        // 1) Check image geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight()))
            throw Exception("Incorrect images geometry. Output image geometry expected to be %dx%d instead of %dx%d.", input.getWidth(), input.getHeight(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images are expected to have the same number of channels");
        
        // 2) Flip the image left-right.
        if ((void *)&input == (void *)&output) // Special case where we are flipping the image itself.
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                    {
                        T2 * __restrict__ left_ptr = output.get(y, c);
                        T2 * __restrict__ right_ptr = output.get(input.getWidth() - 1, y, c);
                        for (unsigned int x = 0; x < input.getWidth() / 2; ++x, ++left_ptr, --right_ptr)
                            srv::srvSwap(*left_ptr, *right_ptr);
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                    {
                        const T1 * __restrict__ input_ptr = input.get(y, c);
                        T2 * __restrict__ output_ptr = output.get(input.getWidth() - 1, y, c);
                        for (unsigned int x = 0; x < input.getWidth(); ++x, ++input_ptr, --output_ptr)
                            *output_ptr = (T2)*input_ptr;
                    }
                }
            }
        }
    }
    
    /** Returns a new image with the input image pixels flipped from left to right.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns a left to right flipped image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageFlipHorizontal(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels()); ImageFlipHorizontal(input, output, number_of_threads); return output; }
    
    /** Flip the image pixels from top to bottom.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image. An exception is generated when the geometry of the image is not correct.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageFlipVertical(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        // 1) Check image geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight()))
            throw Exception("Incorrect images geometry. Output image geometry expected to be %dx%d instead of %dx%d.", input.getWidth(), input.getHeight(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images are expected to have the same number of channels");
        
        // 2) Flip the image left-right.
        if ((void *)&input == (void *)&output) // Special case where we are flipping the image itself.
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < input.getHeight() / 2; y += number_of_threads)
                    {
                        T2 * __restrict__ top_ptr = output.get(y, c);
                        T2 * __restrict__ bot_ptr = output.get(input.getHeight() - y - 1, c);
                        for (unsigned int x = 0; x < input.getWidth(); ++x, ++top_ptr, ++bot_ptr)
                            srv::srvSwap(*top_ptr, *bot_ptr);
                    }
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
                {
                    for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                    {
                        const T1 * __restrict__ input_ptr  = input.get(y, c);
                        T2       * __restrict__ output_ptr = output.get(input.getHeight() - 1 - y, c);
                        for (unsigned int x = 0; x < input.getWidth(); ++x, ++input_ptr, ++output_ptr)
                            *output_ptr = (T2)*input_ptr;
                    }
                }
            }
        }
    }
    
    /** Returns a new image with the input image pixels flipped from top to bottom.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns a left to right flipped image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageFlipVertical(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels()); ImageFlipVertical(input, output, number_of_threads); return output; }
    
    /** Rotates an image 90 degrees anti-clockwise.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image. An exception is generated when the geometry of the image is not correct.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRotate90Left(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        // 1) Check image geometry.
        if ((input.getWidth() != output.getHeight()) || (input.getHeight() != output.getWidth()))
            throw Exception("Incorrect images geometry. Output image geometry expected to be %dx%d instead of %dx%d.", input.getHeight(), input.getWidth(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images are expected to have the same number of channels");
        
        // 2) Rotate the image 90 degrees.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    for (unsigned int x = 0; x < input.getWidth(); ++x)
                        *output.get(y, input.getWidth() - x - 1, c) = (T2)input_ptr[x];
                }
            }
        }
    }
    
    /** Returns a new image with the input image rotated 90 degrees anti-clockwise.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns a rotated image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRotate90Left(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { Image<T> output(input.getHeight(), input.getWidth(), input.getNumberOfChannels()); ImageRotate90Left(input, output, number_of_threads); return output; }
    
    /** Rotates an image 180 degrees.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image. An exception is generated when the geometry of the image is not correct.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRotate180(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        // 1) Check image geometry.
        checkGeometry(input, output);
        
        // 2) Rotate the image 180 degrees.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    T2 *output_ptr = output.get(input.getHeight() - y - 1, c);
                    for (unsigned int x1 = 0, x2 = input.getWidth() - 1; x1 < input.getWidth(); ++x1, --x2)
                        output_ptr[x2] = (T2)input_ptr[x1];
                }
            }
        }
    }
    
    /** Returns a new image with the input image rotated 180 degrees.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns a rotated image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRotate180(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels()); ImageRotate180(input, output, number_of_threads); return output; }
    
    /** Rotates an image 90 degrees clockwise.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image. An exception is generated when the geometry of the image is not correct.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRotate90Right(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        // 1) Check image geometry.
        if ((input.getWidth() != output.getHeight()) || (input.getHeight() != output.getWidth()))
            throw Exception("Incorrect images geometry. Output image geometry expected to be %dx%d instead of %dx%d.", input.getHeight(), input.getWidth(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images are expected to have the same number of channels");
        
        // 2) Rotate the image 90 degrees.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < input.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    for (unsigned int x = 0; x < input.getWidth(); ++x)
                        *output.get(input.getHeight() - y - 1, x, c) = (T2)input_ptr[x];
                }
            }
        }
    }
    
    /** Returns a new image with the input image rotated 90 degrees clockwise.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns a rotated image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRotate90Right(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { Image<T> output(input.getHeight(), input.getWidth(), input.getNumberOfChannels()); ImageRotate90Right(input, output, number_of_threads); return output; }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GENERAL ROTATION FUNCTIONS ]===============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Rotates the input image with respect its center. Both image are only required to have the same amount of channels, but can have a different
     *  geometry (i.e. different size). The output image will not be rescaled but the pixels which fall outside of the input image will be set
     *  to the specified output color.
     *  \param[in] input input image or sub-image.
     *  \param[in] rotation rotation in radians.
     *  \param[out] output output image or sub-image.
     *  \param[in] sampling sampling method used to calculate the value of the output image pixels.
     *  \param[in] number_of_samples number of samples used to process the image concurrently.
     *  \param[in] output_color pointer to the array with the values assigned to the output image points which fall outside the input image boundaries.
     *                          When set to null pointer, the function assigns 0 to each off limits pixel. This parameter is set by default to zero.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRotate(const INPUT_IMAGE<T1> &input, double rotation, OUTPUT_IMAGE<T2> &output, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const T2 *output_color = 0)
    {
        double cosine, sine;
        T2 *inner_black;
        
        // 1) Check the image geometry.
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images are expected to have the same number of channels but, the input image has %d-channels while the output image has %d-channels.", input.getNumberOfChannels(), output.getNumberOfChannels());
        
        // 2) Initialize the rotation variables.
        cosine = cos(rotation);
        sine = sin(rotation);
        if (output_color == 0)
        {
            inner_black = new T2[output.getNumberOfChannels()];
            for (unsigned int i = 0; i < output.getNumberOfChannels(); ++i) inner_black[i] = (T2)0;
            output_color = inner_black;
        }
        else inner_black = 0;
        
        if (sampling == NEAREST)
        {
            // 2) Rotate the image for each channel.
            #pragma omp parallel num_threads(number_of_threads)
            {
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    // 2.1) Get the pointers to the output current row.
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    
                    // 2.2) For each pixel of the row...
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double input_x, input_y, output_x, output_y;
                        
                        // 2.3) Calculate the coordinates of the pixel in the input image.
                        output_x = (double)x - (double)output.getWidth() / 2.0;
                        output_y = (double)y - (double)output.getHeight() / 2.0;
                        input_x = cosine * output_x + sine * output_y + (double)input.getWidth() / 2.0;
                        input_y = -sine * output_x + cosine * output_y + (double)input.getHeight() / 2.0;
                        
                        // 2.4) Check if the point coordinates fall inside the input image.
                        if ((input_x >= 0.0) && (input_y >= 0.0) && (input_x < input.getWidth()) && (input_y < input.getHeight()))
                        {
                            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                                output_ptrs[c][x] = (T2)*input.get((unsigned int)input_x, (unsigned int)input_y, c);
                        }
                        else for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c][x] = output_color[c];
                    }
                }
                delete [] output_ptrs;
            }
        }
        else if (sampling == BILINEAR)
        {
            // 2) Rotate the image for each channel.
            #pragma omp parallel num_threads(number_of_threads)
            {
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    // 2.1) Get the pointers to the output current row.
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    
                    // 2.2) For each pixel of the row...
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double input_x, input_y, output_x, output_y, w00, w01, w10, w11, weight_x, weight_y;
                        unsigned int image_x, image_y;
                        
                        // 2.3) Calculate the coordinates of the pixel in the input image.
                        output_x = (double)x - (double)output.getWidth() / 2.0;
                        output_y = (double)y - (double)output.getHeight() / 2.0;
                        input_x = cosine * output_x + sine * output_y + (double)input.getWidth() / 2.0;
                        input_y = -sine * output_x + cosine * output_y + (double)input.getHeight() / 2.0;
                        
                        // 2.4) Check if the point coordinates fall inside the input image.
                        if ((input_x >= 0.0) && (input_y >= 0.0) && (input_x < input.getWidth() - 1.0) && (input_y < input.getHeight() - 1.0))
                        {
                            image_x = (unsigned int)input_x;
                            image_y = (unsigned int)input_y;
                            weight_x = input_x - (double)image_x;
                            weight_y = input_y - (double)image_y;
                            w00 = (1.0 - weight_x) * (1.0 - weight_y);
                            w10 = weight_x * (1.0 - weight_y);
                            w01 = (1.0 - weight_x) * weight_y;
                            w11 = weight_x * weight_y;
                            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                                output_ptrs[c][x] = (T2)((double)*input.get(image_x, image_y, c) * w00 + (double)*input.get(image_x + 1, image_y, c) * w10 + (double)*input.get(image_x, image_y + 1, c) * w01 + (double)*input.get(image_x + 1, image_y + 1, c) * w11);
                        }
                        else for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c][x] = output_color[c];
                    }
                }
                delete [] output_ptrs;
            }
        }
        
        // 3) Free allocated memory.
        if (inner_black != 0) delete [] inner_black;
    }
    
    /** Rotates the input image with respect its center. Both image are only required to have the same amount of channels, but can have a different
     *  geometry (i.e. different size). The output image will not be rescaled but the pixels which fall outside of the input image will be set
     *  to the specified output color.
     *  \param[in] input input image or sub-image.
     *  \param[in] rotation rotation in radians.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \param[in] output_color pointer to the array with the values assigned to the output image points which fall outside the input image boundaries.
     *                          When set to null pointer, the function assigns 0 to each off limits pixel. This parameter is set by default to zero.
     *  \returns the resulting rotated image.
     */
    template <template <class T> class IMAGE, class T>
    inline Image<T> ImageRotate(const IMAGE<T> &input, double rotation, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const T *output_color = 0) { Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels()); ImageRotate(input, rotation, output, sampling, number_of_threads, output_color); return output; }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE RESCALE FUNCTIONS              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Rescales the given image or sub-image.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image.
     *  \param[in] sampling sampling method. By default set to nearest neighbor sampling.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageResize(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        double scale_x, scale_y;
        
        // 1) Check image geometry
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both image must have the same number of channels but, the input image has %d-channels while the output image has %d-channels.", input.getNumberOfChannels(), output.getNumberOfChannels());
        
        // 2) Initialize variables.
        scale_x = (double)input.getWidth() / (double)output.getWidth();
        scale_y = (double)input.getHeight() / (double)output.getHeight();
        
        // 3 Resize the image.
        if (sampling == NEAREST)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                            output_ptrs[c][x] = (T2)*input.get((unsigned int)(scale_x * (double)x), (unsigned int)(scale_y * (double)y), c);
                    }
                }
                delete [] output_ptrs;
            }
        }
        else if (sampling == BILINEAR)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double real_x, real_y, w00, w01, w10, w11, weight_x, weight_y;
                        unsigned int image_x0, image_y0, image_x1, image_y1;
                        
                        real_x = (double)x * scale_x;
                        real_y = (double)y * scale_y;
                        image_x0 = (unsigned int)real_x;
                        image_y0 = (unsigned int)real_y;
                        weight_x = real_x - (double)image_x0;
                        weight_y = real_y - (double)image_y0;
                        w00 = (1.0 - weight_x) * (1.0 - weight_y);
                        w10 = weight_x * (1.0 - weight_y);
                        w01 = (1.0 - weight_x) * weight_y;
                        w11 = weight_x * weight_y;
                        image_x1 = (image_x0 < input.getWidth() - 1)?image_x0 + 1:image_x0;
                        image_y1 = (image_y0 < input.getHeight() - 1)?image_y0 + 1:image_y0;
                        
                        for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                            output_ptrs[c][x] = (T2)((double)*input.get(image_x0, image_y0, c) * w00 + (double)*input.get(image_x1, image_y0, c) * w10 + (double)*input.get(image_x0, image_y1, c) * w01 + (double)*input.get(image_x1, image_y1, c) * w11);
                    }
                }
                delete [] output_ptrs;
            }
        }
    }
    
    /** Rescales the given image or sub-image.
     *  \param[in] input input image or sub-image.
     *  \param[in] scale_x scaling factor in the X-direction.
     *  \param[in] scale_y scaling factor in the Y-direction.
     *  \param[in] sampling sampling method. By default set to nearest neighbor sampling.
     *  \param[in] number_of_threads number of threads used to concurrently process the image.
     *  \returns resulting rescaled image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageResize(const IMAGE<T> &input, double scale_x, double scale_y, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        Image<T> output((unsigned int)((double)input.getWidth() * scale_x), (unsigned int)((double)input.getHeight() * scale_y), input.getNumberOfChannels());
        ImageResize(input, output, sampling, number_of_threads);
        return output;
    }
    
    /** Rescales the given image or sub-image.
     *  \param[in] input input image or sub-image.
     *  \param[in] scale scaling factor.
     *  \param[in] sampling sampling method. By default set to nearest neighbor sampling.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns resulting rescaled image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageResize(const IMAGE<T> &input, double scale, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        Image<T> output((unsigned int)((double)input.getWidth() * scale), (unsigned int)((double)input.getHeight() * scale), input.getNumberOfChannels());
        ImageResize(input, output, sampling, number_of_threads);
        return output;
    }
    
    /** Reduces the input image by half.
     *  \param[in] input input image or sub-image.
     *  \param[out] output resulting halved image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \note this function does not check the geometry of the input and output images, so that, when images have an incorrect geometry, a segmentation fault can be generated.
     */
    template <template <class> class INPUT_IMAGE, class TINPUT, template <class> class OUTPUT_IMAGE, class TOUTPUT>
    void ImageHalved(const INPUT_IMAGE<TINPUT> &input, OUTPUT_IMAGE<TOUTPUT> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            
            for (unsigned int c = 0; c < input.getNumberOfChannels(); ++c)
            {
                for (unsigned int h = thread_identifier; h < output.getHeight(); h += number_of_threads)
                {
                    const TINPUT * __restrict__ input_current_ptr = input.get(2 * h, c);
                    const TINPUT * __restrict__ input_next_ptr = input.get(2 * h + 1, c);
                    TOUTPUT * __restrict__ output_ptr = output.get(h, c);
                    for (unsigned int w = 0; w < output.getWidth(); ++w)
                    {
                        *output_ptr = (TOUTPUT)(((double)*input_current_ptr + (double)input_current_ptr[1] + (double)*input_next_ptr + (double)input_next_ptr[1]) / 4.0);
                        input_current_ptr += 2;
                        input_next_ptr += 2;
                        ++output_ptr;
                    }
                }
            }
        }
    }
    
    /** Reduces the input image by half.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \returns the resulting halved image.
     *  \note this function does not check the geometry of the input and output images, so that, when images have an incorrect geometry, a segmentation fault can be generated.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageHalved(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform())
    {
        Image<T> output(input.getWidth() / 2, input.getHeight() / 2, input.getNumberOfChannels());
        ImageHalved(input, output, number_of_threads);
        return output;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | IMAGE WRAPPING FUNCTIONS             |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** This function uses the given 3x3 matrix to map the output image pixels to the input image. Both images are only required to have the same
     *  number of channels. Output pixels mapped outside the boundaries of the input image are going to be set to the specified output color value.
     *  \param[in] input input image or sub-image.
     *  \param[in] transform_image constant array where the values of the 3x3 transform matrix are stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]]).
     *  \param[out] output output image or sub-image.
     *  \param[in] sampling sampling method used. By default set to nearest neighbor sampling method.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \param[in] output_color array which stores the color assigned to the output pixels which are mapped outside the boundaries of the input image. When the pointer is set to 0, the function assigns 0 to the offlimit pixels. By default is set to zero.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageTransform(const INPUT_IMAGE<T1> &input, const double *transform_matrix, OUTPUT_IMAGE<T2> &output, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const T2 *output_color = 0)
    {
        double normalization_input, normalization_output;
        T2 * inner_vector;
        
        // 1) Check images geometry.
        if (input.getNumberOfChannels() != output.getNumberOfChannels())
            throw Exception("Both images must have the same number of channels but, the input image has %d-channels while the output image has %d-channels.", input.getNumberOfChannels(), output.getNumberOfChannels());
        
        // 2) Initialize variables.
        if (output_color == 0)
        {
            inner_vector = new T2[output.getNumberOfChannels()];
            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                inner_vector[c] = 0;
            output_color = inner_vector;
        }
        else inner_vector = 0;
        normalization_input = (double)srvMax(input.getWidth(), input.getHeight());
        normalization_output = (double)srvMax(output.getWidth(), output.getHeight());
        
        // 3) Apply the transform to the output image.
        if (sampling == NEAREST)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double output_x, output_y, input_x, input_y, input_z;
                        unsigned int image_x, image_y;
                        
                        output_x = ((double)x - (double)output.getWidth() / 2.0) / normalization_output;
                        output_y = ((double)y - (double)output.getHeight() / 2.0) / normalization_output;
                        input_x = transform_matrix[0] * output_x + transform_matrix[1] * output_y + transform_matrix[2];
                        input_y = transform_matrix[3] * output_x + transform_matrix[4] * output_y + transform_matrix[5];
                        input_z = transform_matrix[6] * output_x + transform_matrix[7] * output_y + transform_matrix[8];
                        input_x = (input_x / input_z) * normalization_input + (double)input.getWidth() / 2.0;
                        input_y = (input_y / input_z) * normalization_input + (double)input.getHeight() / 2.0; 
                        image_x = (unsigned int)input_x;
                        image_y = (unsigned int)input_y;
                        
                        if (/*(image_x >= 0) &&*/ (image_x < input.getWidth()) && /*(image_y >= 0) &&*/ (image_y < input.getHeight()))
                        {
                            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                                output_ptrs[c][x] = (T2)*input.get(image_x, image_y, c);
                        }
                        else for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c][x] = output_color[c];
                    }
                }
                delete [] output_ptrs;
            }
        }
        else if (sampling == BILINEAR)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                T2 **output_ptrs = new T2*[output.getNumberOfChannels()];
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c] = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double output_x, output_y, input_x, input_y, input_z, weight_x, weight_y, w00, w01, w10, w11;
                        unsigned int image_x, image_y;
                        
                        output_x = ((double)x - (double)output.getWidth() / 2.0) / normalization_output;
                        output_y = ((double)y - (double)output.getHeight() / 2.0) / normalization_output;
                        input_x = transform_matrix[0] * output_x + transform_matrix[1] * output_y + transform_matrix[2];
                        input_y = transform_matrix[3] * output_x + transform_matrix[4] * output_y + transform_matrix[5];
                        input_z = transform_matrix[6] * output_x + transform_matrix[7] * output_y + transform_matrix[8];
                        input_x = (input_x / input_z) * normalization_input + (double)input.getWidth() / 2.0;
                        input_y = (input_y / input_z) * normalization_input + (double)input.getHeight() / 2.0; 
                        image_x = (unsigned int)input_x;
                        image_y = (unsigned int)input_y;
                        
                        if (/*(image_x >= 0) &&*/ (image_x < input.getWidth() - 1) && /*(image_y >= 0) &&*/ (image_y < input.getHeight() - 1))
                        {
                            weight_x = input_x - (double)image_x;
                            weight_y = input_y - (double)image_y;
                            w00 = (1.0 - weight_x) * (1.0 - weight_y);
                            w10 = weight_x * (1.0 - weight_y);
                            w01 = (1.0 - weight_x) * weight_y;
                            w11 = weight_x * weight_y;
                            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                                output_ptrs[c][x] = (T2)((double)*input.get(image_x, image_y, c) * w00 + (double)*input.get(image_x + 1, image_y, c) * w10 + (double)*input.get(image_x, image_y + 1, c) * w01 + (double)*input.get(image_x + 1, image_y + 1, c) * w11);
                        }
                        else for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c) output_ptrs[c][x] = output_color[c];
                    }
                }
                delete [] output_ptrs;
            }
        }
        
        // 4) Free allocated memory.
        if (inner_vector != 0) delete [] inner_vector;
    }
    
    /** This function uses the given 3x3 matrix to map the output image pixels to the input image. Both images are only required to have the same
     *  number of channels. Output pixels mapped outside the boundaries of the input image are going to be set to the specified output color value.
     *  \param[in] input input image or sub-image.
     *  \param[in] transform_image constant array where the values of the 3x3 transform matrix are stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]]).
     *  \param[in] sampling sampling method used. By default set to nearest neighbor sampling method.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \param[in] output_color array which stores the color assigned to the output pixels which are mapped outside the boundaries of the input image. When the pointer is set to 0, the function assigns 0 to the offlimit pixels. By default is set to zero.
     *  \returns the resulting transformed image.
     */
    template <template <class> class IMAGE, class T>
    Image<T> ImageTransform(const IMAGE<T> &input, const double *transform_matrix, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const T *output_color = 0)
    {
        Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels());
        ImageTransform(input, transform_matrix, output, sampling, number_of_threads, output_color);
        return output;
    }
    
    /// This class stores offsets and weights for an arbitrary image transform for a predetermined input and output image geometries.
    class TransformValues
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        TransformValues(void);
        /** Constructor which initializes a general image transform matrix.
         *  \param[in] transform array with the values of the 3x3 transform matrix stored in column-major order.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_heignt height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        TransformValues(const double * transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Constructor which initializes a general image transform matrix.
         *  \param[in] transform matrix with a 3x3 transform applied to the images.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_heignt height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        TransformValues(const Matrix<double> &transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Constructor which creates a resize image transform.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_heignt height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        TransformValues(unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Constructor which creates a rotation image transform.
         *  \param[in] angle rotation angle in radians.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_heignt height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        TransformValues(double angle, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Constructor which sets a generic mapping from the pixels of the destination image to the source image.
         *  \param[in] vector_x array with the X-coordinates in the source image associated with each destination image pixel.
         *  \param[in] vector_y array with the Y-coordinates in the source image associated with each destination image pixel.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        TransformValues(const double * vector_x, const double * vector_y, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /// Copy constructor.
        TransformValues(const TransformValues &other);
        /// Destructor.
        ~TransformValues(void);
        /// Assignation operator.
        TransformValues& operator=(const TransformValues &other);
        /** Initializes the transform structure for a general image transform matrix.
         *  \param[in] transform array with the values of the 3x3 transform matrix stored in column-major order.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        void set(const double * transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Initializes the transform structure for a general image transform matrix.
         *  \param[in] transform matrix with a 3x3 transform applied to the images.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        inline void set(const Matrix<double> &transform, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform()) { set(transform(), src_width, src_height, dst_width, dst_height, number_of_threads); }
        /** Initializes the transform structure for a rescale image transform.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        void set(unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Initializes the transform structure for a rotation transform.
         *  \param[in] angle rotation angle in radians.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads [optional] number of threads used to concurrently build the transformation model.
         */
        void set(double angle, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        /** Initializes a generic mapping from the pixels of the destination image to the source image.
         *  \param[in] vector_x array with the X-coordinates in the source image associated with each destination image pixel.
         *  \param[in] vector_y array with the Y-coordinates in the source image associated with each destination image pixel.
         *  \param[in] src_width width of the source image.
         *  \param[in] src_height height of the source image.
         *  \param[in] dst_width width of the destination image.
         *  \param[in] dst_height height of the destination image.
         *  \param[in] number_of_threads number of threads used to concurrently build the transformation model.
         */
        void set(const double * vector_x, const double * vector_y, unsigned int src_width, unsigned int src_height, unsigned int dst_width, unsigned int dst_height, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform());
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the width of the source image.
        inline unsigned int getSourceWidth(void) const { return m_src_width; }
        /// Returns the height of the source image.
        inline unsigned int getSourceHeight(void) const { return m_src_height; }
        /// Returns the width of the destination image.
        inline unsigned int getDestinationWidth(void) const { return m_dst_width; }
        /// Returns the height of the destination image.
        inline unsigned int getDestinationHeight(void) const { return m_dst_height; }
        /// Returns a constant pointer to the X-coordinate in the source image for each pixel of the destination image.
        inline const int* getOffsetsX(void) const { return m_offsets_x; }
        /// Returns a constant pointer to the Y-coordinate in the source image for each pixel of the destination image.
        inline const int* getOffsetsY(void) const { return m_offsets_y; }
        /// Returns a constant pointer to the bilinear weighting values of each image pixel.
        inline const double* getWeights(void) const { return m_weights; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the information
        void convertFromXML(XmlParser &parser);
        
    private:
        // -[ Auxiliary initialization functions ]---------------------------------------------------------------------------------------------------
        /** This function initializes the image mapping arrays for a generic 3x3 matrix transform.
         *  \param[in] transform array with the values of the 3x3 transform matrix stored in column-major order.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the mapping.
         */
        void setTransform(const double * transform, unsigned int number_of_threads);
        /** This function initializes the image mapping arrays for a resize transform.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the mapping.
         */
        void setResize(unsigned int number_of_threads);
        /** This function initializes the image mapping arrays for a rotation transform.
         *  \param[in] angle angle of the rotation in radians.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the mapping.
         */
        void setRotation(double angle, unsigned int number_of_threads);
        /** This function initializes the image mapping arrays for a generic mapping between destination and source images.
         *  \param[in] x array with the X-coordinates in the source image associated with each destination image pixel.
         *  \param[in] y array with the Y-coordinates in the source image associated with each destination image pixel.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the mapping.
         */
        void setMapping(const double * x, const double * y, unsigned int number_of_threads);
        /** This function initializes the mapping to the <x, y>-coordinate of the index-th
         *  destination image pixel.
         *  \param[in] rx X-coordinate of the source image pixel.
         *  \param[in] ry Y-coordinate of the source image pixel.
         *  \param[in] index index of the destination image pixel.
         */
        void setMappingValue(double rx, double ry, unsigned int index);
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Width of the source image.
        unsigned int m_src_width;
        /// Height of the source image.
        unsigned int m_src_height;
        /// Width of the destination image.
        unsigned int m_dst_width;
        /// Height of the destination image.
        unsigned int m_dst_height;
        /// X-coordinate of the pixels in the source image for each pixel of the destination image.
        int * m_offsets_x;
        /// Y-coordinate of the pixels in the source image for each pixel of the destination image.
        int * m_offsets_y;
        /// Bilinear weights for each pixel coordinate.
        double * m_weights;
    };
    
    /** Applies the pre-calculated transform to the given image.
     *  \param[in] input input image or sub-image.
     *  \param[out] output resulting image or sub-image.
     *  \param[in] transform pre-calculated transform method.
     *  \param[in] sampling sampling method used. By default set to nearest neighbor sampling method.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \param[in] output_color array which stores the color assigned to the output pixels which are mapped outside the boundaries of the input image. When the pointer is set to 0, the function assigns 0 to the offlimit pixels. By default is set to zero.
     */
    template <template <class> class INPUT_IMAGE, class TINPUT, template <class> class OUTPUT_IMAGE, class TOUTPUT>
    void ImageTransform(const INPUT_IMAGE<TINPUT> &input, OUTPUT_IMAGE<TOUTPUT> &output, const TransformValues &transform, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const TOUTPUT *output_color = 0)
    {
        TOUTPUT *inner_vector;
        
        // 1) Check the images geometry.
        if (input.getNumberOfChannels() != output.getNumberOfChannels()) throw Exception("Both input and output images must have the same amount of channels.");
        if ((input.getWidth() != transform.getSourceWidth()) || (input.getHeight() != transform.getSourceHeight()))
            throw Exception("Input image geometry expected to be %dx%d but is %dx%d.", transform.getSourceWidth(), transform.getSourceHeight(), input.getWidth(), input.getHeight());
        if ((output.getWidth() != transform.getDestinationWidth()) || (output.getHeight() != transform.getDestinationHeight()))
            throw Exception("Output image geometry expected to be %dx%d but is %dx%d.", transform.getDestinationWidth(), transform.getDestinationHeight(), output.getWidth(), output.getHeight());
        
        // 2) Check the color assigned to the out-side pixels and initialize variables.
        if (output_color == 0)
        {
            inner_vector = new TOUTPUT[output.getNumberOfChannels()];
            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                inner_vector[c] = 0;
            output_color = inner_vector;
        }
        else inner_vector = 0;
        const unsigned int width = output.getWidth();
        
        // 3) Generate the output image.
        switch (sampling)
        {
        case NEAREST:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                {
                    const int * __restrict__ offsets_x = transform.getOffsetsX() + thread_identifier * width;
                    const int * __restrict__ offsets_y = transform.getOffsetsY() + thread_identifier * width;
                    
                    for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                    {
                        TOUTPUT * __restrict__ output_ptr = output.get(y, c);
                        
                        for (unsigned int x = 0; x < width; ++x)
                        {
                            if (*offsets_x >= 0) *output_ptr = (TOUTPUT)*input.get(*offsets_x, *offsets_y, c); // The pixel is within the input image.
                            else *output_ptr = output_color[c]; // Otherwise set the pixel to the output color.
                            
                            ++output_ptr;
                            ++offsets_x;
                            ++offsets_y;
                        }
                        
                        offsets_x += (number_of_threads - 1) * width;
                        offsets_y += (number_of_threads - 1) * width;
                    }
                }
            }
            break;
        case BILINEAR:
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
                {
                    const int * __restrict__ offsets_x = transform.getOffsetsX() + thread_identifier * width;
                    const int * __restrict__ offsets_y = transform.getOffsetsY() + thread_identifier * width;
                    const double * __restrict__ weights = transform.getWeights() + thread_identifier * width * 4;
                    
                    for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                    {
                        TOUTPUT * __restrict__ output_ptr = output.get(y, c);
                        
                        for (unsigned int x = 0; x < width; ++x)
                        {
                            if (*offsets_x >= 0) // The pixel is within the input image.
                            {
                                double value;
                                value = (double)*input.get(*offsets_x, *offsets_y, c) * weights[0];
                                value += (double)*input.get(*offsets_x + 1, *offsets_y, c) * weights[1];
                                value += (double)*input.get(*offsets_x, *offsets_y + 1, c) * weights[2];
                                value += (double)*input.get(*offsets_x + 1, *offsets_y + 1, c) * weights[3];
                                *output_ptr = (TOUTPUT)value;
                            }
                            else *output_ptr = output_color[c]; // Otherwise set the pixel to the output color.
                            
                            ++output_ptr;
                            ++offsets_x;
                            ++offsets_y;
                            weights += 4;
                        }
                        
                        offsets_x += (number_of_threads - 1) * width;
                        offsets_y += (number_of_threads - 1) * width;
                        weights += (number_of_threads - 1) * width * 4;
                    }
                }
            }
            break;
        default:
            throw srv::Exception("The selected sampling method has not been implemented yet.");
        }
        
        // 4) Free allocated memory.
        if (inner_vector != 0) delete [] inner_vector;
    }
    
    /** Applies the pre-calculated transform to the given image.
     *  \param[in] input input image or sub-image.
     *  \param[in] transform pre-calculated transform method.
     *  \param[in] sampling sampling method used. By default set to nearest neighbor sampling method.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageTransform().
     *  \param[in] output_color array which stores the color assigned to the output pixels which are mapped outside the boundaries of the input image. When the pointer is set to 0, the function assigns 0 to the offlimit pixels. By default is set to zero.
     *  \returns an image with the resulting transformed input image or sub-image.
     */
    template <template <class> class IMAGE, class T>
    Image<T> ImageTransform(const IMAGE<T> &input, const TransformValues &transform, IMAGE_SAMPLING_METHOD sampling = NEAREST, unsigned int number_of_threads = DefaultNumberOfThreads::ImageTransform(), const T *output_color = 0)
    {
        Image<T> output(transform.getDestinationWidth(), transform.getDestinationHeight(), input.getNumberOfChannels());
        ImageTransform(input, output, transform, sampling, number_of_threads, output_color);
        return output;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

