// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_COLOR_CONVERSION_HEADER_FILE__
#define __SRV_IMAGE_COLOR_CONVERSION_HEADER_FILE__

#include "srv_image.hpp"
#include "srv_image_processing.hpp"
#include "../srv_math.hpp"
#include <omp.h>

namespace srv
{
    // =[ GRAY-SCALE TO COLOR TRANSFORM AND VICE VERSA ]=============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel color image to a gray-scale image. The gray value is the average of the three color channels.
     *  \param[in] input 3-channel image or sub-image.
     *  \param[out] output 1-channel image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Gray(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight())) throw Exception("Input and output images expected to have the same geometry.");
        if (input.getNumberOfChannels() != 3) throw Exception("Input image expected to have 3-channels but it has %d channels.", input.getNumberOfChannels());
        if (output.getNumberOfChannels() != 1) throw Exception("Output image expected to have 1-channel but it has %d channels.", output.getNumberOfChannels());
        
        // 2) Image conversion.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptr = output.get(y, 0);
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                    output_ptr[x] = (T2)(((double)input_ptrs[0][x] + (double)input_ptrs[1][x] + (double)input_ptrs[2][x]) / 3.0);
            }
        }
    }
    
    /** Converts a 3-channel color image into a gray-scale image. The gray value of each pixel is the average of the three color channels.
     * \param[in] input 3-channel image or sub-image.
     * \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     * \returns single channel image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Gray(const IMAGE<T> &source, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        Image<T> destination(source.getWidth(), source.getHeight(), 1);
        ImageRGB2Gray(source, destination, number_of_threads);
        return destination;
    }
    
    /** Convert a 3-channel color image to a gray-scale image. The gray value is the luminance, i.e. Gray = 0.299 * Red + 0.587 * Green+0.114 * Blue
     *  \param[in] input 3-channel image or sub-image.
     *  \param[out] output 1-channel image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Luminance(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight())) throw Exception("Input and output images expected to have the same geometry.");
        if (input.getNumberOfChannels() != 3) throw Exception("Input image expected to have 3-channels but it has %d channels.", input.getNumberOfChannels());
        if (output.getNumberOfChannels() != 1) throw Exception("Output image expected to have 1-channel but it has %d channels.", output.getNumberOfChannels());
        
        // 2) Image conversion.
        if (std::numeric_limits<T2>::is_exact)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                    T2 *output_ptr = output.get(y, 0);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                        output_ptr[x] = (T2)round((double)input_ptrs[0][x] * 0.299 + (double)input_ptrs[1][x] * 0.587 + (double)input_ptrs[2][x] * 0.114);
                }
            }
        }
        else
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                    T2 *output_ptr = output.get(y, 0);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                        output_ptr[x] = (T2)((double)input_ptrs[0][x] * 0.299 + (double)input_ptrs[1][x] * 0.587 + (double)input_ptrs[2][x] * 0.114);
                }
            }
        }
    }
    
    /** Convert a 3-channel color image to a gray-scale image. The gray value is the luminance, i.e. Gray = 0.299 * Red + 0.587 * Green+0.114 * Blue
     *  \param[in] input 3-channel image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \returns single channel image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Luminance(const IMAGE<T> &source, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        Image<T> destination(source.getWidth(), source.getHeight(), 1);
        ImageRGB2Luminance(source, destination, number_of_threads);
        return destination;
    }
    
    /** Convert a single channel gray image to a 3-channel color image.
     *  \param[in] input single channel image or sub-image.
     *  \param[out] output 3-channel image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageGray2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight())) throw Exception("Input and output images expected to have the same geometry.");
        if (input.getNumberOfChannels() != 1) throw Exception("Input image expected to have 1-channel but it has %d channels.", input.getNumberOfChannels());
        if (output.getNumberOfChannels() != 3) throw Exception("Output image expected to have 3-channels but it has %d channels.", output.getNumberOfChannels());
        
        // 2) Image conversion.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptr = input.get(y, 0);
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                    output_ptrs[0][x] = output_ptrs[1][x] = output_ptrs[2][x] = (T2)input_ptr[x];
            }
        }
    }
    
    /** Convert a single channel gray image to a 3-channel color image.
     *  \param[in] input single channel image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \returns a color image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageGray2RGB(const IMAGE<T> &source, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor())
    {
        Image<T> destination(source.getWidth(), source.getHeight(), 3);
        ImageGray2RGB(source, destination, number_of_threads);
        return destination;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ GRAY TO HUE COLORMAP ALGORITHM ]===========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 1-channel image to a Hue color map (i.e.\ pixels with the high values are going to be mapped to reddish colors while, pixels with
     *  low values are going to be mapped to bluish colors).
     *  \param[in] input 1-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageGray2HueColormap(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight())) throw Exception("Input and output images expected to have the same geometry.");
        if (input.getNumberOfChannels() != 1) throw Exception("Input image expected to have 1-channel but it has %d channels.", input.getNumberOfChannels());
        if (output.getNumberOfChannels() != 3) throw Exception("Output image expected to have 3-channels but it has %d channels.", output.getNumberOfChannels());
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, huep, R, G, B;
                    
                    hue = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    hue = (1.0 - srvMin(1.0, srvMax(0.0, hue))) * 2.0 / 3.0; // Set the hue at 240 degree at max (from red to blue).
                    
                    huep = hue * 3.0; // divided by 2: 3 instead of 6.
                    huep = (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                    if (hue < 1.0 / 6.0)
                    {
                        R = 1.0;
                        G = huep;
                        B = 0.0;
                    }
                    else if (hue < 2.0 / 6.0)
                    {
                        R = huep;
                        G = 1.0;
                        B = 0.0;
                    }
                    else if (hue < 3.0 / 6.0)
                    {
                        R = 0.0;
                        G = 1.0;
                        B = huep;
                    }
                    else if (hue < 4.0 / 6.0)
                    {
                        R = 0.0;
                        G = huep;
                        B = 1.0;
                    }
                    else if (hue < 5.0 / 6.0)
                    {
                        R = huep;
                        G = 0.0;
                        B = 1.0;
                    }
                    else if (hue < 6.0 / 6.0)
                    {
                        R = 1.0;
                        G = 0.0;
                        B = huep;
                    }
                    else
                    {
                        R = 0.0;
                        G = 0.0;
                        B = 0.0;
                    }
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 1-channel image to a Hue color map (i.e.\ pixels with the high values are going to be mapped to reddish colors while, pixels with
     *  low values are going to be mapped to bluish colors).
     *  \param[in] input 1-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns a color image with the color map.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageGray2HueColormap(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageGray2HueColormap(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 1-channel image to a Hue color map (i.e.\ pixels with the high values are going to be mapped to reddish colors while, pixels with
     *  low values are going to be mapped to bluish colors).
     *  \param[in] input 1-channel input image or sub-image.
     *  \param[in] min_value minimum value of the input image that will be mapped in the color map.
     *  \param[in] max_value maximum value of the input image that will be mapped in the color map.
     *  \param[in] outrange_color color assigned to the pixel that are out of the range defined by min_value and max_value.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageGray2HueColormap(const INPUT_IMAGE<T1> &input, const T1 &min_value, const T1 &max_value, const T2 *outrange_color, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight())) throw Exception("Input and output images expected to have the same geometry.");
        if (input.getNumberOfChannels() != 1) throw Exception("Input image expected to have 1-channel but it has %d channels.", input.getNumberOfChannels());
        if (output.getNumberOfChannels() != 3) throw Exception("Output image expected to have 3-channels but it has %d channels.", output.getNumberOfChannels());
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, huep, R, G, B;
                    
                    if ((input_ptrs[0][x] < max_value) && (input_ptrs[0][x] >= min_value))
                    {
                        hue = (double)(input_ptrs[0][x] - (double)min_value) / (double)(max_value - min_value);
                        hue = (1.0 - hue) * 2.0 / 3.0; // Set the hue at 240 degree at max (from red to blue).
                        
                        huep = hue * 3.0; // divided by 2: 3 instead of 6.
                        huep = (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                        if (hue < 1.0 / 6.0)
                        {
                            R = 1.0;
                            G = huep;
                            B = 0.0;
                        }
                        else if (hue < 2.0 / 6.0)
                        {
                            R = huep;
                            G = 1.0;
                            B = 0.0;
                        }
                        else if (hue < 3.0 / 6.0)
                        {
                            R = 0.0;
                            G = 1.0;
                            B = huep;
                        }
                        else if (hue < 4.0 / 6.0)
                        {
                            R = 0.0;
                            G = huep;
                            B = 1.0;
                        }
                        else if (hue < 5.0 / 6.0)
                        {
                            R = huep;
                            G = 0.0;
                            B = 1.0;
                        }
                        else if (hue < 6.0 / 6.0)
                        {
                            R = 1.0;
                            G = 0.0;
                            B = huep;
                        }
                        else
                        {
                            R = 0.0;
                            G = 0.0;
                            B = 0.0;
                        }
                        
                        output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                        output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                        output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    }
                    else
                    {
                        output_ptrs[0][x] = outrange_color[0];
                        output_ptrs[1][x] = outrange_color[1];
                        output_ptrs[2][x] = outrange_color[2];
                    }
                }
            }
        }
    }
    
    /** Convert a 1-channel image to a Hue color map (i.e.\ pixels with the high values are going to be mapped to reddish colors while, pixels with
     *  low values are going to be mapped to bluish colors).
     *  \param[in] input 1-channel input image or sub-image.
     *  \param[in] min_value minimum value of the input image that will be mapped in the color map.
     *  \param[in] max_value maximum value of the input image that will be mapped in the color map.
     *  \param[in] outrange_color color assigned to the pixel that are out of the range defined by min_value and max_value.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns a color image with the color map.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageGray2HueColormap(const IMAGE<T> &input, const T &min_value, const T &max_value, const T *outrange_color, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageGray2HueColormap(input, min_value, max_value, outrange_color, output, number_of_threads, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HSV COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to HSV (Hue-Saturation-Value)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2HSV(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, R, G, B, maximum, minimum, chroma, value, saturation;
                    unsigned int max_index;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // 2.2) Calculate the maximum and minimum values.
                    if (R > G)
                    {
                        maximum = R;
                        max_index = 0;
                        minimum = G;
                    }
                    else
                    {
                        maximum = G;
                        max_index = 1;
                        minimum = R;
                    }
                    if (B > maximum)
                    {
                        maximum = B;
                        max_index = 2;
                    }
                    else if (B < minimum) minimum = B;
                    
                    // 2.3) Calculate the chroma and hue.
                    chroma = maximum - minimum;
                    if (chroma == 0) hue = 0;
                    else if (max_index == 0)
                    {
                        hue = ((G - B) / chroma) / 6.0;
                        if (hue < 0) hue += 1.0;
                    }
                    else if (max_index == 1) hue = ((B - R) / chroma + 2.0) / 6.0;
                    else if (max_index == 2) hue = ((R - G) / chroma + 4.0) / 6.0;
                    value = maximum;
                    saturation = (chroma == 0)?0:chroma / maximum;
                    
                    output_ptrs[0][x] = (T2)(hue * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(saturation * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(value * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to HSV (Hue-Saturation-Value)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns a color image in the HSV color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2HSV(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2HSV(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from HSV (Hue-Saturation-Value) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageHSV2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, huep, R, G, B, m, chroma, value, saturation;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    hue = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    saturation = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    value = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    chroma = value * saturation;
                    huep = hue * 3.0; // divided by 2: 3 instead of 6.
                    huep = chroma * (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                    if (hue < 1.0 / 6.0)
                    {
                        R = chroma;
                        G = huep;
                        B = 0.0;
                    }
                    else if (hue < 2.0 / 6.0)
                    {
                        R = huep;
                        G = chroma;
                        B = 0.0;
                    }
                    else if (hue < 3.0 / 6.0)
                    {
                        R = 0.0;
                        G = chroma;
                        B = huep;
                    }
                    else if (hue < 4.0 / 6.0)
                    {
                        R = 0.0;
                        G = huep;
                        B = chroma;
                    }
                    else if (hue < 5.0 / 6.0)
                    {
                        R = huep;
                        G = 0.0;
                        B = chroma;
                    }
                    else if (hue < 6.0 / 6.0)
                    {
                        R = chroma;
                        G = 0.0;
                        B = huep;
                    }
                    else
                    {
                        R = 0.0;
                        G = 0.0;
                        B = 0.0;
                    }
                    m = value - chroma;
                    R += m;
                    G += m;
                    B += m;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from HSV (Hue-Saturation-Value) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageHSV2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageHSV2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HWB COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to HWB (Hue-Whiteness-Blackness).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads to process the image concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2HWB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, R, G, B, maximum, minimum, chroma, value, saturation, whiteness, blackness;
                    unsigned int max_index;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // 2.2) Calculate the maximum and minimum values.
                    if (R > G)
                    {
                        maximum = R;
                        max_index = 0;
                        minimum = G;
                    }
                    else
                    {
                        maximum = G;
                        max_index = 1;
                        minimum = R;
                    }
                    if (B > maximum)
                    {
                        maximum = B;
                        max_index = 2;
                    }
                    else if (B < minimum) minimum = B;
                    
                    // 2.3) Calculate the chroma and hue.
                    chroma = maximum - minimum;
                    if (chroma == 0) hue = 0;
                    else if (max_index == 0)
                    {
                        hue = ((G - B) / chroma) / 6.0;
                        if (hue < 0) hue += 1.0;
                    }
                    else if (max_index == 1) hue = ((B - R) / chroma + 2.0) / 6.0;
                    else if (max_index == 2) hue = ((R - G) / chroma + 4.0) / 6.0;
                    value = maximum;
                    saturation = (chroma == 0)?0:chroma / maximum;
                    
                    // 2.4) Now, calculate the whiteness and blackness values.
                    whiteness = (1.0 - saturation) * value;
                    blackness = (1.0 - value);
                    whiteness = srvMax(0.0, srvMin(1.0, whiteness));
                    blackness = srvMax(0.0, srvMin(1.0, blackness));
                    
                    output_ptrs[0][x] = (T2)(hue * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(whiteness * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(blackness * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to HWB (Hue-Whiteness-Blackness).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads to process the image concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the HWB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2HWB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2HWB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from HWB (Hue-Whiteness-Blackness) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageHWB2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, huep, R, G, B, m, chroma, value, saturation, whiteness, blackness;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    hue = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    whiteness = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    blackness = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // Recover the saturation and value from the whiteness and blackness values.
                    value = 1.0 - blackness;
                    if (value != 0) saturation = 1.0 - whiteness / value;
                    else saturation = 0;
                    value = srvMax(0.0, srvMin(1.0, value));
                    saturation = srvMax(0.0, srvMin(1.0, saturation));
                    
                    // Continue with the HSV algorithm...
                    chroma = value * saturation;
                    huep = hue * 3.0; // divided by 2: 3 instead of 6.
                    huep = chroma * (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                    if (hue < 1.0 / 6.0)
                    {
                        R = chroma;
                        G = huep;
                        B = 0.0;
                    }
                    else if (hue < 2.0 / 6.0)
                    {
                        R = huep;
                        G = chroma;
                        B = 0.0;
                    }
                    else if (hue < 3.0 / 6.0)
                    {
                        R = 0.0;
                        G = chroma;
                        B = huep;
                    }
                    else if (hue < 4.0 / 6.0)
                    {
                        R = 0.0;
                        G = huep;
                        B = chroma;
                    }
                    else if (hue < 5.0 / 6.0)
                    {
                        R = huep;
                        G = 0.0;
                        B = chroma;
                    }
                    else if (hue < 6.0 / 6.0)
                    {
                        R = chroma;
                        G = 0.0;
                        B = huep;
                    }
                    else
                    {
                        R = 0.0;
                        G = 0.0;
                        B = 0.0;
                    }
                    m = value - chroma;
                    R += m;
                    G += m;
                    B += m;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from HWB (Hue-Whiteness-Blackness) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageHWB2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageHWB2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HSL COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to HSL (Hue-Saturation-Lightness)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2HSL(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, R, G, B, maximum, minimum, chroma, value, saturation;
                    unsigned int max_index;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // 2.2) Calculate the maximum and minimum values.
                    if (R > G)
                    {
                        maximum = R;
                        max_index = 0;
                        minimum = G;
                    }
                    else
                    {
                        maximum = G;
                        max_index = 1;
                        minimum = R;
                    }
                    if (B > maximum)
                    {
                        maximum = B;
                        max_index = 2;
                    }
                    else if (B < minimum) minimum = B;
                    
                    // 2.3) Calculate the chroma and hue.
                    chroma = maximum - minimum;
                    if (chroma == 0) hue = 0;
                    else if (max_index == 0)
                    {
                        hue = ((G - B) / chroma) / 6.0;
                        if (hue < 0) hue += 1.0;
                    }
                    else if (max_index == 1) hue = ((B - R) / chroma + 2.0) / 6.0;
                    else if (max_index == 2) hue = ((R - G) / chroma + 4.0) / 6.0;
                    value = (maximum + minimum) / 2.0;
                    saturation = (chroma == 0)?0:chroma / (1 - srvAbs<double>(2 * value - 1.0));
                    
                    output_ptrs[0][x] = (T2)(hue * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(saturation * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(value * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to HSL (Hue-Saturation-Lightness)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the HSL color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2HSL(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2HSL(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from HSL (Hue-Saturation-Lightness) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageHSL2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double hue, huep, R, G, B, m, chroma, value, saturation;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    hue = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    saturation = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    value = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    chroma = saturation * (1 - srvAbs<double>(2 * value - 1.0));
                    huep = hue * 3.0; // divided by 2: 3 instead of 6.
                    huep = chroma * (1.0 - srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                    if (hue < 1.0 / 6.0)
                    {
                        R = chroma;
                        G = huep;
                        B = 0.0;
                    }
                    else if (hue < 2.0 / 6.0)
                    {
                        R = huep;
                        G = chroma;
                        B = 0.0;
                    }
                    else if (hue < 3.0 / 6.0)
                    {
                        R = 0.0;
                        G = chroma;
                        B = huep;
                    }
                    else if (hue < 4.0 / 6.0)
                    {
                        R = 0.0;
                        G = huep;
                        B = chroma;
                    }
                    else if (hue < 5.0 / 6.0)
                    {
                        R = huep;
                        G = 0.0;
                        B = chroma;
                    }
                    else if (hue < 6.0 / 6.0)
                    {
                        R = chroma;
                        G = 0.0;
                        B = huep;
                    }
                    else
                    {
                        R = 0.0;
                        G = 0.0;
                        B = 0.0;
                    }
                    m = value - chroma / 2.0;
                    R += m;
                    G += m;
                    B += m;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from HSL (Hue-Saturation-Lightness) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageHSL2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageHSL2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ HSI COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to HSI (Hue-Saturation-Intensity)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2HSI(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        const double pi2 = 6.283185307179586;
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, hue, value, saturation;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    value = (R + G + B) / 3.0;
                    if (value > 0)
                    {
                        saturation = 1.0 - srvMin(R, srvMin(G, B)) / value;
                        if (saturation > 0)
                        {
                            hue = acos((0.5 * ((R - G) + (R - B))) / sqrt((R - G) * (R - G) + (R - B) * (G - B))) / pi2; // Normalize between -0.5 to 0.5
                            if (B / value > G / value) hue = 1.0 - hue; // 0 <= H <= 180, therefore when B / I > G / I, H = 360 - H, so that H \in (0, 360).
                        }
                        else hue = 0.0;
                    }
                    else saturation = hue = 0.0;
                    
                    output_ptrs[0][x] = (T2)(hue * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(saturation * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(value * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to HSI (Hue-Saturation-Intensity)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the HSI color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2HSI(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2HSI(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from HSI (Hue-Saturation-Intensity) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number_of_threads used to process the image concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageHSI2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        const double to_radians = 0.017453292519943295;
        const double sixty_radians = 1.0471975511965976;
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, hue, value, saturation;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    hue = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    saturation = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    value = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    hue = 2 * 3.1415926 * hue; // Hue in radians.
                    if (hue <= 120.0 * to_radians)
                    {
                        B = (1.0 - saturation) / 3.0;
                        R = (1.0 + (saturation * cos(hue)) / cos(sixty_radians - hue)) / 3.0;
                        G = 1.0 - (B + R);
                    }
                    else if (hue <= 240.0 * to_radians)
                    {
                        hue = hue - 120.0 * to_radians;
                        R = (1.0 - saturation) / 3.0;
                        G = (1.0 + (saturation * cos(hue)) / cos(sixty_radians - hue)) / 3.0;
                        B = 1.0 - (R + G);
                    }
                    else
                    {
                        hue = hue - 240.0 * to_radians;
                        G = (1.0 - saturation) / 3.0;
                        B = (1.0 + (saturation * cos(hue)) / cos(sixty_radians - hue)) / 3.0;
                        R = 1.0 - (G + B);
                    }
                    R = srvMin(1.0, srvMax(0.0, 3 * value * R));
                    G = srvMin(1.0, srvMax(0.0, 3 * value * G));
                    B = srvMin(1.0, srvMax(0.0, 3 * value * B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from HSI (Hue-Saturation-Intensity) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number_of_threads used to process the image concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageHSI2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageHSI2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CMY COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to CMY (Cian-Magenta-Yellow)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2CMY(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, C, M, Y;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    C = 1.0 - R;
                    M = 1.0 - G;
                    Y = 1.0 - B;
                    
                    output_ptrs[0][x] = (T2)(C * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(M * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to CMY (Cian-Magenta-Yellow)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the CMY color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2CMY(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2CMY(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from CMY (Cian-Magenta-Yellow) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageCMY2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, C, M, Y;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    C = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    M = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Y = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    R = 1.0 - C;
                    G = 1.0 - M;
                    B = 1.0 - Y;
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from CMY (Cian-Magenta-Yellow) to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageCMY2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageCMY2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CMYK COLOR SPACE TRANSFORM ]===============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to a 4-channel CMYK image (Cian-Magenta-Yellow-Black)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2CMYK(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight()))
            throw Exception("Both images are expected to have the same geometry but, the input image is %dx%d while the output is %dx%d", input.getWidth(), input.getHeight(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != 3) throw Exception("Input image is expected to have 3 channels.");
        if (output.getNumberOfChannels() != 4) throw Exception("Output image is expected to have 4 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[4] = { output.get(y, 0), output.get(y, 1), output.get(y, 2), output.get(y, 3) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, C, M, Y, K;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    C = 1.0 - R;
                    M = 1.0 - G;
                    Y = 1.0 - B;
                    K = srvMin(C, srvMin(M, Y));
                    C = (C - K) / (1.0 - K);
                    M = (M - K) / (1.0 - K);
                    Y = (Y - K) / (1.0 - K);
                    
                    output_ptrs[0][x] = (T2)(C * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(M * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[3][x] = (T2)(K * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to a 4-channel CMYK image (Cian-Magenta-Yellow-Black)
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the CMYK color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2CMYK(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 4);
        ImageRGB2CMYK(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 4-channel image from CMYK (Cian-Magenta-Yellow-Black) to a 3-channel RGB image.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageCMYK2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        if ((input.getWidth() != output.getWidth()) || (input.getHeight() != output.getHeight()))
            throw Exception("Both images are expected to have the same geometry but, the input image is %dx%d while the output is %dx%d", input.getWidth(), input.getHeight(), output.getWidth(), output.getHeight());
        if (input.getNumberOfChannels() != 4) throw Exception("Input image is expected to have 4 channels.");
        if (output.getNumberOfChannels() != 3) throw Exception("Output image is expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[4] = { input.get(y, 0), input.get(y, 1), input.get(y, 2), input.get(y, 3) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, C, M, Y, K;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    C = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    M = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Y = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    K = (double)(input_ptrs[3][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    C = C * (1.0 - K) + K;
                    M = M * (1.0 - K) + K;
                    Y = Y * (1.0 - K) + K;
                    R = 1.0 - C;
                    G = 1.0 - M;
                    B = 1.0 - Y;
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 4-channel image from CMYK (Cian-Magenta-Yellow-Black) to a 3-channel RGB image.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageCMYK2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageCMYK2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ YIQ COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to YIQ. The YIQ is the color space used by the NTSC color TV system, where the Y is for
     *  Luminance/Brightness and I-Q are chroma coordinates.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2YIQ(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, I, Q;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = 0.299 * R + 0.587 * G + 0.114 * B;
                    I = 0.596 * R - 0.274 * G - 0.322 * B; // I ranges between -0.596 to 0.596
                    Q = 0.212 * R - 0.523 * G + 0.311 * B; // Q ranges between -0.523 to 0.523
                    I = (I + 0.596) / (2.0 * 0.596); // I ranges between 0 to 1.
                    Q = (Q + 0.523) / (2.0 * 0.523); // Q ranges between 0 to 1.
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    I = srvMin(1.0, srvMax(0.0, I));
                    Q = srvMin(1.0, srvMax(0.0, Q));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(I * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Q * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to YIQ. The YIQ is the color space used by the NTSC color TV system, where the Y is for
     *  Luminance/Brightness and I-Q are chroma coordinates.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the YIQ color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2YIQ(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2YIQ(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from YIQ to RGB. The YIQ is the color space used by the NTSC color TV system, where the Y is for
     *  Luminance/Brightness and I-Q are chroma coordinates.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageYIQ2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, I, Q;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    I = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Q = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    I = I * 2.0 * 0.596 - 0.596; // I ranges between 0 to 1, change to the original range from -0.596 to 0.596.
                    Q = Q * 2.0 * 0.523 - 0.523; // Q ranges between 0 to 1, change to the original range from -0.523 to 0.523.
                    R = Y + 0.956 * I + 0.621 * Q;
                    G = Y - 0.272 * I - 0.647 * Q;
                    B = Y - 1.105 * I + 1.702 * Q;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from YIQ to RGB. The YIQ is the color space used by the NTSC color TV system, where the Y is for
     *  Luminance/Brightness and I-Q are chroma coordinates.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageYIQ2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageYIQ2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ YUV COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to YUV. The YUV is the color space used by the PAL color TV system, where the Y is for
     *  Luminance/Brightness and U-V are chrominance components: U = B - Y (blue - luminance) and V = R - Y (red - luminance).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2YUV(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, U, V;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y =  0.299 * R + 0.587 * G + 0.114 * B;
                    U = -0.147 * R - 0.289 * G + 0.437 * B; // U ranges between -0.437 to 0.437
                    V =  0.615 * R - 0.515 * G - 0.100 * B; // V ranges between -0.615 to 0.615
                    U = (U + 0.437) / (2.0 * 0.437); // U ranges between 0 to 1.
                    V = (V + 0.615) / (2.0 * 0.615); // V ranges between 0 to 1.
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    U = srvMin(1.0, srvMax(0.0, U));
                    V = srvMin(1.0, srvMax(0.0, V));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(U * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(V * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to YUV. The YUV is the color space used by the PAL color TV system, where the Y is for
     *  Luminance/Brightness and U-V are chrominance components: U = B - Y (blue - luminance) and V = R - Y (red - luminance).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the YUV color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2YUV(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2YUV(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from YUV to RGB. The YUV is the color space used by the PAL color TV system, where the Y is for
     *  Luminance/Brightness and U-V are chrominance components: U = B - Y (blue - luminance) and V = R - Y (red - luminance).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageYUV2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, U, V;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    U = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    V = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    U = U * 2.0 * 0.437 - 0.437; // U ranges between 0 to 1, change to the original range from -0.437 to 0.437.
                    V = V * 2.0 * 0.615 - 0.615; // V ranges between 0 to 1, change to the original range from -0.615 to 0.615.
                    R = Y             + 1.140 * V;
                    G = Y - 0.394 * U - 0.581 * V;
                    B = Y + 2.028 * U            ;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from YUV to RGB. The YUV is the color space used by the PAL color TV system, where the Y is for
     *  Luminance/Brightness and U-V are chrominance components: U = B - Y (blue - luminance) and V = R - Y (red - luminance).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageYUV2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageYUV2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ sRGB COLOR SPACE TRANSFORM ]================================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to sRGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2SRGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, sR, sG, sB;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    sR = (R > 0.04045)?pow((R + 0.055) / 1.055, 2.4): R / 12.92;
                    sG = (G > 0.04045)?pow((G + 0.055) / 1.055, 2.4): G / 12.92;
                    sB = (B > 0.04045)?pow((B + 0.055) / 1.055, 2.4): B / 12.92;
                    sR = srvMin(1.0, srvMax(0.0, sR));
                    sG = srvMin(1.0, srvMax(0.0, sG));
                    sB = srvMin(1.0, srvMax(0.0, sB));
                    
                    output_ptrs[0][x] = (T2)(sR * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(sG * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(sB * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to sRGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the sRGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2SRGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2SRGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from sRGB to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageSRGB2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, sR, sG, sB;
                    
                    // 2.1) Normalize the color values between 0 to 1.
                    sR = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    sG = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    sB = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    R = (sR > 0.0031308)?pow(sR, 1.0 / 2.4) * 1.055 - 0.055:sR * 12.92;
                    G = (sG > 0.0031308)?pow(sG, 1.0 / 2.4) * 1.055 - 0.055:sG * 12.92;
                    B = (sB > 0.0031308)?pow(sB, 1.0 / 2.4) * 1.055 - 0.055:sB * 12.92;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from sRGB to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageSRGB2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageSRGB2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CIE XYZ COLOR SPACE TRANSFORM ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to CIE XYZ.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2CIEXYZ(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, X, Y, Z;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    X = 0.4124564 * R + 0.3575761 * G + 0.1804375 * B;
                    Y = 0.2126729 * R + 0.7151522 * G + 0.0721750 * B;
                    Z = 0.0193339 * R + 0.1191920 * G + 0.9503041 * B;
                    X = srvMin(1.0, srvMax(0.0, X / (0.4124564 + 0.3575761 + 0.1804375))); // Normalize the values to be between 0 and 1.
                    Y = srvMin(1.0, srvMax(0.0, Y / (0.2126729 + 0.7151522 + 0.0721750)));
                    Z = srvMin(1.0, srvMax(0.0, Z / (0.0193339 + 0.1191920 + 0.9503041)));
                    
                    output_ptrs[0][x] = (T2)(X * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Z * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to CIE XYZ.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the XYZ color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2CIEXYZ(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2CIEXYZ(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from CIE XYZ to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageCIEXYZ2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, X, Y, Z;
                    
                    X = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Y = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Z = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    X = X * (0.4124564 + 0.3575761 + 0.1804375); // Denormalize the values.
                    Y = Y * (0.2126729 + 0.7151522 + 0.0721750);
                    Z = Z * (0.0193339 + 0.1191920 + 0.9503041);
                    R =  3.2404542 * X - 1.5371385 * Y - 0.4985314 * Z;
                    G = -0.9692660 * X + 1.8760108 * Y + 0.0415560 * Z;
                    B =  0.0556434 * X - 0.2040259 * Y + 1.0572252 * Z;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from CIE XYZ to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageCIEXYZ2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageCIEXYZ2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ CIE LAB COLOR SPACE TRANSFORM ]============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to CIE L*a*b.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2CIELab(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    const double X_ref = 0.950429; // D65 reference white - Daylight.
                    const double Y_ref = 1.000;
                    const double Z_ref = 1.0889;
                    double R, G, B, X, Y, Z, fx, fy, fz, L, a, b, t;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // RGB -> XYZ
                    X = 0.4124564 * R + 0.3575761 * G + 0.1804375 * B;
                    Y = 0.2126729 * R + 0.7151522 * G + 0.0721750 * B;
                    Z = 0.0193339 * R + 0.1191920 * G + 0.9503041 * B;
                    
                    // XYZ -> L*a*b
                    t = X / X_ref; fx = ((t > 0.008856)?pow(t, 1.0 / 3.0):7.787 * t + 16.0 / 116.0); // fx, fy, fz range between 0 and 1.
                    t = Y / Y_ref; fy = ((t > 0.008856)?pow(t, 1.0 / 3.0):7.787 * t + 16.0 / 116.0);
                    t = Z / Z_ref; fz = ((t > 0.008856)?pow(t, 1.0 / 3.0):7.787 * t + 16.0 / 116.0);
                    L = 116.0 * fy - 16.0;
                    a = 500.0 * (fx - fy);
                    b = 200.0 * (fy - fz);
                    L = srvMax(0.0, srvMin(1.0, L / 100.0));
                    a = srvMax(0.0, srvMin(1.0, (a + 100) / 200.0));
                    b = srvMax(0.0, srvMin(1.0, (b + 100) / 200.0));
                    
                    output_ptrs[0][x] = (T2)(L * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(a * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(b * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to CIE L*a*b.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the CIE L*a*b color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2CIELab(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2CIELab(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from CIE L*a*b to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageCIELab2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    const double X_ref = 0.950429; // D65 reference white - Daylight.
                    const double Y_ref = 1.000;
                    const double Z_ref = 1.0889;
                    const double delta = 6.0 / 29.0;
                    double R, G, B, X, Y, Z, L, a, b, fx, fy, fz;
                    
                    L = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    a = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    b = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    L = L * 100;
                    a = a * 200 - 100;
                    b = b * 200 - 100;
                    fy = (L + 16.0) / 116.0;
                    fx = fy + (a / 500.0);
                    fz = fy - (b / 200.0);
                    X = (fx > delta) ? X_ref * (fx * fx * fx) : (fx - 16.0 / 116.0) * 3 * (delta * delta) * X_ref;
                    Y = (fy > delta) ? Y_ref * (fy * fy * fy) : (fy - 16.0 / 116.0) * 3 * (delta * delta) * Y_ref;
                    Z = (fz > delta) ? Z_ref * (fz * fz * fz) : (fz - 16.0 / 116.0) * 3 * (delta * delta) * Z_ref;
                    
                    R =  3.2404542 * X - 1.5371385 * Y - 0.4985314 * Z;
                    G = -0.9692660 * X + 1.8760108 * Y + 0.0415560 * Z;
                    B =  0.0556434 * X - 0.2040259 * Y + 1.0572252 * Z;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from CIE L*a*b to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageCIELab2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageCIELab2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ OHTA COLOR SPACE TRANSFORM ]===============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to OHTA (approximates principal components transformation).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2OHTA(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, L1, L2, L3;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    L1 = (R + G + B) / 3.0;
                    L2 = (R - B) / 2.0;
                    L3 = (G / 2.0 - R / 4.0 - B / 4.0);
                    L1 = srvMin(1.0, srvMax(0.0, L1));
                    L2 = srvMin(1.0, srvMax(0.0, L2 + 0.5));
                    L3 = srvMin(1.0, srvMax(0.0, L3 + 0.5));
                    
                    output_ptrs[0][x] = (T2)(L1 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(L2 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(L3 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to OHTA (approximates principal components transformation).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the OHTA color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2OHTA(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2OHTA(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from OHTA to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageOHTA2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, L1, L2, L3;
                    
                    L1 = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    L2 = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    L3 = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    L2 = L2 - 0.5;
                    L3 = L3 - 0.5;
                    R = L1 + L2 - 2.0 * L3 / 3.0;
                    G = L1 + 4.0 * L3 / 3.0;
                    B = L1 - L2 - 2.0 * L3 / 3.0;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from OHTA to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageOHTA2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageOHTA2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ REC.601 YCbCr COLOR SPACE TRANSFORM ]======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to Rec601YCbCr. This is a variant of the Y'CbCr color space where Rec601 is the abbreviation of 
     *  ITU-R Recommendation BT.601 (standard that was defined for standard-definition television).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Rec601YCbCr(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = 0.299 * R + 0.587 * G + 0.114 * B;
                    Cb = -0.168736 * R - 0.331264 * G + 0.5 * B;
                    Cr = 0.5 * R - 0.418688 * G - 0.081312 * B;
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    Cb = srvMin(1.0, srvMax(0.0, Cb + 0.5));
                    Cr = srvMin(1.0, srvMax(0.0, Cr + 0.5));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(Cb * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Cr * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
   
    
    /** Convert a 3-channel image from RGB to Rec601YCbCr. This is a variant of the Y'CbCr color space where Rec601 is the abbreviation of 
     *  ITU-R Recommendation BT.601 (standard that was defined for standard-definition television).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the Rec.601 YCbCr color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Rec601YCbCr(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2Rec601YCbCr(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from Rec601YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRec601YCbCr2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cb = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cr = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Cb = Cb - 0.5;
                    Cr = Cr - 0.5;
                    R = Y +                         +  1.401999588657340 * Cr;
                    G = Y -  0.344135678165337 * Cb -  0.714136155581812 * Cr;
                    B = Y +  1.772000066073820 * Cb;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from Rec601YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRec601YCbCr2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRec601YCbCr2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ REC.709 YCbCr COLOR SPACE TRANSFORM ]======================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to Rec709YCbCr. This is a variant of the Y'CbCr color space where Rec709 is the abbreviation of 
     *  ITU-R Recommendation BT.709 (standard that was defined primarily for HDTV use).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Rec709YCbCr(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = 0.2126 * R + 0.7152 * G + 0.0722 * B;
                    Cb = -0.114572 * R - 0.385428 * G + 0.5 * B;
                    Cr = 0.5 * R - 0.454153 * G - 0.045847 * B;
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    Cb = srvMin(1.0, srvMax(0.0, Cb + 0.5));
                    Cr = srvMin(1.0, srvMax(0.0, Cr + 0.5));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(Cb * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Cr * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to Rec709YCbCr. This is a variant of the Y'CbCr color space where Rec709 is the abbreviation of 
     *  ITU-R Recommendation BT.709 (standard that was defined primarily for HDTV use).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the Rec.709 YCbCr color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Rec709YCbCr(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2Rec709YCbCr(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from Rec709YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRec709YCbCr2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cb = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cr = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Cb = Cb - 0.5;
                    Cr = Cr - 0.5;
                    R = Y +                         +  1.574799932402920 * Cr;
                    G = Y -  0.187324181518030 * Cb -  0.468124212249768 * Cr;
                    B = Y +  1.855599963134660 * Cb;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from Rec709YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRec709YCbCr2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRec709YCbCr2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ YCbCr COLOR SPACE TRANSFORM ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to YCbCr.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2YCbCr(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = 0.299 * R + 0.587 * G + 0.114 * B;
                    Cb = -0.168736 * R - 0.331264 * G + 0.5 * B;
                    Cr = 0.5 * R - 0.418688 * G - 0.081312*B;
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    Cb = srvMin(1.0, srvMax(0.0, Cb + 0.5));
                    Cr = srvMin(1.0, srvMax(0.0, Cr + 0.5));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(Cb * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Cr * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to YCbCr.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the YCbCr color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2YCbCr(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2YCbCr(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageYCbCr2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Cb, Cr;
                    
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cb = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Cr = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Cb = Cb - 0.5;
                    Cr = Cr - 0.5;
                    R = Y +                         +  1.401999588657340 * Cr;
                    G = Y -  0.344135678165337 * Cb -  0.714136155581812 * Cr;
                    B = Y +  1.772000066073820 * Cb;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from YCbCr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageYCbCr2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageYCbCr2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ YDbDr COLOR SPACE TRANSFORM ]==============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to YDbDr.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2YDbDr(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Db, Dr;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = 0.299 * R + 0.587 * G + 0.114 * B;
                    Db = -0.45 * R - 0.883 * G + 1.333 * B;
                    Dr = -1.333 * R + 1.116 * G + 0.217 * B;
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    Db = srvMin(1.0, srvMax(0.0, Db / 2.666 + 0.5));
                    Dr = srvMin(1.0, srvMax(0.0, Dr / 2.666 + 0.5));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(Db * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(Dr * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to YDbDr.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the YDbDr color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2YDbDr(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2YDbDr(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from YDbDr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageYDbDr2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, Db, Dr;
                    
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Db = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    Dr = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Db = (Db - 0.5) * 2.666;
                    Dr = (Dr - 0.5) * 2.666;
                    R = Y + 0.000092303716148 * Db - 0.525912630661865 * Dr;
                    G = Y - 0.129132898890509 * Db + 0.267899328207599 * Dr;
                    B = Y + 0.664679059978955 * Db - 0.000079202543533 * Dr;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from YDbDr to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageYDbDr2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageYDbDr2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ MATRIX COLOR SPACE TRANSFORM ]=============================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel color image using the given matrix.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] matrix constant array to a matrix stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]]).
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageColorMatrix(const INPUT_IMAGE<T1> &input, const double *matrix, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double IN0, IN1, IN2, OUT0, OUT1, OUT2;
                    
                    IN0 = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    IN1 = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    IN2 = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    OUT0 = matrix[0] * IN0 + matrix[1] * IN1 + matrix[2] * IN2;
                    OUT1 = matrix[3] * IN0 + matrix[4] * IN1 + matrix[5] * IN2;
                    OUT2 = matrix[6] * IN0 + matrix[7] * IN1 + matrix[8] * IN2;
                    
                    OUT0 = srvMin(1.0, srvMax(0.0, OUT0));
                    OUT1 = srvMin(1.0, srvMax(0.0, OUT1));
                    OUT2 = srvMin(1.0, srvMax(0.0, OUT2));
                    
                    output_ptrs[0][x] = (T2)(OUT0 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(OUT1 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(OUT2 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel color image using the given matrix.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] matrix constant array to a matrix stored in rows (i.e.\ M = [m[0], m[1], m[2]; m[3], m[4], m[5]; m[6], m[7], m[8]]).
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image transformed to the color spaces of the given matrix.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageColorMatrix(const IMAGE<T> &input, const double *matrix, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageColorMatrix(input, matrix, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ OPPONENT COLOR SPACE TRANSFORM ]===========================================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to opponent color space.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Opponent(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, O1, O2, O3;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // Calculate the opponent color space.
                    O1 = (R - G);
                    O2 = (R + G - 2.0 * B);
                    O3 = (R + G + B);
                    // Rescale the color space so that all channels have the same contribution.
                    O1 = srvMin(1.0, srvMax(0.0, O1 / 2.0 + 0.5));
                    O2 = srvMin(1.0, srvMax(0.0, O2 / 4.0 + 0.5));
                    O3 = srvMin(1.0, srvMax(0.0, O3 / 3.0));
                    
                    output_ptrs[0][x] = (T2)(O1 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(O2 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(O3 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to opponent color space.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the opponents color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Opponent(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2Opponent(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from opponent color space to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageOpponent2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, O1, O2, O3;
                    
                    O1 = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    O2 = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    O3 = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    O1 = (O1 - 0.5) * 2.0;
                    O2 = (O2 - 0.5) * 4.0;
                    O3 = O3 * 3.0;
                    B = -(O2 - O3) / 3.0;
                    R = (O2 + 2.0 * B + O1) / 2.0;
                    G = (O2 + 2.0 * B - O1) / 2.0;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from opponent color space to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the images concurrently.
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageOpponent2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageOpponent2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ NORMALIZED OPPONENT COLOR SPACE TRANSFORM - USED BY C-SIFT ]===============================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to normalized opponent color space.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2NormalizedOpponent(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, O1, O2, O3, nO1, nO2, nO3;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    // Calculate the opponent color space.
                    O1 = (R - G);
                    O2 = (R + G - 2.0 * B);
                    O3 = (R + G + B);
                    // Normalize the two chromatic components by the intensity component.
                    nO1 = (O3 > 0.0)?O1 / O3:0.0;
                    nO2 = (O3 > 0.0)?O2 / O3:0.0;
                    nO3 = (R + G + B) / 3.0;
                    // Rescale the color space so that all channels have the same contribution.
                    nO1 = srvMin(1.0, srvMax(0.0, (nO1 + 1.0) / 2.0));
                    nO2 = srvMin(1.0, srvMax(0.0, (nO2 + 2.0) / 3.0));
                    nO3 = srvMin(1.0, srvMax(0.0, nO3));
                    
                    output_ptrs[0][x] = (T2)(nO1 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(nO2 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(nO3 * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to normalized opponent color space.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the Normalized Opponent color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2NormalizedOpponent(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2NormalizedOpponent(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from normalized opponent color space to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageNormalizedOpponent2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, O1n, O2n, O3n, O1, O2, O3;
                    
                    O1n = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    O2n = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    O3n = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    O1n = O1n * 2.0 - 1.0;
                    O2n = O2n * 3.0 - 2.0;
                    O3 = O3n * 3.0;
                    O2 = O2n * O3;
                    O1 = O1n * O3;
                    B = -(O2 - O3) / 3.0;
                    R = (O2 + 2.0 * B + O1) / 2.0;
                    G = (O2 + 2.0 * B - O1) / 2.0;
                    
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    /** Convert a 3-channel image from normalized opponent color space to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageNormalizedOpponent2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageNormalizedOpponent2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Yrg COLOR SPACE TRANSFORM - USED BY rg-SIFT ]==============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Convert a 3-channel image from RGB to Yrg (Y for luminance and r-g are the chroma channels).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageRGB2Yrg(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, r, g;
                    
                    R = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    G = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    B = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = (R + G + B) / 3.0;
                    r = (Y > 0.0)?R / (R + G + B):0;
                    g = (Y > 0.0)?G / (R + G + B):0;
                    Y = srvMin(1.0, srvMax(0.0, Y));
                    r = srvMin(1.0, srvMax(0.0, r));
                    g = srvMin(1.0, srvMax(0.0, g));
                    
                    output_ptrs[0][x] = (T2)(Y * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(r * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(g * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from RGB to Yrg (Y for luminance and r-g are the chroma channels).
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the Yrg color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageRGB2Yrg(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageRGB2Yrg(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    /** Convert a 3-channel image from Yrg to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[out] output 3-channel output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageYrg2RGB(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T1 &min_input_value = (T1)0, const T1 &max_input_value = (T1)255, const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        if (output.getNumberOfChannels() != 3) throw Exception("Both images are expected to have 3 channels.");
        
        // 2) Image color conversion
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
            {
                const T1 *input_ptrs[3] = { input.get(y, 0), input.get(y, 1), input.get(y, 2) };
                T2 *output_ptrs[3] = { output.get(y, 0), output.get(y, 1), output.get(y, 2) };
                for (unsigned int x = 0; x < output.getWidth(); ++x)
                {
                    double R, G, B, Y, r, g;
                    
                    Y = (double)(input_ptrs[0][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    r = (double)(input_ptrs[1][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    g = (double)(input_ptrs[2][x] - min_input_value) / (double)(max_input_value - min_input_value);
                    
                    Y = Y * 3;
                    R = r * Y;
                    G = g * Y;
                    B = Y - R - G;
                    R = srvMin(1.0, srvMax(0.0, R));
                    G = srvMin(1.0, srvMax(0.0, G));
                    B = srvMin(1.0, srvMax(0.0, B));
                    
                    output_ptrs[0][x] = (T2)(R * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[1][x] = (T2)(G * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    output_ptrs[2][x] = (T2)(B * (double)(max_output_value - min_output_value) + (double)min_output_value);
                }
            }
        }
    }
    
    /** Convert a 3-channel image from Yrg to RGB.
     *  \param[in] input 3-channel input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_input_value minimum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 0.
     *  \param[in] max_input_value maximum value of input image pixels. This parameter is used to normalize input values between 0 and 1. By default, this parameter is set to 255.
     *  \param[in] min_output_value minimum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. This parameter is used to set the range of the output image pixel values. By default, this parameter is set to 255.
     *  \returns image in the RGB color space.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageYrg2RGB(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_input_value = (T)0, const T &max_input_value = (T)255, const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), 3);
        ImageYrg2RGB(input, output, number_of_threads, min_input_value, max_input_value, min_output_value, max_output_value);
        return output;
    }
    
    //                                      /\.
    //                                     /  \.
    //                                    /    \.
    //                                   +-+  +-+.
    //                                     |  |.
    //                                     +--+.
    // =[ Maximize the contrast of the images - MAXCON ]=============================================================================================
    //                                     +--+.
    //                                     |  |.
    //                                   +-+  +-+.
    //                                    \    /.
    //                                     \  /.
    //                                      \/.
    
    /** Maximizes the contrast of the image by setting the minimum and maximum pixel values of the image to the specified limits.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageMaxcon(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        VectorDense<T1> minimum_value(number_of_threads), maximum_value(number_of_threads);
        
        // 1) Check the images geometry.
        checkGeometry(input, output);
        
        // 2) Get the minimum and maximum values of the channel.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            minimum_value[thread_identifier] = std::numeric_limits<T1>::max();
            maximum_value[thread_identifier] = std::numeric_limits<T1>::min();
            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        if (input_ptr[x] < minimum_value[thread_identifier]) minimum_value[thread_identifier] = input_ptr[x];
                        if (input_ptr[x] > maximum_value[thread_identifier]) maximum_value[thread_identifier] = input_ptr[x];
                    }
                }
            }
        }
        
        // 2.a) Merge the minimum and maximum values for the multi-threaded version of the algorithm.
        for (unsigned int i = 1; i < number_of_threads; ++i)
        {
            minimum_value[0] = srvMin(minimum_value[0], minimum_value[i]);
            maximum_value[0] = srvMax(maximum_value[0], maximum_value[i]);
        }
        
        // 3) Create the output image by maximizing the contrast of the input image.
        #pragma omp parallel num_threads(number_of_threads)
        {
            const unsigned int thread_identifier = omp_get_thread_num();
            for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
            {
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    T2 *output_ptr = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double value = srvMin(1.0, srvMax(0.0, (double)(input_ptr[x] - minimum_value[0]) / (double)(maximum_value[0] - minimum_value[0])));
                        output_ptr[x] = (T2)(value * (double)(max_output_value - min_output_value) + (double)min_output_value);
                    }
                }
            }
        }
    }
    
    /** Maximizes the contrast of the image by setting the minimum and maximum pixel values of the image to the specified limits.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. By default, this parameter is set to 255.
     *  \returns contrast maximized image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageMaxcon(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels());
        ImageMaxcon(input, output, number_of_threads, min_output_value, max_output_value);
        return output;
    }
    
    /** Maximizes the contrast of the each image channel separately by setting the minimum and maximum pixel values of the image to the specified limits.
     *  \param[in] input input image or sub-image.
     *  \param[out] output output image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. By default, this parameter is set to 255.
     */
    template <template <class> class INPUT_IMAGE, class T1, template <class> class OUTPUT_IMAGE, class T2>
    void ImageMaxconChannel(const INPUT_IMAGE<T1> &input, OUTPUT_IMAGE<T2> &output, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T2 &min_output_value = (T2)0, const T2 &max_output_value = (T2)255)
    {
        // 1) Check the images geometry.
        checkGeometry(input, output);
        
        // 2) Maximize the contrast of each channel of the image separately.
        VectorDense<T1> minimum_value(number_of_threads), maximum_value(number_of_threads);
        for (unsigned int c = 0; c < output.getNumberOfChannels(); ++c)
        {
            // 3) Get the minimum and maximum values of the channel.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                minimum_value[thread_identifier] = std::numeric_limits<T1>::max();
                maximum_value[thread_identifier] = std::numeric_limits<T1>::min();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        if (input_ptr[x] < minimum_value[thread_identifier]) minimum_value[thread_identifier] = input_ptr[x];
                        if (input_ptr[x] > maximum_value[thread_identifier]) maximum_value[thread_identifier] = input_ptr[x];
                    }
                }
            }
            
            // 4) Merge the minimum and maximum values for the multi-threaded version of the algorithm.
            for (unsigned int i = 1; i < number_of_threads; ++i)
            {
                minimum_value[0] = srvMin(minimum_value[0], minimum_value[i]);
                maximum_value[0] = srvMax(maximum_value[0], maximum_value[i]);
            }
                
            // 5) Maximize the contrast of the current channel.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                for (unsigned int y = thread_identifier; y < output.getHeight(); y += number_of_threads)
                {
                    const T1 *input_ptr = input.get(y, c);
                    T2 *output_ptr = output.get(y, c);
                    for (unsigned int x = 0; x < output.getWidth(); ++x)
                    {
                        double value = srvMin(1.0, srvMax(0.0, (double)(input_ptr[x] - minimum_value[0]) / (double)(maximum_value[0] - minimum_value[0])));
                        output_ptr[x] = (T2)(value * (double)(max_output_value - min_output_value) + min_output_value);
                    }
                }
            }
        }
    }
    
    /** Maximizes the contrast of the each image channel separately by setting the minimum and maximum pixel values of the image to the specified limits.
     *  \param[in] input input image or sub-image.
     *  \param[in] number_of_threads number of threads used to process the image concurrently. By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor(). By default set to the number of threads specified by \b DefaultNumberOfThreads::ImageColor().
     *  \param[in] min_output_value minimum value of output image pixels. By default, this parameter is set to 0.
     *  \param[in] max_output_value maximum value of output image pixels. By default, this parameter is set to 255.
     *  \returns maximized contrast per channel image.
     */
    template <template <class> class IMAGE, class T>
    inline Image<T> ImageMaxconChannel(const IMAGE<T> &input, unsigned int number_of_threads = DefaultNumberOfThreads::ImageColor(), const T &min_output_value = (T)0, const T &max_output_value = (T)255)
    {
        Image<T> output(input.getWidth(), input.getHeight(), input.getNumberOfChannels());
        ImageMaxconChannel(input, output, number_of_threads, min_output_value, max_output_value);
        return output;
    }
    
    /** Increases the local contrast of the image.
     *  \param[in] input input constant sub-image.
     *  \param[in] factor power factor used to increase the contrast of the image.
     *  \param[in] window_size size of the window used to calculate the local image morphological closing.
     *  \param[in] minimum_value minimum value of the output image.
     *  \param[in] maximum_value maximum value of the output image.
     *  \param[out] output resulting sub-image.
     *  \param[in] number_of_threads number of threads used to concurrently process the image.
     */
    template <class TINPUT, class TOUTPUT>
    void ImageWhitening(const ConstantSubImage<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value, SubImage<TOUTPUT>  output, unsigned int number_of_threads)
    {
        const unsigned int number_of_bins = 256;
        const unsigned int width = input.getWidth();
        const unsigned int height = input.getHeight();
        const unsigned int number_of_channels = input.getNumberOfChannels();
        srv::Image<TINPUT> image_intermid;
        srv::Image<double> image_background;
        double * maximum_division, * minimum_division;
        TOUTPUT * conversion_lut;
        
        if ((width != output.getWidth()) || (height != output.getHeight()) || (number_of_channels != output.getNumberOfChannels()))
            throw srv::Exception("Both image must have the same geometry (%dx%dx%d != %dx%dx%d).", width, height, number_of_channels, output.getWidth(), output.getHeight(), output.getNumberOfChannels());
        
        // Image closing.
        srv::ImageLocalExtrema(         input, window_size, window_size,  true, image_intermid  , number_of_threads);
        srv::ImageLocalExtrema(image_intermid, window_size, window_size, false, image_background, number_of_threads);
        
        maximum_division = new double[number_of_threads];
        minimum_division = new double[number_of_threads];
        conversion_lut = new TOUTPUT[number_of_bins];
        for (unsigned int c = 0; c < number_of_channels; ++c)
        {
            // Calculate the division between the original pixel value and its background estimation. Calculate also the maximum and minimum values of the division.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
#if ((__GNUC__ > 4) || ((__GNUC__ == 4) && (__GNUC_MINOR__ >= 7)))
                maximum_division[thread_identifier] = std::numeric_limits<double>::lowest();
#else
                maximum_division[thread_identifier] = (*image_background.get(thread_identifier, 0, c) != 0)?((double)*input.get(thread_identifier, 0, c) / *image_background.get(thread_identifier, 0, c)):0;
#endif
                minimum_division[thread_identifier] = std::numeric_limits<double>::max();
                for (unsigned int y = 0; y < height; ++y)
                {
                    const TINPUT * __restrict__ ptr_in = input.get(thread_identifier, y, c);
                    double * __restrict__ ptr_div = image_background.get(thread_identifier, y, c);
                    
                    for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_in += number_of_threads, ptr_div += number_of_threads)
                    {
                        double value = (*ptr_div != 0)?((double)*ptr_in / *ptr_div):1.0;
                        if (value > maximum_division[thread_identifier]) maximum_division[thread_identifier] = value;
                        if (value < minimum_division[thread_identifier]) minimum_division[thread_identifier] = value;
                        *ptr_div = value;
                    }
                }
            }
            // Merge the maximum and minimum values of each thread.
            for (unsigned int t = 1; t < number_of_threads; ++t)
            {
                if (maximum_division[t] > maximum_division[0]) maximum_division[0] = maximum_division[t];
                if (minimum_division[t] < minimum_division[0]) minimum_division[0] = minimum_division[t];
            }
            
            // Calculate the LUT that converts each division result into the final pixel value.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int i = thread_identifier; i < number_of_bins; i += number_of_threads)
                {
                    double value = (double)(i + 1) / (double)number_of_bins * (maximum_division[0] - minimum_division[0]) + minimum_division[0];
                    conversion_lut[i] = (TOUTPUT)((maximum_value - minimum_value) * (0.5 - 0.5 * std::cos(std::pow(value, factor) * c_pi)) + minimum_value);
                }
            }
            // Use the LUT to replace the foreground-background ratios by its final value.
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int y = 0; y < height; ++y)
                {
                    const double * __restrict__ ptr_div = image_background.get(thread_identifier, y, c);
                    TOUTPUT * __restrict__ ptr_out = output.get(thread_identifier, y, c);
                    
                    for (unsigned int x = thread_identifier; x < width; x += number_of_threads, ptr_out += number_of_threads, ptr_div += number_of_threads)
                    {
                        const double value = (*ptr_div - minimum_division[0]) / (maximum_division[0] - minimum_division[0]);
                        *ptr_out = conversion_lut[std::max(0U, std::min(number_of_bins - 1, (unsigned int)(number_of_bins * value)))];
                    }
                }
            }
        }
        
        delete [] maximum_division;
        delete [] minimum_division;
        delete [] conversion_lut;
    }
    // Calls of the whitening function using different image types.
    template <class TINPUT, class TOUTPUT> void ImageWhitening(const         SubImage<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            ImageWhitening(ConstantSubImage<TINPUT>(input), factor, window_size, minimum_value, maximum_value,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageWhitening(const            Image<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value, SubImage<TOUTPUT>  output, unsigned int number_of_threads) {                            ImageWhitening(ConstantSubImage<TINPUT>(input), factor, window_size, minimum_value, maximum_value,                   output , number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageWhitening(const ConstantSubImage<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); ImageWhitening(                         input , factor, window_size, minimum_value, maximum_value, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageWhitening(const         SubImage<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); ImageWhitening(ConstantSubImage<TINPUT>(input), factor, window_size, minimum_value, maximum_value, SubImage<TOUTPUT>(output), number_of_threads); }
    template <class TINPUT, class TOUTPUT> void ImageWhitening(const            Image<TINPUT> &input, double factor, unsigned int window_size, double minimum_value, double maximum_value,    Image<TOUTPUT> &output, unsigned int number_of_threads) { output.setGeometry(input); ImageWhitening(ConstantSubImage<TINPUT>(input), factor, window_size, minimum_value, maximum_value, SubImage<TOUTPUT>(output), number_of_threads); }
    
}

#endif

