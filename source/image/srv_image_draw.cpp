// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_image_draw.hpp"
#include "../srv_xml.hpp"

namespace srv
{
    //                   +--------------------------------------+
    //                   | PRIVATE AUXILIARY FUNCTIONS          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void Draw::LinePoints(int x0, int y0, int x1, int y1, int width, int height, bool connectivity4, std::list<Tuple<int, int> > &points)
    {
        int dx, dy, sx, sy, error, e2;
        
        dx = srvAbs(x0 - x1);
        dy = srvAbs(y0 - y1);
        sx = (x0 < x1)?1:-1;
        sy = (y0 < y1)?1:-1;
        error  = dx - dy;
        
        if ((x0 >= 0) && (y0 >= 0) && (x0 < width) && (y0 < height))
            points.push_back(Tuple<int, int>(x0, y0));
        while ((x0 != x1) || (y0 != y1))
        {
            e2 = 2 * error;
            if (e2 > -dy)
            {
                error -= dy;
                x0 += sx;
                if (connectivity4 && (e2 < dx))
                {
                    error += dx;
                    y0 += sy;
                }
            }
            else if (e2 < dx)
            {
                error += dx;
                y0 += sy;
            }
            if ((x0 >= 0) && (y0 >= 0) && (x0 < width) && (y0 < height))
                points.push_back(Tuple<int, int>(x0, y0));
        }
    }
    
    void Draw::LinePoints(int x0, int y0, int x1, int y1, bool connectivity4, std::list<Tuple<int, int> > &points)
    {
        int dx, dy, sx, sy, error, e2;
        
        dx = srvAbs(x0 - x1);
        dy = srvAbs(y0 - y1);
        sx = (x0 < x1)?1:-1;
        sy = (y0 < y1)?1:-1;
        error  = dx - dy;
        
        points.push_back(Tuple<int, int>(x0, y0));
        while ((x0 != x1) || (y0 != y1))
        {
            e2 = 2 * error;
            if (e2 > -dy)
            {
                error -= dy;
                x0 += sx;
                if (connectivity4 && (e2 < dx))
                {
                    error += dx;
                    y0 += sy;
                }
            }
            else if (e2 < dx)
            {
                error += dx;
                y0 += sy;
            }
            points.push_back(Tuple<int, int>(x0, y0));
        }
    }
    
    bool Draw::CutLineCoordinates(int &x0, int &y0, int &x1, int &y1, int width, int height)
    {
        double w, h, dx, dy, ox, oy, alpha, best_alpha;
        
        w = (double)(width - 1);
        h = (double)(height - 1);
        if ((x0 < 0) || (y0 < 0) || (x0 >= width) || (y0 >= height))
        {
            dx = (double)(x1 - x0);
            dy = (double)(y1 - y0);
            ox = (double)x0;
            oy = (double)y0;
            best_alpha = std::numeric_limits<double>::max();
            
            if (dx != 0.0)
            {
                alpha = (0 - ox) / dx;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
                alpha = (w - ox) / dx;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
            }
            
            if (dy != 0.0)
            {
                alpha = (0 - oy) / dy;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
                alpha = (h - oy) / dy;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
            }
            if (best_alpha < 1)
            {
                x0 = (int)(ox + best_alpha * dx);
                y0 = (int)(oy + best_alpha * dy);
            }
            // There is not crossing point?
            else return false;
        }
        // If the second pixel is outside the image, then recalculate its coordinates inside the image.
        if ((x1 < 0) || (y1 < 0) || (x1 >= width) || (y1 >= height))
        {
            dx = (double)(x0 - x1);
            dy = (double)(y0 - y1);
            ox = (double)x1;
            oy = (double)y1;
            best_alpha = std::numeric_limits<double>::max();
            
            if (dx != 0.0)
            {
                alpha = (0 - ox) / dx;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
                alpha = (w - ox) / dx;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
            }
            
            if (dy != 0.0)
            {
                alpha = (0 - oy) / dy;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
                alpha = (h - oy) / dy;
                if ((alpha >= 0) && (alpha < best_alpha)) best_alpha = alpha;
            }
            if (best_alpha < 1)
            {
                x1 = (int)(ox + best_alpha * dx);
                y1 = (int)(oy + best_alpha * dy);
            }
            // There must be a crossing point because the point (x0, y0) is now inside the image.
        }
        return true;
    }
    
    void Draw::CirclePoints(int xc, int yc, int radius, std::list<Tuple<double, double> > &points)
    {
        if (radius < 1) return;
        else
        {
            int r2 = radius * radius;
            int y = 0;
            double value = (double)radius;
            
            points.push_back(Tuple<double, double>(xc, yc + radius));
            points.push_back(Tuple<double, double>(xc, yc - radius));
            points.push_back(Tuple<double, double>(xc + radius, yc));
            points.push_back(Tuple<double, double>(xc - radius, yc));
            y = 1;
            while ((double)y < value)
            {
                value = sqrt(r2 - (double)(y * y));
                
                points.push_back(Tuple<double, double>((double)xc + value, (double)yc + y));
                points.push_back(Tuple<double, double>((double)xc - value, (double)yc + y));
                points.push_back(Tuple<double, double>((double)xc + value, (double)yc - y));
                points.push_back(Tuple<double, double>((double)xc - value, (double)yc - y));
                points.push_back(Tuple<double, double>((double)xc + y, (double)yc + value));
                points.push_back(Tuple<double, double>((double)xc - y, (double)yc + value));
                points.push_back(Tuple<double, double>((double)xc + y, (double)yc - value));
                points.push_back(Tuple<double, double>((double)xc - y, (double)yc - value));
                ++y;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | FONT CLASS AND TEXT DRAW FUNCTIONS   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Default constructor.
    Draw::Font::Font(void) :
        m_line_height(0),
        m_break_line(false)
    {
        for (unsigned int i = 0; i < 256; ++i)
        {
            m_width[i] = 0;
            m_height[i] = 0;
            m_advance[i] = 0;
            m_offset_x[i] = 0;
            m_offset_y[i] = 0;
            m_characters[i] = 0;
        }
    }
    
    Draw::Font::Font(FONT_TYPE font_type, bool break_line) :
        m_break_line(break_line)
    {
        int character_size;
        unsigned char *bitcode;
        // 1) Set the width and height of the characters to initialize the structures.
        switch (font_type)
        {
        case TINY_FONT_5x7:
            m_line_height = 8;
            for (unsigned int i = 0; i < 256; ++i)
            {
                m_width[i] = 5;
                m_height[i] = 7;
                m_advance[i] = 6;
                m_offset_x[i] = 0;
                m_offset_y[i] = 0;
            }
            break;
        case SMALL_FONT_7x14:
            m_line_height = 15;
            for (unsigned int i = 0; i < 256; ++i)
            {
                m_width[i] = 7;
                m_height[i] = 14;
                m_advance[i] = 7;
                m_offset_x[i] = 0;
                m_offset_y[i] = 0;
            }
            break;
        default:
            throw Exception("The specified font has not been implemented yet.");
        }
        // 2) Allocate memory for the character structures.
        for (unsigned int i = 0; i < 256; ++i)
        {
            m_characters[i] = new unsigned char[m_width[i] * m_height[i]];
            for (unsigned int j = 0; j < m_width[i] * m_height[i]; ++j)
                m_characters[i][j] = 0;
        }
        character_size = (int)ceil((double)(m_width[0] * m_height[0]) / 8.0);
        unsigned int bitcode_size = character_size * 256;
        if (bitcode_size % 3 != 0) bitcode_size += 3 - bitcode_size % 3;
        bitcode = new unsigned char[bitcode_size];
        // 3) Initialize the byte codes for each character.
        switch (font_type)
        {
            case TINY_FONT_5x7:
            {
                // Unpack the character bit code from base 64 to binary.
                //const unsigned char font_code64[] = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000248GX24COnZ6COvpdESvptlUzxtl______UzxtlUyvpdESvnZ6COnZ248GX24XYbRpcCO0T207nW3qESW0Fk80008OvY0008pZ24002E.0000000008GX000HIb00005A.hwbA8.ZY.CJ6H4HCOsIeYibDOH00000H48GGWa448H680Ihga8012V8GW0001X68007m0000000nZ014H400T6TSnIuZ248HZdH28YCV.8WWnIuHbI.8Glmy4CKkCY7enIxuH4GX27HYwCKkT6BmYKm360OmW0CO1X684H444880FW.004444H68T48Y00HqGjhLhdHZ7yOnz6FenpvqOGX5BkIZ6CjS_27eGn_y8UX247HXUCKlZ6FunZ5n248HZZY48KbCZAcAIb648GX37uxhMCOnZ7DPnZ5qOnZ5BlHZw48GT6CQoKtqO.fAKNmWu8S..GX248ICOnZ5BenZ6AX4Z6DQrQgCL4L6COnYeX24.4H4Gnzo48GX3WGGGGGWS8GX2KuYeW00000000CVGGG0000071V5BuGXRCS.01q8HIu8Gjd5Bm0T7y4ECb748GXyOl25BeGjcCOn0G1248GG3259J8GbJ5AIOGX24Ou0DLh6CG0jcCOn01qOnIu0FHz2400RDuGX02sOGX007GS7Fa8uX2Gc02COpMq08nYeH00Z6jMg02AYAb408nU5BW0.8YCV4GY2488X248GX4488X680GNn480028.W1508gFxn80Ye_l4YYAZ.yH4S5yKl81mNnI_.CVnZ7ukZ6CKk0e3enIuBfrowq02TMhd8Y2COnIu2WHZDROg.Jv242BvFeX02Ar4007mT5mS.VIdwKfG0D5_HYp9Hv2CVvBahobC3l_.um0EZ6BZ00Htt48015HKGW2480X24V247aOW0HbJ7Fn4GGGCVGGH40ny8If.W12EgGX248GXAkCG0000000003bESEGX00000048Jd000220400360007m_28H00.4n6800H6K8G02VY49W01yXEV00NpAb804VIfI000uGjV03ml2ru00Lg5BW01y000.4fY4OW8YCeGX2VZ48X41yX24vyNn6L8GaVIbAOo9ydo48G3qn29J4Fa8GX41y8GXpzNrA4HY0O3a8bC1y8YAb57qgGW1mHYa8bC0zDHYKmN2V8HY0LhK8X4S3v24OX24CKX224.GY8G0u000ny7mg8gK2V4HrIa48GX4OW11HZ6COG_244F1y8GYKm2A22482V8IjIa1y8L4883WE0umG4H2FpX04AYAb07qVGW1q8.bI480uGX2r_uVX27Ft0.48X4Z6CGY4GbAKhMjWGX6LEO1.COnpy7un28H0O048jS9900003bE000000JMKbDK1mNnIy3e.Zwq00T1aKk16C.mX007qb5BW6J7q8G01yNXIu03a9HY1Q4000040mXIKoYA000004TIhZ4GZYE8GzmBPZ6CL0T6CKk1RCVGX03PnU48GEZ.CKk002ww4007HYhMr0Z6Crj_114Gny0FgKgK_WYeYfH02CNXIu0V4.GX00.XwOn03zVnZ410V0GW0000000______y0";
                const unsigned char font_code64[] = "0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000248GX20COnZ6C0vpdESu3tlUzxmF_____WUz"
                                                    "xtlU0vpdESu1Z6COnW248GX20XYbRp0CO0T23WnW3qEG00Fk80008OvY0008pZ20002E.0000000008GX0081Ib00005A.hwb08.ZY.836H4HCO6IeYicWOH00000H48GGG4448H400Ihg"
                                                    "a0012V8G00001X40007m0000000nW014H400T6TSnS0Z248Hm7H28YFW.8WWnS0HbI.8GFmy4CN0CY7enS3uH4GX07HYwCN0T6BmYO0360Om00CO1X404H444400FW.004444H40T48Y08"
                                                    "1qGjhLm7HZ7yOWz6Feny1qOGX5mEIZ6Ck0_27eG.3y8UX207HXUCNWZ6FunY1n248Hm3Y48Kc0ZAcAIY248GX3u8xhMCOWZ7DPnY1qOnZ5mFHZw480T6CQoQ3qO.fA87mWu8V0.GX2482C"
                                                    "OnZ5m8nZ6AY0Z6DQrK2CL4L688nYeX20.4H4G.1o48GXm0GGGGG0S8GX2S0YeW00000000FWGGG0000071V5u8GXRCV001q8HS08Gjd5u00T7y70Cb748G1yOl25m8GjcCOW0G12480G32"
                                                    "59W8GbJ590OGX24S00DLh6800jcCOW01qOnS00FHz2000RDuGW02sOGW007GS7m48uX2J002COpQ008nYeW00Z6jL002AYAY008nU5m00.8YFW4GY2440X248GW4488X400GNn400028.W"
                                                    "W508gFuW80Ye_Y0YYAZ.814S5yNW81mNnU3.CVnZu8kZ6CN00e3enS0Bfrow002TMha0Y2COnS02WHZDe8g.Jv202BvFeW02Ar4007mT5mV0VIdwKe00D5_HO39Hv2FWvBahoc03l_.u00"
                                                    "EZ6BW00Htt40015HKG02480X20V247aG00HbJ7u14GGGFWGGH40.08If.WW2EgGX208GXAk800000000003bE0EGX00000048JW000222000360007m_28W00.4n4000H6K8002VY4m001"
                                                    "yXFW00NpAa004VIf0000uGlW03ml2y000Lg5m001y000.4fY4G08YCeGW2VZ48Y01yX24.0Nn6L8G4VIbAP09ydo4803qn29W4Fa8GY01y8GX.1NrA4H00O3a8c01y8YAY17qgGWu0HYa8"
                                                    "c00zDHYO0N2V8H00LhK8Y0S3v24G124CKX024.GY800u000.07mg8g02V4HrI048GX4G011HZ688G_247W1y8GYO02A22402V8IjI01y8L4403WE0u804H2FmW04AYAW07qVGWu48.bI40"
                                                    "0uGX2.3uVX27u70.48Y0Z6CGY80bAKhMm0GX6LC01.COn.07un28W0O048k09900003bE000000JMKcWK1mNnU03e.Zw000T1aN016C.mW007qb5m06J7q8001yNXS003a9H01Q4000040"
                                                    "mXIO2YA000004TIhY0GZYE8U1mBPZ6850T6CN01RCVGW03PnU480EZ.CN0002ww0007HYhO50Z6CsW_114G.00FgKgOFWYeYeW02CNXS00V4.GW00.XwOW03zVnY010V0G00000000____"
                                                    "_.00";
                for (int j = 0, k = 0; j < character_size * 256; j += 3, k += 4) srv::fromBase64((char*)&font_code64[k], (char*)&bitcode[j]);
#ifdef __ENABLE_FREE_TYPE__
                m_font_name = "__buildin__ SMALL_FONT_5x7";
#endif
                break;
            }
            case SMALL_FONT_7x14:
            {
                const unsigned char font_code64[] = "00000000000000000000uYgmSTr4S00000000EFjV.xZV7000000006t___V73WWG0000000GSVV_xuu800000008E77t_.s4FW00000023Zx___jX3u0000000008EFZWW000000001__"
                                                    "zyS77t__m000000001n4YH70000000007__ZktRkF__000000071XN4I94S0000000074I94S4FX0W0000001WmKB4YNJ0000000007EaI94Zcp0000000025Hn5ZH75GW000000000WOE"
                                                    "7ZXWW0000000000WmuyE30W00000023YeG84AZWW000004YH8aI90018a000001zKgL7XGeKA52W0000E8a1X8I60aHm00000000000V_____000000GSL212eu80FW00000023YeG8421"
                                                    "0WG000000WG84210XKS400000000010Jy44000000000000WW_820000000000000C631W.00000000002YB.YA000000000000GSVVm000000000000_lZWW000000000000000000000"
                                                    "00000210WG842000G000002H8aI0000000000000I94dv99.I94W0000023YfGeE2XIeu800003ALCQ211OofJ000000OI9311J6YH7O0000010WWW0000000000000GG884210W841000"
                                                    "00G420WG84221100000012euSL20000000000000427mWG0000000000000000031WGm000000000V00000000000000000000C60000002110WWGG88400000001X8aI94YH8O0000000"
                                                    "GOK210WG8V0000000E8WG888887m0000003Y84260WI8u00000004654aI9.210000000V843m42H8ZW0000003Y90WU8aI8u0000001yY110WGG840000000E8aI8uYH8ZW0000003Y94"
                                                    "YF0WI8u000000000630000C60000000001Wm00031WGm0000WWWWWW820W82000000000V003u000000010G410G4444440000074I84442000G000000WeYNAbIv0I60000000422XGeY"
                                                    "V8aG00000072H8aU8aI9u0000000uYH84214YE0000000S94I94YH9700000007o10WU8421y0000001yWG87Y10WG0000000E8a21CYH9ZG0000004I94YV8aI940000001y84210WG8V"
                                                    "0000000F10WG84I9300000004IHGmK94Y9400000010WG84210WV0000000XGisQrQXGeG0000004J9agL9ao940000000uYH8aI94YE0000000U8aI9uWG8400000003Y94YH8aI8u830"
                                                    "0001uYH8dYH4YH0000000E8a10O2H8ZW0000007mWG84210WG00000014YH8aI94YE0000000H8aHGeK42100000008C639LAbHGe00000014YA511GeYH0000000H8aHGG84210000000"
                                                    "7m84444421y000001uWG84210WG87W0000WG420WG420WG00000F0WG84210WG8y000000GSR8W00000000000000000000007u0000420W80000000000000000E8WHv4YF0000000G85"
                                                    "Z94YHCbW000000000uYG8428u000000042D9aI94cD0000000003Y94.G8ZW0000000mWG.4210WG000000000D9aI94cD0aHm000G85Z94YH8aG0000000W0u4210WG8000000080E10W"
                                                    "G84214XW000G84YXWmK94G0000003WG84210WG8000000000QAbIfKgL0000000005Z94YH8aG000000000uYH8aI8u000000000MCaI94oM8420000003IP4YH9ZG842000001OoG8421"
                                                    "0000000000E8a1m4YE0000000427mWG8420m0000000014YH8aIOq000000000H8aHGe840000000004I9KgLAYW0000000014YA22Y94000000000H8YH8KC21510000007m888887m00"
                                                    "003210WG8O210WG830010WG84210WG8421001W84210WC84210XW00002If80000000000000000476o94.00000000E8aI10YH8ZWWCS008aG14YH8aIOq00000WWW0E8aJv0YE00000G"
                                                    "KH03Y84UH8Zm000008aG0uY17aI8y000020W80E8WHv4YF000000SH8ZY84UH8Zm000000000uYG8428u837011H40E8aJv0YE000000YH03Y94.G8ZW0000820W0uYHFa28u000002940"
                                                    "E10WG84200000GKH03WG84210W0000820W0u4210WG800004I808452Y9yYH00000uYE211GeYV8aG00002203v0WU8421y000000000QIXJwHAU0000000F652XT8yI9m0000454G0uYH"
                                                    "8aI8u000002940E8aI94YE00000W8203Y94YH8ZW0000454G14YH8aIOq000020W80H8aI94cD000000YH04I8aI530WHGG0H8W1n4YH8aI8u00004I80YH8aI94YE000000007KIPKgP8"
                                                    "hW000003210Xy84233y0000000wYJ9bJ9aYk000000000028e8A8W00000000mWGU4210WGm0000WWW0E8WHv4YF0000088803WG84210W000022200uYH8aI8u00000WWW0H8aI94cD00"
                                                    "0000oc05Z94YH8aG0000PJ029aoLAaoP400001W8SI703m000000000OI94XW0y0000000004000WGGG8aHm00000001z1krRjMh1V0000000V0WG00000000000GOaIJe8A6aIIS00004"
                                                    "694aw22HHKkX00000080010WG84210000004YYHH8K92X80000004XGaA4YYHH8000000Z48n2CGZ48n2CGZ40ggggggggggggggggWBlUxtkzxlUxtkzxlS0G84210WG84210WG804210"
                                                    "WG84U10WG842011008452Y9yYH00000GKH211GeYV8aG00004100WGKA8do940000001z1kqQD6x1V00000eKA52XJe4w52XGeK0A52XGeKA52XGeKA500000000FWJeKA52XG0eKA52XJ"
                                                    "e4.00000000211nKeKA5HmG80000014YH510Xy840000000000001u4210WG804210WG843m000000010WG84217y0000000000000001_4210WG804210WG843n0WG8420000000007y0"
                                                    "0000000G84210WH_4210WG800CfW0uY17aI8y00006Km08452Y9yYH00000eKA52XGkGFW0000000000000Fa2vGeKA502XGeKA5Eu3.00000000000003.0xb2XGeK0A52XGeKBa2vGeK"
                                                    "A500000000Fu3.0000000eKA52XJk0xb2XGeK00GNo94YHFe800000002GmOI0Zo94YE0000000S94IBaYH97000004503v0WU8421y00004I80.G87Y10WV00000W80Fa21uWG87m0000"
                                                    "003Y91yGU4I8u00000WW0.4210WG8V00000GK0FX0WG8427m0000H8W3uG84210Xy000010WG84217W0000000000000000F4210WG80________________m000000007_______y0G84"
                                                    "210W084210WG008203uG84210Xy0000F_______u00000000088874I94YH8ZW0000001X94aH8KAO0000011H4SH8aI94YE00000W8274I94YH8ZW00000CfW0uYH8aI8u00006Km0SH8"
                                                    "aI94YE000000008aI94YH9dA0000000610iP8aJ9OWGS0F21uYHF4210Wy000004448aI94YH8ZW0000454G14YH8aI8u0000410GYH8aI94YE0000044404I8aI530WHGG0111294YA21"
                                                    "0WG0000001y00000000000000444400000000000000000000V000000000000084FX0W0.000000000000000V07m0000m4iHJ88955Iw400003xfqwT7XGeKA000000uYG74I94S18ZW"
                                                    "00000000WG0V010W00000000000000000430Hm0OI930000000000000004I80000000000000000CF7XW0000000008C211m0000000000060XW8O000000000001W844700000000000"
                                                    "000E73XmuSE73Xm0000000000000000000080U";
                for (int j = 0, k = 0; j < character_size * 256; j += 3, k += 4) srv::fromBase64((char*)&font_code64[k], (char*)&bitcode[j]);
#ifdef __ENABLE_FREE_TYPE__
                m_font_name = "__buildin__ SMALL_FONT_7x14";
#endif
                break;
            }
            default:
                throw Exception("The specified font has not been implemented yet.");
        }
        // 4) Get the character bits from the bit-code structure.
        for (unsigned int character = 0; character < 256; ++character)
        {
            for (int k = 0, bit_index = 0, location = character * character_size; k < character_size; ++k, ++location)
            {
                for (int b = 0; b < 8; ++b)
                {
                    if ((bitcode[location] & 0x80) == 0x80) m_characters[character][bit_index] = 255;
                    bitcode[location] = (unsigned char)(bitcode[location] << 1);
                    ++bit_index;
                    if (bit_index >= (int)(m_width[character] * m_height[character])) break;
                }
            }
        }
        
        delete [] bitcode;
    }
    
#ifdef __ENABLE_FREE_TYPE__
    Draw::Font::Font(const char * filename, unsigned int font_size, bool break_line) :
        m_line_height(font_size + 1),
        m_break_line(break_line)
    {
        int error, maximum_offset;
        FT_Library library;
        FT_Face face;
        
        error = FT_Init_FreeType(&library);
        freeTypeErrorHandle(error, filename, library);
        error = FT_New_Face(library, filename, 0, &face);
        freeTypeErrorHandle(error, filename, library);
        error = FT_Set_Pixel_Sizes(face, 0, font_size); // 0-> Pixel width; 16 -> Pixel height.
        if (error)
        {
            FT_Done_FreeType(library);
            throw srv::Exception("Error '%d' has occurred while changing the size of the font.", error);
        }
        
        // HERE ===================================================================================
        // Try to get the font name from the TTF file.
        // ----------------------------------------------------------------------------------------
        unsigned int number_of_name_strings;
        
        number_of_name_strings = FT_Get_Sfnt_Name_Count(face);
        if (number_of_name_strings > 0)
        {
            std::string family_name, subfamily_name;
            
            // TODO: More cases should be taken into account to store the font name in the correct format.
            for (unsigned int i = 0; i < number_of_name_strings; ++i)
            {
                unsigned int index;
                char * buffer;
                FT_SfntName name;
                bool unicode;
                
                if (FT_Get_Sfnt_Name(face, i, &name)) continue;
                
                // Get only the family name and the family sub-name.
                if ((name.name_id != TT_NAME_ID_FONT_FAMILY) && (name.name_id != TT_NAME_ID_FONT_SUBFAMILY))
                    continue;
                if ((name.platform_id == TT_PLATFORM_APPLE_UNICODE) && (name.language_id != TT_MAC_LANGID_ENGLISH))
                    continue;
                if ((name.platform_id == TT_PLATFORM_MACINTOSH) && (name.language_id != TT_MAC_LANGID_ENGLISH))
                    continue;
                if ((name.platform_id == TT_PLATFORM_MICROSOFT) &&
                        ((name.language_id != TT_MS_LANGID_ENGLISH_UNITED_STATES) && (name.language_id != TT_MS_LANGID_ENGLISH_UNITED_KINGDOM)))
                    continue;
                unicode = false;
                if (name.platform_id == TT_PLATFORM_APPLE_UNICODE) unicode = true;
                if ((name.platform_id == TT_PLATFORM_MICROSOFT) && (name.encoding_id == TT_MS_ID_UNICODE_CS)) unicode = true;
                if (unicode)
                {
                    buffer = new char[name.string_len / 2 + 1];
                    for (index = 0; index < name.string_len / 2; ++index)
                        buffer[index] = (char)name.string[2 * index + 1];
                    buffer[index] = '\0';
                }
                else
                {
                    buffer = new char[name.string_len + 1];
                    for (index = 0; index < name.string_len; ++index)
                        buffer[index] = (char)name.string[index];
                    buffer[index] = '\0';
                }
                if (strcmp(buffer, "New") != 0)
                {
                    if (name.name_id != TT_NAME_ID_FONT_SUBFAMILY)
                        family_name = buffer;
                    if (name.name_id != TT_NAME_ID_FONT_FAMILY)
                        subfamily_name = buffer;
                }
                delete [] buffer;
            }
            m_font_name = family_name + " (" + subfamily_name + ")";
        }
        
        // HERE ===================================================================================
        
        FT_GlyphSlot slot = face->glyph;
        maximum_offset = 0;
        for (unsigned int character = 0; character < 256; ++character)
        {
            const FT_Bitmap * bitmap;
            FT_UInt glyph_index;
            
            m_width[character] = 0;
            m_height[character] = 0;
            m_characters[character] = 0;
            
            glyph_index = FT_Get_Char_Index(face, (unsigned char)character);
            FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
            if (error) continue;
            error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
            if (error) continue;
            
            bitmap = &slot->bitmap;
            m_width[character] = bitmap->width;
            m_height[character] = bitmap->rows;
            m_advance[character] = (unsigned int)(slot->advance.x >> 6);
            m_offset_x[character] = slot->bitmap_left;
            m_offset_y[character] = -slot->bitmap_top;
            if (slot->bitmap_top > maximum_offset) maximum_offset = slot->bitmap_top;
            
            if (bitmap->buffer != 0)
            {
                const unsigned char * bitmap_ptr = bitmap->buffer;
                unsigned char * char_ptr;
                
                char_ptr = m_characters[character] = new unsigned char[bitmap->width * bitmap->rows];
                for (unsigned int y = 0; y < (unsigned int)bitmap->rows; ++y)
                    for (unsigned int x = 0; x < (unsigned int)bitmap->width; ++x, ++char_ptr, ++bitmap_ptr)
                        *char_ptr = *bitmap_ptr;
            }
        }
        for (unsigned int i = 0; i < 256; ++i) m_offset_y[i] += maximum_offset;
        FT_Done_FreeType(library);
    }
#endif
    
    Draw::Font::Font(const Draw::Font &other) :
        m_line_height(other.m_line_height),
#ifdef __ENABLE_FREE_TYPE__
        m_break_line(other.m_break_line),
        m_font_name(other.m_font_name)
#else
        m_break_line(other.m_break_line)
#endif
    {
        for (unsigned int i = 0; i < 256; ++i)
        {
            m_width[i] = other.m_width[i];
            m_height[i] = other.m_height[i];
            m_advance[i] = other.m_advance[i];
            m_offset_x[i] = other.m_offset_x[i];
            m_offset_y[i] = other.m_offset_y[i];
            if (other.m_characters[i] != 0)
            {
                m_characters[i] = new unsigned char[other.m_width[i] * other.m_height[i]];
                for (unsigned int j = 0; j < other.m_width[i] * other.m_height[i]; ++j)
                    m_characters[i][j] = other.m_characters[i][j];
            }
            else m_characters[i] = 0;
        }
    }
    
    /// Destructor.
    Draw::Font::~Font(void)
    {
        for (unsigned int i = 0; i < 256; ++i)
            if (m_characters[i] != 0)
                delete [] m_characters[i];
    }
    
    /// Assignation operator.
    Draw::Font& Draw::Font::operator=(const Draw::Font &other)
    {
        if (this != &other)
        {
            m_line_height = other.m_line_height;
            m_break_line = other.m_break_line;
#ifdef __ENABLE_FREE_TYPE__
            m_font_name = other.m_font_name;
#endif
            for (unsigned int i = 0; i < 256; ++i)
            {
                if (m_characters[i] != 0) delete [] m_characters[i];
                
                m_width[i] = other.m_width[i];
                m_height[i] = other.m_height[i];
                m_advance[i] = other.m_advance[i];
                m_offset_x[i] = other.m_offset_x[i];
                m_offset_y[i] = other.m_offset_y[i];
                if (other.m_characters[i] != 0)
                {
                    m_characters[i] = new unsigned char[other.m_width[i] * other.m_height[i]];
                    for (unsigned int j = 0; j < other.m_width[i] * other.m_height[i]; ++j)
                        m_characters[i][j] = other.m_characters[i][j];
                }
                else m_characters[i] = 0;
            }
        }
        return *this;
    }
    
    void Draw::Font::calculateBox(const char * text, int &x0, int &y0, int &x1, int &y1, int &y_offset) const
    {
        unsigned int row, column;
        int x_offset;
        
        y0 = x1 = y1 = 0;
        x0 = (int)m_line_height * 2;
        y_offset = (int)m_line_height * 2;
        row = column = 0;
        x_offset = 0;
        for (unsigned int index = 0; text[index] != '\0'; ++index)
        {
            if (m_break_line && (text[index] == '\n'))
            {
                ++row;
                column = 0;
                x_offset = 0;
            }
            else
            {
                if (column == 0) x0 = srvMin<int>(x0, m_offset_x[(int)text[index]]);
                if (row == 0)
                {
                    y0 = srvMin<int>(y0, m_offset_y[(int)text[index]]);
                    y_offset = srvMin<int>(y_offset, m_offset_y[(int)text[index]]);
                }
                y1 = srvMax<int>(y1, row * m_line_height + (int)m_height[(int)text[index]] + m_offset_y[(int)text[index]]);
                x1 = srvMax<int>(x1, x_offset + (int)m_width[(int)text[index]] + m_offset_x[(int)text[index]]);
                x_offset += (int)m_advance[(int)text[index]];
                ++column;
            }
        }
    }
    
    unsigned int Draw::Font::calculateBox(const char * text, int * x0, int * y0, int * x1, int * y1) const
    {
        unsigned int character;
        int x, y;
        
        x = y = 0;
        for (character = 0; text[character] != '\0'; ++character)
        {
            if (m_break_line && (text[character] == '\n'))
            {
                x = 0;
                y += m_line_height;
            }
            else
            {
                const unsigned char character_value = (unsigned char)text[character];
                int cx, cy, cw, ch;
                
                cx = x + m_offset_x[character_value];
                cy = y + m_offset_y[character_value];
                cw = (int)m_width[character_value];
                ch = (int)m_height[character_value];
                x0[character] = cx;
                x1[character] = cx + cw;
                y0[character] = cy;
                y1[character] = cy + ch;
                x += m_advance[character_value];
            }
        }
        return character;
    }
    
    void Draw::Font::calculateHeight(int &avg_height, int &max_height) const
    {
        unsigned int idx;
        unsigned char characters[] = "0123456789abcdefghijklmnopqrstuvwxyzçñ_.,ABCDEFGHIJKLMNOPQRSTUVWXYZÇÑ\0";
        
        idx = 0;
        avg_height = max_height = 0;
        for (unsigned int i = 0; characters[i] != '\0'; ++i, ++idx)
        {
            const int h = m_offset_y[characters[i]] + (int)m_height[characters[i]];
            if (h > max_height) max_height = h;
            avg_height += h;
        }
        avg_height /= idx;
    }
    
#ifdef __ENABLE_FREE_TYPE__
    void Draw::Font::freeTypeErrorHandle(int error, const char * filename, FT_Library &library) const
    {
        if (error > 0)
        {
            char message[512];
            
            FT_Done_FreeType(library);
            if (filename != 0) sprintf(message, "The TTF font in '%s' generated the error: ", filename);
            else sprintf(message, "The TTF font generated the error: ");
            
            switch (error)
            {
            case FT_Err_Cannot_Open_Resource:                                                       // Generic errors.
                throw Exception("%sCannot open resource.", message);
            case FT_Err_Unknown_File_Format:
                throw Exception("%sUnknown file format.", message);
            case FT_Err_Invalid_File_Format:
                throw Exception("%sInvalid file format/broken file.", message);
            case FT_Err_Invalid_Version:
                throw Exception("%sInvalid FreeType version.", message);
            case FT_Err_Lower_Module_Version:
                throw Exception("%sModule version is too low.", message);
            case FT_Err_Invalid_Argument:
                throw Exception("%sInvalid argument.", message);
            case FT_Err_Unimplemented_Feature:
                throw Exception("%sUnimplemented feature.", message);
            case FT_Err_Invalid_Table:
                throw Exception("%sBroken table.", message);
            case FT_Err_Invalid_Offset:
                throw Exception("%sBroken offset within table.", message);
            case FT_Err_Array_Too_Large:
                throw Exception("%sArray allocation size too large.", message);
#if (FREETYPE_MAJOR == 2 && FREETYPE_MINOR >= 5) || (FREETYPE_MAJOR > 2)
            case FT_Err_Missing_Module:
                throw Exception("%sMissing module.", message);
            case FT_Err_Missing_Property:
                throw Exception("%sMissing property.", message);
#endif
            case FT_Err_Invalid_Glyph_Index:                                                        // Glyph/character errors.
                throw Exception("%sInvalid glyph index.", message);
            case FT_Err_Invalid_Character_Code:
                throw Exception("%sInvalid character code.", message);
            case FT_Err_Invalid_Glyph_Format:
                throw Exception("%sUnsupported glyph image format.", message);
            case FT_Err_Cannot_Render_Glyph:
                throw Exception("%sCannot render this glyph format.", message);
            case FT_Err_Invalid_Outline:
                throw Exception("%sInvalid outline.", message);
            case FT_Err_Invalid_Composite:
                throw Exception("%sInvalid composite glyph.", message);
            case FT_Err_Too_Many_Hints:
                throw Exception("%sToo many hints.", message);
            case FT_Err_Invalid_Pixel_Size:
                throw Exception("%sInvalid pixel size.", message);
            case FT_Err_Invalid_Handle:                                                             // Handle errors.
                throw Exception("%sInvalid object handle.", message);
            case FT_Err_Invalid_Library_Handle:
                throw Exception("%sInvalid library handle.", message);
            case FT_Err_Invalid_Driver_Handle:
                throw Exception("%sInvalid module handle.", message);
            case FT_Err_Invalid_Face_Handle:
                throw Exception("%sInvalid face handle.", message);
            case FT_Err_Invalid_Size_Handle:
                throw Exception("%sInvalid size handle.", message);
            case FT_Err_Invalid_Slot_Handle:
                throw Exception("%sInvalid glyph slot handle.", message);
            case FT_Err_Invalid_CharMap_Handle:
                throw Exception("%sInvalid charmap handle.", message);
            case FT_Err_Invalid_Cache_Handle:
                throw Exception("%sInvalid cache manager handle.", message);
            case FT_Err_Invalid_Stream_Handle:
                throw Exception("%sInvalid stream handle.", message);
            case FT_Err_Too_Many_Drivers:                                                           // Driver errors.
                throw Exception("%sToo many modules.", message);
            case FT_Err_Too_Many_Extensions:
                throw Exception("%sToo many extensions.", message);
            case FT_Err_Out_Of_Memory:                                                              // Memory errors.
                throw Exception("%sOut of memory.", message);
            case FT_Err_Unlisted_Object:
                throw Exception("%sUnlisted object.", message);
            case FT_Err_Cannot_Open_Stream:                                                         // Stream errors.
                throw Exception("%sCannot open stream.", message);
            case FT_Err_Invalid_Stream_Seek:
                throw Exception("%sInvalid stream seek.", message);
            case FT_Err_Invalid_Stream_Skip:
                throw Exception("%sInvalid stream skip.", message);
            case FT_Err_Invalid_Stream_Read:
                throw Exception("%sInvalid stream read.", message);
            case FT_Err_Invalid_Stream_Operation:
                throw Exception("%sInvalid stream operation.", message);
            case FT_Err_Invalid_Frame_Operation:
                throw Exception("%sInvalid frame operation.", message);
            case FT_Err_Nested_Frame_Access:
                throw Exception("%sNested frame access.", message);
            case FT_Err_Invalid_Frame_Read:
                throw Exception("%sInvalid frame read.", message);
            case FT_Err_Raster_Uninitialized:                                                       // Raster errors.
                throw Exception("%sRaster uninitialized.", message);
            case FT_Err_Raster_Corrupted:
                throw Exception("%sRaster corrupted.", message);
            case FT_Err_Raster_Overflow:
                throw Exception("%sRaster overflow.", message);
            case FT_Err_Raster_Negative_Height:
                throw Exception("%sNegative height while rastering.", message);
            case FT_Err_Too_Many_Caches:                                                            // Cache errors.
                throw Exception("%sToo many registered caches.", message);
            case FT_Err_Invalid_Opcode:                                                             // TrueType and SFNT errors.
                throw Exception("%sInvalid opcode.", message);
            case FT_Err_Too_Few_Arguments:
                throw Exception("%sToo few arguments.", message);
            case FT_Err_Stack_Overflow:
                throw Exception("%sStack overflow.", message);
            case FT_Err_Code_Overflow:
                throw Exception("%sCode overflow.", message);
            case FT_Err_Bad_Argument:
                throw Exception("%sBad argument.", message);
            case FT_Err_Divide_By_Zero:
                throw Exception("%sDivision by zero.", message);
            case FT_Err_Invalid_Reference:
                throw Exception("%sInvalid reference.", message);
            case FT_Err_Debug_OpCode:
                throw Exception("%sFound debug opcode.", message);
            case FT_Err_ENDF_In_Exec_Stream:
                throw Exception("%sFound ENDF opcode in execution stream.", message);
            case FT_Err_Nested_DEFS:
                throw Exception("%sNested DEFS.", message);
            case FT_Err_Invalid_CodeRange:
                throw Exception("%sInvalid code range.", message);
            case FT_Err_Execution_Too_Long:
                throw Exception("%sExecution context too long.", message);
            case FT_Err_Too_Many_Function_Defs:
                throw Exception("%sToo many function definitions.", message);
            case FT_Err_Too_Many_Instruction_Defs:
                throw Exception("%sToo many instruction definitions.", message);
            case FT_Err_Table_Missing:
                throw Exception("%sSFNT font table missing.", message);
            case FT_Err_Horiz_Header_Missing:
                throw Exception("%sHorizontal header (hhea) table missing.", message);
            case FT_Err_Locations_Missing:
                throw Exception("%sLocations (loca) table missing.", message);
            case FT_Err_Name_Table_Missing:
                throw Exception("%sName table missing.", message);
            case FT_Err_CMap_Table_Missing:
                throw Exception("%sCharacter map (cmap) table missing.", message);
            case FT_Err_Hmtx_Table_Missing:
                throw Exception("%sHorizontal metrics (hmtx) table missing.", message);
            case FT_Err_Post_Table_Missing:
                throw Exception("%sPostScript (post) table missing.", message);
            case FT_Err_Invalid_Horiz_Metrics:
                throw Exception("%sInvalid horizontal metrics.", message);
            case FT_Err_Invalid_CharMap_Format:
                throw Exception("%sInvalid character map (cmap) format.", message);
            case FT_Err_Invalid_PPem:
                throw Exception("%sInvalid ppem value.", message);
            case FT_Err_Invalid_Vert_Metrics:
                throw Exception("%sInvalid vertical metrics.", message);
            case FT_Err_Could_Not_Find_Context:
                throw Exception("%sCould not find context.", message);
            case FT_Err_Invalid_Post_Table_Format:
                throw Exception("%sInvalid PostScript (post) table format.", message);
            case FT_Err_Invalid_Post_Table:
                throw Exception("%sInvalid PostScript (post) table.", message);
            case FT_Err_Syntax_Error:                                                               // CFF, CID, and Type 1 errors.
                throw Exception("%sOpcode syntax error.", message);
            case FT_Err_Stack_Underflow:
                throw Exception("%sArgument stack underflow.", message);
            case FT_Err_Ignore:
                throw Exception("%sIgnore.", message);
            case FT_Err_No_Unicode_Glyph_Name:
                throw Exception("%sNo Unicode glyph name found.", message);
#if (FREETYPE_MAJOR == 2 && FREETYPE_MINOR >= 5) || (FREETYPE_MAJOR > 2)
            case FT_Err_Glyph_Too_Big:
                throw Exception("%sGlyph to big for hinting.", message);
#endif
            case FT_Err_Missing_Startfont_Field:                                                    // BDF errors.
                throw Exception("%s`STARTFONT' field missing.", message);
            case FT_Err_Missing_Font_Field:
                throw Exception("%s`FONT' field missing.", message);
            case FT_Err_Missing_Size_Field:
                throw Exception("%s`SIZE' field missing.", message);
            case FT_Err_Missing_Fontboundingbox_Field:
                throw Exception("%s`FONTBOUNDINGBOX' field missing.", message);
            case FT_Err_Missing_Chars_Field:
                throw Exception("%s`CHARS' field missing.", message);
            case FT_Err_Missing_Startchar_Field:
                throw Exception("%s`STARTCHAR' field missing.", message);
            case FT_Err_Missing_Encoding_Field:
                throw Exception("%s`ENCODING' field missing.", message);
            case FT_Err_Missing_Bbx_Field:
                throw Exception("%s`BBX' field missing.", message);
            case FT_Err_Bbx_Too_Big:
                throw Exception("%s`BBX' too big.", message);
            case FT_Err_Corrupted_Font_Header:
                throw Exception("%sFont header corrupted or missing fields.", message);
            case FT_Err_Corrupted_Font_Glyphs:
                throw Exception("%sFont glyphs corrupted or missing fields.", message);
            default:
                throw Exception("Unexpected error code '%d'.", error);
            }
        }
    }
#endif
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
}

