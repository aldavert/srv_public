// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_INTEGRAL_HPP_HEADER_FILE__
#define __SRV_IMAGE_INTEGRAL_HPP_HEADER_FILE__

#include "srv_image.hpp"
// -[ C++ header files ]-----------------------------------------------
#include <cmath>
#include <omp.h>

namespace srv
{
    //                   +--------------------------------------+
    //                   | INTEGRAL IMAGE CLASS DECLARATION     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which calculates the integral image for a standard image and calculates the Haar-like features.
    template <class T>
    class IntegralImage
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        IntegralImage(void);
        /** Constructor where integral image is build form an image of a different data type.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <template <class> class IMAGE, class U> IntegralImage(const IMAGE<U> &image, unsigned int number_of_threads = 1);
        /// Copy constructor.
        IntegralImage(const IntegralImage<T> &copy);
        /// Copy constructor for an integral image of a different type.
        template <class U> IntegralImage(const IntegralImage<U> &copy);
        /// Destructor
        ~IntegralImage(void);
        /// Assignation operator.
        IntegralImage<T>& operator=(const IntegralImage<T> &copy);
        /// Assignation operator of an integral image of a different type.
        template <class U> IntegralImage<T>& operator=(const IntegralImage<U> &copy);
        
        /** Function which initializes the integral image from a image of a different data type.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <template <class> class IMAGE, class U> void set(const IMAGE<U> &image, unsigned int number_of_threads = 1);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the width of the integral image (i.e.\ width of the original image plus an extra column to add a left border of zeros to the integral image).
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the integral image (i.e.\ height of the original image plus an extra row to add a top border of zeros to the integral image).
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of channels of the integral image.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        /// Returns a constant pointer to the integral image of the specified channel.
        inline const T* getData(unsigned int channel) const { return m_image[channel]; }
        /// Returns a boolean which indicates if the integral image is empty (i.e.\ it is not initialized).
        inline bool isEmpty(void) const { return m_image == 0; }
        /// Removes the content of the integral image.
        inline void clear(void)
        {
            if (m_image != 0)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
                delete [] m_image;
            }
            m_width = m_height = m_number_of_channels = 0;
            m_image = 0;
        }
        
        // -[ Haar-like features ]-------------------------------------------------------------------------------------------------------------------
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUM OF THE VALUES IN THE RECTANGLE ]=======================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the sum of the values of a rectangle for the specified channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            return *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
        }
        /** Returns the sum of the values of a rectangle for each channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                values[channel] = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX ]==========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w / 2 + h * m_width] - ptr[w / 2] - ptr[h * m_width];
            P0 = ptr[w / 2] + ptr[w + h * m_width] - ptr[w] - ptr[w / 2 + h * m_width];
            return P0 - N0;
        }
        /** Returns the value of calculating the dX Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w / 2 + h * m_width] - ptr[w / 2] - ptr[h * m_width];
                P0 = ptr[w / 2] + ptr[w + h * m_width] - ptr[w] - ptr[w / 2 + h * m_width];
                values[channel] = P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dY ]==========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dY Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w + (h / 2) * m_width] - ptr[w] - ptr[(h / 2) * m_width];
            P0 = ptr[(h / 2) * m_width] + ptr[w + h * m_width] - ptr[w + (h / 2) * m_width] - ptr[h * m_width];
            return P0 - N0;
        }
        /** Returns the value of calculating the dY Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w + (h / 2) * m_width] - ptr[w] - ptr[(h / 2) * m_width];
                P0 = ptr[(h / 2) * m_width] + ptr[w + h * m_width] - ptr[w + (h / 2) * m_width] - ptr[h * m_width];
                values[channel] = P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX2 ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
            P0 = ptr[w / 4] + ptr[3 * w / 4 + h * m_width] - ptr[3 * w / 4] - ptr[w / 4 + h * m_width];
            return 2 * P0 - N0;
        }
        /** Returns the value of calculating the dX2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
                P0 = ptr[w / 4] + ptr[3 * w / 4 + h * m_width] - ptr[3 * w / 4] - ptr[w / 4 + h * m_width];
                values[channel] = 2 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dY2 ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dY2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
            P0 = ptr[(h / 4) * m_width] + ptr[w + (3 * h / 4) * m_width] - ptr[w + (h / 4) * m_width] - ptr[(3 * h / 4) * m_width];
            return 2 * P0 - N0;
        }
        /** Returns the value of calculating the dY2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
                P0 = ptr[(h / 4) * m_width] + ptr[w + (3 * h / 4) * m_width] - ptr[w + (h / 4) * m_width] - ptr[(3 * h / 4) * m_width];
                values[channel] = 2 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX2dY2 ]======================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX2dY2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
            P0 = ptr[w / 4 + (h / 4) * m_width] + ptr[3 * w / 4 + (3 * h / 4) * m_width] - ptr[3 * w / 4 + (h / 4) * m_width] - ptr[w / 4 + (3 * h / 4) * m_width];
            return 4 * P0 - N0;
        }
        /** Returns the value of calculating the dX2dY2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
                P0 = ptr[w / 4 + (h / 4) * m_width] + ptr[3 * w / 4 + (3 * h / 4) * m_width] - ptr[3 * w / 4 + (h / 4) * m_width] - ptr[w / 4 + (3 * h / 4) * m_width];
                values[channel] = 4 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dXdY ]========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dXdY Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, P1, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
            P0 = ptr[(h / 2) * m_width] + ptr[w / 2 + h * m_width] - ptr[h * m_width] - ptr[w / 2 + (h / 2) * m_width];
            P1 = ptr[w / 2] + ptr[w + (h / 2) * m_width] - ptr[w] - ptr[w / 2 + (h / 2) * m_width];
            return 2 * (P0 + P1)- N0;
        }
        /** Returns the value of calculating the dXdY Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, P1, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = *ptr + ptr[w + h * m_width] - ptr[w] - ptr[h * m_width];
                P0 = ptr[(h / 2) * m_width] + ptr[w / 2 + h * m_width] - ptr[h * m_width] - ptr[w / 2 + (h / 2) * m_width];
                P1 = ptr[w / 2] + ptr[w + (h / 2) * m_width] - ptr[w] - ptr[w / 2 + (h / 2) * m_width];
                values[channel] = 2 * (P0 + P1) - N0;
            }
        }
    private:
        /// Width of the integral image (i.e.\ width of the original image + 1)
        unsigned int m_width;
        /// Height of the integral image (i.e.\ height of the original image + 1)
        unsigned int m_height;
        /// Number of channels of the integral image.
        unsigned int m_number_of_channels;
        /// Integral images.
        T **m_image;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | INTEGRAL IMAGE CLASS IMPLEMENTATION  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which calculates the integral image for a standard image and calculates the Haar-like features.
    template <class T>
    IntegralImage<T>::IntegralImage(void) :
        m_width(0),
        m_height(0),
        m_number_of_channels(0),
        m_image(0)
    {
    }
    
    template <class T>
    template <template <class> class IMAGE, class U>
    IntegralImage<T>::IntegralImage(const IMAGE<U> &image, unsigned int number_of_threads)
    {
        if (image.get() != 0)
        {
            const unsigned int image_width = image.getWidth();
            const unsigned int image_height = image.getHeight();
            const unsigned int image_channels = image.getNumberOfChannels();
            
            m_width = image_width + 1;
            m_height = image_height + 1;
            m_number_of_channels = image_channels;
            m_image = new T * [image_channels];
            // Allocate the memory for each integral layer.
            for (unsigned int c = 0; c < image_channels; ++c)
                m_image[c] = new T[m_width * m_height];
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int c = omp_get_thread_num(); c < image_channels; c += number_of_threads)
                {
                    // Set the first row of the integral image to zero.
                    const T * __restrict__ previous_ptr = m_image[c];
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    for (unsigned int x = 0; x < m_width; ++x, ++integral_ptr)
                        *integral_ptr = 0;
                    for (unsigned int h = 0; h < image_height; ++h)
                    {
                        const U * __restrict__ image_ptr = image.get(h, c);
                        register T accumulated_row;
                        
                        // Set the first column of the integral image to zero.
                        *integral_ptr = accumulated_row = 0;
                        ++integral_ptr;
                        ++previous_ptr;
                        for (unsigned int w = 0; w < image_width; ++w, ++image_ptr, ++previous_ptr, ++integral_ptr)
                        {
                            accumulated_row += (T)*image_ptr;
                            *integral_ptr = accumulated_row + *previous_ptr;
                        }
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    IntegralImage<T>::IntegralImage(const IntegralImage<T> &copy) :
        m_width(copy.m_width),
        m_height(copy.m_height),
        m_number_of_channels(copy.m_number_of_channels),
        m_image((copy.m_image != 0)?new T*[copy.m_number_of_channels]:0)
    {
        for (unsigned int c = 0; c < copy.m_number_of_channels; ++c)
        {
            m_image[c] = new T[copy.m_width * copy.m_height];
            for (unsigned int i = 0; i < copy.m_width * copy.m_height; ++i) m_image[c][i] = copy.m_image[c][i];
        }
    }
    
    template <class T>
    template <class U> IntegralImage<T>::IntegralImage(const IntegralImage<U> &copy) :
        m_width(copy.getWidth()),
        m_height(copy.getHeight()),
        m_number_of_channels(copy.getNumberOfChannels()),
        m_image((copy.isEmpty())?0:new T*[copy.getNumberOfChannels()])
    {
        for (unsigned int c = 0; c < copy.getNumberOfChannels(); ++c)
        {
            m_image[c] = new T[copy.getWidth() * copy.getHeight()];
            for (unsigned int i = 0; i < copy.getWidth() * copy.getHeight(); ++i) m_image[c][i] = (T)copy.getData(c)[i];
        }
    }
    
    template <class T>
    IntegralImage<T>::~IntegralImage(void)
    {
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
            delete [] m_image;
        }
    }
    
    template <class T>
    IntegralImage<T>& IntegralImage<T>::operator=(const IntegralImage<T> &copy)
    {
        if (this != &copy)
        {
            // Free .................................................................................................................................
            if (m_image != 0)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
                delete [] m_image;
            }
            
            // Copy .................................................................................................................................
            m_width = copy.m_width;
            m_height = copy.m_height;
            m_number_of_channels = copy.m_number_of_channels;
            if (copy.m_image != 0)
            {
                m_image = new T*[copy.m_number_of_channels];
                for (unsigned int c = 0; c < copy.m_number_of_channels; ++c)
                {
                    m_image[c] = new T[copy.m_width * copy.m_height];
                    for (unsigned int i = 0; i < copy.m_width * copy.m_height; ++i) m_image[c][i] = copy.m_image[c][i];
                }
            }
            else m_image = 0;
        }
        
        return *this;
    }
    
    template <class T>
    template <class U> IntegralImage<T>& IntegralImage<T>::operator=(const IntegralImage<U> &copy)
    {
        // Both integral images are from a different type, so that, they cannot be the same.
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
            delete [] m_image;
        }
        // Copy .....................................................................................................................................
        m_width = copy.getWidth();
        m_height = copy.getHeight();
        m_number_of_channels = copy.getNumberOfChannels();
        if (copy.isEmpty()) m_image = 0;
        else
        {
            m_image = new T*[copy.getNumberOfChannels()];
            for (unsigned int c = 0; c < copy.getNumberOfChannels(); ++c)
            {
                m_image[c] = new T[copy.getWidth() * copy.getHeight()];
                for (unsigned int i = 0; i < copy.getWidth() * copy.getHeight(); ++i)
                    m_image[c][i] = (T)copy.getData(c)[i];
            }
        }
        return *this;
    }
    
    template <class T>
    template <template <class> class IMAGE, class U>
    void IntegralImage<T>::set(const IMAGE<U> &image, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
            delete [] m_image;
        }
        
        // Create ...................................................................................................................................
        if (image.get() != 0)
        {
            const unsigned int image_width = image.getWidth();
            const unsigned int image_height = image.getHeight();
            const unsigned int image_channels = image.getNumberOfChannels();
            m_width = image_width + 1;
            m_height = image_height + 1;
            m_number_of_channels = image_channels;
            m_image = new T * [image_channels];
            // Allocate the memory for each integral layer.
            for (unsigned int c = 0; c < image_channels; ++c)
                m_image[c] = new T[m_width * m_height];
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int c = omp_get_thread_num(); c < image_channels; c += number_of_threads)
                {
                    // Set the first row of the integral image to zero.
                    const T * __restrict__ previous_ptr = m_image[c];
                    T * __restrict__ integral_ptr = m_image[c];
                    for (unsigned int x = 0; x < m_width; ++x, ++integral_ptr)
                        *integral_ptr = 0;
                    
                    for (unsigned int h = 0; h < image_height; ++h)
                    {
                        const U * __restrict__ image_ptr = image.get(h, c);
                        register T accumulated_row;
                        
                        // Set the first column of the integral image to zero.
                        *integral_ptr = accumulated_row = 0;
                        ++integral_ptr;
                        ++previous_ptr;
                        for (unsigned int w = 0; w < image_width; ++w, ++image_ptr, ++previous_ptr, ++integral_ptr)
                        {
                            accumulated_row += (T)*image_ptr;
                            *integral_ptr = accumulated_row + *previous_ptr;
                        }
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | INTEGRAL IMAGE AUXILIARY FUNCTIONS   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Calculates the sum over an image rectangular region using an integral image.
     *  \param[in] integral_image integral image of the original image.
     *  \param[in] region_width width of the rectangular region.
     *  \param[in] region_height height of the rectangular region.
     *  \param[out] result image or sub-image where the resulting sums are stored.
     *  \param[in] number_of_threads number of threads used to concurrently calculate the sums.
     */
    template <class TINTEGRAL, template <class> class IMAGE, class TIMAGE>
    void integralImageSum(const IntegralImage<TINTEGRAL> &integral_image, unsigned int region_width, unsigned int region_height, IMAGE<TIMAGE> &result, unsigned int number_of_threads = 1)
    {
        if ((integral_image.getWidth() > 1) && (integral_image.getHeight() > 1))
        {
            const unsigned int width = integral_image.getWidth() - 1;
            const unsigned int height = integral_image.getHeight() - 1;
            const unsigned int channels = integral_image.getNumberOfChannels();
            const unsigned int offset_width_begin = region_width / 2;
            const unsigned int offset_width_end = region_width - region_width / 2;
            const unsigned int offset_height_begin = region_height / 2;
            const unsigned int offset_height_end = region_height - region_height / 2;
            const unsigned int ii_width = integral_image.getWidth();
            
            // Check result image geometry.
            if ((result.getWidth() != width) || (result.getHeight() != height) || (result.getNumberOfChannels() != channels))
                result.setGeometry(width, height, channels);
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int c = 0; c < channels; ++c)
                {
                // First pass over the integral image (bottom-left and bottom-right corners) ..................................
                    const unsigned int thread_id = omp_get_thread_num();
                    const TINTEGRAL * top_left_ptr, * top_right_ptr, * bottom_left_ptr, * bottom_right_ptr;
                    unsigned int y;
                    
                    // INITIALIZATION: Bottom pointer initial locations.
                    bottom_left_ptr  = integral_image.getData(c) + offset_height_end * ii_width                    + thread_id * ii_width;
                    bottom_right_ptr = integral_image.getData(c) + offset_height_end * ii_width + offset_width_end + thread_id * ii_width;
                    
                    // REGION A: First rows where only bottom pointers move downwards.
                    for (y = thread_id; y < offset_height_begin; y += number_of_threads)
                    {
                        TIMAGE * __restrict__ result_ptr = result.get(y, c);
                        unsigned int x;
                        
                        // REGION 1: Only right pointer moves, left pointer value is 0.
                        for (x = 0; x < offset_width_begin; ++x, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*bottom_right_ptr - *bottom_left_ptr);
                        // REGION 2: Both pointers move.
                        for (; x < width - offset_width_end; ++x, ++bottom_left_ptr, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*bottom_right_ptr - *bottom_left_ptr);
                        // REGION 3: Only left pointer moves.
                        for (; x < width; ++x, ++bottom_left_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*bottom_right_ptr - *bottom_left_ptr);
                        // Move the integral image pointers to the next row.
                        bottom_left_ptr  += 1 + offset_width_begin + ii_width * (number_of_threads - 1);
                        bottom_right_ptr += 1 + offset_width_end   + ii_width * (number_of_threads - 1);
                    }
                    // INITIALIZATION: Bottom pointer top locations (the initial row depends on the location
                    //                 of the current thread bottom pointers).
                    top_left_ptr     = integral_image.getData(c)                    + (y - offset_height_begin) * ii_width;
                    top_right_ptr    = integral_image.getData(c) + offset_width_end + (y - offset_height_begin) * ii_width;
                    
                    // REGION B: All pointers move downwards until the bottom of the image.
                    for (; y < height - offset_height_end; y += number_of_threads)
                    {
                        TIMAGE * __restrict__ result_ptr = result.get(y, c);
                        unsigned int x;
                        
                        // REGION 1: Only right pointer moves, left pointer value is 0.
                        for (x = 0; x < offset_width_begin; ++x, ++top_right_ptr, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // REGION 2: Both pointers move.
                        for (; x < width - offset_width_end; ++x, ++top_left_ptr, ++top_right_ptr, ++bottom_left_ptr, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // REGION 3: Only left pointer moves.
                        for (; x < width; ++x, ++top_left_ptr, ++bottom_left_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // Move the integral image pointers to the next row.
                        bottom_left_ptr  += 1 + offset_width_begin + ii_width * (number_of_threads - 1);
                        bottom_right_ptr += 1 + offset_width_end   + ii_width * (number_of_threads - 1);
                        top_left_ptr     += 1 + offset_width_begin + ii_width * (number_of_threads - 1);
                        top_right_ptr    += 1 + offset_width_end   + ii_width * (number_of_threads - 1);
                    }
                    // REGION C: The last rows of the image the integral image pointers does not move from the last row.
                    bottom_left_ptr  = integral_image.getData(c) + (height) * ii_width                   ;
                    bottom_right_ptr = integral_image.getData(c) + (height) * ii_width + offset_width_end;
                    for (; y < height; y += number_of_threads)
                    {
                        TIMAGE * __restrict__ result_ptr = result.get(y, c);
                        unsigned int x;
                        
                        // REGION 1: Only right pointer moves, left pointer value is 0.
                        for (x = 0; x < offset_width_begin; ++x, ++top_right_ptr, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // REGION 2: Both pointers move.
                        for (; x < width - offset_width_end; ++x, ++top_left_ptr, ++top_right_ptr, ++bottom_left_ptr, ++bottom_right_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // REGION 3: Only left pointer moves.
                        for (; x < width; ++x, ++top_left_ptr, ++bottom_left_ptr, ++result_ptr)
                            *result_ptr = (TIMAGE)(*top_left_ptr + *bottom_right_ptr - *bottom_left_ptr - *top_right_ptr);
                        // Move bottom pointers back to the beginning of the row.
                        bottom_left_ptr  -= width - offset_width_begin;
                        bottom_right_ptr -= width - offset_width_end;
                        // Move top pointers to the next row.
                        top_left_ptr     += 1 + offset_width_begin + ii_width * (number_of_threads - 1);
                        top_right_ptr    += 1 + offset_width_end   + ii_width * (number_of_threads - 1);
                        
                    }
                }
            }
        }
        else result.clear();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | 45 DEGREE INTEGRAL IMAGE CLASS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which calculates the rotated integral image for a standard image and calculates the 45 degrees rotates Haar-like features.
    template <class T>
    class IntegralImageRotated
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        IntegralImageRotated(void);
        /** Constructor where integral image is build form an image.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        IntegralImageRotated(const Image<T> &image, unsigned int number_of_threads = 1);
        /** Constructor where integral image is build form an image of a different data type.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <class U> IntegralImageRotated(const Image<U> &image, unsigned int number_of_threads = 1);
        /** Constructor where integral image is build form a sub-image.
         *  \param[in] image sub-image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        IntegralImageRotated(const SubImage<T> &image, unsigned int number_of_threads = 1);
        /** Constructor where integral image is build form an sub-image of a different data type.
         *  \param[in] image sub-image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <class U> IntegralImageRotated(const SubImage<U> &image, unsigned int number_of_threads = 1);
        /// Copy constructor.
        IntegralImageRotated(const IntegralImageRotated<T> &copy);
        /// Copy constructor for an integral image of a different type.
        template <class U> IntegralImageRotated(const IntegralImageRotated<U> &copy);
        /// Destructor
        ~IntegralImageRotated(void);
        /// Assignation operator.
        IntegralImageRotated<T>& operator=(const IntegralImageRotated<T> &copy);
        /// Assignation operator of an integral image of a different type.
        template <class U> IntegralImageRotated<T>& operator=(const IntegralImageRotated<U> &copy);
        
        /** Function which initializes the integral image from the given image.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        void set(const Image<T> &image, unsigned int number_of_threads = 1);
        /** Function which initializes the integral image from a image of a different data type.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <class U> void set(const Image<U> &image, unsigned int number_of_threads = 1);
        /** Function which initializes the integral image from the given sub-image.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        void set(const SubImage<T> &image, unsigned int number_of_threads = 1);
        /** Function which initializes the integral image from a sub-image of a different data type.
         *  \param[in] image image which is used to build the integral image.
         *  \param[in] number_of_threads number of threads which are used to process the original image channels concurrently.
         */
        template <class U> void set(const SubImage<U> &image, unsigned int number_of_threads = 1);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the width of the integral image (i.e.\ width of the original image plus an extra column to add a left border of zeros to the integral image).
        inline unsigned int getWidth(void) const { return m_width; }
        /// Returns the height of the integral image (i.e.\ height of the original image plus an extra row to add a top border of zeros to the integral image).
        inline unsigned int getHeight(void) const { return m_height; }
        /// Returns the number of channels of the integral image.
        inline unsigned int getNumberOfChannels(void) const { return m_number_of_channels; }
        /// Returns a constant pointer to the integral image of the specified channel.
        inline const T* getData(unsigned int channel) const { return m_image[channel]; }
        /// Returns a boolean which indicates if the integral image is empty (i.e.\ it is not initialized).
        inline bool isEmpty(void) const { return m_image == 0; }
        
        // -[ Haar-like features ]-------------------------------------------------------------------------------------------------------------------
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ SUM OF THE VALUES IN THE RECTANGLE ]=======================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the sum of the values of a rectangle for the specified channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            return ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
        }
        /** Returns the sum of the values of a rectangle for each channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                values[channel] = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX ]==========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = ptr[h / 2 + (h / 2) * m_width] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h / 2 + w) + (w + h / 2) * m_width];
            P0 = ptr[h] + ptr[(h / 2) + w + (h / 2 + w) * m_width] - ptr[h / 2 + (h / 2) * m_width] - ptr[(h + w) + w * m_width];
            return P0 - N0;
        }
        /** Returns the value of calculating the dX Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = ptr[h / 2 + (h / 2) * m_width] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h / 2 + w) + (w + h / 2) * m_width];
                P0 = ptr[h] + ptr[(h / 2) + w + (h / 2 + w) * m_width] - ptr[h / 2 + (h / 2) * m_width] - ptr[(h + w) + w * m_width];
                values[channel] = P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dY ]==========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dY Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            P0 = ptr[h] + ptr[(w / 2) + (h + w / 2) * m_width] - ptr[h * m_width] - ptr[(h + w / 2) + (w / 2) * m_width];
            N0 = ptr[w / 2 + h + (w / 2) * m_width] + ptr[w + (h + w) * m_width] - ptr[w / 2 + (h + w / 2) * m_width] - ptr[(h + w) + w * m_width];
            return P0 - N0;
        }
        /** Returns the value of calculating the dY Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                P0 = ptr[h] + ptr[(w / 2) + (h + w / 2) * m_width] - ptr[h * m_width] - ptr[(h + w / 2) + (w / 2) * m_width];
                N0 = ptr[w / 2 + h + (w / 2) * m_width] + ptr[w + (h + w) * m_width] - ptr[w / 2 + (h + w / 2) * m_width] - ptr[(h + w) + w * m_width];
                values[channel] = P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX2 ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const unsigned int h2 = h / 2;
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
            ptr = m_image[channel] + (x + h / 4) + (y + h / 4) * m_width;
            P0 = ptr[h2] + ptr[w + (h2 + w) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w) + w * m_width];
            return 2 * P0 - N0;
        }
        /** Returns the value of calculating the dX2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            const unsigned int h2 = h / 2;
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
                ptr = m_image[channel] + (x + h / 4) + (y + h / 4) * m_width;
                P0 = ptr[h2] + ptr[w + (h2 + w) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w) + w * m_width];
                values[channel] = 2 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dY2 ]=========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dY2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const unsigned int w2 = w / 2;
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
            ptr = m_image[channel] + (x + w / 4) + (y + w / 4) * m_width;
            P0 = ptr[h] + ptr[w2 + (h + w2) * m_width] - ptr[h * m_width] - ptr[(h + w2) + w2 * m_width];
            return 2 * P0 - N0;
        }
        /** Returns the value of calculating the dY2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            const unsigned int w2 = w / 2;
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
                ptr = m_image[channel] + (x + w / 4) + (y + w / 4) * m_width;
                P0 = ptr[h] + ptr[w2 + (h + w2) * m_width] - ptr[h * m_width] - ptr[(h + w2) + w2 * m_width];
                values[channel] = 2 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dX2dY2 ]======================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dX2dY2 Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const unsigned int w2 = w / 2;
            const unsigned int h2 = h / 2;
            T P0, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
            ptr = m_image[channel] + (x + w / 4 + h / 4) + (y + w / 4 + h / 4) * m_width;
            P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
            return 4 * P0 - N0;
        }
        /** Returns the value of calculating the dX2dY2 Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            const unsigned int w2 = w / 2;
            const unsigned int h2 = h / 2;
            T P0, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
                ptr = m_image[channel] + (x + w / 4 + h / 4) + (y + w / 4 + h / 4) * m_width;
                P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
                values[channel] = 4 * P0 - N0;
            }
        }
        //                                      /\.
        //                                     /  \.
        //                                    /    \.
        //                                   +-+  +-+.
        //                                     |  |.
        //                                     +--+.
        // =[ HAAR FEATURE dXdY ]========================================================================================================================
        //                                     +--+.
        //                                     |  |.
        //                                   +-+  +-+.
        //                                    \    /.
        //                                     \  /.
        //                                      \/.
        /** Returns the value of calculating the dXdY Haar like feature in a rectangle for the specified
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[in] channel selected channel of the integral image. By default set to zero (first channel).
         *  \return the sum of the values in the rectangle.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline T getDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int channel = 0) const
        {
            const unsigned int w2 = w / 2;
            const unsigned int h2 = h / 2;
            T P0, P1, N0;
            const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
            N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
            ptr = m_image[channel] + (x + h / 2) + y * m_width;
            P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
            ptr = m_image[channel] + (x + w / 2) + (y + w / 2 + h / 2) * m_width;
            P1 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
            return 2 * (P0 + P1)- N0;
        }
        /** Returns the value of calculating the dXdY Haar like feature in a rectangle for each
         *  channel of the integral image.
         *  \param[in] x X-coordinate of the top-left corner of the rectangle.
         *  \param[in] y Y-coordinate of the top-left corner of the rectangle.
         *  \param[in] w width of the rectangle.
         *  \param[in] h height of the rectangle.
         *  \param[out] values an array where the results for each channel of the integral image are stored.
         *  \note This function does not check that the rectangle is within the image boundaries.
         *        Therefore, a segmentation fault can be generated if wrong parameters are given.
         *  \note The rectangle coordinates are given in the original image coordinate system, not in the
         *        integral image coordinate system.
         */
        inline void getDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T *values) const
        {
            const unsigned int w2 = w / 2;
            const unsigned int h2 = h / 2;
            T P0, P1, N0;
            for (unsigned int channel = 0; channel < m_number_of_channels; ++channel)
            {
                const T * __restrict__ ptr = m_image[channel] + x + y * m_width;
                N0 = ptr[h] + ptr[w + (h + w) * m_width] - ptr[h * m_width] - ptr[(h + w) + w * m_width];
                ptr = m_image[channel] + (x + h / 2) + y * m_width;
                P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
                ptr = m_image[channel] + (x + w / 2) + (y + w / 2 + h / 2) * m_width;
                P1 = ptr[h2] + ptr[w2 + (h2 + w2) * m_width] - ptr[h2 * m_width] - ptr[(h2 + w2) + w2 * m_width];
                values[channel] = 2 * (P0 + P1) - N0;
            }
        }
    private:
        /// Width of the integral image (i.e.\ width of the original image + 1)
        unsigned int m_width;
        /// Height of the integral image (i.e.\ height of the original image + 1)
        unsigned int m_height;
        /// Number of channels of the integral image.
        unsigned int m_number_of_channels;
        /// Integral images.
        T **m_image;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | INTEGRAL IMAGE CLASS IMPLEMENTATION  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class which calculates the integral image for a standard image and calculates the Haar-like features.
    template <class T>
    IntegralImageRotated<T>::IntegralImageRotated(void) :
        m_width(0),
        m_height(0),
        m_number_of_channels(0),
        m_image(0)
    {
    }
    
    template <class T>
    IntegralImageRotated<T>::IntegralImageRotated(const Image<T> &image, unsigned int number_of_threads)
    {
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const T * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const T * __restrict__ image_ptr = image.get(y, c);
                        const T * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    template <class U> IntegralImageRotated<T>::IntegralImageRotated(const Image<U> &image, unsigned int number_of_threads)
    {
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const U * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const U * __restrict__ image_ptr = image.get(y, c);
                        const U * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    IntegralImageRotated<T>::IntegralImageRotated(const SubImage<T> &image, unsigned int number_of_threads)
    {
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const T * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const T * __restrict__ image_ptr = image.get(y, c);
                        const T * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    template <class U> IntegralImageRotated<T>::IntegralImageRotated(const SubImage<U> &image, unsigned int number_of_threads)
    {
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const U * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const U * __restrict__ image_ptr = image.get(y, c);
                        const U * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    IntegralImageRotated<T>::IntegralImageRotated(const IntegralImageRotated<T> &copy) :
        m_width(copy.m_width),
        m_height(copy.m_height),
        m_number_of_channels(copy.m_number_of_channels),
        m_image((copy.m_image != 0)?new T*[copy.m_number_of_channels]:0)
    {
        for (unsigned int c = 0; c < copy.m_number_of_channels; ++c)
        {
            m_image[c] = new T[copy.m_width * copy.m_height];
            for (unsigned int i = 0; i < copy.m_width * copy.m_height; ++i) m_image[c][i] = copy.m_image[c][i];
        }
    }
    
    template <class T>
    template <class U> IntegralImageRotated<T>::IntegralImageRotated(const IntegralImageRotated<U> &copy) :
        m_width(copy.getWidth()),
        m_height(copy.getHeight()),
        m_number_of_channels(copy.getNumberOfChannels()),
        m_image((copy.isEmpty())?0:new T*[copy.getNumberOfChannels()])
    {
        for (unsigned int c = 0; c < copy.getNumberOfChannels(); ++c)
        {
            m_image[c] = new T[copy.getWidth() * copy.getHeight()];
            for (unsigned int i = 0; i < copy.getWidth() * copy.getHeight(); ++i) m_image[c][i] = (T)copy.getData(c)[i];
        }
    }
    
    template <class T>
    IntegralImageRotated<T>::~IntegralImageRotated(void)
    {
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
            delete [] m_image;
        }
    }
    
    template <class T>
    IntegralImageRotated<T>& IntegralImageRotated<T>::operator=(const IntegralImageRotated<T> &copy)
    {
        if (this != &copy)
        {
            // Free .................................................................................................................................
            if (m_image != 0)
            {
                for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
                delete [] m_image;
            }
            
            // Copy .................................................................................................................................
            m_width = copy.m_width;
            m_height = copy.m_height;
            m_number_of_channels = copy.m_number_of_channels;
            if (copy.m_image != 0)
            {
                m_image = new T*[copy.m_number_of_channels];
                for (unsigned int c = 0; c < copy.m_number_of_channels; ++c)
                {
                    m_image[c] = new T[copy.m_width * copy.m_height];
                    for (unsigned int i = 0; i < copy.m_width * copy.m_height; ++i) m_image[c][i] = copy.m_image[c][i];
                }
            }
            else m_image = 0;
        }
        
        return *this;
    }
    
    template <class T>
    template <class U> IntegralImageRotated<T>& IntegralImageRotated<T>::operator=(const IntegralImageRotated<U> &copy)
    {
        // Both integral images are from a different type, so that, they cannot be the same.
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < m_number_of_channels; ++c) delete [] m_image[c];
            delete [] m_image;
        }
        // Copy .....................................................................................................................................
        m_width = copy.getWidth();
        m_height = copy.getHeight();
        m_number_of_channels = copy.getNumberOfChannels();
        if (copy.isEmpty()) m_image = 0;
        else
        {
            m_image = new T*[copy.getNumberOfChannels()];
            for (unsigned int c = 0; c < copy.getNumberOfChannels(); ++c)
            {
                m_image[c] = new T[copy.getWidth() * copy.getHeight()];
                for (unsigned int i = 0; i < copy.getWidth() * copy.getHeight(); ++i)
                    m_image[c][i] = (T)copy.getData(c)[i];
            }
        }
        return *this;
    }
    
    template <class T>
    void IntegralImageRotated<T>::set(const Image<T> &image, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c) delete [] m_image[c];
            delete [] m_image;
        }
        
        // Create ...................................................................................................................................
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const T * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const T * __restrict__ image_ptr = image.get(y, c);
                        const T * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    template <class U> void IntegralImageRotated<T>::set(const Image<U> &image, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c) delete [] m_image[c];
            delete [] m_image;
        }
        
        // Create ...................................................................................................................................
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const U * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const U * __restrict__ image_ptr = image.get(y, c);
                        const U * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    void IntegralImageRotated<T>::set(const SubImage<T> &image, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c) delete [] m_image[c];
            delete [] m_image;
        }
        
        // Create ...................................................................................................................................
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const T * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const T * __restrict__ image_ptr = image.get(y, c);
                        const T * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    template <class T>
    template <class U> void IntegralImageRotated<T>::set(const SubImage<U> &image, unsigned int number_of_threads)
    {
        // Free .....................................................................................................................................
        if (m_image != 0)
        {
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c) delete [] m_image[c];
            delete [] m_image;
        }
        
        // Create ...................................................................................................................................
        if (image.get() != 0)
        {
            m_number_of_channels = image.getNumberOfChannels();
            m_width = image.getWidth() + 2;
            m_height = image.getHeight() + 1;
            m_image = new T*[image.getNumberOfChannels()];
            
            for (unsigned int c = 0; c < image.getNumberOfChannels(); ++c)
                m_image[c] = new T[(image.getWidth() + 2) * (image.getHeight() + 1)];
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_identifier = omp_get_thread_num();
                
                for (unsigned int c = thread_identifier; c < image.getNumberOfChannels(); c += number_of_threads)
                {
                    T * __restrict__ integral_ptr = m_image[c];
                    
                    // Set the first row to zero.
                    for (unsigned int w = 0; w < m_width; ++w)
                    {
                        *integral_ptr = 0;
                        ++integral_ptr;
                    }
                    // Initialize the second row.
                    *integral_ptr = 0;
                    ++integral_ptr;
                    const U * __restrict__ row_ptr = image.get(0, c);
                    for (unsigned int w = 0; w < image.getWidth(); ++w)
                    {
                        *integral_ptr = (T)*row_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1);
                        ++row_ptr;
                        ++integral_ptr;
                    }
                    *integral_ptr = 0;
                    ++integral_ptr;
                    // Remaining rows.
                    for (unsigned int y = 1; y < image.getHeight(); ++y)
                    {
                        const U * __restrict__ image_ptr = image.get(y, c);
                        const U * __restrict__ image_previous_ptr = image.get(y - 1, c);
                        *integral_ptr = *(integral_ptr - m_width + 1);
                        ++integral_ptr;
                        for (unsigned int x = 0; x < image.getWidth(); ++x)
                        {
                            *integral_ptr = (T)*image_previous_ptr + (T)*image_ptr + *(integral_ptr - m_width - 1) + *(integral_ptr - m_width + 1) - *(integral_ptr - 2 * m_width);
                            ++image_previous_ptr;
                            ++integral_ptr;
                            ++image_ptr;
                        }
                        *integral_ptr = *(integral_ptr - m_width - 1);
                        ++integral_ptr;
                    }
                }
            }
        }
        else
        {
            m_width = 0;
            m_height = 0;
            m_number_of_channels = 0;
            m_image = 0;
        }
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
    ////// const double sqrt2 = 1.4142135623730951;
    ////// 
    ////// template <class T>
    ////// class IntegralImage
    ////// {
    ////// public:
    //////     // Constructors, destructor and assignation operator ------------------------------------------
    //////     /// Default constructor.
    //////     IntegralImage(void) :
    //////         m_image_width(0),
    //////         m_image_height(0),
    //////         m_integral_width(0),
    //////         m_integral_height(0),
    //////         m_rotated(false),
    //////         m_integral_image(0),
    //////         m_integral_image_anchor(0) {}
    //////     /// Parameters constructor where the integral image geometry is given as parameters.
    //////     IntegralImage(unsigned int width, unsigned int height, bool rotated) :
    //////         m_image_width(width),
    //////         m_image_height(height),
    //////         m_integral_width((rotated)?(unsigned int)(ceil((double)width * sqrt2) + 2):width + 1),
    //////         m_integral_height((rotated)?(unsigned int)(ceil((double)height * sqrt2) + 2):height + 1),
    //////         m_rotated(rotated),
    //////         m_integral_image(0),
    //////         m_integral_image_anchor(0)
    //////         {
    //////             if (m_integral_width * m_integral_height > 0)
    //////             {
    //////                 m_integral_image = new T[m_integral_width * m_integral_height];
    //////                 for (unsigned int i = 0; i < m_integral_width * m_integral_height; ++i) m_integral_image[i] = 0;
    //////                 if (rotated) m_integral_image_anchor = m_integral_image + m_integral_width * 2 + 1;
    //////                 else m_integral_image_anchor = m_integral_image + m_integral_width + 1;
    //////             }
    //////         }
    //////     /// Copy constructor.
    //////     IntegralImage(const IntegralImage &copy) :
    //////         m_image_width(copy.m_image_width),
    //////         m_image_height(copy.m_image_height),
    //////         m_integral_width(copy.m_integral_width),
    //////         m_integral_height(copy.m_integral_height),
    //////         m_rotated(copy.m_rotated),
    //////         m_integral_image((copy.m_integral_image != 0)?new T[copy.m_integral_width * copy.m_integral_height]:0),
    //////         m_integral_image_anchor(0)
    //////         {
    //////             for (unsigned int i = 0; i < copy.m_integral_width * copy.m_integral_height; ++i) m_integral_image[i] = copy.m_integral_image[i];
    //////             if (m_integral_image != 0)
    //////             {
    //////                 if (m_rotated) m_integral_image_anchor = m_integral_image + m_integral_width * 2 + 1;
    //////                 else m_integral_image_anchor = m_integral_image + m_integral_width + 1;
    //////             }
    //////         }
    //////     /// Destructor.
    //////     ~IntegralImage(void) {if (m_integral_image != 0) delete [] m_integral_image;}
    //////     /// Assignation operator.
    //////     IntegralImage& operator=(const IntegralImage &copy)
    //////     {
    //////         if (this != &copy)
    //////         {
    //////             // Free ...............................................................................
    //////             if (m_integral_image != 0) delete [] m_integral_image;
    //////             
    //////             // Copy ...............................................................................
    //////             m_image_width = copy.m_image_width;
    //////             m_image_height = copy.m_image_height;
    //////             m_integral_width = copy.m_integral_width;
    //////             m_integral_height = copy.m_integral_height;
    //////             m_rotated = copy.m_rotated;
    //////             if (copy.m_integral_image != 0)
    //////             {
    //////                 m_integral_image = new T[copy.m_integral_width * copy.m_integral_height];
    //////                 for (unsigned int i = 0; i < copy.m_integral_width * copy.m_integral_height; ++i) m_integral_image[i] = copy.m_integral_image[i];
    //////                 if (m_rotated) m_integral_image_anchor = m_integral_image + m_integral_width * 2 + 1;
    //////                 else m_integral_image_anchor = m_integral_image + m_integral_width + 1;
    //////             }
    //////             else m_integral_image_anchor = m_integral_image = 0;
    //////         }
    //////         return *this;
    //////     }
    //////     
    //////     // Access to the values of the integral images ------------------------------------------------
    //////     /// Reset all the integral image values to the given value (except the integral image margin values).
    //////     void set(const T &value)
    //////     {
    //////         T *ptr;
    //////         ptr = m_integral_image_anchor;
    //////         if (m_rotated)
    //////         {
    //////             --ptr;
    //////             for (unsigned int y = 0; y < m_image_height; ++y)
    //////             {
    //////                 ++ptr;
    //////                 for (unsigned int x = 0; x < m_image_width; ++x, ++ptr) *ptr = value;
    //////                 ++ptr;
    //////             }
    //////         }
    //////         else
    //////         {
    //////             --ptr;
    //////             for (unsigned int y = 0; y < m_image_height; ++y)
    //////             {
    //////                 ++ptr;
    //////                 for (unsigned int x = 0; x < m_image_width; ++x) *ptr = value;
    //////             }
    //////         }
    //////     }
    //////     /// Resizes the integral image to the given geometry and integral orientation.
    //////     void set(unsigned int width, unsigned int height, bool rotated)
    //////     {
    //////         // Free allocated memory.
    //////         if (m_integral_image != 0) delete [] m_integral_image;
    //////         // Set new parameters.
    //////         m_image_width = width;
    //////         m_image_height = height;
    //////         if (rotated)
    //////         {
    //////             m_integral_width = (unsigned int)(ceil((double)width * sqrt2) + 2);
    //////             m_integral_height = (unsigned int)(ceil((double)height * sqrt2) + 2);
    //////         }
    //////         else
    //////         {
    //////             m_integral_width = width + 1;
    //////             m_integral_height = height + 1;
    //////         }
    //////         m_rotated = rotated;
    //////         if (m_integral_width * m_integral_height > 0)
    //////         {
    //////             m_integral_image = new T[m_integral_width * m_integral_height];
    //////             for (unsigned int i = 0; i < m_integral_width * m_integral_height; ++i)
    //////                 m_integral_image[i] = 0;
    //////             if (rotated) m_integral_image_anchor = m_integral_image + m_integral_width * 2 + 1;
    //////             else m_integral_image_anchor = m_integral_image + m_integral_width + 1;
    //////         }
    //////         else m_integral_image_anchor = m_integral_image = 0;
    //////     }
    //////     /// Returns a constant reference to the <x, y> coordinates value of the integral image.
    //////     inline const T& operator()(unsigned int x, unsigned int y) const {return m_integral_image_anchor[x + y * m_integral_width];}
    //////     /// Returns a reference to the <x, y> coordinates value of the integral image.
    //////     inline T& operator()(unsigned int x, unsigned int y) {return m_integral_image_anchor[x + y * m_integral_width];}
    //////     /// Returns a constant pointer to the <y> row of the integral image.
    //////     inline const T* operator()(unsigned int y) const {return &m_integral_image_anchor[y * m_integral_width];}
    //////     /// Returns a pointer to the <y> row of the integral image.
    //////     inline T* operator()(unsigned int y) {return &m_integral_image_anchor[y * m_integral_width];}
    //////     /// Returns the width of the integral image (without the margin(s)).
    //////     inline unsigned int getWidth(void) const {return m_image_width;}
    //////     /// Returns the height of the integram image (without the margin).
    //////     inline unsigned int getHeight(void) const {return m_image_height;}
    //////     /// Returns the area of a feature given a width and height.
    //////     inline unsigned int getArea(unsigned int w, unsigned int h) const
    //////     {
    //////         if (m_rotated) return (w + h) * (w + h - 1) - (w * (w - 1) + h * (h - 1));
    //////         else return w * h;
    //////     }
    //////     
    //////     // Create the integral image ------------------------------------------------------------------
    //////     void create(unsigned int number_of_threads);
    //////     
    //////     // Return the Haar-like features --------------------------------------------------------------
    //////     // --------------------------------------------------------------------------------------------
    //////     // --------------------------------------------------------------------------------------------
    //////     
    //////     inline T getSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T *ptr = m_integral_image + x + y * m_integral_width;
    //////         return *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////     }
    //////     inline T getDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w / 2 + h * m_integral_width] - ptr[w / 2] - ptr[h * m_integral_width];
    //////         P0 = ptr[w / 2] + ptr[w + h * m_integral_width] - ptr[w] - ptr[w / 2 + h * m_integral_width];
    //////         return P0 - N0;
    //////     }
    //////     inline T getDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + (h / 2) * m_integral_width] - ptr[w] - ptr[(h / 2) * m_integral_width];
    //////         P0 = ptr[(h / 2) * m_integral_width] + ptr[w + h * m_integral_width] - ptr[w + (h / 2) * m_integral_width] - ptr[h * m_integral_width];
    //////         return P0 - N0;
    //////     }
    //////     inline void getGradient(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T &dx, T &dy) const
    //////     {
    //////         T N0, P0, P1, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////         P0 = ptr[w / 2] + ptr[w + h * m_integral_width] - ptr[w] - ptr[w / 2 + h * m_integral_width];
    //////         P1 = ptr[(h / 2) * m_integral_width] + ptr[w + h * m_integral_width] - ptr[w + (h / 2) * m_integral_width] - ptr[h * m_integral_width];
    //////         dx = 2 * P0 - N0;
    //////         dy = 2 * P1 - N0;
    //////     }
    //////     inline T getDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////         P0 = ptr[w / 4] + ptr[3 * w / 4 + h * m_integral_width] - ptr[3 * w / 4] - ptr[w / 4 + h * m_integral_width];
    //////         return 2 * P0 - N0;
    //////     }
    //////     inline T getDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////         P0 = ptr[(h / 4) * m_integral_width] + ptr[w + (3 * h / 4) * m_integral_width] - ptr[w + (h / 4) * m_integral_width] - ptr[(3 * h / 4) * m_integral_width];
    //////         return 2 * P0 - N0;
    //////     }
    //////     inline T getDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////         P0 = ptr[w / 4 + (h / 4) * m_integral_width] + ptr[3 * w / 4 + (3 * h / 4) * m_integral_width] - ptr[3 * w / 4 + (h / 4) * m_integral_width] - ptr[w / 4 + (3 * h / 4) * m_integral_width];
    //////         return 4 * P0 - N0;
    //////     }
    //////     inline T getDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, P1, N0, *ptr;
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = *ptr + ptr[w + h * m_integral_width] - ptr[w] - ptr[h * m_integral_width];
    //////         P0 = ptr[(h / 2) * m_integral_width] + ptr[w / 2 + h * m_integral_width] - ptr[h * m_integral_width] - ptr[w / 2 + (h / 2) * m_integral_width];
    //////         P1 = ptr[w / 2] + ptr[w + (h / 2) * m_integral_width] - ptr[w] - ptr[w / 2 + (h / 2) * m_integral_width];
    //////         return 2 * (P0 + P1) - N0;
    //////     }
    //////     
    //////     // Returns the 45 degrees rotated Haar-like features ------------------------------------------
    //////     // --------------------------------------------------------------------------------------------
    //////     // --------------------------------------------------------------------------------------------
    //////     inline T getRotatedSum(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         T *ptr = m_integral_image + x + y * m_integral_width;
    //////         return ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////     }
    //////     inline T getRotatedDx(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h / 2 + (h / 2) * m_integral_width] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h / 2 + w) + (w + h / 2) * m_integral_width];
    //////         P0 = ptr[h] + ptr[(h / 2) + w + (h / 2 + w) * m_integral_width] - ptr[h / 2 + (h / 2) * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         return P0 - N0;
    //////     }
    //////     inline T getRotatedDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         T P0, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         P0 = ptr[h] + ptr[(w / 2) + (h + w / 2) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w / 2) + (w / 2) * m_integral_width];
    //////         N0 = ptr[w / 2 + h + (w / 2) * m_integral_width] + ptr[w + (h + w) * m_integral_width] - ptr[w / 2 + (h + w / 2) * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         return P0 - N0;
    //////     }
    //////     inline void getRotatedGradient(unsigned int x, unsigned int y, unsigned int w, unsigned int h, T &dx, T &dy) const
    //////     {
    //////         T P0, P1, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         P0 = ptr[h] + ptr[(h / 2) + w + (h / 2 + w) * m_integral_width] - ptr[h / 2 + (h / 2) * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         P1 = ptr[h] + ptr[(w / 2) + (h + w / 2) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w / 2) + (w / 2) * m_integral_width];
    //////         dx = 2 * P0 - N0;
    //////         dy = 2 * P1 - N0;
    //////     }
    //////     inline T getRotatedDx2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         unsigned int h2 = h / 2;
    //////         T P0, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         ptr = m_integral_image + (x + h / 4) + (y + h / 4) * m_integral_width;
    //////         P0 = ptr[h2] + ptr[w + (h2 + w) * m_integral_width] - ptr[h2 * m_integral_width] - ptr[(h2 + w) + w * m_integral_width];
    //////         return 2 * P0 - N0;
    //////     }
    //////     inline T getRotatedDy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         unsigned int w2 = w / 2;
    //////         T P0, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         ptr = m_integral_image + (x + w / 4) + (y + w / 4) * m_integral_width;
    //////         P0 = ptr[h] + ptr[w2 + (h + w2) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w2) + w2 * m_integral_width];
    //////         return 2 * P0 - N0;
    //////     }
    //////     inline T getRotatedDx2Dy2(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         unsigned int w2 = w / 2;
    //////         unsigned int h2 = h / 2;
    //////         T P0, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         ptr = m_integral_image + (x + w / 4 + h / 4) + (y + w / 4 + h / 4) * m_integral_width;
    //////         P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_integral_width] - ptr[h2 * m_integral_width] - ptr[(h2 + w2) + w2 * m_integral_width];
    //////         return 4 * P0 - N0;
    //////     }
    //////     inline T getRotatedDxDy(unsigned int x, unsigned int y, unsigned int w, unsigned int h) const
    //////     {
    //////         unsigned int w2 = w / 2;
    //////         unsigned int h2 = h / 2;
    //////         T P0, P1, N0, *ptr;
    //////         x = (unsigned int)((double)x * sqrt2);
    //////         y = (unsigned int)((double)y * sqrt2);
    //////         ptr = m_integral_image + x + y * m_integral_width;
    //////         N0 = ptr[h] + ptr[w + (h + w) * m_integral_width] - ptr[h * m_integral_width] - ptr[(h + w) + w * m_integral_width];
    //////         ptr = m_integral_image + (x + h / 2) + y * m_integral_width;
    //////         P0 = ptr[h2] + ptr[w2 + (h2 + w2) * m_integral_width] - ptr[h2 * m_integral_width] - ptr[(h2 + w2) + w2 * m_integral_width];
    //////         ptr = m_integral_image + (x + w / 2) + (y + w / 2 + h / 2) * m_integral_width;
    //////         P1 = ptr[h2] + ptr[w2 + (h2 + w2) * m_integral_width] - ptr[h2 * m_integral_width] - ptr[(h2 + w2) + w2 * m_integral_width];
    //////         return 2 * (P0 + P1) - N0;
    //////     }
    ////// private:
    //////     
    //////     unsigned int m_image_width;
    //////     unsigned int m_image_height;
    //////     unsigned int m_integral_width;
    //////     unsigned int m_integral_height;
    //////     bool m_rotated;
    //////     T *m_integral_image;
    //////     T *m_integral_image_anchor;
    ////// };
    ////// 
    ////// template <class T>
    ////// void IntegralImage<T>::create(unsigned int number_of_threads)
    ////// {
    //////     T *ptr;
    //////     ptr = m_integral_image_anchor - 1;
    //////     if (m_rotated)
    //////     {
    //////         T *aux;
    //////         // 1) Scale the original image by sqrt(2).
    //////         aux = new T[m_image_width * m_image_height];
    //////         #pragma omp parallel num_threads(number_of_threads)
    //////         {
    //////             unsigned int thread_id;
    //////             
    //////             thread_id = omp_get_thread_num();
    //////             for (unsigned int y = 0, m = 0; y < m_image_height; ++y)
    //////             {
    //////                 if (y % number_of_threads == thread_id)
    //////                 {
    //////                     unsigned int n;
    //////                     n = m_integral_width * (y + 2) + 1;
    //////                     for (unsigned int x = 0; x < m_image_width; ++x, ++m, ++n)
    //////                         aux[m] = m_integral_image[n];
    //////                 }
    //////                 else m += m_image_width;
    //////             }
    //////         }
    //////         #pragma omp parallel num_threads(number_of_threads)
    //////         {
    //////             for (unsigned int y = 2; y < m_integral_height; ++y)
    //////             {
    //////                 unsigned int thread_id;
    //////                 
    //////                 thread_id = omp_get_thread_num();
    //////                 if (y % number_of_threads == thread_id)
    //////                 {
    //////                     for (unsigned int x = 1; x < m_integral_width - 1; ++x)
    //////                     {
    //////                         double rx, ry, wx, wy, v00, v01, v10, v11;
    //////                         unsigned int px, py;
    //////                         rx = (double)(x - 1) / sqrt2;
    //////                         ry = (double)(y - 2) / sqrt2;
    //////                         px = (unsigned int)rx;
    //////                         if (px > m_image_width - 2) {px = m_image_width - 2; wx = 1.0;}
    //////                         else wx = rx - (double)px;
    //////                         py = (unsigned int)ry;
    //////                         if (py > m_image_height - 2) {py = m_image_height - 2; wy = 1.0;}
    //////                         else wy = ry - (double)py;
    //////                         v00 = (1.0 - wx) * (1.0 - wy) * (double)aux[px + m_image_width * py];
    //////                         v10 = wx * (1.0 - wy) * (double)aux[(px + 1) + m_image_width * py];
    //////                         v01 = (1.0 - wx) * wy * (double)aux[px + m_image_width * (py + 1)];
    //////                         v11 = wx * wy * (double)aux[(px + 1) + m_image_width * (py + 1)];
    //////                         m_integral_image[y * m_integral_width + x] = (T)(v00 + v10 + v01 + v11);
    //////                     }
    //////                 }
    //////             }
    //////         }
    //////         delete [] aux;
    //////         // 2) Create the corresponding integral image.
    //////         for (unsigned int y = 2; y < m_integral_height; ++y)
    //////         {
    //////             aux = m_integral_image;
    //////             *ptr = *(ptr - m_integral_width + 1);
    //////             ++ptr;
    //////             for (unsigned int x = 1; x < m_integral_width - 1; ++x, ++aux, ++ptr)
    //////             {
    //////                 T value;
    //////                 
    //////                 value = *ptr; // Make a copy of the current value.
    //////                 *ptr += *(ptr - m_integral_width - 1) + *(ptr - m_integral_width + 1) - *(ptr - 2 * m_integral_width) + *aux; // Update the current value.
    //////                 *aux = value; // Copy the original value to the auxiliar array (will be used in the next row).
    //////             }
    //////             *ptr = *(ptr - m_integral_width - 1);
    //////             ++ptr;
    //////         }
    //////     }
    //////     else
    //////     {
    //////         for (unsigned int y = 1; y < m_integral_height; ++y)
    //////         {
    //////             ++ptr;
    //////             for (unsigned int x = 1; x < m_integral_width; ++x, ++ptr)
    //////                 *ptr += *(ptr - 1) + *(ptr - m_integral_width) - *(ptr - m_integral_width - 1);
    //////         }
    //////     }
    ////// }
    ////// 
    ////// template <class R, class T>
    ////// void setIntegralImage(const R *source, unsigned int row_bytes, unsigned int width, unsigned int height, unsigned int number_of_channels, unsigned int selected_channel, unsigned int number_of_threads, IntegralImage <T> &integral_image)
    ////// {
    //////     #pragma omp parallel num_threads(number_of_threads)
    //////     {
    //////         const R *ptr, *row_ptr;
    //////         unsigned int thread_id;
    //////         T *ii_ptr;
    //////         
    //////         ptr = source;
    //////         
    //////         thread_id = omp_get_thread_num();
    //////         for (unsigned int y = 0; y < height; ++y, ptr += row_bytes)
    //////         {
    //////             if (y % number_of_threads == thread_id)
    //////             {
    //////                 ii_ptr = integral_image(y);
    //////                 row_ptr = ptr;
    //////                 for (unsigned int x = 0; x < width; ++x, ++ii_ptr, row_ptr += number_of_channels)
    //////                     *ii_ptr = (T)row_ptr[selected_channel];
    //////             }
    //////         }
    //////     }
    ////// }
    
}

#endif

