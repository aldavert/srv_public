// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_IMAGE_GTK_FIGURE_HEADER_FILE__
#define __SRV_IMAGE_GTK_FIGURE_HEADER_FILE__

#include <gtk/gtk.h>
#include "srv_image.hpp"
#include "srv_image_draw.hpp"
#include "srv_image_definitions.hpp"

namespace srv
{
    class FigureMouseCallback
    {
    public:
        // -[ Constructor, destructor and assignation operator ]-------------------------------------------------------------------------------------
        /// Default constructor.
        FigureMouseCallback(void) : m_mouse_x(0), m_mouse_y(0), m_mouse_button(BUTTON_NONE), m_mouse_key_mask(MASK_NONE) {}
        
        // -[ Virtual functions called when a mouse event happened ]---------------------------------------------------------------------------------
        /// Virtual function which have to be overloaded when the mouse button press event has to be processed.
        virtual void processButtonPress(void) {}
        /// Virtual function which have to be overloaded when the mouse button release event has to be processed.
        virtual void processButtonRelease(void) {}
        /// Virtual function which have to be overloaded when the mouse motion event has to be processed.
        virtual void processMouseMove(void) {}
        
        // -[ Functions to access to mouse data ]----------------------------------------------------------------------------------------------------
        /// Returns the X-Coordinate of the mouse in the window.
        inline unsigned int getX(void) const { return m_mouse_x; }
        /// Returns the Y-Coordinate of the mouse in the window.
        inline unsigned int getY(void) const { return m_mouse_y; }
        /// Returns the active button of the mouse in the window.
        inline FIGURE_MOUSE_BUTTON getButton(void) const { return m_mouse_button; }
        /// Returns the key mask (e.g.\ control, shift and alt) active while the button is pressed.
        inline FIGURE_MOUSE_KEY getKeyMask(void) const { return m_mouse_key_mask; }
        /// Sets the X-Coordinate of the mouse in the window.
        inline void setX(unsigned int x) { m_mouse_x = x; }
        /// Sets the Y-Coordinate of the mouse in the window.
        inline void setY(unsigned int y) { m_mouse_y = y; }
        /// Sets the mouse button which is active in the window.
        inline void setButton(FIGURE_MOUSE_BUTTON button) { m_mouse_button = button; }
        /// Sets the key mask (e.g.\ control, shift and alt) active while the button is pressed.
        inline void setKeyMask(FIGURE_MOUSE_KEY mouse_key_mask) { m_mouse_key_mask = mouse_key_mask; }
    protected:
        /// X-Coordinate of the mouse in the window.
        unsigned int m_mouse_x;
        /// Y-Coordinate of the mouse in the window.
        unsigned int m_mouse_y;
        /// Button of the mouse which is active.
        FIGURE_MOUSE_BUTTON m_mouse_button;
        /// Mask key (i.e.\ shift, alt or control) which is pressed with the mouse button.
        FIGURE_MOUSE_KEY m_mouse_key_mask;
    };
    
    class FigureKeyboardCallback
    {
    public:
        /// Default constructor.
        FigureKeyboardCallback(void) : m_key(0), m_status(-1) {}
        /// Returns the last key pressed.
        inline int getKey(void) const { return m_key; }
        /// Returns the state of the key (special characters and keys).
        inline int getStatus(void) const { return m_status; }
        /// Sets the key and status of the keyboard.
        virtual void setData(int key, int status) { m_key = key; m_status = status; }
        /// Function which is called after the key has been pressed.
        virtual bool processKey(void) { return true; }
    protected:
        /// Value of the pressed key.
        int m_key;
        /// Status of the pressed key.
        int m_status;
    };
    
    class Figure
    {
    public:
        /// Default constructor.
        Figure(const char *window_name = "", ...);
        /// Destructor.
        ~Figure(void);
        
        /** Function which sets the image shown by the figure.
         *  \param[in] image image shown by the figure.
         */
        template <template <class> class IMAGE, class T> void setImage(const IMAGE<T> &image);
        /** Function operator which sets the image shown by the figure.
         *  \param[in] image image shown by the figure.
         */
        template <template <class> class IMAGE, class T> inline void operator()(const IMAGE<T> &image) { setImage(image); }
        /** Sets the figure mouse object which will be called when a mouse event is generated.
         *  \param[in] mouse_callback figure mouse object which will be called every time that a mouse event is generated. This object is not managed by the figure object so that it has to be managed manually.
         */
        void setMouseCallback(const FigureMouseCallback * mouse_callback);
        
        /** Static function which pauses the execution of the program the specified milliseconds or until a key is pressed.
         *  \param[in] time millisecons which the program is paused. When negative, the program is paused while a key is pressed.
         */
        static char wait(int time = -1);
        /// Adds a keyboard callback object to the figure which is called after the key event is captured by the figure.
        void setKeyboardCallback(const FigureKeyboardCallback * keyboard_callback);
        /// Changes the caption of the window.
        void setTitle(const char * window_name = "", ...);
    private:
        /// Static function which contains the flag used to known if the GTK interface has been already initialized.
        static bool& isInitialized(void) { static bool m_initialized = false; return m_initialized; }
        /// Static function which stores the ASCII code of the pressed key.
        static int& keyPress(void) { static int m_key = -1; return m_key; }
        /// Static function which stores the letter status for accents or diereses.
        static int& keyStatus(void) { static int m_status = -1; return m_status; }
        /// Static function which is called by the GTK interface when a key event is generated (i.e.\ when a key is pressed).
        static int key_press_event(GtkWidget * /* widget */, GdkEventKey *event, gpointer user_data);
        /// Static function which is called by the GTK interface when a mouse button press event is generated.
        static gboolean mouse_press_callback(GtkWidget *widget, GdkEventButton *event, gpointer user_data);
        /// Static function which is called by the GTK interface when a mouse button release event is generated.
        static gboolean mouse_release_callback(GtkWidget *widget, GdkEventButton *event, gpointer user_data);
        /// Static function which is called by the GTK interface when a mouse motion event is generated.
        static gboolean mouse_motion_callback(GtkWidget *widget, GdkEventMotion *event, gpointer user_data);
        /// Static function which is called by the GTK interface when a timer event is generated (events released by the wait function).
        static gboolean timer_stop(gpointer /* user_data */) { gtk_main_quit(); return FALSE; }
        /// GTK widget of the window.
        GtkWidget *m_window;
        /// GTK widget which shows the image in the window.
        GtkWidget *m_image;
        /// GTK widget to capture the mouse events on the image.
        GtkWidget *m_event_box;
        /// Inner pointer to the figure image.
        unsigned char *m_image_data;
        /// Identifier of the destroy window event.
        gulong m_destroy_event;
        /// Identifier of the key pressed event.
        gulong m_key_event;
        /// Identifier of the mouse button pressed event.
        gulong m_mouse_press_event;
        /// Identifier of the mouse button released event.
        gulong m_mouse_release_event;
        /// Identifier of the mouse motion event.
        gulong m_mouse_motion_event;
        /** Constant pointer to the mouse callback object that will be called when any mouse event is released.
         *  This object is not managed by the figure, so that, it has to be manually allocated and deallocated.
         */
        const FigureMouseCallback * m_mouse_callback;
    };
    
    template <template <class> class IMAGE, class T>
    void Figure::setImage(const IMAGE<T> &image)
    {
        gtk_widget_set_size_request(m_window, image.getWidth(), image.getHeight());
        
        if (m_image_data != 0) delete [] m_image_data;
        image.getInterleavedRGBImage(m_image_data);
        GdkPixbuf *gdk_buffer = gdk_pixbuf_new_from_data(m_image_data, GDK_COLORSPACE_RGB, false, 8, image.getWidth(), image.getHeight(), image.getWidth() * 3, 0, 0);
        
        gtk_image_set_from_pixbuf(GTK_IMAGE(m_image), gdk_buffer);
    }
    
}


#endif

