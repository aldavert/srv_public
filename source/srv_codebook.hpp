// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEBOOK_MAIN_HPP_HEADER_FILE__
#define __SRV_CODEBOOK_MAIN_HPP_HEADER_FILE__

// -[ Include the headers of the codebooks ]---------------------------------------------------------------------------------------------------------
#include "codebook/srv_codebook_base.hpp"
#include "codebook/srv_codebook_exhaustive.hpp"
#include "codebook/srv_codebook_hierarchical_cluster.hpp"
#include "codebook/srv_codebook_random_forest.hpp"
// -[ Other needed headers ]-------------------------------------------------------------------------------------------------------------------------
#include "srv_utilities.hpp"
#include "srv_xml.hpp"
#include "srv_file_compression.hpp"
#include <iostream>

//                   +--------------------------------------+
//                   | CODEBOOK MACROS                      |
//                   +--------------------------------------+
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                ##############
//                                  ##########
//                                    ######
//                                      ##

#define ADD_CODEWORD_WEIGHT_FACTORY(CLASS_NAME, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_FACTORY(TEMPLATE_4P(CodewordWeightFactory, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR), TEMPLATE_4P(CLASS_NAME, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR))

#define ADD_CODEWORD_ENCODE_FACTORY(CLASS_NAME, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_FACTORY(TEMPLATE_4P(CodewordEncodeFactory, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR), TEMPLATE_4P(CLASS_NAME, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR))

#define INITIALIZE_CODEWORD_WEIGHT_FACTORIES(TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightUniform, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightDistance, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightLLC, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightGaussian, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightNormalizedGaussian, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_WEIGHT_FACTORY(srv::CodewordWeightFisher, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR)

#define INITIALIZE_CODEWORD_ENCODE_FACTORIES(TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_ENCODE_FACTORY(srv::CodewordEncodeCentroid, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_ENCODE_FACTORY(srv::CodewordEncodeFirstDerivative, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_ENCODE_FACTORY(srv::CodewordEncodeTensor, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_ENCODE_FACTORY(srv::CodewordEncodeSuperVector, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR) \
    ADD_CODEWORD_ENCODE_FACTORY(srv::CodewordEncodeFisher, TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR)

#define ADD_CODEBOOK_FACTORY(CLASS_NAME, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    ADD_FACTORY(TEMPLATE_3P(CodebookFactory, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR), TEMPLATE_3P(CLASS_NAME, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR))

#define ADD_CODEBOOK_FACTORY_DISTANCE(CLASS_NAME, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    ADD_FACTORY(TEMPLATE_3P(CodebookFactory, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR), TEMPLATE_4P(CLASS_NAME, TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR))

//    INITIALIZE_CLUSTER_FACTORIES(TDESCRIPTOR, srv::MODE_TYPE<TDESCRIPTOR>::TYPE, TCODEWORD, NDESCRIPTOR)
#define INITIALIZE_CODEBOOK_FACTORIES(TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    INITIALIZE_CLUSTER_FACTORIES(TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR) \
    INITIALIZE_CODEWORD_WEIGHT_FACTORIES(TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR) \
    INITIALIZE_CODEWORD_ENCODE_FACTORIES(TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR) \
    ADD_CODEBOOK_FACTORY_DISTANCE(srv::CodebookExhaustive, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    ADD_CODEBOOK_FACTORY_DISTANCE(srv::CodebookHierarchicalCluster, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR) \
    ADD_CODEBOOK_FACTORY(srv::CodebookRandomForest, TDESCRIPTOR, TCODEWORD, NDESCRIPTOR)

//                                      ##
//                                    ######
//                                  ##########
//                                ##############
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                   +--------------------------------------+
//                   | CODEBOOK AUXILIARY FUNCTIONS         |
//                   +--------------------------------------+
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                ##############
//                                  ##########
//                                    ######
//                                      ##

namespace srv
{
    
    /** This function loads a codebook stored as an XML object from a disk file.
     *  \param[out] codebook reference to the pointer containing the resulting codebook.
     *  \param[in] filename file containing the codebook as an XML object.
     *  \note this function requires that the factories have been properly initialized.
     */
    template <class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    void loadObject(const char * filename, CodebookBase<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> * &codebook)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        if (parser.isTagIdentifier("Codebook"))
        {
            codebook = CodebookFactory<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR>::Type::getInstantiator((CODEBOOK_IDENTIFIER)((int)parser.getAttribute("Identifier")));
            codebook->convertFromXML(parser);
        }
        else throw srv::Exception("XML file '%s' does not contain a codebook object.", filename);
        
        delete file;
    }
    
    /** This function saves the codebook as an XML object to the specified file.
     *  \param[in] codebook codebook object.
     *  \param[in] filename path to the file where the codebook is going to be stored.
     */
    template <class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    void saveObject(const char * filename, const CodebookBase<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> * codebook)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        codebook->convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    std::ostream& operator<<(std::ostream &out, const CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * object)
    {
        CharacterTab tab(' ', 4);
        out << tab << "[ Codeword Weight ]" << std::endl;
        tab.increase(out);
        out << tab << " · Codeword weight method: ";
        switch (object->getIdentifier())
        {
        case CODEWORD_WEIGHT_UNIFORM:
            out << "Uniform weight";
            break;
        case CODEWORD_WEIGHT_DISTANCE:
            out << "Distance weight";
            break;
        case CODEWORD_WEIGHT_LINEARLY_CONSTRAINED_LOCAL_CODING:
            out << "Linearly-constrained Local Coding (LLC) weight";
            break;
        case CODEWORD_WEIGHT_GAUSSIAN_KERNEL:
            out << "Gaussian kernel weight";
            break;
        case CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL:
            out << "Normalized Gaussian kernel weight";
            break;
        case CODEWORD_WEIGHT_FISHER_VECTOR:
            out << "Fisher vector weight";
            break;
        default:
            out << "UNKNOWN";
            break;
        }
        out << std::endl;
        if (object->getIdentifier() == CODEWORD_WEIGHT_GAUSSIAN_KERNEL)
        {
            const CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * ptr = (CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)object;
            out << tab << " · Sigma of the Gaussian kernel: " << ptr->getSigma() << std::endl;
        }
        else if (object->getIdentifier() == CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL)
        {
            const CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * ptr = (CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)object;
            out << tab << " · Sigma of the Gaussian kernel: " << ptr->getSigma() << std::endl;
        }
        tab.decrease(out);
        return out;
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    std::ostream& operator<<(std::ostream &out, const CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * object)
    {
        CharacterTab tab(' ', 4);
        out << tab << "[ Codeword Encode ]" << std::endl;
        tab.increase(out);
        out << tab << " · Codeword encode method: ";
        switch (object->getIdentifier())
        {
        case CODEWORD_ENCODE_CENTROID:
            out << "Centroid index";
            break;
        case CODEWORD_ENCODE_FIRST_DERIVATIVE:
            out << "First derivative";
            break;
        case CODEWORD_ENCODE_SUPER_VECTOR:
            out << "Super-vector";
            break;
        case CODEWORD_ENCODE_FISHER_VECTOR:
            out << "Fisher vector";
            break;
        case CODEWORD_ENCODE_TENSOR:
            out << "Tensor";
            break;
        default:
            out << "UNKNOWN";
            break;
        }
        out << std::endl;
        if (object->getIdentifier() == CODEWORD_ENCODE_SUPER_VECTOR)
        {
            const CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * ptr = (CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)object;
            out << tab << " · Weight of the centroid index bins: " << ptr->getWeight() << std::endl;;
        }
        tab.decrease(out);
        return out;
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class NDESCRIPTOR>
    std::ostream& operator<<(std::ostream &out, const CodebookBase<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> * object)
    {
        const CODEBOOK_IDENTIFIER codebook_identifier = object->getIdentifier();
        CharacterTab tab(' ', 4);
        
        out << tab << "[ Codebook ]" << std::endl;
        tab.increase(out);
        
        out << tab << " · Codebook: ";
        if      (codebook_identifier == CODEBOOK_EXHAUSTIVE_ID          ) out << "Exhaustive";
        else if (codebook_identifier == CODEBOOK_HIERARCHICAL_CLUSTER_ID) out << "Hierarchical cluster";
        else if (codebook_identifier == CODEBOOK_RANDOM_FOREST_ID       ) out << "Random forest";
        else out << "UNKNOWN";
        out << " codebook." << std::endl;
        
        out << tab << " · Number of dimensions: " << object->getNumberOfDimensions() << std::endl;
        out << tab << " · Number of codewords: " << object->getNumberOfCodewords() << std::endl;
        out << tab << " · Number of bins per codeword: " << object->getNumberOfBinsPerCodeword() << std::endl;
        out << tab << " · Number of bins per descriptor: " << object->getNumberOfBinsPerDescriptor() << std::endl;
        out << tab << " · Number of bins needed to represent the whole codebook: " << object->getNumberOfCodebookFeatures() << std::endl;
        
        if (codebook_identifier == CODEBOOK_EXHAUSTIVE_ID)
        {
            const CodebookExhaustive<TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR> * object_ptr = (CodebookExhaustive<TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR> *)object;
            
            out << object_ptr->getCodewordWeight();
            out << object_ptr->getCodewordEncode();
            out << tab << " · Number of neighbors: " << object_ptr->getNumberOfNeighbors() << std::endl;
            out << object_ptr->getCluster();
        }
        else if (codebook_identifier == CODEBOOK_HIERARCHICAL_CLUSTER_ID)
        {
            const CodebookHierarchicalCluster<TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR> * object_ptr = (CodebookHierarchicalCluster<TDESCRIPTOR, TCODEWORD, TCODEWORD, NDESCRIPTOR> *)object;
            
            out << tab << " · Degree of the tree: " << object_ptr->getDegree() << std::endl;
            out << tab << " · Maximum number of codewords: ";
            if (object_ptr->getMaximumNumberOfCodewords() == 0) out << "[DISABLED]" << std::endl;
            else out << object_ptr->getMaximumNumberOfCodewords() << std::endl;
            out << tab << " · Minimum support: " << object_ptr->getMinimumSupport() << std::endl;
            out << tab << " · Maximum height: " << object_ptr->getMaximumHeight() << std::endl;
            out << tab << " · Number of nodes of the tree: " << object_ptr->getNumberOfNodes() << std::endl;
            out << object_ptr->getCluster();
        }
        else if (codebook_identifier == CODEBOOK_RANDOM_FOREST_ID)
        {
            const CodebookRandomForest<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> * object_ptr = (CodebookRandomForest<TDESCRIPTOR, TCODEWORD, NDESCRIPTOR> *)object;
            
            out << tab << " · Minimum score: " << object_ptr->getMinimumScore() << std::endl;
            out << tab << " · Number of trials: " << object_ptr->getNumberOfTrials() << std::endl;
            out << tab << " · Maximum height: " << object_ptr->getMaximumHeight() << std::endl;
            out << tab << " · Number of trees: " << object_ptr->getNumberOfTrees() << std::endl;
        }
        tab.decrease(out);
        
        return out;
    }
    
}

//                                      ##
//                                    ######
//                                  ##########
//                                ##############
//                                    ######
//                                    ######
//                                    ######
//                                    ######
//                                    ######

// --------------------------------------------------------------------------------------------------------------------------------------------------

#endif
