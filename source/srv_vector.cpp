// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "srv_vector.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | VECTOR DISTANCE PARAMETERS CLASS     |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ParametersVectorDistance::ParametersVectorDistance(void) :
        m_method(1),
        m_method_update_parameter(false),
        m_minkowski_order(2.0),
        m_minkowski_order_update_parameter(false)
    {
    }
    
    ParametersVectorDistance::ParametersVectorDistance(unsigned int method, double minkowski_order) :
        m_method(method),
        m_method_update_parameter(false),
        m_minkowski_order(minkowski_order),
        m_minkowski_order_update_parameter(false)
    {
        if (method > 11)
            throw Exception("Unknown vector distance index '%d'.", method);
    }
    
    DISTANCE_IDENTIFIER ParametersVectorDistance::getMethodIdentifier(void) const
    {
        const DISTANCE_IDENTIFIER distance_identifiers[] = { /*00*/ MINKOWSKI_DISTANCE,
                                                             /*01*/ EUCLIDEAN_DISTANCE,
                                                             /*02*/ MANHATTAN_DISTANCE,
                                                             /*03*/ CHEBYSHEV_DISTANCE,
                                                             /*04*/ COSINE_DISTANCE,
                                                             /*05*/ GEODESIC_DISTANCE,
                                                             /*06*/ CANBERRA_DISTANCE,
                                                             /*07*/ INTERSECTION_DISTANCE,
                                                             /*08*/ CHISQUARE_DISTANCE,
                                                             /*09*/ HELLINGER_DISTANCE,
                                                             /*10*/ BHATTACHARYYA_DISTANCE,
                                                             /*11*/ MAHALANOBIS_DISTANCE };
        return distance_identifiers[m_method];
    }
    
    void ParametersVectorDistance::setMethodIdentifier(DISTANCE_IDENTIFIER identifier)
    {
        if (identifier == MINKOWSKI_DISTANCE) m_method = 0;
        else if (identifier == EUCLIDEAN_DISTANCE) m_method = 1;
        else if (identifier == MANHATTAN_DISTANCE) m_method = 2;
        else if (identifier == CHEBYSHEV_DISTANCE) m_method = 3;
        else if (identifier == COSINE_DISTANCE) m_method = 4;
        else if (identifier == GEODESIC_DISTANCE) m_method = 5;
        else if (identifier == CANBERRA_DISTANCE) m_method = 6;
        else if (identifier == INTERSECTION_DISTANCE) m_method = 7;
        else if (identifier == CHISQUARE_DISTANCE) m_method = 8;
        else if (identifier == HELLINGER_DISTANCE) m_method = 9;
        else if (identifier == BHATTACHARYYA_DISTANCE) m_method = 10;
        else if (identifier == MAHALANOBIS_DISTANCE) m_method = 11;
        else throw Exception("Index '%d' has not been assigned to any known distance.", m_method);
    }
    
    void ParametersVectorDistance::update(const ParametersVectorDistance &other)
    {
        if (other.m_method_update_parameter && (m_method != other.m_method)) { m_method = other.m_method; m_method_update_parameter = true; }
        if (other.m_minkowski_order_update_parameter && (m_minkowski_order != other.m_minkowski_order)) { m_minkowski_order = other.m_minkowski_order; m_minkowski_order_update_parameter = true; }
    }
    
    void ParametersVectorDistance::setParameters(ParameterParser &parameters, const char * suffix)
    {
        char attribute_text[128];
        
        if (suffix[0] != '\0') sprintf(attribute_text, "--%s-distance", suffix);
        else sprintf(attribute_text, "--distance");
        parameters.addParameter(new ParameterUInt(attribute_text, "identifier", "index of the distance measure. Possible values are:"
                                                                                "&0&Minkowski distance."
                                                                                "&1&Euclidean distance."
                                                                                "&2&Manhattan distance."
                                                                                "&3&Chebyshev distance."
                                                                                "&4&Cosine distance."
                                                                                "&5&Geodesic distance."
                                                                                "&6&Canberra distance."
                                                                                "&7&Intersection distance."
                                                                                "&8&Chi-square distance."
                                                                                "&9&Hellinger distance."
                                                                                "&10&Bhattacharyya distance."
                                                                                "&11&Mahalanobis distance.", 1, true, 0, true, 11, &m_method, &m_method_update_parameter));
        
        if (suffix[0] != '\0') sprintf(attribute_text, "--%s-distance-minkowski", suffix);
        else sprintf(attribute_text, "--distance-minkowski");
        parameters.addParameter(new ParameterDouble(attribute_text, "order", "order of the Minkowski distance.", 2.0, true, 0.0, false, 0.0, &m_minkowski_order, &m_minkowski_order_update_parameter));
    }
    
    void ParametersVectorDistance::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Parameters_Vector_Distance");
        parser.setAttribute("Method", m_method);
        parser.setAttribute("Minkowski_Order", m_minkowski_order);
        parser.closeTag();
    }
    
    void ParametersVectorDistance::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Parameters_Vector_Distance"))
        {
            m_method = parser.getAttribute("Method");
            m_minkowski_order = parser.getAttribute("Minkowski_Order");
            if (m_method > 11)
                throw Exception("Unknown vector distance index '%d'.", m_method);
            m_method_update_parameter = false;
            m_minkowski_order_update_parameter = false;
            
            while (!(parser.isTagIdentifier("Parameters_Vector_Distance") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    std::ostream& operator<<(std::ostream &out, const ParametersVectorDistance &parameters)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << "[ DISTANCE ]" << std::endl;
        tab.increase(out);
        out << tab << ((parameters.m_method_update_parameter)?("+"):("·")) << " Distance index: " << parameters.m_method << " ";
        if (parameters.m_method == 0) out << "(Minkowski distance)";
        else if (parameters.m_method == 1) out << "(Euclidean distance)";
        else if (parameters.m_method == 2) out << "(Manhattan distance)";
        else if (parameters.m_method == 3) out << "(Chebyshev distance)";
        else if (parameters.m_method == 4) out << "(Cosine distance)";
        else if (parameters.m_method == 5) out << "(Geodesic distance)";
        else if (parameters.m_method == 6) out << "(Canberra distance)";
        else if (parameters.m_method == 7) out << "(Intersection distance)";
        else if (parameters.m_method == 8) out << "(Chi-square distance)";
        else if (parameters.m_method == 9) out << "(Hellinger distance)";
        else if (parameters.m_method == 10) out << "(Bhattacharyya distance)";
        else if (parameters.m_method == 11) out << "(Mahalanobis distance)";
        else out << "(Unknown distance)";
        out << std::endl;
        
        if (parameters.m_method == 0)
            out << tab << ((parameters.m_minkowski_order_update_parameter)?("+"):("·")) << " Minkowski order: " << parameters.m_minkowski_order << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY VECTOR DISTANCE FUNCTIONS  |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void create(const ParametersVectorDistance &parameters, VectorDistance &object)
    {
        object.set(parameters.getMethodIdentifier(), parameters.getMinkowskiOrder());
    }
    
    std::ostream& operator<<(std::ostream &out, const VectorDistance &object)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << " [ Vector Distance ]" << std::endl;
        tab.increase(out);
        out << tab << " · Type: ";
        if (object.getIdentifier() == MINKOWSKI_DISTANCE) out << "Minkowski";
        else if (object.getIdentifier() == EUCLIDEAN_DISTANCE) out << "Euclidean";
        else if (object.getIdentifier() == MANHATTAN_DISTANCE) out << "Manhattan";
        else if (object.getIdentifier() == CHEBYSHEV_DISTANCE) out << "Chebyshev";
        else if (object.getIdentifier() == COSINE_DISTANCE) out << "Cosine";
        else if (object.getIdentifier() == GEODESIC_DISTANCE) out << "Geodesic";
        else if (object.getIdentifier() == CANBERRA_DISTANCE) out << "Canberra";
        else if (object.getIdentifier() == INTERSECTION_DISTANCE) out << "Intersection";
        else if (object.getIdentifier() == CHISQUARE_DISTANCE) out << "Chi-square";
        else if (object.getIdentifier() == HELLINGER_DISTANCE) out << "Hellinger";
        else if (object.getIdentifier() == BHATTACHARYYA_DISTANCE) out << "Bhattacharyya";
        else if (object.getIdentifier() == MAHALANOBIS_DISTANCE) out << "Mahalanobis";
        out << " distance." << std::endl;
        if (object.getIdentifier() == MINKOWSKI_DISTANCE) 
            out << tab << " · Minkowski distance order: " << object.getOrder() << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    void loadObject(const char * filename, VectorDistance &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object.convertFromXML(parser);
        delete file;
    }
    
    void saveObject(const char * filename, const VectorDistance &object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object.convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | VECTOR NORM PARAMETERS CLASS         |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ParametersVectorNorm::ParametersVectorNorm(void) :
        m_method(0),
        m_method_update_parameter(false),
        m_order(2.0),
        m_order_update_parameter(false)
    {
    }
    
    ParametersVectorNorm::ParametersVectorNorm(unsigned int method, double order) :
        m_method(method),
        m_method_update_parameter(false),
        m_order(order),
        m_order_update_parameter(false)
    {
        if (method > 3)
            throw Exception("Unknown vector norm index '%d'.", method);
    }
    
    NORMAL_IDENTIFIER ParametersVectorNorm::getMethodIdentifier(void) const
    {
        const NORMAL_IDENTIFIER normal_identifier[] = { MANHATTAN_NORM, EUCLIDEAN_NORM, MAXIMUM_NORM, P_NORM };
        
        return normal_identifier[m_method];
    }
    
    void ParametersVectorNorm::setMethodIdentifier(NORMAL_IDENTIFIER identifier)
    {
        if (identifier == MANHATTAN_NORM) m_method = 0;
        else if (identifier == EUCLIDEAN_NORM) m_method = 1;
        else if (identifier == MAXIMUM_NORM) m_method = 2;
        else if (identifier == P_NORM) m_method = 3;
        else throw Exception("Index '%d' has not been assigned to any known normal.", m_method);
    }
    
    void ParametersVectorNorm::update(const ParametersVectorNorm &other)
    {
        if (other.m_method_update_parameter && (m_method != other.m_method)) { m_method = other.m_method; m_method_update_parameter = true; }
        if (other.m_order_update_parameter && (m_order != other.m_order)) { m_order = other.m_order; m_order_update_parameter = true; }
    }
    
    void ParametersVectorNorm::setParameters(ParameterParser &parameters, const char * suffix)
    {
        char attribute_text[128];
        
        if (suffix[0] != '\0') sprintf(attribute_text, "--%s-normal", suffix);
        else sprintf(attribute_text, "--normal");
        parameters.addParameter(new ParameterUInt(attribute_text, "identifier", "index of the distance measure. Possible values are:"
                                                                                "&0&. Manhattan norm."
                                                                                "&1&. Euclidean norm."
                                                                                "&2&. Maximum norm."
                                                                                "&3&. Lp norm", 1, true, 0, true, 3, &m_method, &m_method_update_parameter));
        
        if (suffix[0] != '\0') sprintf(attribute_text, "--%s-normal-order", suffix);
        else sprintf(attribute_text, "--normal-order");
        parameters.addParameter(new ParameterDouble(attribute_text, "order", "order of the Lp-norm.", 2.0, true, 0.0, false, 0.0, &m_order, &m_order_update_parameter));
    }
    
    void ParametersVectorNorm::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Parameters_Vector_Norm");
        parser.setAttribute("Method", m_method);
        parser.setAttribute("Order", m_order);
        parser.closeTag();
    }
    
    void ParametersVectorNorm::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Parameters_Vector_Norm"))
        {
            m_method = parser.getAttribute("Method");
            m_order = parser.getAttribute("Order");
            if (m_method > 3)
                throw Exception("Unknown vector norm index '%d'.", m_method);
            
            m_method_update_parameter = false;
            m_order_update_parameter = false;
            
            while (!(parser.isTagIdentifier("Parameters_Vector_Norm") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    std::ostream& operator<<(std::ostream &out, const ParametersVectorNorm &parameters)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << "[ NORMALIZATION ]" << std::endl;
        tab.increase(out);
        out << tab << ((parameters.m_method_update_parameter)?("+"):("·")) << " Norm index: " << parameters.m_method << " ";
        if (parameters.m_method == 0) out << "(Manhattan norm)";
        else if (parameters.m_method == 1) out << "(Euclidean norm)";
        else if (parameters.m_method == 2) out << "(Maximum norm)";
        else if (parameters.m_method == 3) out << "(Lp-norm)";
        else out << "(Unknown norm)";
        out << std::endl;
        
        if (parameters.m_method == 3)
            out << tab << ((parameters.m_order_update_parameter)?("+"):("·")) << " Lp-norm order: " << parameters.m_order << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY VECTOR NORM FUNCTIONS      |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void create(const ParametersVectorNorm &parameters, VectorNorm &object)
    {
        object.set(parameters.getMethodIdentifier(), parameters.getOrder());
    }
    
    std::ostream& operator<<(std::ostream &out, const VectorNorm &object)
    {
        CharacterTab tab(' ', 4);
        
        out << tab << " [ Vector Norm ]" << std::endl;
        tab.increase(out);
        out << tab << " · Type: ";
        if (object.getIdentifier() == MANHATTAN_NORM) out << "Manhattan norm";
        else if (object.getIdentifier() == EUCLIDEAN_NORM) out << "Euclidean norm";
        else if (object.getIdentifier() == MAXIMUM_NORM) out << "Maximum norm";
        else if (object.getIdentifier() == P_NORM) out << "Lp-norm";
        out << std::endl;
        if (object.getIdentifier() == P_NORM) 
            out << tab << " · Lp-norm order: " << object.getOrder() << std::endl;
        tab.decrease(out);
        
        return out;
    }
    
    void loadObject(const char * filename, VectorNorm &object)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be loaded.", filename);
        XmlParser parser(file);
        object.convertFromXML(parser);
        delete file;
    }
    
    void saveObject(const char * filename, const VectorNorm &object)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw Exception("File '%s' cannot be created.", filename);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while creating '%s'.", filename);
        XmlParser parser(file);
        object.convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

