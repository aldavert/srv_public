// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEBOOK_DEFINITIONS_HPP_HEADER_FILE__
#define __SRV_CODEBOOK_DEFINITIONS_HPP_HEADER_FILE__

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | ENUMERATES AND META-PROGRAMMING      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <typename T> struct META_FACTOR { enum { FACTOR = 1000 }; };
    template <> struct META_FACTOR<char> { enum { FACTOR = 100 }; };
    template <> struct META_FACTOR<unsigned char> { enum { FACTOR = 100 }; };
    template <> struct META_FACTOR<double> { enum { FACTOR = 1 }; };
    template <> struct META_FACTOR<float> { enum { FACTOR = 1 }; };
    
    /// Enumerate with the identifiers of different codebooks.
    enum CODEBOOK_IDENTIFIER { CODEBOOK_EXHAUSTIVE_ID = 3001,                   ///< Exhaustive codebook.
                               CODEBOOK_HIERARCHICAL_CLUSTER_ID,                ///< Hierarchical clustering codebook (i.e. Vocabulary Tree codebook).
                               CODEBOOK_RANDOM_FOREST_ID                        ///< Random forest codebook (i.e. Extremely Randomized Forest codebook).
    };
    /// Enumerate with the identifiers of the different methods used to weight the contribution of the codewords.
    enum CODEWORD_WEIGHT_IDENTIFIER { CODEWORD_WEIGHT_UNIFORM = 3101,                      ///< The codebook uses the same weight for all codewords regardless of their distance to the descriptor.
                                      CODEWORD_WEIGHT_DISTANCE,                            ///< The codebook uses the distance between codewords and the local descriptor to assign its weight.
                                      CODEWORD_WEIGHT_LINEARLY_CONSTRAINED_LOCAL_CODING,   ///< The codebook uses the linearly-constrained local coding to assign the weight of each codeword.
                                      CODEWORD_WEIGHT_GAUSSIAN_KERNEL,                     ///< The codebook uses a Gaussian function to weight the contribution of each codeword.
                                      CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL,          ///< The codebook uses a Gaussian probability function (i.e. L1-normalized) to weight the contribution of each codeword.
                                      CODEWORD_WEIGHT_FISHER_VECTOR                        ///< The codebook uses a Gaussian probability function defined as in Fisher codes to weight the contribution of each codeword.
    };
    /// Enumerate with the identifiers of the different methods used to represent the visual words.
    enum CODEWORD_ENCODE_IDENTIFIER { CODEWORD_ENCODE_CENTROID = 3201,     ///< Descriptors are represented by the index of the selected codewords.
                                      CODEWORD_ENCODE_FIRST_DERIVATIVE,    ///< Descriptors are represented by the difference between the selected codeword and the descriptor.
                                      CODEWORD_ENCODE_SUPER_VECTOR,        ///< Descriptors are represented by the index of the centroid and the first derivative (i.e. the difference) between the codeword and the descriptor.
                                      CODEWORD_ENCODE_FISHER_VECTOR,       ///< Descriptors are represented by the first and the second derivatives in the descriptor space.
                                      CODEWORD_ENCODE_TENSOR               ///< Descriptors are represented by the tensor of the descriptor, i.e. the upper diagonal matrix corresponding to the covariance matrix of the difference vector between the selected codeword and the descriptor.
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

