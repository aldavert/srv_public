// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEBOOK_BASE_HPP_HEADER_FILE__
#define __SRV_CODEBOOK_BASE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../srv_utilities.hpp"
#include "../srv_xml.hpp"
#include "../srv_vector.hpp"
#include "../srv_logger.hpp"
// -[ Codebook auxiliary classes ]-------------------------------------
#include "srv_codebook_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CODEBOOK BASE                        |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Pure virtual class which defines the base structure of the codebooks.
     *  \tparam T type of the descriptors data.
     *  \tparam TDISTANCE type of the distance used to calculate the distance between descriptor and codewords.
     *  \tparam N type of the descriptors size.
     */
    template <class T, class TCODEWORD, class N>
    class CodebookBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Destructor.
        virtual ~CodebookBase(void);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the number of dimensions of the descriptors encoded by the codebook.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns the number of codewords of the codebook.
        inline unsigned int getNumberOfCodewords(void) const { return m_number_of_codewords; }
        /// Returns the number of bins needed to represent a codeword.
        inline unsigned int getNumberOfBinsPerCodeword(void) const { return m_number_of_bins_per_codeword; }
        /// Returns the number of bins needed to encode a single descriptor.
        inline unsigned int getNumberOfBinsPerDescriptor(void) const { return m_number_of_bins_per_descriptor; }
        /// Returns the number of features needed to represent the whole set of codewords of the codebook.
        inline unsigned int getNumberOfCodebookFeatures(void) const { return m_number_of_codebook_features; }
        
        // -[ Pure virtual functions defined in the base codebook ]----------------------------------------------------------------------------------
        /** Pure virtual function which encodes a set of descriptor into codebook words.
         *  \param[in] descriptors array of pointers to constant dense sub-vectors of the descriptors to encode.
         *  \param[out] codes array of sparse vectors with the codewords and weights assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and code arrays.
         *  \param[in] number_of_threads number of threads used to concurrently encode the descriptors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void encode(ConstantSubVectorDense<T, N> const * const * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Pure virtual function which encodes a set of descriptor into codebook words.
         *  \param[in] descriptors array of pointers to constant sparse sub-vectors of the descriptors to encode.
         *  \param[out] codes array of sparse vectors with the codewords and weights assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and code arrays.
         *  \param[in] number_of_threads number of threads used to concurrently encode the descriptors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void encode(ConstantSubVectorSparse<T, N> const * const * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) const = 0;
        /** Pure virtual function which generates the codebook with the descriptor given as examples.
         *  \param[in] descriptors an array of pointers to the constant dense sub-vectors used to generate the codebook.
         *  \param[in] labels an array with the category label assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and labels arrays.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors to generate the codebook.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void generate(ConstantSubVectorDense<T, N> const * const * descriptors, const int *labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
            /** Pure virtual function which generates the codebook with the descriptor given as examples.
         *  \param[in] descriptors an array of pointers to the constant sparse sub-vectors used to generate the codebook.
         *  \param[in] labels an array with the category label assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and labels arrays.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors to generate the codebook.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        virtual void generate(ConstantSubVectorSparse<T, N> const * const * descriptors, const int *labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) = 0;
        
        // -[ Implementation of derived functions for the pure virtual functions ]-------------------------------------------------------------------
        /** Function which encodes a set of descriptor into codebook words.
         *  \param[in] descriptors array of pointers to the vectors with the descriptors to encode.
         *  \param[out] codes array of sparse vectors with the codewords and weights assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and code arrays.
         *  \param[in] number_of_threads number of threads used to concurrently encode the descriptors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void encode(VECTOR<T, N> const * const * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Function which encodes a set of descriptor into codebook words.
         *  \param[in] descriptors array of vectors with the descriptors to encode.
         *  \param[out] codes array of sparse vectors with the codewords and weights assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and code arrays.
         *  \param[in] number_of_threads number of threads used to concurrently encode the descriptors.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void encode(const VECTOR<T, N> * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Function which encodes a descriptor into codebook words.
         *  \param[in] descriptors vector with the descriptor.
         *  \param[out] codes codewords and weights assigned to the descriptor.
         */
        template <template <class, class> class VECTOR>
        void encode(const VECTOR<T, N> &descriptor, VectorSparse<TCODEWORD, unsigned int> &codes) const;
        /** Function which generates the codebook with the descriptor given as examples.
         *  \param[in] descriptors an array of pointers to the vectors used to generate the codebook.
         *  \param[in] labels an array with the category label assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and labels arrays.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors to generate the codebook.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void generate(VECTOR<T, N> const * const * descriptors, const int * labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0);
        /** Function which generates the codebook with the descriptor given as examples.
         *  \param[in] descriptors an array of vectors used to generate the codebook.
         *  \param[in] labels an array with the category label assigned to each descriptor.
         *  \param[in] number_of_descriptors number of elements in both descriptors and labels arrays.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors to generate the codebook.
         *  \param[out] logger pointer to the logger used to show processing information (set to 0 to disable the log information).
         */
        template <template <class, class> class VECTOR>
        void generate(const VECTOR<T, N> * descriptors, const int * labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the codebook information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the codebook information from an XML object.
        void convertFromXML(XmlParser &parser);
        
        // -[ Codebook factory functions ]-----------------------------------------------------------------------------------------------------------
        /// Duplicates the codebook (virtual copy constructor).
        virtual CodebookBase<T, TCODEWORD, N>* duplicate(void) const = 0;
        ///// /// Returns the class identifier for the current codebook method.
        ///// inline static CODEBOOK_IDENTIFIER getClassIdentifier(void) { return ; }
        ///// /// Generates a new empty instance of the codebook class.
        ///// inline static CodebookBase<T, TCODEWORD, N>* generateObject(void) { return (CodebookBase<T, TCODEWORD, N>*)new Codebook(); }
        ///// /// Returns a flag which states if the codebook class has been initialized in the factory.
        ///// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codebook type identifier of the current object.
        virtual CODEBOOK_IDENTIFIER getIdentifier(void) const = 0;
    protected:
        // -[ Protected constructors ]---------------------------------------------------------------------------------------------------------------
        /// Default constructor.
        CodebookBase(void);
        /** Constructor operator which initializes the parameters of the base codebook.
         *  \param[in] number_of_dimensions number of dimensions of the descriptors encoded by the codebook.
         *  \param[in] number_of_codewords number of codewords of the codebook.
         *  \param[in] number_of_bins_per_codeword number of bins needed to represent a codeword.
         *  \param[in] number_of_bins_per_descriptor number of bins needed to represent an encoded descriptor.
         */
        CodebookBase(unsigned int number_of_dimensions, unsigned int number_of_codewords, unsigned int number_of_bins_per_codeword, unsigned int number_of_bins_per_descriptor);
        /// Copy constructor.
        CodebookBase(const CodebookBase<T, TCODEWORD, N> &other);
        /// Assignation operator.
        CodebookBase<T, TCODEWORD, N>& operator=(const CodebookBase<T, TCODEWORD, N> &other);
        
        // -[ Protected functions ]------------------------------------------------------------------------------------------------------------------
        /** This function initializes the information parameters of the base codebook.
         *  \param[in] number_of_dimensions number of dimensions of the descriptors encoded by the codebook.
         *  \param[in] number_of_codewords number of codewords of the codebook.
         *  \param[in] number_of_bins_per_codeword number of bins needed to represent a codeword.
         *  \param[in] number_of_bins_per_descriptor number of bins needed to represent an encoded descriptor.
         */
        void set(unsigned int number_of_dimensions, unsigned int number_of_codewords, unsigned int number_of_bins_per_codeword, unsigned int number_of_bins_per_descriptor);
        
        // -[ Virtual XML functions ]----------------------------------------------------------------------------------------------------------------
        /// Virtual function which stores the attributes of the codebook into the XML object.
        virtual void convertAttributesToXML(XmlParser &parser) const = 0;
        /// Virtual function which stores the data information of the codebook into the XML object.
        virtual void convertDataToXML(XmlParser &parser) const = 0;
        /// Virtual function which initializes the codebook to load the codebook information from an XML object.
        virtual void initializeFromXML(XmlParser &parser) = 0;
        /// Virtual function which retrieves the attributes of the codebook from an XML object.
        virtual void convertAttributesFromXML(XmlParser &parser) = 0;
        /// Virtual function which retrieves the data information of the codebook from an XML object.
        virtual bool convertDataFromXML(XmlParser &parser) = 0;
        /// Virtual function which is called once the data and attributes has been retrieved from an XML object.
        virtual void processFromXML(XmlParser &parser) = 0;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Number of dimensions of the descriptors encoded by the codebook.
        unsigned int m_number_of_dimensions;
        /// Number of codewords of the codebook.
        unsigned int m_number_of_codewords;
        /// Number of bins needed to represent a codeword.
        unsigned int m_number_of_bins_per_codeword;
        /// Number of bins needed to encode a single descriptor.
        unsigned int m_number_of_bins_per_descriptor;
        /// Number of features needed to represent the whole set of codewords of the codebook.
        unsigned int m_number_of_codebook_features;
        
        //// /// Static integer which indicates if the class has been initialized in the codebook base factory.
        //// static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEBOOK BASE                        |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TCODEWORD, class N>
    CodebookBase<T, TCODEWORD, N>::CodebookBase(void) :
        m_number_of_dimensions(0),
        m_number_of_codewords(0),
        m_number_of_bins_per_codeword(0),
        m_number_of_bins_per_descriptor(0),
        m_number_of_codebook_features(0)
    {
    }
    
    template <class T, class TCODEWORD, class N>
    CodebookBase<T, TCODEWORD, N>::CodebookBase(unsigned int number_of_dimensions, unsigned int number_of_codewords, unsigned int number_of_bins_per_codeword, unsigned int number_of_bins_per_descriptor) :
        m_number_of_dimensions(number_of_dimensions),
        m_number_of_codewords(number_of_codewords),
        m_number_of_bins_per_codeword(number_of_bins_per_codeword),
        m_number_of_bins_per_descriptor(number_of_bins_per_descriptor),
        m_number_of_codebook_features(number_of_codewords * number_of_bins_per_codeword)
    {
    }
    
    template <class T, class TCODEWORD, class N>
    CodebookBase<T, TCODEWORD, N>::CodebookBase(const CodebookBase<T, TCODEWORD, N> &other) :
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_number_of_codewords(other.m_number_of_codewords),
        m_number_of_bins_per_codeword(other.m_number_of_bins_per_codeword),
        m_number_of_bins_per_descriptor(other.m_number_of_bins_per_descriptor),
        m_number_of_codebook_features(other.m_number_of_codebook_features)
    {
    }
    
    template <class T, class TCODEWORD, class N>
    CodebookBase<T, TCODEWORD, N>::~CodebookBase(void)
    {
    }
    
    template <class T, class TCODEWORD, class N>
    CodebookBase<T, TCODEWORD, N>& CodebookBase<T, TCODEWORD, N>::operator=(const CodebookBase<T, TCODEWORD, N> &other)
    {
        if (this != &other)
        {
            m_number_of_dimensions = other.m_number_of_dimensions;
            m_number_of_codewords = other.m_number_of_codewords;
            m_number_of_bins_per_codeword = other.m_number_of_bins_per_codeword;
            m_number_of_bins_per_descriptor = other.m_number_of_bins_per_descriptor;
            m_number_of_codebook_features = other.m_number_of_codebook_features;
        }
        return * this;
    }
    
    template <class T, class TCODEWORD, class N>
    void CodebookBase<T, TCODEWORD, N>::set(unsigned int number_of_dimensions, unsigned int number_of_codewords, unsigned int number_of_bins_per_codeword, unsigned int number_of_bins_per_descriptor)
    {
        m_number_of_dimensions = number_of_dimensions;
        m_number_of_codewords = number_of_codewords;
        m_number_of_bins_per_codeword = number_of_bins_per_codeword;
        m_number_of_bins_per_descriptor = number_of_bins_per_descriptor;
        m_number_of_codebook_features = number_of_codewords * number_of_bins_per_codeword;
    }
    
    template <class T, class TCODEWORD, class N>
    template <template <class, class> class VECTOR>
    void CodebookBase<T, TCODEWORD, N>::encode(VECTOR<T, N> const * const * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger) const
    {
        typedef typename ConstantVectorType<VECTOR, T, N>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR * * constant_descriptor;
        
        // 1) Create an array of pointers to the constant sub-vectors of the original descriptors.
        constant_descriptor = new CONSTANT_VECTOR * [number_of_descriptors];
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                constant_descriptor[i] = new CONSTANT_VECTOR(descriptors[i]->getData(), descriptors[i]->size());
        }
        // 2) Call the virtual 'generate' function using the new the constant sub-vectors.
        encode(constant_descriptor, codes, number_of_descriptors, number_of_threads, logger);
        // 3) Free allocated memory.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                delete constant_descriptor[i];
        }
        delete [] constant_descriptor;
    }
    
    template <class T, class TCODEWORD, class N>
    template <template <class, class> class VECTOR>
    void CodebookBase<T, TCODEWORD, N>::encode(const VECTOR<T, N> * descriptors, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger) const
    {
        typedef typename ConstantVectorType<VECTOR, T, N>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR * * constant_descriptor;
        
        // 1) Create an array of pointers to the constant sub-vectors of the original descriptors.
        constant_descriptor = new CONSTANT_VECTOR * [number_of_descriptors];
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                constant_descriptor[i] = new CONSTANT_VECTOR(descriptors[i].getData(), descriptors[i].size());
        }
        // 2) Call the virtual 'generate' function using the new the constant sub-vectors.
        encode(constant_descriptor, codes, number_of_descriptors, number_of_threads, logger);
        // 3) Free allocated memory.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                delete constant_descriptor[i];
        }
        delete [] constant_descriptor;
    }
    
    template <class T, class TCODEWORD, class N>
    template <template <class, class> class VECTOR>
    void CodebookBase<T, TCODEWORD, N>::encode(const VECTOR<T, N> &descriptor, VectorSparse<TCODEWORD, unsigned int> &codes) const
    {
        typedef typename ConstantVectorType<VECTOR, T, N>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR constant_descriptor(descriptor.getData(), descriptor.size());
        CONSTANT_VECTOR * ptr = &constant_descriptor;
        encode(&ptr, &codes, 1, 1);
    }
    
    template <class T, class TCODEWORD, class N>
    template <template <class, class> class VECTOR>
    void CodebookBase<T, TCODEWORD, N>::generate(VECTOR<T, N> const * const * descriptors, const int * labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger)
    {
        typedef typename ConstantVectorType<VECTOR, T, N>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR * * constant_descriptor;
        
        // 1) Create an array of pointers to the constant sub-vectors of the original descriptors.
        constant_descriptor = new CONSTANT_VECTOR * [number_of_descriptors];
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                constant_descriptor[i] = new CONSTANT_VECTOR(descriptors[i]->getData(), descriptors[i]->size());
        }
        // 2) Call the virtual 'generate' function using the new the constant sub-vectors.
        generate(constant_descriptor, labels, number_of_descriptors, number_of_threads, logger);
        // 3) Free allocated memory.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                delete constant_descriptor[i];
        }
        delete [] constant_descriptor;
    }
    
    template <class T, class TCODEWORD, class N>
    template <template <class, class> class VECTOR>
    void CodebookBase<T, TCODEWORD, N>::generate(const VECTOR<T, N> * descriptors, const int * labels, unsigned int number_of_descriptors, unsigned int number_of_threads, BaseLogger * logger)
    {
        typedef typename ConstantVectorType<VECTOR, T, N>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR * * constant_descriptor;
        
        // 1) Create an array of pointers to the constant sub-vectors of the original descriptors.
        constant_descriptor = new CONSTANT_VECTOR * [number_of_descriptors];
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                constant_descriptor[i] = new CONSTANT_VECTOR(descriptors[i].getData(), descriptors[i].size());
        }
        // 2) Call the virtual 'generate' function using the new the constant sub-vectors.
        generate(constant_descriptor, labels, number_of_descriptors, number_of_threads, logger);
        // 3) Free allocated memory.
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                delete constant_descriptor[i];
        }
        delete [] constant_descriptor;
    }
    
    template <class T, class TCODEWORD, class N>
    void CodebookBase<T, TCODEWORD, N>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Codebook");
        parser.setAttribute("Identifier", this->getIdentifier());
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Number_Of_Codewords", m_number_of_codewords);
        parser.setAttribute("Number_Of_Bins_Per_Codeword", m_number_of_bins_per_codeword);
        parser.setAttribute("Number_Of_Bins_Per_Descriptor", m_number_of_bins_per_descriptor);
        convertAttributesToXML(parser);
        parser.addChildren();
        convertDataToXML(parser);
        parser.closeTag();
    }
    
    template <class T, class TCODEWORD, class N>
    void CodebookBase<T, TCODEWORD, N>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Codebook"))
        {
            CODEBOOK_IDENTIFIER current_id = (CODEBOOK_IDENTIFIER)((int)parser.getAttribute("Identifier"));
            if (current_id != this->getIdentifier())
                throw Exception("The given XML object does not contains information about the expected codebook object. It was expected an object with the identifier %d but an object with the identifier %d found instead.", (int)this->getIdentifier(), (int)current_id);
            
            initializeFromXML(parser); // Initialize the derived XML object.
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_number_of_codewords = parser.getAttribute("Number_Of_Codewords");
            m_number_of_bins_per_codeword = parser.getAttribute("Number_Of_Bins_Per_Codeword");
            m_number_of_bins_per_descriptor = parser.getAttribute("Number_Of_Bins_Per_Descriptor");
            m_number_of_codebook_features = m_number_of_codewords * m_number_of_bins_per_codeword;
            convertAttributesFromXML(parser); // Retrieve the attributes from the derived XML object.
            
            while (!(parser.isTagIdentifier("Codebook") && parser.isCloseTag()))
            {
                if (!convertDataFromXML(parser)) parser.getNext(); // Retrieve the data from the derived XML object.
            }
            parser.getNext();
            
            processFromXML(parser); // Post-process the retrieved information from the derived XML object.
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEBOOK FACTORY                     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class T, class TCODEWORD, class N>
    struct CodebookFactory { typedef Factory<CodebookBase<T, TCODEWORD, N> , CODEBOOK_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

