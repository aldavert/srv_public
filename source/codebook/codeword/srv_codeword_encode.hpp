// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEBOOK_ENCODE_HPP_HEADER_FILE__
#define __SRV_CODEBOOK_ENCODE_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../../srv_utilities.hpp"
#include "../../srv_xml.hpp"
#include "../../srv_vector.hpp"
// -[ Codebook auxiliary classes ]-------------------------------------
#include "../srv_codebook_definitions.hpp"
#include "srv_codeword_model.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CODEWORD BASE ENCODE CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which defines the common functionality of the codeword encode objects.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Destructor.
        virtual ~CodewordEncodeBase(void) {}
        
        // -[ Virtual access functions ]-------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        virtual unsigned int getNumberOfBinsPerCodeword(unsigned int number_of_bins) const = 0;
        
        // -[ Virtual encode functions ]-------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        virtual void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const = 0;
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        virtual void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const = 0;
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        virtual void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const = 0;
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        virtual CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const = 0;
        /////// /// Returns the class identifier for the current codeword encode method.
        /////// inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_#######; }
        /////// /// Generates a new empty instance of the codeword encode class.
        /////// inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncode#######<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /////// /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        /////// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        virtual CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the codeword encode object information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the codeword encode object information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeBase(void) {}
        /// Copy constructor.
        CodewordEncodeBase(const CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) {}
        /// Assignation operator.
        CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        virtual void convertAttributesToXML(XmlParser &parser) const = 0;
        /// Pure virtual function which retrieves the object attributes from the XML object.
        virtual void convertAttributesFromXML(XmlParser &parser) = 0;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        // /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        // static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Codeword_Encode");
        parser.setAttribute("Identifier", getIdentifier());
        convertAttributesToXML(parser);
        parser.closeTag();
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Codeword_Encode"))
        {
            const CODEWORD_ENCODE_IDENTIFIER xml_identifier = (CODEWORD_ENCODE_IDENTIFIER)((int)parser.getAttribute("Identifier"));
            if (xml_identifier != getIdentifier())
                throw Exception("Expecting codeword encode object with identifier '%d' but identifier '%d found instead.", (int)getIdentifier(), (int)xml_identifier);
            convertAttributesFromXML(parser);
            while (!(parser.isTagIdentifier("Codeword_Encode") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD ENCODE FACTORY CLASS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Definition of the codeword encode factory class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    struct CodewordEncodeFactory { typedef Factory<CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> , CODEWORD_ENCODE_IDENTIFIER > Type; };
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD CENTROID ENCODE CLASS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Codeword index based codeword encode class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeCentroid : public CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeCentroid(void) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>() {}
        /// Copy constructor.
        CodewordEncodeCentroid(const CodewordEncodeCentroid<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordEncodeCentroid(void) {}
        /// Assignation operator.
        CodewordEncodeCentroid<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeCentroid<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::operator=(other); } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        inline unsigned int getNumberOfBinsPerCodeword(unsigned int /* number_of_bins */) const { return 1; }
        
        // -[ Encode functions ]---------------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                    codes[i].set(codewords[i].size(), 1);
            }
        }
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        inline CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeCentroid<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword encode method.
        inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_CENTROID; }
        /// Generates a new empty instance of the codeword encode class.
        inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeCentroid<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        inline CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const { return CODEWORD_ENCODE_CENTROID; }
        
    protected:
        // -[ Protected auxiliary functions ]--------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        inline void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                    codes[i] = weights[i];
            }
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {};
        /// Function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {};
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        static int m_is_initialized;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD FIRST DERIVATIVE ENCODE     |
    //                   | CLASS                                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// First derivative based codeword encode class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeFirstDerivative : public CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeFirstDerivative(void) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>() {}
        /// Copy constructor.
        CodewordEncodeFirstDerivative(const CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordEncodeFirstDerivative(void) {}
        /// Assignation operator.
        CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::operator=(other); } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        inline unsigned int getNumberOfBinsPerCodeword(unsigned int number_of_bins) const { return number_of_bins; }
        
        // -[ Encode functions ]---------------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor. The function replaces the codeword index by the encoding offset of each codeword.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        inline CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword encode method.
        inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_FIRST_DERIVATIVE; }
        /// Generates a new empty instance of the codeword encode class.
        inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        inline CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const { return CODEWORD_ENCODE_FIRST_DERIVATIVE; }
        
    protected:
        // -[ Protected auxiliary functions ]--------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor dense vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <template <class, class> class VECTOR, class NVECTOR>
        inline void protectedDifference(const ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VECTOR<TCODEWORD, NVECTOR> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = (TCODEWORD)descriptor[i] - centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor sparse vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VectorDense<TCODEWORD> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = -centroid[i];
            for (unsigned int i = 0; i < descriptor.size(); ++i)
                difference[descriptor.getIndex(i)] += (TCODEWORD)descriptor.getValue(i);
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {};
        /// Function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {};
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions;
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                codes[i].set(weights[i].size() * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < weights[i].size(); ++j)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[weights[i][j].getSecond()];
                    unsigned int offset;
                    
                    offset = weights[i][j].getSecond() * bins_per_codeword;
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    for (unsigned int d = 0; d < number_of_dimensions; ++d, ++k, ++offset)
                        codes[i][k].setData(weights[i][j].getFirst() * difference[d], offset);
                }
            }
        }
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeFirstDerivative<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions;
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                const unsigned int number_of_neighbors = codewords[i].size();
                
                codes[i].set(number_of_neighbors * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < number_of_neighbors; ++j, k += number_of_dimensions)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[codewords[i][j]];
                    SubVectorDense<TCODEWORD> current_part(codes[i].getData() + k, number_of_dimensions);
                    
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), current_part);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD SUPER VECTOR ENCODE CLASS   |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Super-vector codeword encode class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeSuperVector : public CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeSuperVector(void) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(), m_weight(1) {}
        /** Constructor which sets the weight factor of the centroid bins.
         *  \param[in] weight factor which weights the contribution of the centroid bin in the histogram of visual words.
         */
        CodewordEncodeSuperVector(TCODEWORD weight) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(), m_weight(weight) {}
        /// Copy constructor.
        CodewordEncodeSuperVector(const CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other), m_weight(other.m_weight) {}
        /// Destructor.
        ~CodewordEncodeSuperVector(void) {}
        /// Assignation operator.
        CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::operator=(other); m_weight = other.m_weight; } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        inline unsigned int getNumberOfBinsPerCodeword(unsigned int number_of_bins) const { return number_of_bins + 1; }
        /// Returns the factor which weights the contribution of the centroid bin in the histogram of visual words.
        inline TCODEWORD getWeight(void) const { return m_weight; }
        /// Sets the factor which weights the contribution of the centroid bin in the histogram of visual words.
        inline void setWeight(TCODEWORD weight) { m_weight = weight; }
        
        // -[ Encode functions ]---------------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        inline CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword encode method.
        inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_SUPER_VECTOR; }
        /// Generates a new empty instance of the codeword encode class.
        inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        inline CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const { return CODEWORD_ENCODE_SUPER_VECTOR; }
        
    protected:
        // -[ Protected auxiliary functions ]--------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor dense vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <template <class, class> class VECTOR, class NVECTOR>
        inline void protectedDifference(const ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VECTOR<TCODEWORD, NVECTOR> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = (TCODEWORD)descriptor[i] - centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor sparse vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VectorDense<TCODEWORD> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = -centroid[i];
            for (unsigned int i = 0; i < descriptor.size(); ++i)
                difference[descriptor.getIndex(i)] += (TCODEWORD)descriptor.getValue(i);
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &parser) const { parser.setAttribute("Weight", m_weight); };
        /// Function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &parser) { m_weight = parser.getAttribute("Weight"); };
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Factor which weights the contribution of the centroid bin in the histogram of visual words.
        TCODEWORD m_weight;
        /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions + 1;
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                codes[i].set(weights[i].size() * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < weights[i].size(); ++j)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[weights[i][j].getSecond()];
                    unsigned int offset;
                    
                    offset = weights[i][j].getSecond() * bins_per_codeword;
                    codes[i][k].setData(weights[i][j].getFirst() * m_weight, offset);
                    ++k;
                    ++offset;
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    for (unsigned int d = 0; d < number_of_dimensions; ++d, ++k, ++offset)
                        codes[i][k].setData(weights[i][j].getFirst() * difference[d], offset);
                }
            }
        }
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeSuperVector<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions + 1;
        #pragma omp parallel num_threads(number_of_threads)
        {
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                const unsigned int number_of_neighbors = codewords[i].size();
                
                codes[i].set(number_of_neighbors * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < number_of_neighbors; ++j, k += bins_per_codeword)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[codewords[i][j]];
                    SubVectorDense<TCODEWORD> current_part(codes[i].getData() + k + 1, number_of_dimensions);
                    
                    codes[i][k] = m_weight;
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), current_part);
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD FISHER ENCODE CLASS         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Fisher kernel based codeword encode class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeFisher : public CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeFisher(void) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>() {}
        /// Copy constructor.
        CodewordEncodeFisher(const CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordEncodeFisher(void) {}
        /// Assignation operator.
        CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::operator=(other); } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        inline unsigned int getNumberOfBinsPerCodeword(unsigned int number_of_bins) const { return 2 * number_of_bins; }
        
        // -[ Encode functions ]---------------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        inline CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword encode method.
        inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_FISHER_VECTOR; }
        /// Generates a new empty instance of the codeword encode class.
        inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        inline CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const { return CODEWORD_ENCODE_FISHER_VECTOR; }
        
    protected:
        // -[ Protected auxiliary functions ]--------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor dense vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <template <class, class> class VECTOR, class NVECTOR>
        inline void protectedDifference(const ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VECTOR<TCODEWORD, NVECTOR> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = (TCODEWORD)descriptor[i] - centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor sparse vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VectorDense<TCODEWORD> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = -centroid[i];
            for (unsigned int i = 0; i < descriptor.size(); ++i)
                difference[descriptor.getIndex(i)] += (TCODEWORD)descriptor.getValue(i);
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {};
        /// Function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {};
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = 2 * number_of_dimensions;
        const double sqrt2 = 1.4142135623730951;
        const double factor = (double)META_FACTOR<TCODEWORD>::FACTOR;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                codes[i].set(weights[i].size() * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < weights[i].size(); ++j)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[weights[i][j].getSecond()];
                    const double * sigma_ptr = current_gaussian.getSigma();
                    unsigned int offset;
                    
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    offset = weights[i][j].getSecond() * bins_per_codeword;
                    for (unsigned int d = 0; d < number_of_dimensions; ++d, ++k, ++offset)
                    {
                        TCODEWORD value;
                        
                        value = (TCODEWORD)(factor * (double)difference[d] / sigma_ptr[d]);
                        codes[i][k].setData(weights[i][j].getFirst() * value, offset);
                    }
                    for (unsigned int d = 0; d < number_of_dimensions; ++d, ++k, ++offset)
                    {
                        double value;
                        
                        value = (double)difference[d] / sigma_ptr[d];
                        value = factor * (value * value - 1.0) / sqrt2;
                        codes[i][k].setData(weights[i][j].getFirst() * (TCODEWORD)value, offset);
                    }
                }
            }
        }
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = 2 * number_of_dimensions;
        const double sqrt2 = 1.4142135623730951;
        const double factor = (double)META_FACTOR<TCODEWORD>::FACTOR;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                const unsigned int number_of_neighbors = codewords[i].size();
                
                codes[i].set(number_of_neighbors * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < number_of_neighbors; ++j, k += bins_per_codeword)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[codewords[i][j]];
                    SubVectorDense<TCODEWORD> first_derivative(codes[i].getData() + k, number_of_dimensions);
                    SubVectorDense<TCODEWORD> second_derivative(codes[i].getData() + k + number_of_dimensions, number_of_dimensions);
                    const double * sigma_ptr = current_gaussian.getSigma();
                    
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    for (unsigned int d = 0; d < number_of_dimensions; ++d)
                    {
                        double sigma_difference;
                        
                        sigma_difference = (double)difference[d] / sigma_ptr[d];
                        first_derivative[d] = (TCODEWORD)(factor * sigma_difference);
                        second_derivative[d] = (TCODEWORD)(factor * (sigma_difference * sigma_difference - 1.0) / sqrt2);
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD SECOND DERIVATIVE ENCODE    |
    //                   | (TENSOR ENCODE) CLASS                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Second derivative (tensor) based codeword encode class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordEncodeTensor : public CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordEncodeTensor(void) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>() {}
        /// Copy constructor.
        CodewordEncodeTensor(const CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) : CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordEncodeTensor(void) {}
        /// Assignation operator.
        CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::operator=(other); } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /** Returns the number of bins used to encode a descriptor for each assigned codeword.
         *  \param[in] number_of_bins number of bins of the encoded descriptor.
         */
        inline unsigned int getNumberOfBinsPerCodeword(unsigned int number_of_bins) const { return number_of_bins * (number_of_bins + 1) / 2; }
        
        // -[ Encode functions ]---------------------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, weights, codes, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Encodes the descriptor using the selected codewords without weighting their contribution.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in] codewords indexes of the codewords used to encode each descriptor.
         *  \param[out] codes array with the encoded descriptors of each selected codeword. The encoding values of each codeword are concatenated together in a single vector for each descriptor.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        
        // -[ Codeword encode factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword encode object (virtual copy constructor).
        inline CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword encode method.
        inline static CODEWORD_ENCODE_IDENTIFIER getClassIdentifier(void) { return CODEWORD_ENCODE_TENSOR; }
        /// Generates a new empty instance of the codeword encode class.
        inline static CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordEncodeBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword encode class has been initialized in the factory.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword encode type identifier of the current object.
        inline CODEWORD_ENCODE_IDENTIFIER getIdentifier(void) const { return CODEWORD_ENCODE_TENSOR; }
        
    protected:
        // -[ Protected auxiliary functions ]--------------------------------------------------------------------------------------------------------
        /** Encodes the descriptor using the selected codewords.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in] weights array of sparse vectors with the weights associated to the codewords used to encode the descriptor.
         *  \param[out] codes array of sparse vectors with the resulting encoded descriptors.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor dense vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <template <class, class> class VECTOR, class NVECTOR>
        inline void protectedDifference(const ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VECTOR<TCODEWORD, NVECTOR> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = (TCODEWORD)descriptor[i] - centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor sparse vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const TCODEWORD * centroid, VectorDense<TCODEWORD> &difference) const
        {
            for (unsigned int i = 0; i < difference.size(); ++i)
                difference[i] = -centroid[i];
            for (unsigned int i = 0; i < descriptor.size(); ++i)
                difference[descriptor.getIndex(i)] += (TCODEWORD)descriptor.getValue(i);
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {};
        /// Function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {};
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword encode factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * weights, VectorSparse<TCODEWORD, unsigned int> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions * (number_of_dimensions + 1) / 2;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            VectorDense<TCODEWORD> tensor(bins_per_codeword);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                codes[i].set(weights[i].size() * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < weights[i].size(); ++j)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[weights[i][j].getSecond()];
                    unsigned int offset;
                    
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    for (unsigned int m = 0, d = 0; m < number_of_dimensions; ++m)
                        for (unsigned int n = m; n < number_of_dimensions; ++n, ++d)
                            tensor[d] = difference[m] * difference[n];
                    
                    offset = weights[i][j].getSecond() * bins_per_codeword;
                    for (unsigned int d = 0; d < bins_per_codeword; ++d, ++k, ++offset)
                        codes[i][k].setData(weights[i][j].getFirst() * tensor[d], offset);
                }
            }
        }
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordEncodeTensor<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, const VectorDense<unsigned int> * codewords, VectorDense<TCODEWORD> * codes, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
        const unsigned int bins_per_codeword = number_of_dimensions * (number_of_dimensions + 1) / 2;
        
        #pragma omp parallel num_threads(number_of_threads)
        {
            VectorDense<TCODEWORD> difference(number_of_dimensions);
            
            for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
            {
                const unsigned int number_of_neighbors = codewords[i].size();
                
                codes[i].set(number_of_neighbors * bins_per_codeword);
                for (unsigned int j = 0, k = 0; j < number_of_neighbors; ++j, k += bins_per_codeword)
                {
                    const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[codewords[i][j]];
                    SubVectorDense<TCODEWORD> tensor(codes[i].getData() + k, number_of_dimensions);
                    
                    protectedDifference(*descriptors[i], current_gaussian.getCentroid(), difference);
                    for (unsigned int m = 0, d = 0; m < number_of_dimensions; ++m)
                        for (unsigned int n = m; n < number_of_dimensions; ++n, ++d)
                            tensor[d] = difference[m] * difference[n];
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

