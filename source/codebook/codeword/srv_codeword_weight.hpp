// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEBOOK_WEIGHT_HPP_HEADER_FILE__
#define __SRV_CODEBOOK_WEIGHT_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../../srv_utilities.hpp"
#include "../../srv_xml.hpp"
#include "../../srv_vector.hpp"
#include "../../srv_matrix.hpp"
// -[ Codebook auxiliary classes ]-------------------------------------
#include "../srv_codebook_definitions.hpp"
#include "srv_codeword_model.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | CODEWORD WEIGHT BASE CLASS           |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Pure virtual class which defines the common functionality of the codeword weight objects.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightBase
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Destructor.
        virtual ~CodewordWeightBase(void) {}
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        virtual void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const = 0;
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        virtual void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const = 0;
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        virtual CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const = 0;
        /////// /// Returns the class identifier for the current codeword weight method.
        /////// inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_#######; }
        /////// /// Generates a new empty instance of the codeword weight class.
        /////// inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeight#######<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /////// /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        /////// inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        virtual CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const = 0;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the codeword weight object information into an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the codeword weight object information from an XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightBase(void) {}
        /// Copy constructor.
        CodewordWeightBase(const CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) {}
        /// Assignation operator.
        CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &operator=(const CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        virtual void convertAttributesToXML(XmlParser &parser) const = 0;
        /// Pure virtual function which retrieves the object attributes from the XML object.
        virtual void convertAttributesFromXML(XmlParser &parser) = 0;
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        // /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        // static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Codeword_Weight");
        parser.setAttribute("Identifier", getIdentifier());
        convertAttributesToXML(parser);
        parser.closeTag();
    }
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    void CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Codeword_Weight"))
        {
            const CODEWORD_WEIGHT_IDENTIFIER xml_identifier = (CODEWORD_WEIGHT_IDENTIFIER)((int)parser.getAttribute("Identifier"));
            if (xml_identifier != getIdentifier())
                throw Exception("Expecting codeword weight object with identifier '%d' but identifier '%d found instead.", (int)getIdentifier(), (int)xml_identifier);
            convertAttributesFromXML(parser);
            while (!(parser.isTagIdentifier("Codeword_Weight") && parser.isCloseTag()))
                parser.getNext();
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD WEIGHT FACTORY CLASS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Definition of the codeword weight factory class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    struct CodewordWeightFactory { typedef Factory<CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> , CODEWORD_WEIGHT_IDENTIFIER > Type; };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD UNIFORM WEIGHT CLASS        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Uniformly distributed codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightUniform : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightUniform(void) {}
        /// Copy constructor.
        CodewordWeightUniform(const CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordWeightUniform(void) {}
        /// Assignation operator.
        CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_UNIFORM; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_UNIFORM; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {}
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {}
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightUniform<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                    for (unsigned int n = 0; n < distances[i].size(); ++n)
                        distances[i][n].setFirst((TDISTANCE)factor);
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD DISTANCE WEIGHT CLASS       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Distance relative codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightDistance : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightDistance(void) {}
        /// Copy constructor.
        CodewordWeightDistance(const CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordWeightDistance(void) {}
        /// Assignation operator.
        CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_DISTANCE; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_DISTANCE; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {}
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {}
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightDistance<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                {
                    if (distances[i].size() == 1) distances[i][0].setFirst((TDISTANCE)factor);
                    else if (distances[i].size() > 1)
                    {
                        double accumulated_distance;
                        
                        accumulated_distance = 0;
                        for (unsigned int n = 0; n < distances[i].size(); ++n)
                            accumulated_distance += (double)distances[i][n].getFirst();
                        
                        if (accumulated_distance > 0)
                            for (unsigned int n = 0; n < distances[i].size(); ++n)
                                distances[i][n].setFirst((TDISTANCE)((double)factor * (1.0 - (double)distances[i][n].getFirst() / accumulated_distance)));
                        else
                            for (unsigned int n = 0; n < distances[i].size(); ++n)
                                distances[i][n].setFirst((TDISTANCE)factor);
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD LINEARLY-CONSTRAINED LOCAL  |
    //                   | CODING WEIGHT CLASS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Linearly-constrained local coding based codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightLLC : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightLLC(void) {}
        /// Copy constructor.
        CodewordWeightLLC(const CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordWeightLLC(void) {}
        /// Assignation operator.
        CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_LINEARLY_CONSTRAINED_LOCAL_CODING; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_LINEARLY_CONSTRAINED_LOCAL_CODING; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor dense vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const ConstantSubVectorDense<TCODEWORD, unsigned int> &centroid, double * difference) const
        {
            for (unsigned int i = 0; i < centroid.size(); ++i)
                difference[i] = (double)descriptor[i] - (double)centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor sparse vector descriptor.
         *  \param[in] centroid dense vector centroid.
         *  \param[in] difference array where the resulting difference is stored.
         */
        inline void protectedDifference(const ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> &descriptor, const ConstantSubVectorDense<TCODEWORD, unsigned int> &centroid, double * difference) const
        {
            for (unsigned int i = 0; i < centroid.size(); ++i)
                difference[i] = -(double)centroid[i];
            for (unsigned int i = 0; i < descriptor.size(); ++i)
                difference[descriptor.getIndex(i)] += (double)descriptor.getValue(i);
        }
        
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {}
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {}
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightLLC<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const unsigned int number_of_dimensions = codeword_gaussians[0].getNumberOfDimensions();
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            unsigned int number_of_neighbors;
            
            number_of_neighbors = 0;
            for (unsigned int i = 0; i < number_of_descriptors; ++i)
                if (distances[i].size() > number_of_neighbors)
                    number_of_neighbors = distances[i].size();
            if (number_of_neighbors == 1)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                        for (unsigned int n = 0; n < distances[i].size(); ++n)
                            distances[i][n].setFirst((TDISTANCE)factor);
                }
            }
            else if (number_of_neighbors > 1)
            {
                #pragma omp parallel num_threads(number_of_threads)
                {
                    Matrix<double> z(number_of_dimensions, number_of_neighbors);
                    Matrix<double> C(number_of_neighbors, number_of_neighbors);
                    Matrix<double> ones(number_of_neighbors, 1, 1.0);
                    Matrix<double> w;
                    ConstantSubVectorDense<TCODEWORD, unsigned int> centroid;
                    
                    for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                    {
                        if (distances[i].size() > 0)
                        {
                            const double beta = 1e-7;
                            double diagonal_factor, weight_sum;
                            
                            // Calculate the z' (transposed) matrix. The transposed is calculated in order to take advantage of the ordering of values inside the matrix.
                            for (unsigned int n = 0; n < number_of_neighbors; ++n)
                            {
                                const CodewordGaussianModel<TCODEWORD> &selected_gaussian = codeword_gaussians[distances[i][n].getSecond()];
                                centroid.set(selected_gaussian.getCentroid(), selected_gaussian.getNumberOfDimensions());
                                protectedDifference(*descriptors[i], centroid, z(n));
                            }
                            
                            // Find the weights assigned to each class
                            MatrixMultiplication(z, true, z, false, C);               // C = z * z';
                            diagonal_factor = beta * MatrixTrace(C);                  // C = C + eye(number_of_neighbors) * beta * trace(C);
                            for (unsigned int k = 0; k < number_of_neighbors; ++k)
                                C(k, k) += diagonal_factor;
                            MatrixSolve(C, ones, w);                                  // w = C \ ones(number_of_neighbors, 1);
                            weight_sum = 0;                                           // w = w / sum(w);
                            for (unsigned int k = 0; k < number_of_neighbors; ++k)
                                weight_sum += w(k, 0);
                            
                            for (unsigned int n = 0; n < number_of_neighbors; ++n)
                                distances[i][n].setFirst((TDISTANCE)((double)factor * w(n, 0) / weight_sum));
                        }
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD GAUSSIAN KERNEL WEIGHT      |
    //                   | CLASS                                |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Gaussian kernel based codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightGaussian : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightGaussian(void) : m_sigma(1) {}
        /// Constructor which initializes the sigma of the Gaussian kernel.
        CodewordWeightGaussian(TDISTANCE sigma) : m_sigma(sigma) {}
        /// Copy constructor.
        CodewordWeightGaussian(const CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other),
            m_sigma(other.m_sigma) {}
        /// Destructor.
        ~CodewordWeightGaussian(void) {}
        /// Assignation operator.
        CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { m_sigma = other.m_sigma; } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the sigma of the Gaussian kernel.
        inline TDISTANCE getSigma(void) const { return m_sigma; }
        /// Sets the sigma of the Gaussian kernel.
        inline void setSigma(TDISTANCE sigma) { m_sigma = sigma; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_GAUSSIAN_KERNEL; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_GAUSSIAN_KERNEL; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &parser) const { parser.setAttribute("Sigma", m_sigma); }
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &parser) { m_sigma = parser.getAttribute("Sigma"); }
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Sigma of the Gaussian kernel.
        TDISTANCE m_sigma;
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                    for (unsigned int n = 0; n < distances[i].size(); ++n)
                        distances[i][n].setFirst((TDISTANCE)((double)factor * exp(-(double)distances[i][n].getFirst() / (double)m_sigma)));
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD NORMALIZED GAUSSIAN KERNEL  |
    //                   | WEIGHT CLASS                         |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Probabilistic Gaussian kernel based codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightNormalizedGaussian : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightNormalizedGaussian(void) : m_sigma(1) {}
        /// Constructor which initializes the sigma of the Gaussian kernel.
        CodewordWeightNormalizedGaussian(TDISTANCE sigma) : m_sigma(sigma) {}
        /// Copy constructor.
        CodewordWeightNormalizedGaussian(const CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other),
            m_sigma(other.m_sigma) {}
        /// Destructor.
        ~CodewordWeightNormalizedGaussian(void) {}
        /// Assignation operator.
        CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) { if (this != &other) { m_sigma = other.m_sigma; } return *this; }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the sigma of the Gaussian kernel.
        inline TDISTANCE getSigma(void) const { return m_sigma; }
        /// Sets the sigma of the Gaussian kernel.
        inline void setSigma(TDISTANCE sigma) { m_sigma = sigma; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &parser) const { parser.setAttribute("Sigma", m_sigma); }
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &parser) { m_sigma = parser.getAttribute("Sigma"); }
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Sigma of the Gaussian kernel.
        TDISTANCE m_sigma;
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightNormalizedGaussian<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * /* descriptors */, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * /* codeword_gaussians */, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                VectorDense<double> weights;
                double accumulated_weight;
                
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                {
                    if (distances[i].size() == 1) distances[i][0].setFirst((TDISTANCE)factor);
                    else if (distances[i].size() > 1)
                    {
                        if (weights.size() != distances[i].size()) weights.set(distances[i].size());
                        accumulated_weight = 0;
                        for (unsigned int n = 0; n < distances[i].size(); ++n)
                        {
                            const double current_weight = exp(-(double)distances[i][n].getFirst() / (double)m_sigma);
                            weights[n] = current_weight;
                            accumulated_weight += current_weight;
                        }
                        
                        for (unsigned int n = 0; n < distances[i].size(); ++n)
                            distances[i][n].setFirst((TDISTANCE)((double)factor * weights[n] / accumulated_weight));
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | CODEWORD FISHER VECTOR WEIGHT CLASS  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Fisher kernel based codeword weight class.
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    class CodewordWeightFisher : public CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordWeightFisher(void) {}
        /// Copy constructor.
        CodewordWeightFisher(const CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &other) :
            CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(other) {}
        /// Destructor.
        ~CodewordWeightFisher(void) {}
        /// Assignation operator.
        CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>& operator=(const CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> &/* other */) { return *this; }
        
        // -[ Virtual weight functions ]-------------------------------------------------------------------------------------------------------------
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the dense vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorDense<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        /** Calculates the weight the codewords for the given set of descriptors.
         *  \param[in] descriptors array of pointers to the sparse vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        inline void process(ConstantSubVectorSparse<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const { protectedProcess(descriptors, distances, number_of_descriptors, codeword_gaussians, number_of_threads); }
        
        // -[ Codeword weight factory functions ]----------------------------------------------------------------------------------------------------
        /// Duplicates the codeword weight object (virtual copy constructor).
        inline CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * duplicate(void) const { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(*this); }
        /// Returns the class identifier for the current codeword weight method.
        inline static CODEWORD_WEIGHT_IDENTIFIER getClassIdentifier(void) { return CODEWORD_WEIGHT_FISHER_VECTOR; }
        /// Generates a new empty instance of the codeword weight class.
        inline static CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> * generateObject(void) { return (CodewordWeightBase<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR> *)new CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>(); }
        /// Returns a flag which states if the codeword weight class has been initialized in the factor.
        inline static int isInitialized(void) { return m_is_initialized; }
        /// Returns the codeword weight type identifier of the current object.
        inline CODEWORD_WEIGHT_IDENTIFIER getIdentifier(void) const { return CODEWORD_WEIGHT_FISHER_VECTOR; }
        
    protected:
        /** Protected template function which implements the <b>process</b> weight function both for dense and sparse vectors.
         *  \param[in] descriptors array of pointers to the vectors with the descriptor values.
         *  \param[in,out] distances array of sparse vectors with the distances to the selected codewords for each descriptor. The distance values are replace by the weight of the codewords.
         *  \param[in] number_of_descriptors number of descriptors encoded.
         *  \param[in] codeword_gaussians array with the parameters of the Gaussian function which model the codewords of the codebook.
         *  \param[in] number_of_threads number of threads used to concurrently process the descriptors.
         */
        template <template <class, class> class VECTOR>
        void protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const;
        // -[ Protected XML functions ]--------------------------------------------------------------------------------------------------------------
        /// Pure virtual function which stores the object attributes in the XML object.
        inline void convertAttributesToXML(XmlParser &/* parser */) const {}
        /// Pure virtual function which retrieves the object attributes from the XML object.
        inline void convertAttributesFromXML(XmlParser &/* parser */) {}
        
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Static integer which indicates if the class has been initialized in the codeword weight factory.
        static int m_is_initialized;
    };
    
    template <class TDESCRIPTOR, class TCODEWORD, class TDISTANCE, class NDESCRIPTOR>
    template <template <class, class> class VECTOR>
    void CodewordWeightFisher<TDESCRIPTOR, TCODEWORD, TDISTANCE, NDESCRIPTOR>::protectedProcess(VECTOR<TDESCRIPTOR, NDESCRIPTOR> const * const * descriptors, VectorSparse<TDISTANCE> * distances, unsigned int number_of_descriptors, const CodewordGaussianModel<TCODEWORD> * codeword_gaussians, unsigned int number_of_threads) const
    {
        if (number_of_descriptors > 0)
        {
            const TCODEWORD factor = (TCODEWORD)META_FACTOR<TCODEWORD>::FACTOR;
            
            #pragma omp parallel num_threads(number_of_threads)
            {
                VectorDense<double> weight;
                ConstantSubVectorDense<TCODEWORD, unsigned int> centroid;
                
                for (unsigned int i = omp_get_thread_num(); i < number_of_descriptors; i += number_of_threads)
                {
                    if (distances[i].size() == 1)
                    {
                        const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[distances[i][0].getSecond()];
                        distances[i][0].setFirst((TDISTANCE)((double)factor * sqrt((double)current_gaussian.getWeight())));
                    }
                    else if (distances[i].size() > 1)
                    {
                        double accumulated_weight;
                        
                        if (weight.size() != distances[i].size()) weight.set(distances[i].size());
                        accumulated_weight = 0;
                        for (unsigned int n = 0; n < distances[i].size(); ++n)
                        {
                            weight[n] = codeword_gaussians[distances[i][n].getSecond()].evaluate(*descriptors[i]);
                            accumulated_weight += weight[n];
                        }
                        
                        if (accumulated_weight > 0)
                        {
                            for (unsigned int n = 0; n < distances[i].size(); ++n)
                            {
                                const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[distances[i][n].getSecond()];
                                const double wi = sqrt((double)current_gaussian.getWeight());
                                distances[i][n].setFirst((TDISTANCE)((double)factor * weight[n] / (wi * accumulated_weight)));
                            }
                        }
                        else
                        {
                            for (unsigned int n = 0; n < distances[i].size(); ++n)
                            {
                                const CodewordGaussianModel<TCODEWORD> &current_gaussian = codeword_gaussians[distances[i][0].getSecond()];
                                const double wi = sqrt((double)current_gaussian.getWeight());
                                distances[i][0].setFirst((TDISTANCE)((double)factor * wi));
                            }
                        }
                    }
                }
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

