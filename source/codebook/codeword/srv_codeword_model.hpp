// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __SRV_CODEWORD_MODEL_HPP_HEADER_FILE__
#define __SRV_CODEWORD_MODEL_HPP_HEADER_FILE__

// -[ C++ header files ]-----------------------------------------------
#include <map>
#include <vector>
// -[ Other header files]----------------------------------------------
#include <omp.h>
// -[ SRV header files ]-----------------------------------------------
#include "../../srv_utilities.hpp"
#include "../../srv_xml.hpp"
#include "../../srv_vector.hpp"
// -[ Codebook auxiliary classes ]-------------------------------------
#include "../srv_codebook_definitions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | GAUSSIAN MODEL OF THE CODEWORD       |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Gaussian model which models the codeword.
    template <class TCODEWORD>
    class CodewordGaussianModel
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        CodewordGaussianModel(void);
        /** Constructor which sets the parameters of the Gaussian model.
         *  \param[in] weight weight of the Gaussian function.
         *  \param[in] centroid center of the codeword.
         *  \param[in] sigma values of the diagonal covariance matrix.
         *  \param[in] number_of_dimensions number of dimensions of the codeword.
         */
        CodewordGaussianModel(double weight, const TCODEWORD * centroid, const double * sigma, unsigned int number_of_dimensions);
        /// Copy constructor.
        CodewordGaussianModel(const CodewordGaussianModel<TCODEWORD> &other);
        /// Destruction.
        ~CodewordGaussianModel(void);
        /// Assignation operator.
        CodewordGaussianModel<TCODEWORD>& operator=(const CodewordGaussianModel<TCODEWORD> &other);
        
        // -[ Generates the Gaussian model ]---------------------------------------------------------------------------------------------------------
        /** This function sets the parameters of the Gaussian function which models the codeword.
         *  \param[in] centroid center point of the codeword cluster.
         *  \param[in] data array of pointer to the dense vectors belonging to the cluster.
         *  \param[in] number_of_samples number of dense vectors in the array.
         *  \param[in] number_of_dimensions number of dimensions of the samples.
         *  \param[in] number_of_threads number of threads used to generate the Gaussian model.
         */
        template <template <class, class> class VECTOR, class TDATA, class NDATA>
        void generate(const TCODEWORD * centroid, VECTOR<TDATA, NDATA> const * const * data, unsigned int number_of_samples, unsigned int number_of_dimensions, unsigned int number_of_threads);
        /** This function sets the parameters of the Gaussian function which models the codeword.
         *  \param[in] centroid center point of the codeword cluster.
         *  \param[in] data array of pointer to the sparse vectors belonging to the cluster.
         *  \param[in] number_of_samples number of sparse vectors in the array.
         *  \param[in] number_of_dimensions number of dimensions of the samples.
         *  \param[in] number_of_threads number of threads used to generate the Gaussian model.
         */
        template <template <class, class> class VECTOR, class TDATA, class NDATA>
        void generate(const TCODEWORD * centroid, const VECTOR<TDATA, NDATA> * data, unsigned int number_of_samples, unsigned int number_of_dimensions, unsigned int number_of_threads);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /// This function sets the centroid of the Gaussian function.
        inline void setCentroid(const TCODEWORD * centroid) { m_centroid = centroid; }
        /** This function sets an isometric Gaussian distribution.
         *  \param[in] centroid center of the codeword.
         *  \param[in] number_of_dimensions number of dimensions of the codeword.
         */
        void setIsometricModel(const TCODEWORD * centroid, double sigma, unsigned int number_of_dimensions);
        /** Set the parameters of the Gaussian model.
         *  \param[in] weight weight of the Gaussian function.
         *  \param[in] centroid center of the codeword.
         *  \param[in] sigma values of the diagonal covariance matrix.
         *  \param[in] number_of_dimensions number of dimensions of the codeword.
         */
        void set(double weight, const TCODEWORD * centroid, const double * sigma, unsigned int number_of_dimensions);
        /// Sets the weight of the Gaussian model.
        inline void setWeight(double weight) { m_weight = weight; }
        /// Sets the values of the sigma vector.
        inline void setSigma(const double * sigma)
        {
            for (unsigned int i = 0; i < m_number_of_dimensions; ++i)
            {
                m_sigma[i] = sigma[i];
                if (m_sigma[i] == 0) m_sigma[i] = 1;
            }
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the weight of the Gaussian function.
        inline double getWeight(void) const { return m_weight; }
        /// Returns a constant pointer to the array with the Gaussian function mean (i.e. codeword centroid).
        inline const TCODEWORD * getCentroid(void) const { return m_centroid; }
        /// Returns the covariance matrix of the Gaussian function.
        inline const double * getSigma(void) const { return m_sigma; }
        /// Returns the number of dimensions of the feature space. 
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        
        // -[ Evaluation functions ]-----------------------------------------------------------------------------------------------------------------
        /** Evaluation of the Gaussian function value at the given descriptor.
         *  \param[in] descriptor descriptor vector.
         *  \returns the value of the Gaussian function at the descriptor point.
         */
        template <template <class, class> class VECTOR, class T, class N>
        double evaluate(const VECTOR<T, N> &descriptor) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the codeword Gaussian model information to an XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the codeword Gaussian model information from an XML object.
        void convertFromXML(XmlParser &parser);
        
        // -[ Static auxiliary functions ]-----------------------------------------------------------------------------------------------------------
        /** Calculates the difference vector between the given descriptor and the codeword centroid.
         *  \param[in] descriptor_data array of the dense vector.
         *  \param[in] descriptor_size size of the descriptor array.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <class TDATA, class NDATA>
        inline void vectorDifference(const TDATA * descriptor_data, NDATA /* descriptor_size */, double * difference) const
        {
            for (unsigned int i = 0; i < m_number_of_dimensions; ++i)
                difference[i] = (double)descriptor_data[i] - (double)m_centroid[i];
        }
        /** Calculates the difference vector between the given descriptor and the given codeword centroid.
         *  \param[in] descriptor_data array of the sparse vector.
         *  \param[in] descriptor_size size of the descriptor array.
         *  \param[in] difference array where the resulting difference is stored.
         */
        template <class TDATA, class NDATA>
        inline void vectorDifference(const Tuple<TDATA, NDATA> * descriptor_data, NDATA descriptor_size, double * difference) const
        {
            for (unsigned int i = 0; i < m_number_of_dimensions; ++i)
                difference[i] = -(double)m_centroid[i];
            for (unsigned int i = 0; i < descriptor_size; ++i)
                difference[descriptor_data[i].getSecond()] += (double)descriptor_data[i].getFirst();
        }
        
    protected:
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Weight of the Gaussian function.
        double m_weight;
        /// Centroid of the Gaussian function.
        const TCODEWORD * m_centroid;
        /// Diagonal of the covariance matrix of the Gaussian function.
        double * m_sigma;
        /// Number of dimensions of the feature space.
        unsigned int m_number_of_dimensions;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | GAUSSIAN MODEL OF THE CODEWORD       |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TCODEWORD>
    CodewordGaussianModel<TCODEWORD>::CodewordGaussianModel(void) :
        m_weight(0),
        m_centroid(0),
        m_sigma(0),
        m_number_of_dimensions(0)
    {
    }
    
    template <class TCODEWORD>
    CodewordGaussianModel<TCODEWORD>::CodewordGaussianModel(const CodewordGaussianModel<TCODEWORD> &other) :
        m_weight(other.m_weight),
        m_centroid(other.m_centroid),
        m_sigma((other.m_number_of_dimensions != 0)?new double[other.m_number_of_dimensions]:0),
        m_number_of_dimensions(other.m_number_of_dimensions)
    {
        for (unsigned int i = 0; i < other.m_number_of_dimensions; ++i)
            m_sigma[i] = other.m_sigma[i];
    }
    
    template <class TCODEWORD>
    CodewordGaussianModel<TCODEWORD>::CodewordGaussianModel(double weight, const TCODEWORD * centroid, const double * sigma, unsigned int number_of_dimensions) :
        m_weight(weight),
        m_centroid(centroid),
        m_sigma((number_of_dimensions > 0)?new double[number_of_dimensions]:0),
        m_number_of_dimensions(number_of_dimensions)
    {
        for (unsigned int i = 0; i < number_of_dimensions; ++i)
            m_sigma[i] = sigma[i];
    }
    
    template <class TCODEWORD>
    CodewordGaussianModel<TCODEWORD>::~CodewordGaussianModel(void)
    {
        if (m_sigma != 0) delete [] m_sigma;
    }
    
    template <class TCODEWORD>
    CodewordGaussianModel<TCODEWORD>& CodewordGaussianModel<TCODEWORD>::operator=(const CodewordGaussianModel<TCODEWORD> &other)
    {
        if (this != &other)
        {
            if (m_sigma != 0) delete [] m_sigma;
            
            m_weight = other.m_weight;
            m_centroid = other.m_centroid;
            if (other.m_number_of_dimensions > 0)
            {
                m_sigma = new double[other.m_number_of_dimensions];
                m_number_of_dimensions = other.m_number_of_dimensions;
                for (unsigned int i = 0; i < other.m_number_of_dimensions; ++i)
                    m_sigma[i] = other.m_sigma[i];
            }
            else
            {
                m_sigma = 0;
                m_number_of_dimensions = 0;
            }
        }
        
        return * this;
    }
    
    template <class TCODEWORD>
    void CodewordGaussianModel<TCODEWORD>::setIsometricModel(const TCODEWORD * centroid, double sigma, unsigned int number_of_dimensions)
    {
        if (m_sigma != 0) { delete [] m_sigma; m_sigma = 0; }
        m_weight = 1.0;
        m_centroid = centroid;
        m_sigma = new double[number_of_dimensions];
        m_number_of_dimensions = number_of_dimensions;
        for (unsigned int i = 0; i < number_of_dimensions; ++i)
            m_sigma[i] = sigma;
    }
    
    template <class TCODEWORD>
    void CodewordGaussianModel<TCODEWORD>::set(double weight, const TCODEWORD * centroid, const double * sigma, unsigned int number_of_dimensions)
    {
        if (m_sigma != 0) delete [] m_sigma;
        
        m_weight = weight;
        m_centroid = centroid;
        m_number_of_dimensions = number_of_dimensions;
        if (number_of_dimensions > 0)
        {
            m_sigma = new double[number_of_dimensions];
            for (unsigned int i = 0; i < number_of_dimensions; ++i)
                m_sigma[i] = sigma[i];
        }
        else m_sigma = 0;
    }
    
    template <class TCODEWORD>
    template <template <class, class> class VECTOR, class TDATA, class NDATA>
    void CodewordGaussianModel<TCODEWORD>::generate(const TCODEWORD * centroid, VECTOR<TDATA, NDATA> const * const * data, unsigned int number_of_samples, unsigned int number_of_dimensions, unsigned int number_of_threads)
    {
        const double factor = (double)META_FACTOR<TCODEWORD>::FACTOR;
        if (m_sigma != 0) delete [] m_sigma;
        
        if ((number_of_dimensions > 0) && (number_of_samples > 0))
        {
            m_centroid = centroid;
            m_number_of_dimensions = number_of_dimensions;
            m_sigma = new double[number_of_dimensions];
            for (unsigned int i = 0; i < number_of_dimensions; ++i) m_sigma[i] = 0;
            
            if (number_of_threads > 1)
            {
                VectorDense<double> * accumulated_difference;
                double current_weight;
                
                // Calculate the covariance diagonal matrix.
                // ....................................................................................
                accumulated_difference = new VectorDense<double>[number_of_threads];
                #pragma omp parallel num_threads(number_of_threads)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    VectorDense<double> difference(number_of_dimensions);
                    
                    accumulated_difference[thread_identifier].set(number_of_dimensions, 0);
                    for (unsigned int i = thread_identifier; i < number_of_samples; i += number_of_threads)
                    {
                        vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                        for (unsigned int j = 0; j < number_of_dimensions; ++j)
                            accumulated_difference[thread_identifier][j] += (difference[j] * difference[j]);
                    }
                }
                for (unsigned int j = 0; j < number_of_dimensions; ++j)
                {
                    for (unsigned int t = 1; t < number_of_threads; ++t)
                        accumulated_difference[0][j] += accumulated_difference[t][j];
                    accumulated_difference[0][j] = srvMax<double>(1e-10, accumulated_difference[0][j] / (double)number_of_samples);
                }
                
                // Rescale the sigma!
                // ....................................................................................
                current_weight = 0;
                #pragma omp parallel num_threads(number_of_threads) reduction(+:current_weight)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    VectorDense<double> difference(number_of_dimensions);
                    
                    for (unsigned int i = thread_identifier; i < number_of_samples; i += number_of_threads)
                    {
                        vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                        for (unsigned int j = 0; j < number_of_dimensions; ++j)
                            current_weight += (difference[j] * difference[j]) / (2.0 * accumulated_difference[0][j]);
                    }
                }
                accumulated_difference[0] *= (current_weight / (double)number_of_samples);
                
                // Calculate the weight for the Gaussian function.
                // ....................................................................................
                current_weight = 0;
                #pragma omp parallel num_threads(number_of_threads) reduction(+:current_weight)
                {
                    const unsigned int thread_identifier = omp_get_thread_num();
                    VectorDense<double> difference(number_of_dimensions);
                    
                    for (unsigned int i = thread_identifier; i < number_of_samples; i += number_of_threads)
                    {
                        double sum;
                        
                        vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                        sum = 0.0;
                        for (unsigned int j = 0; j < number_of_dimensions; ++j)
                            sum += (difference[j] * difference[j]) / (2.0 * accumulated_difference[0][j]);
                        current_weight += exp(-sum);
                    }
                }
                for (unsigned int j = 0; j < number_of_dimensions; ++j)
                    m_sigma[j] = sqrt(accumulated_difference[0][j]);
                m_weight = factor * current_weight / (double)number_of_samples;
                
                delete [] accumulated_difference;
            }
            else // Single thread code.
            {
                VectorDense<double> difference(number_of_dimensions);
                SubVectorDense<double> accumulated_difference;
                double current_weight;
                
                accumulated_difference.set(m_sigma, number_of_dimensions);
                
                // Calculate the covariance diagonal matrix.
                // ....................................................................................
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                    for (unsigned int j = 0; j < number_of_dimensions; ++j)
                        accumulated_difference[j] += (difference[j] * difference[j]);
                }
                for (unsigned int j = 0; j < number_of_dimensions; ++j)
                    accumulated_difference[j] = srvMax<double>(1e-10, accumulated_difference[j] / (double)number_of_samples);
                
                // Rescale the sigma!
                // ....................................................................................
                current_weight = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                    for (unsigned int j = 0; j < number_of_dimensions; ++j)
                        current_weight += (difference[j] * difference[j]) / (2.0 * accumulated_difference[j]);
                }
                accumulated_difference *= (current_weight / (double)number_of_samples);
                
                // Calculate the weight for the Gaussian function.
                // ....................................................................................
                current_weight = 0;
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    double sum;
                    
                    vectorDifference(data[i]->getData(), data[i]->size(), difference.getData());
                    sum = 0.0;
                    for (unsigned int j = 0; j < number_of_dimensions; ++j)
                        sum += (difference[j] * difference[j]) / (2.0 * accumulated_difference[j]);
                    current_weight += exp(-sum);
                }
                for (unsigned int j = 0; j < number_of_dimensions; ++j)
                    accumulated_difference[j] = sqrt(accumulated_difference[j]);
                m_weight = factor * current_weight / (double)number_of_samples;
            }
        }
        else
        {
            m_centroid = 0;
            m_number_of_dimensions = 0;
            m_sigma = 0;
            m_weight = 0.0;
        }
    }
    
    template <class TCODEWORD>
    template <template <class, class> class VECTOR, class TDATA, class NDATA>
    void CodewordGaussianModel<TCODEWORD>::generate(const TCODEWORD * centroid, const VECTOR<TDATA, NDATA> * data, unsigned int number_of_samples, unsigned int number_of_dimensions, unsigned int number_of_threads)
    {
        typedef typename ConstantVectorType<VECTOR, TDATA, NDATA>::Type CONSTANT_VECTOR;
        CONSTANT_VECTOR * * data_ptrs;
        
        // Create an array of constant vectors to the original data vectors.
        data_ptrs = new CONSTANT_VECTOR * [number_of_samples];
        for (unsigned int i = 0; i < number_of_samples; ++i)
            data_ptrs[i] = new CONSTANT_VECTOR(data[i].getData(), data[i].size());
        
        // Call the array of pointers function.
        generate(centroid, data_ptrs, number_of_samples, number_of_dimensions, number_of_threads);
        
        // Free allocated memory.
        for (unsigned int i = 0; i < number_of_samples; ++i)
            delete data_ptrs[i];
        delete [] data_ptrs;
    }
    
    template <class TCODEWORD>
    template <template <class, class> class VECTOR, class T, class N>
    double CodewordGaussianModel<TCODEWORD>::evaluate(const VECTOR<T, N> &descriptor) const
    {
        VectorDense<double> difference(m_number_of_dimensions);
        double value;
        
        vectorDifference(descriptor.getData(), descriptor.size(), difference.getData());
        value = 0;
        for (unsigned int i = 0; i < m_number_of_dimensions; ++i)
            value += difference[i] * difference[i] / (2.0 * m_sigma[i] * m_sigma[i]);
        return m_weight * exp(-value);
    }
    
    template <class TCODEWORD>
    void CodewordGaussianModel<TCODEWORD>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("Codeword_Gaussian_Model");
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Weight", m_weight);
        parser.addChildren();
        if (m_number_of_dimensions > 0)
            saveVector(parser, "Sigma", ConstantSubVectorDense<double, unsigned int>(m_sigma, m_number_of_dimensions));
        parser.closeTag();
    }
    
    template <class TCODEWORD>
    void CodewordGaussianModel<TCODEWORD>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("Codeword_Gaussian_Model"))
        {
            if (m_sigma != 0) { delete [] m_sigma; m_sigma = 0; }
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_weight = parser.getAttribute("Weight");
            if (m_number_of_dimensions > 0)
                m_sigma = new double[m_number_of_dimensions];
            
            while (!(parser.isTagIdentifier("Codeword_Gaussian_Model") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Sigma"))
                {
                    VectorDense<double, unsigned int> data;
                    loadVector(parser, "Sigma", data);
                    if (data.size() != m_number_of_dimensions)
                        throw Exception("The dimensionality of the sigma vector in the XML object (%d) is different than the expected dimensionality %d.", data.size(), m_number_of_dimensions);
                    for (unsigned int i = 0; i < m_number_of_dimensions; ++i)
                        m_sigma[i] = data[i];
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif

