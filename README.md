# Semantic Robot Vision library

This library contains part of the code that I've been developing during my thesis. The code is permanently under development, it still has some bugs and it's not fully optimized. The code is just released as demonstration of the different methods or algorithms proposed in some of my publications.

The library is written in C++ and it requires the following packages:
* Figure windows: _gtk 3.0_
* Multi-threading: _OpenMP_
* Load/save images: _libpng_, _libjpeg_
* Compress XML files: _libz_, _libbz2_

The library currently contains the following applications in the folder **projects/**:

* **Basic semantic segmentation:** Basic implementation of the semantic segmentation which characterizes each pixel of the image using a local Bag-of-Visual-Words signature and applies a linear classifier to obtain the semantic labels.
* **Unsupervised segmentation using super-pixels:** Example of unsupervised segmentation using SLIC super-pixels.
* **Calibration pattern:** Implementation of the chessboard pattern detection proposed in _A. Geiger, F. Moosman, O. Car, B. Schuster, "Automatic Camera and Range Sensor Calibration using Simple Shot", ICRA 2012_.
* **Word Spotting:** Code used to compare the performance of different parameters to generate the Bag-of-Visual-Words signature in a word segmented word spotting setup.
