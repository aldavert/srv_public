// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include "srv_utilities.hpp"
#include "srv_logger.hpp"
#include "srv_image.hpp"

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                             26-July-2013\n"
                                   "=================================================================================\n"
                                   " Uses SLIC super-pixels to calculate the unsupervised segmentation of the given\n"
                                   " image.\n\n";
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int step_size, number_of_threads, method_show;
    char image_filename[4096], destination_filename[4096];
    bool perturb_seeds, convert_to_cielab, use_adaptive, no_wait;
    double compactness;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./slic", initial_message);
    
    // -[ Define use parameters ]--------------------------------------------------------------------------------------------------------------------
    parameters.addParameter(new srv::ParameterCharArray("image_filename", "input image.", image_filename));
    parameters.addParameter(new srv::ParameterUInt("--show", "index", "method used to show the segmentation results. Possible values are:"
                                                                      "&0&Segmentation boundaries."
                                                                      "&1&Region average."
                                                                      "&2&Region colors.", 0, true, 0, true, 2, &method_show));
    parameters.addParameter(new srv::ParameterUInt("--step", "pixels", "separation between seeds.", 64, true, 16, false, 0, &step_size));
    parameters.addParameter(new srv::ParameterDouble("--compact", "value", "compactness value of the SLIC algorithm.", 1.0, true, 0.0, false, 0.0, &compactness));
    parameters.addParameter(new srv::ParameterBoolean("--perturb", "", "perturb the initial location of the seeds to avoid edges.", &perturb_seeds));
    parameters.addParameter(new srv::ParameterBoolean("--cie", "", "converts the input image to CIE l*a*b before applying the SLIC algorithm.", &convert_to_cielab));
    parameters.addParameter(new srv::ParameterBoolean("--adaptive", "", "uses the Adaptive SLIC algorithm which automatically selects the compactness value.", &use_adaptive));
    parameters.addParameter(new srv::ParameterCharArray("--save", "filename", "save the segmentation image in the specified file.", destination_filename));
    parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads to concurrently process the image.", 1, true, 1, false, 0, &number_of_threads));
    parameters.addParameter(new srv::ParameterBoolean("--no-wait", "", "disables to show the segmentation result in a window.", &no_wait));
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        destination_filename[0] = '\0';
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Image segmentation parameters ]------------------------------------------------------------------------------------------------------------
    try
    {
        srv::Image<unsigned char> image;
        srv::Image<unsigned int> segmentation;
        unsigned int number_of_regions;
        
        image.load(image_filename);
        if (use_adaptive)
            number_of_regions = srv::Segmentation::AdaptiveSlic(image, step_size, perturb_seeds, convert_to_cielab, segmentation, number_of_threads);
        else number_of_regions = srv::Segmentation::Slic(image, step_size, compactness, perturb_seeds, convert_to_cielab, segmentation, number_of_threads);
        if (number_of_regions == 0)
            throw srv::Exception("0 super-pixels found. Probably the step is too small.");
        
        if (method_show == 0)
        {
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                unsigned char * image_ptrs[] = { image.get(y, 0), image.get(y, 1), image.get(y, 2) };
                unsigned int * segmentation_ptr = segmentation.get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x)
                {
                    bool boundary;
                    
                    boundary = false;
                    boundary =             ((x > 0)                     && (*segmentation_ptr != *(segmentation_ptr - 1)));
                    boundary = boundary || ((x < image.getWidth() - 1)  && (*segmentation_ptr != *(segmentation_ptr + 1)));
                    boundary = boundary || ((y > 0)                     && (*segmentation_ptr != *(segmentation_ptr - image.getWidth())));
                    boundary = boundary || ((y < image.getHeight() - 1) && (*segmentation_ptr != *(segmentation_ptr + image.getWidth())));
                    if (boundary)
                    {
                        *image_ptrs[0] = 255;
                        *image_ptrs[1] = 0;
                        *image_ptrs[2] = 0;
                    }
                    ++image_ptrs[0];
                    ++image_ptrs[1];
                    ++image_ptrs[2];
                    ++segmentation_ptr;
                }
            }
        }
        else if (method_show == 1)
        {
            srv::VectorDense<unsigned int> red(number_of_regions, 0), green(number_of_regions, 0), blue(number_of_regions, 0), counter(number_of_regions, 0);
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                const unsigned char * __restrict__ r_ptr = image.get(y, 0);
                const unsigned char * __restrict__ g_ptr = image.get(y, 1);
                const unsigned char * __restrict__ b_ptr = image.get(y, 2);
                const unsigned int * __restrict__ s_ptr = segmentation.get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x)
                {
                    red[*s_ptr] += (unsigned int)*r_ptr;
                    green[*s_ptr] += (unsigned int)*g_ptr;
                    blue[*s_ptr] += (unsigned int)*b_ptr;
                    ++counter[*s_ptr];
                    ++r_ptr;
                    ++g_ptr;
                    ++b_ptr;
                    ++s_ptr;
                }
            }
            for (unsigned int i = 0; i < number_of_regions; ++i)
            {
                if (counter[i] == 0) std::cerr << "[WARNING] Empty SLIC cluster." << std::endl; // Something has gone wrong here.
                else
                {
                    red[i] /= counter[i];
                    green[i] /= counter[i];
                    blue[i] /= counter[i];
                }
            }
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                unsigned char * __restrict__ r_ptr = image.get(y, 0);
                unsigned char * __restrict__ g_ptr = image.get(y, 1);
                unsigned char * __restrict__ b_ptr = image.get(y, 2);
                const unsigned int * __restrict__ s_ptr = segmentation.get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x)
                {
                    *r_ptr = (unsigned char)red[*s_ptr];
                    *g_ptr = (unsigned char)green[*s_ptr];
                    *b_ptr = (unsigned char)blue[*s_ptr];
                    ++counter[*s_ptr];
                    ++r_ptr;
                    ++g_ptr;
                    ++b_ptr;
                    ++s_ptr;
                }
            }
        }
        else if (method_show == 2)
        {
            const double value = 1.0;
            srv::VectorDense<unsigned char> red(number_of_regions, 0), green(number_of_regions, 0), blue(number_of_regions, 0), counter(number_of_regions, 0);
            unsigned int hue_divisions, saturation_divisions;
            double step;
            
            // Create the region colors lookup table.
            step = std::sqrt(2.0 * (double)number_of_regions);
            hue_divisions = (unsigned int)ceil(step);
            saturation_divisions = (unsigned int)srv::srvMax<double>(1.0, floor(step));
            while (hue_divisions * saturation_divisions < number_of_regions)
                ++hue_divisions;
            
            for (unsigned int i = 0; i < number_of_regions; ++i)
            {
                double hue, saturation, chroma, huep, m, R, G, B;
                unsigned int hue_index, saturation_index;
                
                hue_index = i % hue_divisions;
                saturation_index = i / hue_divisions;
                hue = (double)hue_index / (double)hue_divisions;
                saturation = 0.5 + (double)saturation_index / (double)saturation_divisions;
                
                chroma = value * saturation;
                huep = hue * 3.0; // divided by 2: 3 instead of 6.
                huep = chroma * (1.0 - srv::srvAbs<double>(2.0 * (huep - floor(huep)) - 1.0));
                if      (hue < 1.0 / 6.0) { R = chroma; G = huep;   B = 0.0;    }
                else if (hue < 2.0 / 6.0) { R = huep;   G = chroma; B = 0.0;    }
                else if (hue < 3.0 / 6.0) { R = 0.0;    G = chroma; B = huep;   }
                else if (hue < 4.0 / 6.0) { R = 0.0;    G = huep;   B = chroma; }
                else if (hue < 5.0 / 6.0) { R = huep;   G = 0.0;    B = chroma; }
                else if (hue < 6.0 / 6.0) { R = chroma; G = 0.0;    B = huep;   }
                else                      { R = 0.0;    G = 0.0;    B = 0.0;    }
                m = value - chroma;
                R += m;
                G += m;
                B += m;
                R = srv::srvMin(1.0, srv::srvMax(0.0, R));
                G = srv::srvMin(1.0, srv::srvMax(0.0, G));
                B = srv::srvMin(1.0, srv::srvMax(0.0, B));
                red[i] = (unsigned char)(255.0 * R);
                green[i] = (unsigned char)(255.0 * G);
                blue[i] = (unsigned char)(255.0 * B);
            }
            // Randomly resort the color values.
            for (unsigned int i = 0; i < number_of_regions; ++i)
            {
                const unsigned int di = i + rand() % (number_of_regions - i);
                srv::srvSwap(red[i], red[di]);
                srv::srvSwap(green[i], green[di]);
                srv::srvSwap(blue[i], blue[di]);
            }
            // Repaint the original image.
            for (unsigned int y = 0; y < image.getHeight(); ++y)
            {
                unsigned char * __restrict__ r_ptr = image.get(y, 0);
                unsigned char * __restrict__ g_ptr = image.get(y, 1);
                unsigned char * __restrict__ b_ptr = image.get(y, 2);
                const unsigned int * __restrict__ s_ptr = segmentation.get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x)
                {
                    *r_ptr = (unsigned char)red[*s_ptr];
                    *g_ptr = (unsigned char)green[*s_ptr];
                    *b_ptr = (unsigned char)blue[*s_ptr];
                    ++counter[*s_ptr];
                    ++r_ptr;
                    ++g_ptr;
                    ++b_ptr;
                    ++s_ptr;
                }
            }
        }
        else throw srv::Exception("Show method with index '%d' has not been implemented yet.");
        
        if (destination_filename[0] != '\0')
            image.save(destination_filename);
        if (!no_wait)
        {
            srv::Figure figure_original("Image");
            
            figure_original(image);
            std::cout << "Press [ESC] to exit." << std::endl;
            for (char key = 0; key != 27; key = srv::Figure::wait());
        }
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the image:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
