// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"

INITIALIZE_DENSE_DESCRIPTOR(short, short)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              18-May-2018\n"
                                   "=================================================================================\n"
                                   " Creates the codebook used to encode descriptors into visual words.\n\n";
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int sampling_method, maximum_train_samples, number_of_threads;
    char filename_database[4096], filename_codebook[4096];
    bool verbose;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./codebook_generate", initial_message);
    srv::DenseVisualWords<short, short, unsigned char, int, unsigned int> visual_words;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int codebook_index, number_of_levels, degree, number_of_trees, minimum_support, number_of_trials, random_seed, maximum_number_of_codewords;
        srv::CodebookBase<unsigned char, int, unsigned int> * codebook;
        srv::DenseDescriptor<short, short> * dense_descriptor;
        double minimum_score, descriptor_norm_threshold;
        char filename_descriptor[4096];
        bool random_seed_initialized;
        
        // -[ Define use parameters ]--------------------------------------------------------------------------------------------------------------------
        parameters.addParameter(new srv::ParameterCharArray("filename_database", "file with the training images used to create the codebook.", filename_database));
        parameters.addParameter(new srv::ParameterUInt("codebook_index", "index of the codebook used to generate the visual words. Possible values are:"
                                                                         "&0&[HKM] Hierarchical K-Means codebook."
                                                                         "&1&[ERF] Extremely Randomized Forest codebook.", true, 0, true, 1, &codebook_index));
        parameters.addParameter(new srv::ParameterCharArray("filename_descriptor", "file with the information of the used descriptors", filename_descriptor));
        parameters.addParameter(new srv::ParameterCharArray("filename_codebook", "file where the resulting codebook is stored.", filename_codebook));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Codebook parameters."));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--levels", "height", "[HKM,ERF] number of levels of the tree.", 5, true, 1, false, 0, &number_of_levels));
        parameters.addParameter(new srv::ParameterUInt("--degree", "nodes", "[HKM] degree of the hierarchical tree.", 10, true, 2, false, 0, &degree));
        parameters.addParameter(new srv::ParameterUInt("--support", "samples", "[HKM] minimum number of descriptors to further divide hierarchical tree.", 100, true, 50, false, 0, &minimum_support));
        parameters.addParameter(new srv::ParameterUInt("--max-leafs", "amount", "[HKM] Maximum number of leafs of the tree. Set to 0 to create as many leafs allowed by levels, degree and support.", 0, true, 0, false, 0, &maximum_number_of_codewords));
        parameters.addParameter(new srv::ParameterDouble("--entropy", "score", "[ERF] minimum entropy score threshold of the random forest.", 0.4, true, 0.0, false, 0.0, &minimum_score));
        parameters.addParameter(new srv::ParameterUInt("--trial", "trials", "[ERF] number of trials to select the boolean test of the random forest.", 100, true, 1, false, 0, &number_of_trials));
        parameters.addParameter(new srv::ParameterUInt("--trees", "trees", "[ERF] number of trees of the random forest.", 10, true, 1, false, 0, &number_of_trees));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Miscellaneous parameters."));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterDouble("--descriptor-thr", "value", "minimum accumulated gradient value of the descriptor to accept it as a valid descriptor.", 1000, true, 0.0, false, 0.0, &descriptor_norm_threshold));
        parameters.addParameter(new srv::ParameterBoolean("--verbose", "", "show codebook creation information.", &verbose));
        parameters.addParameter(new srv::ParameterUInt("--max-samples", "number_of_descriptors", "maximum number of descriptors sampled from the database (set to zero to extract all possible descriptors).", 0, true, 0, false, 0, &maximum_train_samples));
        parameters.addParameter(new srv::ParameterUInt("--sampling", "index", "method used to sample the local descriptors when the maximum number of descriptors is reached. Possible values are:"
                                                                              "&0&Stop gathering new descriptors."
                                                                              "&1&Random substitutions.", 1, true, 0, true, 1, &sampling_method));
        parameters.addParameter(new srv::ParameterUInt("--random", "seed", "seed of the random number generator (time is used when not set).", 0, true, 0, false, 0, &random_seed, &random_seed_initialized));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently generate the codebook.", 1, true, 1, false, 0, &number_of_threads));
        
        random_seed_initialized = false;
        parameters.parse(argv, argc);
        
        // Set the random number generator.
        if (random_seed_initialized) srand(random_seed);
        else srand((unsigned int)time(0));
        
        // Load the dense descriptor object.
        srv::loadObject(filename_descriptor, dense_descriptor);
        const unsigned int number_of_dimensions = dense_descriptor->getNumberOfBins(3);
        
        // Initialize the codebook.
        if (codebook_index == 0) // Hierarchical K-Means codebook.
        {
            srv::ClusterCenterBasedBase<unsigned char, int, int, unsigned int> * cluster;
            srv::SeederBase<unsigned char, int, int, unsigned int> * seeder;
            
            // Use k-means++ as seeder.
            seeder = new srv::SeederPlus<unsigned char, int, int, unsigned int>(srv::VectorDistance(srv::EUCLIDEAN_DISTANCE), false, 1.0);
            // Use the (FULL) fast triangle inequality k-means algorithm.
            cluster = new srv::ClusterKMeansFast<unsigned char, int, int, unsigned int>(degree, number_of_dimensions, seeder, srv::VectorDistance(srv::EUCLIDEAN_DISTANCE), 1000, 0, srv:: CLUSTER_EMPTY_RESAMPLE, false, 1024);
            codebook = new srv::CodebookHierarchicalCluster<unsigned char, int, int, unsigned int>(cluster, maximum_number_of_codewords, degree, minimum_support, number_of_levels);
            delete seeder;
            delete cluster;
        }
        else if (codebook_index == 1) // Extremely Randomized Forest codebook.
            codebook = new srv::CodebookRandomForest<unsigned char, int, unsigned int>(number_of_dimensions, minimum_score, number_of_trials, number_of_levels, number_of_trees);
        else throw srv::Exception("The selected codebook has not been implemented yet.");
        
        // Initialize visual words object.
        visual_words.setDenseDescriptors(dense_descriptor);             // Set the dense descriptors object.
        visual_words.setDescriptorThreshold(descriptor_norm_threshold); // Descriptor threshold to filter out low gradient descriptors.
        visual_words.setCodebook(codebook);                             // Set the codebook object.
        
        // Free allocated memory.
        delete codebook;
        delete dense_descriptor;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Densely sample descriptors ]---------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int total_number_of_regions, current_number_of_regions, image_counter_length;
        srv::DatabaseInformation database(filename_database);
        srv::DatabaseInformation::Subset database_iterator;
        srv::VectorDense<unsigned char> * train_descriptors;
        int * train_labels;
        char text[512], image_counter_mask[512];
        
        // 1) Initialize structures .................................................................................................................
        database.updateLabels();
        database_iterator = database.getIterator(srv::DatabaseInformation::ALL_IMAGES, srv::DatabaseInformation::TRAIN_SET);
        
        // 2) Allocate memory for the train descriptors .............................................................................................
        total_number_of_regions = 0;
        for (unsigned int index = 0; index < database_iterator.size(); ++index)
            total_number_of_regions += visual_words.getDenseDescriptors()->calculateNumberOfRegions(database_iterator[index]->getWidth(), database_iterator[index]->getHeight());
        srv::thousandSeparator(total_number_of_regions, text);
        logger.log("%s regions can be densely sampled from the training set images of the database.", text);
        if ((maximum_train_samples > 0) && (total_number_of_regions > maximum_train_samples))
            total_number_of_regions = maximum_train_samples;
        train_descriptors = new srv::VectorDense<unsigned char>[total_number_of_regions];
        train_labels = new int[total_number_of_regions];
        
        // 3) Process the database images ...........................................................................................................
        current_number_of_regions = 0;
        image_counter_length = (unsigned int)ceil(log10((double)database_iterator.size()));
        sprintf(image_counter_mask, "Extracting descriptors from image %%0%dd of %%0%dd: %%s", image_counter_length, image_counter_length);
        srv::thousandSeparator(database_iterator.size(), text);
        for (unsigned int index = 0; index < database_iterator.size(); ++index)
        {
            srv::VectorDense<unsigned char, unsigned int> * descriptors;
            srv::VectorDense<srv::DescriptorLocation> locations;
            unsigned int valid_descriptors, added_descriptors;
            srv::PixelSemanticInformation * pixel_semantics;
            srv::Image<unsigned char> original_image;
            char text1[512], text2[512];
            srv::Chronometer chrono;
            
            // Extract descriptors ..................................................................................................................
            logger.log(image_counter_mask, index + 1, database_iterator.size(), database_iterator[index]->getImageFilename());
            database_iterator.getOriginalColorImage(index, original_image);
            pixel_semantics = database_iterator.getPixelSegmentationInformation(index);
            chrono.start();
            descriptors = 0;
            visual_words.extractDescriptors(original_image, locations, descriptors, number_of_threads);
            srv::thousandSeparator(locations.size(), text);
            logger.log("%s descriptors extracted in %.3f seconds.", text, chrono.getElapsedTime() / 1000.0);
            
            // Adding the descriptors to the training pool ..........................................................................................
            valid_descriptors = 0;
            added_descriptors = 0;
            for (unsigned int k = 0; k < locations.size(); ++k)
            {
                if ((sampling_method == 0) && (current_number_of_regions >= total_number_of_regions))
                    break;
                if (descriptors[k].size() > 0)
                {
                    if (current_number_of_regions < total_number_of_regions)
                    {
                        unsigned int region_size;
                        
                        region_size = visual_words.getDenseDescriptors()->getScalesInformation(locations[k].getScale()).getRegionSize();
                        train_labels[current_number_of_regions] = pixel_semantics->get(locations[k].getX() + region_size / 2, locations[k].getY() + region_size / 2);
                        train_descriptors[current_number_of_regions] = descriptors[k];
                        ++current_number_of_regions;
                        ++added_descriptors;
                    }
                    else if (sampling_method == 1)
                    {
                        if (rand() % 2 == 0)
                        {
                            unsigned int region_size, current_index;
                            
                            current_index = rand() % current_number_of_regions;
                            region_size = visual_words.getDenseDescriptors()->getScalesInformation(locations[k].getScale()).getRegionSize();
                            train_labels[current_index] = pixel_semantics->get(locations[k].getX() + region_size / 2, locations[k].getY() + region_size / 2);
                            train_descriptors[current_index] = descriptors[k];
                            ++added_descriptors;
                        }
                    }
                    ++valid_descriptors;
                }
            }
            srv::thousandSeparator(added_descriptors, text1);
            srv::thousandSeparator(valid_descriptors, text2);
            srv::thousandSeparator(current_number_of_regions, text);
            logger.log("%s out of %s valid descriptors added to the training set of size %s.", text1, text2, text);
            delete [] descriptors;
            delete pixel_semantics;
            
            if ((sampling_method == 0) && (current_number_of_regions >= total_number_of_regions))
                break;
        }
        
        // 4) Create the codebook ...................................................................................................................
        visual_words.getCodebook()->generate(train_descriptors, train_labels, current_number_of_regions, number_of_threads, (verbose)?(&logger):0);
        
        // 5) Free allocated memory .................................................................................................................
        delete [] train_descriptors;
        delete [] train_labels;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the codebook from the database images:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save visual words object ]-----------------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Saving codebook to disk.");
        saveObject(filename_codebook, visual_words);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while storing the visual words object:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

