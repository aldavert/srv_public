// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_database_information.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"
#include "classification_tools.hpp"

INITIALIZE_LINEAR_SVM_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)
INITIALIZE_PROBABILISTIC_SCORE_FACTORIES(TCLASSIFIER, TCLASSIFIER)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                            03-March-2013\n"
                                   "=================================================================================\n"
                                   " This program test the classification performances of the given classifiers into\n"
                                   " the specified labeled vectors file.\n\n";
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char filename_test[4096], filename_classifier[4096], filename_probabilities[4096];
    unsigned int number_of_threads, score_type, samples_type, joint_score;
    bool joint_background;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./classifier_test", initial_message);
    srv::LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * linear_classifier;
    srv::ProbabilisticScoreBase<TCLASSIFIER, double> * probability_model;
    srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> test_samples;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        // Define use parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_classifier", "file containing the classifiers which are going to be tested.", filename_classifier));
        parameters.addParameter(new srv::ParameterCharArray("filename_test", "file containing the labeled vectors used to evaluate the classifier.", filename_test));
        parameters.addParameter(new srv::ParameterCharArray("--probability", "filename", "file containing the probabilistic model which converts scores into probabilities.", filename_probabilities));
        parameters.addParameter(new srv::ParameterUInt("--score", "index", "index of the method used to evaluate the classifiers. Possible values are:"
                                                                    "&0&mean Average Precision."
                                                                    "&1&Best accuracy."
                                                                    "&2&Precision-Recall at Equal Error Rate"
                                                                    "&3&Best F-Measure", 0, true, 0, true, 3, &score_type));
        parameters.addParameter(new srv::ParameterUInt("--type", "index", "index of the type of samples evaluated. Possible values are:"
                                                                    "&0&All classifiers are evaluated independently."
                                                                    "&1&Joint evaluation (each sample can only belong to a single category).", 0, true, 0, true, 1, &samples_type));
        parameters.addParameter(new srv::ParameterBoolean("--joint-background", "", "enables the background category for jointly evaluated classifiers.", &joint_background));
        parameters.addParameter(new srv::ParameterUInt("--joint-score", "index", "method used to calculate the score in jointly evaluated classifiers. Possible values are:"
                                                                    "&0&Maximum score."
                                                                    "&1&Ratio between the two highest scores.", 0, true, 0, true, 1, &joint_score));
        parameters.addParameter(new srv::ParameterUInt("--thr", "number_of_threads", "number of threads used to concurrently calculate the classifier scores for each sample.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parse user parameters.
        filename_probabilities[0] = '\0';
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load objects from disk ]-------------------------------------------------------------------------------------------------------------------
    try
    {
        std::map<NLABEL, bool> category_labels;
        srv::VectorDense<NLABEL> sample_labels;
        
        logger.log("Loading objects from disk.");
        loadObject(filename_test, test_samples);
        loadObject(filename_classifier, linear_classifier);
        if (filename_probabilities[0] != '\0')
            loadObject(filename_probabilities, probability_model);
        else probability_model = 0;
        
        test_samples.categoryIdentifiers(sample_labels);
        if (test_samples.getNumberOfDimensions() != linear_classifier->getNumberOfDimensions())
            throw srv::Exception("The dimensionality of the samples (%d) is different than the dimensionality of the classifier (%d)", test_samples.getNumberOfDimensions(), linear_classifier->getNumberOfDimensions());
        
        for (unsigned int i = 0; i < linear_classifier->getNumberOfCategories(); ++i)
            category_labels[linear_classifier->getCategoryIdentifier(i)] = false;
        test_samples.categoryIdentifiers(sample_labels);
        for (unsigned int i = 0; i < sample_labels.size(); ++i)
        {
            std::map<NLABEL, bool>::iterator search = category_labels.find(sample_labels[i]);
            if (search != category_labels.end())
                search->second = true;
        }
        for (std::map<NLABEL, bool>::iterator begin = category_labels.begin(), end = category_labels.end(); begin != end; ++begin)
            if (begin->second == false)
                throw srv::Exception("Any training example for the classifier of the category with identifier '%d'.", begin->first);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading objects from disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Evaluate classifiers ]---------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::VectorDense<unsigned int, unsigned int> * labels;
        srv::VectorDense<TCLASSIFIER> * scores;
        srv::Chronometer chrono;
        std::map<NLABEL, unsigned int> label_to_classifier;
        
        logger.log("Generating the scores...");
        scores = new srv::VectorDense<TCLASSIFIER>[test_samples.getNumberOfSamples()];
        labels = new srv::VectorDense<unsigned int, unsigned int>[test_samples.getNumberOfSamples()];
        for (unsigned int i = 0; i < linear_classifier->getNumberOfCategories(); ++i)
            label_to_classifier[linear_classifier->getCategoryIdentifier(i)] = i;
        for (unsigned int i = 0; i < test_samples.getNumberOfSamples(); ++i)
        {
            std::list<unsigned int> valid_classifiers;
            unsigned int index;
            
            for (NLABEL j = 0; j < test_samples[i].getLabels().size(); ++j)
            {
                std::map<NLABEL, unsigned int>::iterator search;
                
                search = label_to_classifier.find(test_samples[i].getLabels().getIndex(j));
                if (search != label_to_classifier.end())
                    valid_classifiers.push_back(search->second);
            }
            labels[i].set((unsigned int)valid_classifiers.size());
            index = 0;
            for (std::list<unsigned int>::iterator begin = valid_classifiers.begin(), end = valid_classifiers.end(); begin != end; ++begin, ++index)
                labels[i][index] = *begin;
        }
        chrono.start();
        linear_classifier->predict(test_samples.getSamples(), scores, test_samples.getNumberOfSamples(), number_of_threads);
        logger.log("The scores of the %d samples have been generated in %f ms.", test_samples.getNumberOfSamples(), chrono.getElapsedTime());
        
        if (probability_model != 0)
        {
            srv::VectorDense<double> * probabilities;
            
            probabilities = new srv::VectorDense<TCLASSIFIER>[test_samples.getNumberOfSamples()];
            probability_model->probabilities(scores, probabilities, test_samples.getNumberOfSamples(), number_of_threads);
            srv::srvSwap(scores, probabilities);
            delete [] probabilities;
            delete probability_model;
        }
        
        if (samples_type == 0) // CLASSIFIERS EVALUATED INDIVIDUALLY ================================================================================
        {
            const unsigned int number_of_samples = test_samples.getNumberOfSamples();
            srv::VectorDense<srv::Tuple<TCLASSIFIER, bool> > sorted_output(number_of_samples);
            srv::VectorDense<TCLASSIFIER> precision(number_of_samples);
            srv::VectorDense<TCLASSIFIER> recall_sensitivity(number_of_samples);
            srv::VectorDense<TCLASSIFIER> specificity_fallout(number_of_samples);
            srv::VectorDense<TCLASSIFIER> accuracy(number_of_samples);
            
            for (unsigned int i = 0; i < linear_classifier->getNumberOfCategories(); ++i)
            {
                for (unsigned int j = 0; j < number_of_samples; ++j)
                {
                    bool current_positive;
                    
                    current_positive = false;
                    for (unsigned int k = 0; k < labels[j].size(); ++k)
                        if (labels[j][k] == i)
                            current_positive = true;
                    
                    sorted_output[j].setData(scores[j][i], current_positive);
                }
                std::sort(&sorted_output[0], &sorted_output[number_of_samples]);
                
                if (score_type == 0)
                {
                    TCLASSIFIER mAP;
                    unsigned int correct, total;
                    
                    mAP = 0;
                    correct = total = 0;
                    for (unsigned int j = 0; j < number_of_samples; ++j)
                    {
                        if (sorted_output[number_of_samples - j - 1].getSecond())
                        {
                            mAP += (TCLASSIFIER)(correct + 1) / (TCLASSIFIER)(total + 1);
                            ++correct;
                        }
                        ++total;
                    }
                    if (i > 0) std::cout << " ";
                    if (correct > 0) std::cout << mAP / (TCLASSIFIER)correct;
                    else std::cout << "0";
                }
                else
                {
                    unsigned int current_positive, current_negative, total_positive, total_negative;
                    
                    total_positive = total_negative = 0;
                    for (unsigned int j = 0; j < number_of_samples; ++j)
                    {
                        if (sorted_output[j].getSecond()) ++total_positive;
                        else ++total_negative;
                    }
                    
                    current_negative = current_positive = 0;
                    for (unsigned int j = 0; j < number_of_samples; ++j)
                    {
                        // current_negative -> false positive.
                        // current_positive -> true positive
                        if (sorted_output[number_of_samples - j - 1].getSecond()) ++current_positive;
                        else ++current_negative;
                        
                        // Positive Predictive Value or Precision =
                        //                       = [True Positive] / ([True Positive] + [False Positive])
                        precision[j] = (TCLASSIFIER)current_positive / (TCLASSIFIER)(current_positive + current_negative);
                        // Recall, Sensitivity, True Positive Rate, Hit Rate =
                        //                       = [True Positive] / ([True Positive] + [False Negative])
                        recall_sensitivity[j] = (TCLASSIFIER)current_positive / (TCLASSIFIER)total_positive;
                        // Specificity, Fall-out = [True Negative] / ([False Positive] + [True Negative])
                        specificity_fallout[j] = (TCLASSIFIER)(total_negative - current_negative) / (TCLASSIFIER)(total_negative);
                        // Accuracy = ([True Positive] + [True Negative]) / ([True Positive] + [True Negative] + [False Positive] + [False Negative])
                        //          = ([True Positive] + [True Negative]) / [All samples]
                        accuracy[j] = (TCLASSIFIER)(current_positive + total_negative - current_negative) / (TCLASSIFIER)number_of_samples;
                    }
                    
                    if (i > 0) std::cout << " ";
                    if (score_type == 1) // Best accuracy.
                    {
                        TCLASSIFIER max_accuracy;
                        
                        max_accuracy = accuracy[0];
                        for (unsigned int j = 1; j < number_of_samples; ++j)
                            if (accuracy[j] > max_accuracy)
                                max_accuracy = accuracy[j];
                        std::cout << max_accuracy;
                    }
                    else if (score_type == 2) // Precision-recall at equal error rate.
                    {
                        TCLASSIFIER equal_error_rate, difference;
                        
                        difference = 100;
                        equal_error_rate = 0;
                        for (unsigned int j = 0; j < number_of_samples; ++j)
                        {
                            TCLASSIFIER current_difference;
                            
                            if ((precision[j] > 0.0) && (recall_sensitivity[j] > 0.0))
                            {
                                current_difference = srv::srvAbs<TCLASSIFIER>(precision[j] - recall_sensitivity[j]);
                                if (current_difference < difference)
                                {
                                    difference = current_difference;
                                    equal_error_rate = srv::srvMin<TCLASSIFIER>(precision[j], recall_sensitivity[j]);
                                }
                            }
                        }
                        std::cout << equal_error_rate;
                    }
                    else if (score_type == 3) // Best f-Measure.
                    {
                        TCLASSIFIER f_measure;
                        
                        f_measure = 0.0;
                        for (unsigned int j = 0; j < number_of_samples; ++j)
                        {
                            if ((precision[j] > 0.0) && (recall_sensitivity[j] > 0.0))
                            {
                                TCLASSIFIER current_f_measure;
                                
                                current_f_measure = 2.0 * precision[j] * recall_sensitivity[j] / (precision[j] + recall_sensitivity[j]);
                                if (current_f_measure > f_measure)
                                    f_measure = current_f_measure;
                            }
                        }
                        std::cout << f_measure;
                    }
                }
            }
            std::cout << std::endl;
        }
        else // MULTI-CLASS CLASSIFIERS WHERE ALL CLASSIFIERS ARE EVALUATED TOGETHER ================================================================
        {
            const unsigned int number_of_samples = test_samples.getNumberOfSamples();
            srv::VectorDense<srv::Triplet<TCLASSIFIER, unsigned int, unsigned int> > sorted_output(number_of_samples);
            srv::VectorDense<unsigned int> samples_per_category(linear_classifier->getNumberOfCategories() + 1, 0);
            
            for (unsigned int j = 0; j < number_of_samples; ++j)
            {
                unsigned int assigned_category;
                TCLASSIFIER maximum_score, second_maximum_score;
                unsigned int actual_category;
                
                // The jointly evaluated classifiers only support a single category per label.
                // In this mode, background samples (i.e. all samples which are not assigned to any
                // category of the evaluated linear classifiers) are assigned to the single category
                // labeled as N, where N is the number of linear classifiers.
                if (labels[j].size() == 0) actual_category = linear_classifier->getNumberOfCategories();
                else actual_category = labels[j][0];
                ++samples_per_category[actual_category];
                
                second_maximum_score = 0;
                maximum_score = scores[j][0];
                assigned_category = 0;
                for (unsigned int i = 1; i < linear_classifier->getNumberOfCategories(); ++i)
                {
                    if (scores[j][i] > maximum_score)
                    {
                        second_maximum_score = maximum_score;
                        maximum_score = scores[j][i];
                        assigned_category = i;
                    }
                    else if (scores[j][i] > second_maximum_score)
                        second_maximum_score = scores[j][i];
                }
                if (joint_score == 0)
                    sorted_output[j].setData(maximum_score, actual_category, assigned_category);
                else if (joint_score == 1)
                {
                    if (maximum_score > 0.0) sorted_output[j].setData(second_maximum_score / maximum_score, actual_category, assigned_category);
                    else sorted_output[j].setData(0.0, actual_category, assigned_category);
                }
            }
            std::sort(&sorted_output[0], &sorted_output[number_of_samples]);
            
            if (score_type == 0) // mean Average Precision ==========================================================================================
            {
                srv::VectorDense<unsigned int> correct(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0);
                srv::VectorDense<TCLASSIFIER> mAP(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0.0);
                const unsigned int number_of_categories = linear_classifier->getNumberOfCategories();
                
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    const unsigned int index = number_of_samples - i - 1;
                    
                    if (sorted_output[index].getSecond() == sorted_output[index].getThird()) // if [actual category] == [assigned category] then, ...
                    {
                        const unsigned int selected = sorted_output[index].getThird();
                        ++correct[selected];
                        mAP[selected] += (TCLASSIFIER)correct[selected] / (TCLASSIFIER)(i + 1);
                    }
                }
                if (joint_background) // Background samples are run the other way around from end to head.
                {
                    for (unsigned int i = 0; i < number_of_samples; ++i)
                    {
                        if (sorted_output[i].getSecond() == number_of_categories)
                        {
                            ++correct[number_of_categories];
                            mAP[number_of_categories] += (TCLASSIFIER)correct[number_of_categories] / (TCLASSIFIER)(i + 1);
                        }
                    }
                }
                for (unsigned int i = 0; i < mAP.size(); ++i)
                    mAP[i] = (correct[i] > 0)?(mAP[i] / (TCLASSIFIER)correct[i]):0.0;
                std::cout << mAP << std::endl;
                if (correct.size() > 0)
                {
                    std::cout << (TCLASSIFIER)correct[0] / (TCLASSIFIER)samples_per_category[0];
                    for (unsigned int i = 1; i < correct.size(); ++i)
                        std::cout << " " << (TCLASSIFIER)correct[i] / (TCLASSIFIER)samples_per_category[i];
                    std::cout << std::endl;
                }
            }
            else
            {
                // Accuracy = ([True Positive] + [True Negative]) / [All samples]
                srv::VectorDense<unsigned int> total_true_positive(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0);
                srv::VectorDense<unsigned int> total_false_positive(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0);
                srv::VectorDense<TCLASSIFIER> classifier_score(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0.0);
                srv::VectorDense<TCLASSIFIER> current_classifier_score(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 0.0);
                srv::VectorDense<TCLASSIFIER> auxiliary_score(linear_classifier->getNumberOfCategories() + ((joint_background)?1:0), 1000.0);
                const unsigned int number_of_categories = linear_classifier->getNumberOfCategories();
                TCLASSIFIER average_classifier_score, best_classifier_score;
                
                best_classifier_score = 0;
                if (joint_background)
                {
                    for (unsigned int i = 0; i < number_of_samples; ++i)
                    {
                        if (sorted_output[i].getSecond() == number_of_categories)
                            ++total_true_positive[number_of_categories];
                        else ++total_false_positive[number_of_categories];
                    }
                }
                for (unsigned int i = 0; i < number_of_samples; ++i)
                {
                    const unsigned int index = number_of_samples - i - 1;
                    
                    if (sorted_output[index].getSecond() == sorted_output[index].getThird()) // if [actual category] == [assigned category] then, ...
                        ++total_true_positive[sorted_output[index].getThird()];
                    else ++total_false_positive[sorted_output[index].getThird()];
                    
                    if (joint_background)
                    {
                        if (sorted_output[index].getSecond() == number_of_categories)
                            --total_true_positive[number_of_categories];
                        else --total_false_positive[number_of_categories];
                    }
                    
                    if (score_type == 1) // = Best accuracy =========================================================================================
                    {
                        // ACCURACY = ([True Positive] + [True Negative]) / [All samples]
                        for (unsigned int k = 0; k < total_true_positive.size(); ++k)
                        {
                            unsigned int total_negative;
                            TCLASSIFIER accuracy;
                            
                            if (joint_background) // Use Pascal Measure for accuracy => Precision!
                                accuracy = (TCLASSIFIER)total_true_positive[k] / (TCLASSIFIER)samples_per_category[k];
                            else
                            {
                                total_negative = number_of_samples - samples_per_category[k];
                                accuracy = (TCLASSIFIER)(total_true_positive[k] + total_negative - total_false_positive[k]) / (TCLASSIFIER)number_of_samples;
                            }
                            
                            current_classifier_score[k] = accuracy;
                        }
                    }
                    else if (score_type == 2) // = Precision-recall at equal error rate =============================================================
                    {
                        // SCORE WHICH WHERE PRECISION EQUALS RECALL
                        for (unsigned int k = 0; k < total_true_positive.size(); ++k)
                        {
                            TCLASSIFIER precision, recall;
                            
                            precision = (TCLASSIFIER)total_true_positive[k] / (TCLASSIFIER)(total_true_positive[k] + total_false_positive[k]);
                            recall = (TCLASSIFIER)total_true_positive[k] / (TCLASSIFIER)samples_per_category[k];
                            if ((precision > 0.0) && (recall > 0.0))
                            {
                                TCLASSIFIER difference;
                                
                                difference = srv::srvAbs<TCLASSIFIER>(precision - recall);
                                if (difference < auxiliary_score[k])
                                {
                                    auxiliary_score[k] = difference;
                                    classifier_score[k] = srv::srvMin<TCLASSIFIER>(precision, recall);
                                }
                            }
                        }
                    }
                    else if (score_type == 3) // = Best f-measure ===================================================================================
                    {
                        // F-MEASURE = 2 * [PRECISION] * [RECALL] / ([PRECISION] + [RECALL])
                        for (unsigned int k = 0; k < total_true_positive.size(); ++k)
                        {
                            TCLASSIFIER precision, recall, fmeasure;
                            
                            precision = (TCLASSIFIER)total_true_positive[k] / (TCLASSIFIER)(total_true_positive[k] + total_false_positive[k]);
                            recall = (TCLASSIFIER)total_true_positive[k] / (TCLASSIFIER)samples_per_category[k];
                            if ((precision > 0.0) && (recall > 0.0))
                            {
                                fmeasure = 2.0 * precision * recall / (precision + recall);
                                current_classifier_score[k] = fmeasure;
                            }
                            else current_classifier_score[k] = 0.0;
                        }
                    }
                    
                    if (score_type != 2) // [Precision-recall at equal error rate] measure is not defined as a global measure for all classifiers.
                    {
                        average_classifier_score = 0.0;
                        for (unsigned int k = 0; k < total_true_positive.size(); ++k)
                            average_classifier_score += current_classifier_score[k];
                        average_classifier_score /= (TCLASSIFIER)total_true_positive.size();
                        
                        if (average_classifier_score > best_classifier_score)
                        {
                            best_classifier_score = average_classifier_score;
                            classifier_score.copy(current_classifier_score);
                        }
                    }
                }
                
                std::cout << classifier_score << std::endl;
            }
        }
        
        delete [] labels;
        delete [] scores;
        delete linear_classifier;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while evaluating the classifiers:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
   
    return EXIT_SUCCESS;
}

