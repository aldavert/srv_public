// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              17-May-2018\n"
                                   "=================================================================================\n"
                                   " Creates a XML object with the information of the descriptors used to extract\n"
                                   " information from the images.\n\n";
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int descriptor_index, ihog_number_of_orientation_bins, ihog_partitions_x, ihog_partitions_y, upright_spatial_levels, upright_spatial_initial_x, upright_spatial_initial_y;
    unsigned int upright_spatial_degree_x, upright_spatial_degree_y;
    double sigma_ratio, upright_cutoff, upright_power, upright_weight_sigma;
    bool upright_spatial_interpolation, upright_weight_gaussian;
    char filename_descriptor[4096];
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./dense_descriptor", initial_message);
    srv::VectorSparse<unsigned int, unsigned int> scales_information;
    srv::DDESCRIPTOR_GAUSSIAN_KERNEL upright_gaussian_kernel_method;
    srv::ImageFeatureBase<short, short> * * upright_features;
    srv::DenseDescriptor<short, short> * dense_descriptor;
    srv::DDESCRIPTOR_SPATIAL upright_descriptor_spatial;
    srv::VectorNorm norm_spatial, norm_descriptor;
    unsigned int upright_number_of_features;
    srv::DDESCRIPTOR_COLOR color_method;
    
    // -[ Parse use parameters ]---------------------------------------------------------------------------------------------------------------------
    try
    {
        char descriptor_scales_text[1024], descriptor_step_text[1024], upright_feature_list[1024], upright_derivatives_features[128], upright_derivatives_features_rotated[128];
        unsigned int color_index, upright_gaussian_kernel_index, upright_norm_spatial_index, upright_norm_descriptor_index, upright_number_of_orientations, upright_spatial_index;
        double upright_norm_spatial_p, upright_norm_descriptor_p;
        bool upright_orientation_interpolation;
        
        parameters.addParameter(new srv::ParameterUInt("descriptor_id", "type of descriptor extracted from the image. Possible values are:"
                                                                        "&0&Upright descriptor."
                                                                        "&1&Integral Histogram of Oriented Gradients.", true, 0, true, 1, &descriptor_index));
        parameters.addParameter(new srv::ParameterCharArray("filename", "file where the descriptor XML object is stored.", filename_descriptor));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Common parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterCharArray("--size", "list", "list of the size of the local regions at different scales (e.g. use '--size 16:24:32' to extract descriptors from regions of 16, 24 and 32 pixels wide). By default descriptors are extracted from regions of 16, 24, 32 and 40 pixels wide.", descriptor_scales_text));
        parameters.addParameter(new srv::ParameterCharArray("--step", "list", "list with the separation between sampled regions at each scale (e.g. use '--step 4:5:6' to sample region each 4 pixels at the 1st scale, 5 pixels at the 2on and 6 pixels at the 3rd). When a single value is set, it is applied to all scales. By default the step is set to 4.", descriptor_step_text));
        parameters.addParameter(new srv::ParameterDouble("--sigma", "ratio", "factor applied to the region size to obtain the sigma of the filter used to extract the image features.", 0.05, true, 0, true, 1.0, &sigma_ratio));
        parameters.addParameter(new srv::ParameterUInt("--color", "index", "method used to process color information. Possible values are:"
                                                                           "&0&[GRAYSCALE] Convert the image to gray-scale."
                                                                           "&1&[LUMINANCE] Convert the image to luminance."
                                                                           "&2&[MAXIMUM] Process each channel independently and select the channel with the largest descriptor filter response."
                                                                           "&3&[RGB] Convert the image to RGB, process the 3 channels independently and concatenate the descriptors obtained at each channel."
                                                                           "&4&[OPPONENT] Same as RGB but converting the image to the opponent color space."
                                                                           "&5&[NORMALIZED OPPONENT] Same as RGB but converting the image to the opponent color space."
                                                                           "&6&[INDEPENDENT] Process all the channels of the image independently and concatenate the channel-descriptors to obtain the final descriptor (for images with more than 3 channels).", 0, true, 0, true, 6, &color_index));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Upright descriptors:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--ud-gaussian", "index", "method to compute the Gaussian filter of the image. Possible values are:"
                                                                                 "&0&Standard Gaussian convolution."
                                                                                 "&1&Deriche's recursive filter."
                                                                                 "&2&3rd order Vliet's recursive filter."
                                                                                 "&3&4rd order Vliet's recursive filter."
                                                                                 "&4&5rd order Vliet's recursive filter.", 3, true, 0, true, 4, &upright_gaussian_kernel_index));
        parameters.addParameter(new srv::ParameterCharArray("--ud-features", "list", "list of features extracted from the image separated by ':' (e.g. --features 0:3). Possible values are:"
                                                                                     "&0&Gradient Module."
                                                                                     "&1&Gradient Orientation."
                                                                                     "&2&Gradient."
                                                                                     "&3&Derivatives."
                                                                                     "&4&45-degrees rotated derivatives."
                                                                                     "&5&Line orientation.", upright_feature_list));
        parameters.addParameter(new srv::ParameterCharArray("--ud-derivatives", "code", "code with the selected derivative image features [dX, dY, dXdY, dX2, dY2, dX2dY, dXdY2, dX2dY2] (e.g. use '--derivatives 00111000' to extract dX2, dY2 and dXdY).", upright_derivatives_features));
        parameters.addParameter(new srv::ParameterCharArray("--ud-derivatives-rotated", "code", "code with the selected rotated derivative image features [dX, dY, dXdY, dX2, dY2, dX2dY, dXdY2, dX2dY2] (e.g. use '--derivatives-rotated 00111000' to extract dX2, dY2 and dXdY).", upright_derivatives_features_rotated));
        parameters.addParameter(new srv::ParameterUInt("--ud-orientation", "bins", "Number of orientations of the feature bins (gradient and line orientation).", 8, true, 1, false, 0, &upright_number_of_orientations));
        parameters.addParameter(new srv::ParameterBoolean("--ud-orientation-interpolation", "", "applies interpolation to the orientation features.", &upright_orientation_interpolation));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial", "index", "Method used to create the spatial bins. Possible values are:"
                                                                                "&0&Cartesian."
                                                                                "&1&Polar.", 0, true, 0, true, 1, &upright_spatial_index));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial-level", "levels", "Number of levels of the spatial pyramid.", 1, true, 1, false, 0, &upright_spatial_levels));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial-initial-x", "partitions", "Initial spatial partitions in the X-direction.", 4, true, 1, false, 0, &upright_spatial_initial_x));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial-initial-y", "partitions", "Initial spatial partitions in the Y-direction.", 4, true, 1, false, 0, &upright_spatial_initial_y));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial-degree-x", "degree", "Growth factor of the partitions in the X-direction.", 2, true, 1, false, 0, &upright_spatial_degree_x));
        parameters.addParameter(new srv::ParameterUInt("--ud-spatial-degree-y", "degree", "Growth factor of the partitions in the Y-direction.", 2, true, 1, false, 0, &upright_spatial_degree_y));
        parameters.addParameter(new srv::ParameterBoolean("--ud-spatial-interpolation", "", "enable the use of spatial interpolation.", &upright_spatial_interpolation));
        parameters.addParameter(new srv::ParameterBoolean("--ud-spatial-gaussian", "", "enable the use of spatial Gaussian weighting of the features.", &upright_weight_gaussian));
        parameters.addParameter(new srv::ParameterDouble("--ud-spatial-gaussian-weight", "sigma", "Sigma of the spatial Gaussian weighting function.", 0.5, true, 0.0, false, 0.0, &upright_weight_sigma));
        parameters.addParameter(new srv::ParameterUInt("--ud-norm-spatial", "index", "Normalization method applied to each spatial level of the descriptor. Possible values are:"
                                                                                     "&0&Manhattan norm."
                                                                                     "&1&Euclidean norm."
                                                                                     "&2&Maximum norm."
                                                                                     "&3&p-Norm.", 0, true, 0, true, 2, &upright_norm_spatial_index));
        parameters.addParameter(new srv::ParameterDouble("--ud-norm-spatial-p", "value", "Power factor of the generic spatial p-Norm.", 2.0, true, 0.0, false, 0.0, &upright_norm_spatial_p));
        parameters.addParameter(new srv::ParameterUInt("--ud-norm-descriptor", "index", "Normalization method applied to the whole descriptor. Possible values are:"
                                                                                     "&0&Manhattan_norm."
                                                                                     "&1&Euclidean norm."
                                                                                     "&2&Maximum norm."
                                                                                     "&3&p-Norm.", 1, true, 0, true, 3, &upright_norm_descriptor_index));
        parameters.addParameter(new srv::ParameterDouble("--ud-norm-descriptor-p", "value", "Power factor of the generic descriptor p-Norm.", 2.0, true, 0.0, false, 0.0, &upright_norm_descriptor_p));
        parameters.addParameter(new srv::ParameterDouble("--ud-norm-cutoff", "value", "Cutoff value applied to the limit the maximum normalized response of the descriptor bins.", 1.0, true, 0.0, true, 1.0, &upright_cutoff));
        parameters.addParameter(new srv::ParameterDouble("--ud-norm-power", "factor", "Power-factor applied to the normalized descriptor.", 1.0, true, 0.0, true, 1.0, &upright_power));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Integral of Histogram Oriented Gradients (iHOG):"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--ihog-orientations", "bins", "number of orientation bins.", 8, true, 1, false, 0, &ihog_number_of_orientation_bins));
        parameters.addParameter(new srv::ParameterUInt("--ihog-partition-x", "bins", "number of horizontal partitions of the spatial grid.", 4, true, 1, false, 0, &ihog_partitions_x));
        parameters.addParameter(new srv::ParameterUInt("--ihog-partition-y", "bins", "number of vertical partitions of the spatial grid.", 4, true, 1, false, 0, &ihog_partitions_y));
        
        descriptor_scales_text[0] = descriptor_step_text[0] = upright_feature_list[0] = upright_derivatives_features[0] = upright_derivatives_features_rotated[0] = '\0';
        parameters.parse(argv, argc);
        
        if (descriptor_scales_text[0] == '\0')
        {
            scales_information.set(4);
            scales_information[0].setFirst(16);
            scales_information[1].setFirst(24);
            scales_information[2].setFirst(32);
            scales_information[3].setFirst(40);
        }
        else
        {
            std::list<int> list_scales;
            unsigned int idx;
            
            srv::text2list<int>(descriptor_scales_text, 1, std::numeric_limits<int>::max(), list_scales);
            
            if (list_scales.size() == 0) throw srv::Exception("The '--size' parameter does not contain any valid region value size.");
            scales_information.set((unsigned int)list_scales.size());
            idx = 0;
            for (const auto &v : list_scales)
                scales_information[idx++].setFirst(v);
        }
        
        if (descriptor_step_text[0] == '\0')
        {
            for (unsigned int i = 0; i < scales_information.size(); ++i)
                scales_information[i].setSecond(4);
        }
        else
        {
            std::list<int> list_step;
            
            srv::text2list<int>(descriptor_step_text, 1, std::numeric_limits<int>::max(), list_step);
            if (list_step.size() == 0) throw srv::Exception("The '--step' parameter does not contain any valid region step value.");
            else if (list_step.size() == 1)
            {
                for (unsigned int i = 0; i < scales_information.size(); ++i)
                    scales_information[i].setSecond(list_step.front());
            }
            else if ((unsigned int)list_step.size() == scales_information.size())
            {
                unsigned int idx;
                
                idx = 0;
                for (const auto &v : list_step)
                    scales_information[idx++].setSecond(v);
            }
            else throw srv::Exception("The size of the region size (--size parameter) and region step (--step parameter) lists does not match.");
        }
        if      (color_index == 0) color_method = srv::DD_GRAYSCALE;
        else if (color_index == 1) color_method = srv::DD_LUMINANCE;
        else if (color_index == 2) color_method = srv::DD_MAXIMUM_CHANNEL;
        else if (color_index == 3) color_method = srv::DD_COLOR_RGB;
        else if (color_index == 4) color_method = srv::DD_COLOR_OPPONENT;
        else if (color_index == 5) color_method = srv::DD_COLOR_OPPONENT_NORMALIZED;
        else if (color_index == 6) color_method = srv::DD_INDEPENDENT_CHANNELS;
        else throw srv::Exception("The selected color method (with index '%d') is not implemented yet.", color_index);
        if      (upright_gaussian_kernel_index == 0) upright_gaussian_kernel_method = srv::DD_STANDARD_GAUSSIAN;
        else if (upright_gaussian_kernel_index == 1) upright_gaussian_kernel_method = srv::DD_DERICHE_RECURSIVE;
        else if (upright_gaussian_kernel_index == 2) upright_gaussian_kernel_method = srv::DD_VLIET_3RD_RECURSIVE;
        else if (upright_gaussian_kernel_index == 3) upright_gaussian_kernel_method = srv::DD_VLIET_4TH_RECURSIVE;
        else if (upright_gaussian_kernel_index == 4) upright_gaussian_kernel_method = srv::DD_VLIET_5TH_RECURSIVE;
        else throw srv::Exception("The selected Gaussian kernel method (with index '%d') is not implemented yet.", upright_gaussian_kernel_index);
        
        if (upright_feature_list[0] != '\0')
        {
            std::list<int> upright_feature_index_list;
            unsigned int idx;
            
            srv::text2list<int>(upright_feature_list, 0, 5, upright_feature_index_list);
            if (upright_feature_index_list.size() == 0)
                throw srv::Exception("No valid feature has been selected for the upright descriptor.");
            
            upright_number_of_features = (unsigned int)upright_feature_index_list.size();
            upright_features = new srv::ImageFeatureBase<short, short> * [(unsigned int)upright_feature_index_list.size()];
            idx = 0;
            for (const auto &value : upright_feature_index_list)
            {
                if      (value == 0) upright_features[idx] = new srv::GradientModuleImageFeature<short, short>();
                else if (value == 1) upright_features[idx] = new srv::GradientOrientationImageFeature<short, short>(upright_number_of_orientations, upright_orientation_interpolation);
                else if (value == 2) upright_features[idx] = new srv::GradientImageFeature<short, short>();
                else if (value == 3)
                {
                    bool enable[8];
                    
                    for (unsigned int i = 0; i < 8; ++i)
                    {
                        if      (upright_derivatives_features[i] == '0') enable[i] = false;
                        else if (upright_derivatives_features[i] == '1') enable[i] = true;
                        else throw srv::Exception("The derivative features vector is not properly formatted.");
                    }
                    if (upright_derivatives_features[8] != '\0') throw srv::Exception("The derivative features vector is not properly formatted.");
                    upright_features[idx] = new srv::DerivativeImageFeature<short, short>(enable[0], enable[1], enable[2], enable[3], enable[4], enable[5], enable[6], enable[7]);
                }
                else if (value == 4)
                {
                    bool enable[8];
                    
                    for (unsigned int i = 0; i < 8; ++i)
                    {
                        if      (upright_derivatives_features_rotated[i] == '0') enable[i] = false;
                        else if (upright_derivatives_features_rotated[i] == '1') enable[i] = true;
                        else throw srv::Exception("The derivative features vector is not properly formatted.");
                    }
                    if (upright_derivatives_features_rotated[8] != '\0') throw srv::Exception("The rotated derivative features vector is not properly formatted.");
                    upright_features[idx] = new srv::RotatedDerivativeImageFeature<short, short>(enable[0], enable[1], enable[2], enable[3], enable[4], enable[5], enable[6], enable[7]);
                }
                else if (value == 5) upright_features[idx] = new srv::LineImageFeature<short, short>(upright_number_of_orientations, upright_orientation_interpolation);
                else throw srv::Exception("The selected feature method has not been implemented yet.");
                ++idx;
            }
        }
        else
        {
            upright_features = 0;
            upright_number_of_features = 0;
        }
        if      (upright_spatial_index == 0) upright_descriptor_spatial = srv::DD_GRID_CARTESIAN;
        else if (upright_spatial_index == 1) upright_descriptor_spatial = srv::DD_GRID_POLAR;
        else throw srv::Exception("The selected upright spatial method '%d' is not implemented yet.", upright_spatial_index);
        
        if      (upright_norm_spatial_index == 0) norm_spatial.set(srv::MANHATTAN_NORM, upright_norm_spatial_p);
        else if (upright_norm_spatial_index == 1) norm_spatial.set(srv::EUCLIDEAN_NORM, upright_norm_spatial_p);
        else if (upright_norm_spatial_index == 2) norm_spatial.set(srv::MAXIMUM_NORM  , upright_norm_spatial_p);
        else if (upright_norm_spatial_index == 3) norm_spatial.set(srv::P_NORM        , upright_norm_spatial_p);
        else throw srv::Exception("The selected norm spatial method (%d) is not implemented yet.", upright_norm_spatial_index);
        if      (upright_norm_descriptor_index == 0) norm_descriptor.set(srv::MANHATTAN_NORM, upright_norm_descriptor_p);
        else if (upright_norm_descriptor_index == 1) norm_descriptor.set(srv::EUCLIDEAN_NORM, upright_norm_descriptor_p);
        else if (upright_norm_descriptor_index == 2) norm_descriptor.set(srv::MAXIMUM_NORM  , upright_norm_descriptor_p);
        else if (upright_norm_descriptor_index == 3) norm_descriptor.set(srv::P_NORM        , upright_norm_descriptor_p);
        else throw srv::Exception("The selected norm spatial method (%d) is not implemented yet.", upright_norm_descriptor_index);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Create the descriptor object ]-------------------------------------------------------------------------------------------------------------
    try
    {
        if      (descriptor_index == 0)
        {
            srv::UprightDescriptor<short, short> * descriptor_upright_ptr;
            
            dense_descriptor = descriptor_upright_ptr = new srv::UprightDescriptor<short, short>();
            descriptor_upright_ptr->setDerivativesGaussian(upright_gaussian_kernel_method);
            if (upright_number_of_features == 0)
                throw srv::Exception("The upright descriptor requires that at least an image feature is extracted from the image (use parameter '--ud-features').");
            descriptor_upright_ptr->setFeatureObjects(upright_features, upright_number_of_features);
            descriptor_upright_ptr->setSpatialParameters(upright_descriptor_spatial, upright_spatial_levels, upright_spatial_initial_x, upright_spatial_initial_y, upright_spatial_degree_x, upright_spatial_degree_y, upright_spatial_interpolation, upright_weight_gaussian, upright_weight_sigma);
            descriptor_upright_ptr->setNormalizationParameters(norm_spatial, norm_descriptor, upright_cutoff, upright_power);
        }
        else if (descriptor_index == 1)
        {
            srv::IntegralHOG<short, short> * descriptor_ihog_ptr;
            
            dense_descriptor = descriptor_ihog_ptr = new srv::IntegralHOG<short, short>();
            descriptor_ihog_ptr->setNumberOfOrientationBins(ihog_number_of_orientation_bins);
            descriptor_ihog_ptr->setDescriptorPartitions(ihog_partitions_x, ihog_partitions_y);
        }
        else throw srv::Exception("The selected dense descriptor index (%d) is not implemented yet.", descriptor_index);
        
        dense_descriptor->setScalesParameters(scales_information, sigma_ratio);
        dense_descriptor->setColorMethod(color_method);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the dense descriptor object:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save to disk ]-----------------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::saveObject(filename_descriptor, dense_descriptor);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while saving the dense descriptor information into the XML file:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Free allocated memory ]--------------------------------------------------------------------------------------------------------------------
    if (upright_features != 0)
    {
        for (unsigned int i = 0; i < upright_number_of_features; ++i)
            delete upright_features[i];
        delete [] upright_features;
    }
    delete dense_descriptor;
    
    return EXIT_SUCCESS;
}

