// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_database_information.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"
#include "classification_tools.hpp"

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                            03-March-2013\n"
                                   "=================================================================================\n"
                                   " This program creates a classifier from the given set of labeled vectors.\n\n";
    const double positive_probability = 0.4;
    const double difficult_probability = 0.6;
    const bool ignore_difficult_samples = true;
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    // Other parameters.
    char filename_train[4096], filename_validation[4096], filename_classifier[4096];
    unsigned int number_of_threads;
    bool verbose;
    // Classifier basic parameters.
    unsigned int classifier_index, maximum_number_of_iterations;
    // Classifier cross-validation parameters.
    double minimum_regularization_factor, maximum_regularization_factor, minimum_balancing_factor;
    double maximum_balancing_factor, regularization_dwindling, balancing_dwindling;
    unsigned int regularization_partitions, regularization_scale_index, balancing_partitions, balacing_scale_index, number_of_levels;
    unsigned int validation_measure_index, validation_samples_index, number_of_folds;
    // Batch algorithm parameters (SVMs).
    double epsilon;
    unsigned int transpose_index;
    // Online algorithm parameters.
    unsigned int average_classifier, average_skip, chunk_size, loss_index, number_of_perceptrons;
    unsigned int projection_optimization, initial_step, skip;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./classifier_create", initial_message);
    srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> train_samples, validation_samples;
    srv::LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * classifier;
    srv::ClassifierValidationParameters<TCLASSIFIER> validation_parameters;
    srv::VectorDense<NLABEL> categories_identifiers;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        // Define use parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_train", "file containing the labeled vectors used to create the classifiers.", filename_train));
        parameters.addParameter(new srv::ParameterCharArray("filename_classifier", "file where the resulting classifiers are going to be stored.", filename_classifier));
        parameters.addParameter(new srv::ParameterCharArray("--validation", "filename", "file containing the labeled vectors used as validation set.", filename_validation));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Classifier common parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--classifier", "identifier", "classifier algorithm used to create the linear classifier. Possible values are:"
                                    "&0&L2-Regularized Linear SVM using Logistic Regression (Primal)"
                                    "&1&L2-Regularized Linear SVM using Logistic Regression (Dual)"
                                    "&2&L1-Regularized Linear SVM using Logistic Regression"
                                    "&3&Multiclass Linear SVM (Crammer and Singer)"
                                    "&4&L2-Regularized Linear SVM using L2-loss (Dual)"
                                    "&5&L2-Regularized Linear SVM using L2-loss (Primal)"
                                    "&6&L2-Regularized Linear SVM using L1-loss (Dual)"
                                    "&7&L2-Regularized Linear SVM using L1-loss (Dual)"
                                    "&8&Online Pegasos algorithm"
                                    "&9&Online first-order stochastic gradient descent algorithm."
                                    "&10&Online quasi-newton stochastic gradient descent algorithm."
                                    "&11&Online Perceptron algorithm."
                                    "&12&Online Multiple Perceptron algorithm.", 0, true, 0, true, 12, &classifier_index));
        parameters.addParameter(new srv::ParameterUInt("--max-iterations", "iterations", "maximum number of iterations of the optimization algorithm.", 1000, true, 1, false, 0, &maximum_number_of_iterations));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Cross-validation parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterDouble("--reg-min", "factor", "minimum regularization factor cross-validated.", 1e-6, true, 0.0, false, 0, &minimum_regularization_factor));
        parameters.addParameter(new srv::ParameterDouble("--reg-max", "factor", "maximum regularization factor cross-validated.", 1e6, true, 0.0, false, 0, &maximum_regularization_factor));
        parameters.addParameter(new srv::ParameterUInt("--reg-num", "partitions", "number of regularization values evaluated between the maximum and minimum regularization values.", 12, true, 1, false, 0, &regularization_partitions));
        parameters.addParameter(new srv::ParameterUInt("--reg-sca", "index", "index of the scale used to generate the regularization partitions. Possible values are"
                    "&0&Uniform scale."
                    "&1&Base 2 logarithmic scale."
                    "&2&Base 10 logarithmic scale.", 2, true, 0, true, 2, &regularization_scale_index));
        parameters.addParameter(new srv::ParameterDouble("--bal-min", "factor", "minimum positive-negative balancing factor cross-validated.", 0.05, true, 0.0, true, 1.0, &minimum_balancing_factor));
        parameters.addParameter(new srv::ParameterDouble("--bal-max", "factor", "maximum positive-negative balancing factor cross-validated.", 0.95, true, 0.0, true, 1.0, &maximum_balancing_factor));
        parameters.addParameter(new srv::ParameterUInt("--bal-num", "partitions", "number of positive-negative balancing factors evaluated between the maximum and the minimum balancing values.", 8, true, 1, false, 0, &balancing_partitions));
        parameters.addParameter(new srv::ParameterUInt("--bal-sca", "index", "index of the scale used to generate the balancing partitions. Possible values are"
                    "&0&Uniform scale."
                    "&1&Base 2 logarithmic scale."
                    "&2&Base 10 logarithmic scale.", 0, true, 0, true, 2, &balacing_scale_index));
        parameters.addParameter(new srv::ParameterUInt("--pre-num", "levels", "number of precision levels used in the cross-validation process.", 1, true, 1, false, 0, &number_of_levels));
        parameters.addParameter(new srv::ParameterDouble("-pre-reg", "factor", "decrease factor of the range of regularization values at each precision level.", 0.5, true, 0.0, true, 1.0, &regularization_dwindling));
        parameters.addParameter(new srv::ParameterDouble("--pre-bal", "factor", "decrease factor of the range of balancing values at each precision level.", 0.5, true, 0.0, true, 1.0, &balancing_dwindling));
        parameters.addParameter(new srv::ParameterUInt("--validation-score", "index", "index of the validation measure used to evaluate the performance of the classifiers during the cross validation process. Possible values are:"
                                "&0&mean Average Precision."
                                "&1&average Geometric Mean.", 0, true, 0, true, 1, &validation_measure_index));
        parameters.addParameter(new srv::ParameterUInt("--validation-samples", "index", "index of the action performed with the validation samples once the best set of parameters is found. Possible values are:"
                                "&0&retrain the classifier using only train samples."
                                "&1&retrain the classifier using both train and validation samples.", 0, true, 0, true, 1, &validation_samples_index));
        parameters.addParameter(new srv::ParameterUInt("--folds", "partitions", "number of folds used to search the best parameter combination when a validation set is not available.", 4, true, 1, false, 0, &number_of_folds));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Batch linear SVM algorithm parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterDouble("--eps", "epsilon", "tolerance of the termination criterion.", 0.1, true, 0.00001, false, 1.0, &epsilon));
        parameters.addParameter(new srv::ParameterUInt("--transpose", "index", "enable or disable the transposition of data to accelerate the learning algorithm. Possible values are:"
                    "&0&Disable."
                    "&1&Enable.", 1, true, 0, true, 1, &transpose_index));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Online linear algorithm parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--average", "index", "enable or disable the use of average classifiers. Possible values are:"
                    "&0&Disable."
                    "&1&Enable.", 1, true, 0, true, 1, &average_classifier));
        parameters.addParameter(new srv::ParameterUInt("--average-skip", "iterations", "number of iterations until the average model is updated.", 1, true, 1, false, 0, &average_skip));
        parameters.addParameter(new srv::ParameterUInt("--chunk", "samples", "number of samples used to update the model at once.", 1, true, 1, false, 0, &chunk_size));
        parameters.addParameter(new srv::ParameterUInt("--loss", "index", "index of the loss function of the online algorithm. Possible values are:"
                    "&0&Logistic loss."
                    "&1&Margin logistic loss."
                    "&2&Smooth hinge loss."
                    "&3&Square hinge loss."
                    "&4&Hinge loss.", 4, true, 1, true, 4, &loss_index));
        parameters.addParameter(new srv::ParameterUInt("--perceptrons", "amount", "number of Perceptrons of the multiple Perceptron algorithm.", 10, true, 1, false, 0, &number_of_perceptrons));
        parameters.addParameter(new srv::ParameterUInt("--projection", "index", "enable or disable the spherical optimization. Possible values are:"
                    "&0&Disable."
                    "&1&Enable.", 0, true, 0, true, 1, &projection_optimization));
        parameters.addParameter(new srv::ParameterUInt("--initial-step", "value", "initial number of iterations used to calculate stabilize the online algorithm step.", 10, true, 1, false, 0, &initial_step));
        parameters.addParameter(new srv::ParameterUInt("--skip", "iterations", "number of iterations until the weight vector is re-normalized.", 5, true, 1, false, 0, &skip));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Other parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterBoolean("--verbose", "", "show the internal information of the linear learner algorithm.", &verbose));
        parameters.addParameter(new srv::ParameterUInt("--thr", "number_of_threads", "number of threads used to create and calibrate the classifiers concurrently.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parse user parameters.
        filename_validation[0] = '\0';
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load samples ]-----------------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::VectorDense<NLABEL> current_categories_identifiers;
        unsigned int valid_categories;
        
        loadObject(filename_train, train_samples);
        train_samples.categoryIdentifiers(current_categories_identifiers);
        valid_categories = 0;
        for (unsigned int i = 0; i < current_categories_identifiers.size(); ++i)
            if (current_categories_identifiers[i] >= 0)
                ++valid_categories;
        categories_identifiers.set(valid_categories);
        valid_categories = 0;
        for (unsigned int i = 0; i < current_categories_identifiers.size(); ++i)
        {
            if (current_categories_identifiers[i] >= 0)
            {
                categories_identifiers[valid_categories] = current_categories_identifiers[i];
                ++valid_categories;
            }
        }
        
        if (!((train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_SET) || (train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::VALIDATION_SET) || (train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_VALIDATION_SET)))
            throw srv::Exception("Test information cannot be used to train the classifier.");
        if (filename_validation[0] != '\0')
        {
            srv::VectorDense<NLABEL> validation_identifiers;
            std::set<NLABEL> validation_set;
            
            loadObject(filename_validation, validation_samples);
            if (!((validation_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_SET) || (validation_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::VALIDATION_SET) || (validation_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_VALIDATION_SET)))
                throw srv::Exception("Test information cannot be used to validate the classifier.");
            if (validation_samples.getNumberOfDimensions() != train_samples.getNumberOfDimensions())
                throw srv::Exception("The dimensionality of the train samples (%d) is different than the dimensionality of the validation samples (%d).", train_samples.getNumberOfDimensions(), validation_samples.getNumberOfDimensions());
            if (validation_samples.getContext() != train_samples.getContext())
                throw srv::Exception("Train (%d) and validation (%d) samples must be extracted from the same context.", (int)train_samples.getContext(), (int)validation_samples.getContext());
            validation_samples.categoryIdentifiers(validation_identifiers);
            for (unsigned int i = 0; i < validation_identifiers.size(); ++i)
                validation_set.insert(validation_identifiers[i]);
            for (unsigned int i = 0; i < categories_identifiers.size(); ++i)
                if (validation_set.find(categories_identifiers[i]) == validation_set.end())
                    throw srv::Exception("Not all train categories are present in the validation set.");
        }
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the samples from disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Create and train the classifiers ]---------------------------------------------------------------------------------------------------------
    try
    {
        const double label_factor = calculateFactor<TLABEL>();
        
        const srv::LOSS_FUNCTION_IDENTIFIER loss_identifiers[] = {
            srv::LOGISTIC_LOSS,
            srv::MARGIN_LOGISTIC_LOSS,
            srv::SMOOTH_HINGE_LOSS,
            srv::SQUARE_HINGE_LOSS,
            srv::HINGE_LOSS
        };
        const srv::PARTITION_SCALE regularization_scale_identifiers[] = {
            srv::PARTITION_UNIFORM,
            srv::PARTITION_LOG2,
            srv::PARTITION_LOG10
        };
        const srv::CLASSIFIER_VALIDATION_IDENTIFIER validation_measure_identifiers[] =
        {
            srv::CLASSIFIER_VALIDATION_MEAN_AVERAGE_PRECISION,
            srv::CLASSIFIER_VALIDATION_AVERAGE_GEOMETRIC_MEAN
        };
        
        switch (classifier_index)
        {
        case 0:
            classifier = new srv::L2RegularizedLogisticRegressionPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 1:
            classifier = new srv::L2RegularizedLogisticRegressionDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 2:
            classifier = new srv::L1RegularizedLogisticRegressionSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations, transpose_index == 1);
            break;
        case 3:
            classifier = new srv::MulticlassSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 4:
            classifier = new srv::L2RegularizedL2LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 5:
            classifier = new srv::L2RegularizedL2LossPrimalSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 6:
            classifier = new srv::L2RegularizedL1LossDualSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations);
            break;
        case 7:
            classifier = new srv::L1RegularizedL2LossSVM<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    (TCLASSIFIER)epsilon, maximum_number_of_iterations, transpose_index == 1);
            break;
        case 8:
            classifier = new srv::Pegasos<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    average_classifier == 1, average_skip, chunk_size, projection_optimization == 1, loss_identifiers[loss_index]);
            break;
        case 9:
            classifier = new srv::SgdFirstOrder<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    average_classifier == 1, average_skip, chunk_size, initial_step, skip, projection_optimization == 1, loss_identifiers[loss_index]);
            break;
        case 10:
            classifier = new srv::SgdQuasiNewton<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    average_classifier == 1, average_skip, chunk_size, initial_step, skip, projection_optimization == 1, loss_identifiers[loss_index]);
            break;
        case 11:
            classifier = new srv::Perceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    average_classifier == 1, average_skip, chunk_size, loss_identifiers[loss_index]);
            break;
        case 12:
            classifier = new srv::MultiplePerceptron<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL>((NSAMPLE)train_samples.getNumberOfDimensions(),
                    categories_identifiers.getData(), (unsigned int)categories_identifiers.size(),
                    (TLABEL)(label_factor * positive_probability), (TLABEL)(label_factor * difficult_probability), ignore_difficult_samples,
                    average_classifier == 1, average_skip, chunk_size, number_of_perceptrons, loss_identifiers[loss_index]);
            break;
        default:
            throw srv::Exception("Classifier with identifier number '%d' unknown or not yet implemented.", classifier_index);
        }
        
        validation_parameters.setRegularizationFactorParameters((TCLASSIFIER)minimum_regularization_factor, (TCLASSIFIER)maximum_regularization_factor, regularization_partitions, regularization_scale_identifiers[regularization_scale_index]);
        validation_parameters.setBalancingFactorParameters((TCLASSIFIER)minimum_balancing_factor, (TCLASSIFIER)maximum_balancing_factor, balancing_partitions, regularization_scale_identifiers[balacing_scale_index]);
        validation_parameters.setNumberOfLevels(number_of_levels);
        validation_parameters.setRegularizationFactorDwindling((TCLASSIFIER)regularization_dwindling);
        validation_parameters.setBalancingFactorDwindling((TCLASSIFIER)balancing_dwindling);
        validation_parameters.setValidationIdentifier(validation_measure_identifiers[validation_measure_index]);
        
        if (filename_validation[0] != '\0')
            classifier->train(train_samples.getSamples(), train_samples.getNumberOfSamples(), validation_samples.getSamples(), validation_samples.getNumberOfSamples(), validation_parameters, validation_samples_index == 1, false, number_of_threads, (verbose)?&logger:0);
        else
            classifier->train(train_samples.getSamples(), train_samples.getNumberOfSamples(), number_of_folds, validation_parameters, false, number_of_threads, (verbose)?&logger:0);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the classifiers:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Saving the classifier to disk ]------------------------------------------------------------------------------------------------------------
    try
    {
        std::cout << " * Classifier information:" << std::endl;
        std::cout << "----------------------------------------------------------------------------------" << std::endl;
        std::cout << classifier << std::endl;
        std::cout << "----------------------------------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        saveObject(filename_classifier, classifier);
        delete classifier;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while saving the classifiers:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
   
    return EXIT_SUCCESS;
}

