// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_database_information.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"
#include "classification_tools.hpp"

INITIALIZE_DENSE_VISUAL_WORDS(short, short, unsigned char, int, unsigned int)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              18-May-2018\n"
                                   "=================================================================================\n"
                                   " This program extract the visual words and accumulated them into histograms for"
                                   " the selected database images.\n\n";
    const unsigned int pyr_levels = 1;
    const unsigned int pyr_initial_x = 1;
    const unsigned int pyr_initial_y = 1;
    const unsigned int pyr_growth_x = 2;
    const unsigned int pyr_growth_y = 2;
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int number_of_threads, histogram_type_index, local_region_size;
    unsigned int local_region_step, local_region_sampling, local_region_maximum;
    char filename_database[4096], filename_visual_words[4096], filename_histogram[4096];
    double normal_threshold;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::DenseVisualWords<short, short, unsigned char, int, unsigned int> visual_words;
    std::list<srv::Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * > histogram_samples;
    srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::CONTEXT samples_context;
    srv::ParameterParser parameters("./histogram_extract", initial_message);
    srv::VisualWordsHistogramParameters histogram_configuration;
    srv::DatabaseInformation::IMAGE_TYPE image_type_identifier;
    srv::DatabaseInformation::IMAGE_SET image_set_identifier;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int image_type_index, image_set_index;
        
        // Define user parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_database", "filename with the information of the images where the histograms are going to be extracted from.", filename_database));
        parameters.addParameter(new srv::ParameterUInt("set_index", "set of images selected from the database. Possible values are:"
                                                                    "&0&Only train set images."
                                                                    "&1&Only validation set images."
                                                                    "&2&Only test set images."
                                                                    "&3&Train and validation sets images."
                                                                    "&4&Train and test sets images."
                                                                    "&5&Validation and test sets images."
                                                                    "&6&All images", true, 0, true, 6, &image_set_index));
        parameters.addParameter(new srv::ParameterUInt("type_index", "semantic information available in the selected images. Possible values are:"
                                                                     "&0&All images."
                                                                     "&1&Images with segmentation annotation."
                                                                     "&2&Images with bounding boxes of the objects annotation.", true, 0, true, 2, &image_type_index));
        parameters.addParameter(new srv::ParameterUInt("histogram_type_index", "type of histogram extracted from the images. Possible values are:"
                                                                          "&0&Global image histogram."
                                                                          "&1&Local image histograms.", true, 0, true, 1, &histogram_type_index));
        parameters.addParameter(new srv::ParameterCharArray("filename_visual_words", "file with the dense visual words object information.", filename_visual_words));
        parameters.addParameter(new srv::ParameterCharArray("filename_histogram", "file where the resulting histograms of visual words are stored.", filename_histogram));
        parameters.addParameter(new srv::ParameterDouble("--normal-threshold", "value", "minimum value of the normal of the histogram of visual words to reject the histogram.", 10, true, 0.0, false, 0.0, &normal_threshold));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Local histogram sampling parameters"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--local-region-size", "pixels", "size of the local squared regions where the local histograms are extracted from.", 40, true, 20, false, 0, &local_region_size));
        parameters.addParameter(new srv::ParameterUInt("--local-region-step", "pixels", "displacement step between neighboring local regions.", 5, true, 1, false, 0, &local_region_step));
        parameters.addParameter(new srv::ParameterUInt("--local-region-max", "amount", "maximum number of local histograms extracted from the images [set to 0 to extract all the histograms].", 0, true, 0, false, 0, &local_region_maximum));
        parameters.addParameter(new srv::ParameterUInt("--local-region-sampling", "index", "options available to sample local regions from the images. Possible values are:"
                                                                                           "&0&Proportional."
                                                                                           "&1&Uniform.", 1, true, 0, true, 1, &local_region_sampling));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Other parameters "));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--thr", "number_of_threads", "number of threads used to extract the histograms of visual words concurrently.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parse user parameters.
        parameters.parse(argv, argc);
        
        // Set method identifiers.
        if      (image_type_index == 0) image_type_identifier = srv::DatabaseInformation::ALL_IMAGES;
        else if (image_type_index == 1) image_type_identifier = srv::DatabaseInformation::SEGMENTATION_IMAGES_ONLY;
        else if (image_type_index == 2) image_type_identifier = srv::DatabaseInformation::BOUNDINGBOXES_IMAGES_ONLY;
        else throw srv::Exception("Image type with index '%d' not yet implemented.", image_type_index);
        if      (image_set_index == 0) image_set_identifier = srv::DatabaseInformation::TRAIN_SET;
        else if (image_set_index == 1) image_set_identifier = srv::DatabaseInformation::VALIDATION_SET;
        else if (image_set_index == 2) image_set_identifier = srv::DatabaseInformation::TEST_SET;
        else if (image_set_index == 3) image_set_identifier = srv::DatabaseInformation::TRAIN_VALIDATION_SET;
        else if (image_set_index == 4) image_set_identifier = srv::DatabaseInformation::TRAIN_TEST_SET;
        else if (image_set_index == 5) image_set_identifier = srv::DatabaseInformation::VALIDATION_TEST_SET;
        else if (image_set_index == 6) image_set_identifier = srv::DatabaseInformation::ALL_SETS;
        else throw srv::Exception("Image set with index '%d' not yet implemented.", image_set_index);
        samples_context = (image_type_index == 0)?srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::GLOBAL:srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::LOCAL;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load visual words object form disk ]-------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Loading visual words object from '%s' file.", filename_visual_words);
        loadObject(filename_visual_words, visual_words);
        
        // Codebook information.
        histogram_configuration.setNumberOfVisualWords(visual_words.getCodebook()->getNumberOfCodebookFeatures(), visual_words.getCodebook()->getNumberOfBinsPerDescriptor());
        // Spatial options.
        histogram_configuration.setPyramidParameters(pyr_levels, pyr_initial_x, pyr_initial_y, pyr_growth_x, pyr_growth_y);
        // Pooling options.
        histogram_configuration.setPoolingMethod(srv::BOVW_POOLING_SUM);
        histogram_configuration.setPolarity(false);
        histogram_configuration.setInterpolation(false);
        histogram_configuration.setUnreliable(false);
        histogram_configuration.setDifferentiateScales(false);
        histogram_configuration.setNumberOfScales(0);
        histogram_configuration.setScalesMargin(false);
        // Normalization options.
        histogram_configuration.setSpatialNormalization(false);
        histogram_configuration.setAreaWeighting(false);
        histogram_configuration.setSpatialNorm(srv::VectorNorm(srv::MANHATTAN_NORM));
        histogram_configuration.setHistogramNorm(srv::VectorNorm(srv::MANHATTAN_NORM));
        histogram_configuration.setPowerFactor(1.0);
        histogram_configuration.setCutoffValue(1.0);
    }
    catch (srv::Exception & e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the visual words to the XML file:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
    }
    
    // -[ Load database information and extract histograms of visual words ]-------------------------------------------------------------------------
    try
    {
        srv::DatabaseInformation database(filename_database);
        srv::DatabaseInformation::Subset database_iterator;
        char text[512], image_counter_mask[512];
        unsigned int number_of_histograms, image_counter_length;
        srv::VectorDense<int> initial_scales;
        srv::VectorDense<unsigned int> scale_sizes(visual_words.getDenseDescriptors()->getNumberOfScales());
        const double histogram_factor = calculateFactor<TSAMPLE>();
        const double label_factor = calculateFactor<TLABEL>();
        
        // 1) Initialize structures .................................................................................................................
        database.updateLabels();
        database_iterator = database.getIterator(image_type_identifier, image_set_identifier);
        for (unsigned int i = 0; i < visual_words.getDenseDescriptors()->getNumberOfScales(); ++i)
            scale_sizes[i] = visual_words.getDenseDescriptors()->getScalesInformation(i).getRegionSize();
        
        // 2) Allocate memory for the histogram of visual words .....................................................................................
        logger.log("Calculating the number of histograms generated regarding the amount of scales of the dense regions.");
        if (histogram_configuration.getNumberOfScales() == 0) initial_scales.set(1, 0);
        else if (histogram_configuration.getScalesMargin())
        {
            initial_scales.set(visual_words.getDenseDescriptors()->getNumberOfScales() + histogram_configuration.getNumberOfScales() - 1);
            for (int i = -((int)histogram_configuration.getNumberOfScales()) + 1, j = 0; i < (int)visual_words.getDenseDescriptors()->getNumberOfScales(); ++i, ++j)
                initial_scales[(unsigned int)j] = i;
        }
        else if (histogram_configuration.getNumberOfScales() >= visual_words.getDenseDescriptors()->getNumberOfScales())
            initial_scales.set(1, 0);
        else
        {
            initial_scales.set(visual_words.getDenseDescriptors()->getNumberOfScales());
            for (unsigned int i = 0; i < visual_words.getDenseDescriptors()->getNumberOfScales(); ++i)
                initial_scales[i] = (int)i;
        }
        
        
        // 3) Global or local histograms? ...........................................................................................................
        if (histogram_type_index == 0) // GLOBAL HISTOGRAMS
        {
            number_of_histograms = initial_scales.size() * database_iterator.size();
            srv::thousandSeparator(number_of_histograms, text);
            logger.log("Up to %s histogram(s) of visual words can be extracted from the database images.", text);
            image_counter_length = (unsigned int)ceil(log10((double)database_iterator.size()));
            sprintf(image_counter_mask, "Extracting visual words from image %%0%dd of %%0%dd: %%s", image_counter_length, image_counter_length);
            
            for (unsigned int index = 0; index < database_iterator.size(); ++index)
            {
                srv::Image<unsigned char> original_image;
                srv::VectorDense<srv::DescriptorLocation> locations;
                srv::VectorSparse<int, unsigned int> * codes;
                srv::VisualWordsImage<int, unsigned int> visual_words_image;
                unsigned int current_number_of_histograms, width, height;
                srv::VisualWordsHistogram * histograms;
                srv::VectorSparse<double, int> * labels;
                srv::Chronometer chrono;
                
                logger.log("Processing image %d of %d.", index + 1, database_iterator.size());
                // Extract visual words .................................................................................................................
                width = database_iterator[index]->getWidth();
                height = database_iterator[index]->getHeight();
                logger.log(image_counter_mask, index + 1, database_iterator.size(), database_iterator[index]->getImageFilename());
                database_iterator.getOriginalColorImage(index, original_image);
                chrono.start();
                codes = 0;
                visual_words.extract(original_image, locations, codes, number_of_threads);
                srv::thousandSeparator(locations.size(), text);
                logger.log("%s visual words extracted in %.3f seconds.", text, chrono.getElapsedTime() / 1000.0);
                
                // Generate visual words image ..........................................................................................................
                chrono.start();
                visual_words_image.set(locations.getData(), codes, locations.size(), original_image.getWidth(), original_image.getHeight(), scale_sizes.getData(), scale_sizes.size());
                logger.log("Visual words image generated in %.3f seconds.", chrono.getElapsedTime() / 1000.0);
                
                logger.log("Generating the histogram of visual words structures for the current image.");
                current_number_of_histograms = 0;
                histograms = 0;
                labels = 0;
                if (initial_scales.size() > 0)
                {
                    histograms = new srv::VisualWordsHistogram[initial_scales.size()];
                    labels = new srv::VectorSparse<double, int>[initial_scales.size()];
                    
                    for (unsigned int scale_index = 0; scale_index < initial_scales.size(); ++scale_index, ++current_number_of_histograms)
                    {
                        histograms[current_number_of_histograms] = srv::VisualWordsHistogram(0, 0, (int)width, (int)height, initial_scales[scale_index]);
                        labels[current_number_of_histograms].set(database_iterator[index]->getNumberOfGlobalCategories());
                        for (unsigned int catid = 0; catid < database_iterator[index]->getNumberOfGlobalCategories(); ++catid)
                            labels[current_number_of_histograms][catid].setData(1.0, database_iterator[index]->getGlobalCategories()[catid].getLabel());
                    }
                }
                srv::thousandSeparator(current_number_of_histograms, text);
                logger.log("Extracting %s histograms from the image.", text);
                
                if (current_number_of_histograms > 0)
                {
                    srv::VectorDense<double> normals(current_number_of_histograms);
                    
                    chrono.start();
                    visual_words_image.extract(histograms, normals, histogram_configuration, number_of_threads);
                    logger.log("Histograms of visual words extracted in %.3f seconds.", chrono.getElapsedTime() / 1000.0);
                    
                    for (unsigned int i = 0; i < current_number_of_histograms; ++i)
                    {
                        bool valid_label;
                        
                        valid_label = true;
                        if ((labels[i].size() == 1) && (labels[i][0].getSecond() == -1000))
                            valid_label = false;
                        // Add only histograms which have a strong enough norm and also have a set of valid labels (i.e. avoid samples labeled as unknown).
                        if ((normals[i] >= normal_threshold) && valid_label)
                        {
                            srv::Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * current_sample;
                            srv::VectorSparse<double, int> current_labels(labels[i].size());
                            srv::VectorSparse<double> current_histogram = histograms[i].getHistogram();
                            for (unsigned int d = 0; d < current_histogram.size(); ++d)
                                current_histogram[d].getFirst() *= histogram_factor;
                            for (int d = 0; d < labels[i].size(); ++d)
                                current_labels[d].setData(label_factor * labels[i].getValue(d), labels[i].getIndex(d));
                            current_sample = new srv::Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>(current_histogram, current_labels);
                            histogram_samples.push_back(current_sample);
                        }
                    }
                }
            }
        }
        else // LOCAL HISTOGRAMS
        {
            const unsigned int number_of_categories = database.getCategoriesInformation().getNumberOfSubcategories();
            std::list<srv::Tuple<unsigned int, unsigned int> > * selected_offsets;
            bool background_samples;
            
            // 3.1) Evaluate the number of pixels available for each category.
            logger.log("Evaluating the number of pixels availble for each category.");
            selected_offsets = new std::list<srv::Tuple<unsigned int, unsigned int> >[number_of_categories + 1];
            for (unsigned int image_index = 0; image_index < database_iterator.size(); ++image_index)
            {
                srv::PixelSemanticInformation * pixel_semantics;
                
                logger.log("Processing image %d of %d.", image_index + 1, database_iterator.size());
                pixel_semantics = database_iterator.getPixelSegmentationInformation(image_index);
                if ((pixel_semantics->getWidth() >= local_region_size) && (pixel_semantics->getHeight() >= local_region_size))
                {
                    unsigned int begin_x, begin_y, end_x, end_y, number_of_regions_x, number_of_regions_y;
                    std::list<unsigned int> * * category_offsets;
                    
                    number_of_regions_x = (pixel_semantics->getWidth() - local_region_size) / local_region_step + 1;
                    number_of_regions_y = (pixel_semantics->getHeight() - local_region_size) / local_region_step + 1;
                    category_offsets = new std::list<unsigned int> * [number_of_categories + 1];
                    for (unsigned int k = 0; k < number_of_categories + 1; ++k)
                        category_offsets[k] = new std::list<unsigned int>[number_of_regions_x * number_of_regions_y];
                    begin_x = local_region_size / 2;
                    end_x = pixel_semantics->getWidth() - local_region_size + local_region_size / 2;
                    begin_y = local_region_size / 2;
                    end_y = pixel_semantics->getHeight() - local_region_size + local_region_size / 2;
                    for (unsigned int y = 0, offset = 0; y < pixel_semantics->getHeight(); ++y)
                    {
                        if ((y >= begin_y) && (y <= end_y))
                        {
                            const int * label_ptr = pixel_semantics->get(y);
                            const unsigned int ty = (y - begin_y) / local_region_step;
                            
                            for (unsigned int x = 0; x < pixel_semantics->getWidth(); ++x, ++label_ptr)
                            {
                                if ((x >= begin_x) && (x <= end_x))
                                {
                                    const unsigned int tx = (x - begin_x) / local_region_step;
                                    
                                    if (*label_ptr >= 0)
                                        category_offsets[(unsigned int)*label_ptr][tx + ty * number_of_regions_x].push_back(offset + x);
                                    else if (*label_ptr == -1)
                                        category_offsets[number_of_categories][tx + ty * number_of_regions_x].push_back(offset + x);
                                }
                            }
                        }
                        offset += pixel_semantics->getWidth();
                    }
                    
                    for (unsigned int k = 0; k < number_of_categories + 1; ++k)
                    {
                        for (unsigned int m = 0; m < number_of_regions_x * number_of_regions_y; ++m)
                        {
                            if (category_offsets[k][m].size() > 0)
                            {
                                unsigned int selected_index, selected_offset;
                                
                                selected_index = rand() % (unsigned int)category_offsets[k][m].size();
                                selected_offset = std::numeric_limits<unsigned int>::max();
                                for (std::list<unsigned int>::iterator begin = category_offsets[k][m].begin(), end = category_offsets[k][m].end(); begin != end; ++begin, --selected_index)
                                {
                                    if (selected_index == 0)
                                    {
                                        selected_offset = *begin;
                                        break;
                                    }
                                }
                                if (selected_offset < std::numeric_limits<unsigned int>::max())
                                    selected_offsets[k].push_back(srv::Tuple<unsigned int, unsigned int>(image_index, selected_offset));
                            }
                        }
                        delete [] category_offsets[k];
                    }
                    delete [] category_offsets;
                }
                delete pixel_semantics;
            }
            number_of_histograms = 0;
            for (unsigned int k = 0; k < number_of_categories + 1; ++k)
                number_of_histograms += (unsigned int)selected_offsets[k].size();
            background_samples = selected_offsets[number_of_categories].size() > 0;
            
            // 3.2) Calculate the coordinates of the samples extracted at each image.
            logger.log("Select the histograms sampled for each category.");
            srv::VectorDense<std::list<srv::Tuple<unsigned int, int> > > image_offsets(database_iterator.size());
            srv::VectorDense<unsigned int> histograms_per_category(number_of_categories + 1);
            if ((local_region_maximum == 0) || (number_of_histograms < local_region_maximum)) local_region_maximum = number_of_histograms;
            
            if (local_region_sampling == 0) // Proportional: the number of histograms of each category is proportional to the initial rations.
            {
                unsigned int sum;
                
                if (background_samples)
                {
                    sum = 0;
                    for (unsigned int c = 0; c < number_of_categories; ++c)
                    {
                        histograms_per_category[c] = (unsigned int)((double)local_region_maximum * (double)selected_offsets[c].size() / (double)number_of_histograms);
                        sum += histograms_per_category[c];
                    }
                    // The remaining histograms are assigned to the background class.
                    histograms_per_category[number_of_categories] = srv::srvMin<unsigned int>((unsigned int)selected_offsets[number_of_categories].size(), local_region_maximum - sum);
                }
                else
                {
                    sum = 0;
                    for (unsigned int c = 0; c < number_of_categories - 1; ++c)
                    {
                        histograms_per_category[c] = (unsigned int)((double)local_region_maximum * (double)selected_offsets[c].size() / (double)number_of_histograms);
                        sum += histograms_per_category[c];
                    }
                    // Assign the remaining histograms to the last class.
                    histograms_per_category[number_of_categories - 1] = srv::srvMin<unsigned int>((unsigned int)selected_offsets[number_of_categories - 1].size(), local_region_maximum - sum);
                    histograms_per_category[number_of_categories] = 0;
                }
            }
            else if (local_region_sampling == 1) // Uniform: Each category has the same number of samples.
            {
                if (background_samples)
                {
                    for (unsigned int c = 0; c < number_of_categories + 1; ++c)
                        histograms_per_category[c] = srv::srvMin<unsigned int>((unsigned int)selected_offsets[c].size(), local_region_maximum / (number_of_categories + 1));
                }
                else
                {
                    for (unsigned int c = 0; c < number_of_categories + 1; ++c)
                        histograms_per_category[c] = srv::srvMin<unsigned int>((unsigned int)selected_offsets[c].size(), local_region_maximum / number_of_categories);
                }
            }
            else throw srv::Exception("Unknown sampling method with index '%d'.", local_region_sampling);
            
            for (unsigned int c = 0; c < number_of_categories + 1; ++c)
            {
                srv::VectorDense<srv::Tuple<unsigned int, unsigned int> > selected_offsets_vector((unsigned int)selected_offsets[c].size());
                unsigned int offset_index;
                
                offset_index = 0;
                for (std::list<srv::Tuple<unsigned int, unsigned int> >::iterator begin = selected_offsets[c].begin(), end = selected_offsets[c].end(); begin != end; ++begin, ++offset_index)
                    selected_offsets_vector[offset_index] = *begin;
                for (unsigned int i = 0; i < selected_offsets_vector.size(); ++i)
                    srv::srvSwap(selected_offsets_vector[i], selected_offsets_vector[i + rand() % (selected_offsets_vector.size() - i)]);
                
                for (unsigned int i = 0; i < histograms_per_category[c]; ++i)
                {
                    int current_label = (c >= number_of_categories)?-1:((int)c);
                    image_offsets[selected_offsets_vector[i].getFirst()].push_back(srv::Tuple<unsigned int, int>(selected_offsets_vector[i].getSecond(), current_label));
                }
            }
            number_of_histograms = 0;
            for (unsigned int m = 0; m < image_offsets.size(); ++m)
                number_of_histograms += (unsigned int)image_offsets[m].size();
            
            srv::thousandSeparator(number_of_histograms, text);
            logger.log("Up to %s histogram(s) of visual words can be extracted from the database images.", text);
            for (unsigned int c = 0; c < number_of_categories; ++c)
            {
                srv::thousandSeparator((unsigned int)image_offsets[c].size(), text);
                logger.log("  · Category %04d: up to %s histograms.", c, text);
            }
            if (background_samples)
            {
                srv::thousandSeparator((unsigned int)image_offsets[number_of_categories].size(), text);
                logger.log("  · Background: up to %s histograms.", text);
            }
            
            image_counter_length = (unsigned int)ceil(log10((double)database_iterator.size()));
            sprintf(image_counter_mask, "Extracting visual words from image %%0%dd of %%0%dd: %%s", image_counter_length, image_counter_length);
            
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            // DEBUG CODE: Check that the selected histograms have the correct sample on the image!!
            for (unsigned int index = 0; index < database_iterator.size(); ++index)
            {
                if (image_offsets[index].size() > 0)
                {
                    srv::PixelSemanticInformation * pixel_semantics;
                    unsigned int width;
                    
                    pixel_semantics = database_iterator.getPixelSegmentationInformation(index);
                    width = database_iterator[index]->getWidth();
                    
                    for (std::list<srv::Tuple<unsigned int, int> >::iterator begin = image_offsets[index].begin(), end = image_offsets[index].end(); begin != end; ++begin)
                    {
                        unsigned int x, y;
                        int current_label;
                        
                        x = begin->getFirst() % width - local_region_size / 2;
                        y = begin->getFirst() / width - local_region_size / 2;
                        current_label = pixel_semantics->get((unsigned int)x + local_region_size / 2, (unsigned int)y + local_region_size / 2);
                        if (current_label != begin->getSecond())
                            std::cout << current_label << " " << begin->getSecond() << std::endl;
                    }
                    
                    delete pixel_semantics;
                }
            }
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            // DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE!!DEBUG CODE
            
            // 3.3) Extract the histograms from the database.
            logger.log("Extracting the histograms of visual words from the images.");
            for (unsigned int index = 0; index < database_iterator.size(); ++index)
            {
                logger.log("Processing image %d of %d.", index + 1, database_iterator.size());
                if (image_offsets[index].size() > 0)
                {
                    srv::Image<unsigned char> original_image;
                    srv::VectorDense<srv::DescriptorLocation> locations;
                    srv::VectorSparse<int, unsigned int> * codes;
                    srv::VisualWordsImage<int, unsigned int> visual_words_image;
                    unsigned int n, width;
                    srv::VisualWordsHistogram * histograms;
                    srv::VectorSparse<double, int> * labels;
                    srv::Chronometer chrono;
                    
                    width = database_iterator[index]->getWidth();
                    database_iterator.getOriginalColorImage(index, original_image);
                    chrono.start();
                    codes = 0;
                    visual_words.extract(original_image, locations, codes, number_of_threads);
                    srv::thousandSeparator(locations.size(), text);
                    logger.log("%s visual words extracted in %.3f seconds.", text, chrono.getElapsedTime() / 1000.0);
                    
                    chrono.start();
                    visual_words_image.set(locations.getData(), codes, locations.size(), original_image.getWidth(), original_image.getHeight(), scale_sizes.getData(), scale_sizes.size());
                    logger.log("Visual words image generated in %.3f seconds.", chrono.getElapsedTime() / 1000.0);
                    
                    histograms = new srv::VisualWordsHistogram[image_offsets[index].size()];
                    labels = new srv::VectorSparse<double, int>[image_offsets[index].size()];
                    n = 0;
                    for (std::list<srv::Tuple<unsigned int, int> >::iterator begin = image_offsets[index].begin(), end = image_offsets[index].end(); begin != end; ++begin, ++n)
                    {
                        unsigned int x, y;
                        
                        x = begin->getFirst() % width - local_region_size / 2;
                        y = begin->getFirst() / width - local_region_size / 2;
                        if (begin->getSecond() >= 0)
                        {
                            labels[n].set(1);
                            labels[n][0].setData(1.0, begin->getSecond());
                        }
                        else labels[n].set(0);
                        histograms[n] = srv::VisualWordsHistogram(x, y, (int)local_region_size, (int)local_region_size, initial_scales[0]);
                    }
                    srv::thousandSeparator(n, text);
                    logger.log("Extracting %s histograms from the image.", text);
                    
                    if (n > 0)
                    {
                        srv::VectorDense<double> normals(n);
                        
                        chrono.start();
                        visual_words_image.extract(histograms, normals, histogram_configuration, number_of_threads);
                        logger.log("Histograms of visual words extracted in %.3f seconds.", chrono.getElapsedTime() / 1000.0);
                        
                        for (unsigned int i = 0; i < n; ++i)
                        {
                            // Add only histograms which have a strong enough norm and also have a set of valid labels (i.e. avoid samples labeled as unknown).
                            if (normals[i] >= normal_threshold)
                            {
                                srv::Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * current_sample;
                                srv::VectorSparse<double, int> current_labels(labels[i].size());
                                srv::VectorSparse<double> current_histogram = histograms[i].getHistogram();
                                
                                for (unsigned int d = 0; d < current_histogram.size(); ++d)
                                    current_histogram[d].getFirst() *= histogram_factor;
                                for (int d = 0; d < labels[i].size(); ++d)
                                    current_labels[d].setData(label_factor * labels[i].getValue(d), labels[i].getIndex(d));
                                current_sample = new srv::Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>(current_histogram, current_labels);
                                histogram_samples.push_back(current_sample);
                            }
                        }
                    }
                    
                    delete [] codes;
                    delete [] histograms;
                    delete [] labels;
                }
            }
            
            delete [] selected_offsets;
        }
    }
    catch (srv::Exception & e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the database images:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
    }
    
    // -[ Save histogram samples to disk ]-----------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Saving histograms of visual words object into '%s' file.", filename_histogram);
        srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::SET_TYPE set_type;
        if      (image_set_identifier == srv::DatabaseInformation::TRAIN_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_SET;
        else if (image_set_identifier == srv::DatabaseInformation::VALIDATION_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::VALIDATION_SET;
        else if (image_set_identifier == srv::DatabaseInformation::TEST_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TEST_SET;
        else if (image_set_identifier == srv::DatabaseInformation::TRAIN_VALIDATION_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_VALIDATION_SET;
        else if (image_set_identifier == srv::DatabaseInformation::TRAIN_TEST_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_TEST_SET;
        else if (image_set_identifier == srv::DatabaseInformation::VALIDATION_TEST_SET) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::VALIDATION_TEST_SET;
        else if (image_set_identifier == srv::DatabaseInformation::ALL_SETS) set_type = srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::ALL_SET;
        else throw srv::Exception("Unknown conversion between database images type and samples type.");
        srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> samples((unsigned int)histogram_samples.size(), histogram_configuration.getNumberOfBins(), set_type, samples_context);
        unsigned int index;
        
        index = 0;
        for (auto &current : histogram_samples)
        {
            samples[index++] = *current;
            delete current;
        }
        
        saveObject(filename_histogram, samples);
    }
    catch (srv::Exception & e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the visual words to the XML file:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
    }
    
    return EXIT_SUCCESS;
}

