// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_database_information.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"
#include "classification_tools.hpp"


INITIALIZE_LINEAR_SVM_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                            09-March-2013\n"
                                   "=================================================================================\n"
                                   " This program creates a probabilistic model for the given set of classifiers.\n\n";
    const double probability_factor = 1.0;
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    // Probability object parameters.
    unsigned int method_index, multiple_categories_index, balance_index, window_size, number_of_bins, gaussian_index;
    unsigned int lut_index, lut_size;
    // Other parameters.
    char filename_train[4096], filename_classifier[4096], filename_probabilities[4096];
    unsigned int number_of_threads;
    bool verbose;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./classifier_probabilities", initial_message);
    srv::LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * linear_classifier;
    srv::ProbabilisticScoreBase<TCLASSIFIER, double> * probability_model;
    srv::StreamLogger logger(&std::cout);
    srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> train_samples;
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        // Define user parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_train", "file with the labeled samples used to generate the probabilistic model of the classifier.", filename_train));
        parameters.addParameter(new srv::ParameterCharArray("filename_classifier", "file with the linear classifiers used to categorize the train samples and obtain the scores.", filename_classifier));
        parameters.addParameter(new srv::ParameterCharArray("filename_probabilities", "file where the resulting probabilistic model is going to be stored.", filename_probabilities));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Probability object parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--prob-model", "index", "identifier of the probabilistic algorithm used to convert scores into probabilities. Possible values are:"
                                "&0&Sigmoid."
                                "&1&Asymmetric-Laplace."
                                "&2&Asymmetric-Gaussian."
                                "&3&Binning."
                                "&4&Isotonic regression.", 0, true, 0, true, 4, &method_index));
        parameters.addParameter(new srv::ParameterUInt("--multiple", "index", "method used to generate the probabilities for multiple class classifiers. Possible values are:"
                                "&0&process each score independently."
                                "&1&create a joint probability estimation.", 0, true, 0, true, 1, &multiple_categories_index));
        parameters.addParameter(new srv::ParameterUInt("--balance", "index", "enables or disables to balance the amount of positive and negative samples while fitting the probabilistic model:"
                                "&0&Disabled."
                                "&1&Enabled.", 0, true, 0, true, 1, &balance_index));
        parameters.addParameter(new srv::ParameterUInt("--window", "scores", "number of neighboring scores selected to calculate the probability at each score.", 20, true, 1, false, 0, &window_size));
        parameters.addParameter(new srv::ParameterUInt("--bins", "bins", "number of bins used to divide the score space.", 20, true, 1, false, 0, &number_of_bins));
        parameters.addParameter(new srv::ParameterUInt("--gaussian", "index", "enables or disables the use of a Gaussian function to weight the contribution of neighboring scores:"
                                "&0&Disabled."
                                "&1&Enabled.", 0, true, 0, true, 1, &gaussian_index));
        parameters.addParameter(new srv::ParameterUInt("--lut", "index", "enables or disables the use of a look-up-table to approximate the probabilistic model:"
                                "&0&Disabled."
                                "&1&Enabled.", 0, true, 0, true, 1, &lut_index));
        parameters.addParameter(new srv::ParameterUInt("--lut-size", "bins", "number of bins of the look-up-table.", 100, true, 1, false, 0, &lut_size));
        // ··············································································································································
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator(" Other parameters:"));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterBoolean("--verbose", "", "show the internal information of the linear learner algorithm.", &verbose));
        parameters.addParameter(new srv::ParameterUInt("--thr", "number_of_threads", "number of threads used to create and calibrate the classifiers concurrently.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parse user parameters.
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load the classifiers ]---------------------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Loading the classifiers...");
        loadObject(filename_classifier, linear_classifier);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the linear classifiers from disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load train samples and generate the scores ]-----------------------------------------------------------------------------------------------
    try
    {
        std::map<NLABEL, bool> category_labels;
        srv::VectorDense<NLABEL> sample_labels;
        
        logger.log("Loading samples...");
        loadObject(filename_train, train_samples);
        train_samples.categoryIdentifiers(sample_labels);
        if (!((train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_SET) || (train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::VALIDATION_SET) || (train_samples.getDataType() == srv::SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::TRAIN_VALIDATION_SET)))
            throw srv::Exception("Test information cannot be used to train the classifier.");
        if (train_samples.getNumberOfDimensions() != linear_classifier->getNumberOfDimensions())
            throw srv::Exception("The dimensionality of the samples (%d) is different than the dimensionality of the classifier (%d)", train_samples.getNumberOfDimensions(), linear_classifier->getNumberOfDimensions());
        
        for (unsigned int i = 0; i < linear_classifier->getNumberOfCategories(); ++i)
            category_labels[linear_classifier->getCategoryIdentifier(i)] = false;
        train_samples.categoryIdentifiers(sample_labels);
        for (unsigned int i = 0; i < sample_labels.size(); ++i)
        {
            std::map<NLABEL, bool>::iterator search = category_labels.find(sample_labels[i]);
            if (search != category_labels.end())
                search->second = true;
        }
        for (std::map<NLABEL, bool>::iterator begin = category_labels.begin(), end = category_labels.end(); begin != end; ++begin)
            if (begin->second == false)
                throw srv::Exception("Any training example for the classifier of the category with identifier '%d'.", begin->first);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the samples from disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Create the probabilistic model ]-----------------------------------------------------------------------------------------------------------
    try
    {
        srv::VectorDense<unsigned int, unsigned int> * labels;
        srv::VectorDense<TCLASSIFIER> * scores;
        srv::Chronometer chrono;
        std::map<NLABEL, unsigned int> label_to_classifier;
        
        logger.log("Generating the scores...");
        scores = new srv::VectorDense<TCLASSIFIER>[train_samples.getNumberOfSamples()];
        labels = new srv::VectorDense<unsigned int, unsigned int>[train_samples.getNumberOfSamples()];
        for (unsigned int i = 0; i < linear_classifier->getNumberOfCategories(); ++i)
            label_to_classifier[linear_classifier->getCategoryIdentifier(i)] = i;
        for (unsigned int i = 0; i < train_samples.getNumberOfSamples(); ++i)
        {
            std::list<unsigned int> valid_classifiers;
            unsigned int index;
            
            for (NLABEL j = 0; j < train_samples[i].getLabels().size(); ++j)
            {
                std::map<NLABEL, unsigned int>::iterator search;
                
                search = label_to_classifier.find(train_samples[i].getLabels().getIndex(j));
                if (search != label_to_classifier.end())
                    valid_classifiers.push_back(search->second);
            }
            labels[i].set((unsigned int)valid_classifiers.size());
            index = 0;
            for (std::list<unsigned int>::iterator begin = valid_classifiers.begin(), end = valid_classifiers.end(); begin != end; ++begin, ++index)
                labels[i][index] = *begin;
        }
        chrono.start();
        linear_classifier->predict(train_samples.getSamples(), scores, train_samples.getNumberOfSamples(), number_of_threads);
        logger.log("The scores of the %d samples have been generated in %f ms.", train_samples.getNumberOfSamples(), chrono.getElapsedTime());
        
        switch (method_index)
        {
        case 0: // Sigmoid.
            probability_model = new srv::ProbabilisticSigmoid<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, balance_index == 1);
            break;
        case 1: // Asymmetric-Laplace
            probability_model = new srv::ProbabilisticAsymmetricLaplace<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, balance_index == 1);
            break;
        case 2: // Asymmetric-Gaussian
            probability_model = new srv::ProbabilisticAsymmetricGaussian<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, balance_index == 1);
            break;
        case 3: // Binning
            probability_model = new srv::ProbabilisticBinning<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, number_of_bins, gaussian_index == 1);
            break;
        case 4: // Isotonic regression
            probability_model = new srv::ProbabilisticIsotonicRegression<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, number_of_bins);
            break;
        default:
            throw srv::Exception("Probabilistic model with index '%d' not yet implemented.", method_index);
        }
        if (lut_index == 1)
        {
            srv::ProbabilisticScoreBase<TCLASSIFIER, double> * aux_model = probability_model;
            
            aux_model->train(scores, labels, train_samples.getNumberOfSamples(), number_of_threads, (verbose)?&logger:0);
            probability_model = new srv::ProbabilisticLUT<TCLASSIFIER, double>(linear_classifier->getNumberOfCategories(), multiple_categories_index == 1, probability_factor, lut_size);
            ((srv::ProbabilisticLUT<TCLASSIFIER, double> *)probability_model)->approximate(aux_model, number_of_threads, (verbose)?&logger:0);
            
            delete aux_model;
        }
        else probability_model->train(scores, labels, train_samples.getNumberOfSamples(), number_of_threads, (verbose)?&logger:0);
        
        delete [] scores;
        delete [] labels;
        delete linear_classifier;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the probabilistic model:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save the probabilistic model to disk ]-----------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Storing the probabilistic object:");
        std::cout << probability_model << std::endl;
        saveObject(filename_probabilities, probability_model);
        delete probability_model;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while saving the probabilistic model to disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

