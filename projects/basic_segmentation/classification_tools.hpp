// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_database_information.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"

typedef double TCLASSIFIER;
typedef float TSAMPLE;
typedef unsigned int NSAMPLE;
typedef unsigned char TLABEL;
typedef char NLABEL;

template <class T>
double calculateFactor(void)
{
    double factor;
    
    if (sizeof(T) <= 2) factor = (double)std::numeric_limits<T>::max();
    else
    {
        if (std::numeric_limits<T>::is_integer) factor = 100000.0;
        else factor = 1.0;
    }
    return factor;
}

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | SET OF SAMPLES -- DECLARATION        |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Container class which manages the information of a set of data samples (vector with
     *  labels) and information related to those samples.
     */
    template <class TSAMPLE, class NSAMPLE = unsigned int, class TLABEL = float, class NLABEL = short>
    class SetOfSamples
    {
    public:
        // -[ Enumerates ]---------------------------------------------------------------------------------------------------------------------------
        /// Enumerate with the type of data stored in the set.
        enum SET_TYPE { TRAIN_SET = 1, VALIDATION_SET, TRAIN_VALIDATION_SET, TEST_SET, TRAIN_TEST_SET, VALIDATION_TEST_SET, ALL_SET };
        /// Enumerate with the context where the samples have been obtained.
        enum CONTEXT { GLOBAL = 1, LOCAL };
        
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SetOfSamples(void);
        /** Constructor which initializes the set of samples structures.
         *  \param[in] number_of_samples number of samples of the set.
         *  \param[in] number_of_dimensions number of dimensions of the samples.
         *  \param[in] data_type type of data used to build the set.
         *  \param[in] context context where the samples are obtained.
         */
        SetOfSamples(unsigned int number_of_samples, unsigned int number_of_dimensions, SET_TYPE data_type, CONTEXT context);
        /// Copy constructor.
        SetOfSamples(const SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        /// Destructor.
        ~SetOfSamples(void);
        /// Assignation operator.
        SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator=(const SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the array of samples of the set.
        inline const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * getSamples(void) const { return m_samples; }
        /// Returns a constant reference to the index-th sample of the set.
        inline const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& getSample(unsigned int index) const { return m_samples[index]; }
        /// Returns a reference to the index-th sample of the set.
        inline Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& getSample(unsigned int index) { return m_samples[index]; }
        /// Returns a constant reference to the index-th sample of the set.
        inline const Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator[](unsigned int index) const { return m_samples[index]; }
        /// Returns a reference to the index-th sample of the set.
        inline Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& operator[](unsigned int index) { return m_samples[index]; }
        /// Returns the number of samples in the set.
        inline unsigned int getNumberOfSamples(void) const { return m_number_of_samples; }
        /// Sets the number of samples in the set.
        void setNumberOfSamples(unsigned int number_of_samples);
        /// Returns the number of dimensions of the set samples.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Sets the number of dimensions of the set samples.
        inline void setNumberOfDimensions(unsigned int number_of_dimensions) { m_number_of_dimensions = number_of_dimensions; }
        /// Returns the type of samples contained in the set.
        inline SET_TYPE getDataType(void) const { return m_data_type; }
        /// Sets the type of samples contained in the set.
        inline void setDataType(SET_TYPE data_type) { m_data_type = data_type; }
        /// Returns the identifier of the context where the samples are obtained.
        inline CONTEXT getContext(void) const { return m_context; }
        /// Sets the identifier of the context where the samples are obtained.
        inline void setContext(CONTEXT context) { m_context = context; }
        
        // -[ Other functions ]----------------------------------------------------------------------------------------------------------------------
        /// Returns an array with the identifiers of the different categories of the set of samples.
        void categoryIdentifiers(VectorDense<NLABEL> &identifiers) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the samples corpus information into an XML file.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the samples corpus information from an XML file.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Array with the samples of the set.
        Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL> * m_samples;
        /// Number of samples of the set.
        unsigned int m_number_of_samples;
        /// Total number of dimensions of the samples.
        unsigned int m_number_of_dimensions;
        /// Type of samples contained in the set.
        SET_TYPE m_data_type;
        /// Context where the samples are obtained.
        CONTEXT m_context;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SET OF SAMPLES -- IMPLEMENTATION     |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::SetOfSamples(void) :
        m_samples(0),
        m_number_of_samples(0),
        m_number_of_dimensions(0),
        m_data_type(ALL_SET),
        m_context(GLOBAL)
    {
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::SetOfSamples(unsigned int number_of_samples, unsigned int number_of_dimensions, SET_TYPE data_type, CONTEXT context) :
        m_samples((number_of_samples > 0)?new Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>[number_of_samples]:0),
        m_number_of_samples(number_of_samples),
        m_number_of_dimensions(number_of_dimensions),
        m_data_type(data_type),
        m_context(context)
    {
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::SetOfSamples(const SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other) :
        m_samples((other.m_number_of_samples != 0)?new Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>[other.m_number_of_samples]:0),
        m_number_of_samples(other.m_number_of_samples),
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_data_type(other.m_data_type),
        m_context(other.m_context)
    {
        for (unsigned int i = 0; i < m_number_of_samples; ++i)
            m_samples[i] = other.m_samples[i];
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::~SetOfSamples(void)
    {
        if (m_samples != 0) delete [] m_samples;
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>& SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::operator=(const SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &other)
    {
        if (this != other)
        {
            if (m_samples != 0) delete [] m_samples;
            
            if (other.m_number_of_samples > 0)
            {
                m_samples = new Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>[other.m_number_of_samples];
                m_number_of_samples = other.m_number_of_samples;
                for (unsigned int i = 0; i < other.m_number_of_samples; ++i)
                    m_samples[i] = other.m_samples[i];
            }
            else
            {
                m_samples = 0;
                m_number_of_samples = 0;
            }
            m_number_of_dimensions = other.m_number_of_dimensions;
            m_data_type = other.m_data_type;
            m_context = other.m_context;
        }
        
        return *this;
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::setNumberOfSamples(unsigned int number_of_samples)
    {
        if (m_samples != 0) delete [] m_samples;
        if (number_of_samples > 0)
        {
            m_number_of_samples = number_of_samples;
            m_samples = new Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>[number_of_samples];
        }
        else
        {
            m_number_of_samples = 0;
            m_samples = 0;
        }
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::categoryIdentifiers(VectorDense<NLABEL> &identifiers) const
    {
        std::set<NLABEL> categories_set;
        unsigned int index;
        
        for (unsigned int i = 0; i < m_number_of_samples; ++i)
            for (unsigned int j = 0; j < (unsigned int)m_samples[i].getLabels().size(); ++j)
                categories_set.insert(m_samples[(NSAMPLE)i].getLabels().getIndex((NLABEL)j));
        identifiers.set((unsigned int)categories_set.size());
        index = 0;
        for (typename std::set<NLABEL>::iterator begin = categories_set.begin(), end = categories_set.end(); begin != end; ++begin, ++index)
            identifiers[index] = *begin;
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("SetOfSamples");
        parser.setAttribute("Number_Of_Samples", m_number_of_samples);
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Data_Type", (int)m_data_type);
        parser.setAttribute("Context", (int)m_context);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_samples; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_samples[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("SetOfSamples"))
        {
            if (m_samples != 0) delete [] m_samples;
            
            m_number_of_samples = parser.getAttribute("Number_Of_Samples");
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_data_type = (SET_TYPE)((int)parser.getAttribute("Data_Type"));
            m_context = (CONTEXT)((int)parser.getAttribute("Context"));
            
            if (m_number_of_samples > 0) m_samples = new Sample<TSAMPLE, NSAMPLE, TLABEL, NLABEL>[m_number_of_samples];
            else m_samples = 0;
            
            while (!(parser.isTagIdentifier("SetOfSamples") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("Sample"))
                {
                    unsigned int index;
                    
                    index = parser.getAttribute("ID");
                    m_samples[index].convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /** Saves a set of samples into a compressed XML object.
     *  \param[in] filename XML file where the set of samples is going to be stored.
     *  \param[in] samples set of samples stored into the file.
     */
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void saveObject(const char * filename, const SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &samples)
    {
        compressionPolicy(ZLibCompression);
        std::ostream * file = oZipStream(filename);
        if (file == 0)
            throw srv::Exception("Cannot open file '%s'.", filename);
        srv::XmlParser parser(file);
        samples.convertToXML(parser);
        delete file;
    }
    
    /** Loads a set of samples from a compressed XML object.
     *  \param[in] filename XML file containing the samples.
     *  \param[out] samples loaded set of samples.
     */
    template <class TSAMPLE, class NSAMPLE, class TLABEL, class NLABEL>
    void loadObject(const char * filename, SetOfSamples<TSAMPLE, NSAMPLE, TLABEL, NLABEL> &samples)
    {
        compressionPolicy(ZLibCompression);
        std::istream * file = iZipStream(filename);
        if (file == 0)
            throw srv::Exception("Cannot open file '%s'.", filename);
        srv::XmlParser parser(file);
        samples.convertFromXML(parser);
        delete file;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

