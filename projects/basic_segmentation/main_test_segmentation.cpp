// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_classifier.hpp"
#include "classification_tools.hpp" // for the classifier type declarations.

INITIALIZE_DENSE_VISUAL_WORDS(short, short, unsigned char, int, unsigned int)
INITIALIZE_LINEAR_SVM_FACTORIES(TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL)
INITIALIZE_PROBABILISTIC_SCORE_FACTORIES(TCLASSIFIER, double)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              14-May-2013\n"
                                   "=================================================================================\n"
                                   " Test program which locally applies the classifier over an image.\n\n";
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char visual_words_filename[4096], classifier_filename[4096], probabilities_filename[4096], image_filename[4096];
    unsigned int local_region_size, number_of_threads;
    double normal_threshold;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::DenseVisualWords<short, short, unsigned char, int, unsigned int> visual_words;
    srv::LinearLearnerBase<TCLASSIFIER, TSAMPLE, NSAMPLE, TLABEL, NLABEL> * linear_classifier;
    srv::ParameterParser parameters("./test_segmentation", initial_message);
    srv::ProbabilisticScoreBase<TCLASSIFIER, double> * probability_model;
    srv::LinearQuantized<int> quantized_classifier;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        // Define user parameters.
        parameters.addParameter(new srv::ParameterCharArray("visual_words_filename", "file with the dense visual words object information.", visual_words_filename));
        parameters.addParameter(new srv::ParameterCharArray("classifier_filename", "file containing the classifiers which are going to be tested.", classifier_filename));
        parameters.addParameter(new srv::ParameterCharArray("image_filename", "image where the basic segmentation algorithm is applied to.", image_filename));
        parameters.addParameter(new srv::ParameterCharArray("--probability", "filename", "file containing the probabilistic model which converts scores into probabilities.", probabilities_filename));
        parameters.addParameter(new srv::ParameterDouble("--normal-threshold", "value", "minimum value of the normal of the histogram of visual words to reject the histogram.", 10, true, 0.0, false, 0.0, &normal_threshold));
        parameters.addParameter(new srv::ParameterUInt("--local-region-size", "pixels", "size of the local squared regions where the local histograms are extracted from.", 40, true, 20, false, 0, &local_region_size));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently process the image.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parse user parameters.
        probabilities_filename[0] = '\0';
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load information from disk ]---------------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Loading structures from disk.");
        
        loadObject(visual_words_filename, visual_words);
        loadObject(classifier_filename, linear_classifier);
        if (probabilities_filename[0] != '\0')
            loadObject(probabilities_filename, probability_model);
        else probability_model = 0;
        
        quantized_classifier.set(linear_classifier, 1000, false);
    }
    catch (srv::Exception & e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the structures from disk:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
    }
    
    // -[ Process image ]----------------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::VectorDense<srv::DescriptorLocation> locations;
        srv::VectorSparse<int, unsigned int> * codes;
        srv::Image<int> * image_scores, image_normal;
        srv::Image<unsigned char> image;
        srv::IntegralImage<long> * integral_scores, integral_normal;
        srv::Image<TCLASSIFIER> * classification_scores;
        srv::Image<unsigned char> zero_mask; //< Show as black the pixels which the classifier does not provide any score (border and low-gradient regions).
        srv::VectorDense<TCLASSIFIER * > classification_ptrs(linear_classifier->getNumberOfCategories());
        srv::VectorDense<TCLASSIFIER> classifier_constant(linear_classifier->getNumberOfCategories());
        srv::Draw::Pencil<unsigned char> red_pencil("FF0000", "00000000", 2, false);
        srv::Chronometer chrono;
        
        image.load(image_filename);
        // A) Set image structures.
        logger.log("Initializing structures.");
        image_scores = new srv::Image<int>[linear_classifier->getNumberOfCategories()];
        integral_scores = new srv::IntegralImage<long>[linear_classifier->getNumberOfCategories()];
        classification_scores = new srv::Image<TCLASSIFIER>[linear_classifier->getNumberOfCategories()];
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
        {
            image_scores[c].setGeometry(image.getWidth(), image.getHeight(), 1);
            classification_scores[c].setGeometry(image.getWidth(), image.getHeight(), 1);
            classifier_constant[c] = (TCLASSIFIER)quantized_classifier.getNumberOfQuantizationBins() / (quantized_classifier.getMaximumValue(c) - quantized_classifier.getMinimumValue(c));
        }
        image_normal.setGeometry(image.getWidth(), image.getHeight(), 1);
        zero_mask.setGeometry(image.getWidth(), image.getHeight(), 3);
        zero_mask.setValue(0);
        
        // B) Extract the visual words from the image.
        logger.log("Processing image.");
        codes = 0;
        chrono.start();
        visual_words.extract(image, locations, codes, number_of_threads);
        logger.log("%d visual words extracted in %f seconds.", locations.size(), chrono.getElapsedTime() / 1000.0);
        
        // C) Generate the classifier weights and normal images.
        chrono.start();
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
            image_scores[c].setValue(0);
        image_normal.setValue(0);
        for (unsigned int k = 0; k < locations.size(); ++k)
        {
            unsigned int region_size, x, y;
            
            region_size = visual_words.getDenseDescriptors()->getScalesInformation(locations[k].getScale()).getRegionSize();
            x = locations[k].getX() + region_size / 2;
            y = locations[k].getY() + region_size / 2;
            for (unsigned int m = 0; m < codes[k].size(); ++m)
            {
                for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    *image_scores[c].get(x, y, 0) += codes[k].getValue(m) * quantized_classifier.getClassifier(c)[codes[k].getIndex(m)];
                *image_normal.get(x, y, 0) += codes[k].getValue(m);
            }
        }
        
        // D) Build the integral images for each classifier weights and normal images.
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
            integral_scores[c].set(image_scores[c], number_of_threads);
        integral_normal.set(image_normal, number_of_threads);
        
        // E) Calculate the classification score at each pixel of the image.
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
            classification_scores[c].setValue(0.0);
        for (unsigned int y = 0; y < image.getHeight() - local_region_size; ++y)
        {
            unsigned char * zero_mask_ptr[] = { zero_mask.get(local_region_size / 2, y + local_region_size / 2, 0), zero_mask.get(local_region_size / 2, y + local_region_size / 2, 1), zero_mask.get(local_region_size / 2, y + local_region_size / 2, 2) };
            for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                classification_ptrs[c] = classification_scores[c].get(local_region_size / 2, y + local_region_size / 2, 0);
            for (unsigned int x = 0; x < image.getWidth() - local_region_size; ++x)
            {
                const long normal_value = integral_normal.getSum(x, y, local_region_size, local_region_size);
                
                if ((double)normal_value > normal_threshold)
                {
                    for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    {
                        TCLASSIFIER dot_product, score;
                        
                        // Calculate the quantized dot product.
                        dot_product = (TCLASSIFIER)integral_scores[c].getSum(x, y, local_region_size, local_region_size) / (TCLASSIFIER)normal_value;
                        // Remove the quantization scale.
                        score  = (TCLASSIFIER)dot_product / classifier_constant[c];
                        // Remove the quantization minimum.
                        score += quantized_classifier.getMinimumValue(c);
                        *classification_ptrs[c] = score;
                        *zero_mask_ptr[2] = *zero_mask_ptr[1] = *zero_mask_ptr[0] = 1;
                    }
                }
                
                for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    ++classification_ptrs[c];
                ++zero_mask_ptr[0];
                ++zero_mask_ptr[1];
                ++zero_mask_ptr[2];
            }
        }
        logger.log("Classifier scores generated in %f seconds.", chrono.getElapsedTime() / 1000.0);
        
        // F) Convert the classification scores into probabilities.
        if (probability_model != 0)
        {
            // This is not efficient, but calling a virtual function for each pixel will be slower.
            srv::VectorDense<TCLASSIFIER> * score_vector, * probability_vector;
            score_vector = new srv::VectorDense<TCLASSIFIER>[image.getWidth() * image.getHeight()];
            probability_vector = new srv::VectorDense<TCLASSIFIER>[image.getWidth() * image.getHeight()];
            
            // F.a) Get the scores from the images and store them into the vector.
            for (unsigned int y = 0, k = 0; y < image.getHeight(); ++y)
            {
                for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    classification_ptrs[c] = classification_scores[c].get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x, ++k)
                {
                    score_vector[k].set(linear_classifier->getNumberOfCategories());
                    for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    {
                        score_vector[k][c] = *classification_ptrs[c];
                        ++classification_ptrs[c];
                    }
                }
            }
            // F.b) Convert the scores into probabilities.
            probability_model->probabilities(score_vector, probability_vector, image.getWidth() * image.getHeight(), number_of_threads);
            // F.c) Copy the probabilities from the vector to the images.
            for (unsigned int y = 0, k = 0; y < image.getHeight(); ++y)
            {
                for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    classification_ptrs[c] = classification_scores[c].get(y, 0);
                for (unsigned int x = 0; x < image.getWidth(); ++x, ++k)
                {
                    for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                    {
                        *classification_ptrs[c] = probability_vector[k][c];
                        ++classification_ptrs[c];
                    }
                }
            }
            delete [] score_vector;
            delete [] probability_vector;
        }
        
        logger.log("Showing results (press [ESC] to exit).");
        srv::Figure figure_image("Image"), * * figure_scores;
        
        TCLASSIFIER maximum_value, minimum_value;
        minimum_value = 1e20;
        maximum_value = -1e20;
        for (unsigned int y = 0; y < image.getHeight(); ++y)
        {
            for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                classification_ptrs[c] = classification_scores[c].get(y, 0);
            for (unsigned int x = 0; x < image.getWidth(); ++x)
            {
                for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
                {
                    if (*classification_ptrs[c] < minimum_value)
                        minimum_value = *classification_ptrs[c];
                    if (*classification_ptrs[c] > maximum_value)
                        maximum_value = *classification_ptrs[c];
                    ++classification_ptrs[c];
                }
            }
        }
        
        srv::Draw::Rectangle(image, local_region_size / 2, local_region_size / 2, image.getWidth() - local_region_size / 2, image.getHeight() - local_region_size / 2, red_pencil);
        figure_image(image);
        figure_scores = new srv::Figure * [linear_classifier->getNumberOfCategories()];
        //std::cout << maximum_value << " " << minimum_value << std::endl;
        
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
        {
            char figure_name[128];
            sprintf(figure_name, "Category %d", c);
            
            figure_scores[c] = new srv::Figure(figure_name);
            if (probability_model != 0)
                (*figure_scores[c])(zero_mask * srv::ImageGray2HueColormap(255.0 * classification_scores[c]));
            else
                (*figure_scores[c])(zero_mask * srv::ImageGray2HueColormap(255.0 * (classification_scores[c] - minimum_value) / (maximum_value - minimum_value)));
        }
        for (char key = 0; key != 27; key = srv::Figure::wait());
        
        // Free allocated memory.
        delete [] image_scores;
        delete [] integral_scores;
        delete [] codes;
        delete [] classification_scores;
        for (unsigned int c = 0; c < linear_classifier->getNumberOfCategories(); ++c)
            delete figure_scores[c];
        delete [] figure_scores;
        
        delete linear_classifier;
        if (probability_model != 0) delete probability_model;
    }
    catch (srv::Exception & e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the image:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
    }
    
    return EXIT_SUCCESS;
}

