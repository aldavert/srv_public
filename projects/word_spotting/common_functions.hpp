// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#ifndef __COMMON_FUNCTIONS_HEADER_FILE__
#define __COMMON_FUNCTIONS_HEADER_FILE__

#include "srv_utilities.hpp"
#include "srv_xml.hpp"
#include "srv_vector.hpp"
#include "srv_visual_words.hpp"
#include "srv_documents.hpp"
#include <limits>

#define DEBUG 0
#define DEBUGRANK 0

#if 0
// Definitions which disable quantization.
typedef float DEF_TVALUE;
typedef unsigned short DEF_TINDEX;
#else
// Definitions that enable quantization of the scores in the inverted file.
// typedef char DEF_TVALUE; /* WITH CHAR TOO MUCH INFORMATION IS LOST IN THE QUANTIZATION */
typedef unsigned char DEF_TVALUE;
typedef unsigned short DEF_TINDEX;
#endif

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS AND ENUMERATIONS |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Compute the identifier of a word snippet given its position within the document.
    unsigned long calculateSnippetIdentifier(unsigned int page_number, unsigned int block_number, unsigned int line_number, unsigned int position_number);
    /// Retrieves the symbol position within the document from its identifier.
    void retrieveSnippetLocation(unsigned long snippet_identifier, unsigned int &page_number, unsigned int &block_number, unsigned int &line_number, unsigned int &position_number);
    
    /// Enumeration of the different method available to pool the codewords in the histogram of visual words.
    enum POOLING_METHOD_IDENTIFIER { POOLING_ADDITIVE = 7738001,          ///< Add the weights of the different codewords.
                                     POOLING_MAXIMUM,                     ///< Set the weight of the visual words which attained the highest response.
                                     POOLING_ABSOLUTE_MAXIMUM             ///< Set the weight of the visual words which attained the highest absolute response.
    };
    /// Enumeration of the different options available to deal with negative visual word contributions.
    enum NEGATIVE_METHOD_IDENTIFIER { NEGATIVE_NOTHING = 7738101,         ///< Do nothing, i.e. treat them the same way as positive contributions.
                                      NEGATIVE_ABSOLUTE,                  ///< Do not take into account the sign of the visual word (i.e. apply the absolute value operator).
                                      NEGATIVE_SEPARATE                   ///< Separate the positive and negative contributions into different bins.
    };
    
    /// Enumeration of the distances available to rank the snippets.
    enum INDEX_DISTANCE_IDENTIFIER { DOCUMENT_INDEX_DISTANCE_EUCLIDEAN = 7738201,        ///< Use the Euclidean distance as similarity measure.
                                     DOCUMENT_INDEX_DISTANCE_MANHATTAN                   ///< Use the Manhattan distance as similarity measure.
    };
    
    /// Enumeration with the identifiers of the different methods available to evaluate the performance of the visual signatures.
    enum RANK_SCORE_IDENTIFIER { RANK_SCORE_AVERAGE_PRECISION = 7738501,             ///< Calculate the average precision.
                                 RANK_SCORE_PRECISION,                               ///< Calculate the precision at a certain rank.
                                 RANK_SCORE_PRECISION_AT_RECALL                      ///< Calculate the precision at a certain recall.
    };
    
    /// Quantizes a signature values that ranges [0, 1] or [-1 to 1] (when the signature has negative values, the quantization has to be done with a signed data type).
    template <class TDATA> inline TDATA quantizeValue(float value) { return (TDATA)value; }
    template <> inline char quantizeValue<char>(float value) { return (char)(127.0f * value); }
    template <> inline unsigned char quantizeValue<unsigned char>(float value) { return (char)(255.0f * value); }
    template <> inline short quantizeValue<short>(float value) { return (short)(10000.0f * value); }
    template <> inline unsigned short quantizeValue<unsigned short>(float value) { return (unsigned short)(10000.0f * value); }
    template <> inline int quantizeValue<int>(float value) { return (int)(10000.0f * value); }
    template <> inline unsigned int quantizeValue<unsigned int>(float value) { return (unsigned int)(10000.0f * value); }
    template <> inline long quantizeValue<long>(float value) { return (long)(10000.0f * value); }
    template <> inline unsigned long quantizeValue<unsigned long>(float value) { return (unsigned long)(10000.0f * value); }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SNIPPET INFORMATION                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container storing all the information needed to represent a word snippet with a BoVW signature.
    class SnippetInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        SnippetInformation(void);
        /// Copy constructor.
        SnippetInformation(const SnippetInformation &other);
        /// Destructor.
        ~SnippetInformation(void);
        /// Assignation operator.
        SnippetInformation& operator=(const SnippetInformation &other);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /** Initializes the content of the snippet information object.
         *  \param[in] snippet_identifier identifier of the word snippet in the document.
         *  \param[in] word_category_id identifier of the word category.
         *  \param[in] x0 X-coordinate of the top-left corner of the snippet.
         *  \param[in] y0 Y-coordinate of the top-left corner of the snippet.
         *  \param[in] x1 X-coordinate of the bottom-right corner of the snippet.
         *  \param[in] y1 Y-coordinate of the bottom-right corner of the snippet.
         *  \param[in] locations vector with the coordinates of each descriptor.
         *  \param[in] descriptors array with the vectors extracted at each location.
         *  \param[in] visual_words object used to encode the descriptors.
         *  \param[in] number_of_neighbors maximum number of closest codewords stored for each descriptor.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances between descriptors and codewords.
         */
        void initialize(unsigned long snippet_identifier, unsigned int word_category_id, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, const VectorDense<DescriptorLocation> &locations, const VectorDense<unsigned char> * descriptors, const DenseVisualWords<short, short, unsigned char, int, unsigned int> &visual_words, unsigned int number_of_neighbors, unsigned int number_of_threads);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the relative X-coordinate of the index-th descriptor.
        inline float getX(unsigned int index) const { return (float)m_x[index] / (float)std::numeric_limits<short>::max(); }
        /// Returns the relative Y-coordinate of the index-th descriptor.
        inline float getY(unsigned int index) const { return (float)m_y[index] / (float)std::numeric_limits<short>::max(); }
        /// Returns a constant reference to the index-th descriptor of the word snippet.
        inline const ConstantSubVectorDense<unsigned char>& getDescriptor(unsigned int index) const { return m_descriptor[index]; }
        /// Returns an array with the distances of the closer codewords to the index-th descriptor.
        inline const unsigned int * getDescriptorNeighborDistance(unsigned int index) const { return &m_neighbor_distance[m_number_of_distances * index]; }
        /// Returns an array with the indexes of the closer codewords to the index-th descriptor.
        inline const unsigned int * getDescriptorNeighborIndex(unsigned int index) const { return &m_neighbor_index[m_number_of_distances * index]; }
        /// Number of descriptors extracted from the word snippet.
        inline unsigned int getNumberOfDescriptors(void) const { return m_number_of_descriptors; }
        /// Maximum number of distances to the closest codewords precomputed for each descriptor.
        inline unsigned int getNumberOfDistances(void) const { return m_number_of_distances; }
        /// Returns the number of dimensions of the descriptor.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns the word label identifier.
        inline unsigned int getWordCategoryID(void) const { return m_word_category_id; }
        /// Return the snippet identifier.
        inline unsigned long getSnippetIdentifier(void) const { return m_snippet_identifier; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the container information into a XML object.
        void convertToXML(XmlParser &parser);
        /// Retrieves the container information from a XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Relative X-coordinate of the descriptors in the word snippet.
        unsigned short * m_x;
        /// Relative Y-coordinate of the descriptors in the word snippet.
        unsigned short * m_y;
        /// Vector which stores all the values of the snippet descriptors.
        VectorDense<unsigned char> m_descriptor_values;
        /// Array with the constant sub-vector to each descriptor of the snippet.
        ConstantSubVectorDense<unsigned char> * m_descriptor;
        /// Array with the distance between the descriptors and the N-nearest codewords.
        unsigned int * m_neighbor_distance;
        /// Array with the index of the N-nearest codewords and the descriptors.
        unsigned int * m_neighbor_index;
        /// Number of descriptors.
        unsigned int m_number_of_descriptors;
        /// Number of nearest codewords computed for each descriptor.
        unsigned int m_number_of_distances;
        /// Number of dimensions of the descriptor.
        unsigned int m_number_of_dimensions;
        /// Word label identifier.
        unsigned int m_word_category_id;
        /// Snippet identifier.
        unsigned long m_snippet_identifier;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INFORMATION          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Class containing all the information needed to create a BoVW index of a document database.
    class DocumentVisualInformation
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        DocumentVisualInformation(void);
        /// Copy constructor.
        DocumentVisualInformation(const DocumentVisualInformation &other);
        /// Destructor.
        ~DocumentVisualInformation(void);
        /// Assignation operator.
        DocumentVisualInformation& operator=(const DocumentVisualInformation &other);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /** Initializes (extract) the visual information of the document dataset.
         *  \param[in] database object with the document information.
         *  \param[in] visual_words object used to encode the descriptors.
         *  \param[in] number_of_neighbors maximum number of closest codewords stored for each descriptor.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances between descriptors and codewords.
         *  \param[in] logger logger object used to show generation messages (set to null to not show any message).
         */
        void initialize(const DocumentDatabase &database, const DenseVisualWords<short, short, unsigned char, int, unsigned int> &visual_words, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger = 0);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant reference to the index-th snippet information object.
        inline const SnippetInformation& getSnippetInformation(unsigned int index) const { return m_snippet_information[index]; }
        /// Returns a reference to the index-th snippet information object.
        inline SnippetInformation& getSnippetInformation(unsigned int index) { return m_snippet_information[index]; }
        /// Returns a constant reference to the index-th snippet information object.
        inline const SnippetInformation& operator[](unsigned int index) const { return m_snippet_information[index]; }
        /// Returns a reference to the index-th snippet information object.
        inline SnippetInformation& operator[](unsigned int index) { return m_snippet_information[index]; }
        /// Returns a constant reference to the snippet information object at the given location.
        inline const SnippetInformation& getSnippetInformation(unsigned int page, unsigned int block, unsigned int line, unsigned int position)
        {
            const unsigned long identifier = calculateSnippetIdentifier(page, block, line, position);
            for (unsigned int i = 0; i < m_number_of_snippets; ++i)
                if (m_snippet_information[i].getSnippetIdentifier() == identifier)
                    return m_snippet_information[i];
            throw Exception("The selected snippet at page=%d block=%d line=%d position=%d not found.", page, block, line, position);
        }
        /// Returns the number of snippets in the document.
        inline unsigned int getNumberOfSnippets(void) const { return m_number_of_snippets; }
        /// Return the text label of the index-th word category in the database.
        inline const char * getWord(unsigned int index) const { return &m_word_text[m_word_offset[index]]; }
        /// Vector with the offset the labels to each category in the database.
        inline unsigned int getNumberOfWords(void) const { return m_word_offset.size(); }
        /// Vector with the text string of all unique words of the document.
        inline const VectorDense<char>& getWordTextVector(void) const { return m_word_text; }
        /// Vector with the offset to each unique word text string.
        inline const VectorDense<unsigned int>& getWordOffsetVector(void) const { return m_word_offset; }
        /// Returns the maximum number of neighbors of pre-computed distances for each descriptor.
        inline unsigned int getMaximumNumberOfNeighbors(void) const { return m_maximum_number_of_neighbors; }
        /// Returns the number of codewords of the codebook.
        inline unsigned int getNumberOfCodewords(void) const { return m_number_of_codewords; }
        /// Returns the number of dimensions of the descriptor.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns a constant reference to the index-th codeword of the codebook.
        inline const VectorDense<double>& getCodeword(unsigned int index) const { return m_codewords[index]; }
        /// Returns a constant pointer to the array of codebook's codewords.
        inline const VectorDense<double> * getCodewords(void) const { return m_codewords; }
        /// Returns a constant reference to the index-th codeword Gaussian model.
        inline const CodewordGaussianModel<double>& getCodewordModel(unsigned int index) const { return m_codeword_models[index]; }
        /// Returns a constant pointer to the array of codeword Gaussian models.
        inline const CodewordGaussianModel<double> * getCodewordModels(void) const { return m_codeword_models; }
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the document visual information into a XML object.
        void convertToXML(XmlParser &parser);
        /// Retrieve the document visual information from a XML object.
        void convertFromXML(XmlParser &parser);
        
    protected:
        /// Array with the information of each document word snippet.
        SnippetInformation * m_snippet_information;
        /// Number of segment words in the document.
        unsigned int m_number_of_snippets;
        /// Vector with the text labels of all the categories in the database.
        VectorDense<char> m_word_text;
        /// Vector with the offset the labels to each category in the database.
        VectorDense<unsigned int> m_word_offset;
        /// Maximum number of neighbors of pre-computed distances for each descriptor.
        unsigned int m_maximum_number_of_neighbors;
        /// Number of codewords in the codebook.
        unsigned int m_number_of_codewords;
        /// Number of dimensions of the descriptor.
        unsigned int m_number_of_dimensions;
        /// Array with the codewords of the codebook.
        VectorDense<double> * m_codewords;
        /// Array with the Gaussian model of each codeword.
        CodewordGaussianModel<double> * m_codeword_models;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RANK INFORMATION - DECLARATION       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Base rank score measure object.
    class RankScore
    {
    public:
        /// Default constructor.
        RankScore(void) {}
        /// Destructor.
        virtual ~RankScore(void) {}
        /// Returns the score for the current ranked list.
        virtual double evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const = 0;
        /// Returns the identifier of the scoring method.
        virtual RANK_SCORE_IDENTIFIER getIdentifier(void) const = 0;
        /// Returns a copy of the scoring method.
        virtual RankScore * duplicate(void) const = 0;
        /// Retrieves the rank-score information from a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Stores the rank-score information into a XML object.
        void convertFromXML(XmlParser &parser);
        /// Returns the name of the rank score method.
        inline const char * getName(void) const { return m_name.c_str(); }
        /// Returns the abbreviation of the rank score method.
        inline const char * getAbbreviation(void) const { return m_abbreviation.c_str(); }
    protected:
        /// Stores the rank-score object attributes into a XML object.
        virtual void convertToXMLAttributes(XmlParser &parser) const = 0;
        /// Retrieves the rank-score object attributes from a XML object.
        virtual void convertFromXMLAttributes(XmlParser &parser) = 0;
        /// Name of the rank score.
        std::string m_name;
        /// Abbreviation of the rank score.
        std::string m_abbreviation;
    };
    
    /// Creates a rank score object of the type specified by the identifier.
    RankScore * createScore(RANK_SCORE_IDENTIFIER identifier);
    
    /// Calculates the average precision of a given rank list.
    class RankScoreAP : public RankScore
    {
    public:
        /// Default constructor.
        RankScoreAP(void) : RankScore(), m_rank(-1) { this->m_name = "mean Average Precision"; this->m_abbreviation = "mAP"; }
        /// Constructor which sets the maximum position considered while calculating the average precision.
        RankScoreAP(int rank) : m_rank(rank)
        {
            if (rank < 0) { this->m_name = "mean Average Precision"; this->m_abbreviation = "mAP"; }
            else { this->m_name = "mean Average Precision at rank " + std::to_string(rank); this->m_abbreviation = "mAP@rank=" + std::to_string(rank); }
        }
        /// Sets the maximum position considered while calculating the average precision (set it to -1 to consider all results).
        inline void setRank(int rank)
        {
            m_rank = rank;
            if (rank < 0) { this->m_name = "mean Average Precision"; this->m_abbreviation = "mAP"; }
            else { this->m_name = "mean Average Precision at rank " + std::to_string(rank); this->m_abbreviation = "mAP@rank=" + std::to_string(rank); }
        }
        /// Return the maximum position considered while calculating the average precision.
        inline int getRank(void) const { return m_rank; }
        /// Returns the average precision of the given ranked list.
        double evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const;
        /// Returns the identifier of the scoring method.
        inline RANK_SCORE_IDENTIFIER getIdentifier(void) const { return RANK_SCORE_AVERAGE_PRECISION; }
        /// Returns a copy of the scoring method.
        inline RankScore * duplicate(void) const { return (RankScore *)new RankScoreAP(*this); }
    protected:
        /// Stores the rank-score object attributes into a XML object.
        void convertToXMLAttributes(XmlParser &parser) const { parser.setAttribute("rank", m_rank); }
        /// Retrieves the rank-score object attributes from a XML object.
        void convertFromXMLAttributes(XmlParser &parser)
        {
            m_rank = parser.getAttribute("rank");
            if (m_rank < 0) { this->m_name = "mean Average Precision"; this->m_abbreviation = "mAP"; }
            else { this->m_name = "mean Average Precision at rank " + std::to_string(m_rank); this->m_abbreviation = "mAP@rank=" + std::to_string(m_rank); }
        }
        /// Maximum position considered while calculating the average precision (set it to -1 to consider all the results).
        int m_rank;
    };
    
    /// Calculates the retrieval precision taking into account the X top positions.
    class RankScorePrecision : public RankScore
    {
    public:
        /// Default constructor.
        RankScorePrecision(void) : m_rank(1) { this->m_name = "precision at rank 1"; this->m_abbreviation = "P@rank=1"; }
        /// Constructor which sets the amount of closest positions considered.
        RankScorePrecision(unsigned int rank) : RankScore(), m_rank(rank) { this->m_name = "precision at rank " + std::to_string(rank); this->m_abbreviation = "P@rank=" + std::to_string(rank); }
        /// Sets the amount of closest positions considered to compute the precision.
        inline void setRank(unsigned int rank)
        {
            m_rank = rank;
            this->m_name = "precision at rank " + std::to_string(rank);
            this->m_abbreviation = "P@rank=" + std::to_string(rank);
        }
        /// Returns the amount of closest positions considered to compute the precision.
        inline unsigned int getRank(void) const { return m_rank; }
        /// Returns the precision of the given ranked list.
        double evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const;
        /// Returns the identifier of the scoring method.
        inline RANK_SCORE_IDENTIFIER getIdentifier(void) const { return RANK_SCORE_PRECISION; }
        /// Returns a copy of the scoring method.
        inline RankScore * duplicate(void) const { return (RankScore *)new RankScorePrecision(*this); }
    protected:
        /// Stores the rank-score object attributes into a XML object.
        void convertToXMLAttributes(XmlParser &parser) const { parser.setAttribute("rank", m_rank); }
        /// Retrieves the rank-score object attributes from a XML object.
        void convertFromXMLAttributes(XmlParser &parser)
        {
            m_rank = parser.getAttribute("rank");
            this->m_name = "precision at rank " + std::to_string(m_rank);
            this->m_abbreviation = "P@rank=" + std::to_string(m_rank);
        }
        /// Maximum number of positions considered to calculate the precision.
        unsigned int m_rank;
    };
    
    /// Calculates the precision at a given recall.
    class RankScorePrecisionAtRecall : public RankScore
    {
    public:
        /// Default constructor.
        RankScorePrecisionAtRecall(void) : m_recall(1.0) { this->m_name = "precision at recall 1.0"; this->m_abbreviation = "P@R=1"; }
        /// Constructor which sets the minimum recall considered.
        RankScorePrecisionAtRecall(double recall) : RankScore(), m_recall(recall) { this->m_name = "precision at recall " + std::to_string(recall); this->m_abbreviation = "P@R=" + std::to_string(recall); }
        /// Sets the minimum recall considered.
        inline void setRecall(double recall)
        {
            m_recall = recall;
            this->m_name = "precision at recall " + std::to_string(recall);
            this->m_abbreviation = "P@R=" + std::to_string(recall);
        }
        /// Returns the minimum recall considered.
        inline double getRecall(void) const { return m_recall; }
        /// Returns the precision at the required recall of the given ranked list.
        double evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const;
        /// Returns the identifier of the scoring method.
        inline RANK_SCORE_IDENTIFIER getIdentifier(void) const { return RANK_SCORE_PRECISION_AT_RECALL; }
        /// Returns a copy of the scoring method.
        inline RankScore * duplicate(void) const { return (RankScore *)new RankScorePrecisionAtRecall(*this); }
    protected:
        /// Stores the rank-score object attributes into a XML object.
        void convertToXMLAttributes(XmlParser &parser) const { parser.setAttribute("recall", m_recall); }
        /// Retrieves the rank-score object attributes from a XML object.
        void convertFromXMLAttributes(XmlParser &parser)
        {
            m_recall = parser.getAttribute("recall");
            this->m_name = "precision at recall " + std::to_string(m_recall);
            this->m_abbreviation = "P@R=" + std::to_string(m_recall);
        }
        /// Minimum recall required at each rank.
        double m_recall;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SNIPPET QUERY INFORMATION            |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Information of the snippets which are searchable and/or queried.
    class SnippetQueryInformation
    {
    public:
        /// Default constructor.
        SnippetQueryInformation(void) : m_identifier(-1), m_enabled(false), m_query(false) {}
        /// Parameter constructor.
        SnippetQueryInformation(unsigned long identifier, bool searchable, bool query) : m_identifier(identifier), m_enabled(searchable), m_query(query) {}
        /// Sets the query parameters of the snippet.
        inline void set(unsigned long identifier, bool searchable, bool query) { m_identifier = identifier; m_enabled = searchable; m_query = query; }
        /// Sets the unique identifier of the snippet.
        inline void setIdentifier(unsigned long identifier) { m_identifier = identifier; }
        /// Returns the unique identifier of the snippet.
        inline unsigned long getIdentifier(void) const { return m_identifier; }
        /// Sets the searchable flag of the snippet.
        inline void setSearchable(bool enabled) { m_enabled = enabled; }
        /// Returns the searchable flag of the snippet.
        inline bool getSearchable(void) const { return m_enabled; }
        /// Sets the enabled query flag of the snippet.
        inline void setQuery(bool query) { m_query = query; }
        /// Returns the enabled query flag of the snippet.
        inline bool getQuery(void) const { return m_query; }
        /// Stores the snippet query information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the snippet query information from a XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Unique identifier of the word snippet.
        unsigned long m_identifier;
        /// Enables the snippet as searchable.
        bool m_enabled;
        /// Enables the snippet as possible query.
        bool m_query;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | TRIAL INFORMATION                    |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container with all the snippets set as searchable and/or queried for the trial.
    class TrialInformation
    {
    public:
        /// Default constructor.
        TrialInformation(void) {}
        /// Parameters constructor.
        TrialInformation(unsigned int number_of_snippets, const char * name) : m_snippets(number_of_snippets), m_name(name) {}
        /// Sets the number of queries snippets information of the trial.
        void set(unsigned int number_of_snippets) { m_snippets.set(number_of_snippets); }
        /// Returns the number of query snippet information objects of the trial.
        inline unsigned int getNumberOfSnippets(void) const { return m_snippets.size(); }
        /// Returns a reference to the index-th query snippet information.
        SnippetQueryInformation& getSnippet(unsigned int index) { return m_snippets[index]; }
        /// Returns a reference to the index-th query snippet information.
        const SnippetQueryInformation& getSnippet(unsigned int index) const { return m_snippets[index]; }
        /// Returns a reference to the index-th query snippet information.
        SnippetQueryInformation& operator[](unsigned int index) { return m_snippets[index]; }
        /// Returns a reference to the index-th query snippet information.
        const SnippetQueryInformation& operator[](unsigned int index) const { return m_snippets[index]; }
        /// Sets the name of the trial.
        inline void setName(const char * name) { m_name = name; }
        /// Returns the name of the trial.
        inline const char * getName(void) const { return m_name.c_str(); }
        /// Stores the trial information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the trial information from a XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Vector with all the snippets selected as searchable and/or as queries.
        VectorDense<SnippetQueryInformation> m_snippets;
        /// Name of the trial.
        std::string m_name;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | EXPERIMENTS INFORMATION              |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Container with the different trials belonging to the same experiment.
    class ExperimentInformation
    {
    public:
        /// Default constructor.
        ExperimentInformation(void) {}
        /// Parameters constructor.
        ExperimentInformation(unsigned int number_of_trials, unsigned int number_of_scores, const char * name) : m_trials(number_of_trials), m_scores(number_of_scores, 0), m_name(name) {}
        /// Copy constructor.
        ExperimentInformation(const ExperimentInformation &other);
        /// Destructor.
        ~ExperimentInformation(void);
        /// Assignation operator.
        ExperimentInformation& operator=(const ExperimentInformation &other);
        /// Sets the number of trials of the experiment.
        void set(unsigned int number_of_trials, unsigned int number_of_scores) { m_trials.set(number_of_trials); m_scores.set(number_of_scores); }
        /// Returns the number of trials of the experiment.
        inline unsigned int getNumberOfTrials(void) const { return m_trials.size(); }
        /// Returns the number of measures of the experiment.
        inline unsigned int getNumberOfScores(void) const { return m_scores.size(); }
        /// Returns a reference to the index-th trial object.
        inline TrialInformation& getTrial(unsigned int index) { return m_trials[index]; }
        /// Returns a constant reference to the index-th trial object.
        inline const TrialInformation& getTrial(unsigned int index) const { return m_trials[index]; }
        /// Returns a reference to the index-th trial object.
        inline TrialInformation& operator[](unsigned int index) { return m_trials[index]; }
        /// Returns a constant reference to the index-th trial object.
        inline const TrialInformation& operator[](unsigned int index) const { return m_trials[index]; }
        /// Returns a constant pointer to the index-th experiment score measure.
        inline const RankScore * getScore(unsigned int index) const { return m_scores[index]; }
        /// Sets the index-th experiment score measure.
        inline void setScore(const RankScore * score, unsigned int index) { m_scores[index] = score->duplicate(); }
        /// Sets the name of the experiment.
        inline void setName(const char * name) { m_name = name; }
        /// Returns the name of the experiment.
        inline const char * getName(void) const { return m_name.c_str(); }
        /// Stores the trial information into a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the trial information from a XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /// Vector with all the trials associated with the experiment.
        VectorDense<TrialInformation> m_trials;
        /// Vector with the evaluation measures calculated in the experiment.
        VectorDense<RankScore *> m_scores;
        /// Name of the experiment.
        std::string m_name;
    };
    
    /// Load all the experiment information stored in a XML file.
    void loadExperiments(const char * filename, VectorDense<ExperimentInformation > &experiments);
    /// Stores the experiment list into a XML file.
    void saveExperiments(const char * filename, VectorDense<ExperimentInformation > &experiments);
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INDEX                |
    //                   | DECLARATION                          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Objects containing all the visual signatures of a document.
    template <class TVALUE, class TINDEX>
    class DocumentVisualIndex
    {
    public:
        // -[ Constructors, destructor and assignation operator ]------------------------------------------------------------------------------------
        /// Default constructor.
        DocumentVisualIndex(void);
        /// Copy constructor.
        DocumentVisualIndex(const DocumentVisualIndex<TVALUE, TINDEX> &other);
        /// Destructor.
        ~DocumentVisualIndex(void);
        /// Assignation operator.
        DocumentVisualIndex<TVALUE, TINDEX>& operator=(const DocumentVisualIndex<TVALUE, TINDEX> &other);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /** Sets the number of signatures stored in the index.
         *  \param[in] number_of_dimensions number of dimensions of the visual signatures.
         *  \param[in] number_of_signatures number of signatures in the document.
         *  \param[in] negative_values flag which indicates that the signature may have negative values (true when it has negative values, false otherwise).
         */
        void initialize(unsigned int number_of_dimensions, unsigned int number_of_signatures);
        
        /// Resets the elements stored in the inverted file.
        void clearInvertedFile(void)
        {
            if (m_inverted_file_values != 0) { delete [] m_inverted_file_values; m_inverted_file_values = 0; }
            if (m_inverted_file_indexes != 0) { delete [] m_inverted_file_indexes; m_inverted_file_indexes = 0; }
            if (m_inverted_file_counter != 0) { delete [] m_inverted_file_counter; m_inverted_file_counter = 0; }
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
                m_inverted_file_size[d] = 0;
            m_number_of_nonzero_elements = 0;
        }
        /** Update the histogram of elements present at each dimension.
         *  \param[in] signature signature to be allocated in the inverted file.
         *  \return number of non-zero in the signature.
         */
        unsigned long updateInvertedFileSize(const VectorDense<float> &signature);
        /// Allocate the inverted file structures.
        void allocateInvertedFile(void);
        
        /** Sets the visual signature information of the index-th word snippet of the index.
         *  \param[in] index index of the word snippet.
         *  \param[in] signature visual signature of the snippet.
         *  \param[in] word_category index of the word category of the word snippet.
         *  \param[in] snippet_identifier unique identifier of the word snippet in the document.
         */
        void setSignature(unsigned int index, const VectorDense<float> &signature, unsigned int word_category, unsigned long snippet_identifier);
        
        /** Sets the word categories information of the index.
         *  \param[in] word_text vector with the text identifier/representation of the word categories.
         *  \param[in] word_offset vector with the offset to each text identifier/representation of the word categories.
         */
        inline void setWordCategories(const VectorDense<char> &word_text, const VectorDense<unsigned int> &word_offset) { m_word_text = word_text; m_word_offset = word_offset; }
        /// Sets the word categories information of the index.
        void setWordCategories(char const * const * word_text, unsigned int number_of_word_categories);
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns the total number of nonzero elements of the signatures.
        inline unsigned long getNumberOfNonzeroElements(void) const { return m_number_of_nonzero_elements; }
        /// Returns the number of dimensions of the signatures.
        inline unsigned int getNumberOfDimensions(void) const { return m_number_of_dimensions; }
        /// Returns the number of signatures in the index.
        inline unsigned int getNumberOfSignatures(void) const { return m_number_of_signatures; }
#if DEBUG
        /// Returns the visual signature of the index-th word snippet.
        inline const VectorDense<float>& getSignature(unsigned int index) const { return m_signature[index]; }
#endif
        /// Returns the index of the word category of the index-th word snippet.
        inline unsigned int getWordCategory(unsigned int index) const { return m_word_category[index]; }
        /// Returns the unique identifier of the index-th word snippet.
        inline unsigned long getSnippetIdentifier(unsigned int index) const { return m_snippet_identifier[index]; }
        /// Returns the number of word categories in the index (i.e. number of unique word identifiers).
        inline unsigned int getNumberOfWordCategories(void) const { return m_word_offset.size(); }
        /// Returns the text identifier/representation of the index-th word category.
        inline const char * getWordCategoryText(unsigned int index) const { return &m_word_text[m_word_offset[index]]; }
        
        // -[ Index evaluation ]---------------------------------------------------------------------------------------------------------------------
        /** Compute the retrieval score of the given selection of sample.
         *  \param[in] distance_identifier distance used to compare snippet signatures.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         *  \param[in] logger pointer to the log object where generation information is verbosed (set to null to disable log information).
         *  \return return the mean average precision score.
         */
        std::tuple<double, unsigned int> computeMeanAveragePrecision(INDEX_DISTANCE_IDENTIFIER distance_identifier, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Compute the retrieval score of the whole dataset.
         *  \param[in] experiments array with the different experiments applied to the index.
         *  \param[in] number_of_experiments number of elements in the array.
         *  \param[in] distance_identifier distance used to compare snippet signatures.
         *  \param[out] scores vector where the score measures obtained at each experiment-trial are stored.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         *  \param[in] logger pointer to the log object where generation information is verbosed (set to null to disable log information).
         *  \return return the mean average precision score.
         */
        void evaluate(const ExperimentInformation * experiments, unsigned int number_of_experiments, INDEX_DISTANCE_IDENTIFIER distance_identifier, VectorDense<VectorDense<VectorDense<std::tuple<double, unsigned int> > > > &scores, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Calculates the ranked visual similarity list of the selected query.
         *  \param[in] query_idx index of the query snippet.
         *  \param[in] distances_thread array of the vectors where the partial distances are stored (<b>note:</b> the array and its vectors need to be pre-allocated).
         *  \param[in] distance_identifier distance used to compare snippet signatures.
         *  \param[in] distances_rank vector with the ranked distances (<b>note:</b> the vector needs to be pre-allocated).
         *  \param[in] number_of_threads number of threads used to concurrently calculate the distances.
         *  \note this function <b>does not</b> allocates \p distance_thread and \p distance_rank structures.
         */
        void computeRank(unsigned int query_idx, VectorDense<float> * distances_thread, INDEX_DISTANCE_IDENTIFIER distance_identifier, VectorDense<std::tuple<float, unsigned int> > &distances_rank, unsigned int number_of_threads) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Retrieves the visual index information from a XML object.
        void convertFromXML(XmlParser &parser);
        /// Stores the visual index information into a XML object.
        void convertToXML(XmlParser &parser) const;
        
    protected:
        // -[ Member variables ]---------------------------------------------------------------------------------------------------------------------
        /// Values of the inverted file.
        TVALUE * m_inverted_file_values; /* This can be a unsigned char when quantized: 1 byte */
        /// Document indexes of the inverted file.
        TINDEX * m_inverted_file_indexes; /* This can be a unsigned short: 2 bytes */
        /// Number of elements at inverted file of each dimensions.
        TINDEX * m_inverted_file_size;
        /// Temporary array to track the position of the elements in the inverted file while storing the signatures.
        TINDEX * m_inverted_file_counter;
        /// Total number of non-zero elements.
        unsigned long m_number_of_nonzero_elements;
#if DEBUG
        /// Vector with the signatures of all the snippets of the document.
        VectorDense<float> * m_signature;
#endif
        /// Vector with the L1-norm of each signature of the document.
        float * m_l1_norm;
        /// Vector with the L2-norm of each signature of the document.
        float * m_l2_norm;
        /// Minimum value of each document signature.
        float * m_signature_minimum;
        /// Maximum value of each document signature.
        float * m_signature_range;
        /// Array with the semantic category (i.e. word identifier) of each word snippet.
        unsigned int * m_word_category;
        /// Array with the unique identifier of each word snippet.
        unsigned long * m_snippet_identifier;
        /// Number of documents in the database.
        unsigned int m_number_of_signatures;
        /// Number of dimensions of the signature.
        unsigned int m_number_of_dimensions;
        /// Vector with the text labels of all the categories in the database.
        VectorDense<char> m_word_text;
        /// Vector with the offset the labels to each category in the database.
        VectorDense<unsigned int> m_word_offset;
    };
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INDEX                |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    template <class TVALUE, class TINDEX>
    DocumentVisualIndex<TVALUE, TINDEX>::DocumentVisualIndex(void) :
        m_inverted_file_values(0),
        m_inverted_file_indexes(0),
        m_inverted_file_size(0),
        m_inverted_file_counter(0),
        m_number_of_nonzero_elements(0),
#if DEBUG
        m_signature(0),
#endif
        m_l1_norm(0),
        m_l2_norm(0),
        m_signature_minimum(0),
        m_signature_range(0),
        m_word_category(0),
        m_snippet_identifier(0),
        m_number_of_signatures(0),
        m_number_of_dimensions(0)
    {
    }
    
    template <class TVALUE, class TINDEX>
    DocumentVisualIndex<TVALUE, TINDEX>::DocumentVisualIndex(const DocumentVisualIndex<TVALUE, TINDEX> &other) :
        m_inverted_file_values((other.m_number_of_nonzero_elements > 0)?new TVALUE[other.m_number_of_nonzero_elements]:0),
        m_inverted_file_indexes((other.m_number_of_nonzero_elements > 0)?new TINDEX[other.m_number_of_nonzero_elements]:0),
        m_inverted_file_size((other.m_number_of_dimensions > 0)?new TINDEX[other.m_number_of_dimensions]:0),
        m_inverted_file_counter((other.m_inverted_file_counter != 0)?new TINDEX[other.m_number_of_dimensions]:0),
        m_number_of_nonzero_elements(other.m_number_of_nonzero_elements),
#if DEBUG
        m_signature((other.m_number_of_signatures != 0)?new VectorDense<float>[other.m_number_of_signatures]:0),
#endif
        m_l1_norm((other.m_number_of_signatures != 0)?new float[other.m_number_of_signatures]:0),
        m_l2_norm((other.m_number_of_signatures != 0)?new float[other.m_number_of_signatures]:0),
        m_signature_minimum((other.m_number_of_signatures != 0)?new float[other.m_number_of_signatures]:0),
        m_signature_range((other.m_number_of_signatures != 0)?new float[other.m_number_of_signatures]:0),
        m_word_category((other.m_number_of_signatures != 0)?new unsigned int[other.m_number_of_signatures]:0),
        m_snippet_identifier((other.m_number_of_signatures != 0)?new unsigned long[other.m_number_of_signatures]:0),
        m_number_of_signatures(other.m_number_of_signatures),
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_word_text(other.m_word_text),
        m_word_offset(other.m_word_offset)
    {
        for (unsigned int i = 0; i < other.m_number_of_signatures; ++i)
        {
            m_inverted_file_size[i] = other.m_inverted_file_size[i];
#if DEBUG
            m_signature[i] = other.m_signature[i];
#endif
            m_l1_norm[i] = other.m_l1_norm[i];
            m_l2_norm[i] = other.m_l2_norm[i];
            m_signature_minimum[i] = other.m_signature_minimum[i];
            m_signature_range[i] = other.m_signature_range[i];
            m_word_category[i] = other.m_word_category[i];
            m_snippet_identifier[i] = other.m_snippet_identifier[i];
        }
        for (unsigned long i = 0; i < other.m_number_of_nonzero_elements; ++i)
        {
            m_inverted_file_values[i] = other.m_inverted_file_values[i];
            m_inverted_file_indexes[i] = other.m_inverted_file_indexes[i];
        }
        if (other.m_inverted_file_counter != 0)
        {
            for (unsigned int d = 0; d < other.m_number_of_dimensions; ++d)
                m_inverted_file_counter[d] = other.m_inverted_file_counter[d];
        }
    }
    
    template <class TVALUE, class TINDEX>
    DocumentVisualIndex<TVALUE, TINDEX>::~DocumentVisualIndex(void)
    {
        if (m_inverted_file_values != 0) delete [] m_inverted_file_values;
        if (m_inverted_file_indexes != 0) delete [] m_inverted_file_indexes;
        if (m_inverted_file_size != 0) delete [] m_inverted_file_size;
        if (m_inverted_file_counter != 0) delete [] m_inverted_file_counter;
#if DEBUG
        if (m_signature != 0) delete [] m_signature;
#endif
        if (m_l1_norm != 0) delete [] m_l1_norm;
        if (m_l2_norm != 0) delete [] m_l2_norm;
        if (m_signature_minimum != 0) delete [] m_signature_minimum;
        if (m_signature_range != 0) delete [] m_signature_range;
        if (m_word_category != 0) delete [] m_word_category;
        if (m_snippet_identifier != 0) delete [] m_snippet_identifier;
    }
    
    template <class TVALUE, class TINDEX>
    DocumentVisualIndex<TVALUE, TINDEX>& DocumentVisualIndex<TVALUE, TINDEX>::operator=(const DocumentVisualIndex<TVALUE, TINDEX> &other)
    {
        if (this != &other)
        {
            // Free structures.
            if (m_inverted_file_values != 0) delete [] m_inverted_file_values;
            if (m_inverted_file_indexes != 0) delete [] m_inverted_file_indexes;
            if (m_inverted_file_size != 0) delete [] m_inverted_file_size;
            if (m_inverted_file_counter != 0) delete [] m_inverted_file_counter;
#if DEBUG
            if (m_signature != 0) delete [] m_signature;
#endif
            if (m_l1_norm != 0) delete [] m_l1_norm;
            if (m_l2_norm != 0) delete [] m_l2_norm;
            if (m_signature_minimum != 0) delete [] m_signature_minimum;
            if (m_signature_range != 0) delete [] m_signature_range;
            if (m_word_category != 0) delete [] m_word_category;
            if (m_snippet_identifier != 0) delete [] m_snippet_identifier;
            
            // Copy content.
            if (other.m_number_of_nonzero_elements > 0)
            {
                m_inverted_file_values = new TVALUE[other.m_number_of_nonzero_elements];
                m_inverted_file_indexes = new TINDEX[other.m_number_of_nonzero_elements];
                m_number_of_nonzero_elements = other.m_number_of_nonzero_elements;
                for (unsigned long i = 0; i < other.m_number_of_nonzero_elements; ++i)
                {
                    m_inverted_file_values[i] = other.m_inverted_file_values[i];
                    m_inverted_file_indexes[i] = other.m_inverted_file_indexes[i];
                }
            }
            else
            {
                m_inverted_file_values = 0;
                m_inverted_file_indexes = 0;
                m_number_of_nonzero_elements = 0;
            }
            if (other.m_number_of_dimensions > 0)
            {
                m_inverted_file_size = new TINDEX[other.m_number_of_dimensions];
                for (unsigned int i = 0; i < other.m_number_of_dimensions; ++i)
                    m_inverted_file_size[i] = other.m_inverted_file_size[i];
            }
            else m_inverted_file_size = 0;
            
            if (other.m_number_of_signatures > 0)
            {
#if DEBUG
                m_signature = new VectorDense<float>[other.m_number_of_signatures];
#endif
                m_l1_norm = new float[other.m_number_of_signatures];
                m_l2_norm = new float[other.m_number_of_signatures];
                m_signature_minimum = new float[other.m_number_of_signatures];
                m_signature_range = new float[other.m_number_of_signatures];
                m_word_category = new unsigned int[other.m_number_of_signatures];
                m_snippet_identifier = new unsigned long[other.m_number_of_signatures];
                for (unsigned int i = 0; i < other.m_number_of_signatures; ++i)
                {
#if DEBUG
                    m_signature[i] = other.m_signature[i];
#endif
                    m_l1_norm[i] = other.m_l1_norm[i];
                    m_l2_norm[i] = other.m_l2_norm[i];
                    m_signature_minimum[i] = other.m_signature_minimum[i];
                    m_signature_range[i] = other.m_signature_range[i];
                    m_word_category[i] = other.m_word_category[i];
                    m_snippet_identifier[i] = other.m_snippet_identifier[i];
                }
            }
            else
            {
#if DEBUG
                m_signature = 0;
#endif
                m_l1_norm = m_l2_norm = m_signature_minimum = m_signature_range = 0;
                m_word_category = 0;
                m_snippet_identifier = 0;
            }
            m_number_of_signatures = other.m_number_of_signatures;
            m_number_of_dimensions = other.m_number_of_signatures;
            m_word_text = other.m_word_text;
            m_word_offset = other.m_word_offset;
            if (other.m_inverted_file_counter != 0)
            {
                m_inverted_file_counter = new TINDEX[other.m_number_of_dimensions];
                for (unsigned int d = 0; d < other.m_number_of_dimensions; ++d)
                    m_inverted_file_counter[d] = other.m_inverted_file_counter[d];
            }
            else m_inverted_file_counter = 0;
        }
        
        return *this;
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::initialize(unsigned int number_of_dimensions, unsigned int number_of_signatures)
    {
        if (m_inverted_file_values != 0) { delete [] m_inverted_file_values; m_inverted_file_values = 0; }
        if (m_inverted_file_indexes != 0) { delete [] m_inverted_file_indexes; m_inverted_file_indexes = 0; }
        if (m_inverted_file_size != 0) delete [] m_inverted_file_size;
        if (m_inverted_file_counter != 0) { delete [] m_inverted_file_counter; m_inverted_file_counter = 0; }
#if DEBUG
        if (m_signature != 0) delete [] m_signature;
#endif
        if (m_l1_norm != 0) delete [] m_l1_norm;
        if (m_l2_norm != 0) delete [] m_l2_norm;
        if (m_signature_minimum != 0) delete [] m_signature_minimum;
        if (m_signature_range != 0) delete [] m_signature_range;
        if (m_word_category != 0) delete [] m_word_category;
        if (m_snippet_identifier != 0) delete [] m_snippet_identifier;
        
        if ((number_of_dimensions > 0) && (number_of_signatures > 0))
        {
            m_inverted_file_size = new TINDEX[number_of_dimensions];
            m_number_of_nonzero_elements = 0;
#if DEBUG
            m_signature = new VectorDense<float>[number_of_signatures];
#endif
            m_l1_norm = new float[number_of_signatures];
            m_l2_norm = new float[number_of_signatures];
            m_signature_minimum = new float[number_of_signatures];
            m_signature_range = new float[number_of_signatures];
            m_word_category = new unsigned int[number_of_signatures];
            m_snippet_identifier = new unsigned long[number_of_signatures];
            for (unsigned int i = 0; i < number_of_signatures; ++i)
            {
                m_l1_norm[i] = m_l2_norm[i] = m_signature_minimum[i] = m_signature_range[i] = 0.0f;
                m_word_category[i] = 0;
                m_snippet_identifier[i] = 0;
            }
            m_number_of_dimensions = number_of_dimensions;
            m_number_of_signatures = number_of_signatures;
        }
        else
        {
            m_inverted_file_size = 0;
#if DEBUG
            m_signature = 0;
#endif
            m_l1_norm = m_l2_norm = m_signature_minimum = m_signature_range = 0;
            m_word_category = 0;
            m_snippet_identifier = 0;
            m_number_of_dimensions = 0;
            m_number_of_signatures = 0;
        }
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::setWordCategories(char const * const * word_text, unsigned int number_of_word_categories)
    {
        unsigned int total_length;
        
        total_length = 0;
        for (unsigned int i = 0; i < number_of_word_categories; ++i)
        {
            for (unsigned int j = 0; word_text[i][j] != '\0'; ++j) ++total_length;
            ++total_length;
        }
        if (total_length > 0)
        {
            m_word_text.set(total_length);
            m_word_offset.set(number_of_word_categories);
            total_length = 0;
            for (unsigned int i = 0; i < number_of_word_categories; ++i)
            {
                m_word_offset[i] = total_length;
                for (unsigned int j = 0; word_text[i][j] != '\0'; ++j) m_word_text[total_length++] = word_text[i][j];
                m_word_text[total_length++] = '\0';
            }
        }
        else
        {
            m_word_text.set(0);
            m_word_offset.set(0);
        }
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("DocumentVisualIndex"))
        {
            bool init_word_text, init_word_offset, init_word_category, init_snippet_identifier, init_inverted_file_values;
            bool init_inverted_file_indexes, init_inverted_file_size, init_signature_minimum, init_signature_range;
            
            // Free allocated memory.
            if (m_inverted_file_values != 0) delete [] m_inverted_file_values;
            if (m_inverted_file_indexes != 0) delete [] m_inverted_file_indexes;
            if (m_inverted_file_size != 0) delete [] m_inverted_file_size;
            if (m_inverted_file_counter != 0) { delete [] m_inverted_file_counter; m_inverted_file_counter = 0; }
#if DEBUG
            if (m_signature != 0) delete [] m_signature;
#endif
            if (m_l1_norm != 0) delete [] m_l1_norm;
            if (m_l2_norm != 0) delete [] m_l2_norm;
            if (m_signature_minimum != 0) delete [] m_signature_minimum;
            if (m_signature_range != 0) delete [] m_signature_range;
            if (m_word_category != 0) delete [] m_word_category;
            if (m_snippet_identifier != 0) delete [] m_snippet_identifier;
#if DEBUG
            m_signature = 0;
#endif
            m_l1_norm = m_l2_norm = m_signature_minimum = m_signature_range = 0;
            m_word_category = 0;
            m_snippet_identifier = 0;
            m_word_text.set(0);
            m_word_offset.set(0);
            
            // Gather attributes.
            m_number_of_signatures = parser.getAttribute("Number_Of_Signatures");
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_number_of_nonzero_elements = parser.getAttribute("Number_Of_Nonzero_Elements");
            if ((m_number_of_signatures > 0) && (m_number_of_dimensions > 0))
            {
                m_inverted_file_size = new TINDEX[m_number_of_dimensions];
#if DEBUG
                m_signature = new VectorDense<float>[m_number_of_signatures];
#endif
                m_l1_norm = new float[m_number_of_signatures];
                m_l2_norm = new float[m_number_of_signatures];
                m_signature_minimum = new float[m_number_of_signatures];
                m_signature_range = new float[m_number_of_signatures];
                m_word_category = new unsigned int[m_number_of_signatures];
                m_snippet_identifier = new unsigned long[m_number_of_signatures];
            }
            else
            {
                m_inverted_file_size = 0;
#if DEBUG
                m_signature = 0;
#endif
                m_l1_norm = m_l2_norm = m_signature_minimum = m_signature_range = 0;
                m_word_category = 0;
                m_snippet_identifier = 0;
            }
            if (m_number_of_nonzero_elements > 0)
            {
                if (m_number_of_dimensions == 0)
                    throw Exception("Inverted file has %d elements but the signature dimensionality in the XML object is 0.", m_number_of_nonzero_elements);
                if (m_number_of_signatures == 0)
                    throw Exception("Inverted file has %d elements but the XML object contains 0 signatures.", m_number_of_nonzero_elements);
                m_inverted_file_values = new TVALUE[m_number_of_nonzero_elements];
                m_inverted_file_indexes = new TINDEX[m_number_of_nonzero_elements];
            }
            else
            {
                m_inverted_file_values = 0;
                m_inverted_file_indexes = 0;
            }
            init_word_text = init_word_offset = init_word_category = init_snippet_identifier = init_inverted_file_values = false;
            init_inverted_file_indexes = init_inverted_file_size = init_signature_minimum = init_signature_range = false;
            
            while (!(parser.isTagIdentifier("DocumentVisualIndex") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("InvertedFileValues"))
                {
                    VectorDense<TVALUE, unsigned long> data;
                    
                    if (init_inverted_file_values)
                        throw Exception("Multiple definitions of the inverted file values array in the XML object.");
                    loadVector(parser, "InvertedFileValues", data);
                    if (data.size() != m_number_of_nonzero_elements)
                        throw Exception("The inverted file values array has an unexpected number of elements (%d != %d).", data.size(), m_number_of_nonzero_elements);
                    for (unsigned long k = 0; k < m_number_of_nonzero_elements; ++k)
                        m_inverted_file_values[k] = data[k];
                    init_inverted_file_values = true;
                }
                else if (parser.isTagIdentifier("InvertedFileIndexes"))
                {
                    VectorDense<TINDEX, unsigned long> data;
                    
                    if (init_inverted_file_indexes)
                        throw Exception("Multiple definitions of the inverted file indexes array in the XML object.");
                    loadVector(parser, "InvertedFileIndexes", data);
                    if (data.size() != m_number_of_nonzero_elements)
                        throw Exception("The inverted file indexes array has an unexpected number of elements (%d != %d).", data.size(), m_number_of_nonzero_elements);
                    for (unsigned long k = 0; k < m_number_of_nonzero_elements; ++k)
                        m_inverted_file_indexes[k] = data[k];
                    init_inverted_file_indexes = true;
                }
                else if (parser.isTagIdentifier("InvertedFileSize"))
                {
                    VectorDense<TINDEX> data;
                    unsigned long counter;
                    
                    if (init_inverted_file_size)
                        throw Exception("Multiple definitions of the inverted file size array in the XML object.");
                    loadVector(parser, "InvertedFileSize", data);
                    if (data.size() != m_number_of_dimensions)
                        throw Exception("The inverted file size array has an unexpected number of elements (%d != %d).", data.size(), m_number_of_dimensions);
                    counter = 0;
                    for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
                        counter += (unsigned long)(m_inverted_file_size[d] = data[d]);
                    if (counter != m_number_of_nonzero_elements)
                        throw Exception("The number of nonzero elements in the inverted file size array differs from the number of nonzero elements in the object (%d != %d).", counter, m_number_of_nonzero_elements);
                    init_inverted_file_size = true;
                }
                else if (parser.isTagIdentifier("SignatureMinimum"))
                {
                    VectorDense<float> data;
                    
                    if (init_signature_minimum)
                        throw Exception("Multiple definitions of the signature minimum vector.");
                    loadVector(parser, "SignatureMinimum", data);
                    if (data.size() != m_number_of_signatures)
                        throw Exception("The signature minimum vector has an unexpected number of elements (%d != %d).", data.size(), m_number_of_signatures);
                    for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                        m_signature_minimum[k] = data[k];
                    init_signature_minimum = true;
                }
                else if (parser.isTagIdentifier("SignatureRange"))
                {
                    VectorDense<float> data;
                    
                    if (init_signature_range)
                        throw Exception("Multiple definitions of the signature range vector.");
                    loadVector(parser, "SignatureRange", data);
                    if (data.size() != m_number_of_signatures)
                        throw Exception("The signature range vector has an unexpected number of elements (%d != %d).", data.size(), m_number_of_signatures);
                    for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                        m_signature_range[k] = data[k];
                    init_signature_range = true;
                }
                else if (parser.isTagIdentifier("SnippetCategory"))
                {
                    VectorDense<unsigned int> data;
                    
                    if (init_word_category)
                        throw Exception("Multiple definitions of the word category vector.");
                    loadVector(parser, "SnippetCategory", data);
                    if (data.size() != m_number_of_signatures)
                        throw Exception("The word category vector has an unexpected number of elements (%d != %d).", data.size(), m_number_of_signatures);
                    for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                        m_word_category[k] = data[k];
                    init_word_category = true;
                }
                else if (parser.isTagIdentifier("SnippetIdentifier"))
                {
                    VectorDense<unsigned long> data;
                    
                    if (init_snippet_identifier)
                        throw Exception("Multiple definitions of the snippet identifier vector.");
                    loadVector(parser, "SnippetIdentifier", data);
                    if (data.size() != m_number_of_signatures)
                        throw Exception("The snippet identifier vector has an unexpected number of elements (%d != %d).", data.size(), m_number_of_signatures);
                    for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                        m_snippet_identifier[k] = data[k];
                    init_snippet_identifier = true;
                }
                else if (parser.isTagIdentifier("WordText"))
                {
                    if (init_word_text)
                        throw Exception("Multiple definitions of the unique words text vector.");
                    loadVector(parser, "WordText", m_word_text);
                    init_word_text = true;
                }
                else if (parser.isTagIdentifier("WordOffset"))
                {
                    if (init_word_offset)
                        throw Exception("Multiple definitions of the unique word offset vector.");
                    loadVector(parser, "WordOffset", m_word_offset);
                    init_word_offset = true;
                }
                else parser.getNext();
            }
            parser.getNext();
            
            // Check that information has been properly loaded.
            if ((m_number_of_signatures > 0) && (m_number_of_dimensions > 0))
            {
                if (!init_word_text) throw Exception("The XML object does not contain word text information.");
                if (!init_word_offset) throw Exception("The XML object does not contain word offset information.");
                if (!init_word_category) throw Exception("The XML object does not contain word category information.");
                if (!init_snippet_identifier) throw Exception("The XML object does not contain snippet identifier information.");
                if (!init_inverted_file_size) throw Exception("The XML object does not contain inverted file size information.");
                if (!init_signature_minimum) throw Exception("The XML object does not contain a signature minimum vector.");
                if (!init_signature_range) throw Exception("The XML object does not contain a signature range vector.");
                if (m_number_of_nonzero_elements > 0)
                {
                    if (!init_inverted_file_values) throw Exception("The XML object does not contain inverted file values information.");
                    if (!init_inverted_file_indexes) throw Exception("The XML object does not contain inverted file indexes information.");
                }
            }
            if (m_number_of_nonzero_elements > 0)
            {
                const float quantization_factor = std::min(1000000.0f, (float)std::numeric_limits<TVALUE>::max());
                
                // Calculate the norm of the descriptor.
                for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                    m_l1_norm[k] = m_l2_norm[k] = 0;
                for (unsigned long k = 0; k < m_number_of_nonzero_elements; ++k)
                {
                    const TINDEX index = m_inverted_file_indexes[k];
                    float dequantized_value;
                    
                    dequantized_value = ((float)m_inverted_file_values[k] / quantization_factor) * m_signature_range[index] + m_signature_minimum[index];
                    m_l1_norm[index] += std::abs(dequantized_value);
                    m_l2_norm[index] += dequantized_value * dequantized_value;
                }
            }
        }
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::convertToXML(XmlParser &parser) const
    {
        parser.openTag("DocumentVisualIndex");
        parser.setAttribute("Number_Of_Signatures", m_number_of_signatures);
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Number_Of_Nonzero_Elements", m_number_of_nonzero_elements);
        parser.addChildren();
        if ((m_number_of_dimensions > 0) && (m_number_of_signatures > 0))
        {
            if (m_number_of_nonzero_elements > 0)
            {
                saveVector(parser, "InvertedFileValues", ConstantSubVectorDense<TVALUE, unsigned long>(m_inverted_file_values, m_number_of_nonzero_elements));
                saveVector(parser, "InvertedFileIndexes", ConstantSubVectorDense<TINDEX, unsigned long>(m_inverted_file_indexes, m_number_of_nonzero_elements));
            }
            saveVector(parser, "InvertedFileSize", ConstantSubVectorDense<TINDEX>(m_inverted_file_size, m_number_of_dimensions));
            saveVector(parser, "SignatureMinimum", ConstantSubVectorDense<float>(m_signature_minimum, m_number_of_signatures));
            saveVector(parser, "SignatureRange", ConstantSubVectorDense<float>(m_signature_range, m_number_of_signatures));
            saveVector(parser, "SnippetCategory", ConstantSubVectorDense<unsigned int>(m_word_category, m_number_of_signatures));
            saveVector(parser, "SnippetIdentifier", ConstantSubVectorDense<unsigned long>(m_snippet_identifier, m_number_of_signatures));
        }
        saveVector(parser, "WordText", m_word_text);
        saveVector(parser, "WordOffset", m_word_offset);
        parser.closeTag();
    }
    
    template <class TVALUE, class TINDEX>
    unsigned long DocumentVisualIndex<TVALUE, TINDEX>::updateInvertedFileSize(const VectorDense<float> &signature)
    {
        unsigned long non_zero;
        
        non_zero = 0;
        if (std::numeric_limits<TVALUE>::is_integer)
        {
            const float quantization_factor = std::min(1000000.0f, (float)std::numeric_limits<TVALUE>::max());
            float signature_minimum, signature_range;
            
            // Calculate the maximum-minimum values of the signature.
            signature_minimum = signature_range = 0.0f;
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] < signature_minimum) signature_minimum = signature[d];
                if (signature[d] > signature_range  ) signature_range   = signature[d];
            }
            signature_range = signature_range - signature_minimum; // range = maximum - minimum.
            // Calculate the number of non-zero elements in the quantized signature.
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] != 0)
                {
                    float dequantized_value;
                    TVALUE quantized_value;
                    
                    // Check if the quantized value is also different than zero.
                    quantized_value = (TVALUE)(quantization_factor * ((signature[d] - signature_minimum) / signature_range));
                    dequantized_value = ((float)quantized_value / quantization_factor) * signature_range + signature_minimum;
                    if (dequantized_value != 0)
                    {
                        ++m_inverted_file_size[d];
                        ++non_zero;
                    }
                }
            }
            m_number_of_nonzero_elements += non_zero;
        }
        else
        {
            // For real value data-types no quantization is needed, so only check which values are different than 0.
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] != 0)
                {
                    ++m_inverted_file_size[d];
                    ++non_zero;
                }
            }
            m_number_of_nonzero_elements += non_zero;
        }
        
        return non_zero;
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::allocateInvertedFile(void)
    {
        if (m_inverted_file_values != 0) delete [] m_inverted_file_values;
        if (m_inverted_file_indexes != 0) delete [] m_inverted_file_indexes;
        if (m_inverted_file_counter != 0) delete [] m_inverted_file_counter;
        if (m_number_of_nonzero_elements > 0)
        {
            m_inverted_file_values = new TVALUE[m_number_of_nonzero_elements];
            m_inverted_file_indexes = new TINDEX[m_number_of_nonzero_elements];
            m_inverted_file_counter = new TINDEX[m_number_of_dimensions];
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
                m_inverted_file_counter[d] = 0;
        }
        else { m_inverted_file_values = 0; m_inverted_file_indexes = 0; m_inverted_file_counter = 0; }
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::setSignature(unsigned int index, const VectorDense<float> &signature, unsigned int word_category, unsigned long snippet_identifier)
    {
        unsigned long offset;
        
        if (index >= m_number_of_signatures) throw Exception("Signature out of range (%d >= %d).", index, m_number_of_signatures);
#if DEBUG
        m_signature[index] = signature;
#endif
        m_word_category[index] = word_category;
        m_snippet_identifier[index] = snippet_identifier;
        
        m_l2_norm[index] = m_l1_norm[index] = 0;
        offset = 0;
        if (std::numeric_limits<TVALUE>::is_integer)
        {
            const float quantization_factor = std::min(1000000.0f, (float)std::numeric_limits<TVALUE>::max());
            unsigned long non_zero;
            
            // Calculate the maximum-minimum values of the signature.
            m_signature_minimum[index] = m_signature_range[index] = 0.0f;
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] < m_signature_minimum[index]) m_signature_minimum[index] = signature[d];
                if (signature[d] > m_signature_range[index]  ) m_signature_range[index]   = signature[d];
            }
            m_signature_range[index] = m_signature_range[index] - m_signature_minimum[index]; // range = maximum - minimum.
            // Store the non-zero elements of the signature in the inverted file.
            non_zero = 0;
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] != 0)
                {
                    float dequantized_value;
                    TVALUE quantized_value;
                    
                    // Check if the quantized value is also different than zero.
                    quantized_value = (TVALUE)(quantization_factor * ((signature[d] - m_signature_minimum[index]) / m_signature_range[index]));
                    dequantized_value = ((float)quantized_value / quantization_factor) * m_signature_range[index] + m_signature_minimum[index];
                    if (dequantized_value != 0)
                    {
                        m_inverted_file_values[m_inverted_file_counter[d] + offset] = quantized_value;
                        m_inverted_file_indexes[m_inverted_file_counter[d] + offset] = (TINDEX)index;
                        ++(m_inverted_file_counter[d]);
                        m_l2_norm[index] += dequantized_value * dequantized_value;
                        m_l1_norm[index] += std::abs(dequantized_value);
                    }
                }
                offset += (unsigned long)m_inverted_file_size[d];
            }
            m_number_of_nonzero_elements += non_zero;
        }
        else
        {
            // For non-integer values maximum-minimum values are not needed.
            m_signature_range[index] = 1.0f;
            m_signature_minimum[index] = 0.0f;
            // Store the non-zero elements of the signature in the inverted file.
            for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
            {
                if (signature[d] != 0)
                {
                    m_inverted_file_values[m_inverted_file_counter[d] + offset] = (TVALUE)signature[d];
                    m_inverted_file_indexes[m_inverted_file_counter[d] + offset] = (TINDEX)index;
                    ++(m_inverted_file_counter[d]);
                    m_l2_norm[index] += (float)signature[d] * (float)signature[d];
                    m_l1_norm[index] += std::abs((float)signature[d]);
                }
                offset += (unsigned long)m_inverted_file_size[d];
            }
        }
    }
    
    template <class TVALUE, class TINDEX>
    std::tuple<double, unsigned int> DocumentVisualIndex<TVALUE, TINDEX>::computeMeanAveragePrecision(INDEX_DISTANCE_IDENTIFIER distance_identifier, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<VectorDense<VectorDense<std::tuple<double, unsigned int> > > > scores;
        VectorDense<ExperimentInformation> experiments(1);
        RankScore * score;
        
        experiments[0].set(1, 1); // Set a single trial with a single measure.
        experiments[0].setScore((score = new RankScoreAP()), 0);
        experiments[0][0].set(m_number_of_signatures);
        delete score;
        for (unsigned int i = 0; i < m_number_of_signatures; ++i)
            experiments[0][0][i].set(m_snippet_identifier[i], true, true);
        evaluate(experiments.getData(), 1, distance_identifier, scores, number_of_threads, logger);
        
        return scores[0][0][0];
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::computeRank(unsigned int query_idx, VectorDense<float> * distances_thread, INDEX_DISTANCE_IDENTIFIER distance_identifier, VectorDense<std::tuple<float, unsigned int> > &distances_rank, unsigned int number_of_threads) const
    {
#if DEBUG && DEBUGRANK    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        VectorDistance distance_debug((distance_identifier == DOCUMENT_INDEX_DISTANCE_EUCLIDEAN)?EUCLIDEAN_DISTANCE:MANHATTAN_DISTANCE);
        for (unsigned int j = 0; j < m_number_of_signatures; ++j)
        {
            float current_distance;
            
            distance_debug.distance(m_signature[query_idx], m_signature[j], current_distance);
            distances_rank[j] = std::make_tuple(current_distance, j);
        }
#else   // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        const float quantization_factor = std::min(1000000.0f, (float)std::numeric_limits<TVALUE>::max());
        // Calculate the distances between the snippets.
        if (distance_identifier == DOCUMENT_INDEX_DISTANCE_EUCLIDEAN)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                unsigned long offset;
                
                for (unsigned int j = 0; j < m_number_of_signatures; ++j)
                    distances_thread[thread_id][j] = 0.0f;
                offset = 0;
                for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
                {
                    if (d % number_of_threads == thread_id)
                    {
                        if (std::numeric_limits<TVALUE>::is_integer)
                        {
                            float query_value;
                            
                            query_value = 0;
                            
                            for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                            {
                                if (m_inverted_file_indexes[offset + k] == (TINDEX)query_idx)
                                {
                                    query_value = ((float)m_inverted_file_values[offset + k] / quantization_factor) * m_signature_range[query_idx] + m_signature_minimum[query_idx];
                                    break;
                                }
                            }
                            if (query_value != 0)
                            {
                                for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                                {
                                    const float current_value = ((float)m_inverted_file_values[offset + k] / quantization_factor) * m_signature_range[m_inverted_file_indexes[offset + k]] + m_signature_minimum[m_inverted_file_indexes[offset + k]];
                                    distances_thread[thread_id][m_inverted_file_indexes[offset + k]] += query_value * current_value;
                                }
                            }
                        }
                        else
                        {
                            float query_value;
                            
                            query_value = 0;
                            for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                            {
                                if (m_inverted_file_indexes[offset + k] == (TINDEX)query_idx)
                                {
                                    query_value = (float)m_inverted_file_values[offset + k];
                                    break;
                                }
                            }
                            if (query_value != 0)
                                for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                                    distances_thread[thread_id][m_inverted_file_indexes[offset + k]] += query_value * (float)m_inverted_file_values[offset + k];
                        }
                    }
                    offset += (unsigned long)m_inverted_file_size[d];
                }
            }
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < m_number_of_signatures; j += number_of_threads)
                {
                    float accumulated;
                    
                    accumulated = distances_thread[0][j];
                    for (unsigned int t = 1; t < number_of_threads; ++t)
                        accumulated += distances_thread[t][j];
                    distances_rank[j] = std::make_tuple(m_l2_norm[j] + m_l2_norm[query_idx] - (float)2 * accumulated, j);
                }
            }
        }
        else if (distance_identifier == DOCUMENT_INDEX_DISTANCE_MANHATTAN)
        {
            #pragma omp parallel num_threads(number_of_threads)
            {
                const unsigned int thread_id = omp_get_thread_num();
                unsigned long offset;
                
                for (unsigned int j = 0; j < m_number_of_signatures; ++j)
                    distances_thread[thread_id][j] = 0.0f;
                offset = 0;
                for (unsigned int d = 0; d < m_number_of_dimensions; ++d)
                {
                    if (d % number_of_threads == thread_id)
                    {
                        if (std::numeric_limits<TVALUE>::is_integer)
                        {
                            float query_value;
                            
                            query_value = 0;
                            
                            for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                            {
                                if (m_inverted_file_indexes[offset + k] == (TINDEX)query_idx)
                                {
                                    query_value = ((float)m_inverted_file_values[offset + k] / quantization_factor) * m_signature_range[query_idx] + m_signature_minimum[query_idx];
                                    break;
                                }
                            }
                            if (query_value != 0)
                            {
                                for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                                {
                                    const float current_value = ((float)m_inverted_file_values[offset + k] / quantization_factor) * m_signature_range[m_inverted_file_indexes[offset + k]] + m_signature_minimum[m_inverted_file_indexes[offset + k]];
                                    distances_thread[thread_id][m_inverted_file_indexes[offset + k]] += std::abs(query_value - current_value) - std::abs(query_value) - std::abs(current_value);
                                }
                            }
                        }
                        else
                        {
                            float query_value;
                            
                            query_value = 0;
                            for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                            {
                                if (m_inverted_file_indexes[offset + k] == (TINDEX)query_idx)
                                {
                                    query_value = (float)m_inverted_file_values[offset + k];
                                    break;
                                }
                            }
                            if (query_value != 0)
                                for (unsigned long k = 0; k < (unsigned long)m_inverted_file_size[d]; ++k)
                                    distances_thread[thread_id][m_inverted_file_indexes[offset + k]] += std::abs(query_value - (float)m_inverted_file_values[offset + k]) - std::abs(query_value) - std::abs((float)m_inverted_file_values[offset + k]);
                        }
                    }
                    offset += (unsigned long)m_inverted_file_size[d];
                }
            }
            #pragma omp parallel num_threads(number_of_threads)
            {
                for (unsigned int j = omp_get_thread_num(); j < m_number_of_signatures; j += number_of_threads)
                {
                    float accumulated;
                    
                    accumulated = distances_thread[0][j];
                    for (unsigned int t = 1; t < number_of_threads; ++t)
                        accumulated += distances_thread[t][j];
                    distances_rank[j] = std::make_tuple(m_l1_norm[j] + m_l1_norm[query_idx] + accumulated, j);
                }
            }
        }
#endif
        // Sort the contributions (unselected snippets have infinity distance, so they are at the end of the array).
        std::sort(&distances_rank[0], &distances_rank[m_number_of_signatures]);
    }
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndex<TVALUE, TINDEX>::evaluate(const ExperimentInformation * experiments, unsigned int number_of_experiments, INDEX_DISTANCE_IDENTIFIER distance_identifier, VectorDense<VectorDense<VectorDense<std::tuple<double, unsigned int> > > > &scores, unsigned int number_of_threads, BaseLogger * logger) const
    {
        VectorDense<std::tuple<float, unsigned int> > distances_rank;
        bool * * * signatures_enabled, * * * signatures_queries;
        VectorDense<float> * distances_thread;
        std::set<unsigned long> selection_set;
        
        // Initialize the search structures.
        scores.set(number_of_experiments);
        for (unsigned int i = 0; i < number_of_experiments; ++i)
        {
            scores[i].set(experiments[i].getNumberOfTrials());
            if (experiments[i].getNumberOfScores() == 0)
                throw Exception("Experiment '%d' does not have any score measure selected.", i);
            for (unsigned int j = 0; j < experiments[i].getNumberOfTrials(); ++j)
                scores[i][j].set(experiments[i].getNumberOfScores(), std::make_tuple(0.0, (unsigned int)0));
        }
        if ((distance_identifier != DOCUMENT_INDEX_DISTANCE_EUCLIDEAN) && (distance_identifier != DOCUMENT_INDEX_DISTANCE_MANHATTAN))
            throw Exception("The selected distance (identifier=%d) has not been implemented yet.", (int)distance_identifier);
        if (number_of_threads < 1)
            number_of_threads = 1;
        signatures_enabled = new bool * * [number_of_experiments];
        signatures_queries = new bool * * [number_of_experiments];
        for (unsigned int i = 0; i < number_of_experiments; ++i)
        {
            signatures_enabled[i] = new bool * [experiments[i].getNumberOfTrials()];
            signatures_queries[i] = new bool * [experiments[i].getNumberOfTrials()];
            for (unsigned int j = 0; j < experiments[i].getNumberOfTrials(); ++j)
            {
                selection_set.clear();
                signatures_enabled[i][j] = new bool[m_number_of_signatures];
                for (unsigned int k = 0; k < experiments[i][j].getNumberOfSnippets(); ++k)
                    if (experiments[i][j][k].getSearchable())
                        selection_set.insert(experiments[i][j][k].getIdentifier());
                for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                {
                    if (selection_set.find(m_snippet_identifier[k]) != selection_set.end())
                        signatures_enabled[i][j][k] = true;
                    else signatures_enabled[i][j][k] = false;
                }
                
                selection_set.clear();
                signatures_queries[i][j] = new bool[m_number_of_signatures];
                for (unsigned int k = 0; k < experiments[i][j].getNumberOfSnippets(); ++k)
                    if (experiments[i][j][k].getQuery())
                        selection_set.insert(experiments[i][j][k].getIdentifier());
                for (unsigned int k = 0; k < m_number_of_signatures; ++k)
                {
                    if (selection_set.find(m_snippet_identifier[k]) != selection_set.end())
                        signatures_queries[i][j][k] = true;
                    else signatures_queries[i][j][k] = false;
                }
            }
        }
        
        distances_rank.set(m_number_of_signatures);
        distances_thread = new VectorDense<float>[number_of_threads];
        for (unsigned int t = 0; t < number_of_threads; ++t)
            distances_thread[t].set(m_number_of_signatures);
        if (logger != 0)
            logger->log("Evaluating the visual index using the %s distance.", ((distance_identifier == DOCUMENT_INDEX_DISTANCE_EUCLIDEAN)?"Euclidean":"Manhattan"));
        
        for (unsigned int i = 0; i < m_number_of_signatures; ++i)
        {
            bool not_query;
            
            // Skip queries which are not required by any experiment.
            not_query = true;
            for (unsigned int idxe = 0; idxe < number_of_experiments; ++idxe)
                for (unsigned int idxt = 0; idxt < experiments[idxe].getNumberOfTrials(); ++idxt)
                    if (signatures_queries[idxe][idxt][i]) not_query = false;
            if (not_query) continue;
            if (logger != 0)
                logger->log("Query %d/%d: %s                       \r", i + 1, m_number_of_signatures, &m_word_text[m_word_offset[m_word_category[i]]]);
            // Compute the rank list.
            computeRank(i, distances_thread, distance_identifier, distances_rank, number_of_threads);
            
            for (unsigned int idxe = 0; idxe < number_of_experiments; ++idxe)
            {
                for (unsigned int idxt = 0; idxt < experiments[idxe].getNumberOfTrials(); ++idxt)
                {
                    if (signatures_queries[idxe][idxt][i])
                    {
                        unsigned int total_valid;
                        
                        total_valid = 0;
                        for (unsigned int ni = 0; ni < m_number_of_signatures; ++ni)
                        {
                            const unsigned int idx = std::get<1>(distances_rank[ni]);
                            // Check if the current rank contains...
                            if ((m_snippet_identifier[idx] != m_snippet_identifier[i])  // snippets with a different identifier (i.e. which are not the query snippet itself),
                                && signatures_enabled[idxe][idxt][idx]                  // that are enabled,
                                && (m_word_category[idx] == m_word_category[i]))        // and belong to the same word category.
                                    ++total_valid;
                        }
                        if (total_valid > 0)
                        {
#if 0
                            /* DEBUG */ unsigned int debug_counter;
                            /* DEBUG */
                            /* DEBUG */ debug_counter = 0;
                            /* DEBUG */ for (unsigned int debug_i = 0; debug_i < m_number_of_signatures; ++debug_i)
                            /* DEBUG */ {
                            /* DEBUG */     const unsigned int debug_idx = std::get<1>(distances_rank[debug_i]);
                            /* DEBUG */     // Matching instances are...
                            /* DEBUG */     if ((m_snippet_identifier[debug_idx] != m_snippet_identifier[i])        // snippets with a different identifier (i.e. which are not the query snippet itself),
                            /* DEBUG */         && signatures_enabled[idxe][idxt][debug_idx]                        // that are enabled,
                            /* DEBUG */         && (m_word_category[debug_idx] == m_word_category[i]))             // and belong to the same word category.
                            /* DEBUG */         ++debug_counter;
                            /* DEBUG */ }
                            /* DEBUG */ if (debug_counter > 0)  // If there are matching samples in the rank.
                            /* DEBUG */ {
                            /* DEBUG */     printf("%02d %02d %05d %020lu %12s:", idxe, idxt, i, m_snippet_identifier[i], getWordCategoryText(m_word_category[i]));
                            /* DEBUG */     for (unsigned int debug_i = 0; debug_i < m_number_of_signatures; ++debug_i)
                            /* DEBUG */     {
                            /* DEBUG */         const unsigned int debug_idx = std::get<1>(distances_rank[debug_i]);
                            /* DEBUG */         // Matching instances are...
                            /* DEBUG */         if ((m_snippet_identifier[debug_idx] != m_snippet_identifier[i])        // snippets with a different identifier (i.e. which are not the query snippet itself),
                            /* DEBUG */             && signatures_enabled[idxe][idxt][debug_idx]                        // that are enabled,
                            /* DEBUG */             && (m_word_category[debug_idx] == m_word_category[i]))             // and belong to the same word category.
                            /* DEBUG */             printf(" %020lu", m_snippet_identifier[debug_idx]);
                            /* DEBUG */     }
                            /* DEBUG */     printf("\n");
                            /* DEBUG */ }
#endif
                            for (unsigned int idxs = 0; idxs < experiments[idxe].getNumberOfScores(); ++idxs)
                            {
                                double current_score;
                                
                                current_score = experiments[idxe].getScore(idxs)->evaluate(m_snippet_identifier[i], m_word_category[i], distances_rank.getData(), m_number_of_signatures, m_snippet_identifier, m_word_category, signatures_enabled[idxe][idxt]);
                                if (current_score >= 0)
                                {
                                    std::get<0>(scores[idxe][idxt][idxs]) += current_score;
                                    std::get<1>(scores[idxe][idxt][idxs]) += 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        if (logger != 0)
            logger->log("");
        // Calculate the average score.
        for (unsigned int i = 0; i < number_of_experiments; ++i)
            for (unsigned int j = 0; j < experiments[i].getNumberOfTrials(); ++j)
                for (unsigned int k = 0; k < experiments[i].getNumberOfScores(); ++k)
                    std::get<0>(scores[i][j][k]) /= (double)std::get<1>(scores[i][j][k]);
        
        // Free allocated memory.
        delete [] distances_thread;
        for (unsigned int i = 0; i < number_of_experiments; ++i)
        {
            for (unsigned int j = 0; j < experiments[i].getNumberOfTrials(); ++j)
            {
                delete [] signatures_enabled[i][j];
                delete [] signatures_queries[i][j];
            }
            delete [] signatures_enabled[i];
            delete [] signatures_queries[i];
        }
        delete [] signatures_enabled;
        delete [] signatures_queries;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INDEX GENERATOR      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    /// Object that creates the BoVW histograms with the given visual information object.
    class DocumentVisualIndexGenerator
    {
    public:
        // -[  Constructors, destructor and assignation operator ]-----------------------------------------------------------------------------------
        /// Default constructor.
        DocumentVisualIndexGenerator(void);
        /// Copy constructor.
        DocumentVisualIndexGenerator(const DocumentVisualIndexGenerator &other);
        /// Destructor.
        ~DocumentVisualIndexGenerator(void);
        /// Assignation operator.
        DocumentVisualIndexGenerator& operator=(const DocumentVisualIndexGenerator &other);
        
        // -[ Initialization functions ]-------------------------------------------------------------------------------------------------------------
        /// Sets the encoding parameters.
        inline void setEncode(const CodewordWeightBase<double, double, double, unsigned int> * codeword_weight, CodewordEncodeBase<double, double, double, unsigned int> * codeword_encode, unsigned int number_of_neighbors, POOLING_METHOD_IDENTIFIER pooling_identifier, NEGATIVE_METHOD_IDENTIFIER negative_identifier)
        {
            // Free memory.
            if (m_codeword_weight != 0) delete m_codeword_weight;
            if (m_codeword_encode != 0) delete m_codeword_encode;
            // Set if all values are valid.
            if ((codeword_weight != 0) && (codeword_encode != 0) && (number_of_neighbors > 0))
            {
                m_codeword_weight = codeword_weight->duplicate();
                m_codeword_encode = codeword_encode->duplicate();
                m_number_of_neighbors = number_of_neighbors;
            }
            // Reset otherwise.
            else
            {
                m_codeword_weight = 0;
                m_codeword_encode = 0;
                m_number_of_neighbors = 0;
            }
            m_pooling_identifier = pooling_identifier;
            m_negative_identifier = negative_identifier;
        }
        /// Sets the spatial partitions information.
        inline void setSpatialPartitions(const VectorDense<std::tuple<unsigned int, unsigned int> > &partition_values)
        {
            m_partitions_values = partition_values;
            m_number_of_spatial_bins = 0;
            for (unsigned int i = 0; i < partition_values.size(); ++i)
                m_number_of_spatial_bins += std::get<0>(partition_values[i]) * std::get<1>(partition_values[i]);
        }
        /// Sets the normalization information.
        inline void setNormalization(const VectorNorm &normalization_spatial, double power_factor, const VectorNorm &normalization_global)
        {
            m_normalization_spatial = normalization_spatial;
            m_power_factor = power_factor;
            m_normalization_global = normalization_global;
        }
        
        // -[ Access functions ]---------------------------------------------------------------------------------------------------------------------
        /// Returns a constant pointer to the object used to weight the contribution of the codewords representing an encoded descriptor.
        inline const CodewordWeightBase<double, double, double, unsigned int> * getCodewordWeight(void) const { return m_codeword_weight; }
        /// Returns a constant pointer to the object used to encode a descriptor with the given codewords.
        inline const CodewordEncodeBase<double, double, double, unsigned int> * getCodewordEncode(void) const { return m_codeword_encode; }
        /// Returns the number of codewords used to encode a descriptor.
        inline unsigned int getNumberOfNeighbors(void) const { return m_number_of_neighbors; }
        /// Returns the identifier of the method used to pool the different visual words in the signature.
        inline POOLING_METHOD_IDENTIFIER getPoolingIdentifier(void) const { return m_pooling_identifier; }
        /// Returns the identifier of the method used to treat negative visual word contributions.
        inline NEGATIVE_METHOD_IDENTIFIER getNegativeIdentifier(void) const { return m_negative_identifier; }
        /// Returns the number of spatial levels of the visual pyramid representation.
        inline unsigned int getNumberOfSpatialLevels(void) const { return m_partitions_values.size(); }
        /// Returns a tuple with the (horizontal,vertical)-tuple of the spatial partition of the index-th level.
        inline const std::tuple<unsigned int, unsigned int>& getSpatialPartition(unsigned int index) const { return m_partitions_values[index]; }
        /// Returns the number of spatial bins of the signature.
        inline unsigned int getNumberOfSpatialBins(void) const { return m_number_of_spatial_bins; }
        /// Returns the object used to normalize each spatial level of the signature.
        inline const VectorNorm& getNormalizationSpatial(void) const { return m_normalization_spatial; }
        /// Returns the power factor applied each bin of the visual signature.
        inline double getPowerFactor(void) const { return m_power_factor; }
        /// Returns the object used to normalize the whole signature vector.
        inline const VectorNorm& getNormalizationGlobal(void) const { return m_normalization_global; }
        
        // -[ Generation functions ]-----------------------------------------------------------------------------------------------------------------
        /** Computes the visual signature of the selected word snippet.
         *  \param[in] selected_idx index of the selected word snippet.
         *  \param[in] visual_information document visual information.
         *  \param[out] signature resulting signature.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the signature.
         *  \param[in] logger logger object used to show generation messages (set to null to not show any message).
         */
        void computeSignature(unsigned int selected_idx, const DocumentVisualInformation &visual_information, VectorDense<double> &signature, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /** Generates the BoVW signatures of all the snippets of the given visual information object.
         *  \param[in] visual_information object containing the visual information used to create the index of the document snippets.
         *  \param[out] visual_index visual index generated with the current parameters of the generator.
         *  \param[in] number_of_threads number of threads used to process the information concurrently.
         *  \param[in] logger logger object used to show generation messages (set to null to not show any message).
         */
        template <class TVALUE, class TINDEX>
        void generate(const DocumentVisualInformation &visual_information, DocumentVisualIndex<TVALUE, TINDEX> &visual_index, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        
        // -[ XML functions ]------------------------------------------------------------------------------------------------------------------------
        /// Stores the histogram generation parameters in a XML object.
        void convertToXML(XmlParser &parser) const;
        /// Retrieves the histogram generation parameters from a XML object.
        void convertFromXML(XmlParser &parser);
    protected:
        /** Computes the visual signature of the selected word snippet.
         *  \param[in] selected_idx index of the selected word snippet.
         *  \param[in] visual_information document visual information.
         *  \param[in] descriptors_double temporary structure where the real descriptors are stored.
         *  \param[in] descriptors_ptrs temporary structure with the vector pointers to the descriptors.
         *  \param[in] descriptors_distances temporary structure which stores the distances between the descriptor and the codewords.
         *  \param[in] descriptors_codes temporary structure which store the codes encoding each descriptor.
         *  \param[out] signature resulting signature.
         *  \param[in] number_of_threads number of threads used to concurrently calculate the signature.
         *  \param[in] logger logger object used to show generation messages (set to null to not show any message).
         */
        void computeSignature(unsigned int selected_idx, const DocumentVisualInformation &visual_information, VectorDense<double> * descriptors_double, ConstantSubVectorDense<double> * * descriptors_ptrs, VectorSparse<double> * descriptors_distances, VectorSparse<double> * descriptors_codes, VectorDense<double> &signature, unsigned int number_of_threads, BaseLogger * logger = 0) const;
        /// Method used to contribution of the different codewords representing a descriptor.
        CodewordWeightBase<double, double, double, unsigned int> * m_codeword_weight;
        /// Method used to encode a descriptor.
        CodewordEncodeBase<double, double, double, unsigned int> * m_codeword_encode;
        /// Number of neighbors used to encode a descriptor.
        unsigned int m_number_of_neighbors;
        /// Identifier of the method used to group the codewords 
        POOLING_METHOD_IDENTIFIER m_pooling_identifier;
        /// Identifier of the method used to treat negative visual word contributions.
        NEGATIVE_METHOD_IDENTIFIER m_negative_identifier;
        /// Vector with the spatial partitions of the visual signature.
        VectorDense<std::tuple<unsigned int, unsigned int> > m_partitions_values;
        /// Number of spatial bins of the visual signature.
        unsigned int m_number_of_spatial_bins;
        /// Normalization applied at each spatial level of the signature.
        VectorNorm m_normalization_spatial;
        /// Power factor applied each bin of the visual signature.
        double m_power_factor;
        /// Normalization applied to the whole signature vector.
        VectorNorm m_normalization_global;
    };
    
    template <class TVALUE, class TINDEX>
    void DocumentVisualIndexGenerator::generate(const DocumentVisualInformation &visual_information, DocumentVisualIndex<TVALUE, TINDEX> &visual_index, unsigned int number_of_threads, BaseLogger * logger) const
    {
        const unsigned int number_of_dimensions = visual_information.getNumberOfDimensions();
        const unsigned int encode_size = m_codeword_encode->getNumberOfBinsPerCodeword(number_of_dimensions) * ((m_negative_identifier == NEGATIVE_SEPARATE)?2:1);
        const unsigned int signature_size = m_number_of_spatial_bins * encode_size * visual_information.getNumberOfCodewords();
        VectorDense<double> signature(signature_size);
        VectorSparse<double> * descriptors_distances, * descriptors_codes;
        ConstantSubVectorDense<double> * * descriptors_ptrs;
        VectorDense<double> * descriptors_double;
        unsigned int maximum_number_of_descriptors;
        unsigned long total_non_zero;
        double average_sparsity;
        
        visual_index.initialize(signature_size, visual_information.getNumberOfSnippets());
        visual_index.setWordCategories(visual_information.getWordTextVector(), visual_information.getWordOffsetVector());
        visual_index.clearInvertedFile();
        maximum_number_of_descriptors = 0;
        for (unsigned int i = 0; i < visual_information.getNumberOfSnippets(); ++i)
            maximum_number_of_descriptors = std::max(maximum_number_of_descriptors, visual_information[i].getNumberOfDescriptors());
        if (logger != 0)
            logger->log("Allocating structures for a maximum of %d descriptors.", maximum_number_of_descriptors);
        descriptors_ptrs = new ConstantSubVectorDense<double> * [maximum_number_of_descriptors];
        descriptors_double = new VectorDense<double>[maximum_number_of_descriptors];
        descriptors_distances = new VectorSparse<double>[maximum_number_of_descriptors];
        descriptors_codes = new VectorSparse<double>[maximum_number_of_descriptors];
        for (unsigned int i = 0; i < maximum_number_of_descriptors; ++i)
        {
            descriptors_double[i].set(number_of_dimensions);
            descriptors_ptrs[i] = new ConstantSubVectorDense<double>(descriptors_double[i]);
            descriptors_distances[i].set(m_number_of_neighbors);
        }
        average_sparsity = 0.0;
        if (logger != 0)
            logger->log("Allocating memory for the inverted file.");
        total_non_zero = 0;
        for (unsigned int i = 0; i < visual_information.getNumberOfSnippets(); ++i)
        {
            unsigned long non_zero;
            
            if (visual_information[i].getNumberOfDistances() < m_number_of_neighbors)
                throw Exception("The visual information of the %d-th snippet does not contain enough neighbor distances pre-computed.", i);
            if (logger != 0)
                logger->log("Snippet %07d [%s]: Gathering information of %d descriptors.         \r", i, visual_information.getWord(visual_information[i].getWordCategoryID()), visual_information[i].getNumberOfDescriptors());
            computeSignature(i, visual_information, descriptors_double, descriptors_ptrs, descriptors_distances, descriptors_codes, signature, number_of_threads, logger);
            total_non_zero += (non_zero = visual_index.updateInvertedFileSize(signature));
            if (logger != 0)
                logger->log("Snippet %07d [%s]: %.1f%% sparse [DONE]                          ", i, visual_information.getWord(visual_information[i].getWordCategoryID()), 100.0 * (1.0 - (double)non_zero / (double)signature.size()));
            average_sparsity += 1.0 - (double)non_zero / (double)signature.size();
        }
        average_sparsity /= (double)visual_information.getNumberOfSnippets();
        if (logger != 0)
        {
            logger->log("Signature average sparsity: %.1f%%", 100.0 * average_sparsity);
            logger->log("Total non zero elements: %ld [Minimum memory required: %ld bytes]", total_non_zero, total_non_zero * ((unsigned long)sizeof(DEF_TVALUE) + (unsigned long)sizeof(DEF_TINDEX)));
        }
        visual_index.allocateInvertedFile();
        for (unsigned int i = 0; i < visual_information.getNumberOfSnippets(); ++i)
        {
            
            // Gather the information of the current snippet.
            if (logger != 0)
                logger->log("Snippet %07d [%s]: Gathering information of %d descriptors.         \r", i, visual_information.getWord(visual_information[i].getWordCategoryID()), visual_information[i].getNumberOfDescriptors());
            computeSignature(i, visual_information, descriptors_double, descriptors_ptrs, descriptors_distances, descriptors_codes, signature, number_of_threads, logger);
            if (logger != 0)
                logger->log("Snippet %07d [%s]: [DONE]                          ", i, visual_information.getWord(visual_information[i].getWordCategoryID()));
            visual_index.setSignature(i, VectorDense<float>(signature), visual_information[i].getWordCategoryID(), visual_information[i].getSnippetIdentifier());
        }
        // Free allocated memory.
        delete [] descriptors_distances;
        delete [] descriptors_double;
        delete [] descriptors_codes;
        for (unsigned int i = 0; i < maximum_number_of_descriptors; ++i)
            delete descriptors_ptrs[i];
        delete [] descriptors_ptrs;
        // Call the function which creates the inverted file.
        if (logger != 0)
            logger->log("Creating the inverted file [Signatures have %d dimensions and they are %.1f%% sparse].", visual_index.getNumberOfDimensions(), 100.0 * average_sparsity / (double)visual_information.getNumberOfSnippets());
    }
    
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

#endif
