// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"
#include "common_functions.hpp"

INITIALIZE_DENSE_VISUAL_WORDS(short, short, unsigned char, int, unsigned int)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              21-May-2018\n"
                                   "=================================================================================\n"
                                   " Extract the visual words information of each document snippet of the dataset.\n\n";
    
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char filename_database[4096], filename_visual_information[4096];
    unsigned int number_of_threads, maximum_number_of_neighbors;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::DenseVisualWords<short, short, unsigned char, int, unsigned int> visual_words;
    srv::ParameterParser parameters("./extract_information", initial_message);
    srv::DocumentVisualInformation visual_information;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        char filename_codebook[4096];
        
        // Define user parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_database", "file with the information of the documents used to generate the codebook", filename_database));
        parameters.addParameter(new srv::ParameterCharArray("filename_codebook", "file where the codebook used to extract the visual words from the image.", filename_codebook));
        parameters.addParameter(new srv::ParameterCharArray("filename_visual_information", "file where the document's visual information is stored into.", filename_visual_information));
        parameters.addParameter(new srv::ParameterUInt("--nn", "neighbors", "maximum number of neighbors used to encode descriptors.", 50, true, 1, false, 0, &maximum_number_of_neighbors));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently generate the codebook.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parser the user parameters.
        parameters.parse(argv, argc);
        
        // Loads the codebook.
        loadObject(filename_codebook, visual_words);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Densely sample descriptors ]---------------------------------------------------------------------------------------------------------------
    try
    {
        srv::compressionPolicy(srv::ZLibCompression);
        
        std::ostream * file = srv::oZipStream(filename_visual_information);
        srv::DocumentDatabase database(filename_database);
        srv::XmlParser parser(file);
        
        if (file == 0)
            throw srv::Exception("Visual information file '%s' cannot be created.", filename_visual_information);
        if (file->rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while creating '%s'.", filename_visual_information);
        
        visual_information.initialize(database, visual_words, maximum_number_of_neighbors, number_of_threads, &logger);
        logger.log("Saving results to '%s'...", filename_visual_information);
        visual_information.convertToXML(parser);
        logger.log("Done.");
        
        if (file->rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while saving the object into '%s'.", filename_visual_information);
        delete file;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the codebook from the database images:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

