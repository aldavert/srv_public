// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"
#include "common_functions.hpp"

INITIALIZE_CODEWORD_WEIGHT_FACTORIES(double, double, double, unsigned int)
INITIALIZE_CODEWORD_ENCODE_FACTORIES(double, double, double, unsigned int)
INITIALIZE_DENSE_VISUAL_WORDS(short, short, unsigned char, int, unsigned int)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                             15-June-2018\n"
                                   "=================================================================================\n"
                                   " Calculates the performance of multiple BoVW signature configurations using\n"
                                   " different evaluation parameters.\n\n";
    
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char filename_visual_information[4096], filename_output[4096];
    unsigned int number_of_threads;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./index_batch", initial_message);
    srv::VectorDense<srv::DocumentVisualIndexGenerator> histogram_parameters;
    srv::VectorDense<srv::ExperimentInformation> experiments;
    srv::DocumentVisualInformation visual_information;
    srv::StreamLogger logger(&std::cout);
    
    try
    {
        char filename_parameters[4096], filename_experiments[4096];
        std::list<srv::DocumentVisualIndexGenerator *> histogram_parameters_list;
        unsigned int idx;
        
        parameters.addParameter(new srv::ParameterCharArray("filename_visual_information", "file where the document's visual information is stored into.", filename_visual_information));
        parameters.addParameter(new srv::ParameterCharArray("filename_parameters", "file with the different visual words configuration parameters evaluated.", filename_parameters));
        parameters.addParameter(new srv::ParameterCharArray("filename_experiments", "file with the experiments used to evaluate the different histogram configurations.", filename_experiments));
        parameters.addParameter(new srv::ParameterCharArray("filename_output", "file where the results are stored into.", filename_output));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently evaluate the index.", 1, true, 1, false, 0, &number_of_threads));
        
        parameters.parse(argv, argc);
        
        logger.log("Loading experiments.");
        srv::loadExperiments(filename_experiments, experiments);
        logger.log("Loaded '%d' experiments.", experiments.size());
        
        logger.log("Loading histogram configurations.", experiments.size());
        std::ifstream file(filename_parameters);
        if (!file.is_open()) throw srv::Exception("Parameters file '%s' cannot be opened.", filename_parameters);
        srv::XmlParser parser(&file);
        while (!(parser.isTagIdentifier("DocumentVisualIndexParameters") && parser.isCloseTag()))
        {
            histogram_parameters_list.push_back(new srv::DocumentVisualIndexGenerator());
            histogram_parameters_list.back()->convertFromXML(parser);
        }
        file.close();
        
        histogram_parameters.set((unsigned int)histogram_parameters_list.size());
        idx = 0;
        for (auto &v : histogram_parameters_list)
        {
            histogram_parameters[idx++] = *v;
            delete v;
        }
        logger.log("Loaded '%d' configurations.", histogram_parameters.size());
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load the visual information object ]-------------------------------------------------------------------------------------------------------
    try
    {
        srv::compressionPolicy(srv::ZLibCompression);
        std::istream * file = srv::iZipStream(filename_visual_information);
        
        logger.log("Loading visual information from '%s'.", filename_visual_information);
        if (file == 0) throw srv::Exception("File '%s' cannot be loaded.", filename_visual_information);
        srv::XmlParser parser(file);
        
        if (parser.isTagIdentifier("DocumentVisualInformation")) visual_information.convertFromXML(parser);
        else throw srv::Exception("XML file '%s' does not contain a document visual information object.", filename_visual_information);
        
        delete file;
        logger.log("Done");
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the visual information object:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Test the different visual configurations ]-------------------------------------------------------------------------------------------------
    try
    {
        const srv::INDEX_DISTANCE_IDENTIFIER test_distances[] = { srv::DOCUMENT_INDEX_DISTANCE_EUCLIDEAN, srv::DOCUMENT_INDEX_DISTANCE_MANHATTAN };
        srv::VectorDense<srv::VectorDense<srv::VectorDense<std::tuple<double, unsigned int> > > > scores;
        std::ofstream file(filename_output);
        srv::XmlParser parser(&file);
        
        if (!file.is_open()) throw srv::Exception("Cannot open file '%s' to store the experiments information.", filename_output);
        if (file.rdstate() != std::ios_base::goodbit) throw srv::Exception("An error flag has been raised while creating '%s'.", filename_output);
        logger.log("Generating visual signatures.");
        for (unsigned int hp_idx = 0; hp_idx < histogram_parameters.size(); ++hp_idx)
        {
            srv::DocumentVisualIndex<DEF_TVALUE, DEF_TINDEX> visual_index;
            
            logger.log("Testing histogram visual configuration %d/%d:", hp_idx + 1, histogram_parameters.size());
            // ## SHOW INFORMATION ################################################################
            switch (histogram_parameters[hp_idx].getCodewordWeight()->getIdentifier())
            {
            case srv::CODEWORD_WEIGHT_UNIFORM:
                logger.log(" · Codeword weight: Uniform");
                break;
            case srv::CODEWORD_WEIGHT_DISTANCE:
                logger.log(" · Codeword weight: Distance");
                break;
            case srv::CODEWORD_WEIGHT_LINEARLY_CONSTRAINED_LOCAL_CODING:
                logger.log(" · Codeword weight: Linearly-constrained Local Coding (LLC)");
                break;
            case srv::CODEWORD_WEIGHT_GAUSSIAN_KERNEL:
                logger.log(" · Codeword weight: Gaussian Kernel (sigma=%f)", ((srv::CodewordWeightGaussian<double, double, double, unsigned int> *)histogram_parameters[hp_idx].getCodewordWeight())->getSigma());
                break;
            case srv::CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL:
                logger.log(" · Codeword weight: Normalized Gaussian Kernel (sigma=%f)", ((srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int> *)histogram_parameters[hp_idx].getCodewordWeight())->getSigma());
                break;
            case srv::CODEWORD_WEIGHT_FISHER_VECTOR:
                logger.log(" · Codeword weight: Fisher like");
                break;
            default:
                logger.log(" · Codeword weight: UNKNOWN");
                break;
            }
            switch (histogram_parameters[hp_idx].getCodewordEncode()->getIdentifier())
            {
            case srv::CODEWORD_ENCODE_CENTROID:
                logger.log(" · Codeword encode: Centroid");
                break;
            case srv::CODEWORD_ENCODE_FIRST_DERIVATIVE:
                logger.log(" · Codeword encode: First derivative (VLAD)");
                break;
            case srv::CODEWORD_ENCODE_SUPER_VECTOR:
                logger.log(" · Codeword encode: Supervector (weight=%f)", ((srv::CodewordEncodeSuperVector<double, double, double, unsigned int> *)histogram_parameters[hp_idx].getCodewordEncode())->getWeight());
                break;
            case srv::CODEWORD_ENCODE_FISHER_VECTOR:
                logger.log(" · Codeword encode: Fisher vector");
                break;
            case srv::CODEWORD_ENCODE_TENSOR:
                logger.log(" · Codeword encode: Tensor");
                break;
            default:
                logger.log(" · Codeword encode: UNKNOWN");
                break;
            }
            logger.log(" · Number of neighbors: %d", histogram_parameters[hp_idx].getNumberOfNeighbors());
            if      (histogram_parameters[hp_idx].getPoolingIdentifier() == srv::POOLING_ADDITIVE        ) logger.log(" · Pooling: Additive");
            else if (histogram_parameters[hp_idx].getPoolingIdentifier() == srv::POOLING_MAXIMUM         ) logger.log(" · Pooling: Maximum");
            else if (histogram_parameters[hp_idx].getPoolingIdentifier() == srv::POOLING_ABSOLUTE_MAXIMUM) logger.log(" · Pooling: Absolute value maximum");
            if      (histogram_parameters[hp_idx].getNegativeIdentifier() == srv::NEGATIVE_NOTHING ) logger.log(" · Negative: do nothing");
            else if (histogram_parameters[hp_idx].getNegativeIdentifier() == srv::NEGATIVE_ABSOLUTE) logger.log(" · Negative: absolute value");
            else if (histogram_parameters[hp_idx].getNegativeIdentifier() == srv::NEGATIVE_SEPARATE) logger.log(" · Negative: separate");
            logger.log(" · Number of spatial levels: %d", histogram_parameters[hp_idx].getNumberOfSpatialLevels());
            for (unsigned int l = 0; l < histogram_parameters[hp_idx].getNumberOfSpatialLevels(); ++l)
                logger.log(" · Spatial partition %d: %dx%d", l + 1, std::get<0>(histogram_parameters[hp_idx].getSpatialPartition(l)), std::get<1>(histogram_parameters[hp_idx].getSpatialPartition(l)));
            logger.log(" · Total number of spatial bins: %d", histogram_parameters[hp_idx].getNumberOfSpatialBins());
            switch (histogram_parameters[hp_idx].getNormalizationSpatial().getIdentifier())
            {
            case srv::MANHATTAN_NORM:
                logger.log(" · Normalization spatial: Manhattan norm");
                break;
            case srv::EUCLIDEAN_NORM:
                logger.log(" · Normalization spatial: Euclidean-norm");
                break;
            case srv::MAXIMUM_NORM:
                logger.log(" · Normalization spatial: Maximum-norm (uniform-norm)");
                break;
            case srv::P_NORM:
                logger.log(" · Normalization spatial: P-norm (p=%f)", histogram_parameters[hp_idx].getNormalizationSpatial().getOrder());
                break;
            default:
                logger.log(" · Normalization spatial: UNKNOWN");
                break;
            }
            logger.log(" · Power factor: %f", histogram_parameters[hp_idx].getPowerFactor());
            switch (histogram_parameters[hp_idx].getNormalizationGlobal().getIdentifier())
            {
            case srv::MANHATTAN_NORM:
                logger.log(" · Normalization spatial: Manhattan norm");
                break;
            case srv::EUCLIDEAN_NORM:
                logger.log(" · Normalization spatial: Euclidean-norm");
                break;
            case srv::MAXIMUM_NORM:
                logger.log(" · Normalization spatial: Maximum-norm (uniform-norm)");
                break;
            case srv::P_NORM:
                logger.log(" · Normalization spatial: P-norm (p=%f)", histogram_parameters[hp_idx].getNormalizationGlobal().getOrder());
                break;
            default:
                logger.log(" · Normalization spatial: UNKNOWN");
                break;
            }
            // ## SHOW INFORMATION ################################################################
            
            logger.log("Generating index");
            histogram_parameters[hp_idx].generate(visual_information, visual_index, number_of_threads, &logger);
            
            for (unsigned int distance_idx = 0; distance_idx < 2; ++distance_idx)
            {
                char distance_name[64];
                
                sprintf(distance_name, "%s", ((test_distances[distance_idx] == srv::DOCUMENT_INDEX_DISTANCE_EUCLIDEAN)?("EUCLIDEAN"):("MANHATTAN")));
                logger.log("Index evaluation using %s distance:", distance_name);
                parser.openTag("results");
                parser.setAttribute("distance", distance_name);
                parser.addChildren();
                histogram_parameters[hp_idx].convertToXML(parser);
                visual_index.evaluate(experiments.getData(), experiments.size(), test_distances[distance_idx], scores, number_of_threads, &logger);
                for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
                {
                    for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                    {
                        logger.log("Experiment: %s  [%s]", experiments[eidx].getName(), experiments[eidx].getScore(sidx)->getName());
                        for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                            logger.log("Trial %s: %f [%d queries]", experiments[eidx][tidx].getName(), std::get<0>(scores[eidx][tidx][sidx]), std::get<1>(scores[eidx][tidx][sidx]));
                    }
                }
                for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
                {
                    parser.openTag("experiment");
                    parser.setAttribute("name", experiments[eidx].getName());
                    parser.addChildren();
                    for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                    {
                        parser.openTag("trial");
                        parser.setAttribute("name", experiments[eidx][tidx].getName());
                        parser.addChildren();
                        for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                        {
                            parser.openTag("score");
                            parser.setAttribute("value", std::get<0>(scores[eidx][tidx][sidx]));
                            parser.setAttribute("queries", std::get<1>(scores[eidx][tidx][sidx]));
                            parser.setAttribute("type", experiments[eidx].getScore(sidx)->getAbbreviation());
                            parser.closeTag();
                        }
                        parser.closeTag();
                    }
                    parser.closeTag();
                }
                parser.closeTag();
            }
        }
        
        if (file.rdstate() != std::ios_base::goodbit) throw srv::Exception("An error flag has been raised while saving the object into '%s'.", filename_output);
        file.close();
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the histograms:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
