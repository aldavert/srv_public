# SRV project: _Word Spotting_

Project used to compute the performance of word spotting approaches where words are pre-segmented and characterized with the Bag-of-Visual-Words framework. This project has been used to generate the results from:

D. Aldavert, M. Rusiñol, R. Toledo, J. Lladós, ["A Study of Bag-of-Visual-Words Representations for Handwritten Keyword Spotting"](http://www.cvc.uab.es/people/aldavert/documents/journals/ijdar2015.pdf), IJDAR 2015

The project is located in `projects/word_spotting/`. Use `make RELEASE=1` to generate the binaries in release mode (otherwise, they are generated without optimization and adding debug information). This will generate seven binaries:

* **dense_descriptor:** Creates a local descriptor object which will be used to generate the visual words. For example, the command:
```bash
$ ./dense_descriptor 1 examples/descriptor_hog32.xml.gz --size 16:24:32:40 --step 3\
    --ihog-partition-x 2 --ihog-partition-y 2
```
generates the descriptor object found at the `examples` folder.
* **codebook_generate:** Creates the codebook from the given database and returns an object that will be able to extract visual words from images (i.e. it contains both the descriptor and the codebook). For example, the command
```bash
$ ./codebook_generate DBs/GW/washington.xml 256 examples/descriptor_hog32.xml.gz\
    examples/codebook_hog32_256c.xml.gz --iterations 100 --cutoff 10 --max-samples 100000 --thr 4
```
generates the codebook found at `examples/codebook_hog32_256c.xml.gz`. This is an small codebook used to demonstrate how codebooks are created. The file `washington.xml` is also added to the folder `examples/` and contains information about the words and its location (bounding boxes) present in a collection of images. This file expects that the images are present at a sub-folder called `images/` which contains:
```bash
$ ls DBs/GW/images/
Page270.png  Page272.png  Page274.png  Page276.png  Page278.png  Page300.png  Page302.png
Page304.png  Page306.png  Page308.png  Page271.png  Page273.png  Page275.png  Page277.png
Page279.png  Page301.png  Page303.png  Page305.png  Page307.png  Page309.png
```
This files are not available in the repository but you can download them from [IAM-HistDB](http://www.fki.inf.unibe.ch/databases/iam-historical-document-database/washington-database).
* **extract_information:** Extracts the visual words for all the snippets of the database. For example, the command
```bash
$ ./extract_information DBs/GW/washington.xml examples/codebook_hog32_256c.xml.gz\
    examples/information_hog32_256c.xml.gz --nn 7 --thr 4
```
extracts the visual words using the codebook previously generated and stores the 7 closest neighbors for each descriptor. This means that while creating the visual signatures a descriptor can be represented from a single codeword to 7 codewords _(file not included as it weights 1.1Gb)_.
* **index_create:** Creates a index with the visual signatures of the dataset snippets with the selected parameters. For example, the command
```bash
$ ./index_create examples/information_hog32_256c.xml.gz 2 0 3\
    examples/index_hog32_256c_vlad_nosqrt.xml.gz --pool 2 --pyramid-x 8 --pow 1.0\
    --norm-global 0 --thr 4
```
creates an XML file with the 4863 snippets of the dataset created using the VLAD encoding and without square root normalization of the signature _(file not included as it weights 12.9Mb)_.
* **index_evaluate:** Evaluates the retrieval performance of the index. For example, the command
```bash
$ ./index_evaluate examples/index_hog32_256c_vlad_nosqrt.xml.gz\
    examples/experiments_washington_batch.xml examples/output_hog32_256c_vlad_nosqrt.xml\
    --rank 0 --thr 4
[ 000000.64 ] Index evaluation using EUCLIDEAN distance:
[ 000000.64 ] Evaluating the visual index using the Euclidean distance.
[ 000029.51 ] Query 4862/4864: be
[ 000029.51 ] Experiment: normal  [mean Average Precision]
[ 000029.51 ] Trial normal: 0.444796 [4218 queries]
[ 000029.51 ] Index evaluation using MANHATTAN distance:
[ 000029.51 ] Evaluating the visual index using the Manhattan distance.
[ 000052.39 ] Query 4862/4864: be
[ 000052.39 ] Experiment: normal  [mean Average Precision]
[ 000052.39 ] Trial normal: 0.670358 [4218 queries]
```
evaluates the previously generated index using the experiments defined in the file `examples/experiments_washington_batch.xml`. This file sets the experiments to include all snippets in the index but only use snippets that appear multiple times as queries. It also sets that the index is evaluated using the mean Average Precision score.
* **index_parameters:** Creates an XML object with the information of the parameters used to generate the BoVW signature. For example the commands
```bash
$ ./index_parameters examples/parameters01.xml 2 1 3 --pool 2 --pyramid-x 8 --pow 0.5 --norm-global 0
$ ./index_parameters examples/parameters02.xml 2 1 3 --pool 2 --pyramid-x 8 --pow 1.0 --norm-global 0
$ ./index_parameters examples/parameters03.xml 2 0 3 --pool 2 --pyramid-x 8 --pow 0.5 --norm-global 0
$ ./index_parameters examples/parameters04.xml 2 0 3 --pool 2 --pyramid-x 8 --pow 1.0 --norm-global 0
$ cp examples/parameters01.xml examples/parameters_all.xml
$ tail -n+2 examples/parameters02.xml >> examples/parameters_all.xml
$ tail -n+2 examples/parameters03.xml >> examples/parameters_all.xml
$ tail -n+2 examples/parameters04.xml >> examples/parameters_all.xml
```
first generate four different configurations (enabling/disabling power normalization and codeword index/VLAD encodings) then we put them all together in the file `examples/parameters_all.xml`.
* **index_batch:** Evaluates multiple signature parameters and experiments configurations without storing the intermediate indexes to disk. For example the following command
```bash
$ ./index_batch examples/information_hog32_256c.xml.gz examples/parameters_all.xml\
    examples/experiments_washington_batch.xml examples/output_all.xml --thr 4
```
will evaluate the 4 signature configurations created in the previous example of `index_parameters`. The advantage of this binary is that it avoids saving indexes that will likely be discarded to disk and it also saves the time wasted loading multiple times the information file when multiple parameters are tested.
