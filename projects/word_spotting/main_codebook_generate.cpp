// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"

INITIALIZE_DENSE_DESCRIPTOR(short, short)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              18-May-2018\n"
                                   "=================================================================================\n"
                                   " Creates the codebook used to encode descriptors into visual words.\n\n";
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int sampling_method, maximum_train_samples, number_of_threads;
    char filename_database[4096], filename_codebook[4096];
    bool verbose;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::DenseVisualWords<short, short, unsigned char, int, unsigned int> visual_words;
    srv::ParameterParser parameters("./codebook_generate", initial_message);
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int random_seed, number_of_codewords, cluster_index, maximum_number_of_iterations, cutoff, maximum_number_of_neighbors;
        srv::CodewordEncodeCentroid<unsigned char, int, int, unsigned int> codeword_encode;
        srv::CodewordWeightUniform<unsigned char, int, int, unsigned int> codeword_weight;
        srv::ClusterCenterBasedBase<unsigned char, int, int, unsigned int> * cluster;
        srv::SeederBase<unsigned char, int, int, unsigned int> * seeder;
        srv::CodebookBase<unsigned char, int, unsigned int> * codebook;
        srv::DenseDescriptor<short, short> * dense_descriptor;
        double descriptor_norm_threshold;
        char filename_descriptor[4096];
        bool random_seed_initialized;
        
        // -[ Define use parameters ]--------------------------------------------------------------------------------------------------------------------
        parameters.addParameter(new srv::ParameterCharArray("filename_database", "file with the information of the documents used to generate the codebook", filename_database));
        parameters.addParameter(new srv::ParameterUInt("codebook_size", "number of codewords of the codebook (i.e. number of visual words).", true, 1, false, 0, &number_of_codewords));
        parameters.addParameter(new srv::ParameterCharArray("filename_descriptor", "file with the information of the used descriptors", filename_descriptor));
        parameters.addParameter(new srv::ParameterCharArray("filename_codebook", "file where the resulting codebook is stored.", filename_codebook));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Codebook parameters."));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterUInt("--cluster", "index", "algorithm used to create the clusters. Possible values are:"
                                                                             "&0&k-Means."
                                                                             "&1&k-Medians.", 0, true, 0, true, 1, &cluster_index));
        parameters.addParameter(new srv::ParameterUInt("--iterations", "maximum", "maximum number of iterations of the clustering algorithm.", 1000, true, 1, false, 0, &maximum_number_of_iterations));
        parameters.addParameter(new srv::ParameterUInt("--cutoff", "displacement", "minimum update required to stop the clustering algorithm.", 0, true, 0, false, 0, &cutoff));
        parameters.addParameter(new srv::ParameterUInt("--neighbors", "amount", "maximum number of neighbors considered by the partial triangle inequality while creating the clusters.", 512, true, 16, false, 0, &maximum_number_of_neighbors));
        parameters.addParameter(new srv::BlockSeparator(""));
        parameters.addParameter(new srv::BlockSeparator("Miscellaneous parameters."));
        parameters.addParameter(new srv::BlockSeparator("----------------------------------------------------------------------------"));
        parameters.addParameter(new srv::ParameterDouble("--descriptor-thr", "value", "minimum accumulated gradient value of the descriptor to accept it as a valid descriptor.", 1000, true, 0.0, false, 0.0, &descriptor_norm_threshold));
        parameters.addParameter(new srv::ParameterBoolean("--verbose", "", "show codebook creation information.", &verbose));
        parameters.addParameter(new srv::ParameterUInt("--max-samples", "number_of_descriptors", "maximum number of descriptors sampled from the database (set to zero to extract all possible descriptors).", 0, true, 0, false, 0, &maximum_train_samples));
        parameters.addParameter(new srv::ParameterUInt("--sampling", "index", "method used to sample the local descriptors when the maximum number of descriptors is reached. Possible values are:"
                                                                              "&0&Stop gathering new descriptors."
                                                                              "&1&Random substitutions.", 1, true, 0, true, 1, &sampling_method));
        parameters.addParameter(new srv::ParameterUInt("--random", "seed", "seed of the random number generator (time is used when not set).", 0, true, 0, false, 0, &random_seed, &random_seed_initialized));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently generate the codebook.", 1, true, 1, false, 0, &number_of_threads));
        
        random_seed_initialized = false;
        parameters.parse(argv, argc);
        
        // Set the random number generator.
        if (random_seed_initialized) srand(random_seed);
        else srand((unsigned int)time(0));
        
        // Load the dense descriptor object.
        srv::loadObject(filename_descriptor, dense_descriptor);
        const unsigned int number_of_dimensions = dense_descriptor->getNumberOfBins(3);
        
        // Use k-means++ as seeder.
        seeder = new srv::SeederPlus<unsigned char, int, int, unsigned int>(srv::VectorDistance(srv::EUCLIDEAN_DISTANCE), false, 1.0);
        // Use the (PARTIAL) fast triangle inequality k-means algorithm.
        if (cluster_index == 0) cluster = new srv::ClusterKMeansFast<unsigned char, int, int, unsigned int>(number_of_codewords, number_of_dimensions, seeder, srv::VectorDistance(srv::EUCLIDEAN_DISTANCE), maximum_number_of_iterations, cutoff, srv::CLUSTER_EMPTY_RESAMPLE, true, maximum_number_of_neighbors);
        else if (cluster_index == 1) cluster = new srv::ClusterKMediansFast<unsigned char, int, int, unsigned int>(number_of_codewords, number_of_dimensions, seeder, srv::VectorDistance(srv::EUCLIDEAN_DISTANCE), maximum_number_of_iterations, cutoff, srv::CLUSTER_EMPTY_RESAMPLE, true, maximum_number_of_neighbors);
        else throw srv::Exception("The selected clustering algorithm has not been implemeted yet.");
        codebook = new srv::CodebookExhaustive<unsigned char, int, int, unsigned int>(cluster, &codeword_weight, &codeword_encode, 1);
        delete cluster;
        delete seeder;
        
        // Initialize visual words object.
        visual_words.setDenseDescriptors(dense_descriptor);             // Set the dense descriptors object.
        visual_words.setDescriptorThreshold(descriptor_norm_threshold); // Descriptor threshold to filter out low gradient descriptors.
        visual_words.setCodebook(codebook);                             // Set the codebook object.
        
        // Free allocated memory.
        delete codebook;
        delete dense_descriptor;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Densely sample descriptors ]---------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int number_of_snippets, maximum_descriptor_size, number_of_valid_descriptors;
        srv::VectorDense<std::tuple<srv::VectorDense<unsigned char> *, unsigned int> > snippet_descriptors;
        srv::ConstantSubVectorDense<unsigned char> * * descriptor_ptrs;
        srv::DocumentDatabase database(filename_database);
        bool sample_descriptors;
        
        number_of_snippets = 0;
        for (unsigned int page_idx = 0; page_idx < database.getNumberOfPages(); ++page_idx)
            for (unsigned int block_idx = 0; block_idx < database[page_idx].getNumberOfBlocks(); ++block_idx)
                for (unsigned int line_idx = 0; line_idx < database[page_idx][block_idx].getNumberOfLines(); ++line_idx)
                    number_of_snippets += database[page_idx][block_idx][line_idx].getNumberOfSymbols();
        maximum_descriptor_size = 0;
        for (unsigned int i = 0; i < visual_words.getDenseDescriptors()->getNumberOfScales(); ++i)
            maximum_descriptor_size = std::max(maximum_descriptor_size, visual_words.getDenseDescriptors()->getScalesInformation(i).getRegionSize());
        logger.log("The database contains %d snippets.", number_of_snippets);
        snippet_descriptors.set(number_of_snippets);
        number_of_valid_descriptors = 0;
        sample_descriptors = true;
        for (unsigned int page_idx = 0, current_idx = 0; sample_descriptors && (page_idx < database.getNumberOfPages()); ++page_idx)
        {
            srv::Image<unsigned char> image_page;
            
            logger.log("Page %d of %d...", page_idx + 1, database.getNumberOfPages());
            database[page_idx].loadImage(image_page);
            for (unsigned int block_idx = 0; sample_descriptors && (block_idx < database[page_idx].getNumberOfBlocks()); ++block_idx)
            {
                for (unsigned int line_idx = 0; sample_descriptors && (line_idx < database[page_idx][block_idx].getNumberOfLines()); ++line_idx)
                {
                    for (unsigned int symbol_idx = 0; sample_descriptors && (symbol_idx < database[page_idx][block_idx][line_idx].getNumberOfSymbols()); ++symbol_idx, ++current_idx)
                    {
                        const srv::SymbolLocalization &current = database[page_idx][block_idx][line_idx][symbol_idx];
                        srv::VectorDense<srv::DescriptorLocation> locations;
                        srv::Image<unsigned char> image_snippet_descriptors;
                        unsigned int current_number_of_valid_descriptors;
                        int x0, y0, x1, y1;
                        
                        logger.log("Processing symbol %d of %d...\r", current_idx + 1, number_of_snippets);
                        // Crop the current symbol/word snippet.
                        x0 = (int)current.getX() - (int)current.getWidth() / 2 - maximum_descriptor_size / 2;
                        y0 = (int)current.getY() - (int)current.getHeight() / 2 - maximum_descriptor_size / 2;
                        x1 = x0 + (int)current.getWidth() + maximum_descriptor_size;
                        y1 = y0 + (int)current.getHeight() + maximum_descriptor_size;
                        x0 = std::max(0, x0);
                        y0 = std::max(0, y0);
                        x1 = std::min((int)image_page.getWidth(), x1);
                        y1 = std::min((int)image_page.getHeight(), y1);
                        image_snippet_descriptors = image_page.subimage(x0, y0, x1 - x0, y1 - y0);
                        if ((x1 - x0 <= (int)maximum_descriptor_size) || (y1 - y0 <= (int)maximum_descriptor_size))
                        {
                            logger.log("Word snippet too small (%dx%d)", x1 - x0, y1 - y0);
                            continue;
                        }
                        
                        std::get<0>(snippet_descriptors[current_idx]) = 0;
                        visual_words.extractDescriptors(image_snippet_descriptors, locations, std::get<0>(snippet_descriptors[current_idx]), number_of_threads);
                        std::get<1>(snippet_descriptors[current_idx]) = locations.size();
                        current_number_of_valid_descriptors = 0;
                        for (unsigned int l = 0; l < locations.size(); ++l)
                        {
                            const unsigned int side = visual_words.getDenseDescriptors()->getScalesInformation(locations[l].getScale()).getRegionSize();
                            unsigned int x, y;
                            
                            x = locations[l].getX() + side / 2;
                            y = locations[l].getY() + side / 2;
                            if (std::get<0>(snippet_descriptors[current_idx])[l].size() > 0)
                            {
                                if ((x < maximum_descriptor_size / 2) || (y < maximum_descriptor_size / 2) || (x >= current.getWidth() + maximum_descriptor_size / 2) || (y >= current.getHeight() + maximum_descriptor_size / 2))
                                    std::get<0>(snippet_descriptors[current_idx])[l].set(0);
                                else ++current_number_of_valid_descriptors;
                            }
                        }
                        number_of_valid_descriptors += current_number_of_valid_descriptors;
                        logger.log("Processing symbol %d of %d: extracted=%d; valid=%d; total=%d", current_idx + 1, number_of_snippets, locations.size(), current_number_of_valid_descriptors, number_of_valid_descriptors);
                        
                        if ((sampling_method == 0) && (number_of_valid_descriptors > maximum_train_samples))
                            sample_descriptors = false;
                    }
                }
            }
        }
        
        // Gather all the training descriptors in a single array.
        descriptor_ptrs = new srv::ConstantSubVectorDense<unsigned char> * [number_of_valid_descriptors];
        for (unsigned int i = 0, j = 0; i < number_of_snippets; ++i)
            for (unsigned int k = 0; k < std::get<1>(snippet_descriptors[i]); ++k)
                if (std::get<0>(snippet_descriptors[i])[k].size() != 0)
                    descriptor_ptrs[j++] = new srv::ConstantSubVectorDense<unsigned char>(std::get<0>(snippet_descriptors[i])[k]);
        
        if ((maximum_train_samples != 0) && (number_of_valid_descriptors > maximum_train_samples))
        {
            for (unsigned int i = 0; i < maximum_train_samples; ++i)
                srv::srvSwap(descriptor_ptrs[i], descriptor_ptrs[i + rand() % (number_of_valid_descriptors - i)]);
            for (unsigned int i = maximum_train_samples; i < number_of_valid_descriptors; ++i)
                delete descriptor_ptrs[i];
            number_of_valid_descriptors = maximum_train_samples;
        }
        
        // Create the codebook.
        visual_words.getCodebook()->generate(descriptor_ptrs, 0, number_of_valid_descriptors, number_of_threads, &logger); 
        
        // Free allocated memory.
        for (unsigned int i = 0; i < number_of_snippets; ++i)
            delete [] std::get<0>(snippet_descriptors[i]);
        for (unsigned int i = 0; i < number_of_valid_descriptors; ++i)
            delete descriptor_ptrs[i];
        delete [] descriptor_ptrs;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while creating the codebook from the database images:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save visual words object ]-----------------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Saving codebook to disk.");
        saveObject(filename_codebook, visual_words);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while storing the visual words object:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

