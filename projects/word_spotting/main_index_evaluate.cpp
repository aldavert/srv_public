// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"
#include "common_functions.hpp"

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                             11-June-2018\n"
                                   "=================================================================================\n"
                                   " Generates an XML file with the results obtained by the visual index.\n\n";
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int number_of_threads;
    char filename_output[4096];
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./index_evaluate", initial_message);
    srv::VectorDense<srv::ExperimentInformation> experiments;
    srv::DocumentVisualIndex<DEF_TVALUE, DEF_TINDEX> visual_index;
    srv::StreamLogger logger(&std::cout);
    bool generate_ranked_list;
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        char filename_index[4096], filename_experiments[4096];
        unsigned int generate_ranked_list_index;
        
        parameters.addParameter(new srv::ParameterCharArray("filename_index", "file with the visual index of the evaluated dataset/document.", filename_index));
        parameters.addParameter(new srv::ParameterCharArray("filename_evaluate", "file information of experiments used to test the performance of the visual index.", filename_experiments));
        parameters.addParameter(new srv::ParameterCharArray("filename_output", "file where the results are stored into.", filename_output));
        parameters.addParameter(new srv::ParameterUInt("--rank", "index", "enables/disables the generation of the ranked list of each snippet. Possible values are:&0&Disabled.&1&Enabled.", 1, true, 0, true, 1, &generate_ranked_list_index));
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently evaluate the index.", 1, true, 1, false, 0, &number_of_threads));
        generate_ranked_list = false;
        parameters.parse(argv, argc);
        generate_ranked_list = generate_ranked_list_index == 1;
        
        // Load the experiments.
        srv::loadExperiments(filename_experiments, experiments);
        if (experiments.size() == 0)
            throw srv::Exception("No experiments have been loaded from '%s'...", filename_experiments);
        // Load the visual index.
        srv::compressionPolicy(srv::ZLibCompression);
        std::istream * file = srv::iZipStream(filename_index);
        if (file == 0)
            throw srv::Exception("File '%s' cannot be loaded.", filename_index);
        srv::XmlParser parser(file);
        if (parser.isTagIdentifier("DocumentVisualIndex"))
            visual_index.convertFromXML(parser);
        else throw srv::Exception("XML file '%s' does not contain a document visual index object.", filename_index);
        
        delete file;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Evaluate the visual index ]----------------------------------------------------------------------------------------------------------------
    try
    {
        const srv::INDEX_DISTANCE_IDENTIFIER test_distances[] = { srv::DOCUMENT_INDEX_DISTANCE_EUCLIDEAN, srv::DOCUMENT_INDEX_DISTANCE_MANHATTAN };
        srv::VectorDense<srv::VectorDense<srv::VectorDense<std::tuple<double, unsigned int> > > > scores;
        srv::VectorDense<std::tuple<float, unsigned int> > distances_rank;
        srv::VectorDense<float> * distances_thread;
        std::ofstream file(filename_output);
        srv::XmlParser parser(&file);
        
        if (!file.is_open()) throw srv::Exception("Cannot open file '%s' to store the experiments information.", filename_output);
        if (file.rdstate() != std::ios_base::goodbit) throw srv::Exception("An error flag has been raised while creating '%s'.", filename_output);
        if (generate_ranked_list)
        {
            distances_thread = new srv::VectorDense<float>[number_of_threads];
            distances_rank.set(visual_index.getNumberOfSignatures());
            for (unsigned int t = 0; t < number_of_threads; ++t)
                distances_thread[t].set(visual_index.getNumberOfSignatures());
        }
        else distances_thread = 0;
        
        for (unsigned int distance_idx = 0; distance_idx < 2; ++distance_idx)
        {
            char distance_name[64];
            
            sprintf(distance_name, "%s", ((test_distances[distance_idx] == srv::DOCUMENT_INDEX_DISTANCE_EUCLIDEAN)?("EUCLIDEAN"):("MANHATTAN")));
            logger.log("Index evaluation using %s distance:", distance_name);
            parser.openTag("results");
            parser.setAttribute("distance", distance_name);
            parser.addChildren();
            if (generate_ranked_list)
            {
                logger.log("Computing the ranked list of each snippet:");
                parser.openTag("rank");
                parser.addChildren();
                for (unsigned int query_idx = 0; query_idx < visual_index.getNumberOfSignatures(); ++query_idx)
                {
                    unsigned int page_number, block_number, line_number, position_number;
                    
                    logger.log("Processing snippet %d/%d            \r", query_idx + 1, visual_index.getNumberOfSignatures());
                    visual_index.computeRank(query_idx, distances_thread, test_distances[distance_idx], distances_rank, number_of_threads);
                    
                    srv::retrieveSnippetLocation(visual_index.getSnippetIdentifier(query_idx), page_number, block_number, line_number, position_number);
                    parser.openTag("query");
                    parser.setAttribute("page", page_number);
                    parser.setAttribute("block", block_number);
                    parser.setAttribute("line", line_number);
                    parser.setAttribute("position", position_number);
                    parser.addChildren();
                    for (unsigned int rank_idx = 0; rank_idx < distances_rank.size(); ++rank_idx)
                    {
                        srv::retrieveSnippetLocation(visual_index.getSnippetIdentifier(std::get<1>(distances_rank[rank_idx])), page_number, block_number, line_number, position_number);
                        parser.openTag("result");
                        parser.setAttribute("page", page_number);
                        parser.setAttribute("block", block_number);
                        parser.setAttribute("line", line_number);
                        parser.setAttribute("position", position_number);
                        parser.setAttribute("rank", rank_idx);
                        parser.setAttribute("distance", std::get<0>(distances_rank[rank_idx]));
                        parser.closeTag();
                    }
                    parser.closeTag();
                }
                logger.log("Done                                  ");
                parser.closeTag();
            }
            visual_index.evaluate(experiments.getData(), experiments.size(), test_distances[distance_idx], scores, number_of_threads, &logger);
            for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
            {
                for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                {
                    logger.log("Experiment: %s  [%s]", experiments[eidx].getName(), experiments[eidx].getScore(sidx)->getName());
                    for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                        logger.log("Trial %s: %f [%d queries]", experiments[eidx][tidx].getName(), std::get<0>(scores[eidx][tidx][sidx]), std::get<1>(scores[eidx][tidx][sidx]));
                }
            }
            for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
            {
                parser.openTag("experiment");
                parser.setAttribute("name", experiments[eidx].getName());
                parser.addChildren();
                for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                {
                    parser.openTag("trial");
                    parser.setAttribute("name", experiments[eidx][tidx].getName());
                    parser.addChildren();
                    for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                    {
                        parser.openTag("score");
                        parser.setAttribute("value", std::get<0>(scores[eidx][tidx][sidx]));
                        parser.setAttribute("queries", std::get<1>(scores[eidx][tidx][sidx]));
                        parser.setAttribute("type", experiments[eidx].getScore(sidx)->getAbbreviation());
                        parser.closeTag();
                    }
                    parser.closeTag();
                }
                parser.closeTag();
            }
            parser.closeTag();
        }
        
        if (distances_thread != 0) delete [] distances_thread;
        if (file.rdstate() != std::ios_base::goodbit) throw srv::Exception("An error flag has been raised while saving the object into '%s'.", filename_output);
        file.close();
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the histograms:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    
    return EXIT_SUCCESS;
}
