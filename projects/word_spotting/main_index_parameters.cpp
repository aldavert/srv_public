// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"
#include "common_functions.hpp"

INITIALIZE_CODEWORD_WEIGHT_FACTORIES(double, double, double, unsigned int)
INITIALIZE_CODEWORD_ENCODE_FACTORIES(double, double, double, unsigned int)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                             15-June-2018\n"
                                   "=================================================================================\n"
                                   " Stores in a XML object the histogram parameters of a visual signature.\n\n";
    
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char filename_parameters[4096];
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./index_parameters", initial_message);
    srv::DocumentVisualIndexGenerator histogram_generator;
    
    try
    {
        unsigned int codeword_weight_index, codeword_encode_index, normalization_spatial_index, normalization_global_index, number_of_neighbors, pooling_method_index, negative_values_index;
        double codeword_weight_sigma, codeword_super_weight, normalization_spatial_order, normalization_global_order, power_factor;
        char partition_x_text[4096], partition_y_text[4096];
        srv::CodewordWeightBase<double, double, double, unsigned int> * codeword_weight;
        srv::CodewordEncodeBase<double, double, double, unsigned int> * codeword_encode;
        srv::VectorDense<std::pair<unsigned int, unsigned int> > partitions_values;
        srv::VectorNorm normalization_spatial, normalization_global;
        std::list<int> partition_x_list, partition_y_list;
        srv::POOLING_METHOD_IDENTIFIER pooling_identifier;
        srv::NEGATIVE_METHOD_IDENTIFIER negative_identifier;
        
        // Mandatory parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_parameters", "file where the selected parameters information is stored into.", filename_parameters));
        parameters.addParameter(new srv::ParameterUInt("weight", "method used to weight the contribution of each codeword while encoding a descriptor. Possible values are:"
                                                                 "&0&Uniform"
                                                                 "&1&Distance"
                                                                 "&2&LLC"
                                                                 "&3&Gaussian Kernel"
                                                                 "&4&Normalized Gaussian Kernel"
                                                                 "&5&Fischer vector", true, 0, true, 5, &codeword_weight_index));
        parameters.addParameter(new srv::ParameterUInt("encode", "method used to encode the descriptor with the selected codewords. Possible values are:"
                                                                 "&0&Centroid"
                                                                 "&1&First derivative (VLAD)"
                                                                 "&2&Super vector (centroid + VLAD)"
                                                                 "&3&Fischer vector (VLAD + Diagonal 2on Derivative)"
                                                                 "&4&Tensor (full 2on derivative)", true, 0, true, 4, &codeword_encode_index));
        parameters.addParameter(new srv::ParameterUInt("neighbors", "number of codewords used to encode a descriptor.", true, 1, false, 0, &number_of_neighbors));
        // Codeword parameters.
        parameters.addParameter(new srv::ParameterDouble("--sigma", "value", "sigma of the Gaussian kernel.", 2.0f, true, 0.0f, false, 0.0f, &codeword_weight_sigma));
        parameters.addParameter(new srv::ParameterDouble("--super", "weight", "multiplicative weight applied to the centroid bin of the super vector representation.", 1.0f, true, 0.0f, false, 0.0f, &codeword_super_weight));
        parameters.addParameter(new srv::ParameterUInt("--pool", "index", "method used to pool the values of the codewords. Possible values are:&0&Additive.&1&Maximum value.&2&Maximum absolute value.", 0, true, 0, true, 2, &pooling_method_index));
        parameters.addParameter(new srv::ParameterUInt("--polarize", "index", "method used to deal with negative codeword contributions. Possible values are:&0&Nothing (i.e. no different than positive contributions).&1&Absolute value.&2&Separate positive and negative contributions.", 0, true, 0, true, 2, &negative_values_index));
        // Spatial pyramid.
        parameters.addParameter(new srv::ParameterCharArray("--pyramid-x", "list", "list of horizontal spatial divisions (e.g. '--pyramid-x 3:9' for 3 partitions in the 1st level and 9 in the 2on).", partition_x_text));
        parameters.addParameter(new srv::ParameterCharArray("--pyramid-y", "list", "list of vectical spatial divisions (e.g. '--pyramid 1:2' for a single partition in the 1st level and 2 in the 2on).", partition_y_text));
        // Normalization parameters.
        parameters.addParameter(new srv::ParameterUInt("--norm-spatial", "index", "normalization method independently applied to each spatial level. Possible values are:"
                                                                                  "&0&Manhattan norm"
                                                                                  "&1&Euclidean norm"
                                                                                  "&2&Maximum norm"
                                                                                  "&3&p-norm", 0, true, 0, true, 3, &normalization_spatial_index));
        parameters.addParameter(new srv::ParameterDouble("--norm-spatial-power", "factor", "power factor of the spatial p-norm", 1.0, true, 0.0, false, 0.0, &normalization_spatial_order));
        parameters.addParameter(new srv::ParameterUInt("--norm-global", "index", "normalization method applied to the whole signature. Possible values are:"
                                                                                  "&0&Manhattan norm"
                                                                                  "&1&Euclidean norm"
                                                                                  "&2&Maximum norm"
                                                                                  "&3&p-norm", 1, true, 0, true, 3, &normalization_global_index));
        parameters.addParameter(new srv::ParameterDouble("--norm-global-power", "factor", "power factor of the global p-norm", 1.0, true, 0.0, false, 0.0, &normalization_global_order));
        parameters.addParameter(new srv::ParameterDouble("--pow", "factor", "normalization power factor applied to each bin of the histogram of visual words.", 1.0f, true, 0.0f, true, 1.0f, &power_factor));
        
        // Parser the user parameters.
        partition_x_text[0] = partition_y_text[0] = '\0';
        parameters.parse(argv, argc);
        
        if      (codeword_weight_index == 0) codeword_weight = new srv::CodewordWeightUniform<double, double, double, unsigned int>();
        else if (codeword_weight_index == 1) codeword_weight = new srv::CodewordWeightDistance<double, double, double, unsigned int>();
        else if (codeword_weight_index == 2) codeword_weight = new srv::CodewordWeightLLC<double, double, double, unsigned int>();
        else if (codeword_weight_index == 3) codeword_weight = new srv::CodewordWeightGaussian<double, double, double, unsigned int>(codeword_weight_sigma);
        else if (codeword_weight_index == 4) codeword_weight = new srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int>(codeword_weight_sigma);
        else if (codeword_weight_index == 5) codeword_weight = new srv::CodewordWeightFisher<double, double, double, unsigned int>();
        else throw srv::Exception("The selected codeword weight (index '%d') is not implemented yet.", codeword_weight_index);
        
        if      (codeword_encode_index == 0) codeword_encode = new srv::CodewordEncodeCentroid<double, double, double, unsigned int>();
        else if (codeword_encode_index == 1) codeword_encode = new srv::CodewordEncodeFirstDerivative<double, double, double, unsigned int>();
        else if (codeword_encode_index == 2) codeword_encode = new srv::CodewordEncodeSuperVector<double, double, double, unsigned int>(codeword_super_weight);
        else if (codeword_encode_index == 3) codeword_encode = new srv::CodewordEncodeFisher<double, double, double, unsigned int>();
        else if (codeword_encode_index == 4) codeword_encode = new srv::CodewordEncodeTensor<double, double, double, unsigned int>();
        else throw srv::Exception("The selected codeword encode (index '%d') is not implemented yet.", codeword_encode_index);
        
        if      (pooling_method_index == 0) pooling_identifier = srv::POOLING_ADDITIVE;
        else if (pooling_method_index == 1) pooling_identifier = srv::POOLING_MAXIMUM;
        else if (pooling_method_index == 2) pooling_identifier = srv::POOLING_ABSOLUTE_MAXIMUM;
        else throw srv::Exception("The method selected to pool the codewords (index '%d') together is not implemented yet.", pooling_method_index);
        
        if      (negative_values_index == 0) negative_identifier = srv::NEGATIVE_NOTHING;
        else if (negative_values_index == 1) negative_identifier = srv::NEGATIVE_ABSOLUTE;
        else if (negative_values_index == 2) negative_identifier = srv::NEGATIVE_SEPARATE;
        else throw srv::Exception("The method used to treat negative codeword contributions (index '%d') is not implemented yet.", negative_values_index);
        
        
        if (partition_x_text[0] != '\0')
            srv::text2list<int>(partition_x_text, 1, std::numeric_limits<int>::max(), partition_x_list);
        if (partition_y_text[0] != '\0')
            srv::text2list<int>(partition_y_text, 1, std::numeric_limits<int>::max(), partition_y_list);
        if (partition_x_list.size() == 0) partition_x_list.push_back(1);
        if (partition_y_list.size() == 0) partition_y_list.push_back(1);
        
        if ((partition_x_list.size() != 1) && (partition_y_list.size() != 1) && (partition_x_list.size() != partition_y_list.size()))
            throw srv::Exception("The size of the partition lists is incompatible (they should have the same amount of elements or one of the must have a single element).");
        partitions_values.set((unsigned int)std::max(partition_x_list.size(), partition_y_list.size()));
        if (partition_x_list.size() == 1)
            for (unsigned int i = 0; i < partitions_values.size(); ++i) std::get<0>(partitions_values[i]) = partition_x_list.front();
        else
        {
            unsigned int idx = 0;
            for (const auto &v : partition_x_list)
                std::get<0>(partitions_values[idx++]) = v;
        }
        if (partition_y_list.size() == 1)
            for (unsigned int i = 0; i < partitions_values.size(); ++i) std::get<1>(partitions_values[i]) = partition_y_list.front();
        else
        {
            unsigned int idx = 0;
            for (const auto &v : partition_y_list)
                std::get<1>(partitions_values[idx++]) = v;
        }
        if      (normalization_spatial_index == 0) normalization_spatial.set(srv::MANHATTAN_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 1) normalization_spatial.set(srv::EUCLIDEAN_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 2) normalization_spatial.set(srv::MAXIMUM_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 3) normalization_spatial.set(srv::P_NORM, normalization_spatial_order);
        else throw srv::Exception("The selected spatial norm (with index '%d') has not been implemented yet.", normalization_spatial_index);
        if      (normalization_global_index == 0) normalization_global.set(srv::MANHATTAN_NORM, normalization_global_order);
        else if (normalization_global_index == 1) normalization_global.set(srv::EUCLIDEAN_NORM, normalization_global_order);
        else if (normalization_global_index == 2) normalization_global.set(srv::MAXIMUM_NORM, normalization_global_order);
        else if (normalization_global_index == 3) normalization_global.set(srv::P_NORM, normalization_global_order);
        else throw srv::Exception("The selected global norm (with index '%d') has not been implemented yet.", normalization_global_index);
        
        histogram_generator.setEncode(codeword_weight, codeword_encode, number_of_neighbors, pooling_identifier, negative_identifier);
        histogram_generator.setSpatialPartitions(partitions_values);
        histogram_generator.setNormalization(normalization_spatial, power_factor, normalization_global);
        
        // Free allocated memory.
        delete codeword_weight;
        delete codeword_encode;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save index parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        std::ofstream file(filename_parameters);
        if (!file.is_open())
            throw srv::Exception("File '%s' cannot be opened.", filename_parameters);
        if (file.rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while creating '%s'.", filename_parameters);
        srv::XmlParser parser(&file);
        histogram_generator.convertToXML(parser);
        if (file.rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while saving the object into '%s'.", filename_parameters);
        file.close();
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while saving the visual signature parameters:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }

#if 1
    // -[ DEBUG: Reload the object and check that its the same ]-------------------------------------------------------------------------------------
    try
    {
        const double difference_threshold = 1e-7;
        srv::DocumentVisualIndexGenerator histogram_generator_copy;
        std::ifstream file(filename_parameters);
        if (!file.is_open())
            throw srv::Exception("[DEBUG] File '%s' cannot be opened.", filename_parameters);
        srv::XmlParser parser(&file);
        
        // Load the object.
        if (!parser.isTagIdentifier("DocumentVisualIndexParameters"))
            throw srv::Exception("[DEBUG] The object has an unexpected tag.");
        histogram_generator_copy.convertFromXML(parser);
        file.close();
        
        // Check that both the original and the XML objects have the same parameters.
        /***
        inline const CodewordWeightBase<double, double, double, unsigned int> * getCodewordWeight(void) const { return m_codeword_weight; }
        inline const CodewordEncodeBase<double, double, double, unsigned int> * getCodewordEncode(void) const { return m_codeword_encode; }
         */
        if (histogram_generator.getNumberOfNeighbors() != histogram_generator_copy.getNumberOfNeighbors())
            throw srv::Exception("The number of neighbors differs (%d != %d).", histogram_generator.getNumberOfNeighbors(), histogram_generator_copy.getNumberOfNeighbors());
        if (histogram_generator.getPoolingIdentifier() != histogram_generator_copy.getPoolingIdentifier())
            throw srv::Exception("The pooling method differs (%d != %d).", (int)histogram_generator.getPoolingIdentifier(), (int)histogram_generator_copy.getPoolingIdentifier());
        if (histogram_generator.getNegativeIdentifier() != histogram_generator_copy.getNegativeIdentifier())
            throw srv::Exception("The negative method differs (%d != %d).", histogram_generator.getNegativeIdentifier(), histogram_generator_copy.getNegativeIdentifier());
        if (histogram_generator.getNumberOfSpatialLevels() != histogram_generator_copy.getNumberOfSpatialLevels())
            throw srv::Exception("The number of spatial levels differs (%d != %d).", histogram_generator.getNumberOfSpatialLevels(), histogram_generator_copy.getNumberOfSpatialLevels());
        if (histogram_generator.getNumberOfSpatialBins() != histogram_generator_copy.getNumberOfSpatialBins())
            throw srv::Exception("The total number of spatial bins differs (%d != %d).", histogram_generator.getNumberOfSpatialBins(), histogram_generator_copy.getNumberOfSpatialBins());
        for (unsigned int i = 0; i < histogram_generator.getNumberOfSpatialLevels(); ++i)
            if (histogram_generator.getSpatialPartition(i) != histogram_generator_copy.getSpatialPartition(i))
                throw srv::Exception("The %d-th spatial level has a different number of partitions (%dx%d != %dx%d).", std::get<0>(histogram_generator.getSpatialPartition(i)), std::get<1>(histogram_generator.getSpatialPartition(i)), std::get<0>(histogram_generator_copy.getSpatialPartition(i)), std::get<1>(histogram_generator_copy.getSpatialPartition(i)));
        if (std::abs(histogram_generator.getPowerFactor() - histogram_generator_copy.getPowerFactor()) > difference_threshold)
            throw srv::Exception("The power factor differs (%f != %f).", histogram_generator.getPowerFactor() != histogram_generator_copy.getPowerFactor());
        if (histogram_generator.getNormalizationSpatial().getIdentifier() != histogram_generator_copy.getNormalizationSpatial().getIdentifier())
            throw srv::Exception("The spatial normalization differs (type '%d' != type '%d').", histogram_generator.getNormalizationSpatial().getIdentifier(), histogram_generator_copy.getNormalizationSpatial().getIdentifier());
        if ((histogram_generator.getNormalizationSpatial().getIdentifier() == srv::P_NORM)
            && (std::abs(histogram_generator.getNormalizationSpatial().getOrder() - histogram_generator_copy.getNormalizationSpatial().getOrder()) > difference_threshold))
            throw srv::Exception("The order of the spatial Lp-norm differs (%f != %f).", histogram_generator.getNormalizationSpatial().getOrder(), histogram_generator_copy.getNormalizationSpatial().getOrder());
        if (histogram_generator.getNormalizationGlobal().getIdentifier() != histogram_generator_copy.getNormalizationGlobal().getIdentifier())
            throw srv::Exception("The global normalization differs (type '%d' != type '%d').", histogram_generator.getNormalizationGlobal().getIdentifier(), histogram_generator_copy.getNormalizationGlobal().getIdentifier());
        if ((histogram_generator.getNormalizationGlobal().getIdentifier() == srv::P_NORM)
            && (std::abs(histogram_generator.getNormalizationGlobal().getOrder() - histogram_generator_copy.getNormalizationGlobal().getOrder()) > difference_threshold))
            throw srv::Exception("The order of the global Lp-norm differs (%f != %f).", histogram_generator.getNormalizationGlobal().getOrder(), histogram_generator_copy.getNormalizationGlobal().getOrder());
        // Codeword weight ........................................................................
        if (histogram_generator.getCodewordWeight()->getIdentifier() != histogram_generator_copy.getCodewordWeight()->getIdentifier())
            throw srv::Exception("The codeword weight object differs (type '%d' != type '%d').", histogram_generator.getCodewordWeight()->getIdentifier(), histogram_generator_copy.getCodewordWeight()->getIdentifier());
        if ((histogram_generator.getCodewordWeight()->getIdentifier() == srv::CODEWORD_WEIGHT_GAUSSIAN_KERNEL) &&
                (std::abs(((srv::CodewordWeightGaussian<double, double, double, unsigned int> *)histogram_generator.getCodewordWeight())->getSigma() - ((srv::CodewordWeightGaussian<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordWeight())->getSigma()) > difference_threshold))
            throw srv::Exception("The sigma of the Gaussian kernel codeword weight differs (%f != %f).", ((srv::CodewordWeightGaussian<double, double, double, unsigned int> *)histogram_generator.getCodewordWeight())->getSigma(), ((srv::CodewordWeightGaussian<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordWeight())->getSigma());
        if ((histogram_generator.getCodewordWeight()->getIdentifier() == srv::CODEWORD_WEIGHT_NORMALIZED_GAUSSIAN_KERNEL) &&
                (std::abs(((srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int> *)histogram_generator.getCodewordWeight())->getSigma() - ((srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordWeight())->getSigma()) > difference_threshold))
            throw srv::Exception("The sigma of the normalized Gaussian kernel codeword weight differs (%f != %f).", ((srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int> *)histogram_generator.getCodewordWeight())->getSigma(), ((srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordWeight())->getSigma());
        // Codeword encode .........................................................................
        if (histogram_generator.getCodewordEncode()->getIdentifier() != histogram_generator_copy.getCodewordEncode()->getIdentifier())
            throw srv::Exception("The codeword encode object differs (type '%d' != type '%d').", histogram_generator.getCodewordEncode()->getIdentifier(), histogram_generator_copy.getCodewordEncode()->getIdentifier());
        if ((histogram_generator.getCodewordEncode()->getIdentifier() == srv::CODEWORD_ENCODE_SUPER_VECTOR) &&
                (std::abs(((srv::CodewordEncodeSuperVector<double, double, double, unsigned int> *)histogram_generator.getCodewordEncode())->getWeight() - ((srv::CodewordEncodeSuperVector<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordEncode())->getWeight()) > difference_threshold))
            throw srv::Exception("The weight of the codeword supervector encode differs (%f != %d).", ((srv::CodewordEncodeSuperVector<double, double, double, unsigned int> *)histogram_generator.getCodewordEncode())->getWeight(), ((srv::CodewordEncodeSuperVector<double, double, double, unsigned int> *)histogram_generator_copy.getCodewordEncode())->getWeight());
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while testing the signature parameters XML object:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
#endif
    
    return EXIT_SUCCESS;
}
