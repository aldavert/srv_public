// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include "common_functions.hpp"

namespace srv
{
    
    //                   +--------------------------------------+
    //                   | AUXILIARY FUNCTIONS                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    unsigned long calculateSnippetIdentifier(unsigned int page_number, unsigned int block_number, unsigned int line_number, unsigned int position_number)
    {
        return (((unsigned long)(    page_number % std::numeric_limits<unsigned short>::max()) << 48) |
                ((unsigned long)(   block_number % std::numeric_limits<unsigned short>::max()) << 32) |
                ((unsigned long)(    line_number % std::numeric_limits<unsigned short>::max()) << 16) |
                ((unsigned long)(position_number % std::numeric_limits<unsigned short>::max())      ));
    }
    
    void retrieveSnippetLocation(unsigned long snippet_identifier, unsigned int &page_number, unsigned int &block_number, unsigned int &line_number, unsigned int &position_number)
    {
        page_number     = (unsigned int)(0xFFFF & (snippet_identifier >> 48));
        block_number    = (unsigned int)(0xFFFF & (snippet_identifier >> 32));
        line_number     = (unsigned int)(0xFFFF & (snippet_identifier >> 16));
        position_number = (unsigned int)(0xFFFF & (snippet_identifier      ));
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | SNIPPET INFORMATION                  |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    SnippetInformation::SnippetInformation(void) :
        m_x(0),
        m_y(0),
        m_descriptor(0),
        m_neighbor_distance(0),
        m_neighbor_index(0),
        m_number_of_descriptors(0),
        m_number_of_distances(0),
        m_number_of_dimensions(0)
    {
    }
    
    SnippetInformation::SnippetInformation(const SnippetInformation &other) :
        m_x((other.m_number_of_descriptors != 0)?new unsigned short[other.m_number_of_descriptors]:0),
        m_y((other.m_number_of_descriptors != 0)?new unsigned short[other.m_number_of_descriptors]:0),
        m_descriptor_values(other.m_descriptor_values),
        m_descriptor((other.m_number_of_descriptors != 0)?new ConstantSubVectorDense<unsigned char>[other.m_number_of_descriptors]:0),
        m_neighbor_distance((other.m_neighbor_distance != 0)?new unsigned int[other.m_number_of_descriptors * m_number_of_distances]:0),
        m_neighbor_index((other.m_neighbor_index != 0)?new unsigned int[other.m_number_of_descriptors * m_number_of_distances]:0),
        m_number_of_descriptors(other.m_number_of_descriptors),
        m_number_of_distances(other.m_number_of_distances),
        m_number_of_dimensions(other.m_number_of_dimensions)
    {
        for (unsigned int i = 0, k = 0, m = 0; i < other.m_number_of_descriptors; ++i, m += other.m_number_of_dimensions)
        {
            m_x[i] = other.m_x[i];
            m_y[i] = other.m_y[i];
            m_descriptor[i].set(&m_descriptor_values[m], other.m_number_of_dimensions);
            for (unsigned int j = 0; j < other.m_number_of_distances; ++j, ++k)
            {
                m_neighbor_distance[k] = other.m_neighbor_distance[k];
                m_neighbor_index[k] = other.m_neighbor_index[k];
            }
        }
    }
    
    SnippetInformation::~SnippetInformation(void)
    {
        if (m_x != 0) delete [] m_x;
        if (m_y != 0) delete [] m_y;
        if (m_descriptor != 0) delete [] m_descriptor;
        if (m_neighbor_distance != 0) delete [] m_neighbor_distance;
        if (m_neighbor_index != 0) delete [] m_neighbor_index;
    }
    
    SnippetInformation& SnippetInformation::operator=(const SnippetInformation &other)
    {
        if (this != &other)
        {
            // Free allocated memory ..............................................................
            if (m_x != 0) delete [] m_x;
            if (m_y != 0) delete [] m_y;
            if (m_descriptor != 0) delete [] m_descriptor;
            if (m_neighbor_distance != 0) delete [] m_neighbor_distance;
            if (m_neighbor_index != 0) delete [] m_neighbor_index;
            
            // Copy information ...................................................................
            if ((other.m_number_of_descriptors != 0) && (other.m_number_of_distances != 0))
            {
                // Copy the descriptor values.
                m_descriptor_values = other.m_descriptor_values;
                // Allocate structures.
                m_x = new unsigned short[other.m_number_of_descriptors];
                m_y = new unsigned short[other.m_number_of_descriptors];
                m_descriptor = new ConstantSubVectorDense<unsigned char>[other.m_number_of_descriptors];
                m_neighbor_distance = new unsigned int[other.m_number_of_descriptors * other.m_number_of_distances];
                m_neighbor_index = new unsigned int[other.m_number_of_descriptors * other.m_number_of_distances];
                // Copy the structure values.
                for (unsigned int i = 0, k = 0, m = 0; i < other.m_number_of_descriptors; ++i, m += other.m_number_of_dimensions)
                {
                    m_x[i] = other.m_x[i];
                    m_y[i] = other.m_y[i];
                    m_descriptor[i].set(&m_descriptor_values[m], other.m_number_of_dimensions);
                    for (unsigned int j = 0; j < other.m_number_of_distances; ++j, ++k)
                    {
                        m_neighbor_distance[k] = other.m_neighbor_distance[k];
                        m_neighbor_index[k] = other.m_neighbor_index[k];
                    }
                }
            }
            else
            {
                m_x = m_y = 0;
                m_descriptor = 0;
                m_neighbor_distance = m_neighbor_index = 0;
            }
            m_number_of_descriptors = other.m_number_of_descriptors;
            m_number_of_distances = other.m_number_of_distances;
            m_number_of_dimensions = other.m_number_of_dimensions;
        }
        return *this;
    }
    
    void SnippetInformation::convertToXML(XmlParser &parser)
    {
        parser.openTag("SnippetInformation");
        parser.setAttribute("Number_Of_Descriptors", m_number_of_descriptors);
        parser.setAttribute("Number_Of_Distances", m_number_of_distances);
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.setAttribute("Word_Category_ID", m_word_category_id);
        parser.setAttribute("Snippet_Identifier", m_snippet_identifier);
        parser.addChildren();
        saveVector(parser, "PositionX", ConstantSubVectorDense<unsigned short>(m_x, m_number_of_descriptors));
        saveVector(parser, "PositionY", ConstantSubVectorDense<unsigned short>(m_y, m_number_of_descriptors));
        saveVector(parser, "DescriptorValues", m_descriptor_values);
        saveVector(parser, "NeighborDistance", ConstantSubVectorDense<unsigned int>(m_neighbor_distance, m_number_of_descriptors * m_number_of_distances));
        saveVector(parser, "NeighborIndex", ConstantSubVectorDense<unsigned int>(m_neighbor_index, m_number_of_descriptors * m_number_of_distances));
        parser.closeTag();
    }
    
    void SnippetInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("SnippetInformation"))
        {
            bool init_position_x, init_position_y, init_descriptor_values, init_neighbor_distance, init_neighbor_index;
            
            // Free allocated memory.
            if (m_x != 0) delete [] m_x;
            if (m_y != 0) delete [] m_y;
            if (m_descriptor != 0) delete [] m_descriptor;
            if (m_neighbor_distance != 0) delete [] m_neighbor_distance;
            if (m_neighbor_index != 0) delete [] m_neighbor_index;
            
            // Retrieve the object variables.
            m_number_of_descriptors = parser.getAttribute("Number_Of_Descriptors");
            m_number_of_distances = parser.getAttribute("Number_Of_Distances");
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            m_word_category_id = parser.getAttribute("Word_Category_ID");
            m_snippet_identifier = parser.getAttribute("Snippet_Identifier");
            
            // Create structures.
            if ((m_number_of_descriptors > 0) && (m_number_of_distances > 0) && (m_number_of_dimensions > 0))
            {
                m_x = new unsigned short[m_number_of_descriptors];
                m_y = new unsigned short[m_number_of_descriptors];
                m_descriptor_values.set(m_number_of_descriptors * m_number_of_dimensions);
                m_descriptor = new ConstantSubVectorDense<unsigned char>[m_number_of_descriptors];
                m_neighbor_distance = new unsigned int[m_number_of_descriptors * m_number_of_distances];
                m_neighbor_index = new unsigned int[m_number_of_descriptors * m_number_of_distances];
            }
            else
            {
                m_descriptor_values.set(0);
                m_x = m_y = 0;
                m_descriptor = 0;
                m_neighbor_distance = m_neighbor_index = 0;
                m_number_of_descriptors = 0;
                m_number_of_distances = 0;
                m_number_of_dimensions = 0;
                m_word_category_id = 0;
                m_snippet_identifier = 0;
            }
            
            // Retrieve structures data.
            init_position_x = init_position_y = init_descriptor_values = init_neighbor_distance = init_neighbor_index = false;
            while (!(parser.isTagIdentifier("SnippetInformation") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("PositionX"))
                {
                    VectorDense<unsigned short> data;
                    
                    if (init_position_x) throw Exception("Multiple definitions of the X-coordinate array in the XML object.");
                    loadVector(parser, "PositionX", data);
                    if (data.size() != m_number_of_descriptors)
                        throw Exception("The X-coordinate array of the XML object has an unexpected dimensionality (%d != %d).", data.size(), m_number_of_descriptors);
                    for (unsigned int i = 0; i < m_number_of_descriptors; ++i)
                        m_x[i] = data[i];
                    init_position_x = true;
                }
                else if (parser.isTagIdentifier("PositionY"))
                {
                    VectorDense<unsigned short> data;
                    
                    if (init_position_y) throw Exception("Multiple definitions of the Y-coordinate array in the XML object.");
                    loadVector(parser, "PositionY", data);
                    if (data.size() != m_number_of_descriptors)
                        throw Exception("The Y-coordinate array of the XML object has an unexpected dimensionality (%d != %d).", data.size(), m_number_of_descriptors);
                    for (unsigned int i = 0; i < m_number_of_descriptors; ++i)
                        m_y[i] = data[i];
                    init_position_y = true;
                }
                else if (parser.isTagIdentifier("DescriptorValues"))
                {
                    if (init_descriptor_values) throw Exception("Multiple definitions of the descriptor values vector in the XML object.");
                    loadVector(parser, "DescriptorValues", m_descriptor_values);
                    for (unsigned int i = 0, m = 0; i < m_number_of_descriptors; ++i, m += m_number_of_dimensions)
                        m_descriptor[i].set(&m_descriptor_values[m], m_number_of_dimensions);
                    init_descriptor_values = true;
                }
                else if (parser.isTagIdentifier("NeighborDistance"))
                {
                    VectorDense<unsigned int> data;
                    
                    if (init_neighbor_distance) throw Exception("Multiple definitions of the neighbor distances array in the XML object.");
                    loadVector(parser, "NeighborDistance", data);
                    if (data.size() != m_number_of_descriptors * m_number_of_distances)
                        throw Exception("The neighbor distance array of the XML object has an unexpected dimensionality (%d != %d).", data.size(), m_number_of_descriptors * m_number_of_distances);
                    for (unsigned int i = 0; i < m_number_of_descriptors * m_number_of_distances; ++i)
                        m_neighbor_distance[i] = data[i];
                    init_neighbor_distance = true;
                }
                else if (parser.isTagIdentifier("NeighborIndex"))
                {
                    VectorDense<unsigned int> data;
                    
                    if (init_neighbor_index) throw Exception("Multiple definitions of the neighbor index array in the XML object.");
                    loadVector(parser, "NeighborIndex", data);
                    if (data.size() != m_number_of_descriptors * m_number_of_distances)
                        throw Exception("The neighbor index array of the XML object has an unexpected dimensionality (%d != %d).", data.size(), m_number_of_descriptors * m_number_of_distances);
                    for (unsigned int i = 0; i < m_number_of_descriptors * m_number_of_distances; ++i)
                        m_neighbor_index[i] = data[i];
                    init_neighbor_index = true;
                }
                else parser.getNext();
            }
            parser.getNext();
            
            if ((m_number_of_descriptors > 0) && (m_number_of_distances > 0) && (m_number_of_dimensions > 0))
            {
                if (!init_position_x)
                    throw Exception("Initial X-position array has not been defined in the XML object.");
                if (!init_position_y)
                    throw Exception("Initial Y-position array has not been defined in the XML object.");
                if (!init_descriptor_values)
                    throw Exception("Descriptor values vector has not been defined in the XML object.");
                if (!init_neighbor_distance)
                    throw Exception("Neighbor distance array has not been defined in the XML object.");
                if (!init_neighbor_index)
                    throw Exception("Neighbor index array has not been defined in the XML object.");
            }
        }
    }
    
    void SnippetInformation::initialize(unsigned long snippet_identifier, unsigned int word_category_id, unsigned int x0, unsigned int y0, unsigned int x1, unsigned int y1, const VectorDense<DescriptorLocation> &locations, const VectorDense<unsigned char> * descriptors, const DenseVisualWords<short, short, unsigned char, int, unsigned int> &visual_words, unsigned int number_of_neighbors, unsigned int number_of_threads)
    {
        // Free allocated memory.
        if (m_x != 0) delete [] m_x;
        if (m_y != 0) delete [] m_y;
        if (m_descriptor != 0) delete [] m_descriptor;
        if (m_neighbor_distance != 0) delete [] m_neighbor_distance;
        if (m_neighbor_index != 0) delete [] m_neighbor_index;
        
        m_snippet_identifier = snippet_identifier;
        m_word_category_id = word_category_id;
        m_number_of_descriptors = 0;
        m_number_of_dimensions = 0;
        for (unsigned int i = 0; i < locations.size(); ++i)
        {
            const unsigned int side = visual_words.getDenseDescriptors()->getScalesInformation(locations[i].getScale()).getRegionSize();
            unsigned int x, y;
            
            x = locations[i].getX() + side / 2;
            y = locations[i].getY() + side / 2;
            
            if ((x >= x0) && (y >= y0) && (x <= x1) && (y <= y1) && (descriptors[i].size() > 0))
            {
                if (m_number_of_descriptors == 0) m_number_of_dimensions = descriptors[i].size();
                else if (descriptors[i].size() != m_number_of_dimensions)
                    throw Exception("The descriptors have different dimensionality (%d != %d).", descriptors[i].size(), m_number_of_dimensions);
                ++m_number_of_descriptors;
            }
        }
        
        if ((m_number_of_descriptors > 0) && (number_of_neighbors > 0))
        {
            // Constants.
            const CodebookExhaustive<unsigned char, int, int, unsigned int> * codebook = (const CodebookExhaustive<unsigned char, int, int, unsigned int> *)visual_words.getCodebook();
            const unsigned int number_of_codewords = codebook->getNumberOfClusterCenters();
            const unsigned int width = x1 - x0 + 1;
            const unsigned int height = y1 - y0 + 1;
            // Variables.
            VectorDense<std::tuple<int, unsigned int> > codeword_distance;
            VectorDistance distance;
            
            // Initialize variables.
            m_number_of_distances = std::min(number_of_neighbors, number_of_codewords);
            codeword_distance.set(number_of_codewords);
            // Allocate memory.
            m_x = new unsigned short[m_number_of_descriptors];
            m_y = new unsigned short[m_number_of_descriptors];
            m_descriptor_values.set(m_number_of_descriptors * m_number_of_dimensions);
            m_descriptor = new ConstantSubVectorDense<unsigned char>[m_number_of_descriptors];
            m_neighbor_distance = new unsigned int[m_number_of_distances * m_number_of_descriptors];
            m_neighbor_index = new unsigned int[m_number_of_distances * m_number_of_descriptors];
            
            // Assign values.
            m_number_of_descriptors = 0;
            for (unsigned int i = 0, k = 0, m = 0; i < locations.size(); ++i)
            {
                const unsigned int side = visual_words.getDenseDescriptors()->getScalesInformation(locations[i].getScale()).getRegionSize();
                unsigned int x, y;
                
                x = locations[i].getX() + side / 2;
                y = locations[i].getY() + side / 2;
                
                if ((x >= x0) && (y >= y0) && (x <= x1) && (y <= y1) && (descriptors[i].size() > 0))
                {
                    m_x[m_number_of_descriptors] = (unsigned short)((float)std::numeric_limits<short>::max() * ((float)(x - x0) / (float)width));
                    m_y[m_number_of_descriptors] = (unsigned short)((float)std::numeric_limits<short>::max() * ((float)(y - y0) / (float)height));
                    m_descriptor[m_number_of_descriptors].set(&m_descriptor_values[m], m_number_of_dimensions);
                    for (unsigned int l = 0; l < m_number_of_dimensions; ++l, ++m)
                        m_descriptor_values[m] = descriptors[i][l];
                    #pragma omp parallel num_threads(number_of_threads)
                    {
                        for (unsigned int j = omp_get_thread_num(); j < number_of_codewords; j += number_of_threads)
                        {
                            int current_distance;
                            
                            distance.distance(descriptors[i], codebook->getCodeword(j), current_distance);
                            codeword_distance[j] = std::make_tuple(current_distance, j);
                        }
                    }
                    std::sort(&codeword_distance[0], &codeword_distance[number_of_codewords]);
                    for (unsigned int j = 0; j < m_number_of_distances; ++j, ++k)
                    {
                        m_neighbor_distance[k] = std::get<0>(codeword_distance[j]);
                        m_neighbor_index[k] = std::get<1>(codeword_distance[j]);
                    }
                    ++m_number_of_descriptors;
                }
            }
        }
        else
        {
            m_number_of_distances = 0;
            m_x = 0;
            m_y = 0;
            m_descriptor = 0;
            m_neighbor_distance = 0;
            m_neighbor_index = 0;
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INFORMATION          |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    DocumentVisualInformation::DocumentVisualInformation(void) :
        m_snippet_information(0),
        m_number_of_snippets(0),
        m_maximum_number_of_neighbors(0),
        m_number_of_codewords(0),
        m_number_of_dimensions(0),
        m_codewords(0),
        m_codeword_models(0)
    {
    }
    
    DocumentVisualInformation::DocumentVisualInformation(const DocumentVisualInformation &other) :
        m_snippet_information((other.m_number_of_snippets > 0)?new SnippetInformation[other.m_number_of_snippets]:0),
        m_number_of_snippets(other.m_number_of_snippets),
        m_word_text(other.m_word_text),
        m_word_offset(other.m_word_offset),
        m_maximum_number_of_neighbors(other.m_maximum_number_of_neighbors),
        m_number_of_codewords(other.m_number_of_codewords),
        m_number_of_dimensions(other.m_number_of_dimensions),
        m_codewords((other.m_number_of_codewords > 0)?new VectorDense<double>[other.m_number_of_codewords]:0),
        m_codeword_models((other.m_number_of_codewords > 0)?new CodewordGaussianModel<double>[other.m_number_of_codewords]:0)
    {
        for (unsigned int i = 0; i < other.m_number_of_snippets; ++i)
            m_snippet_information[i] = other.m_snippet_information[i];
        for (unsigned int i = 0; i < other.m_number_of_codewords; ++i)
        {
            m_codewords[i] = other.m_codewords[i];
            m_codeword_models[i] = other.m_codeword_models[i];
            m_codeword_models[i].setCentroid(m_codewords[i].getData());
        }
    }
    
    DocumentVisualInformation::~DocumentVisualInformation(void)
    {
        if (m_snippet_information != 0) delete [] m_snippet_information;
        if (m_codewords != 0) delete [] m_codewords;
        if (m_codeword_models != 0) delete [] m_codeword_models;
    }
    
    DocumentVisualInformation& DocumentVisualInformation::operator=(const DocumentVisualInformation &other)
    {
        if (this != &other)
        {
            if (m_snippet_information != 0) delete [] m_snippet_information;
            if (m_codewords != 0) delete [] m_codewords;
            if (m_codeword_models != 0) delete [] m_codeword_models;
            
            if ((other.m_number_of_snippets > 0) && (other.m_number_of_codewords > 0))
            {
                m_snippet_information = new SnippetInformation[other.m_number_of_snippets];
                for (unsigned int i = 0; i < other.m_number_of_snippets; ++i)
                    m_snippet_information[i] = other.m_snippet_information[i];
                m_number_of_snippets = other.m_number_of_snippets;
                m_word_text = other.m_word_text;
                m_word_offset = other.m_word_offset;
                m_maximum_number_of_neighbors = other.m_maximum_number_of_neighbors;
                m_number_of_codewords = other.m_number_of_codewords;
                m_number_of_dimensions = other.m_number_of_dimensions;
                m_codewords = new VectorDense<double>[other.m_number_of_codewords];
                m_codeword_models = new CodewordGaussianModel<double>[other.m_number_of_codewords];
                for (unsigned int i = 0; i < other.m_number_of_codewords; ++i)
                {
                    m_codewords[i] = other.m_codewords[i];
                    m_codeword_models[i] = other.m_codeword_models[i];
                    m_codeword_models[i].setCentroid(m_codewords[i].getData());
                }
            }
            else
            {
                m_snippet_information = 0;
                m_number_of_snippets = 0;
                m_word_text.set(0);
                m_word_offset.set(0);
                m_maximum_number_of_neighbors = 0;
                m_number_of_codewords = 0;
                m_number_of_dimensions = 0;
                m_codewords = 0;
                m_codeword_models = 0;
            }
        }
        return *this;
    }
    
    void DocumentVisualInformation::initialize(const DocumentDatabase &database, const DenseVisualWords<short, short, unsigned char, int, unsigned int> &visual_words, unsigned int number_of_neighbors, unsigned int number_of_threads, BaseLogger * logger)
    {
        unsigned int maximum_descriptor_size, number_of_valid_descriptors, total_text_length;
        const CodebookExhaustive<unsigned char, int, int, unsigned int> * codebook = (const CodebookExhaustive<unsigned char, int, int, unsigned int> *)visual_words.getCodebook();
        VectorDense<double> current_sigma;
        
        // Free allocated memory.
        if (m_snippet_information != 0) delete [] m_snippet_information;
        if (m_codewords != 0) delete [] m_codewords;
        if (m_codeword_models != 0) delete [] m_codeword_models;
        
        m_number_of_snippets = 0;
        for (unsigned int page_idx = 0; page_idx < database.getNumberOfPages(); ++page_idx)
            for (unsigned int block_idx = 0; block_idx < database[page_idx].getNumberOfBlocks(); ++block_idx)
                for (unsigned int line_idx = 0; line_idx < database[page_idx][block_idx].getNumberOfLines(); ++line_idx)
                    m_number_of_snippets += database[page_idx][block_idx][line_idx].getNumberOfSymbols();
        m_number_of_codewords = codebook->getNumberOfClusterCenters();
        m_number_of_dimensions = visual_words.getDenseDescriptors()->getNumberOfBins(3);
        if ((m_number_of_snippets == 0) || (m_number_of_codewords == 0) || (m_number_of_dimensions == 0))
        {
            m_word_text.set(0);
            m_word_offset.set(0);
            m_snippet_information = 0;
            m_number_of_codewords = 0;
            m_number_of_dimensions = 0;
            m_codewords = 0;
            m_codeword_models = 0;
            return;
        }
        
        m_word_offset.set(database.getNumberOfSymbols());
        total_text_length = 0;
        for (unsigned int i = 0; i < database.getNumberOfSymbols(); ++i)
        {
            const char * identifier_text = database.getSymbol(i).getIdentifier();
            unsigned int idx;
            
            for (idx = 0; identifier_text[idx] != '\0'; ++idx);
            total_text_length += (idx + 1);
        }
        m_word_text.set(total_text_length);
        total_text_length = 0;
        for (unsigned int i = 0; i < database.getNumberOfSymbols(); ++i)
        {
            const char * identifier_text = database.getSymbol(i).getIdentifier();
            
            m_word_offset[i] = total_text_length;
            for (unsigned int j = 0; identifier_text[j] != '\0'; ++j, ++total_text_length)
                m_word_text[total_text_length] = identifier_text[j];
            m_word_text[total_text_length++] = '\0';
        }
        m_maximum_number_of_neighbors = number_of_neighbors;
        m_codewords = new VectorDense<double>[m_number_of_codewords];
        m_codeword_models = new CodewordGaussianModel<double>[m_number_of_codewords];
        current_sigma.set(m_number_of_dimensions);
        for (unsigned int i = 0; i < m_number_of_codewords; ++i)
        {
            const VectorDense<int> &current_codeword = codebook->getCodeword(i);
            const CodewordGaussianModel<int> &current_model = codebook->getCodewordModel(i);
            
            m_codewords[i].set(m_number_of_dimensions);
            for (unsigned int j = 0; j < m_number_of_dimensions; ++j)
            {
                m_codewords[i][j] = (double)current_codeword[j] / 255.0;
                current_sigma[j] = (double)current_model.getSigma()[j] / 255.0;
            }
            m_codeword_models[i].set(current_model.getWeight() / 1000.0 /* 1000 multiplicative factor is applied on 'int' type codewords*/,
                                     m_codewords[i].getData(),
                                     current_sigma.getData(),
                                     m_number_of_dimensions);
            //// DEBUG //// std::cout << "Weight: " << m_codeword_models[i].getWeight() << std::endl;
            //// DEBUG //// std::cout << "Centroid: " << ConstantSubVectorDense<float>(m_codeword_models[i].getCentroid(), m_number_of_dimensions) << std::endl;
            //// DEBUG //// std::cout << "Sigma: " << ConstantSubVectorDense<double>(m_codeword_models[i].getSigma(), m_number_of_dimensions) << std::endl;
            //// DEBUG //// std::cout << "Number of dimensions: " << m_codeword_models[i].getNumberOfDimensions() << std::endl;
        
        }
        
        m_snippet_information = new SnippetInformation[m_number_of_snippets];
        maximum_descriptor_size = 0;
        for (unsigned int i = 0; i < visual_words.getDenseDescriptors()->getNumberOfScales(); ++i)
            maximum_descriptor_size = std::max(maximum_descriptor_size, visual_words.getDenseDescriptors()->getScalesInformation(i).getRegionSize());
        if (logger != 0)
            logger->log("The database contains %d snippets.", m_number_of_snippets);
        
        number_of_valid_descriptors = 0;
        for (unsigned int page_idx = 0, current_idx = 0; page_idx < database.getNumberOfPages(); ++page_idx)
        {
            srv::Image<unsigned char> image_page;
            
            if (logger != 0)
                logger->log("Page %d of %d...", page_idx + 1, database.getNumberOfPages());
            database[page_idx].loadImage(image_page);
            for (unsigned int block_idx = 0; block_idx < database[page_idx].getNumberOfBlocks(); ++block_idx)
            {
                for (unsigned int line_idx = 0; line_idx < database[page_idx][block_idx].getNumberOfLines(); ++line_idx)
                {
                    for (unsigned int symbol_idx = 0; symbol_idx < database[page_idx][block_idx][line_idx].getNumberOfSymbols(); ++symbol_idx, ++current_idx)
                    {
                        const srv::SymbolLocalization &current = database[page_idx][block_idx][line_idx][symbol_idx];
                        unsigned int offset_x0, offset_y0, offset_x1, offset_y1;
                        srv::Image<unsigned char> image_snippet_descriptors;
                        srv::VectorDense<srv::DescriptorLocation> locations;
                        srv::VectorDense<unsigned char> * descriptors;
                        int x0, y0, x1, y1;
                        
                        if (logger != 0)
                            logger->log("Processing symbol [%s] %d of %d:...\r", database.getSymbol(current.getSymbolIdentifier()).getIdentifier(), current_idx + 1, m_number_of_snippets);
                        // Crop the current symbol/word snippet (add margins).
                        x0 = (int)current.getX() - (int)current.getWidth() / 2 - maximum_descriptor_size / 2;
                        y0 = (int)current.getY() - (int)current.getHeight() / 2 - maximum_descriptor_size / 2;
                        x1 = x0 + (int)current.getWidth() + maximum_descriptor_size;
                        y1 = y0 + (int)current.getHeight() + maximum_descriptor_size;
                        // Compute the actual snippet offset.
                        offset_x0 = (unsigned int)(maximum_descriptor_size / 2 + ((x0 < 0)?x0:0));
                        offset_y0 = (unsigned int)(maximum_descriptor_size / 2 + ((y0 < 0)?y0:0));
                        offset_x1 = (offset_x0 + (unsigned int)current.getWidth() );
                        offset_y1 = (offset_y0 + (unsigned int)current.getHeight());
                        // Avoid the image borders.
                        x0 = std::max(0, x0);
                        y0 = std::max(0, y0);
                        x1 = std::min((int)image_page.getWidth(), x1);
                        y1 = std::min((int)image_page.getHeight(), y1);
                        image_snippet_descriptors = image_page.subimage(x0, y0, x1 - x0, y1 - y0);
                        if ((x1 - x0 <= (int)maximum_descriptor_size) || (y1 - y0 <= (int)maximum_descriptor_size))
                        {
                            if (logger != 0)
                                logger->log("Word snippet too small (%dx%d)", x1 - x0, y1 - y0);
                            continue;
                        }
                        
                        descriptors = 0;
                        visual_words.extractDescriptors(image_snippet_descriptors, locations, descriptors, number_of_threads);
#if 0
                        {
                            unsigned int debug_page, debug_block, debug_line, debug_pos;
                            unsigned long debug_identifier;
                            
                            debug_identifier = calculateSnippetIdentifier(page_idx, block_idx, line_idx, symbol_idx);
                            srv::retrieveSnippetLocation(debug_identifier, debug_page, debug_block, debug_line, debug_pos);
                            //std::cout << "[DEBUG] INPUT= " << page_idx << " " << block_idx << " " << line_idx << " " << symbol_idx << " ===> " << debug_identifier << "        " << std::endl;
                            //std::cout << "[DEBUG] OUTPUT= " << debug_page << " " << debug_block << " " << debug_line << " " << debug_pos << std::endl;
                            if ((page_idx != debug_page) || (block_idx != debug_block) || (line_idx != debug_line) || (symbol_idx != debug_pos))
                                throw srv::Exception("Error while retrieving the symbol position from the symbol identifier [%d, %d, %d, %d]=>%d=>[%d, %d, %d, %d].", page_idx, block_idx, line_idx, symbol_idx, debug_identifier, debug_page, debug_block, debug_line, debug_pos);
                        }
#endif
                        m_snippet_information[current_idx].initialize(calculateSnippetIdentifier(page_idx, block_idx, line_idx, symbol_idx), current.getSymbolIdentifier(), offset_x0, offset_y0, offset_x1, offset_y1, locations, descriptors, visual_words, number_of_neighbors, number_of_threads);
                        number_of_valid_descriptors += m_snippet_information[current_idx].getNumberOfDescriptors();
                        if (logger != 0)
                            logger->log("Processing symbol [%s] %d of %d: extracted=%d; valid=%d; total=%d", database.getSymbol(current.getSymbolIdentifier()).getIdentifier(), current_idx + 1, m_number_of_snippets, locations.size(), m_snippet_information[current_idx].getNumberOfDescriptors(), number_of_valid_descriptors);
                        
                        delete [] descriptors;
                    }
                }
            }
        }
        
    }
    
    void DocumentVisualInformation::convertToXML(XmlParser &parser)
    {
        parser.openTag("DocumentVisualInformation");
        parser.setAttribute("Number_Of_Snippets", m_number_of_snippets);
        parser.setAttribute("Number_Of_Neighbors", m_maximum_number_of_neighbors);
        parser.setAttribute("Number_Of_Codewords", m_number_of_codewords);
        parser.setAttribute("Number_Of_Dimensions", m_number_of_dimensions);
        parser.addChildren();
        for (unsigned int i = 0; i < m_number_of_snippets; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_snippet_information[i].convertToXML(parser);
        }
        saveVector(parser, "WordText", m_word_text);
        saveVector(parser, "WordOffset", m_word_offset);
        for (unsigned int i = 0; i < m_number_of_codewords; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            saveVector(parser, "Codeword", m_codewords[i]);
        }
        for (unsigned int i = 0; i < m_number_of_codewords; ++i)
        {
            parser.setSucceedingAttribute("ID", i);
            m_codeword_models[i].convertToXML(parser);
        }
        parser.closeTag();
    }
    
    void DocumentVisualInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("DocumentVisualInformation"))
        {
            VectorDense<bool> init_snippet_information, init_codeword_information, init_model_information;
            bool init_word_text, init_word_offset;
            
            // Free.
            if (m_snippet_information != 0) delete [] m_snippet_information;
            if (m_codewords != 0) delete [] m_codewords;
            if (m_codeword_models != 0) delete [] m_codeword_models;
            
            // Load attributes.
            m_number_of_snippets = parser.getAttribute("Number_Of_Snippets");
            m_maximum_number_of_neighbors = parser.getAttribute("Number_Of_Neighbors");
            m_number_of_codewords = parser.getAttribute("Number_Of_Codewords");
            m_number_of_dimensions = parser.getAttribute("Number_Of_Dimensions");
            
            // Initialize structures.
            if ((m_number_of_snippets == 0) && (m_number_of_codewords != 0))
                throw Exception("The document visual information in the XML object contains codebook information without any snippet visual information.");
            if ((m_number_of_snippets != 0) && (m_number_of_codewords == 0))
                throw Exception("The document visual information in the XML object contains snippet visual information without any codebook information.");
            if (m_number_of_snippets > 0)
            {
                m_snippet_information = new SnippetInformation[m_number_of_snippets];
                init_snippet_information.set(m_number_of_snippets, false);
            }
            if (m_number_of_codewords > 0)
            {
                m_codewords = new VectorDense<double>[m_number_of_codewords];
                m_codeword_models = new CodewordGaussianModel<double>[m_number_of_codewords];
                init_codeword_information.set(m_number_of_codewords, false);
                init_model_information.set(m_number_of_codewords, false);
            }
            init_word_text = init_word_offset = false;
            
            while (!(parser.isTagIdentifier("DocumentVisualInformation") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("SnippetInformation"))
                {
                    unsigned int position;
                    
                    position = parser.getAttribute("ID");
                    if (position >= m_number_of_snippets)
                        throw Exception("The snippet information of the XML object is out of range (%d >= %d).", position, m_number_of_snippets);
                    if (init_snippet_information[position])
                        throw Exception("Multiple definitions of the snippet information in the XML object.");
                    m_snippet_information[position].convertFromXML(parser);
                    init_snippet_information[position] = true;
                }
                else if (parser.isTagIdentifier("WordText"))
                {
                    if (init_word_text)
                        throw Exception("Multiple definitions of the word text vector in the XML object.");
                    loadVector(parser, "WordText", m_word_text);
                    init_word_text = true;
                }
                else if (parser.isTagIdentifier("WordOffset"))
                {
                    if (init_word_offset)
                        throw Exception("Multiple definitions of the word offset vector in the XML object.");
                    loadVector(parser, "WordOffset", m_word_offset);
                    init_word_offset = true;
                }
                else if (parser.isTagIdentifier("Codeword"))
                {
                    unsigned int index;
                    
                    index = parser.getAttribute("ID");
                    if (index >= m_number_of_codewords)
                        throw Exception("The codeword in the XML object is out of range (%d >= %d).", index, m_number_of_codewords);
                    if (init_codeword_information[index])
                        throw Exception("Multiple definitions of the same codeword (idx=%d) in the XML object.", index);
                    loadVector(parser, "Codeword", m_codewords[index]);
                    init_codeword_information[index] = true;
                }
                else if (parser.isTagIdentifier("Codeword_Gaussian_Model"))
                {
                    unsigned int index;
                    
                    index = parser.getAttribute("ID");
                    if (index >= m_number_of_codewords)
                        throw Exception("The codeword Gausian model in the XML object is out of range (%d >= %d).", index, m_number_of_codewords);
                    if (init_model_information[index])
                        throw Exception("Multiple definitions of the same codeword (idx=%d) in the XML object.", index);
                    m_codeword_models[index].convertFromXML(parser);
                    init_model_information[index] = true;
                }
                else parser.getNext();
            }
            parser.getNext();
            
            // Check that the structures have been properly initialized.
            if ((m_number_of_snippets > 0) && (!(init_word_text || init_word_offset)))
                throw Exception("The XML does not provide information about the word categories");
            for (unsigned int i = 0; i < m_number_of_snippets; ++i)
                if (!init_snippet_information[i])
                    throw Exception("Not all snippet information objects have been initialized by the XML object (object %d not initialized).", i);
            for (unsigned int i = 0; i < m_number_of_codewords; ++i)
            {
                // Check that the codeword centroid and Gaussian model has been properly initialized.
                if (!init_codeword_information[i])
                    throw Exception("Not all codewords have been initialized by the XML object (idx=%d).", i);
                if (!init_model_information[i])
                    throw Exception("Not all the codeword Gaussian models have been initialized by the XML object (idx=%d).", i);
                // Assign the codeword to its Gaussian model.
                m_codeword_models[i].setCentroid(m_codewords[i].getData());
            }
            // Check that the strings are well formed.
            if (m_word_offset[0] != 0) throw Exception("The word offset array of the XML object has a wrong initial value (it should be 0).");
            for (unsigned int i = 1; i < m_word_offset.size(); ++i)
                if (m_word_text[m_word_offset[i] - 1] != '\0')
                    throw Exception("The word text vector is not well formed.");
            if (m_word_text[m_word_text.size() - 1] != '\0')
                throw Exception("The word text vector is not well formed.");
            // Check that the labels of the snippets are correct.
            for (unsigned int i = 0; i < m_number_of_snippets; ++i)
                if (m_snippet_information[i].getWordCategoryID() >= m_word_offset.size())
                    throw Exception("The word category identifier of the snippet '%d' is out of range (%d >= %d).", i, m_snippet_information[i].getWordCategoryID(), m_word_offset.size());
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | RANK INFORMATION - IMPLEMENTATION    |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    RankScore * createScore(RANK_SCORE_IDENTIFIER identifier)
    {
        switch (identifier)
        {
        case RANK_SCORE_AVERAGE_PRECISION:
            return (RankScore *)new RankScoreAP();
        case RANK_SCORE_PRECISION:
            return (RankScore *)new RankScorePrecision();
        case RANK_SCORE_PRECISION_AT_RECALL:
            return (RankScore *)new RankScorePrecisionAtRecall();
        default:
            throw Exception("Rank score object with identifier '%d' has not been implemented yet.", (int)identifier);
        }
    }
    
    void RankScore::convertToXML(XmlParser &parser) const
    {
        parser.openTag("score");
        parser.setAttribute("id", (int)this->getIdentifier());
        this->convertToXMLAttributes(parser);
        parser.closeTag();
    }
    
    void RankScore::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("score"))
        {
            this->convertFromXMLAttributes(parser);
            while (!(parser.isTagIdentifier("score") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    double RankScoreAP::evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const
    {
        unsigned int valid, total, counter;
        double score;
        
        counter = (m_rank < 0)?std::numeric_limits<unsigned int>::max():m_rank;
        score = 0.0;
        valid = total = 0;
        for (unsigned int i = 0; (i < number_of_distances) && (counter > 0); ++i)
        {
            const unsigned int idx = std::get<1>(distances_rank[i]);
            if ((snippet_identifiers[idx] != query_identifier) && enabled[idx]) // If the distance does not belong to the own query and it belongs to an enabled sample.
            {
                if (snippet_categories[idx] == query_category)
                    score += (double)(++valid) / (double)(++total);
                else ++total;
                --counter;
            }
        }
        return (valid > 0)?(score / (double)valid):0.0;
    }
    
    double RankScorePrecision::evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const
    {
        unsigned int valid, total;
        
        valid = total = 0;
        for (unsigned int i = 0; (i < number_of_distances) && (total < m_rank); ++i)
        {
            const unsigned int idx = std::get<1>(distances_rank[i]);
            if ((snippet_identifiers[idx] != query_identifier) && enabled[idx]) // If the distance does not belong to the own query and it belongs to an enabled sample.
            {
                if (snippet_categories[idx] == query_category)
                    ++valid;
                ++total;
            }
        }
        return (valid > 0)?((double)valid / (double)total):0.0;
    }
    
    double RankScorePrecisionAtRecall::evaluate(unsigned long query_identifier, unsigned int query_category, const std::tuple<float, unsigned int> * distances_rank, unsigned int number_of_distances, const unsigned long * snippet_identifiers, const unsigned int * snippet_categories, const bool * enabled) const
    {
        unsigned int valid, total, total_valid;
        
        total_valid = 0;
        for (unsigned int i = 0; i < number_of_distances; ++i)
        {
            const unsigned int idx = std::get<1>(distances_rank[i]);
            if ((snippet_identifiers[idx] != query_identifier) && enabled[idx] // If the distance does not belong to the own query and it belongs to an enabled sample.
                && (snippet_categories[idx] == query_category))
                    ++total_valid;
        }
        if (total_valid == 0) // If no valid elements can be retrieved, then just exit.
            return 0.0;
        
        valid = total = 0;
        for (unsigned int i = 0; (i < number_of_distances) && ((double)valid / (double)total_valid < m_recall); ++i)
        {
            const unsigned int idx = std::get<1>(distances_rank[i]);
            if ((snippet_identifiers[idx] != query_identifier) && enabled[idx]) // If the distance does not belong to the own query and it belongs to an enabled sample.
            {
                if (snippet_categories[idx] == query_category)
                    ++valid;
                ++total;
            }
        }
        return (double)valid / (double)total;
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | EXPERIMENTS INFORMATION              |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    void SnippetQueryInformation::convertToXML(XmlParser &parser) const
    {
        unsigned int page_idx, block_idx, line_idx, symbol_idx;
        
        parser.openTag("symbol");
        parser.setAttribute("Identifier", m_identifier);
        retrieveSnippetLocation(m_identifier, page_idx, block_idx, line_idx, symbol_idx);
        parser.setAttribute("page", page_idx);
        parser.setAttribute("block", block_idx);
        parser.setAttribute("line", line_idx);
        parser.setAttribute("symbol", symbol_idx);
        parser.setAttribute("enabled", m_enabled);
        parser.setAttribute("query", m_query);
        parser.addChildren();
        parser.closeTag();
    }
    
    void SnippetQueryInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("symbol"))
        {
            unsigned int page_idx, block_idx, line_idx, symbol_idx;
            
            page_idx = parser.getAttribute("page");
            block_idx = parser.getAttribute("block");
            line_idx = parser.getAttribute("line");
            symbol_idx = parser.getAttribute("symbol");
            m_identifier = calculateSnippetIdentifier(page_idx, block_idx, line_idx, symbol_idx);
            m_enabled = parser.getAttribute("enabled");
            m_query = parser.getAttribute("query");
            while (!(parser.isTagIdentifier("symbol") && parser.isCloseTag())) parser.getNext();
            parser.getNext();
        }
    }
    
    void TrialInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("trial");
        parser.setAttribute("name", m_name.c_str());
        parser.addChildren();
        for (unsigned int i = 0; i < m_snippets.size(); ++i)
            m_snippets[i].convertToXML(parser);
        parser.closeTag();
    }
    
    void TrialInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("trial"))
        {
            std::list<SnippetQueryInformation *> query;
            unsigned int idx;
            
            m_name = (const char *)parser.getAttribute("name");
            while (!(parser.isTagIdentifier("trial") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("symbol"))
                {
                    query.push_back(new SnippetQueryInformation());
                    query.back()->convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
            
            m_snippets.set((unsigned int)query.size());
            idx = 0;
            for (auto &v : query)
            {
                m_snippets[idx++] = *v;
                delete v;
            }
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | EXPERIMENTS INFORMATION              |
    //                   | IMPLEMENTATION                       |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    ExperimentInformation::ExperimentInformation(const ExperimentInformation &other) :
        m_trials(other.m_trials),
        m_scores(other.m_scores.size()),
        m_name(other.m_name)
    {
        for (unsigned int i = 0; i < other.m_scores.size(); ++i)
            m_scores[i] = other.m_scores[i]->duplicate();
    }
    
    ExperimentInformation::~ExperimentInformation(void)
    {
        for (unsigned int i = 0; i < m_scores.size(); ++i)
            delete m_scores[i];
    }
    
    ExperimentInformation& ExperimentInformation::operator=(const ExperimentInformation &other)
    {
        if (this != &other)
        {
            for (unsigned int i = 0; i < m_scores.size(); ++i)
                delete m_scores[i];
            m_trials = other.m_trials;
            m_scores.set(other.m_scores.size());
            m_name = other.m_name;
            for (unsigned int i = 0; i < other.m_scores.size(); ++i)
                m_scores[i] = other.m_scores[i]->duplicate();
        }
        return *this;
    }
    
    void ExperimentInformation::convertToXML(XmlParser &parser) const
    {
        parser.openTag("experiment");
        parser.setAttribute("name", m_name.c_str());
        parser.addChildren();
        for (unsigned int i = 0; i < m_scores.size(); ++i)
            m_scores[i]->convertToXML(parser);
        for (unsigned int i = 0; i < m_trials.size(); ++i)
            m_trials[i].convertToXML(parser);
        parser.closeTag();
    }
    
    void ExperimentInformation::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("experiment"))
        {
            std::list<TrialInformation * > trials;
            std::list<RankScore *> scores;
            unsigned int idx;
            
            for (unsigned int i = 0; i < m_scores.size(); ++i)
                delete m_scores[i];
            
            m_name = (const char *)parser.getAttribute("name");
            while (!(parser.isTagIdentifier("experiment") && parser.isCloseTag()))
            {
                if (parser.isTagIdentifier("trial"))
                {
                    trials.push_back(new TrialInformation());
                    trials.back()->convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("score"))
                {
                    scores.push_back(createScore((RANK_SCORE_IDENTIFIER)((unsigned int)parser.getAttribute("id"))));
                    scores.back()->convertFromXML(parser);
                }
                else parser.getNext();
            }
            parser.getNext();
            
            m_trials.set((unsigned int)trials.size());
            idx = 0;
            for (auto &v : trials)
            {
                m_trials[idx++] = *v;
                delete v;
            }
            m_scores.set((unsigned int)scores.size());
            idx = 0;
            for (auto &v : scores)
                m_scores[idx++] = v;
        }
    }
    
    void loadExperiments(const char * filename, VectorDense<ExperimentInformation > &experiments)
    {
        std::list<ExperimentInformation *> experiment_list;
        std::ifstream file(filename);
        XmlParser parser(&file);
        unsigned int idx;
        
        if (!file.is_open()) throw Exception("Cannot load file '%s' to load the experiments information.", filename);
        while (parser.isTagIdentifier("experiment") && !parser.isCloseTag())
        {
            experiment_list.push_back(new ExperimentInformation());
            experiment_list.back()->convertFromXML(parser);
        }
        file.close();
        
        experiments.set((unsigned int)experiment_list.size());
        idx = 0;
        for (auto &v : experiment_list)
        {
            experiments[idx++] = *v;
            delete v;
        }
    }
    
    void saveExperiments(const char * filename, VectorDense<ExperimentInformation > &experiments)
    {
        std::ofstream file(filename);
        XmlParser parser(&file);
        
        if (!file.is_open()) throw Exception("Cannot open file '%s' to store the experiments information.", filename);
        if (file.rdstate() != std::ios_base::goodbit) throw Exception("An error flag has been raised while creating '%s'.", filename);
        for (unsigned int i = 0; i < experiments.size(); ++i)
            experiments[i].convertToXML(parser);
        if (file.rdstate() != std::ios_base::goodbit) throw Exception("An error flag has been raised while saving the object into '%s'.", filename);
        file.close();
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                   +--------------------------------------+
    //                   | DOCUMENT VISUAL INDEX GENERATOR      |
    //                   +--------------------------------------+
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                ##############
    //                                  ##########
    //                                    ######
    //                                      ##
    
    DocumentVisualIndexGenerator::DocumentVisualIndexGenerator(void) :
        m_codeword_weight(0),
        m_codeword_encode(0),
        m_number_of_neighbors(0),
        m_pooling_identifier(POOLING_ADDITIVE),
        m_negative_identifier(NEGATIVE_NOTHING),
        m_number_of_spatial_bins(0),
        m_power_factor(1.0)
    {
    }
    
    DocumentVisualIndexGenerator::DocumentVisualIndexGenerator(const DocumentVisualIndexGenerator &other) :
        m_codeword_weight((other.m_codeword_weight != 0)?other.m_codeword_weight->duplicate():0),
        m_codeword_encode((other.m_codeword_encode != 0)?other.m_codeword_encode->duplicate():0),
        m_number_of_neighbors(other.m_number_of_neighbors),
        m_pooling_identifier(other.m_pooling_identifier),
        m_negative_identifier(other.m_negative_identifier),
        m_partitions_values(other.m_partitions_values),
        m_number_of_spatial_bins(other.m_number_of_spatial_bins),
        m_normalization_spatial(other.m_normalization_spatial),
        m_power_factor(other.m_power_factor),
        m_normalization_global(other.m_normalization_global)
    {
    }
    
    DocumentVisualIndexGenerator::~DocumentVisualIndexGenerator(void)
    {
        if (m_codeword_weight != 0) delete m_codeword_weight;
        if (m_codeword_encode != 0) delete m_codeword_encode;
    }
    
    DocumentVisualIndexGenerator& DocumentVisualIndexGenerator::operator=(const DocumentVisualIndexGenerator &other)
    {
        if (this != &other)
        {
            // Free allocated memory.
            if (m_codeword_weight != 0) delete m_codeword_weight;
            if (m_codeword_encode != 0) delete m_codeword_encode;
            
            // Copy structures.
            m_codeword_weight = (other.m_codeword_weight != 0)?other.m_codeword_weight->duplicate():0;
            m_codeword_encode = (other.m_codeword_encode != 0)?other.m_codeword_encode->duplicate():0;
            m_number_of_neighbors = other.m_number_of_neighbors;
            m_pooling_identifier = other.m_pooling_identifier;
            m_negative_identifier = other.m_negative_identifier;
            m_partitions_values = other.m_partitions_values;
            m_number_of_spatial_bins = other.m_number_of_spatial_bins;
            m_normalization_spatial = other.m_normalization_spatial;
            m_power_factor = other.m_power_factor;
            m_normalization_global = other.m_normalization_global;
        }
        return *this;
    }
    
    void DocumentVisualIndexGenerator::convertToXML(XmlParser &parser) const
    {
        parser.openTag("DocumentVisualIndexParameters");
        parser.setAttribute("Number_Of_Neighbors", m_number_of_neighbors);
        parser.setAttribute("Pooling", (int)m_pooling_identifier);
        parser.setAttribute("Negative", (int)m_negative_identifier);
        parser.setAttribute("Power", m_power_factor);
        parser.addChildren();
        for (unsigned int i = 0; i < m_partitions_values.size(); ++i)
        {
            parser.openTag("SpatialPartition");
            parser.setAttribute("Level", i);
            parser.setAttribute("Horizontal", std::get<0>(m_partitions_values[i]));
            parser.setAttribute("Vertical", std::get<1>(m_partitions_values[i]));
            parser.closeTag();
        }
        m_codeword_weight->convertToXML(parser);
        m_codeword_encode->convertToXML(parser);
        parser.setSucceedingAttribute("Global", false);
        m_normalization_spatial.convertToXML(parser);
        parser.setSucceedingAttribute("Global", true);
        m_normalization_global.convertToXML(parser);
        parser.closeTag();
    }
    
    void DocumentVisualIndexGenerator::convertFromXML(XmlParser &parser)
    {
        if (parser.isTagIdentifier("DocumentVisualIndexParameters"))
        {
            std::map<unsigned int, std::tuple<int, int> > partitions_map;
            bool norm_global_defined, norm_spatial_defined;
            unsigned int maximum_level;
            
            // Free allocated memory.
            if (m_codeword_weight != 0) { delete m_codeword_weight; m_codeword_weight = 0; }
            if (m_codeword_encode != 0) { delete m_codeword_encode; m_codeword_encode = 0; }
            m_partitions_values.set(0);
            
            // Retrieve parameters.
            m_number_of_neighbors = parser.getAttribute("Number_Of_Neighbors");
            m_pooling_identifier = (POOLING_METHOD_IDENTIFIER)((int)parser.getAttribute("Pooling"));
            m_negative_identifier = (NEGATIVE_METHOD_IDENTIFIER)((int)parser.getAttribute("Negative"));
            m_power_factor = parser.getAttribute("Power");
            
            // Retrieve the remaining information.
            norm_global_defined = norm_spatial_defined = false;
            maximum_level = 0;
            while (!(parser.isTagIdentifier("DocumentVisualIndexParameters") && parser.isCloseTag()))
            {
                if      (parser.isTagIdentifier("SpatialPartition"))
                {
                    unsigned int current_level;
                    
                    current_level = parser.getAttribute("Level");
                    if (partitions_map.find(current_level) != partitions_map.end())
                        throw Exception("Multiple definitions of the spatial partitions at the %d-th level of the spatial pyramid.", current_level);
                    if (current_level > maximum_level) maximum_level = current_level;
                    partitions_map[current_level] = std::make_tuple((int)parser.getAttribute("Horizontal"), (int)parser.getAttribute("Vertical"));
                    while (!(parser.isTagIdentifier("SpatialPartition") && parser.isCloseTag())) parser.getNext();
                    parser.getNext();
                }
                else if (parser.isTagIdentifier("Codeword_Weight"))
                {
                    if (m_codeword_weight != 0) throw Exception("Multiple definitions of the codeword weight object.");
                    m_codeword_weight = CodewordWeightFactory<double, double, double, unsigned int>::Type::getInstantiator((CODEWORD_WEIGHT_IDENTIFIER)((int)parser.getAttribute("Identifier")));
                    m_codeword_weight->convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("Codeword_Encode"))
                {
                    if (m_codeword_encode != 0) throw Exception("Multiple definitions of the codeword encode object.");
                    m_codeword_encode = CodewordEncodeFactory<double, double, double, unsigned int>::Type::getInstantiator((CODEWORD_ENCODE_IDENTIFIER)((int)parser.getAttribute("Identifier")));
                    m_codeword_encode->convertFromXML(parser);
                }
                else if (parser.isTagIdentifier("Vector_Norm"))
                {
                    if ((bool)parser.getAttribute("Global"))
                    {
                        if (norm_global_defined) throw Exception("Multiple definitions of the global signature norm.");
                        norm_global_defined = true;
                        m_normalization_global.convertFromXML(parser);
                    }
                    else
                    {
                        if (norm_spatial_defined) throw Exception("Multiple definitions of the spatial norm of the signature.");
                        norm_spatial_defined = true;
                        m_normalization_spatial.convertFromXML(parser);
                    }
                }
                else parser.getNext();
            }
            parser.getNext();
            
            if (m_codeword_weight == 0) throw Exception("No codeword weight object defined in the XML object.");
            if (m_codeword_encode == 0) throw Exception("No codeword encode object defined in the XML object.");
            if (!norm_global_defined) throw Exception("No global signature norm defined in the XML object.");
            if (!norm_spatial_defined) throw Exception("No spatial normalization defined int he XML object.");
            if (maximum_level + 1 != (unsigned int)partitions_map.size()) throw Exception("Unexpected number of spatial partitions: expected %d but found %d, instead.", maximum_level + 1, (unsigned int)partitions_map.size());
            m_partitions_values.set((unsigned int)partitions_map.size());
            m_number_of_spatial_bins = 0;
            for (const auto &v : partitions_map)
            {
                m_partitions_values[v.first] = v.second;
                m_number_of_spatial_bins += std::get<0>(v.second) * std::get<1>(v.second);
            }
        }
    }
    
    void DocumentVisualIndexGenerator::computeSignature(unsigned int selected_idx, const DocumentVisualInformation &visual_information, VectorDense<double> &signature, unsigned int number_of_threads, BaseLogger * logger) const
    {
        const unsigned int number_of_dimensions = visual_information.getNumberOfDimensions();
        const unsigned int number_of_descriptors = visual_information[selected_idx].getNumberOfDescriptors();
        const unsigned int encode_size = m_codeword_encode->getNumberOfBinsPerCodeword(number_of_dimensions) * ((m_negative_identifier == NEGATIVE_SEPARATE)?2:1);
        const unsigned int signature_size = m_number_of_spatial_bins * encode_size * visual_information.getNumberOfCodewords();
        VectorSparse<double> * descriptors_distances, * descriptors_codes;
        ConstantSubVectorDense<double> * * descriptors_ptrs;
        VectorDense<double> * descriptors_double;
        
        // Allocate structures.
        descriptors_ptrs = new ConstantSubVectorDense<double> * [number_of_descriptors];
        descriptors_double = new VectorDense<double>[number_of_descriptors];
        descriptors_distances = new VectorSparse<double>[number_of_descriptors];
        descriptors_codes = new VectorSparse<double>[number_of_descriptors];
        for (unsigned int i = 0; i < number_of_descriptors; ++i)
        {
            descriptors_double[i].set(number_of_dimensions);
            descriptors_ptrs[i] = new ConstantSubVectorDense<double>(descriptors_double[i]);
            descriptors_distances[i].set(m_number_of_neighbors);
        }
        signature.set(signature_size);
        
        // Compute the signature.
        computeSignature(selected_idx, visual_information, descriptors_double, descriptors_ptrs, descriptors_distances, descriptors_codes, signature, number_of_threads, logger);
        
        // Free allocated memory.
        delete [] descriptors_distances;
        delete [] descriptors_double;
        delete [] descriptors_codes;
        for (unsigned int i = 0; i < number_of_descriptors; ++i)
            delete descriptors_ptrs[i];
        delete [] descriptors_ptrs;
    }
    
    void DocumentVisualIndexGenerator::computeSignature(unsigned int selected_idx, const DocumentVisualInformation &visual_information, VectorDense<double> * descriptors_double, ConstantSubVectorDense<double> * * descriptors_ptrs, VectorSparse<double> * descriptors_distances, VectorSparse<double> * descriptors_codes, VectorDense<double> &signature, unsigned int number_of_threads, BaseLogger * logger) const
    {
        const unsigned int number_of_dimensions = visual_information.getNumberOfDimensions();
        const unsigned int encode_size = m_codeword_encode->getNumberOfBinsPerCodeword(number_of_dimensions) * ((m_negative_identifier == NEGATIVE_SEPARATE)?2:1);
        const unsigned int signature_size = m_number_of_spatial_bins * encode_size * visual_information.getNumberOfCodewords();
        
        signature.setValue(0);
        for (unsigned int j = 0; j < visual_information[selected_idx].getNumberOfDescriptors(); ++j)
        {
            const unsigned int * neighbor_distance = visual_information[selected_idx].getDescriptorNeighborDistance(j);
            const unsigned int * neighbor_indexes = visual_information[selected_idx].getDescriptorNeighborIndex(j);
            for (unsigned int k = 0; k < number_of_dimensions; ++k)
                descriptors_double[j][k] = (double)visual_information[selected_idx].getDescriptor(j)[k] / 255.0;
            for (unsigned int k = 0; k < m_number_of_neighbors; ++k)
                descriptors_distances[j][k].setData((double)neighbor_distance[k] / 255.0, neighbor_indexes[k]);
        }
        
        // Calculate the weights of the visual words.
        if (logger != 0)
            logger->log("Snippet %07d [%s]: Encoding the %d descriptors.                     \r", selected_idx, visual_information.getWord(visual_information[selected_idx].getWordCategoryID()), visual_information[selected_idx].getNumberOfDescriptors());
        m_codeword_weight->process(descriptors_ptrs,                                       // Descriptors (converted to double and 'de-quantized')
                                descriptors_distances,                                     // Sparse vector with the distance to the selected codewords as input. The function stores here the weight of the codewords.
                                visual_information[selected_idx].getNumberOfDescriptors(), // Number of descriptors to weight.
                                visual_information.getCodewordModels(),                    // Pointer to the codeword models.
                                number_of_threads);                                        // Number of threads used to calculate the weights.
        // Calculate the visual words encoding.
        m_codeword_encode->process(descriptors_ptrs,                                       // Descriptors (converted to double and 'de-quantized')
                                descriptors_distances,                                     // Sparse vector with the weights of the selected codewords.
                                descriptors_codes,                                         // Vector with the selected encoding of the codewords.
                                visual_information[selected_idx].getNumberOfDescriptors(), // Number of descriptors to weight.
                                visual_information.getCodewordModels(),                    // Pointer to the codeword models.
                                number_of_threads);                                        // Number of threads used to calculate the weights.
        // Add the weights to the signature.
        if (logger != 0)
            logger->log("Snippet %07d [%s]: Pooling the %d visual words.                     \r", selected_idx, visual_information.getWord(visual_information[selected_idx].getWordCategoryID()), visual_information[selected_idx].getNumberOfDescriptors() * m_number_of_neighbors * encode_size / ((m_negative_identifier == NEGATIVE_SEPARATE)?2:1));
        for (unsigned int j = 0; j < visual_information[selected_idx].getNumberOfDescriptors(); ++j)
        {
            for (unsigned int p = 0, offset = 0; p < m_partitions_values.size(); ++p)
            {
                const unsigned int px = std::min((unsigned int)((float)std::get<0>(m_partitions_values[p]) * visual_information[selected_idx].getX(j)), std::get<0>(m_partitions_values[p]) - 1);
                const unsigned int py = std::min((unsigned int)((float)std::get<1>(m_partitions_values[p]) * visual_information[selected_idx].getY(j)), std::get<1>(m_partitions_values[p]) - 1);
                const unsigned int pbin = (px + py * std::get<0>(m_partitions_values[p]) + offset) * encode_size * visual_information.getNumberOfCodewords();
                
                if       ((m_negative_identifier == NEGATIVE_NOTHING) && (m_pooling_identifier == POOLING_ADDITIVE))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + descriptors_codes[j].getIndex(k);
                        signature[current_bin] += descriptors_codes[j].getValue(k);
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_NOTHING) && (m_pooling_identifier == POOLING_MAXIMUM))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + descriptors_codes[j].getIndex(k);
                        if (descriptors_codes[j].getValue(k) > signature[current_bin])
                            signature[current_bin] = descriptors_codes[j].getValue(k);
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_NOTHING) && (m_pooling_identifier == POOLING_ABSOLUTE_MAXIMUM))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + descriptors_codes[j].getIndex(k);
                        if (std::abs(descriptors_codes[j].getValue(k)) > std::abs(signature[current_bin]))
                            signature[current_bin] = descriptors_codes[j].getValue(k);
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_ABSOLUTE) && (m_pooling_identifier == POOLING_ADDITIVE))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + descriptors_codes[j].getIndex(k);
                        signature[current_bin] += std::abs(descriptors_codes[j].getValue(k));
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_ABSOLUTE) && ((m_pooling_identifier == POOLING_MAXIMUM) || (m_pooling_identifier == POOLING_ABSOLUTE_MAXIMUM)))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + descriptors_codes[j].getIndex(k);
                        if (std::abs(descriptors_codes[j].getValue(k)) > signature[current_bin])
                            signature[current_bin] = std::abs(descriptors_codes[j].getValue(k));
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_SEPARATE) && (m_pooling_identifier == POOLING_ADDITIVE))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + 2 * descriptors_codes[j].getIndex(k) + ((descriptors_codes[j].getValue(k) < 0)?1:0);
                        signature[current_bin] += std::abs(descriptors_codes[j].getValue(k));
                    }
                }
                else if  ((m_negative_identifier == NEGATIVE_SEPARATE) && ((m_pooling_identifier == POOLING_MAXIMUM) || (m_pooling_identifier == POOLING_ABSOLUTE_MAXIMUM)))
                {
                    for (unsigned int k = 0; k < descriptors_codes[j].size(); ++k)
                    {
                        const unsigned int current_bin = pbin + 2 * descriptors_codes[j].getIndex(k) + ((descriptors_codes[j].getValue(k) < 0)?1:0);
                        if (std::abs(descriptors_codes[j].getValue(k)) > signature[current_bin])
                            signature[current_bin] = std::abs(descriptors_codes[j].getValue(k));
                    }
                }
                
                offset += std::get<0>(m_partitions_values[p]) * std::get<1>(m_partitions_values[p]);
            }
        }
        for (unsigned int p = 0, offset = 0; p < m_partitions_values.size(); ++p)
        {
            SubVectorDense<double> current;
            
            const unsigned int current_size = std::get<0>(m_partitions_values[p]) * std::get<1>(m_partitions_values[p]);
            current = SubVectorDense<double>(&signature[offset * encode_size * visual_information.getNumberOfCodewords()], current_size * encode_size * visual_information.getNumberOfCodewords());
            current /= m_normalization_spatial.norm(current);
            offset += current_size;
        }
        signature /= m_normalization_global.norm(signature);
        if (m_power_factor != 1.0)
        {
            for (unsigned int d = 0; d < signature_size; ++d)
                signature[d] = (signature[d] < 0.0)?(-pow(-signature[d], m_power_factor)):pow(signature[d], m_power_factor);
            signature /= m_normalization_global.norm(signature);
        }
    }
    
    //                                      ##
    //                                    ######
    //                                  ##########
    //                                ##############
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    //                                    ######
    
}

