// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <list>
#include <string>
#include <omp.h>
#include <memory>
#include <zfstream/zfstream.hpp>
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_image.hpp"
#include "srv_logger.hpp"
#include "srv_visual_words.hpp"
#include "srv_database_information.hpp"
#include "srv_documents.hpp"
#include "common_functions.hpp"

INITIALIZE_DENSE_VISUAL_WORDS(short, short, unsigned char, int, unsigned int)

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                              21-May-2018\n"
                                   "=================================================================================\n"
                                   " Extract the visual words information of each document snippet of the dataset.\n\n";
    
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char filename_visual_information[4096], filename_index[4096];
    unsigned int number_of_threads;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./index_create", initial_message);
    srv::DocumentVisualInformation visual_information;
    srv::DocumentVisualIndexGenerator histogram_generator;
    srv::VectorDense<srv::ExperimentInformation> experiments;
    srv::DocumentVisualIndex<DEF_TVALUE, DEF_TINDEX> visual_index;
    srv::StreamLogger logger(&std::cout);
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        unsigned int codeword_weight_index, codeword_encode_index, normalization_spatial_index, normalization_global_index, number_of_neighbors, pooling_method_index, negative_values_index;
        double codeword_weight_sigma, codeword_super_weight, normalization_spatial_order, normalization_global_order, power_factor;
        char partition_x_text[4096], partition_y_text[4096], filename_experiments[4096];
        srv::CodewordWeightBase<double, double, double, unsigned int> * codeword_weight;
        srv::CodewordEncodeBase<double, double, double, unsigned int> * codeword_encode;
        srv::VectorDense<std::pair<unsigned int, unsigned int> > partitions_values;
        srv::VectorNorm normalization_spatial, normalization_global;
        std::list<int> partition_x_list, partition_y_list;
        srv::POOLING_METHOD_IDENTIFIER pooling_identifier;
        srv::NEGATIVE_METHOD_IDENTIFIER negative_identifier;
        
        // Mandatory parameters.
        parameters.addParameter(new srv::ParameterCharArray("filename_visual_information", "file where the document's visual information is stored into.", filename_visual_information));
        parameters.addParameter(new srv::ParameterUInt("weight", "method used to weight the contribution of each codeword while encoding a descriptor. Possible values are:"
                                                                 "&0&Uniform"
                                                                 "&1&Distance"
                                                                 "&2&LLC"
                                                                 "&3&Gaussian Kernel"
                                                                 "&4&Normalized Gaussian Kernel"
                                                                 "&5&Fischer vector", true, 0, true, 5, &codeword_weight_index));
        parameters.addParameter(new srv::ParameterUInt("encode", "method used to encode the descriptor with the selected codewords. Possible values are:"
                                                                 "&0&Centroid"
                                                                 "&1&First derivative (VLAD)"
                                                                 "&2&Super vector (centroid + VLAD)"
                                                                 "&3&Fischer vector (VLAD + Diagonal 2on Derivative)"
                                                                 "&4&Tensor (full 2on derivative)", true, 0, true, 4, &codeword_encode_index));
        parameters.addParameter(new srv::ParameterUInt("neighbors", "number of codewords used to encode a descriptor.", true, 1, false, 0, &number_of_neighbors));
        parameters.addParameter(new srv::ParameterCharArray("filename_index", "file where the database index is stored into.", filename_index));
        // Codeword parameters.
        parameters.addParameter(new srv::ParameterDouble("--sigma", "value", "sigma of the Gaussian kernel.", 2.0f, true, 0.0f, false, 0.0f, &codeword_weight_sigma));
        parameters.addParameter(new srv::ParameterDouble("--super", "weight", "multiplicative weight applied to the centroid bin of the super vector representation.", 1.0f, true, 0.0f, false, 0.0f, &codeword_super_weight));
        parameters.addParameter(new srv::ParameterUInt("--pool", "index", "method used to pool the values of the codewords. Possible values are:&0&Additive.&1&Maximum value.&2&Maximum absolute value.", 0, true, 0, true, 2, &pooling_method_index));
        parameters.addParameter(new srv::ParameterUInt("--polarize", "index", "method used to deal with negative codeword contributions. Possible values are:&0&Nothing (i.e. no different than positive contributions).&1&Absolute value.&2&Separate positive and negative contributions.", 0, true, 0, true, 2, &negative_values_index));
        // Spatial pyramid.
        parameters.addParameter(new srv::ParameterCharArray("--pyramid-x", "list", "list of horizontal spatial divisions (e.g. '--pyramid-x 3:9' for 3 partitions in the 1st level and 9 in the 2on).", partition_x_text));
        parameters.addParameter(new srv::ParameterCharArray("--pyramid-y", "list", "list of vectical spatial divisions (e.g. '--pyramid 1:2' for a single partition in the 1st level and 2 in the 2on).", partition_y_text));
        // Normalization parameters.
        parameters.addParameter(new srv::ParameterUInt("--norm-spatial", "index", "normalization method independently applied to each spatial level. Possible values are:"
                                                                                  "&0&Manhattan norm"
                                                                                  "&1&Euclidean norm"
                                                                                  "&2&Maximum norm"
                                                                                  "&3&p-norm", 0, true, 0, true, 3, &normalization_spatial_index));
        parameters.addParameter(new srv::ParameterDouble("--norm-spatial-power", "factor", "power factor of the spatial p-norm", 1.0, true, 0.0, false, 0.0, &normalization_spatial_order));
        parameters.addParameter(new srv::ParameterUInt("--norm-global", "index", "normalization method applied to the whole signature. Possible values are:"
                                                                                  "&0&Manhattan norm"
                                                                                  "&1&Euclidean norm"
                                                                                  "&2&Maximum norm"
                                                                                  "&3&p-norm", 1, true, 0, true, 3, &normalization_global_index));
        parameters.addParameter(new srv::ParameterDouble("--norm-global-power", "factor", "power factor of the global p-norm", 1.0, true, 0.0, false, 0.0, &normalization_global_order));
        parameters.addParameter(new srv::ParameterDouble("--pow", "factor", "normalization power factor applied to each bin of the histogram of visual words.", 1.0f, true, 0.0f, true, 1.0f, &power_factor));
        parameters.addParameter(new srv::ParameterCharArray("--evaluate", "filename", "file information of experiments used to test the performance of the visual index.", filename_experiments));
        // Miscellaneous parameters.
        parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently create the index.", 1, true, 1, false, 0, &number_of_threads));
        
        // Parser the user parameters.
        filename_experiments[0] = partition_x_text[0] = partition_y_text[0] = '\0';
        parameters.parse(argv, argc);
        
        logger.log("Creating the histogram generator object.");
        if      (codeword_weight_index == 0) codeword_weight = new srv::CodewordWeightUniform<double, double, double, unsigned int>();
        else if (codeword_weight_index == 1) codeword_weight = new srv::CodewordWeightDistance<double, double, double, unsigned int>();
        else if (codeword_weight_index == 2) codeword_weight = new srv::CodewordWeightLLC<double, double, double, unsigned int>();
        else if (codeword_weight_index == 3) codeword_weight = new srv::CodewordWeightGaussian<double, double, double, unsigned int>(codeword_weight_sigma);
        else if (codeword_weight_index == 4) codeword_weight = new srv::CodewordWeightNormalizedGaussian<double, double, double, unsigned int>(codeword_weight_sigma);
        else if (codeword_weight_index == 5) codeword_weight = new srv::CodewordWeightFisher<double, double, double, unsigned int>();
        else throw srv::Exception("The selected codeword weight (index '%d') is not implemented yet.", codeword_weight_index);
        
        if      (codeword_encode_index == 0) codeword_encode = new srv::CodewordEncodeCentroid<double, double, double, unsigned int>();
        else if (codeword_encode_index == 1) codeword_encode = new srv::CodewordEncodeFirstDerivative<double, double, double, unsigned int>();
        else if (codeword_encode_index == 2) codeword_encode = new srv::CodewordEncodeSuperVector<double, double, double, unsigned int>(codeword_super_weight);
        else if (codeword_encode_index == 3) codeword_encode = new srv::CodewordEncodeFisher<double, double, double, unsigned int>();
        else if (codeword_encode_index == 4) codeword_encode = new srv::CodewordEncodeTensor<double, double, double, unsigned int>();
        else throw srv::Exception("The selected codeword encode (index '%d') is not implemented yet.", codeword_encode_index);
        
        if      (pooling_method_index == 0) pooling_identifier = srv::POOLING_ADDITIVE;
        else if (pooling_method_index == 1) pooling_identifier = srv::POOLING_MAXIMUM;
        else if (pooling_method_index == 2) pooling_identifier = srv::POOLING_ABSOLUTE_MAXIMUM;
        else throw srv::Exception("The method selected to pool the codewords (index '%d') together is not implemented yet.", pooling_method_index);
        
        if      (negative_values_index == 0) negative_identifier = srv::NEGATIVE_NOTHING;
        else if (negative_values_index == 1) negative_identifier = srv::NEGATIVE_ABSOLUTE;
        else if (negative_values_index == 2) negative_identifier = srv::NEGATIVE_SEPARATE;
        else throw srv::Exception("The method used to treat negative codeword contributions (index '%d') is not implemented yet.", negative_values_index);
        
        
        if (partition_x_text[0] != '\0')
            srv::text2list<int>(partition_x_text, 1, std::numeric_limits<int>::max(), partition_x_list);
        if (partition_y_text[0] != '\0')
            srv::text2list<int>(partition_y_text, 1, std::numeric_limits<int>::max(), partition_y_list);
        if (partition_x_list.size() == 0) partition_x_list.push_back(1);
        if (partition_y_list.size() == 0) partition_y_list.push_back(1);
        
        if ((partition_x_list.size() != 1) && (partition_y_list.size() != 1) && (partition_x_list.size() != partition_y_list.size()))
            throw srv::Exception("The size of the partition lists is incompatible (they should have the same amount of elements or one of the must have a single element).");
        partitions_values.set((unsigned int)std::max(partition_x_list.size(), partition_y_list.size()));
        if (partition_x_list.size() == 1)
            for (unsigned int i = 0; i < partitions_values.size(); ++i) std::get<0>(partitions_values[i]) = partition_x_list.front();
        else
        {
            unsigned int idx = 0;
            for (const auto &v : partition_x_list)
                std::get<0>(partitions_values[idx++]) = v;
        }
        if (partition_y_list.size() == 1)
            for (unsigned int i = 0; i < partitions_values.size(); ++i) std::get<1>(partitions_values[i]) = partition_y_list.front();
        else
        {
            unsigned int idx = 0;
            for (const auto &v : partition_y_list)
                std::get<1>(partitions_values[idx++]) = v;
        }
        if      (normalization_spatial_index == 0) normalization_spatial.set(srv::MANHATTAN_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 1) normalization_spatial.set(srv::EUCLIDEAN_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 2) normalization_spatial.set(srv::MAXIMUM_NORM, normalization_spatial_order);
        else if (normalization_spatial_index == 3) normalization_spatial.set(srv::P_NORM, normalization_spatial_order);
        else throw srv::Exception("The selected spatial norm (with index '%d') has not been implemented yet.", normalization_spatial_index);
        if      (normalization_global_index == 0) normalization_global.set(srv::MANHATTAN_NORM, normalization_global_order);
        else if (normalization_global_index == 1) normalization_global.set(srv::EUCLIDEAN_NORM, normalization_global_order);
        else if (normalization_global_index == 2) normalization_global.set(srv::MAXIMUM_NORM, normalization_global_order);
        else if (normalization_global_index == 3) normalization_global.set(srv::P_NORM, normalization_global_order);
        else throw srv::Exception("The selected global norm (with index '%d') has not been implemented yet.", normalization_global_index);
        
        histogram_generator.setEncode(codeword_weight, codeword_encode, number_of_neighbors, pooling_identifier, negative_identifier);
        histogram_generator.setSpatialPartitions(partitions_values);
        histogram_generator.setNormalization(normalization_spatial, power_factor, normalization_global);
        
        if (filename_experiments[0] != '\0')
            srv::loadExperiments(filename_experiments, experiments);
        
        // Free allocated memory.
        delete codeword_weight;
        delete codeword_encode;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Load the visual information object ]-------------------------------------------------------------------------------------------------------
    try
    {
        srv::compressionPolicy(srv::ZLibCompression);
        std::istream * file = srv::iZipStream(filename_visual_information);
        
        logger.log("Loading visual information from '%s'.", filename_visual_information);
        if (file == 0) throw srv::Exception("File '%s' cannot be loaded.", filename_visual_information);
        srv::XmlParser parser(file);
        
        if (parser.isTagIdentifier("DocumentVisualInformation")) visual_information.convertFromXML(parser);
        else throw srv::Exception("XML file '%s' does not contain a document visual information object.", filename_visual_information);
        
        delete file;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while loading the visual information object:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Create the BoVW histograms ]---------------------------------------------------------------------------------------------------------------
    try
    {
        logger.log("Generating visual signatures.");
        histogram_generator.generate(visual_information, visual_index, number_of_threads, &logger);
        
        if (experiments.size() > 0)
        {
            srv::VectorDense<srv::VectorDense<srv::VectorDense<std::tuple<double, unsigned int> > > > scores;
            
            logger.log("Index evaluation using EUCLIDEAN distance:");
            visual_index.evaluate(experiments.getData(), experiments.size(), srv::DOCUMENT_INDEX_DISTANCE_EUCLIDEAN, scores, number_of_threads, &logger);
            for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
            {
                for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                {
                    logger.log("Experiment: %s  [%s]", experiments[eidx].getName(), experiments[eidx].getScore(sidx)->getName());
                    for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                    {
                        logger.log("Trial %s: %f [%d queries]", experiments[eidx][tidx].getName(), std::get<0>(scores[eidx][tidx][sidx]), std::get<1>(scores[eidx][tidx][sidx]));
                    }
                }
            }
            logger.log("Index evaluation using MANHATTAN distance:");
            visual_index.evaluate(experiments.getData(), experiments.size(), srv::DOCUMENT_INDEX_DISTANCE_MANHATTAN, scores, number_of_threads, &logger);
            for (unsigned int eidx = 0; eidx < experiments.size(); ++eidx)
            {
                for (unsigned int sidx = 0; sidx < experiments[eidx].getNumberOfScores(); ++sidx)
                {
                    logger.log("Experiment: %s  [%s]", experiments[eidx].getName(), experiments[eidx].getScore(sidx)->getName());
                    for (unsigned int tidx = 0; tidx < experiments[eidx].getNumberOfTrials(); ++tidx)
                    {
                        logger.log("Trial %s: %f [%d queries]", experiments[eidx][tidx].getName(), std::get<0>(scores[eidx][tidx][sidx]), std::get<1>(scores[eidx][tidx][sidx]));
                    }
                }
            }
        }
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the histograms:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    // -[ Save the visual index ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::compressionPolicy(srv::ZLibCompression);
        std::ostream * file = srv::oZipStream(filename_index);
        
        logger.log("Saving the signatures to '%s'.", filename_index);
        if (file == 0)
            throw srv::Exception("File '%s' cannot be created.", filename_index);
        if (file->rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while creating '%s'.", filename_index);
        srv::XmlParser parser(file);
        visual_index.convertToXML(parser);
        if (file->rdstate() != std::ios_base::goodbit)
            throw srv::Exception("An error flag has been raised while saving the object into '%s'.", filename_index);
        delete file;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while processing the histograms:" << std::endl << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}
