// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include "srv_image.hpp"
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_calibration.hpp"

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                            13-March-2013\n"
                                   "=================================================================================\n"
                                   " Application which test the calibration chessboard detector.\n\n";
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    char image_filename[4096], save_filename[4096];
    unsigned int number_of_threads;
    bool gaussian_corners;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./chessboard", initial_message);
    
    // -[ Define use parameters ]--------------------------------------------------------------------------------------------------------------------
    parameters.addParameter(new srv::ParameterCharArray("image_filename", "image where the chessboard pattern is searched.", image_filename));
    parameters.addParameter(new srv::ParameterBoolean("--gaussian", "", "uses a Gaussian kernel to calculate the location of the chessboard kernels.", &gaussian_corners));
    parameters.addParameter(new srv::ParameterCharArray("--save", "filename", "file where the chessboard result is directly saved.", save_filename));
    parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently process the image.", 1, true, 1, false, 0, & number_of_threads));
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        save_filename[0] = '\0';
        parameters.parse(argv, argc);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        
        return EXIT_FAILURE;
    }
    
    // -[ Search the chessboard and show the results ]-----------------------------------------------------------------------------------------------
    try
    {
        srv::Figure figure("Image");
        srv::Image<unsigned char> image(image_filename), show_image;
        srv::VectorDense<srv::ChessboardCorner> corners;
        srv::VectorDense<srv::ChessboardInformation> chessboards;
        srv::Draw::Pencil<unsigned char> red_pencil;
        srv::Chronometer chrono;
        red_pencil.setBorderColor(255, 0, 0, 255);
        red_pencil.setBackgroundColor(0, 0, 0, 0);
        red_pencil.setBorderSize(1);
        red_pencil.setAntialiasing(false);
        
        ///////// // ----| Sample code to call the DFT algorithm |-------------------------------------------------------------------
        ///////// srv::Image<unsigned char> debug_image(srv::ImageRGB2Gray(image));
        ///////// unsigned int t_width = debug_image.getWidth();
        ///////// unsigned int t_height = debug_image.getHeight();
        ///////// srv::Image<double> test_in(debug_image);
        ///////// srv::Image<double> test_result(t_width, t_height, 1);
        ///////// unsigned int t_width_complex = t_width / 2 + 1;
        ///////// 
        ///////// fftw_complex * test_out    = (fftw_complex *)fftw_malloc(sizeof(fftw_complex) * t_width_complex * t_height);
        ///////// fftw_plan plan_in, plan_out;
        ///////// 
        ///////// plan_in  = fftw_plan_dft_r2c_2d(t_height, t_width, test_in.get(0), test_out, FFTW_MEASURE);
        ///////// plan_out = fftw_plan_dft_c2r_2d(t_height, t_width, test_out, test_result.get(0), FFTW_MEASURE);
        ///////// fftw_execute(plan_in);
        ///////// fftw_execute(plan_out);
        ///////// 
        ///////// debug_image = test_result / (double)(t_width * t_height);
        ///////// 
        ///////// fftw_destroy_plan(plan_in);
        ///////// fftw_destroy_plan(plan_out);
        ///////// fftw_free(test_out);
        ///////// figure(debug_image);
        ///////// for (char key = 0; key != 27; key = srv::Figure::wait());
        ////srv::ImageFFT fft(2 * image.getWidth(), 2 * image.getHeight(), image.getNumberOfChannels());
        ////srv::Image<unsigned char> output(2 * image.getWidth(), 2 * image.getHeight(), image.getNumberOfChannels());
        ////fft.setImage(image);
        ////fft.getImage(output);
        ////image = output;
        
        chrono.start();
        if (gaussian_corners)
            findCornersGaussian(image, 0.01, corners, number_of_threads);
        else findCorners(image, 0.01, corners, number_of_threads);
        std::cout << corners.size() << " corners detected in " << chrono.getElapsedTime() / 1000.0 << " seconds." << std::endl;
        chrono.start();
        detectChessboard(corners, chessboards);
        std::cout << chessboards.size() << " chessboard/s detected in " << chrono.getElapsedTime() / 1000.0 << " seconds." << std::endl;
        
        if (image.getNumberOfChannels() == 4) show_image = image.subimage(0, 2);
        else if (image.getNumberOfChannels() == 1) srv::ImageGray2RGB(image, show_image);
        else if (image.getNumberOfChannels() == 3) show_image = image;
        else srv::ImageGray2RGB(image.subimage(0), show_image);
        for (unsigned int i = 0; i < corners.size(); ++i)
        {
            int x, y, vx1, vy1, vx2, vy2;
            
            x = (int)round(corners[i].getX());
            y = (int)round(corners[i].getY());
            vx1 = (int)(corners[i].getX() + 20.0 * corners[i].getFirstEdgeX());
            vy1 = (int)(corners[i].getY() + 20.0 * corners[i].getFirstEdgeY());
            vx2 = (int)(corners[i].getX() + 20.0 * corners[i].getSecondEdgeX());
            vy2 = (int)(corners[i].getY() + 20.0 * corners[i].getSecondEdgeY());
            srv::Draw::Rectangle(show_image, x - 3, y - 3, x + 3, y + 3, red_pencil);
            srv::Draw::Line(show_image, x, y, vx1, vy1, red_pencil);
            srv::Draw::Line(show_image, x, y, vx2, vy2, red_pencil);
        }
        for (unsigned int i = 0; i < chessboards.size(); ++i)
            chessboards[i].draw(show_image);
        if (save_filename[0] != '\0') show_image.save(save_filename);
        else
        {
            figure(show_image);
            for (char key = 0; key != 27; key = srv::Figure::wait());
        }
        
        if (0)
        {
            chrono.start();
            srv::Image<double> gray_image(srv::ImageMaxcon(srv::ImageRGB2Gray(image)) / 255.0);
            srv::Image<double> pattern, pattern_45, accumulated;
            pattern.setGeometry(gray_image);
            pattern_45.setGeometry(gray_image);
            accumulated.setGeometry(gray_image);
            accumulated.setValue(0.0);
            for (unsigned int i = 0; i < 3; ++i)
            {
                double sigma = (double)(i + 1) * 3.0;
                srv::Gaussian2DFilterRecursiveKernel kernel(srv::VLIET_4TH, sigma, 1, 1, false);
                srv::Gaussian2DFilterRecursiveKernel kernel45(srv::VLIET_4TH, sigma, 1, 1, true);
                srv::GaussianFilter(gray_image, kernel, pattern, number_of_threads);
                srv::GaussianFilter(gray_image, kernel45, pattern_45, number_of_threads);
                srv::ImageAbs(pattern, pattern);
                srv::ImageAbs(pattern_45, pattern_45);
                srv::ImageMaximum(accumulated, pattern, accumulated);
                srv::ImageMaximum(accumulated, pattern_45, accumulated);
            }
            std::cout << "Elapsed time: " << chrono.getElapsedTime() / 1000.0 << std::endl;
            figure(ImageGray2HueColormap(srv::ImageMaxcon(accumulated)));
            for (char key = 0; key != 27; key = srv::Figure::wait());
        }
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while searching the chessboard pattern in the image:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

