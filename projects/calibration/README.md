# SRV project: _Calibration Pattern Detection_

C++ implementation of the chessboard pattern detection proposed in _A. Geiger, F. Moosmann, O. Car, B. Schuster, "[Automatic Camera and Range Sensor Calibration using Simple Shot](https://doi.org/10.1109/ICRA.2012.6224570)", ICRA 2012_

The project is located in `projects/calibration/`. Use `make RELEASE=1` to generate the binaries in release mode (otherwise, they are generated without optimization and adding debug information). This will generate two binaries:

* **chessboard:** Applies the pattern detection algorithm to a file and save the detected corners to a file.
* **chessboard_camera:** Connects to a camera and applies the algorithm to the video feed _(demo in the video below)_.


<div align="center">
  <a href="https://www.youtube.com/watch?v=u8fq2TOZmNw"><img src="https://img.youtube.com/vi/u8fq2TOZmNw/0.jpg" alt="Chessboard pattern detection"></a>
</div>
