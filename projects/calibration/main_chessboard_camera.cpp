// Semantic Robot Vision algorithms
// Copyright (C) 2010- David X. Aldavert Miró
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or (at
// your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111, USA

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <pthread.h>
#include "srv_image.hpp"
#include "image/srv_camera.hpp"
#include "srv_xml.hpp"
#include "srv_utilities.hpp"
#include "srv_calibration.hpp"

typedef struct
{
    srv::Image<unsigned char> camera_image;
    srv::Image<unsigned char> pattern_image;
    unsigned int number_of_threads;
    double process_time_step;
    bool gaussian_corners;
    bool search_patterns;
    bool thread_enabled;
    srv::VectorDense<srv::ChessboardInformation> chessboards;
} ProcessInformation;

void * process_pattern(void * ptr)
{
    ProcessInformation & information = *((ProcessInformation *)ptr);
    srv::VectorDense<srv::ChessboardCorner> corners;
    srv::Chronometer chrono;
    double previous_time;
    const unsigned int sleep_time = (unsigned int)((srv::srvMin<double>(0.5, information.process_time_step) / 10.0) * 1000000.0);
    srv::Image<unsigned char> thread_buffer(information.pattern_image);
    
    previous_time = chrono.getElapsedTime() / 1000.0;
    
    while (information.thread_enabled)
    {
        if (information.search_patterns && (chrono.getElapsedTime() / 1000.0 - previous_time > information.process_time_step))
        {
            previous_time = chrono.getElapsedTime() / 1000.0;
            thread_buffer.copy(information.camera_image);
            if (information.gaussian_corners)
                findCornersGaussian(thread_buffer, 0.01, corners, information.number_of_threads);
            else findCorners(thread_buffer, 0.01, corners, information.number_of_threads);
            detectChessboard(corners, information.chessboards);
            for (unsigned int i = 0; i < information.chessboards.size(); ++i)
                information.chessboards[i].draw(thread_buffer);
            information.pattern_image.copy(thread_buffer);
        }
        else usleep(sleep_time);
    }
    return 0;
}

int main(int argc, char * * argv)
{
    // -[ Constants definitions ]--------------------------------------------------------------------------------------------------------------------
    const char initial_message[] = " David X. Aldavert Miró                                            13-March-2013\n"
                                   "=================================================================================\n"
                                   " Application which test the calibration chessboard detector.\n\n";
   
    // -[ Parameters variables ]---------------------------------------------------------------------------------------------------------------------
    unsigned int width, height, rate, image_format_index;
    char camera_filename[4096];
    ProcessInformation information;
    
    // -[ Other variables ]--------------------------------------------------------------------------------------------------------------------------
    srv::ParameterParser parameters("./chessboard_camera", initial_message);
    srv::CAMERA_IMAGE_FORMAT image_format;
    
    // -[ Define use parameters ]--------------------------------------------------------------------------------------------------------------------
    parameters.addParameter(new srv::ParameterCharArray("camera_filename", "file of the camera video stream (e.g. /dev/camera0).", camera_filename));
    parameters.addParameter(new srv::ParameterUInt("--width", "pixels", "width of the retrieved image of the camera.", 640, false, 0, false, 0, &width));
    parameters.addParameter(new srv::ParameterUInt("--height", "pixels", "height of the retrieved image of the camera.", 480, false, 0, false, 0, &height));
    parameters.addParameter(new srv::ParameterUInt("--rate", "rate", "denominator of the frame rate of the camera (i.e 1/<rate>).", 30, false, 0, false, 0, &rate));
    parameters.addParameter(new srv::ParameterUInt("--format", "index", "format of the retrieved images. Possible values are:"
                                                                        "&0&YUYV image format."
                                                                        "&1&JPEG image format."
                                                                        "&2&Motion JPEG (MJPEG) image format.", 0, true, 0, true, 2, & image_format_index));
    parameters.addParameter(new srv::ParameterBoolean("--gaussian", "", "uses a Gaussian kernel to calculate the location of the chessboard kernels.", &information.gaussian_corners));
    parameters.addParameter(new srv::ParameterDouble("--time", "seconds", "time between processed frames to search a chessboard pattern.", 1.0, true, 0.03, false, 0.0, &information.process_time_step));
    parameters.addParameter(new srv::ParameterUInt("--thr", "threads", "number of threads used to concurrently process the image.", 1, true, 1, false, 0, &information.number_of_threads));
    
    // -[ Parse user parameters ]--------------------------------------------------------------------------------------------------------------------
    try
    {
        parameters.parse(argv, argc);
        if      (image_format_index == 0) image_format = srv::YUYV_FORMAT;
        else if (image_format_index == 1) image_format = srv::JPEG_FORMAT;
        else if (image_format_index == 2) image_format = srv::MJPEG_FORMAT;
        else throw srv::Exception("Image format index '%d' is not known or it is not yet implemented.", image_format_index);
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception while parsing the user arguments:" << std::endl;
        std::cerr << e.what() << std::endl;
        parameters.synopsisMessage(std::cerr);
        std::cerr << std::endl;
        
        return EXIT_FAILURE;
    }
    
    // -[ Camera retrieved image ]-------------------------------------------------------------------------------------------------------------------
    try
    {
        srv::Image<unsigned char> merged_images(width * 2, height, 3);
        srv::Figure figure("Camera");
        srv::Camera * virtual_camera;
        unsigned int video_counter;
        srv::CameraLinux * camera;
        pthread_t work_thread;
        bool enable_video;
        
        virtual_camera = camera = new srv::CameraLinux(camera_filename, width, height, rate, image_format, false);
        camera->information(std::cout);
        information.camera_image.setGeometry(width, height, 3);
        information.pattern_image.setGeometry(width, height, 3);
        information.camera_image.setValue(128);
        information.pattern_image.setValue(128);
        information.search_patterns = false;
        information.thread_enabled = true;
        if (pthread_create(&work_thread, 0, &process_pattern, &information))
            throw srv::Exception("Work thread cannot be created.");
        
        std::cout << "[p/P] Enable or disable the search of chessboard patterns." << std::endl;
        std::cout << "[v/V] Enable or disable to sequentially store the capture results." << std::endl;
        enable_video = false;
        video_counter = 0;
        for (char key = 0; key != 27; key = srv::Figure::wait(20))
        {
            camera->retrieveFrame(50, 100);
            virtual_camera->copyImage(information.camera_image);
            
            if ((key == 'p') || (key == 'P')) information.search_patterns = !information.search_patterns;
            if ((key == 'v') || (key == 'V')) enable_video = !enable_video;
            
            merged_images.subimage(0, 0, width, height).copy(information.camera_image);
            merged_images.subimage(width, 0, width, height).copy(information.pattern_image);
            if (enable_video)
            {
                char filename[512];
                sprintf(filename, "sequence%06d.jpg", video_counter);
                merged_images.save(filename);
                ++video_counter;
            }
            figure(merged_images);
        }
        information.thread_enabled = false;
        pthread_join(work_thread, 0);
        
        delete virtual_camera;
    }
    catch (srv::Exception &e)
    {
        std::cerr << "[ERROR] Unhanded exception calling the camera interface:" << std::endl;
        std::cerr << e.what() << std::endl << std::endl;
        
        return EXIT_FAILURE;
    }
    
    return EXIT_SUCCESS;
}

